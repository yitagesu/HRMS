/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.camisweb.viewmodel;

import java.util.ArrayList;

/**
 *
 * @author user
 */
public class ProjectInfo {
    public static class project{
        public static class project_mgmt{
            public int project_id;
            public String project_name;
            public String project_type;
            public String project_location;
            public String zone;
            public String woreda;
            public String kebele;
            public String city;
            public String landmark; 
            public String perquisite_activites;
            public String briefdesc_work;
            public String briefdesc_psubactivites;
            public String start_date;
            public String end_date;
            public String milestones;
            
            
        }
        public ArrayList<project_mgmt> pm;
    }
    
}
