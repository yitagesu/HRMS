/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.camisweb.model;


 public  class ProjectDetailModel {
    private int id;
    private String name;
    private String type;
    private String location;
    private String zone;
    private String woreda;
    private String kebele;
    private String city;
    private String neighborhood;
    private String prerequisite;
    private String description;
    private String subActivityDescription;
    
    public ProjectDetailModel(){

    }
     
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * @return the zone
     */
    public String getZone() {
        return zone;
    }

    /**
     * @param zone the zone to set
     */
    public void setZone(String zone) {
        this.zone = zone;
    }

    /**
     * @return the woreda
     */
    public String getWoreda() {
        return woreda;
    }

    /**
     * @param woreda the woreda to set
     */
    public void setWoreda(String woreda) {
        this.woreda = woreda;
    }

    /**
     * @return the kebele
     */
    public String getKebele() {
        return kebele;
    }

    /**
     * @param kebele the kebele to set
     */
    public void setKebele(String kebele) {
        this.kebele = kebele;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return the neighborhood
     */
    public String getNeighborhood() {
        return neighborhood;
    }

    /**
     * @param neighborhood the neighborhood to set
     */
    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    /**
     * @return the prerequisite
     */
    public String getPrerequisite() {
        return prerequisite;
    }

    /**
     * @param prerequisite the prerequisite to set
     */
    public void setPrerequisite(String prerequisite) {
        this.prerequisite = prerequisite;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the subActivityDescription
     */
    public String getSubActivityDescription() {
        return subActivityDescription;
    }

    /**
     * @param subActivityDescription the subActivityDescription to set
     */
    public void setSubActivityDescription(String subActivityDescription) {
        this.subActivityDescription = subActivityDescription;
    }
   }