package com.intaps.camisweb.viewmodel;

import java.util.ArrayList;

public class LandInfo {
  public static class LandBank{
        public static class LandValue{
            public String investorName;
            public String upi;
            public String location;
            public double area;
            public boolean status;
        }
        public ArrayList<LandValue> values;
    }
  public static class CommInvestor{
      public static class CommInverstorData{
          public String investorName;
          public double area;
          public long capital;
      }
      public ArrayList<CommInverstorData> values;
  }
  public static class Investor{
      public static class InvestorData{
          public String inverstorName;
          public double area;
      }
      public ArrayList<InvestorData> data;
  }
  public static class LandBankDashBoard{
      public static class LandBankDashBoardData{
          public String upi;
          public String investmentType;
          public double area;
      }
      public ArrayList<LandBankDashBoardData> data;
  }
  public static class Evaluator{
      public static class EvaluatorData{
          public String evaluatorName;
          public String evaluatorEmailAddress;
          public long evaluatorPhoneNumber;
      }
      public ArrayList<EvaluatorData> data;
  }
  public static class LandTransferProcess{
      public static class LandTransferProcessData{
          public String landId;
          public String landType;
          public double area;
          public String status;
      }
      public ArrayList<LandTransferProcessData> data;
  }

    public static class LandBankProfile{
      public static class LandBankProfileData{
          public String parcelId;
          public String kebele;
          public String woreda;
          public String zone;
          public String region;
          public String city;
          public String address;
           public String centroid;
          public double xCordinate;
          public double yCordinate;
          public double area;
          public String investmentType;
          public String landUse;
          public String statusInfraRoadElec;
          public String statusInfraTele;
          public String statusInfraHealth;
          public String statusInfraEducation;
          public String statusInfraAdmin;
          public String conditionExpansion;
          public String socioEconomy;
          public String accessibilityAir;
          public String accessibilityWater;
          public String accessibilityRoad;
          public String accessibilityRail;
          public double distance;
          public String ageroEcology;
          public String surUnderWaterPote;
          public String waterPote;
          public double leasePerHectare;
          public String leaseType;
          public String leaseDuration;
          public String neighrbourFromNorth;
          public String neighrbourFromSouth;
          public String neighrbourFromWest;
          public String neighrbourFromEast;
          public double height;
          public String windDirecSpeed;
          public String rainfallDist;
          public String humudity;
          public String coverOfHerbs;
          public String landscape;
          public String others;
      }
      public ArrayList<LandBankProfileData> data;
  }
  
  public static class AuthenticationForm{
      public static class Form
      {
         public String companyName;
         public String kebele;
         public String woreda;
         public String zone;
         public String fax;
         public String region;
         public String phone;
         public String email;
         public String landBelongs;
         public Double landPerHec;
         public String landNeigbours;
         public String east;
         public String west;
         public String north;
         public String south;
         public Double landLeaseRate;
         public String leaseContractDetail;
                 
      }
      public ArrayList<Form> data;
  }
}
