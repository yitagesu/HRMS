/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.camisweb.servlet;



import com.intaps.camisweb.mcontroller.ProjectDetailController;
import com.intaps.camisweb.model.ProjectDetailModel;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author user
 */
@WebServlet("/projectDetails")
public class ProjectDetailServlet extends HttpServlet{
     @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
        String url = "/proj_detail_form.jsp";
    
            
         String action = request.getParameter("action");
        if (action == null) {
            action = "join";  // default action
        }

        // perform action and set URL to appropriate page
        if (action.equals("join")) {
            url = "/proj_detail_form.jsp";    // the "join" page
        } 
        else if (action.equals("add")) {
            
            
            String id = request.getParameter("id");
            String name = request.getParameter("name");
            String type = request.getParameter("type");
            String location = request.getParameter("location");
            String zone = request.getParameter("zone");
            String woreda = request.getParameter("woreda");
            String kebele = request.getParameter("kebele");
            String city = request.getParameter("city");
            String neighborhood = request.getParameter("neighborhood");
            String prerequisite = request.getParameter("prerequisite");
            String description = request.getParameter("description");
            String subActivities = request.getParameter("sub_activities");
            
           ProjectDetailModel pd = new ProjectDetailModel();
            
            pd.setId(Integer.parseInt(id));
            pd.setName(name);
            pd.setType(type);
            pd.setLocation(location);
            pd.setZone(zone);
            pd.setWoreda(woreda);
            pd.setKebele(kebele);
            pd.setCity(city);
            pd.setNeighborhood(neighborhood);
            pd.setPrerequisite(prerequisite);
            pd.setDescription(description);
            pd.setSubActivityDescription(subActivities);

            // validate the parameters
            String message;
            if (id == null || name == null || type == null ||
                 location == null || zone == null || woreda == null ||
                kebele == null || city == null || neighborhood == null ||
                prerequisite == null || description == null || subActivities == null ||
                id.isEmpty() || name.isEmpty() || type.isEmpty() || location.isEmpty() ||
                zone.isEmpty() || woreda.isEmpty() || kebele.isEmpty() ||
                 city.isEmpty() || neighborhood.isEmpty() || prerequisite.isEmpty() ||
                 description.isEmpty() || subActivities.isEmpty()) {
                message = "Please fill out all Fields.";
                url = "/proj_detail_form.jsp";
               
            } 
            else {
                message = "";
                url = "/thanks.jsp";
                 new ProjectDetailController(request, request).saveProject(pd);
               
            }
           
            request.setAttribute("project", pd);
            request.setAttribute("message", message);
            
        }
        
        getServletContext()
                .getRequestDispatcher(url)
                .forward(request, response);
    }
    
    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    
}
}
