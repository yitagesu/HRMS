/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.camisweb.vmcontroller;

import com.intaps.camisweb.viewmodel.InvestorInfo;
import java.util.ArrayList;

/**
 *
 * @author joseph
 */
public class Investor {
     public static InvestorInfo.investerAssesment getInvestorP(){
        InvestorInfo.investerAssesment lv = new InvestorInfo.investerAssesment();
        lv.ip = new ArrayList();
        
        InvestorInfo.investerAssesment.investorProfile lbv = new InvestorInfo.investerAssesment.investorProfile();
        lbv.no=1;
        lbv.Company = "Merti";
        lbv.Owner = "Debebe Dawit";
        lbv.CType = "PLC";
        lbv.PType ="NEW";
        lbv.PSDate = "2010-7-4";
        
        lv.ip.add(lbv);
        return lv;
     }
     public static InvestorInfo.investerAssesDetails getInvestorD(){
        InvestorInfo.investerAssesDetails lv = new InvestorInfo.investerAssesDetails();
        lv.ID = new ArrayList();
        
        InvestorInfo.investerAssesDetails.investorDetails lbv = new InvestorInfo.investerAssesDetails.investorDetails();
            lbv.Company="Anbesa PLC ";
            lbv.Owner="Abebe Balcha";
            lbv.CType=" PLC";
            lbv.IType="Local Investor ";
            lbv.IPNo=" 123";
            lbv.PType=" Farming";
            lbv.PSDate="07/04/2009";
            lbv.PPeriod=" 5 Years";
            lbv.Region="Amhara ";
            lbv.Zone=" 1";
            lbv.Woreda="12 ";
            lbv.City=" Bahir Dar";
            lbv.Address=" dd";
            lbv.Phone="05874125862452 ";
            lbv.Email="dskajflkad@email.com ";
            lbv.Fax="798465312 ";
            lbv.Faxi="78645312";
            lbv.Perma="17 male 2 female ";
            lbv.Tempo="9 male 4 female ";
            lbv.Total="26 male, 6female ";
            lbv.PStatus="on progress";
            lbv.GovIncentive="none ";
            lbv.SCapital="2,000,000 ";
            lbv.CCapital="5,000,000 ";
            lbv.PCTAmtperYR="1500 ";
            lbv.Suppliers="local farmers ";
            lbv.TechUsed="Tractor ";
            lbv.ProductionUsed="machine";
            lbv.ProductionCapacity="1200/day ";
            lbv.ProductionSellsArea="Oromia";
            lbv.Evaluation="Excellent ";
            
            lbv.Nationality="Ethiopian";
            lbv.taxid="48565";
            lbv.comreg="djfas45684789";
            lbv.formofownership="Public Enterprize";
            lbv.kebele="12";
            lbv.HouseNo="1212";
            lbv.Telephone="09123456789";
            lbv.towner="Domestic Investor";
            lbv.sa="123 Bale street";
            lbv.post="1234";
            lbv.ec="09123456789";
            
        
        lv.ID.add(lbv);
        return lv;
     }
     public static InvestorInfo.investordocuments getinvestordocs() {
     InvestorInfo.investordocuments lv = new InvestorInfo.investordocuments();
        lv.ids = new ArrayList();
        InvestorInfo.investordocuments.investordocs doc = new InvestorInfo.investordocuments.investordocs();
            doc.poa="power of Attorny.pdf";
            doc.memorandum="memeo.pdf";
            doc.roe="Regulations.pdf";
            doc.aoa="Articles.pdf";
            doc.slla="support letter.pdf";
            doc.ailid="Agriculture.pdf";
            doc.landtl="land transfer letter.pdf";
            doc.workpermit="workpermit.pdf";
            doc.cflrs="Certificate.pdf";
            doc.profitorloss="Balance.pdf";
           doc.organiztionlaw="Organizational law.pdf";
           doc.meetingminutes="Meeting Minutes.pdf";
           doc.id="ID.pdf";
           doc.passport="Passport.pdf";
           doc.lfdremofa="Letter.pdf"; 
           doc.supportletter="Support letter.pdf";
           doc.businesslicence="business licence.pdf";
           doc.businessplan="business plan.pdf";
           doc.siteplanofleaseland="site plan.pdf";
           doc.leasedlanddeliveryminute="leased land minute.pdf";
           doc.eiad="Environment impact assessment document.pdf";
           doc.performancereports="Performance reports.pdf";
           doc.closureplan="Closure plan.pdf";
           doc.communityagree="Community agreement (social contract).pdf";
           doc.landreqform="Land request form.pdf";
           doc.cropcalander="Crop Calendar.pdf";
           doc.employeecontractdoc="Employee Contract Document.pdf";
           doc.internalruleandregdoc="Internal Rule and regulation document .pdf";
           doc.raciasdmt="Annual crop infestation and spread record.pdf";
           doc.iitrd="Investor investment track record document.pdf";

            lv.ids.add(doc);
        
        
        return lv;
     }
}
