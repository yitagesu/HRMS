/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.camisweb.viewmodel;

import java.util.ArrayList;


/**
 *
 * @author joseph
 */
public class InvestorInfo {
    
   public static class investerAssesment{
        public static class investorProfile{
            public int no;
            public String Company;
            public String Owner;
            public String CType;
            public String PType;
            public String PSDate;
            
        }
        public ArrayList<investorProfile> ip;
    }
   
  
   
   
   public static class investerAssesDetails{
        public static class investorDetails{
          
            public String Company;
            public String Owner;
            public String CType;
            public String IType;
            public String IPNo;
            public String PType;
            public String PSDate;
            public String PPeriod;
            public String Region;
            public String Zone;
            public String Woreda;
            public String City;
            public String Address;
            public String Phone;
            public String Email;
            public String Fax;
            public String Faxi;
            public String Perma;
            public String Tempo;
            public String Total;
            public String PStatus;
            public String GovIncentive;
            public String SCapital;
            public String CCapital;
            public String PCTAmtperYR;
            public String Suppliers;
            public String TechUsed;
            public String ProductionUsed;
            public String ProductionCapacity;
            public String ProductionSellsArea;
            public String Evaluation;
            
            public String Nationality;
            public String taxid;
            public String comreg;
            public String formofownership;
            public String kebele;
            public String HouseNo;
            public String Telephone;
            public String towner;
            public String post;
            public String sa;
            public String ec;
            
            
        }
        public ArrayList<investorDetails> ID;
    }
   public static class investordocuments{
       public static class investordocs{
           public String poa;// power of Attorny
           public String memorandum;
           public String roe;
           public String aoa;
           public String slla; //Support letter for Lease Applications
           public String ailid;
           public String landtl;
           public String workpermit;
           public String cflrs;//Certificate for land use by Regional State Environment, Forest & Land Administration Bureau
           public String profitorloss;
           public String organiztionlaw;
           public String meetingminutes;
           public String id;
           public String passport;
           public String lfdremofa; //Letter from Federal Democratic Republic of Ethiopia Ministry of Foreign Affairs for Diaspora Association 
           public String supportletter;//Support letter to Invest as Diaspora from Foreign Minister Investor for Diaspora Activity Directorate General : 
           public String businesslicence;
           public String businessplan;
           public String siteplanofleaseland;
           public String leasedlanddeliveryminute;
           public String eiad;
           public String performancereports;
           public String closureplan;
           public String communityagree;
           public String landreqform;
           public String cropcalander;
           public String employeecontractdoc;
           public String internalruleandregdoc;
           public String raciasdmt;//Record of annual crop infestations and spread of disease and measures taken
           public String iitrd;//Investor investment track record document
           
       }
       public ArrayList<investordocs> ids;
   }
}
