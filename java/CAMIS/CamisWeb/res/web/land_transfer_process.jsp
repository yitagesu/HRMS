<%--
    Document   : land_transfer_process
    Created on : Nov 25, 2017, 10:38:44 AM
    Author     : Isaac
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%@page import="com.intaps.camisweb.*"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>CAMIS</title>
        <link rel="stylesheet" href="assets/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="assets/css/external.css"/>

        <link rel="stylesheet" href="assets/css/custom.min.css"/>

    </head>
    <body class="nav-sm">
            <div class="container body">
            <div class="main_container">

                <div class="col-md-3 left_col menu_fixed">
                    <div class="left_col scroll-view">
                        <div class="navbar nav_title" style="border: 0;">
                            <a href="#" class="site_title"><i class="fa fa-map-marker"></i> <span>CAMiS</span></a>
                        </div>

                        <div id="sidebar-menu" class="main_menu_side main_menu">
                            <div class="menu_section">
                                <h3 style="visibility:hidden;">general</h3>
                                <ul class="nav side-menu">
                                    <li class="active"><a href=".dashboard" data-toggle="tab" ><i class="fa fa-table"></i> Dashboard </a>
                                    </li>
                                    <li><a href="#user" data-toggle="tab" ><i class="fa fa-user"></i> User Management</a>
                                    </li>
                                    <li><a><i class="fa fa-gears"></i> Setting</a>
                                    </li>
                                    <li><a><i class="fa fa-gear"></i> Other</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="top_nav">
                    <div class="nav_menu">
                        <nav>
                            <div class="nav toggle">
                                <a id=""><i class="fa fa-map-marker"></i> <span>CAMiS</span></a>
                                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                            </div>

                            <ul class="nav navbar-nav navbar-right">
                                <li class="">
                                    <a href="" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        Welcome yite
                                        <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                                        <li><a href="#"> Change Password</a></li>
                                        <li><a href="#">Help</a></li>
                                        <li><a href="/login.jsp"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="right_col tab-content" role="main">
                    <div>
                        <div class="row">
                            <div class="col-md-8 col-sm-8 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Land Transfer Process</h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>
                                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>

                                    <div class="x_content">
                                        <form class="form-horizontal form-label-left" novalidate>
                                            <div class="input-group">
                                                <span class="input-group-addon">Land type</span>
                                                <input type="text" id="invName" name="invName"  class="form-control">
                                            </div>
                                            <div class="input-group">
                                                <span class="input-group-addon">Land status</span>
                                                <input type="text" id="parNum" name="parNum"  class="form-control">
                                            </div>
                                            <div class="form-group align-right">
                                                <button id="search_button" class="btn btn-success "><i class="fa fa-search"></i> Search</button>
                                            </div>
                                        </form>
                                        <div class="search-result has-scroll">
                                            <table class="table table-striped jambo_table bulk_action">
                                                <thead>
                                                    <tr class="headings">
                                                        <th class="column-title">LandID </th>
                                                        <th class="column-title">Land type </th>
                                                        <th class="column-title">Area</th>
                                                        <th class="column-title">Status </th>
                                                    </tr>
                                                </thead>

                                                 <%
                                                LandInfo.LandTransferProcess retLand = Land.getLandTransferProcess();
                                                for(LandInfo.LandTransferProcess.LandTransferProcessData val:retLand.data){
                                                %>
                                                <tbody>

                                                    <tr class="even pointer">
                                                        <td class=" "><%=val.landId%></td>
                                                        <td class=" "><%=val.landType%> </td>
                                                        <td class=" "><%=val.area%></td>
                                                        <td class="a-right a-right "><%=val.status%></td>
                                                    </tr>

                                                    <%}%>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Gambela land adminisration </h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>

                                    <div class="x_content">
                                        <div class="row">
                                        <div class="map has-scroll" id="map">
                                            <div class="col-md-6">
                                                <ul>
                                                    <li>Prepared</li>
                                                    <li>Bid</li>
                                                    <li>Moderate</li>
                                                </ul>
                                            </div>
                                            <div class="col-md-6"> <img src="assets/images/ethiopia.png" class="img-responsive"/></div>
                                         </div>
                                        </div>
                                        <hr style="border-width: 3px">
                                           <img src="assets/images/map.jpg" class="img-responsive"/>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

    </body>
    <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/external.js"></script>
        <script src="assets/js/Char.min.js"></script>
        <script src="assets/js/bootstrap-progressbar.min.js"></script>
        <script src="assets/js/icheck.min.js"></script>
        <script src="assets/js/custom.min.js"></script>
</html>
