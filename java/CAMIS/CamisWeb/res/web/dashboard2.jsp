
<!DOCTYPE html>
<%@page import="com.intaps.camisweb.*"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>CAMIS</title>
        <link rel="stylesheet" href="/assets/css/external.css"/>
        <link rel="stylesheet" href="/assets/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="/assets/css/custom.min.css"/>
        

    </head>

    <body class="nav-sm">
        <div class="container body">
            <div class="main_container">

                <%@include file="all.jsp" %>
                <div class="right_col tab-content" role="main">
                    <div>
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Search...</h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>
                                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>

                                    <div class="x_content">
                                        <form class="form-horizontal form-label-left" novalidate>
                                            <div class="input-group">
                                                <span class="input-group-addon">Investor Name</span>
                                                <input type="text" id="invName" name="invName"  class="form-control">
                                            </div>
                                            <div class="input-group">
                                                <span class="input-group-addon">Parcel Number</span>
                                                <input type="text" id="parNum" name="parNum"  class="form-control">
                                            </div>
                                            <div class="form-group align-right">
                                                <button id="search_button" class="btn btn-success "><i class="fa fa-search"></i> Search</button>
                                            </div>
                                        </form>
                                        <div class="search-result has-scroll">
                                            <table class="table table-striped jambo_table bulk_action">
                                                <thead>
                                                    <tr class="headings">
                                                        <th class="column-title">Investor Name </th>
                                                        <th class="column-title">Parcel Number </th>
                                                        <th class="column-title">Location</th>
                                                        <th class="column-title">Area</th>
                                                        <th class="column-title">Status </th>
                                                    </tr>
                                                </thead>
 <%
                                                LandInfo.LandBank retLand3 = Land.getLandValue();
                                                for(LandInfo.LandBank.LandValue val:retLand3.values){
                                                %>
                                                <tbody>
                                                    <tr class="even pointer">
                                                        <td class=" "><%=val.investorName%></td>
                                                        <td class=" "><%=val.upi%></td>
                                                        <td class=" "><%=val.location%></td>
                                                        <td class=" "><%=val.area%></td>
                                                        <td class="a-right a-right "><%=val.status%></td>
                                                    </tr>
                                                  <%}%>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Thematic</h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>
                                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>

                                    <div class="x_content">
                                        <div class="map has-scroll" id="map">
                                            <img src="assets/images/map.jpg" class="img-responsive"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Land Bank</h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>
                                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>

                                     <div class="x_content">
                                     <%
                                                LandInfo.LandBankDashBoard retLand = Land.getLandBankDashBoardData();
                                                for(LandInfo.LandBankDashBoard.LandBankDashBoardData val:retLand.data){
                                                %>
                                   
                                        <article class="media event">
                                            <a class="pull-left date">
                                                <p class="day"><i class="fa fa-map-marker"></i></p>
                                            </a>
                                            <div class="media-body">
                                                <a class="title" href="#"><%=val.upi%></a>
                                                <p><%=val.investmentType%> </p>
                                                <p>Area: <%=val.area%></p>
                                            </div>
                                        </article>
                                         <%}%>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Commercial Investor</h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>
                                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>

                                    <div class="x_content">
                                        <table class="table table-striped">
                                                <thead>
                                                    <tr class="headings">
                                                        <th class="column-title">Investor Name </th>
                                                        <th class="column-title">Investment Area</th>
                                                        <th class="column-title">Capital</th>
                                                    </tr>
                                                </thead>
  <%
                                                LandInfo.CommInvestor retLand1 = Land.getCommInvestor();
                                                for(LandInfo.CommInvestor.CommInverstorData val:retLand1.values){
                                                %>
                                                <tbody>
                                                    <tr class="even pointer">
                                                        <td class=" "><%=val.investorName%></td>
                                                        <td class=" "><%=val.area%></td>
                                                        <td class=" "><%=val.capital%></td>
                                                    </tr>
                                                     <%}%>
                                                </tbody>
                                            </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Investor</h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>
                                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>

                                    <div class="x_content">
                                        <table class="table table-striped">
                                                <thead>
                                                    <tr class="headings">
                                                        <th class="column-title">Investor Name </th>
                                                        <th class="column-title">Investment Area</th>
                                                    </tr>
                                                </thead>
 <%
                                                LandInfo.Investor retLand2 = Land.getInvestorData();
                                                for(LandInfo.Investor.InvestorData val:retLand2.data){
                                                %>
                                                <tbody>
                                                    <tr class="even pointer">
                                                        <td class=" "><%=val.inverstorName%></td>
                                                        <td class=" "><%=val.area%></td>
                                                    </tr>
                                                    <%}%>
                                                </tbody>
                                            </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script src="/assets/js/jquery.min.js"></script>
        <script src="/assets/js/external.js"></script>
        <script src="/assets/js/Char.min.js"></script>
        <script src="/assets/js/bootstrap-progressbar.min.js"></script>
        <script src="/assets/js/bootstrap.min.js"></script>
        <script src="/assets/js/custom.min.js"></script>
    </body>
</html>
