<%@page contentType="text/html" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Project Details</title>
    <link rel="stylesheet" href="styles/main.css" type="text/css"/>    
</head>

<body>
  

    <p>Here is the information that you entered:</p>

    <label>Project Id:</label>
    <span>${project.id}</span><br>
    <label>Project Name:</label>
    <span>${project.name}</span><br>
    <label>Project Type:</label>
    <span>${project.type}</span><br>
    <label>Project Location</label>
    <span>${project.location}

    <p>To enter another Project, click on the Back 
    button in your browser or the Return button shown 
    below.</p>

    <form action="" method="post">
        <input type="hidden" name="action" value="join">
        <input type="submit" value="Return">
    </form>

</body>
</html>