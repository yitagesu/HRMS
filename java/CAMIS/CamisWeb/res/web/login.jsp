<%-- 
    Document   : login.jsp
    Created on : Nov 9, 2017, 4:30:56 PM
    Author     : Yitagesu
--%>



<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>CAMIS Login</title>
        <link rel="stylesheet" href="/assets/css/external.css"/>
        <link rel="stylesheet" href="/assets/css/bootstrap.min.css"/>
        <script src="/assets/js/jquery.js"></script>
        <script src="/assets/js/jquery-ui.js"></script>
        <script src="/assets/js/external.js"></script>
    </head>

    <body id="login">
        <div class="container">
            <div class="login-warper">
                <div class="parties">
                    <div class="camis">
                        <i class="fa fa-map-marker"></i>
                        <h1>CAMiS</h1>
                        <p>Commercial Agriculture Management Information System</p>
                    </div>
                    <div class="parties-logo">
                        <img src="assets/images/GIZ-logo.png" class="first"/>
                        <img src="assets/images/ata-logo.png" class="middle"/>
                        <img src="assets/images/horticulture.png" class="last"/>

                    </div>
                </div>
                <div class="login-page">
                    <div>

                        <div class="login-content">
                            <div class="align-center login-icon">
                                <img src="assets/images/camis.png">
                                <p>Commercial Agriculture Management Information System</p>
                            </div>
                            <div class="align-center">
                                <h3>Please Login</h3>
                                <hr>
                            </div>
                            <div >
                                <input type="text" class="username" id="username" placeholder="Username">
                                <input type="password" class="password" id="password" placeholder="Password">

                            </div>
                            <div class="login-message" id="login-message">
                                
                            </div>
                            <div class="align-right">
                                <button class="btn btn-defualt">Clear</button>
                                <button class="btn btn-success" onclick="valLogin()">Login</button>
                            </div>
                            
                        </div>
                        <div class="login-footer">
                            <div ><p class="align-center">powered by <a href="https://www.intaps.com">INTAPS Consultancy plc</a></p></div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </body>
</html>
