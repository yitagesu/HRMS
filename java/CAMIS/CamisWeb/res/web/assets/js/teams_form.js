/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var teamInfo = {
    teamParcelNoRanges: []
}
function saveTeamInfo()
{
    putTeamInfo();
//    alert(JSON.stringify(teamInfo));
    $.post("/teaminfo", JSON.stringify(teamInfo), function (res)
    {
        if (res.error != null)
        {
            bootbox.alert(res.error);
        } else
        {
            bootbox.alert({
                message: _lang_you_have_successfully_register_team_information_,
                callback: function () {
                    window.location.href = "/index?action=WELCOME";
                    $("#addTeamModal").modal("hide");
                    $("#addTeamForm").trigger("reset");
                    $("#parcel_range_table_body").children().remove();
                }
            })
        }
    });

}
function putTeamInfo()
{
    teamInfo.userId = parseInt($("#admin_teams_userid").val());
    if (isNaN(teamInfo.userId))
        teamInfo.userId = -1;
    teamInfo.teamName = $("#admin_teams_name").val();
    teamInfo.teamDescription = $("#admin_teams_teamdescription").val();
    teamInfo.kebele = $("#teams_kebele").val();
}
function putPracelRange(index)
{
    var rangeObject = {}
    rangeObject.rangefrom = parseInt($("#admin_teams_rangefrom").val());
    rangeObject.rangeto = parseInt($("#admin_teams_rangeto").val());

    if (index == null)
    {
        teamInfo.teamParcelNoRanges.push(rangeObject);
    } else
    {
        teamInfo.teamParcelNoRanges[index] = rangeObject;
    }
    return rangeObject;
}
function removeParcelRenge(rowid)
{
    index = $("#" + rowid).index();

    bootbox.confirm("Are You Sure You Want to Remove This?", function (result) {
        if (result) {
            $("#" + rowid).remove();
            teamInfo.teamParcelNoRanges.splice(index, 1);
        }
    });



}
var range_row_id = 1;
var edit_row = null;
function populateTeamInfoByIndex(rowID)
{
    rowID = teamInfo.teamid;
    $("#admin_teams_userid").val(teamInfo.userId);
    $("#admin_teams_name").val(teamInfo.teamName);
    $("#admin_teams_teamdescription").val(teamInfo.teamDescription);
    $("#teams_kebele").val(teamInfo.kebele);
    $("#addTeamModal").modal("show");
}

var range_edit_row = null;
function addParcelRangetoTable(rowid, rangeObject)
{
    var rowHTML = ` <td>
                                                                    <span>` + rangeObject.rangefrom + `</span>
                                                                </td>
                                                                <td>
                                                                    <span>` + rangeObject.rangeto + `</span>
                                                                </td>
                                                                <td class="align-center">
                                                                   <a href="javascript:removeParcelRenge('` + rowid + `')" class="btn btn-default"><i class="fa fa-trash"></i></a>
                                                                </td>`;
    if (range_edit_row == null)
    {
        $("#parcel_range_table_body").append(`<tr id="` + rowid + `">` + rowHTML + `</tr>`);
        range_row_id++;
    } else
    {
        $("#parcel_range_table_body").children().eq(range_edit_row).html(rowHTML);
    }
}
function buttonAddParcelRange()
{
    var parcelRange = putPracelRange(range_edit_row);
    var rowid;
    if (range_edit_row == null)
    {
        rowid = "range_row_" + range_row_id;
        range_row_id++;
    } else
        rowid = $("#parcel_range_table_body").children().eq(range_edit_row).attr('id');

    addParcelRangetoTable(rowid, parcelRange);
    range_edit_row = null
    $("#range-input input[type=text]").val("");
}


function validateParcelRange()
{
    var valid = true;
    var requiredField = ['admin_teams_rangefrom', 'admin_teams_rangeto'];
    for (var i = 0; i < requiredField.length; i++) {
        var arraylist = $('#' + requiredField[i]).val();
        if (arraylist == "" || arraylist == -1 || arraylist == null || isNaN(arraylist)) {
            $('#' + requiredField[i]).removeClass('form-control')
            $('#' + requiredField[i]).addClass('error');

            valid = false;
        } else
        {
            $('#' + requiredField[i]).removeClass('error')
            $('#' + requiredField[i]).addClass('form-control');
        }
    }
    var rfrom;
    var rto;
    var rangefrom = parseInt($('#admin_teams_rangefrom').val());
    var rangeto = parseInt($('#admin_teams_rangeto').val());
    if (rangefrom >= rangeto)
    {
        $('#range-error').show();
        valid = false;
    } else
    {
        $('#range-error').hide();
        for (var i = 0; i < teamInfo.teamParcelNoRanges.length; i++)
        {
            rfrom = teamInfo.teamParcelNoRanges[i].rangefrom;
            rto = teamInfo.teamParcelNoRanges[i].rangeto;
            if(rangefrom<=rfrom &&(rangeto<=rto && rangeto>=rfrom)){
                $('#range-error3').show();
                valid = false;
            }
            else if((rangefrom>=rfrom && rangefrom<=rto)&& rangeto>=rto){
                $('#range-error3').show();
                valid = false;
            }
            else if(rangefrom<=rfrom && rangeto>=rto){
                $('#range-error3').show();
                valid = false;
            }
            else if(rangefrom>=rfrom && rangeto<=rto){
                $('#range-error3').show();
                valid = false;
            }
            else{
                $('#range-error3').hide();
            }
        }
    }

    if (valid)
    {
        buttonAddParcelRange();
    }
}

function validateTeamInfo() {
    var valid = true;
    var requiredField = ['admin_teams_userid', 'admin_teams_name', 'admin_teams_teamdescription', 'teams_kebele'];
    for (var i = 0; i < requiredField.length; i++) {
        var arraylist = $('#' + requiredField[i]).val();
        if (arraylist == "" || arraylist == -1 || arraylist == null) {
            $('#' + requiredField[i]).removeClass('form-control')
            $('#' + requiredField[i]).addClass('error');

            valid = false;
        } else {
            $('#' + requiredField[i]).removeClass('error')
            $('#' + requiredField[i]).addClass('form-control');
        }
    }
    var range = $("#parcel_range_table_body").children();
    if (range.length < 1)
    {
        $("#parcel-range-error").show();
        valid = false;
    } else
    {
        $("#parcel-range-error").hide();
    }
    if (valid)
    {
        saveTeamInfo();
    }
}
var editMode = false;
function editTeam(rowID)
{
    editMode = true;
    $.get("/teaminfo?teamID=" + rowID, function (response) {
        if (response.error)
        {
            alert(response.error);
        } else {
            teamInfo = response.res;
            $("#teams_kebele").val(response.res.kebele);
            $("#admin_teams_userid").val(response.res.userId);
            $("#admin_teams_name").val(response.res.teamName);
            $("#admin_teams_teamdescription").val(response.res.teamDescription);

            response.res.teamParcelNoRanges.forEach(function (rng)
            {
                addParcelRangetoTable(rowID, rng);
            });
            $("#addTeamModal").modal("show");
        }
    });
}
function deleteTeam(rowID)
{
    bootbox.confirm(_lang_are_you_sure_to_delete_, function (result) {
        if (result) {
            $.post("/teaminfo?teamID=" + rowID, function (res)
            {
                if (res.error != null)
                {
                    bootbox.alert(res.error);
                } else
                {
                    bootbox.alert({
                        message: _lang_you_have_successfully_deleted_a_team_,
                        callback: function () {
                            $('#team_table').load("index?action=WELCOME #team_table");
                        }
                    })
                }
            });
        }
    });
}

function resetform() {
    $("#addTeamForm").trigger("reset");
    $("#parcel_range_table_body td").remove();
}

function getKebleStr(kebele)
{
    return $("#teams_kebele option[value='" + kebele + "']").text();
}