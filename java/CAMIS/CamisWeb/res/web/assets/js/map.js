/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


            
            
            var container = document.getElementById('popup');
            var content = document.getElementById('popup-content');
            var closer = document.getElementById('popup-closer');

            var overlay = new ol.Overlay({
              element: container,
              autoPan: true,
              autoPanAnimation: {
                duration: 250
              }
            });
            

            closer.onclick = function() {
            overlay.setPosition(undefined);
            closer.blur();
            return false;
          };

        proj4.defs("EPSG:20137","+proj=utm +zone=37 +ellps=clrk80 +towgs84=-166,-15,204,0,0,0,0 +units=m +no_defs");
            var extentStyle = new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: 'white',
                    width: 1.5
                }),
                fill: new ol.style.Fill({
                    color: 'rgba(255, 255, 255, 0.5)'
                })
            });

            var extentSource = new ol.source.Vector({});
            var extentLayer = new ol.layer.Vector({
                projection: 'EPSG:20137',
                source: extentSource
            });
            

                  var format = 'image/png';
                  var bounds = [-170185.4375, 355913.125,
                                1502477.75, 1653799.875];
                

                  var mousePositionControl = new ol.control.MousePosition({
                    className: 'custom-mouse-position',
                    target: document.getElementById('location'),
                    coordinateFormat: ol.coordinate.createStringXY(5),
                    undefinedHTML: '&nbsp;'
                  });
                  var untiled = new ol.layer.Image({
                    source: new ol.source.ImageWMS({
                      ratio: 1,
                      url: 'http://localhost:8080/geoserver/ethio_map/wms',
                      params: {'FORMAT': format,
                               'VERSION': '1.1.1',  
                            STYLES: '',
                            LAYERS: 'ethio_map:ethiopia'
                      }
                    })
                  });
                  
                      var tiledE = new ol.layer.Tile({
                        visible: true,
                        source: new ol.source.TileWMS({
                          url: 'http://localhost:8080/geoserver/ethio_map/wms',
                          params: {'FORMAT': format, 
                                   'VERSION': '1.1.1',
                                   tiled: true,
                                STYLES: '',
                                LAYERS: 'ethio_map:ethiopia',
                             tilesOrigin: -170185.4375 + "," + 355913.125
                          }
                        })
                      });
                  
                       var tiled = new ol.layer.Tile({
                        visible: true,
                        source: new ol.source.TileWMS({
                          url: 'http://localhost:8080/geoserver/ethio_map/wms',
                          params: {'FORMAT': format, 
                                   'VERSION': '1.1.1',
                                   tiled: true,
                                STYLES: '',
                                LAYERS: 'ethio_map:ethiopiaboundary',
                             tilesOrigin: -170168.390625 + "," + 370040.375
                          }
                        })
                      });                     
    
                     var untiledL = new ol.layer.Image({
                    source: new ol.source.ImageWMS({
                      ratio: 1,
                      url: 'http://localhost:8080/geoserver/ethio_map/wms',
                      params: {'FORMAT': format,
                               'VERSION': '1.1.1',  
                            STYLES: '',
                            LAYERS: 'ethio_map:intaps_land'
                      }
                    })
                  });
                      

                  var projection = new ol.proj.Projection({
                      code: 'EPSG:20137',
                      units: 'm',
                      axisOrientation: 'neu',
                      global: false
                  });
                  
                        var vectorSource = new ol.source.Vector({
                        format: new ol.format.GeoJSON(),
                        url: function(extent) {
                          return 'http://localhost:8080/geoserver/ethio_map/wfs?service=WFS&' +
                              'version=1.1.0&request=GetFeature&typename=ethio_map:intaps_land&' +
                              'outputFormat=application/json&srsname=EPSG:20137&' +
                              'bbox=' + extent.join(',') + ',EPSG:4326';
                        },
                        strategy: ol.loadingstrategy.bbox
                      });
                      
                       var vectorSource1 = new ol.source.Vector({
                        format: new ol.format.GeoJSON(),
                        url: function(extent) {
                          return 'http://localhost:8080/geoserver/ethio_map/wfs?service=WFS&' +
                              'version=1.1.0&request=GetFeature&typename=ethio_map:in_land_bank&' +
                              'outputFormat=application/json&srsname=EPSG:20137&' +
                              'bbox=' + extent.join(',') + ',EPSG:4326';
                        },
                        strategy: ol.loadingstrategy.bbox
                      });
                      
                      var vectorSource2 = new ol.source.Vector({
                        format: new ol.format.GeoJSON(),
                        url: function(extent) {
                          return 'http://localhost:8080/geoserver/ethio_map/wfs?service=WFS&' +
                              'version=1.1.0&request=GetFeature&typename=ethio_map:unindentified&' +
                              'outputFormat=application/json&srsname=EPSG:20137&' +
                              'bbox=' + extent.join(',') + ',EPSG:4326';
                        },
                        strategy: ol.loadingstrategy.bbox
                      });
                      
                      
                      


                      var vec = new ol.layer.Vector({
                        source: vectorSource,
                            style: new ol.style.Style({
                            stroke: new ol.style.Stroke({
                            color: 'fuchsia',
                            width: 2
                            })
                          })
                      });
                      
                      var vec1 = new ol.layer.Vector({
                        source: vectorSource1,
                            style: new ol.style.Style({
                            stroke: new ol.style.Stroke({
                            color: 'greenyellow',
                            width: 2
                            })
                          })
                      });
                      
                      var vec2 = new ol.layer.Vector({
                        source: vectorSource2,
                            style: new ol.style.Style({
                            stroke: new ol.style.Stroke({
                            color: 'firebrick',
                            width: 2
                            })
                          })
                      });
                      
             var map    	
             loadMap();
             
             
                  var map2 = new ol.Map({
                    controls: ol.control.defaults({
                      attribution: false
                    }).extend([mousePositionControl]),
                    target: 'map_preview2',
                    layers: [
                      untiled,
                      extentLayer
                    ],
                    view: new ol.View({
                       projection: projection
                    })
                  });
                  
            map2.getView().fit(bounds, map.getSize());
            
            
            var featureOverlay = new ol.layer.Vector({
            source: new ol.source.Vector(),
            map: map,
            style: function(feature) {
             // highlightStyle.getText().setText(feature.get('name'));
              return highlightStyle;
            }
          });
          
          
                        var highlightStyle = new ol.style.Style({
                        stroke: new ol.style.Stroke({
                          color: '#f00',
                          width: 1
                        }),
                        fill: new ol.style.Fill({
                          color: 'rgb(0,0,255)'
                        })
                      });  
            
              var parser = new ol.format.WMSGetFeatureInfo();
              var highlightOverlay = new ol.layer.Vector({
               // style: (customize your highlight style here),
                style: new ol.style.Style({
                stroke: new ol.style.Stroke({
                color: 'rgba(0, 255, 255, 1.0)',
                width: 2
                })
              }),
		  source: new ol.source.Vector(),
		  map: map
		}); 
                  
            map.getView().on('change:resolution', function(evt) {
            var resolution = evt.target.get('resolution');
            var units = map.getView().getProjection().getUnits();
            var dpi = 25.4 / 0.28;
            var mpu = ol.proj.METERS_PER_UNIT[units];
            var scale = resolution * mpu * 39.37 * dpi;
            if (scale >= 9500 && scale <= 950000) {
              scale = Math.round(scale / 1000) + "K";
            } else if (scale >= 950000) {
              scale = Math.round(scale / 1000000) + "M";
            } else {
              scale = Math.round(scale);
            }
            document.getElementById('scale').innerHTML = "Scale = 1 : " + scale;
            addExtent();
          });
                  
             map.on('singleclick', function(evt) {
                
                     var cfeat =displayFeatureInfo(evt.pixel);
                     console.log(cfeat);
                     if(cfeat != undefined)
                     {
                     var coordinate = evt.coordinate;
                     var hdms = ol.coordinate.toStringHDMS(ol.proj.transform(
                     coordinate, 'EPSG:20137', 'EPSG:4326'));
                     content.innerHTML = '<p>Owenr: '+cfeat.values_.owner+'</p>\n\
                                          <p>Region: '+cfeat.values_.region+'</p>\n\
                                          <p>Area(hec): '+cfeat.values_.area+'</p><code>' + hdms +
                                         '</code>';
                                          overlay.setPosition(coordinate);
                    
                 }                     
                    });
                    var displayFeatureInfo = function(pixel) {
                   var feature = map.forEachFeatureAtPixel(pixel, function(feature) {
                    return feature;
                });
                return feature;

            };
            

                function addExtent(){
                // Remove the previous polygon
                extentSource.clear();
                // Get the extent of the map and store it in an array of coordinates 
                var extentMap = map.getView().calculateExtent(map.getSize());
                var bottomLeft = ol.proj.transform(ol.extent.getBottomLeft(extentMap),
                  'EPSG:20137', 'EPSG:4326');
                var topRight = ol.proj.transform(ol.extent.getTopRight(extentMap),
                  'EPSG:20137', 'EPSG:4326');
                var bottomRight = ol.proj.transform(ol.extent.getBottomRight(extentMap),
                  'EPSG:20137', 'EPSG:4326');
                var topLeft  = ol.proj.transform(ol.extent.getTopLeft(extentMap),
                  'EPSG:20137', 'EPSG:4326');
                var ring = [ 
                    [bottomLeft[0], bottomLeft[1]], 
                    [topLeft[0], topLeft[1]] , 
                    [topRight[0], topRight[1]],
                    [bottomRight[0], bottomRight[1]]
                ];
               // Create a polygon based on the array of coordinates
               var polygon = new ol.geom.Polygon([ring]);
               polygon.transform('EPSG:4326', 'EPSG:20137');
               // Add the polygon to the layer and style it
               var feature = new ol.Feature(polygon);
               extentSource.addFeature(feature);
               feature.setStyle(extentStyle);
            }
             var feat = [];
             var extent;
             var highlight;
               vectorSource.on('change', function(evt){
                var source=evt.target;
                if(source.getState() === 'ready'){
                       for(var i = 0; i < vectorSource.getFeatures().length; i++)
                       {
                                    feat.push(vectorSource.getFeatures()[i]);
                       }
                }
            });
            
            
            function highLight(name,zoom){
                 
          
            setTimeout(function(){
               
                var feature;
               //console.log(feat);
               for(var i = 0; i < feat.length; i++){
                   //console.log(feat[i].values_);
                    if(feat[i].values_.owner === name){
                        feature = feat[i];
                                    if (feature !== highlight) {
                                    if (highlight) {
                                      featureOverlay.getSource().removeFeature(highlight);
                                    }
                                    if (feature) {
                                        
                                      featureOverlay.getSource().addFeature(feature);
                                    }
                                    highlight = feature;
                                  }
                                  
                                  extent=feature.getGeometry().getExtent();
                                  if(zoom === true)
                                  {
                                            var center=ol.extent.getCenter(extent);
                                            map.setView( new ol.View({
                                                projection: 'EPSG:20137',
                                                center: [center[0] , center[1]],//zoom to the center of your feature
                                                zoom: 9 
                                            }));
                                            addExtent();
                                  }
                                  else
                                      return extent;
                          

               }
               }
            }, 200);
          
                        }
                        
                        
                   $('#tclick').on('click', 'tr', function() {
                   

                    
            }); 
            
            $('a[href ="#view"]').on('click',function(event){
                    var currentRow = $(this).closest("tr"); 
                    var comapany_name=currentRow.find("td:eq(1)").html();
                    highLight(comapany_name,true);
                    
            });
            
            $('a[href ="#view"]').hover(function() {
                $( this )
                  .toggleClass( "view" );
             });
            

            $('a[href ="#details"]').on('click',function(event){
                    var currentRow = $(this).closest("tr"); 
                    var comapany_name=currentRow.find("td:eq(1)").html();
                    highLight(comapany_name,false);
                    
                        setTimeout(function(){
                                              $.ajax({
                                                url: '/land_profile_data.jsp',
                                                data: {'extent':extent,'cname':comapany_name},
                                                dataType: 'xml',
                                                complete : function(){
                                                window.location.href = this.url;


                                                },
                                               success: function(xml){
                                                   console.log(xml);
                                                }
                                            });
                            
                            
                                }, 200);
                    
      
                    
            });
            
             $('a[href ="#details"]').hover(function() {
                $( this )
                  .toggleClass( "details" );
             });
             

       var landBankData; 
       var transferedData;
       var unidentifiedData;
       var ID;
    $('#in_land_bank').on('click', function(event) { 
  
        if($('#in_land_bank').is(":checked")){
                map.addLayer(vec1);
                $("#nodelist1").html(landBankData);
           }
           
        else {
             if(landBankData === undefined){
                landBankData = $("#nodelist1").html();
            }
           
           
           //console.log(tdata);
           var layers = map.getLayers().array_;
               for(var i = 0; i < layers.length; i ++)
               {
                   if(layers[i] === vec1)
                      map.removeLayer(layers[i]);
               }
               $("#nodelist1").html('');
            //
            

         }
    });
    
    $('#transfered').on('click', function(event) {
        
           if($('#transfered').is(":checked")){
               
                           map.addLayer(vec);
                        $("#nodelist").html(transferedData);
                       row1 = [];
                            $('#tclick tr').each(function(){
                           row1.push($(this));
                            });
                   $('#nextt').on('click',function(evetn){
                       next(row1);
                      });

                   $('#prevt').on('click',function(event){
                       previous(row1);
                   });
               
      
           }
        else
        {
                   if(transferedData === undefined){
                transferedData = $("#nodelist").html();
                console.log(transferedData);
            }
           var layers = map.getLayers().array_;
               for(var i = 0; i < layers.length; i ++)
               {
                   if(layers[i] === vec)
                      map.removeLayer(layers[i]);
               }
               
               $("#nodelist").html("");

        }
        
        
    });
    
    $('#unidentified').on('change', function(event) {
        if($('#unidentified').is(":checked")){
             map.addLayer(vec2);
            $("#nodelist2").html(unidentifiedData);

           }
        else{
               if(unidentifiedData === undefined){
                unidentifiedData = $("#nodelist2").html();
            }
            
           var layers = map.getLayers().array_;
               for(var i = 0; i < layers.length; i ++)
               {
                   if(layers[i] === vec2)
                      map.removeLayer(layers[i]);
               }
               $("#nodelist2").html("");
        }

    });
    function loadMap(){
                      map = new ol.Map({
                    controls: ol.control.defaults({
                      attribution: false
                    }).extend([mousePositionControl]),
                    overlays: [overlay],
                    target: 'map_preview',
                    layers: [
                      untiled,vec,vec1,vec2
                    ],
                    view: new ol.View({
                       projection: projection
                    })
                  });
            map.getView().fit(bounds, map.getSize());
    }
    
var tcount = 0;
var rows = [];
var row1 = [];
var last;

     $(document).on('ready', function(event) {
    $('#tclick tr').each(function(){
   // $(this).find('td').each(function(){
   rows.push($(this));
    });
    
    rows[0].show();
 for(var i = 1; i < rows.length; i++){
     if(i > 5){
         rows[i].hide();
     }
      else
   {
       rows[i].show();
        tcount++;
   }
 }
});
$('#nextt').on('click', function(event) {
   next(rows);
});


function next(rows){
    if(tcount >= rows.length-1)
    {
        return;
    }
    var size;
    var count = 0;
    if(rows.length-1 - tcount >= 5)
        size = tcount+5;
    else{
        size = rows.length-1;
        last = rows.length-1 - tcount;
    }
    for(var i = 1; i < rows.length; i++){
        if(i > tcount && i <= size){
            rows[i].show();
            count++;
        }
        else
        {
            rows[i].hide();
        }
        
    }
    rows[0].show();
tcount += count;
    
}

$('#prevt').on('click', function(event) {
    previous(rows);
    
});

function previous(rows){
    if(tcount <= 5){
        return;
    }
    var ul;
    var ll;
    var count = 0;
    var ls = false;
    if(tcount >= rows.length -1){
        ul = rows.length-1 -last;
        ll = ul-5;
        ls = true;
    }
    else{
        ul = tcount-5;
        ll = ul - 5;
    }
    for(var i = 1; i < rows.length; i ++){
        if(i > ll && i <= ul){
            rows[i].show();
            count++;
            
        }
        else
            rows[i].hide();
    }
    if(ls){
        tcount -=  --count;
    }
    else
    {
        tcount -= count; 
    }
}


    

