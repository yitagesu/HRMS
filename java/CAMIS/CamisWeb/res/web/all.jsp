<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>


                <div class="col-md-3 left_col menu_fixed">
                    <div class="left_col scroll-view">
                        <div class="navbar nav_title" style="border: 0;">
                            <a href="#" class="site_title"><i class="fa fa-map-marker"></i> <span>CAMiS</span></a>
                        </div>

                        <div id="sidebar-menu" class="main_menu_side main_menu">
                            <div class="menu_section">
                                <h3 style="visibility:hidden;">general</h3>
                                <ul class="nav side-menu">
                                   <li class="active"><a href="/dashboard2.jsp"  ><i class="fa fa-table"></i> Dashboard </a>
                                    </li>
                                    <li><a href="/land_bank.jsp" ><i class="fa fa-square"></i>Land bank</a>
                                    </li>
                                    <li><a href="/investorsmain.jsp" ><i class="fa fa-user"></i> Investor File</a>
                                    </li>
                                    <li><a href="/project_man.jsp"><i class="fa fa-product-hunt"></i> Project Management</a>
                                    </li>
                                     <li ><a href="/report.jsp"><i class="fa fa-files-o"></i> Reports</a>
                                    </li>
                                    <li><a href="/user.jsp" ><i class="fa fa-users"></i> User Management</a>
                                    </li>
                                    
                                   
                                    <li><a><i class="fa fa-gears"></i> Setting</a>
                                    </li>
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="top_nav" >
                    <div class="nav_menu">
                        <nav>
                            <div class="nav toggle">
                                <a id=""><i class="fa fa-map-marker"></i> <span>CAMiS</span></a>
                                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                            </div>
                            <ul class="nav navbar-nav navbar-right">
                                <li class="">
                                    <a href="" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        Welcome yite
                                        <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                                        <li><a href="#"> Change Password</a></li>
                                        <li><a href="#">Help</a></li>
                                        <li><a href="/login.jsp"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                                    </ul>
                                </li>
                            </ul>
                            <ul class="nav navbar-nav navbar-right">
                                <li class="">
                                    <a href="" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        Language
                                        <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                                        <li><a href="#">English</a></li>
                                        <li><a href="#">Amharic</a></li>
                                        <li><a href="#">Oromiffa</a></li>
                                        <li><a href="#">Tigrigna</a></li>
                                    </ul>
                                </li>
                            </ul>

                        </nav>
                    </div>
                </div>
