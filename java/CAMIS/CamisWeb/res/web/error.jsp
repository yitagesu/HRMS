<%-- 
    Document   : error.jsp
    Created on : Nov 9, 2017, 4:30:56 PM
    Author     : Tewelde
--%>



<!DOCTYPE html>

    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
            <title>CAMIS Error</title>
            <link rel="stylesheet" href="/assets/css/external.css"/>
            <link rel="stylesheet" href="/assets/css/bootstrap.min.css"/>
        </head>
        
        <body>
            <div class="error">
                <div class="align-center">
                <i class="fa fa-warning faa-flash animated warning icon"></i>
                <div class="row" style="display: flex">
                    <div class="col-md-4 col-sm-4 col-xs-4">
                        <div class="refresh align-center">
                            <i class="fa fa-refresh"></i>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4">
                        <div class="message">
                            <h1 class="">Error</h1>
                            <p>There seems to be a little problem</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4">
                        <div class="cancel align-center">
                            <i class="fa fa-close"></i>
                        </div>
                    </div>
                </div>
                
                </div>
            </div>
            
        </body>
    </html>
