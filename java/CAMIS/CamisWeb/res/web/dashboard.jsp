<%-- 
    Document   : login.jsp
    Created on : Nov 9, 2017, 4:30:56 PM
    Author     : Yitagesu
--%>



<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>CAMIS</title>
        <link rel="stylesheet" href="/assets/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="/assets/css/external.css"/>
        
        <link rel="stylesheet" href="/assets/css/custom.min.css"/>
        

    </head>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <div class="col-md-3 left_col menu_fixed">
                    <div class="left_col scroll-view">
                        <div class="navbar nav_title" style="border: 0;">
                            <a href="#" class="site_title"><i class="fa fa-map-marker"></i> <span>CAMiS</span></a>
                        </div>

                        <div id="sidebar-menu" class="main_menu_side main_menu">
                            <div class="menu_section">
                                <h3 style="visibility:hidden;">general</h3>
                                <ul class="nav side-menu">
                                    <li class="active"><a href=".dashboard" data-toggle="tab" ><i class="fa fa-table"></i> Dashboard </a>
                                    </li>
                                    <li><a href="#user" data-toggle="tab" ><i class="fa fa-user"></i> User Management</a>
                                    </li>
                                    <li><a><i class="fa fa-gears"></i> Setting</a>
                                    </li>
                                    <li><a><i class="fa fa-gear"></i> Other</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="top_nav">
                    <div class="nav_menu">
                        <nav>
                            <div class="nav toggle">
                                <a id=""><i class="fa fa-map-marker"></i> <span>CAMiS</span></a>
                                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                            </div>
                            

                            <ul class="nav navbar-nav navbar-right">
                                <li class="">
                                    <a href="" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        Welcome yite
                                        <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                                        <li><a href="#"> Change Password</a></li>
                                        <li><a href="#">Help</a></li>
                                        <li><a href="/login.jsp"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="right_col tab-content" role="main">
                    <div id="dashboard" class="tab-pane active in dashboard">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="dashboard_graph">

                                    <div class="row x_title">
                                        <div class="col-md-6">
                                            <h3>Network Activities</h3>
                                        </div>
                                    </div>

                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <div id="chart_plot_01" class="demo-placeholder"></div>
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-12 bg-white">
                                        <div class="x_title">
                                            <h2>Top Network Activities</h2>
                                            <div class="clearfix"></div>
                                        </div>

                                        <div class="col-md-12 col-sm-12 col-xs-6">
                                            <div>
                                                <p>Pyschian</p>
                                                <div class="">
                                                    <div class="progress progress_sm" style="width: 76%;">
                                                        <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="80"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <p>Laboratory</p>
                                                <div class="">
                                                    <div class="progress progress_sm" style="width: 76%;">
                                                        <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="60"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-6">
                                            <div>
                                                <p>Farmacy</p>
                                                <div class="">
                                                    <div class="progress progress_sm" style="width: 76%;">
                                                        <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="40"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <p>Reception</p>
                                                <div class="">
                                                    <div class="progress progress_sm" style="width: 76%;">
                                                        <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="50"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Recent Activities</h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>
                                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <div class="dashboard-widget-content">

                                            <ul class="list-unstyled timeline widget">
                                                <li>
                                                    <div class="block">
                                                        <div class="block_content">
                                                            <h2 class="title">
                                                                <a>Change his password</a>
                                                            </h2>
                                                            <div class="byline">
                                                                <span>13 hours ago</span> by <a>Yitagesu Kebede</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="block">
                                                        <div class="block_content">
                                                            <h2 class="title">
                                                                <a>Change his password</a>
                                                            </h2>
                                                            <div class="byline">
                                                                <span>13 hours ago</span> by <a>Yitagesu Kebede</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="block">
                                                        <div class="block_content">
                                                            <h2 class="title">
                                                                <a>Change his password</a>
                                                            </h2>
                                                            <div class="byline">
                                                                <span>13 hours ago</span> by <a>Yitagesu Kebede</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="block">
                                                        <div class="block_content">
                                                            <h2 class="title">
                                                                <a>Change his password</a>
                                                            </h2>
                                                            <div class="byline">
                                                                <span>13 hours ago</span> by <a>Yitagesu Kebede</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="user" class="tab-pane">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>User Administration</h2>
                                        <div class="title_right">
                                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" placeholder="Search for...">
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-default" type="button">Go!</button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>

                                    <div class="x_content">
                                        <div class="table-responsive">
                                            <table class="table table-striped jambo_table bulk_action">
                                                <thead>
                                                    <tr class="headings">
                                                        <th>
                                                            <input type="checkbox" id="check-all" class="flat">
                                                        </th>
                                                        <th class="column-title">Name </th>
                                                        <th class="column-title">Role </th>
                                                        <th class="column-title">Phone </th>
                                                        <th class="column-title">Active On</th>
                                                        <th class="column-title">Last Login</th>
                                                        <th class="column-title">Status </th>
                                                        <th class="column-title"><span class="nobr">Action</span>
                                                        </th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    <tr class="even pointer">
                                                        <td class="a-center ">
                                                            <input type="checkbox" class="flat" name="table_records">
                                                        </td>
                                                        <td class=" ">yitagesu Kebede</td>
                                                        <td class=" ">Doctor </td>
                                                        <td class=" ">+251910616253</td>
                                                        <td class=" ">May 23, 2014 11:30:12 PM</td>
                                                        <td class=" ">May 23, 2014 11:30:12 PM</td>
                                                        <td class="a-right a-right ">Active</td>
                                                        <td class=" last"><div class='btn-group' style='display: flex;'>
                                                                <a href = '#' class='btn btn-success btn-xs editButton' data-toggle='tooltip' title='Edit'><span class='fa fa-edit'> </span></a>
                                                                <a href = '#' class='btn btn-info btn-xs' data-toggle='tooltip' title='View'><span class='fa fa-eye'> </span></a>
                                                                <a href = '#' class='btn btn-warning btn-xs' data-toggle='tooltip' title='Delete'><span class='fa fa-trash-o'> </span></a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr class="even pointer">
                                                        <td class="a-center ">
                                                            <input type="checkbox" class="flat" name="table_records">
                                                        </td>
                                                        <td class=" ">yitagesu Kebede</td>
                                                        <td class=" ">Doctor </td>
                                                        <td class=" ">+251910616253</td>
                                                        <td class=" ">May 23, 2014 11:30:12 PM</td>
                                                        <td class=" ">May 23, 2014 11:30:12 PM</td>
                                                        <td class="a-right a-right ">Active</td>
                                                        <td class=" last"><div class='btn-group' style='display: flex;'>
                                                                <a href = '#' class='btn btn-success btn-xs editButton' data-toggle='tooltip' title='Edit'><span class='fa fa-edit'> </span></a>
                                                                <a href = '#' class='btn btn-info btn-xs' data-toggle='tooltip' title='View'><span class='fa fa-eye'> </span></a>
                                                                <a href = '#' class='btn btn-warning btn-xs' data-toggle='tooltip' title='Delete'><span class='fa fa-trash-o'> </span></a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr class="even pointer">
                                                        <td class="a-center ">
                                                            <input type="checkbox" class="flat" name="table_records">
                                                        </td>
                                                        <td class=" ">yitagesu Kebede</td>
                                                        <td class=" ">Doctor </td>
                                                        <td class=" ">+251910616253</td>
                                                        <td class=" ">May 23, 2014 11:30:12 PM</td>
                                                        <td class=" ">May 23, 2014 11:30:12 PM</td>
                                                        <td class="a-right a-right ">Active</td>
                                                        <td class=" last"><div class='btn-group' style='display: flex;'>
                                                                <a href = '#' class='btn btn-success btn-xs editButton' data-toggle='tooltip' title='Edit'><span class='fa fa-edit'> </span></a>
                                                                <a href = '#' class='btn btn-info btn-xs' data-toggle='tooltip' title='View'><span class='fa fa-eye'> </span></a>
                                                                <a href = '#' class='btn btn-warning btn-xs' data-toggle='tooltip' title='Delete'><span class='fa fa-trash-o'> </span></a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr class="even pointer">
                                                        <td class="a-center ">
                                                            <input type="checkbox" class="flat" name="table_records">
                                                        </td>
                                                        <td class=" ">yitagesu Kebede</td>
                                                        <td class=" ">Doctor </td>
                                                        <td class=" ">+251910616253</td>
                                                        <td class=" ">May 23, 2014 11:30:12 PM</td>
                                                        <td class=" ">May 23, 2014 11:30:12 PM</td>
                                                        <td class="a-right a-right ">Active</td>
                                                        <td class=" last"><div class='btn-group' style='display: flex;'>
                                                                <a href = '#' class='btn btn-success btn-xs editButton' data-toggle='tooltip' title='Edit'><span class='fa fa-edit'> </span></a>
                                                                <a href = '#' class='btn btn-info btn-xs' data-toggle='tooltip' title='View'><span class='fa fa-eye'> </span></a>
                                                                <a href = '#' class='btn btn-warning btn-xs' data-toggle='tooltip' title='Delete'><span class='fa fa-trash-o'> </span></a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr class="even pointer">
                                                        <td class="a-center ">
                                                            <input type="checkbox" class="flat" name="table_records">
                                                        </td>
                                                        <td class=" ">yitagesu Kebede</td>
                                                        <td class=" ">Doctor </td>
                                                        <td class=" ">+251910616253</td>
                                                        <td class=" ">May 23, 2014 11:30:12 PM</td>
                                                        <td class=" ">May 23, 2014 11:30:12 PM</td>
                                                        <td class="a-right a-right ">Active</td>
                                                        <td class=" last"><div class='btn-group' style='display: flex;'>
                                                                <a href = '#' class='btn btn-success btn-xs editButton' data-toggle='tooltip' title='Edit'><span class='fa fa-edit'> </span></a>
                                                                <a href = '#' class='btn btn-info btn-xs' data-toggle='tooltip' title='View'><span class='fa fa-eye'> </span></a>
                                                                <a href = '#' class='btn btn-warning btn-xs' data-toggle='tooltip' title='Delete'><span class='fa fa-trash-o'> </span></a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr class="even pointer">
                                                        <td class="a-center ">
                                                            <input type="checkbox" class="flat" name="table_records">
                                                        </td>
                                                        <td class=" ">yitagesu Kebede</td>
                                                        <td class=" ">Doctor </td>
                                                        <td class=" ">+251910616253</td>
                                                        <td class=" ">May 23, 2014 11:30:12 PM</td>
                                                        <td class=" ">May 23, 2014 11:30:12 PM</td>
                                                        <td class="a-right a-right ">Active</td>
                                                        <td class=" last"><div class='btn-group' style='display: flex;'>
                                                                <a href = '#' class='btn btn-success btn-xs editButton' data-toggle='tooltip' title='Edit'><span class='fa fa-edit'> </span></a>
                                                                <a href = '#' class='btn btn-info btn-xs' data-toggle='tooltip' title='View'><span class='fa fa-eye'> </span></a>
                                                                <a href = '#' class='btn btn-warning btn-xs' data-toggle='tooltip' title='Delete'><span class='fa fa-trash-o'> </span></a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script src="/assets/js/jquery.min.js"></script>
        <script src="/assets/js/bootstrap.min.js"></script>
        <script src="/assets/js/external.js"></script>
        <script src="/assets/js/Char.min.js"></script>
        <script src="/assets/js/bootstrap-progressbar.min.js"></script>
        <script src="/assets/js/icheck.min.js"></script>
        <script src="/assets/js/custom.min.js"></script>
</html>
