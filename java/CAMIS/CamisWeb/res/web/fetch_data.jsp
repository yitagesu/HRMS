<%-- 
    Document   : fetch_data
    Created on : Dec 22, 2017, 2:50:27 PM
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.intaps.camisweb.*"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>CAMIS</title>
        <link rel="stylesheet" href="assets/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="assets/css/external.css"/>

        <link rel="stylesheet" href="assets/css/custom.min.css"/>
        
        <style>
          
            #second{
                position: relative;
                left:300px;
                top:-378px;
            }
            
            
        </style>
    </head>
        <body class="nav-sm">
            <div class="container body">
            <div class="main_container">
                 <%@include file="all.jsp" %>
                 
                    
                <div class="right_col tab-content" role="main">
                    <div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <button id="return"><-</button>
                                    <%
                                    String cname = request.getParameter("cname");
                                    
                                    
                                    %>
                                    <span><%=cname%> Details</span>
                                    <br><br>
                                            
                                                
                                                 <%
                                                 
                                                 LandInfo.AuthenticationForm rdata = Land.getAuthenticationForm(cname);
                                                 for(LandInfo.AuthenticationForm.Form fdata: rdata.data)
                                                 {
                                         
                                                 %>
                                                 <table id ="first">
                                                 <tr><td><strong>Company Name</strong><td></tr>
                                                 <tr><td><%=fdata.companyName%></td></tr>
                                                 <tr><td><strong>Kebele</strong><td></tr>
                                                 <tr><td><%=fdata.kebele%></td></tr>
                                                  <tr><td><strong>Woreda</strong><td></tr>
                                                 <tr><td><%=fdata.woreda%></td></tr>
                                                  <tr><td><strong>Zone</strong><td></tr>
                                                 <tr><td><%=fdata.zone%></td></tr>
                                                  <tr><td><strong>Fax</strong><td></tr>
                                                 <tr><td><%=fdata.fax%></td></tr>
                                                 <tr><td><strong>Phone</strong><td></tr>
                                                 <tr><td><%=fdata.phone%></td></tr>
                                                 <tr><td><strong>Email</strong><td></tr>
                                                 <tr><td><%=fdata.email%></td></tr>
                                                 <tr><td><strong>Region Where The Land Belongs</strong><td></tr>
                                                 <tr><td><%=fdata.landBelongs%></td></tr>
                                                 <tr><td><strong>Size of Land given Per Hectare</strong><td></tr>
                                                 <tr><td><%=fdata.landPerHec%></td></tr>
                                                 <tr><td><strong>Landlord / landers / Neighbours</strong><td></tr>
                                                 <tr><td><%=fdata.landNeigbours%></td></tr>
                                                 </table>
                                                 <table id = "second">
                                                  <tr><td><strong>East</strong><td></tr>
                                                  <tr><td><%=fdata.east%></td></tr>
                                                  <tr><td><strong>West</strong><td></tr>
                                                  <tr><td><%=fdata.west%></td></tr>
                                                  <tr><td><strong>North</strong><td></tr>
                                                  <tr><td><%=fdata.north%></td></tr>
                                                  <tr><td><strong>South</strong><td></tr>
                                                  <tr><td><%=fdata.south%></td></tr>
                                                  <tr><td><strong>Land Lease Rate</strong><td></tr>
                                                  <tr><td><%=fdata.landLeaseRate%></td></tr>
                                                  <tr><td><strong>Lease Contract Detail</strong><td></tr>
                                                  <tr><td><%=fdata.leaseContractDetail%></td></tr>
                                                 </table>
                                                 <%
                                                 
                                                 }
     
                                                 %>

                                         
                                    
                                    
                     
                        </div>
                    </div>
                </div>
            </div>

    
    <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/external.js"></script>
        <script src="assets/js/Char.min.js"></script>
        <script src="assets/js/bootstrap-progressbar.min.js"></script>
        <script src="assets/js/icheck.min.js"></script>
        <script src="assets/js/custom.min.js"></script>
        <script>
            
          $('#return').on('click',  function() {
                history.go(-1);
            });  
            
        </script>
</body>
</html>
