<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.intaps.camisweb.*"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>CAMIS</title>
        <link rel="stylesheet" href="assets/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="assets/css/external.css"/>

        <link rel="stylesheet" href="assets/css/custom.min.css"/>
        <style></style>
    </head>
        <body class="nav-sm">
            <div class="container body">
            <div class="main_container">

                <%@include file="all.jsp" %>
                <div class="right_col tab-content" role="main">
                    <div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>User Management</h2>
                                       
                                        <div class="clearfix"></div>
                                    </div>

                                  <div class="x_content">
                                     <div class="row">
                                        
                                            
                                         <div class="col-md-12 col-sm-12 col-xs-12">
                                          <div class="form-group" style="float:left">
                                              <button class="btn btn-success"  data-toggle='modal' data-target='#addusr' data-backdrop='static' > Add User</button>
                                            </div>
                                               
                                        
                                        <table class="table table-striped jambo_table bulk_action table-bordered">
                                            <thead>
                                            <th>User Name</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Phone Number</th>
                                            <th>Role</th>
                                            <th>Action</th>
                                            </thead>
                                            <tbody>
                                               
                                                <tr>
                                                    <td>Abe</td>
                                                    <td>Abebe</td>
                                                    <td>ab@mail.com</td>
                                                    <td>09123456789</td>
                                                    <td>Administrator</td>
                                                    
                                                    <td>
                                                    <button id="view"  data-toggle='modal' data-target='#viewIp' data-backdrop='static' ><i class="fa fa-pencil-square-o"></i></button>
                                                    <button id="delete" data-toggle='modal' data-target='#del' data-backdrop='static' ><i class="fa fa-trash" aria-hidden="true"></i></button> 
                                                    <button id="password"  data-toggle='modal' data-target='#passw' data-backdrop='static' ><i class="fa fa-unlock-alt" aria-hidden="true"></i></button> 
                                                    
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Kebe</td>
                                                    <td>Kebede</td>
                                                    <td>kk@mail.com</td>
                                                    <td>09123456789</td>
                                                    <td>Administrator</td>
                                                    <td>
                                                    <button id="view"  data-toggle='modal' data-target='#viewIp' data-backdrop='static' ><i class="fa fa-pencil-square-o"></i></button>
                                                    <button id="delete" data-toggle='modal' data-target='#del' data-backdrop='static' ><i class="fa fa-trash" aria-hidden="true"></i></button> 
                                                    <button id="password"  data-toggle='modal' data-target='#passw' data-backdrop='static' ><i class="fa fa-unlock-alt" aria-hidden="true"></i></button> 
                                                    
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Cha</td>
                                                    <td>Chaltu</td>
                                                    <td>cc@mail.com</td>
                                                    <td>09123456789</td>
                                                    <td>Administrator</td>
                                                    <td>
                                                    <button id="view"  data-toggle='modal' data-target='#viewIp' data-backdrop='static' ><i class="fa fa-pencil-square-o"></i></button>
                                                    <button id="delete" data-toggle='modal' data-target='#del' data-backdrop='static' ><i class="fa fa-trash" aria-hidden="true"></i></button> 
                                                    <button id="password"  data-toggle='modal' data-target='#passw' data-backdrop='static' ><i class="fa fa-unlock-alt" aria-hidden="true"></i></button> 
                                                    
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Ber</td>
                                                    <td>Berihun</td>
                                                    <td>bb@mail.com</td>
                                                    <td>09123456789</td>
                                                    <td>Administrator</td>
                                                    <td>
                                                    <button id="view"  data-toggle='modal' data-target='#viewIp' data-backdrop='static' ><i class="fa fa-pencil-square-o"></i></button>
                                                    <button id="delete" data-toggle='modal' data-target='#del' data-backdrop='static' ><i class="fa fa-trash" aria-hidden="true"></i></button> 
                                                    <button id="password"  data-toggle='modal' data-target='#passw' data-backdrop='static' ><i class="fa fa-unlock-alt" aria-hidden="true"></i></button> 
                                                    
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Zeru</td>
                                                    <td>Zerubabel</td>
                                                    <td>zz@mail.com</td>
                                                    <td>09123456789</td>
                                                    <td>Administrator</td>
                                                    <td>
                                                    <button id="view"  data-toggle='modal' data-target='#viewIp' data-backdrop='static' ><i class="fa fa-pencil-square-o"></i></button>
                                                    <button id="delete" data-toggle='modal' data-target='#del' data-backdrop='static' ><i class="fa fa-trash" aria-hidden="true"></i></button> 
                                                    <button id="password"  data-toggle='modal' data-target='#passw' data-backdrop='static' ><i class="fa fa-unlock-alt" aria-hidden="true"></i></button> 
                                                    
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Abrsh</td>
                                                    <td>Abraham</td>
                                                    <td>AA@mail.com</td>
                                                    <td>09123456789</td>
                                                    <td>Administrator</td>
                                                    <td>
                                                    <button id="view"  data-toggle='modal' data-target='#viewIp' data-backdrop='static' ><i class="fa fa-pencil-square-o"></i></button>
                                                    <button id="delete" data-toggle='modal' data-target='#del' data-backdrop='static' ><i class="fa fa-trash" aria-hidden="true"></i></button> 
                                                    <button id="password"  data-toggle='modal' data-target='#passw' data-backdrop='static' ><i class="fa fa-unlock-alt" aria-hidden="true"></i></button> 
                                                    
                                                    </td>
                                                </tr>
                                            
                                            </tbody>
                                            

                                        </table>
                                             <div id="view">
                                                 
                                             </div>
                                             
                                             <div id="password"> </div>
                                        </div>
                                        
                                   </div>

                                   
                                        </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                
                
            </div>
                
                <div class="modal fade " id="viewIp" tabindex="-1" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">

                                <div class="modal-header">
                                    <h2>Edit User Information</h2>
                                    
                                </div>            <!-- /modal-header -->
                               
                                    <div class="modal-body table-responsive">
                                        <form class="form-group">
                                            <div>
                                        <label>First Name</label>
                                        <input type="text" class="form-control">
                                            </div>
                                            <div>
                                        <label>Father's Name</label>
                                        <input type="text" class="form-control">
                                            </div><div>
                                        <label>Grandfather's Name</label>
                                        <input type="text" class="form-control">
                                            </div><div>
                                        <label>Phone Number</label>
                                        <input type="text" class="form-control">
                                            </div>
                                           <div>
                                        <label>Email</label>
                                        <input type="text" class="form-control">
                                            </div>
                                            <div>
                                        <label>Role</label>
                                         <div class="dropdown">
                                            <button class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown" >Roles
                                                <span class="caret"></span></button>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Role 1</a></li>
                                                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Role 2</a></li>
                                                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Role 3</a></li>
      
                                                    </ul>
                                             
                                         </div>
                                            </div>
                                            <div>
                                                <br>
                                        <label>Username</label>
                                        <input type="text" class="form-control">
                                            </div>
                                                <div>
                                        <label>Password</label>
                                        <input type="text" class="form-control">
                                            </div>
                                            <div>
                                        <label>Confirm Password</label>
                                        <input type="text" class="form-control">
                                            </div>
                                          <div style="margin-top: 10pt; float: right;">
                                            <button class="btn btn-danger">Cancel</button>
                                            <button class="btn btn-primary">Save</button>
                                        </div>  
                                        </form>
                                    </div>
                                    


                            </div>
                        </div>
                    </div>
                <div class="modal fade " id="passw" tabindex="-1" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">

                                <div class="modal-header">
                                    <h2>Change Password</h2>
                                    
                                    
                                </div>            <!-- /modal-header -->
                               
                                    <div class="modal-body table-responsive">
                                        <form> <div>
                                       <label>Password</label>
                                        <input type="text" class="form-control">
                                            </div>
                                            <div>
                                        <label>Confirm Password</label>
                                        <input type="text" class="form-control">
                                            </div>
                                          <div style="margin-top: 10pt; float: right;">
                                            <button class="btn btn-danger">Cancel</button>
                                            <button class="btn btn-primary">Save</button>
                                        </div>
                                    </form>
                                    </div>
                                    </div>
                                   


                            </div>
                        </div>
                <div class="modal fade " id="addusr" tabindex="-1" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">

                                <div class="modal-header">
                                    <h2>Add New User Information</h2>
                                    
                                </div>            <!-- /modal-header -->
                               
                                    <div class="modal-body table-responsive">
                                        <form class="form-group">
                                            <div>
                                        <label>First Name</label>
                                        <input type="text" class="form-control">
                                            </div>
                                            <div>
                                        <label>Father's Name</label>
                                        <input type="text" class="form-control">
                                            </div><div>
                                        <label>Grandfather's Name</label>
                                        <input type="text" class="form-control">
                                            </div><div>
                                        <label>Phone Number</label>
                                        <input type="text" class="form-control">
                                            </div>
                                            <div>
                                        <label>Email</label>
                                        <input type="text" class="form-control">
                                            </div>
                                            <div>
                                        <label>Role</label>
                                         <div class="dropdown">
                                            <button class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown" >Roles
                                                <span class="caret"></span></button>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Role 1</a></li>
                                                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Role 2</a></li>
                                                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Role 3</a></li>
      
                                                    </ul>
                                             
                                         </div>
                                            </div>
                                            <div >
                                                <br>
                                        <label>Username</label>
                                        <input type="text" class="form-control">
                                            </div>
                                                <div>
                                        <label>Password</label>
                                        <input type="text" class="form-control">
                                            </div>
                                            <div>
                                        <label>Confirm Password</label>
                                        <input type="text" class="form-control">
                                            </div>
                                          <div style="margin-top: 10pt; float: right;">
                                            <button class="btn btn-danger">Cancel</button>
                                            <button class="btn btn-primary">Save</button>
                                        </div>  
                                        </form>
                                    </div>
                                    


                            </div>
                        </div>
                    </div>
                <div class="modal fade " id="del" tabindex="-1" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">

                                <div class="modal-header">
                                    <h2>Are you sure you want to delete</h2>
                                    
                                    
                                </div>            <!-- /modal-header -->
                               
                                    <div class="modal-body table-responsive">
                                        <form> 
                                            
                                          <div style="margin-top: 10pt; float: right;">
                                            <button class="btn btn-danger">Cancel</button>
                                            <button class="btn btn-primary">Ok</button>
                                        </div>
                                    </form>
                                    </div>
                                    </div>
                                   


                            </div>
                        </div>
                    </div>

    </body>
    
        <script src="assets/js/jquery.js"></script>
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/external.js"></script>
        <script src="assets/js/Chart.min.js"></script>
        <script src="assets/js/bootstrap-progressbar.min.js"></script>
        <script src="assets/js/icheck.min.js"></script>
        <script src="assets/js/custom.min.js"></script>
</html>
