<%-- 
    Document   : fetch_data
    Created on : Dec 22, 2017, 2:50:27 PM
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.intaps.camisweb.*"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>CAMIS</title>
        <link rel="stylesheet" href="assets/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="assets/css/external.css"/>

        <link rel="stylesheet" href="assets/css/custom.min.css"/>
        <link rel="stylesheet" href="/assets/css/ol.css"/>
        <style>
            #second{
                position: relative;
                left:227px;
                top:-570px;
            }
            

        </style>
              
    </head>
  
        <body class="nav-sm">
            <div class="container body">
            <div class="main_container">
                 <%@include file="all.jsp" %>
                <div class="right_col tab-content" role="main">
                    <div>
                        <div class="row">
                            
                            <div class="col-md-12 col-sm-6 col-xs-12">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="x_panel">
                                    
                                      <div class="form-group align-left">
                                                <button id="return" class="btn btn-success "> Back</button>
                                                <button id="return" class="btn btn-success ">Update</button>
                                                <button id="return" class="btn btn-success "> Edit</button>
                                                <%
                                    String cname = request.getParameter("cname");
                                 
                                                String [] put = new String[4];
                                                String[] myJsonData = request.getParameterValues("extent[]");
                                                for (int i=0; i < myJsonData.length; ++i) {
                                                   put[i] = myJsonData[i] ;
                                                }
                                                out.println(cname);
                                                
                                                %>
                                    
                                    
                                    
                                   
                                    <label id ="mvalue"><%=cname%></label>
                                    <label id ="ext"><%=put[0]%>,<%=put[1]%>,<%=put[2]%>,<%=put[3]%></label>
                                               
                                            </div>
                                    
                                        <table class="table">
                                            <thead>
                                            <th></th>
                                            <th></th>
                                            </thead>
                                            <tbody>
                                                <%
                                                LandInfo.LandBankProfile retLandPro = Land.getLandBankProfileData();
                                                for(LandInfo.LandBankProfile.LandBankProfileData val: retLandPro.data){
                                                %>
                                                <tr class="bg-primary"><td>Parcel ID/Block ID/Farm ID (UPID)</td><td></td></tr>
                                                <tr ><td ><strong>Parcel Id</strong></td>
                                                <td ><%=val.parcelId%></td>
                                                </tr>
                                                 <tr class="bg-primary"><td>Address</td><td></td></tr>
                                                <tr><td><strong>Kebele</strong></td>
                                                <td ><%=val.kebele%></td>
                                                    </tr>
                                                <tr><td><strong>Woreda</strong></td>
                                                <td ><%=val.woreda%></td></tr>
                                                
                                                <tr><td><strong>Zone</strong></td>
                                                <td ><%=val.zone%></td></tr>
                                                
                                                <tr><td><strong>Region</strong></td>
                                                <td><%=val.region%></td></tr>
                                                
                                                <tr><td><strong>City</strong></td>
                                                <td><%=val.city%></td></tr>
                                                
                                                <tr><td><strong>Address</strong></td>
                                               	<td><%=val.address%></td></tr>
                                                 <tr><td><strong>Distance from point of center(region capital)</strong></td>
                                                     <td><%=val.distance%></td></tr>
                                                 <tr><td><strong>Centroid</strong></td>
                                                 <td><%=val.centroid%></td></tr>
                                                 <tr><td><strong>X Coordinate</strong></td>
                                                 <td><%=val.xCordinate%></td></tr>
                                                 <tr><td><strong>Y Coordinate</strong></td>
                                                   <td><%=val.yCordinate%></td></tr>
                                                
                                                 <tr class="bg-primary"><td>Neighbor</td><td></td></tr>
                                                  <tr><td><strong>From North---</strong></td>
                                                    <td><%=val.neighrbourFromNorth%></td></tr>
                                                   <tr><td><strong>From South--</strong></td>
                                                    <td><%=val.neighrbourFromSouth%></td></tr>
                                                    <tr><td><strong>From East--</strong></td>
                                                    <td><%=val.neighrbourFromWest%></td></tr>
                                                     <tr><td><strong>From West--</strong></td>
                                                    <td><%=val.neighrbourFromEast%></td></tr>
                                                  
                                                     <tr class="bg-primary"><td>Accessibility</td><td></td></tr>
                                                    <tr><td><strong>Air</strong></td>
                                                    <td><%=val.accessibilityAir%></td></tr>
                                                     <tr><td><strong>Road</strong></td>
                                                    <td><%=val.accessibilityRoad%></td></tr>
                                                    <tr><td><strong>Rail</strong></td>
                                                    <td><%=val.accessibilityRail%></td></tr>
                                                     <tr><td><strong>Water</strong></td>
                                                    <td><%=val.accessibilityWater%></td></tr>
                                                 
                                                  <tr class="bg-primary"><td>Area</td><td></td></tr>
                                                   <tr><td><strong>Area(hectare)</strong></td>
                                                    <td><%=val.area%></td></tr>
                                                     <tr><td><strong>condition of the expansion spots</strong></td>
                                                    <td><%=val.conditionExpansion%></td></tr>
                                                    
                                                    <tr class="bg-primary"><td>Status of infrastructure and services</td><td></td></tr>
                                                   <tr><td><strong>Road-Asphalt and Electric Line</strong></td>
                                                    <td><%=val.statusInfraRoadElec%></td></tr>
                                                     <tr><td><strong>Telecommunication Service</strong></td>
                                                    <td><%=val.statusInfraTele%></td></tr>
                                                      <tr><td><strong>Health</strong></td>
                                                    <td><%=val.statusInfraHealth%></td></tr>
                                                       <tr><td><strong>education</strong></td>
                                                    <td><%=val.statusInfraEducation%></td></tr>
                                                       <tr><td><strong>Closest administration office</strong></td>
                                                    <td><%=val.statusInfraAdmin%></td></tr>
                                                    
                                                  <%
                                                        }
                                                        %>
                                            </tbody>
                                        </table>
                                                        
                          
                        </div>
                                                        
                    </div>
                                              
                       <div class="col-md-6 col-sm-18 col-xs-18">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Thematic</h2>
                                       
                                        
                                    </div>

                                    <div class="x_content">
                                        <div class="map has-scroll" id="map">
                                            
                                        </div>
                                    </div
                                    
                                </div>
                           
                                           <table class="table">
                                            <thead>
                                            <th></th>
                                            <th></th>
                                            </thead>
                                            <tbody>
                                                <%
                                                LandInfo.LandBankProfile retLandPro1 = Land.getLandBankProfileData();
                                                for(LandInfo.LandBankProfile.LandBankProfileData val: retLandPro1.data){
                                                %>
                                            
                                                <tr><td></td><td></td></tr>
                                                 <tr class="bg-primary"><td>Land Use</td><td></td></tr>
                                                     <tr><td><strong>Land use</strong></td>
                                                    <td><%=val.landUse%></td></tr>
                                                    <tr><td><strong>Investment Type</strong></td>
                                                    <td><%=val.investmentType%></td></tr>
                                                    
                                                     <tr class="bg-primary"><td>Soil Status</td><td></td></tr>
                                                    <tr><td><strong>Soil fertility test result</strong></td>
                                                    <td>testResult.docx</td></tr>
                                                   
                                                  <tr class="bg-primary"><td>Agro-Ecology</td><td></td></tr>
                                                    <tr><td><strong>Socio-economic study</strong></td>
                                                    <td><%=val.socioEconomy%></td></tr>
                                                    <tr><td><strong>Agero-ecology</strong></td>
                                                    <td><%=val.ageroEcology%></td></tr>
                                                    
                                                     <tr class="bg-primary"><td>Lease Information</td><td></td></tr>
                                                      <tr><td><strong>Lease Type</strong></td>
                                                    <td><%=val.leaseType%></td></tr>
                                                      <tr><td><strong>Lease Duration</strong></td>
                                                    <td><%=val.leaseDuration%></td></tr>
                                                      <tr><td><strong>Lease amount per hectare</strong></td>
                                                    <td><%=val.leasePerHectare%></td></tr>
                                                     
                                                     <tr class="bg-primary"><td>Weather Condition and Others</td><td></td></tr>
                                                     <tr><td><strong>Surface and underground water potential</strong></td>
                                                    <td><%=val.surUnderWaterPote%></td></tr>
                                                     
                                                     <tr><td><strong>Water potential for irrigation services</strong></td>
                                                    <td><%=val.waterPote%></td></tr>
                                                      <tr><td><strong>Wind directions and speed</strong></td>
                                                    <td><%=val.windDirecSpeed%></td></tr>
                                                     <tr><td><strong>Rainfall distribution</strong></td>
                                                    <td><%=val.rainfallDist%></td></tr>
                                                     
                                                     <tr><td><strong>Atmospheric moisture content(Humidity)</strong></td>
                                                    <td><%=val.humudity%></td></tr>
                                                     
                                                      <tr><td><strong>The height above the sea level</strong></td>
                                                    <td><%=val.height%></td></tr>
                                                    
                                                     <tr><td><strong>Cover of herbs</strong></td>
                                                    <td><%=val.coverOfHerbs%></td></tr>
                                                     <tr><td><strong>Landscape</strong></td>
                                                    <td><%=val.landscape%></td></tr>
                                                     <tr><td><strong>Others</strong></td>
                                                    <td><%=val.others%></td></tr>
                                                <%}%>
                                            </tbody>
                                            </table>
                            </div>
                            
                                            
                        </div>                                  
                
                    
                    
                    
                    
                    </div>
                                            </div>
            </div>
             </div>                                  
                </div>
            </div>

    <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/external.js"></script>
        <script src="assets/js/Chart.min.js"></script>
        <script src="assets/js/bootstrap-progressbar.min.js"></script>
        <script src="assets/js/icheck.min.js"></script>
        <script src="assets/js/custom.min.js"></script>
        <script src="/assets/js/ol-debug.js"></script>
        <script>
      var mv = $('#mvalue').html();
  var bounds = [];
    var b =  $('#ext').html().split(',');
     for(var i = 0; i < b.length; i++){
          bounds.push(parseInt(b[i]));
     }
    
      console.log(mv+"one");
      var filter = "owner = '"+mv+"'";
      console.log(filter);
      var pureCoverage = false;
      // if this is just a coverage or a group of them, disable a few items,
      // and default to jpeg format
      var format = 'image/png';
 
      if (pureCoverage) {
        format = "image/jpeg";
      }

      var mousePositionControl = new ol.control.MousePosition({
        className: 'custom-mouse-position',
        target: document.getElementById('location'),
        coordinateFormat: ol.coordinate.createStringXY(5),
        undefinedHTML: '&nbsp;'
      });
      var untiled = new ol.layer.Image({
        source: new ol.source.ImageWMS({
          ratio: 1,
          url: 'http://localhost:8080/geoserver/ethio_map/wms',
          params: {'FORMAT': format,
                   'VERSION': '1.1.1',  
                STYLES: '',
                LAYERS: 'ethio_map:intaps_land',
                CQL_FILTER: filter
          }
        })
      });
      var tiled = new ol.layer.Tile({
        visible: false,
        source: new ol.source.TileWMS({
          url: 'http://localhost:8080/geoserver/ethio_map/wms',
          params: {'FORMAT': format, 
                   'VERSION': '1.1.1',
                   tiled: true,
                STYLES: '',
                LAYERS: 'ethio_map:intaps_land',
             tilesOrigin: -150655.671875 + "," + 885372.5625
          }
        })
      });
      var projection = new ol.proj.Projection({
          code: 'EPSG:20137',
          units: 'm',
          axisOrientation: 'neu',
          global: false
      });
      var map = new ol.Map({
        controls: ol.control.defaults({
          attribution: false
        }).extend([mousePositionControl]),
        target: 'map',
        layers: [
          untiled
        ],
        view: new ol.View({
           projection: projection,
           zoom:18
        })
        
      });
      map.getView().on('change:resolution', function(evt) {
        var resolution = evt.target.get('resolution');
        var units = map.getView().getProjection().getUnits();
        var dpi = 25.4 / 0.28;
        var mpu = ol.proj.METERS_PER_UNIT[units];
        var scale = resolution * mpu * 39.37 * dpi;
        if (scale >= 9500 && scale <= 950000) {
          scale = Math.round(scale / 1000) + "K";
        } else if (scale >= 950000) {
          scale = Math.round(scale / 1000000) + "M";
        } else {
          scale = Math.round(scale);
        }
      });
      map.getView().fit(bounds, map.getSize());

          $('#return').on('click',  function() {
                history.go(-1);
            });  
            
        </script>
        
        
</body>
</html>
