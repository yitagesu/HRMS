<%-- 
    Document   : view_candidate
    Created on : Dec 22, 2017, 12:30:56 PM
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>CAMIS</title>
        <link rel="stylesheet" href="assets/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="assets/css/external.css"/>

        <link rel="stylesheet" href="assets/css/custom.min.css"/>
    </head>
        <body class="nav-sm">
            <div class="container body">
            <div class="main_container">
                 <%@include file="all.jsp" %>
                    
                <div class="right_col tab-content" role="main" id ="content">
                    <div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel" id="content">


                                       <h4>Viable Candidates</h4>
                                    <div class="search-result has-scroll col-md-6">
                                        
                                        <label>Zone:<select name = "zone">
                                            <option>Amhara</option>
                                            <option>Oromia</option>
                                            <option>Tigrai</>
                                            
                                            </select></label>

                                                <label>Woreda:<select>
                                                        <option>Woreda_1</option>
                                                        <option>Woreda_2</option>
                                                        <option>Woreda_3</option>
                                            </select></label>
                                                <label>Kebele:<select>
                                                        <option>Kebele_1</option>
                                                        <option>Kebele_2</option>
                                                        <option>Kebele_3</option>
                                            </select></label>
                                        <br><br>
                                            <table class="table table-striped jambo_table bulk_action" id="tclick">
                                                <thead>
                                                    <tr class="headings">
                                                        <th class="column-title">Date </th>
                                                        <th class="column-title">Company Name</th>
                                                        <th class="column-title">Region</th>
                                                        
                                                    </tr>
                                                </thead>

                                                <tbody>

                                                    <tr class="even pointer">
                                                        <td class=" ">12/22/2017</td>
                                                        <td class=" ">INTAPS CONULTANCY</td>
                                                        <td class=" ">Amhara</td>
                                                    </tr>
                                                    <tr class="even pointer">
                                                        <td class=" ">3/4/1006</td>
                                                        <td class=" ">Elfora</td>
                                                        <td class=" ">Oromia</td>
                                                    </tr>

                                                </tbody>
                                            </table>
                                    </div>
                                        </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

    <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/external.js"></script>
        <script src="assets/js/Char.min.js"></script>
        <script src="assets/js/bootstrap-progressbar.min.js"></script>
        <script src="assets/js/icheck.min.js"></script>
        <script src="assets/js/custom.min.js"></script>
        <script>
            
          $('#tclick').on('click', 'tr', function() {
              var currentRow = $(this).closest("tr"); 
               var comapany_name=currentRow.find("td:eq(1)").html();
              
                    $.ajax({
                        url: '/fetch_data.jsp',
                        data: {'cname':comapany_name},
                        dataType: 'xml',
                        complete : function(){
                            //var stateObj = { foo: "bar" };
                           // history.pushState(stateObj, "page 2", this.url);
                            window.location.href = this.url;
                            
                            
                        },
                        success: function(xml){
                        }
                    });
          // 
            });  
            
        </script>
</body>
</html>