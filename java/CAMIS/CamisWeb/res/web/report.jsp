<%-- 
    Document   : report
    Created on : Jan 3, 2018, 3:21:33 PM
    Author     : meda
--%>
<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.intaps.camisweb.*"%>
<html>
   <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>CAMIS</title>
        <link rel="stylesheet" href="/assets/css/external.css"/>
        <link rel="stylesheet" href="/assets/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="/assets/css/custom.min.css"/>
        <link rel="stylesheet" href="/assets/css/popup.css"/>
        <style>
.accordion {
    background-color: #eee;
    color: #444;
    cursor: pointer;
    padding: 18px;
    width: 100%;
    border: none;
    text-align: left;
    outline: none;
    font-size: 15px;
    transition: 0.4s;
}

.active, .accordion:hover {
    background-color: #ccc; 
}

.panel {
    padding: 0 18px;
    display: none;
    background-color: white;
}
</style>

<style>
.vertical-menu {
    width: 400px;
}

.vertical-menu a {
    background-color: #FFFFFf;
    color: black;
    display: block;
    padding: 12px;
    text-decoration: none;
}

.vertical-menu a:hover {
    background-color: #ccc;
}

.vertical-menu a.active {
    background-color: #4CAF50;
    color: white;
}
</style>

            </head>
              
            <body class="nav-sm">
                <div class="container body">
                    <div class="main_container">
                        <%@include file="all.jsp" %>
                        <div class="right_col tab-content" role="main"  id ="content">
                            <div>
                                <div class="row">
                                     <div class="col-md-2">
                                    <div class="vertical-menu">
                                        
                                        <a href="#" class="active" style="width:50%"><h2>List of Reports</h2></a>
                                        <button class="accordion" style="width:50%">Land Bank Report</button>
                                        <div class="panel" style="width:50%">
                                            <a href="Report/listLandInvestment_container.jsp" target="myFrame">Land Identified for Investment </a>
                                            <a href="Report/InvestmentLandPrep_container.jsp" target="myFrame">Investment Land for Preparation</a>
                                            <a href="Report/InvestmentLandTransferred_container.jsp" target="myFrame">Investment Land Transferred</a>
                                            <a href="Report/transferredDeveloLand_container.jsp" target="myFrame">Transferred and Developed Land</a>
                                            <a href="Report/unidentifiedInvesLand_container.jsp" target="myFrame">Unidentified Investment Land</a>
                                            <a href="Report/investmentLandBid_container.jsp" target="myFrame">Investment Land on Bid</a>
                                            <a href="Report/projLandUse_container.jsp" target="myFrame">Projects by Land Use</a>
                                            <a href="Report/clearedLand_container.jsp" target="myFrame">Cleared Lands</a>
                                        </div>
                                        <button class="accordion" style="width:50%">Investor Report</button>
                                        <div class="panel" style="width:50%">
                                            <a href="Report/investorByInveType_container.jsp" target="myFrame">Investors by Investor Type</a>
                                            <a href="Report/investorByCapital_container.jsp" target="myFrame">Investors by Capital</a>
                                            <a href="Report/investorByInvestmentType_container.jsp" target="myFrame">Investor by Investment Type</a>
                                            <a href="Report/investorByAreaHec_container.jsp" target="myFrame">Investors by Area of Land in Hectare</a>
                                            <a href="Report/investorByIrrigation_container.jsp" target="myFrame">Investors by Irrigation Type</a>
                                            <a href="Report/investorsByCropType_container.jsp" target="myFrame">Investors by Crop Type</a>
                                            <a href="Report/investorsByTypeIncentive_container.jsp" target="myFrame">Investors Type of Incentive Taken</a>
                                            <a href="Report/investorWarning_container.jsp" target="myFrame">Investors who has taken warning for their investment</a>
                                            <a href="Report/investorAwarded_container.jsp" target="myFrame">Investors Awarded</a>
                                            <a href="Report/investorStoppedProject_container.jsp" target="myFrame">Investors Stopped Project Implementation</a>
                                            <a href="Report/investorsImplementedProj_container.jsp" target="myFrame">Investors who Implemented Project According to Plan</a>
                                            <a href="Report/investorUnderFederal_container.jsp" target="myFrame">Investor Under Federal</a>
                                            <a href="Report/investorUnderRegion_container.jsp" target="myFrame">Investor Under Regions</a>
                                            <a href="Report/investorAwardedLeaseNot_container.jsp" target="myFrame">Investor Who is Awarded but did not make Lease Agreement</a>
                                        </div>
                                           <button class="accordion" style="width:50%">Farm Follow Up</button>
                                        <div class="panel" style="width:50%">
                                       <a href="/bid_process.jsp" target="myFrame">Link 1</a>
                                            <a href="/report.jsp" target="myFrame">Link 2</a>
                                            <a href="/report.jsp" target="myFrame">Link 3</a>
                                            <a href="/report.jsp" target="myFrame">Link 4</a>
                                        </div>
                                              <button class="accordion" style="width:50%">EIA</button>
                                        <div class="panel" style="width:50%">
                                       <a href="/bid_process.jsp" target="myFrame">Link 1</a>
                                            <a href="/report.jsp" target="myFrame">Link 2</a>
                                            <a href="/report.jsp" target="myFrame">Link 3</a>
                                            <a href="/report.jsp" target="myFrame">Link 4</a>
                                        </div>
                                                 <button class="accordion" style="width:50%">Monitoring and Evaluation</button>
                                        <div class="panel" style="width:50%">
                                       <a href="/bid_process.jsp" target="myFrame">Link 1</a>
                                            <a href="/report.jsp" target="myFrame">Link 2</a>
                                            <a href="/report.jsp" target="myFrame">Link 3</a>
                                            <a href="/report.jsp" target="myFrame">Link 4</a>
                                        </div>
                                          <button class="accordion" style="width:50%">Lease Report</button>
                                        <div class="panel" style="width:50%">
                                            <a href="Report/leaseAgreeByInvest_container.jsp" target="myFrame">Lease Agreements by Investment Type</a>
                                            <a href="Report/newLease_container.jsp" target="myFrame">New Lease</a>
                                            <a href="Report/renewedLease_container.jsp" target="myFrame">Renewed Lease</a>
                                            <a href="Report/terminatedLease_container.jsp" target="myFrame">Terminated Lease</a>
                                            <a href="Report/leaseAgreeExpired_container.jsp" target="myFrame">Lease Agreement to be Expired</a>
                                            
                                        </div>
                                        
                                    </div>
                                        </div>
                                    <div class="col-md-10 col-sm-12 col-xs-12">
                                        <iframe class="report-iframe" src="Report/listLandInvestment_container.jsp" width= "1030" height="30000" name ="myFrame">
                                        
                                    </iframe>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </body>
            <script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
            panel.style.display = "none";
        } else {
            panel.style.display = "block";
        }
    });
}
</script>

</html>
