<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.intaps.camisweb.*"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>CAMIS</title>
        <link rel="stylesheet" href="assets/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="assets/css/external.css"/>

        <link rel="stylesheet" href="assets/css/custom.min.css"/>
    </head>
        <body class="nav-sm">
            <div class="container body">
            <div class="main_container">

                <div class="col-md-3 left_col menu_fixed">
                    <div class="left_col scroll-view">
                        <div class="navbar nav_title" style="border: 0;">
                            <a href="#" class="site_title"><i class="fa fa-map-marker"></i> <span>CAMiS</span></a>
                        </div>

                        <div id="sidebar-menu" class="main_menu_side main_menu">
                            <div class="menu_section">
                                <h3 style="visibility:hidden;">general</h3>
                                <ul class="nav side-menu">
                                    <li class="active"><a href=".dashboard" data-toggle="tab" ><i class="fa fa-table"></i> Dashboard </a>
                                    </li>
                                    <li><a href="#user" data-toggle="tab" ><i class="fa fa-user"></i> Investor Files</a>
                                    </li>
                                    <li><a><i class="fa fa-gears"></i> Setting</a>
                                    </li>
                                    <li><a><i class="fa fa-files-o"></i> Reports</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="top_nav">
                    <div class="nav_menu">
                        <nav>
                            <div class="nav toggle">
                                <a id=""><i class="fa fa-map-marker"></i> <span>CAMiS</span></a>
                                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                            </div>

                            <ul class="nav navbar-nav navbar-right">
                                <li class="">
                                    <a href="" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        Welcome yite
                                        <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                                        <li><a href="#"> Change Password</a></li>
                                        <li><a href="#">Help</a></li>
                                        <li><a href="/login.jsp"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="right_col tab-content" role="main">
                    <div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Investor Project Assessment Profile View</h2>
                                       
                                        <div class="clearfix"></div>
                                    </div>

                                  <div class="x_content">
                                     <div class="row">
                                        
                                            <div class="col-md-2">
                                            <form class="form-horizontal form-label-left" novalidate>
                                            <div class="input-group">
                                                <span class="input-group-addon">Zone</span>
                                                <!--<input type="text" id="zone" name="zoni"  class="form-control">-->
                                                 <div class="dropdown col-md-4">
                                                <button class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">Please Select Zone
                                                <span class="caret"></span></button>
                                                <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Zone1</a></li>
                                                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Zone2</a></li>
                                                </ul>
                                              </div>
                                            </div>
                                            <div class="input-group">
                                                <span class="input-group-addon">Woreda</span>
                                               <!-- <input type="text" id="woreda" name="wor"  class="form-control">-->
                                                <div class="dropdown col-md-4">
                                                <button class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">Please Select Woreda
                                                <span class="caret"></span></button>
                                                <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Woreda1</a></li>
                                                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Woreda2</a></li>
                                                </ul>
                                              </div>
                                            </div>
                                                 <div class="input-group">
                                                <span class="input-group-addon">Kebele</span>
                                                <!-- <input type="text" id="kebele" name="keb"  class="form-control">-->
                                                <div class="dropdown col-md-4">
                                                <button class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">Please Select Kebele
                                                <span class="caret"></span></button>
                                                <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Kebele1</a></li>
                                                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Kebele2</a></li>
                                                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Kebele3</a></li>
                                                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Kebele4</a></li>
                                                </ul>
                                              </div>
                                            </div>
                                            <div class="form-group align-right">
                                                <button id="search_button" class="btn btn-success "><i class="fa fa-search"></i> Search</button>
                                            </div>
                                        </form>
                                            </div>
                                         <div class="col-md-12">
                                        <h3>Profile</h3>
                                        
                                        <table class="table table-striped jambo_table bulk_action">
                                            <thead>
                                            <th>No</th>
                                            <th>Company</th>
                                            <th>Company Owner</th>
                                            <th>Company Type</th>
                                            <th>Project start date</th>
                                            <th>Project Type</th>
                                            <th>More Details</th>
                                            </thead>
                                            <tbody>
                                                <%
                                                InvestorInfo.investerAssesment retInAsses = Investor.getInvestorP();
                                                for(InvestorInfo.investerAssesment.investorProfile val:retInAsses.ip){
                                                %>
                                                <tr><td><%=val.no%></td>
                                                    <td><%=val.Company%></td>
                                                    <td><%=val.Owner%></td>
                                                    <td><%=val.CType%></td>
                                                    <td><%=val.PType%></td>
                                                    <td><%=val.PSDate%></td>
                                                    <td>
                                                        <button class="btn btn-success" data-toggle='modal' data-target='#viewIp' data-backdrop='static'>View Details</button>
                                                    </td>
                                                    <%
                                                        }
                                                        %>
                                                
                                                </tr>
                                            
                                            </tbody>
                                            

                                        </table>
                                        </div>
                                         <div class="col-md-2 col-md-offset-6">
                                        
                                     </div>
                                   </div>

                                   
                                        </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                
                <div class="modal fade " id="viewIp" tabindex="-1" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">

                                <div class="modal-header">
                                    <h2>Investor Profile</h2>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    
                                </div>            <!-- /modal-header -->
                               
                                    <div class="modal-body table-responsive">
                                        <table class="table table-striped jambo_table bulk_action">
                                            <%
                                                InvestorInfo.investerAssesDetails retDetails = Investor.getInvestorD();
                                                for(InvestorInfo.investerAssesDetails.investorDetails val:retDetails.ID){
                                                %>
                                            <tr><td bgcolor="#ABEBC6"><strong> &nbsp;&nbsp; &nbsp;&nbsp; Investor Details :</strong> </td> <td bgcolor="#ABEBC6"></td></tr>
                                            <tr><td>Company's name : </td> <td><%=val.Company%></td></tr>
                                            <tr><td>Company's owner name : </td> <td><%=val.Owner%></td></tr>
                                            <tr><td>Company Type : </td><td><%=val.CType%></td></tr>
                                            <tr><td>Investor type : </td><td><%=val.IType%></td></tr>
                                            <tr><td>Investment permit no : </td> <td><%=val.IPNo%></td></tr>
                                             <tr><td>Starting capital : </td> <td><%=val.SCapital%> </td></tr>
                                            <tr><td>Current capital : </td> <td><%=val.CCapital%> </td></tr>
                                            
                                            <tr><td bgcolor="#ABEBC6"><strong> &nbsp;&nbsp; &nbsp;&nbsp; Project Details :</strong> </td> <td bgcolor="#ABEBC6"></td></tr>
                                            <tr><td>Project type : </td> <td><%=val.PType%></td></tr>
                                            <tr><td>Project start date : </td> <td><%=val.PSDate%> </td></tr>
                                            <tr><td>Project period : </td> <td><%=val.PPeriod%> </td></tr>
                                            <tr><td>Project Status:(starting, on progress ) : </td> <td><%=val.PStatus%> </td></tr>
                                            
                                            <tr bgcolor="#ABEBC6" ><td><strong> &nbsp;&nbsp; &nbsp;&nbsp; Address :</strong> </td> <td></td></tr>
                                            <tr><td>Region : </td> <td><%=val.Region%> </td></tr>
                                            <tr><td>Zone : </td> <td><%=val.Zone%> </td></tr>
                                            <tr><td>Woreda : </td> <td><%=val.Woreda%> </td></tr>
                                            <tr><td>City : </td> <td><%=val.City%> </td></tr>
                                            <tr><td>Address : </td> <td><%=val.Address%> </td></tr>
                                            <tr><td>Phone : </td> <td><%=val.Phone%> </td></tr>
                                            <tr><td>Email : </td> <td><%=val.Email%> </td></tr>
                                            <tr><td>Fax : </td> <td><%=val.Fax%> </td></tr>
                                            <tr><td>Fax : </td> <td><%=val.Faxi%> </td></tr>
                                            
                                            <tr><td><tr  ><td bgcolor="#ABEBC6"><strong> &nbsp;&nbsp; &nbsp;&nbsp; Work opportunity:</strong></td><td bgcolor="#ABEBC6"></td></tr>
                                            <tr><td><i>  Permanent(Male,Female)</i></td><td><%=val.Perma%></td></tr>
                                            <tr><td><i> Temporary(Male,Female)</i></td><td><%=val.Tempo%></td></tr>
                                            <tr><td><i> Total(Male,Female)</i></td><td><%=val.Total%></td></tr> </td></tr>
                                            
                                            <tr><td><tr bgcolor="#ABEBC6" ><td><strong> &nbsp;&nbsp; &nbsp;&nbsp; Miscellaneous:</strong></td><td></td></tr>
                                            <tr><td>Government incentive: : </td> <td><%=val.GovIncentive%> </td></tr>
                                            <tr><td>Production consumption type and amount per year : </td> <td><%=val.PCTAmtperYR%> </td></tr>
                                            <tr><td>Suppliers : </td> <td><%=val.Suppliers%> </td></tr>
                                            <tr><td>Technology Used : </td> <td><%=val.TechUsed%> </td></tr>
                                            <tr><td>Production Used : </td> <td><%=val.ProductionUsed%> </td></tr>
                                            <tr><td>Production capacity : </td>  <td><%=val.ProductionCapacity%> </td></tr>
                                            <tr><td>Production Sells Area : </td><td><%=val.ProductionSellsArea%>  </td></tr>
                                            <tr><td>Evaluation : </td> <td><%=val.Evaluation%> </td></tr>
                                            
                                            <%}%>

                                        </table>        
                                    </div>
                                    <div class="modal-footer">
                                    </div>


                            </div>
                        </div>
                    </div>
            </div>

    </body>
    <script>
        var select1 = document.getElementById('searchType');
        var selects = document.getElementsByTagName('select');

select1.onchange = function() {
    var select2 = this.value.toLowerCase();
    for (i = 0; i < selects.length; i++) {
        if (selects[i].id != this.id) {
            selects[i].style.display = 'none';
        }
    }
    document.getElementById(select2).style.display = 'block';
    document.getElementById('textAreaSearchBox').style.display = 'block';
};
        </script>
    <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/external.js"></script>
        <script src="assets/js/Char.min.js"></script>
        <script src="assets/js/bootstrap-progressbar.min.js"></script>
        <script src="assets/js/icheck.min.js"></script>
        <script src="assets/js/custom.min.js"></script>
</html>
