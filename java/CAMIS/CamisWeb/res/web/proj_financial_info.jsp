<%-- 
    Document   : proj_financial_info
    Created on : Jan 25, 2018, 10:27:02 AM
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<f:view>
  <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>CAMIS</title>
        <link rel="stylesheet" href="/assets/css/external.css"/>
        <link rel="stylesheet" href="/assets/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="/assets/css/custom.min.css"/>
        <link rel="stylesheet" href="/assets/css/popup.css"/>
            </head>
    <body class="nav-sm">
        
        <div class="container body">
            <div class="main_container">

           <%@include file="all.jsp" %>
           
               <div class="right_col tab-content" role="main">
                    <div>
                        <div class="row" >
                        <p><i>${message}</i></p>
           <div class="col-md-12 col-sm-12 col-xs-12" > 
           
              

            <form action="financialInformation" method="post">
              
                <div class="form-row align-items-center">
                  
            <input type="hidden" name="action" value="add" >
             <div class="col-sm-3 my-1">
            <label for="aname">Account Name</label>
            <input class="form-control" type="text" id = "aname" name="account_name" value ="${finance.accountName}" placeholder="Account Name"/>
            </div>
            <div class="col-sm-3 my-1">
            <label for="amount">Budget Amount</label>
            <input class="form-control" type="text" id = "amount" name="budget_amount" value ="${finance.budgetAmount}" placeholder="Budget Amount"/>
            </div>  
               
           </div>
            </form>
                        </div>
                    </div>
               </div>
            </div>
        </div>
           
        </body>
    </html>
</f:view>
