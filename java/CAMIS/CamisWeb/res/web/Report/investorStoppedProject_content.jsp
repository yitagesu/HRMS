<%-- 
    Document   : leaseInverstmentType
    Created on : Jan 3, 2018, 3:00:27 PM
    Author     : meda
--%>


<!DOCTYPE html>
<%@page import="com.intaps.camisweb.*"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>CAMIS</title>
        <link rel="stylesheet" href="/assets/css/external.css"/>
        <link rel="stylesheet" href="/assets/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="/assets/css/custom.min.css"/>
        <link rel="stylesheet" href="/assets/css/popup.css"/>
           <script src="/assets/js/jquery.min.js"></script>
        <script src="/assets/js/external.js"></script>
        <script src="/assets/js/Char.min.js"></script>
        <script src="/assets/js/bootstrap-progressbar.min.js"></script>
        <script src="/assets/js/bootstrap.min.js"></script>
        <script src="/assets/js/custom.min.js"></script>
        <style>
        .bg-white {
    background-color: #ffffff;
        }
    </style>

        
            </head>
           
           <table class="table table-bordered table-hover bg-white">

         <tr>
            <td colspan = "10" style = "text-align:center">
              <h2><strong>List of Investors Stopped Project Implementation</strong></h2>
            </td></tr>
         <tr>
             <td colspan ="10" style = "border-collapse: collapse; border: none;" ><strong>Investor Type: </strong>Local</td>
         </tr>
          <tr>
            <td colspan ="10" style = "border-collapse: collapse; border: none;" ><strong>Investment Type: </strong>Horticulture</td>
         </tr>
      
         <tr>
             <th class="column-title">No.</th>
             <th class="column-title">Investor Name</th>
             <th class="column-title">Gender</th>
             <th class="column-title">Organization</th>
             <th class="column-title">Woreda</th>
             <th class="column-title">Kebele</th>     
              <th class="column-title">Area</th>
             <th class="column-title">Project Type</th>
              <th class="column-title">Lease Period</th>
             <th class="column-title">Evaluation</th>     
         </tr>
         <tbody>
             <tr class="even pointer">
                 <td >1</td>
                 <td >Ermias Chebud</td>
                 <td >Male</td>
                 <td >private</td>
                 <td >Meskan</td>
                 <td >Meseretewegeramo</td>
                 <td >777666.99</td>
                 <td>private</td>
                  <td>50 years</td>
                 <td >Good</td>
             </tr>
            <tr class="even pointer">
                 <td >3</td>
                 <td >Lidya Mesfin</td>
                 <td >Female</td>
                 <td >private</td>
                 <td >Meskan</td>
                 <td >Semen Shorshora</td>
                 <td >2900.99</td>
                 <td>private</td>
                 <td>30 years</td>
                 <td >Good</td>
             </tr>
             <tr class="even pointer">
                 <td >4</td>
                 <td >Genet Worede</td>
                 <td >Female</td>
                 <td >private</td>
                 <td >Meskan</td>
                 <td >Degagogot</td>
                 <td >2500.99</td>
                 <td>private</td>
                  <td>100 years</td>
                 <td >Good</td>
             </tr>
             <tr class="even pointer">
                 <td >4</td>
                 <td >Fasil Ashenafi</td>
                 <td >Male</td>
                 <td >private</td>
                 <td >Sodo Woreda</td>
                 <td >Genet Mariam</td>
                 <td >455.99</td>
                 <td>private</td>
                  <td>60 years</td>
                 <td >Good</td>
             </tr>
            <tr class="even pointer">
                 <td >5</td>
                 <td >Yodahi Petros</td>
                 <td >Female</td>
                 <td >private</td>
                 <td >Sodo Woreda</td>
                 <td >Genet Mariam</td>
                 <td >10000.99</td>
                 <td>private</td>
                 <td>100 years</td>
                 <td >Good</td>
             </tr>
             <tr class="even pointer">
                 <td >6</td>
                 <td >Abel Getachew</td>
                 <td >Male</td>
                 <td >private</td>
                 <td >Sodo Woreda</td>
                 <td >Suten Akebabi</td>
                 <td >678.99</td>
                 <td>private</td>
                  <td>25 years</td>
                 <td >Good</td>
             </tr>
         </tbody>
           </table>
        
</html>
