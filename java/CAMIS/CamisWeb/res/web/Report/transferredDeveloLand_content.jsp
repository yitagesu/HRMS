<%-- 
    Document   : leaseInverstmentType
    Created on : Jan 3, 2018, 3:00:27 PM
    Author     : meda
--%>


<!DOCTYPE html>
<%@page import="com.intaps.camisweb.*"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>CAMIS</title>
        <link rel="stylesheet" href="/assets/css/external.css"/>
        <link rel="stylesheet" href="/assets/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="/assets/css/custom.min.css"/>
        <link rel="stylesheet" href="/assets/css/popup.css"/>
           <script src="/assets/js/jquery.min.js"></script>
        <script src="/assets/js/external.js"></script>
        <script src="/assets/js/Char.min.js"></script>
        <script src="/assets/js/bootstrap-progressbar.min.js"></script>
        <script src="/assets/js/bootstrap.min.js"></script>
        <script src="/assets/js/custom.min.js"></script>
        <style>
        .bg-white {
    background-color: #ffffff !important;
        }
    </style>

        
            </head>
           
           <table class="table table-bordered table-hover bg-white">

         <tr>
            <td colspan = "8" style = "text-align:center">
              <h2><strong>List of Transferred and Developed Land</strong></h2>
            </td></tr>
         <tr>
             <td colspan ="8" style = "border-collapse: collapse; border: none;" ><strong>Region: </strong>Southern People</td>
         </tr>
          <tr>
            <td colspan ="8" style = "border-collapse: collapse; border: none;" ><strong>Zone: </strong>Gurage</td>
         </tr>
        
          <tr>
            <td colspan ="8" style = "border-collapse: collapse; border: none;" ><strong>Woreda: </strong>Meskan</td>
         </tr>
        
         <tr>
             <th class="column-title">No.</th>
             <th class="column-title">UPID</th>
             <th class="column-title">Kebele</th>
             <th class="column-title">Area</th>
             <th class="column-title">Coordinate</th>
             <th class="column-title">Distance From The City</th>     
         </tr>
         <tbody>
             <tr class="even pointer">
                 <td class=" ">1</td>
                 <td class=" ">070106001/00002</td>
                 <td class=" ">Meseretewegeramo</td>
                 <td class=" ">10000.99</td>
                 <td>8.122768,38.155574</td>
                 <td class=" ">12km</td>
             </tr>
              <tr class="even pointer">
                 <td class=" ">2</td>
                 <td class=" ">070106001/00025</td>
                 <td class=" ">Meseretewegeramo</td>
                 <td class=" ">500.22</td>
                 <td>7.522768,37.154345</td>
                 <td class=" ">24km</td>
             </tr>
              <tr class="even pointer">
                 <td class=" ">3</td>
                 <td class=" ">070106001/00070</td>
                 <td class=" ">Meseretewegeramo</td>
                 <td class=" ">6544.99</td>
                 <td>6.122768,36.155574</td>
                 <td class=" ">19km</td>
             </tr>
         </tbody>
           </table>
        
</html>
