<%-- 
    Document   : login.jsp
    Created on : Nov 9, 2017, 4:30:56 PM
    Author     : Joseph
--%>
<%@page import="com.intaps.camisweb.*"%>
<%@page import="com.intaps.camisweb.viewmodel.*"%>
<%@page import="com.intaps.camisweb.vmcontroller.*"%>


<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>CAMIS</title>
        <link rel="stylesheet" href="/assets/css/external.css"/>
        <link rel="stylesheet" href="/assets/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="/assets/css/custom.min.css"/>
        
<style>
body {font-family: Arial;}

/* Style the tab */
.tab {
    overflow: hidden;
    border: 1px solid #ccc;
    background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
.tab button {
    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    transition: 0.3s;
    font-: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
    background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
    background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
    display: none;
    padding: 6px 12px;
    border: 1px solid #ccc;
    border-top: none;
}
.mtable th, .mtable td { 
     border-top: none !important; 
 }
</style>
    </head>

    <body class="nav-sm" onload="openTab(event, 'Details')">
        <div class="container body">
            <div class="main_container">
<%@include file="all.jsp" %>
                <div class="right_col tab-content" role="main">
                    <div class=" right_col_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12" style="width: 92vw;" >
                                <div class="container">
  
  <ul class="nav nav-tabs" style>
      <li class="active" style="width:200pt"><a data-toggle="tab" href="#project1"><i class="fa fa-tags" aria-hidden="true"></i> Project 1</a></li>
      <li style="width:200pt"><a data-toggle="tab" href="#project2"><i class="fa fa-envelope-open" aria-hidden="true"></i> Project 2</a></li>
    <li style="width:200pt"><a data-toggle="tab" href="#project3"><i class="fa fa-male" aria-hidden="true"></i> Project 3</a></li>
    
  </ul>

  <div class="tab-content">
    <div id="project1" class="tab-pane fade in active">
      
      
   
                                <div class="tab col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <button class="tablinks" onclick="openTab(event, 'Details')">Company/ Investor Profile</button>
                                    <button class="tablinks" onclick="openTab(event, 'Document')">Document</button>
                                    <button class="tablinks" onclick="openTab(event, 'lease')">Lease Agreement</button>
                                    <button class="tablinks" onclick="openTab(event, 'cadaster')">Cadaster Information</button>
                                    <button class="tablinks" onclick="openTab(event, 'PromotedLand')">Promoted Land Information</button>
                                    <button class="tablinks" onclick="openTab(event, 'pdetail')">Project Details</button>
                                    <button class="tablinks" onclick="openTab(event, 'invsup')">Investment Support</button>
                                    <button class="tablinks" onclick="openTab(event, 'nott')">Notifications</button>
                                </div>
                                
                                
                                <div id="Details" class="tabcontent">
                                    <div class="col-md-8">
                                    <table class=" col-md-8 table table-borderless mtable" >
                                            <%
                                                InvestorInfo.investerAssesDetails retDetails1 = Investor.getInvestorD();
                                                for(InvestorInfo.investerAssesDetails.investorDetails val:retDetails1.ID){
                                                %>
                                            <tr class="bg-primary"> <td><font color="white"><i class="fa fa-align-justify" aria-hidden="true"style="color: goldenrod"></i><strong>  Investor Details :</strong> </strong></td><td></td></tr>
                                            <tr><td style="color: black"><strong>  Company's name :</font></strong></td><td><%=val.Company%></td></tr>
                                            <tr><td style="color: black"><strong>Company's owner name :</strong></td><td><%=val.Owner%></td></tr>
                                            <tr><td style="color: black"><strong>Company Type : </strong></td><td><%=val.CType%></td></tr>
                                            
                                            <tr><td style="color: black"><strong> Nationality / Country of incorporation, if company :</strong></td><td><%=val.Nationality%> </td></tr>
                                            <tr><td style="color: black"><strong>Tax Identification no. :</strong></td><td><%=val.taxid%></td> </tr>
                                            <tr><td style="color: black"><strong>Commercial Registration No. </strong></td><td><%=val.comreg%></td> </tr>
                                            
                                            <tr><td style="color: black"><strong>Investor type : </strong></td><td><%=val.IType%></td></tr>
                                            <tr><td style="color: black"><strong>Investment permit no : </strong></td><td><%=val.IPNo%></td></tr>
                                            
                                           
                                            
                                            <tr class="bg-primary"><td><i class="fa fa-address-book" aria-hidden="true"style="color: goldenrod"></i> <strong><font color="white"><strong>  Address :</strong> </td> <td  ></td></tr>
                                            <tr><td style="color: black"><strong>Region :</strong></td><td><%=val.Region%> </td></tr>
                                            <tr><td style="color: black"><strong>Zone :</strong></td><td><%=val.Zone%> </td></tr>
                                            <tr><td style="color: black"><strong>City :</strong></td><td><%=val.City%> </td></tr>
                                            <tr><td style="color: black"><strong>Sub-city/ Woreda :</strong></td><td><%=val.Woreda%> </td></tr>
                                            
                                            <tr><td style="color: black"><strong>Kebele :</td>  <td><%=val.kebele%> </td></tr>
                                            <tr><td style="color: black"><strong>House No.</strong></td><td><%=val.HouseNo%></td> </tr>
                                            <tr><td style="color: black"><strong>Telephone No.( Land line)</td>  <td><%=val.Telephone%> </td> </tr>
                                            <tr><td style="color: black"><strong>Address :</strong></td><td><%=val.Address%> </td></tr>
                                            <tr><td style="color: black"><strong>Post</strong></td><td><%=val.post%></td></tr>
                                            <tr><td style="color: black"><strong>Street Address</strong></td><td><%=val.sa%></td></tr>
                                            <tr><td style="color: black"><strong>Emergency Contact</strong></td><td><%=val.ec%></td></tr>
                                            
                                            <tr><td style="color: black"><strong>Phone :</strong></td><td><%=val.Phone%> </td></tr>
                                            <tr><td style="color: black"><strong>Email :</strong></td><td><%=val.Email%> </td></tr>
                                            <tr><td style="color: black"><strong>Fax : </strong></td><td> <%=val.Fax%> </td></tr>
                                            
                                             <tr class="bg-primary"><td  ><font color="white"><strong><i class="fa fa-money" aria-hidden="true"style="color: goldenrod"></i>  Finance :</strong> </td> <td  ></td></tr>
                                            <tr><td style="color: black"><strong>Starting capital :</strong></td><td><%=val.SCapital%> </td></tr>
                                            <tr><td style="color: black"><strong>Current capital :</strong></td><td><%=val.CCapital%> </td></tr>
                                            <tr><td style="color: black"><strong>Form of ownership : </strong></td><td><%=val.formofownership%></td> </tr>
                                            <tr><td style="color: black"><strong>Type of ownership :</strong></td><td><%=val.towner%> </td> </tr>
                                         
                                            <%}%>
                                     </table>
                                    </div>
                                     <div class="col-md-2 col-md-offset-2 menu_fixed" >
                                        <image height="140px" width="140px" src="assets/images/mal.jpg"/>
                                        </div>
                                     
                                     
                                  </div>
                                     

                                  <div id="Document" class="tabcontent">
                                    <h3> <font color="black">Full Investor File </font> </h3>
                                      
                                      <div class="panel panel-primary">
                                      <div class="panel-heading"><font color="white"><i class="fa fa-files-o" aria-hidden="true"style="color: goldenrod"></i> Files </font></div>
                                      <div class="panel-body">    
                                          <table>
                                               <%
                                                InvestorInfo.investordocuments retinvdocs11 = Investor.getinvestordocs();
                                                for(InvestorInfo.investordocuments.investordocs val:retinvdocs11.ids){
                                                %>
                                        <tr><td style="color: black"><strong>Power of Attorney : </strong></td><td style="padding-left:50pt"><%=val.poa%></td></tr>
                                        <tr><td style="color: black"><strong>Memorandum (if organization) : </strong></td><td style="padding-left:50pt"><%=val.memorandum%></td></tr>
                                        <tr><td style="color: black"><strong>Regulation of establishment (if public enterprise) : </strong></td><td style="padding-left:50pt"><%=val.roe %></td></tr>                                             
                                        <tr><td style="color: black"><strong>Articles of Association (if cooperative society) : </strong></td><td style="padding-left:50pt"><%=val.aoa %></td></tr>
                                       <tr><td style="color: black"><strong>Support letter for Lease Applications :</strong></td><td style="padding-left:50pt"><%=val.slla %></td></tr> 
                                       
                                       <tr><td style="color: black"><strong>Agricultural Investment Land Identification Document :</strong></td><td style="padding-left:50pt"><%=val.ailid %></td></tr>
                                       
                                       <tr><td style="color: black"><strong>Land Transfer Letter :</strong></td><td style="padding-left:50pt"><%=val.landtl %></td></tr>
                                       
                                       <tr><td style="color: black"><strong>Work permit :</strong></td><td style="padding-left:50pt"><%=val.workpermit %></td></tr>
                                       
                                       <tr><td style="color: black"><strong>Certificate for land use by Regional State Environment, Forest & Land Administration Bureau :</strong></td><td style="padding-left:50pt"><%=val.cflrs %></td></tr>
                                       
                                       
                                       
                                       <tr><td style="color: black"><strong>Profit or Loss Balance from Business :</strong></td><td style="padding-left:50pt"><%=val.profitorloss %></td></tr>
                                       
                                       <tr><td style="color: black"><strong>Organization Law : </strong></td><td style="padding-left:50pt"><%=val.organiztionlaw %></td></tr>
                                       
                                       <tr><td style="color: black"><strong>Meeting Minutes :</strong></td><td style="padding-left:50pt"><%=val.meetingminutes %></td></tr>
                                       <tr><td style="color: black"><strong> ID (if individual) :</strong></td><td style="padding-left:50pt"><%=val.id%></td></tr>
                                       <%}%>
                                       
                                       </table>
                                              </div>
                                      </div>
                                  
                                  
                                    <div class="panel panel-primary">
                                        <div class="panel panel-heading"><font color="white"><i class="fa fa-users" aria-hidden="true"style="color: goldenrod"></i> Foreign Investor </font></div>
                                        <div class="panel panel-body"> 
                                            <table>
                                                <%
                                                InvestorInfo.investordocuments retinvdocs1f = Investor.getinvestordocs();
                                                for(InvestorInfo.investordocuments.investordocs val:retinvdocs1f.ids){
                                                %>
                                           <tr><td style="color: black"><strong> Passport (if individual) : </strong></td><td style="padding-left:50pt"><%=val.passport%></td></tr>
                                           <tr><td style="color: black"><strong>   Letter from Federal Democratic Republic of Ethiopia Ministry of Foreign Affairs for Diaspora Association :</strong></td><td style="padding-left:50pt"><%=val.lfdremofa%></td></tr>
                                       
                                      <tr><td style="color: black"><strong> Support letter to Invest as Diaspora from Foreign Minister Investor for Diaspora Activity Directorate General :</strong></td><td style="padding-left:50pt"><%=val.supportletter%></td></tr>
                                            <%}%>
                                            </table>
                                        </div> 
                                    </div>
                                    <div class="panel panel-primary">
                                      <div class="panel panel-heading"><font color="white" ><i class="fa fa-expand" aria-hidden="true"style="color: goldenrod"></i> For Expansion or Upgrading of an existing enterprise</font></div>
                                       <div class="panel panel-body"> 
                                           <table>
                                                <%
                                                InvestorInfo.investordocuments retinvdocs1b = Investor.getinvestordocs();
                                                for(InvestorInfo.investordocuments.investordocs val:retinvdocs1b.ids){
                                                %>
                                            <tr><td style="color: black"><strong>  business license : <br> </strong></td><td style="padding-left:50pt"><%=val.businesslicence%></td></tr>
                                            <%}%>
                                           </table>
                                       </div>   
                                    </div>
                                    <div class="panel panel-primary">
                                      <div class="panel panel-heading"><font color="white" ><i class="fa fa-file" aria-hidden="true"style="color: goldenrod"></i> Lease Required Documents</font></div>
                                       <div class="panel panel-body"> 
                                           <table>
                                                <%
                                                InvestorInfo.investordocuments retinvdocs1l = Investor.getinvestordocs();
                                                for(InvestorInfo.investordocuments.investordocs val:retinvdocs1l.ids){
                                                %>
                                            <tr><td style="color: black"><strong>  business plan : <br> </strong></td><td style="padding-left:50pt"><%=val.businessplan%></td></tr>
                                            <tr><td style="color: black"><strong>  The site plan of the leased land : <br> </strong></td><td style="padding-left:50pt"><%=val.siteplanofleaseland%></td></tr>
                                            <tr><td style="color: black"><strong>  Leased land delivery minute :<br> </strong></td><td style="padding-left:50pt"><%=val.leasedlanddeliveryminute%></td></tr>
                                            <tr><td style="color: black"><strong>  Environment impact assessment document : <br> </strong></td><td style="padding-left:50pt"><%=val.eiad%></td></tr>
                                            <tr><td style="color: black"><strong>  Performance reports. : <br> </strong></td><td style="padding-left:50pt"><%=val.performancereports%></td></tr>
                                            <tr><td style="color: black"><strong>  Closure plan : <br> </strong></td><td style="padding-left:50pt"><%=val.closureplan%></td></tr>
                                            <tr><td style="color: black"><strong>  Community agreement (social contract): <br> </strong></td><td style="padding-left:50pt"><%=val.communityagree%></td></tr>
                                            <tr><td style="color: black"><strong>  Land request form : <br> </strong></td><td style="padding-left:50pt"><%=val.landreqform%></td></tr>
                                            <%}%>
                                           </table>
                                       </div>   
                                    </div>
                                    <div class="panel panel-primary">
                                      <div class="panel panel-heading"><font color="white" > <i class="fa fa-archive" aria-hidden="true"style="color: goldenrod"></i> Miscellaneous </font></div>
                                       <div class="panel panel-body"> 
                                           <table>
                                                <%
                                                InvestorInfo.investordocuments retinvdocs1m = Investor.getinvestordocs();
                                                for(InvestorInfo.investordocuments.investordocs val:retinvdocs1m.ids){
                                                %>
                                            <tr><td style="color: black"><strong>  Crop Calendar : <br> </strong></td><td style="padding-left:50pt"><%=val.cropcalander%></td></tr>
                                            <tr><td style="color: black"><strong>  Employee Contract Document: <br> </strong></td><td style="padding-left:50pt"><%=val.employeecontractdoc%></td></tr>
                                            <tr><td style="color: black"><strong>  Internal Rule and regulation document :<br> </strong></td><td style="padding-left:50pt"><%=val.internalruleandregdoc%></td></tr>
                                            <tr><td style="color: black"><strong>  Record of annual crop infestations and spread of disease and measures taken : <br> </strong></td><td style="padding-left:50pt"><%=val.raciasdmt%></td></tr> 
                                            <tr><td style="color: black"><strong> Investor investment track record document</strong></td><td style="padding-left:50pt"><%=val.iitrd%></td></tr>
                                            <%}%>
                                           </table>
                                       </div>   
                                    </div>
                                    
                                    
                                    
                                  </div>
                                  <div id="PromotedLand" class="tabcontent">
                                      <div class="col-md-6 col-sm-18 col-xs-18">
                                    <table class="table table-borderless mtable">
                                            <thead>
                                            <th></th>
                                            <th></th>
                                            </thead>
                                            <tbody>
                                                <%
                                                LandInfo.LandBankProfile retLandPro1 = Land.getLandBankProfile();
                                                for(LandInfo.LandBankProfile.LandBankProfileData val: retLandPro1.data){
                                                %>
                                                <tr class="bg-primary"><td><i class="fa fa-address-card" aria-hidden="true"style="color: goldenrod"></i> Parcel ID/Block ID/Farm ID (UPID)</td><td></td></tr>
                                                <tr><td style="color: black"><strong>Parcel Id</strong></td>
                                                    <td><%=val.parcelId%></b></td>
                                                </tr>
                                                 <tr class="bg-primary"><td><i class="fa fa-address-book" aria-hidden="true"style="color: goldenrod"></i> Address</td><td></td></tr>
                                                <tr><td style="color: black"><strong>Kebele</strong></td>
                                                    <td ><%=val.kebele%></b></td>
                                                    </tr>
                                                <tr><td style="color: black"><strong>Woreda</strong></td>
                                                    <td ><%=val.woreda%></b></td></tr>
                                                
                                                <tr><td style="color: black"><strong>Zone</strong></td>
                                                <td ><%=val.zone%></td></tr>
                                                
                                                <tr><td style="color: black"><strong>Region</strong></td>
                                                <td><%=val.region%></td></tr>
                                                
                                                <tr><td style="color: black"><strong>City</strong></td>
                                                <td><%=val.city%></td></tr>
                                                
                                                <tr><td style="color: black"><strong>Address</strong></td>
                                               	<td><%=val.address%></td></tr>
                                                 <tr><td style="color: black"><strong>Distance from point of center(region capital)</strong></td>
                                                     <td><%=val.distance%></td></tr>
                                                 <tr><td style="color: black"><strong>X Coordinate</strong></td>
                                                 <td><%=val.xCordinate%></td></tr>
                                                 <tr><td style="color: black"><strong>Y Coordinate</strong></td>
                                                   <td><%=val.yCordinate%></td></tr>
                                                
                                                 <tr class="bg-primary"><td><i class="fa fa-arrows" aria-hidden="true"style="color: goldenrod"></i> Neighbor</td><td></td></tr>
                                                  <tr><td style="color: black"><strong>From North---</strong></td>
                                                    <td><%=val.neighrbourFromNorth%></td></tr>
                                                   <tr><td style="color: black"><strong>From South--</strong></td>
                                                    <td><%=val.neighrbourFromSouth%></td></tr>
                                                    <tr><td style="color: black"><strong>From East--</strong></td>
                                                    <td><%=val.neighrbourFromWest%></td></tr>
                                                     <tr><td style="color: black"><strong>From West--</strong></td>
                                                    <td><%=val.neighrbourFromEast%></td></tr>
                                                  
                                                     <tr class="bg-primary"><td><i class="fa fa-plane" aria-hidden="true"style="color: goldenrod"></i> Accessibility</td><td></td></tr>
                                                    <tr><td style="color: black"><strong>Air</strong></td>
                                                    <td><%=val.accessibilityAir%></td></tr>
                                                     <tr><td style="color: black"><strong>Road</strong></td>
                                                    <td><%=val.accessibilityRoad%></td></tr>
                                                    <tr><td style="color: black"><strong>Rail</strong></td>
                                                    <td><%=val.accessibilityRail%></td></tr>
                                                     <tr><td style="color: black"><strong>Water</strong></td>
                                                    <td><%=val.accessibilityWater%></td></tr>
                                                 
                                                  <tr class="bg-primary"><td><i class="fa fa-square-o" aria-hidden="true"style="color: goldenrod"></i> Area</td><td></td></tr>
                                                   <tr><td style="color: black"><strong>Area(hectare)</strong></td>
                                                    <td><%=val.area%></td></tr>
                                                     <tr><td style="color: black"><strong>condition of the expansion spots</strong></td>
                                                    <td><%=val.conditionExpansion%></td></tr>
                                                    
                                                    <tr class="bg-primary"><td><i class="fa fa-line-chart" aria-hidden="true"style="color: goldenrod"></i> Status of infrastructure and services</td><td></td></tr>
                                                   <tr><td style="color: black"><strong>Road-Asphalt and Electric Line</strong></td>
                                                    <td><%=val.statusInfraRoadElec%></td></tr>
                                                     <tr><td style="color: black"><strong>Telecommunication Service</strong></td>
                                                    <td><%=val.statusInfraTele%></td></tr>
                                                      <tr><td style="color: black"><strong>Health</strong></td>
                                                    <td><%=val.statusInfraHealth%></td></tr>
                                                       <tr><td style="color: black"><strong>education</strong></td>
                                                    <td><%=val.statusInfraEducation%></td></tr>
                                                       <tr><td style="color: black"><strong>Closest administration office</strong></td>
                                                    <td><%=val.statusInfraAdmin%></td></tr>
                                                    
                                                  <% break;
                                                        }
                                                        %>
                                            </tbody>
                                        </table>
                                            </div>
                                            <div class="col-md-6 col-sm-18 col-xs-18">
                                            <table class="table table-borderless mtable">
                                            <thead>
                                            <th></th>
                                            <th></th>
                                            </thead>
                                            <tbody>
                                                <%
                                                LandInfo.LandBankProfile retLandPro11 = Land.getLandBankProfile();
                                                for(LandInfo.LandBankProfile.LandBankProfileData val: retLandPro11.data){
                                                %>
                                            
                                              
                                                 <tr class="bg-primary"><td><i class="fa fa-square" aria-hidden="true"style="color: goldenrod"></i> Land Use</td><td></td></tr>
                                                     <tr><td style="color: black"><strong>Land use</strong></td>
                                                    <td><%=val.landUse%></td></tr>
                                                    <tr><td style="color: black"><strong>Investment Type</strong></td>
                                                    <td><%=val.investmentType%></td></tr>
                                                    
                                                     <tr class="bg-primary"><td> <i class="fa fa-font-awesome" aria-hidden="true"style="color: goldenrod"></i> Soil Status</td><td></td></tr>
                                                    <tr><td style="color: black"><strong>Soil fertility test result</strong></td>
                                                    <td>testResult.docx</td></tr>
                                                   
                                                  <tr class="bg-primary"><td><i class="fa fa-tree" aria-hidden="true"style="color: goldenrod"></i> Agro-Ecology</td><td></td></tr>
                                                    <tr><td style="color: black"><strong>Socio-economic study</strong></td>
                                                    <td><%=val.socioEconomy%></td></tr>
                                                    <tr><td style="color: black"><strong>Agero-ecology</strong></td>
                                                    <td><%=val.ageroEcology%></td></tr>
                                                    
                                                     <tr class="bg-primary"><td><i class="fa fa-info" aria-hidden="true"style="color: goldenrod"></i> Lease Information</td><td></td></tr>
                                                      <tr><td style="color: black"><strong>Lease Type</strong></td>
                                                    <td><%=val.leaseType%></td></tr>
                                                      <tr><td style="color: black"><strong>Lease Duration</strong></td>
                                                    <td><%=val.leaseDuration%></td></tr>
                                                      <tr><td style="color: black"><strong>Lease amount per hectare</strong></td>
                                                    <td><%=val.leasePerHectare%></td></tr>
                                                     
                                                     <tr class="bg-primary"><td><i class="fa fa-cloud" aria-hidden="true"style="color: goldenrod"></i> Weather Condition and Others</td><td></td></tr>
                                                     <tr><td style="color: black"><strong>Surface and underground water potential</strong></td>
                                                    <td><%=val.surUnderWaterPote%></td></tr>
                                                     
                                                     <tr><td style="color: black"><strong>Water potential for irrigation services</strong></td>
                                                    <td><%=val.waterPote%></td></tr>
                                                      <tr><td style="color: black"><strong>Wind directions and speed</strong></td>
                                                    <td><%=val.windDirecSpeed%></td></tr>
                                                     <tr><td style="color: black"><strong>Rainfall distribution</strong></td>
                                                    <td><%=val.rainfallDist%></td></tr>
                                                     
                                                     <tr><td style="color: black"><strong>Atmospheric moisture content(Humidity)</strong></td>
                                                    <td><%=val.humudity%></td></tr>
                                                     
                                                      <tr><td style="color: black"><strong>The height above the sea level</strong></td>
                                                    <td><%=val.height%></td></tr>
                                                    
                                                     <tr><td style="color: black"><strong>Cover of herbs</strong></td>
                                                    <td><%=val.coverOfHerbs%></td></tr>
                                                     <tr><td style="color: black"><strong>Landscape</strong></td>
                                                    <td><%=val.landscape%></td></tr>
                                                     <tr><td style="color: black"><strong>Others</strong></td>
                                                    <td><%=val.others%></td></tr>
                                                <%
                                                    break;
            
                                                            }%>
                                            </tbody>
                                           
                                            </table>
                                    </div>
                                        </div>
                                   
                                    
                                      
                               
                                     <div id="invsup" class="tabcontent">
                                          <div class="panel panel-group" style="margin-top: 50pt">
                                    <div class="panel panel-primary">
                                    
                                        <div class="panel panel-heading">   
                                            <font color="white"><i class="fa fa-shopping-bag" aria-hidden="true"style="color: goldenrod"></i> Duty free Privileges</font>
                                        </div>
                                        <div class="panel panel-body">
                                            <table class="table table-striped jambo_table bulk_action table-bordered">
                                                <thead>
                                                    <tr><td rowspan="2">Types of Privilege</td><td colspan="3">Status of use</td><td rowspan="2">Reason</td></tr>
                                                    <tr><td>Project use</td><td>Out of order</td><td>Used for intended purpose</td></tr>
                                                </thead>
                                    <tr><td>duty free import</td><td>15</td><td>2</td><td>13</td><td>To farm the land</td></tr>
                                    <tr><td>duty free export</td><td>15</td><td>2</td><td>13</td><td>To earn money</td></tr>
                                            </table>
                                    
                                        </div>
                                    </div>
                                              
                                              <div class="panel panel-primary">
                                    
                                        <div class="panel panel-heading">    
                                            <font color="white"><i class="fa fa-shopping-basket" aria-hidden="true"style="color: goldenrod"></i> Incentives</font>
                                        </div>
                                        <div class="panel panel-body">
                                            <table >
                                                
                                    <tr><td style="color: black"><strong>Investment Incentive Provided:</strong></td><td style="padding-left:50pt">duty free import</td></tr>
                                    <tr><td style="color: black"><strong>Technical Support at field level:</strong></td><td style="padding-left:50pt">Not Required</td></tr>
                                    <tr><td style="color: black"><strong>Grace Period:</strong></td><td style="padding-left:50pt">1 Year</td></tr>
                                    <tr><td style="color: black"><strong>Competence assurance support:</strong></td><td style="padding-left:50pt">Complete</td></tr>
                                    <tr><td style="color: black"><strong>Work Permit Renewal:</strong></td><td style="padding-left:50pt">Renewed</td></tr>
                                    <tr><td style="color: black"><strong>Administrative support:</strong></td><td style="padding-left:50pt">Not Required</td></tr>
                                            </table>
                                    
                                        </div>
                                    </div>
                                              
                                              
                                              
                                              
                                          </div>
                                </div>
                                 <div id="lease" class="tabcontent">
                                    <div class="panel panel-group" style="margin-top: 50pt">
                                    <div class="panel panel-primary">
                                    
                                        <div class="panel panel-heading">   
                                            <font color="white"><i class="fa fa-book" aria-hidden="true"style="color: goldenrod"></i>  </font>
                                        </div>
                                        <div class="panel panel-body">
                                     <div class="col-md-2 col-md-offset-0 menu_fixed" >
                                        
                                         <a style="color:blue" href="assets/files/Lease.pdf">Download Lease Document</a> 
                                         
                                        </div>
                                
                                    
                                        </div>
                                    </div>
                                  </div>
                                 </div>
                                <div id="cadaster" class="tabcontent">
                                    <div class="panel panel-group" style="margin-top: 50pt">
                                    <div class="panel panel-primary">
                                    
                                        <div class="panel panel-heading">   
                                            <font color="white"> <i class="fa fa-map-o" aria-hidden="true"style="color: goldenrod"></i> Cadaster</font>
                                        </div>
                                        <div class="panel panel-body">
                                     <div class="col-md-2 col-md-offset-0 menu_fixed" >
                                         <a href="assets/images/map.png"><image height="600px" width="600px" src="assets/images/map.png"/> </a>
                                        
                                        <a href="assets/images/map.png" download="assets/images/map.png"><font color="blue" >Download Cadaster</font></a> 
                                        </div>
                                    
                                    
                                        </div>
                                    </div>
                                        <div class="panel panel-primary">
                                    
                                        <div class="panel panel-heading">   
                                            <font color="white"><i class="fa fa-book" aria-hidden="true"style="color: goldenrod"></i> Green Book</font>
                                        </div>
                                        <div class="panel panel-body">
                                     <div class="col-md-6" >
                                         <a href="assets/images/Image39.jpg"> <image height="500px" width="600px" src="assets/images/Image39.jpg"/></a>
                                        
                                        
                                        </div>
                                    <div class="col-md-6 col-md-offset-0 menu_fixed" >
                                        <a href="assets/images/Image40.jpg"> <image height="500px" width="600px" src="assets/images/Image40.jpg"/> </a>
                                     
                                        
                                        </div>
                                    
                                        </div>
                                    </div>
                                </div>
                                    
                                  </div>
                                <div id="pdetail" class="tabcontent">
                                   
                                    <div class="panel panel-group" style="margin-top: 50pt">
                                    <div class="panel panel-primary">
                                    
                                        <div class="panel panel-heading">   
                                            <font color="white"><i class="fa fa-product-hunt" aria-hidden="true"style="color: goldenrod"></i> Project</font>
                                        </div>
                                        <div class="panel panel-body">
                                            <table>
                                    <tr><td style="color: black"><strong> Project Name: </strong></td><td style="padding-left:50pt">Project one</td></tr>
                                    <tr><td style="color: black"><strong>Project Type: </strong></td><td style="padding-left:50pt">Agriculture</td></tr>
                                    <tr><td style="color: black"><strong>Brief Description of the project: </strong></td><td style="padding-left:50pt">Farming and providing for both internal and external markets</td></tr>
                                            </table>
                                    
                                        </div>
                                    </div>
                                        <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"> <i class="fa fa-map-marker" aria-hidden="true"style="color: goldenrod"></i> Project location</div>
                                            
                                        <div class="panel panel-body">
                                            <table>
                                            <tr><td style="color: black"><strong>Region : </strong></td><td style="padding-left:50pt">Tigray</td></tr>
                                            <tr><td style="color: black"><strong>Woreda :</strong></td><td style="padding-left:50pt"> 12</td></tr>
                                            <tr><td style="color: black"><strong>Zone :</strong></td><td style="padding-left:50pt">04</td></tr>
                                            <tr><td style="color: black"><strong>Kebele :</strong></td><td style="padding-left:50pt">3</td></tr>
                                            </table>
                                        </div>
                                        </div>
                                    
                                         <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"><i class="fa fa-plus-circle" aria-hidden="true"style="color: goldenrod"></i> Total Investment Cost (Birr)</div>
                                            
                                        <div class="panel panel-body">
                                            <table>
                                            <tr><td style="color: black"><strong>Land :</strong></td><td style="padding-left:50pt">25000</td></tr>
                                            <tr><td style="color: black"><strong> Building and Civil works :</strong></td><td style="padding-left:50pt">50000</td></tr>
                                            <tr><td style="color: black"><strong> Machinery/Equipment :</strong></td><td style="padding-left:50pt">30000</td></tr>
                                            <tr><td style="color: black"><strong> Other Fixed Capital cost :</strong></td><td style="padding-left:50pt">10000</td></tr>
                                            <tr><td style="color: black"><strong> Initial Working Capital :</strong></td><td style="padding-left:50pt">10000</td></tr>
                                            <tr><td style="color: black"><strong>Total :</strong></td><td style="padding-left:50pt">100000</td></tr>
                                            </table>
                                        </div>
                                        </div>
                                         <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"><i class="fa fa-plus-square" aria-hidden="true"style="color: goldenrod"></i> Infrastructure Development</div>
                                            
                                        <div class="panel panel-body">
                                            <table>
                                            <tr><td style="color: black"><strong>Road : </strong></td><td style="padding-left:50pt">Asphalt</td></tr>
                                            <tr><td style="color: black"><strong>Electricity :</strong></td><td style="padding-left:50pt">Yes</td></tr>
                                            <tr><td style="color: black"><strong>Tel communication :</strong></td><td style="padding-left:50pt">Yes</td></tr>
                                            <tr><td style="color: black"><strong>Others :</strong></td><td style="padding-left:50pt">Water supply provided</td></tr>
                                            </table>
                                        </div>
                                        </div>
                                        <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"><i class="fa fa-tree" aria-hidden="true"style="color: goldenrod"></i> Environmental Management</div>
                                            
                                        <div class="panel panel-body">
                                            <table>
                                            <tr><td style="color: black"><strong>Forest Management : </strong></td><td style="padding-left:50pt">well protected</td></tr>
                                            <tr><td style="color: black"><strong>Soil and Water Conservation :</strong></td><td style="padding-left:50pt">properly handled</td></tr>
                                            <tr><td style="color: black"><strong>Wildlife Protection :</strong></td><td style="padding-left:50pt">No wildlife around</td></tr>
                                            </table>
                                        </div>
                                        </div>  
                                        
                                        
                                        <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"><i class="fa fa-credit-card-alt" aria-hidden="true"style="color: goldenrod"></i> Source of Finance (Total in Birr)</div>
                                            
                                        <div class="panel panel-body">
                                            <table>
                                            <tr><td style="color: black"><strong>Equity : </strong></td><td style="padding-left:50pt">5000</td></tr>
                                            <tr><td style="color: black"><strong>Loan :</strong></td><td style="padding-left:50pt">2000</td></tr>
                                            <tr><td style="color: black"><strong>Another Source :</strong></td><td style="padding-left:50pt">3000</td></tr>
                                            <tr><td style="color: black"><strong>Total :</strong></td><td style="padding-left:50pt">10000</td></tr>
                                            </table>
                                        </div>
                                        </div>  
                                         <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"><i class="fa fa-dollar" aria-hidden="true"style="color: goldenrod"></i> Employment Opportunity</div>
                                            
                                        <div class="panel panel-body">
                                            <table class="table table-striped jambo_table bulk_action table-bordered">
                                                <thead>
                                                    <tr><td rowspan="2"></td><td colspan="2">Temporary Employees</td><td colspan="2">Permanent Employees</td><td rowspan="2"> Total</td><td rowspan="2">Result</td></tr>
                                                    <tr><td>Male</td><td>Female</td><td>Male</td><td>Female</td></tr>
                                                </thead>
                                                
                                                   
                                                <tr><td>Local</td><td>10</td><td>40</td><td>20</td><td>25</td><td>95</td><td>10</td></tr>
                                                <tr><td>Expatriate</td><td>10</td><td>20</td><td>30</td><td>40</td><td>100</td><td>9</td></tr>
                                            <tr><td>Total</td><td>20</td><td>60</td><td>50</td><td>65</td><td>195</td><td>9</td></tr>
                                            
                                            </table>
                                            <br><tr><td style="color: black"><strong>information if the project creates opportunity for unprivileged group :</strong></td><td style="padding-left:50pt"></td></tr>
                                        </div>
                                        </div>  
                                         <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"> <i class="fa fa-square" aria-hidden="true"style="color: goldenrod"></i> Land Requirement (in Sq. M or Ha.)</div>
                                            
                                        <div class="panel panel-body">
                                            <table>
                                                <tr><td style="color: black"><strong>Industrial : </strong></td><td style="padding-left:50pt">2500</td></tr>
                                                <tr><td style="color: black"><strong>Agricultural :</strong> </strong></td><td style="padding-left:50pt">15000</td></tr>
                                                <tr><td style="color: black"><strong>Service : </strong> </strong></td><td style="padding-left:50pt">200</td></tr>
                                            </table>
                                        </div>
                                        </div>  
                                        <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"><i class="fa fa-check-square-o" aria-hidden="true"style="color: goldenrod"></i> Production/Service</div>
                                            
                                        <div class="panel panel-body">
                                            <table class="table table-striped jambo_table bulk_action table-bordered">
                                                <thead>
                                                    <tr><td rowspan="2">No</td><td td rowspan="2">Type of product/service</td><td colspan="2">production/service capacity</td><td colspan="2">Sales Program</td><td rowspan="2">Result</td></tr>
                                                    <tr><td>Unit</td><td>Quantity</td><td>Domestic market share</td><td>Export market share</td></tr>
                                                </thead>
                                                
                                                   
                                                <tr><td>1</td><td>fruits</td><td>48</td><td>95</td><td>87</td><td>95</td><td>9</td></tr>
                                                <tr><td>2</td><td>Vegetables</td><td>48</td><td>95</td><td>87</td><td>95</td><td>10</td></tr>
                                                
                                            
                                            </table>
                                            
                                        </div>
                                        </div>
                                        <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"> <i class="fa fa-envira" aria-hidden="true"style="color: goldenrod"></i> Raw Material Requirements</div>
                                            
                                        <div class="panel panel-body">
                                            <table class="table table-striped jambo_table bulk_action table-bordered">
                                                <thead>
                                                    <tr><td rowspan="2">No</td><td rowspan="2">Type of raw material</td><td colspan="2">Source of supply</td><td rowspan="2">Result</td></tr>
                                                    <tr><td>Local</td><td>Foreign</td></tr>
                                                </thead>
                                                
                                                   
                                                <tr><td>1</td><td>fertilizers</td><td>EFA</td><td>CFA</td><td>10</td></tr>
                                                <tr><td>2</td><td>Seeds</td><td>ESA</td><td>CSA</td><td>9</td></tr>
                                            
                                            </table>
                                            
                                        </div>
                                        </div>
                                     <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"> <i class="fa fa-truck" aria-hidden="true"style="color: goldenrod"></i> Technology Used</div>
                                            
                                        <div class="panel panel-body">
                                            <table class="table table-striped jambo_table bulk_action table-bordered">
                                                <thead>
                                                    <tr><td rowspan="2">Machinery type</td><td rowspan="2">Quantity</td><td colspan="2">Source</td><td rowspan="2"> Remark</td><td rowspan="2">Result</td></tr>
                                                    <tr><td>Rent</td><td>Bought</td></tr>
                                                </thead>
                                                
                                                   
                                                <tr><td>Tractors</td><td>12</td><td>2</td><td>10</td><td></td><td>9</td></tr>
                                                <tr><td>Water pump</td><td>2</td><td>0</td><td>2</td><td></td><td>10</td></tr>
                                            
                                            
                                            </table>
                                            
                                        </div>
                                        </div>
                                    </div>
                                    
                                    <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"> <i class="fa fa-flask" aria-hidden="true"style="color: goldenrod"></i> Chemical Used</div>
                                            
                                        <div class="panel panel-body">
                                            <table class="table table-striped jambo_table bulk_action table-bordered">
                                                <thead>
                                                    <tr><td colspan="3">Herbicide</td><td colspan="3">Pesticide</td><td colspan="3">Fungicide</td><td colspan="3"> Fertilizer</td><td rowspan="2"> Others</td><td rowspan="2">Result</td></tr>
                                                    <tr><td>Type</td><td>Quantity</td><td>Source of Supply</td><td>Type</td><td>Quantity</td><td>Source of Supply</td><td>Type</td><td>Quantity</td><td>Source of Supply</td><td>Type</td><td>Quantity</td><td>Source of Supply</td>
                                                    </tr>
                                                </thead>
                                                
                                                   
                                                <tr><td>Non selective</td><td>3</td><td>EHA</td><td>insecticides</td><td>3</td><td>EHA</td><td>organic</td><td>48</td><td>EHA</td><td>Organic</td><td>3</td><td>EHA</td><td></td><td>10</td></tr>
                                            
                                            
                                            </table>
                                            
                                        </div>
                                        </div>
                                    
                                    
                                    <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"> <i class="fa fa-cubes" aria-hidden="true"style="color: goldenrod"></i> Seeds</div>
                                            
                                        <div class="panel panel-body">
                                            <table class="table table-striped jambo_table bulk_action table-bordered">
                                                <thead>
                                                    <tr><td>Crop Type</td><td>Quantity</td><td>Source of Supply</td><td>Remark</td><td>Result</td></tr>
                                                </thead>
                                                <tbody>
                                                    <tr><td>wheat </td><td>12</td><td>EHA</td><td></td><td>8</td></tr>
                                                    <tr><td>vegetables </td><td>18</td><td>EHA</td><td></td><td>10</td></tr>
                                                    <tr><td>Teff </td><td>97</td><td>EHA</td><td></td><td>10</td></tr>
                                                <tbody>                     
                                            </table>
                                            
                                        </div>
                                        </div>
                                    <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"> <i class="fa fa-square-o" aria-hidden="true"style="color: goldenrod"></i> Farm Management and Information Handling</div>
                                            
                                        <div class="panel panel-body">
                                            <table>
                                                
                                                <tr><td style="color: black"><strong>Frequency of Plowing before sowing </strong></td><td style="padding-left:50pt">5 times</td></tr>
                                                <tr><td style="color: black"><strong>Sowing Method </strong></td><td style="padding-left:50pt">line sowing</td></tr>
                                                <tr><td style="color: black"><strong>Fertilizer Quantity and Application Method </strong></td><td style="padding-left:50pt">placement</td></tr>
                                                <tr><td style="color: black"><strong>Water Management: </strong></td><td style="padding-left:50pt">efficient</td></tr>
                                                <tr><td style="color: black"><strong>Training/Technical Support </strong></td><td style="padding-left:50pt">no technical support given</td></tr>
                                                                 
                                            </table>
                                            
                                        </div>
                                        </div>
                                    <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"> <i class="fa fa-bed" aria-hidden="true"style="color: goldenrod"></i> Camp Infrastructure</div>
                                            
                                        <div class="panel panel-body">
                                            <table>
                                                
                                                <tr><td style="color: black"><strong>Worker residential house </strong></td><td style="padding-left:50pt"> huts</td></tr>
                                                <tr><td style="color: black"><strong>Water supply </strong></td><td style="padding-left:50pt">good</td></tr>
                                                <tr><td style="color: black"><strong>Medical facility </strong></td><td style="padding-left:50pt">first level clinic</td></tr>
                                                <tr><td style="color: black"><strong>Toilets and bath rooms </strong></td><td style="padding-left:50pt">two provided for each</td></tr>
                                                <tr><td style="color: black"><strong>Kitchen facilities </strong></td><td style="padding-left:50pt">provided for each</td></tr>
                                                <tr><td style="color: black"><strong>Dining hall </strong></td><td style="padding-left:50pt"></td></tr>                 
                                                <tr><td style="color: black"><strong>Refreshment and entertainment facility </strong></td><td style="padding-left:50pt">restaurant</td></tr>                 
                                                <tr><td style="color: black"><strong>Consumable items supply </strong></td><td style="padding-left:50pt">department store</td></tr>                 
                                                <tr><td style="color: black"><strong>Food supply </strong></td><td style="padding-left:50pt">department store</td></tr>                 
                                                <tr><td style="color: black"><strong>Safety outfits supplies </strong></td><td style="padding-left:50pt">two provided for each</td></tr>                 
                                            </table>
                                            
                                        </div>
                                        </div>
                                    <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"> <i class="fa fa-check" aria-hidden="true"style="color: goldenrod"></i> Creating good working environment for workers</div>
                                            
                                        <div class="panel panel-body">
                                            <table class="table table-striped jambo_table bulk_action table-bordered">
                                                
                                                <thead>
                                                    <tr><td rowspan =2>Office building</td><td rowspan="2">Machinery Shed</td><td colspan="4">Store</td><td rowspan="2">Result</td></tr>
                                                    <tr><td>production</td><td>chemicals</td><td>Fertilizers</td><td>Others</td></tr>
                                                </thead>
                                                <tr><td>3 office buildings</td><td>2 machinery sheds</td><td>5 production stores</td><td>2 chemical stores</td><td>5 fertilizer stores</td><td></td><td>10</td></tr>
                                                
                                            </table>
                                            
                                        </div>
                                        </div>
                                    <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"> <i class="fa fa-child" aria-hidden="true"style="color: goldenrod"></i> Employment</div>
                                            
                                        <div class="panel panel-body">
                                            <table class="table table-striped jambo_table bulk_action table-bordered">
                                                
                                                <thead>
                                                    <tr><td>Professionals</td><td>Quantity</td><td>Result</td></tr>
                                                </thead>
                                                <tr><td>General Manager</td><td>1</td><td>9</td></tr>
                                                <tr><td>Deputy Manager</td><td>3</td><td>10</td></tr>
                                                <tr><td>Agronomist</td><td>5</td><td>10</td>
                                                </tr>
                                            </table>
                                            
                                        </div>
                                        </div>
                                    <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"> <i class="fa fa-bullhorn" aria-hidden="true"style="color: goldenrod"></i> Social Contribution</div>
                                            
                                        <div class="panel panel-body">
                                            <table >
                                                
                                                <tr><td style="color: black"><strong>School</strong></td><td style="padding-left:50pt">1 school</td></tr>
                                                <tr><td style="color: black"><strong>Hospital</strong></td><td style="padding-left:50pt"></td></tr>
                                                <tr><td style="color: black"><strong>Road</strong></td><td style="padding-left:50pt">1 second level road</td></tr>
                                                <tr><td style="color: black"><strong>Water Facility</strong></td><td style="padding-left:50pt"></td></tr>
                                            </table>
                                            
                                        </div>
                                        </div>
                                      <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"> <i class="fa fa-circle-o-notch" aria-hidden="true"style="color: goldenrod"></i> Project Bottleneck</div>
                                            
                                        <div class="panel panel-body">
                                            <table class="table table-striped jambo_table bulk_action table-bordered">
                                                <thead>
                                                    <tr>
                                                        <td>Type</td>
                                                        <td>Remark</td>
                                                    </tr>
                                                </thead>
                                                    
                                                <tr><td>Infrastructure</td><td> No road</td></tr>
                                                <tr><td>Technology</td><td></td></tr>
                                                <tr><td>Training</td><td>No training taken</td></tr>
                                                <tr><td>Supply</td><td>no supply and demand relation created</td></tr>
                                                <tr><td>Quality Seed</td><td>no quality seed supplied</td></tr>
                                                <tr><td>Man Power</td><td>lack of agricultural irrigation experts</td></tr>
                                                <tr><td>Others</td><td></td></tr>
                                                
                                                
                                                
                                            </table>
                                            
                                        </div>
                                        </div>
                                     <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"> <i class="fa fa-plus-square" aria-hidden="true"style="color: goldenrod"></i> Project assessment total result
</div>
                                            
                                        <div class="panel panel-body">
                                            <table >
                                                
                                                <tr><td style="color: black"><strong>Result</strong></td><td style="padding-left:50pt">A</td></tr>
                                                
                                                
                                            </table>
                                            
                                        </div>
                                        </div>
                                    <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"> <i class="fa fa-expand" aria-hidden="true"style="color: goldenrod"></i> Expansion Detail</div>
                                            
                                        <div class="panel panel-body">
                                            <table >
                                                
                                                <tr><td style="color: black"><strong>Detail Expansion Request</strong></td><td style="padding-left:50pt">Expansion Request.pdf</td></tr>
                                                <tr><td style="color: black"><strong>Area of land transferred for expansion</strong></td><td style="padding-left:50pt">500 sq.M</td></tr>
                                                
                                            </table>
                                            
                                        </div>
                                        </div>
                                   
                                    
                                    
                                    
                                    
                                    
                                    </div>
                                    <div id="nott" class="tabcontent">
                                    <div class="panel panel-group" style="margin-top: 50pt">
                                        <div class="panel panel-primary">
                                            <div class="panel panel-heading" style="color:white"><i class="fa fa-bell-o" aria-hidden="true"style="color: goldenrod"></i></div>
                                            <div class="panel panel-body">
                                                <div class="container">
  
  <ul class="nav nav-tabs" style>
      <li class="active" style="width:200pt"><a data-toggle="tab" href="#Promotion"><i class="fa fa-tags" aria-hidden="true"></i> Promotion</a></li>
      <li style="width:200pt"><a data-toggle="tab" href="#Bid"><i class="fa fa-envelope-open" aria-hidden="true"></i> Bid</a></li>
    <li style="width:200pt"><a data-toggle="tab" href="#Training"><i class="fa fa-male" aria-hidden="true"></i> Training</a></li>
    <li style="width:200pt"><a data-toggle="tab" href="#Warning" ><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Warning</a></li>
    <li style="width:200pt"><a data-toggle="tab" href="#Cert"><i class="fa fa-file-text" aria-hidden="true"></i> Certification</a></li>
    <li style="width:200pt"><a data-toggle="tab" href="#Others"><i class="fa fa-id-card" aria-hidden="true"></i> Others</a></li>
  </ul>

  <div class="tab-content">
    <div id="Promotion" class="tab-pane fade in active">
      
      <table class="table table-striped table-bordered table-hover table-condensed">
      <tr><td>Promotion notification</td></tr>
      <tr><td>Promotion notification</td></tr>
      <tr><td>Promotion notification</td></tr>
      </table>
    </div>
    <div id="Bid" class="tab-pane fade">
        <script>
            
            
            console.log("Bid");
        </script>
      
       <table class="table table-striped table-bordered table-hover table-condensed">
      <tr><td>Bid notification</td></tr>
      <tr><td>Bid notification</td></tr>
      <tr><td>Bid notification</td></tr>
      </table>
    </div>
    <div id="Training" class="tab-pane fade">
      
       <table class="table table-striped table-bordered table-hover table-condensed">
      <tr><td>Training notification</td></tr>
      <tr><td>Training notification</td></tr>
      <tr><td>Training notification</td></tr>
      </table>
    </div>
      <div id="Warning" class="tab-pane fade">
      
       <table class="table table-striped table-bordered table-hover table-condensed">
      <tr><td>Warning notification</td></tr>
      <tr><td>Warning notification</td></tr>
      <tr><td>Warning notification</td></tr>
      </table>
    </div>
      <div id="Cert" class="tab-pane fade">
      
      <table class="table table-striped table-bordered table-hover table-condensed">
      <tr><td>Certification notification</td></tr>
      <tr><td>Certification notification</td></tr>
      <tr><td>Certification notification</td></tr>
      </table>
    </div>
    <div id="Others" class="tab-pane fade">
      
      <table class="table table-striped table-bordered table-hover table-condensed">
      <tr><td>Others notification</td></tr>
      <tr><td>Others notification</td></tr>
      <tr><td>Others notification</td></tr>
      </table>
    </div>
  </div>
</div>

                                                
                                                
                                            </div>
                                            
                                        </div>
                                        
                                    </div>
                                    
                                    
                                  </div>
                            </div>
                 <div id="project2" class="tab-pane">
      
      
   
                                <div class="tab col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <button class="tablinks" onclick="openTab(event, 'Details2')">Company/ Investor Profile</button>
                                    <button class="tablinks" onclick="openTab(event, 'Document2')">Document</button>
                                    <button class="tablinks" onclick="openTab(event, 'lease2')">Lease Agreement</button>
                                    <button class="tablinks" onclick="openTab(event, 'cadaster2')">Cadaster Information</button>
                                    <button class="tablinks" onclick="openTab(event, 'PromotedLand2')">Promoted Land Information</button>
                                    <button class="tablinks" onclick="openTab(event, 'pdetail2')">Project Details</button>
                                    <button class="tablinks" onclick="openTab(event, 'invsup2')">Investment Support</button>
                                    <button class="tablinks" onclick="openTab(event, 'nott2')">Notifications</button>
                                </div>
                                
                                
                                <div id="Details2" class="tabcontent">
                                    <div class="col-md-8">
                                    <table class=" col-md-8 table table-borderless mtable" >
                                            <%
                                                InvestorInfo.investerAssesDetails retDetails2 = Investor.getInvestorD();
                                                for(InvestorInfo.investerAssesDetails.investorDetails val:retDetails2.ID){
                                                %>
                                            <tr class="bg-primary"> <td><font color="white"><i class="fa fa-align-justify" aria-hidden="true"style="color: goldenrod"></i><strong>  Investor Details :</strong> </strong></td><td></td></tr>
                                            <tr><td style="color: black"><strong>  Company's name :</font></strong></td><td><%=val.Company%></td></tr>
                                            <tr><td style="color: black"><strong>Company's owner name :</strong></td><td><%=val.Owner%></td></tr>
                                            <tr><td style="color: black"><strong>Company Type : </strong></td><td><%=val.CType%></td></tr>
                                            
                                            <tr><td style="color: black"><strong> Nationality / Country of incorporation, if company :</strong></td><td><%=val.Nationality%> </td></tr>
                                            <tr><td style="color: black"><strong>Tax Identification no. :</strong></td><td><%=val.taxid%></td> </tr>
                                            <tr><td style="color: black"><strong>Commercial Registration No. </strong></td><td><%=val.comreg%></td> </tr>
                                            
                                            <tr><td style="color: black"><strong>Investor type : </strong></td><td><%=val.IType%></td></tr>
                                            <tr><td style="color: black"><strong>Investment permit no : </strong></td><td><%=val.IPNo%></td></tr>
                                            
                                           
                                            
                                            <tr class="bg-primary"><td><i class="fa fa-address-book" aria-hidden="true"style="color: goldenrod"></i> <strong><font color="white"><strong>  Address :</strong> </td> <td  ></td></tr>
                                            <tr><td style="color: black"><strong>Region :</strong></td><td><%=val.Region%> </td></tr>
                                            <tr><td style="color: black"><strong>Zone :</strong></td><td><%=val.Zone%> </td></tr>
                                            <tr><td style="color: black"><strong>City :</strong></td><td><%=val.City%> </td></tr>
                                            <tr><td style="color: black"><strong>Sub-city/ Woreda :</strong></td><td><%=val.Woreda%> </td></tr>
                                            
                                            <tr><td style="color: black"><strong>Kebele :</td>  <td><%=val.kebele%> </td></tr>
                                            <tr><td style="color: black"><strong>House No.</strong></td><td><%=val.HouseNo%></td> </tr>
                                            <tr><td style="color: black"><strong>Telephone No.( Land line)</td>  <td><%=val.Telephone%> </td> </tr>
                                            <tr><td style="color: black"><strong>Address :</strong></td><td><%=val.Address%> </td></tr>
                                            <tr><td style="color: black"><strong>Post</strong></td><td><%=val.post%></td></tr>
                                            <tr><td style="color: black"><strong>Street Address</strong></td><td><%=val.sa%></td></tr>
                                            <tr><td style="color: black"><strong>Emergency Contact</strong></td><td><%=val.ec%></td></tr>
                                            
                                            <tr><td style="color: black"><strong>Phone :</strong></td><td><%=val.Phone%> </td></tr>
                                            <tr><td style="color: black"><strong>Email :</strong></td><td><%=val.Email%> </td></tr>
                                            <tr><td style="color: black"><strong>Fax : </strong></td><td> <%=val.Fax%> </td></tr>
                                            
                                             <tr class="bg-primary"><td  ><font color="white"><strong><i class="fa fa-money" aria-hidden="true"style="color: goldenrod"></i>  Finance :</strong> </td> <td  ></td></tr>
                                            <tr><td style="color: black"><strong>Starting capital :</strong></td><td><%=val.SCapital%> </td></tr>
                                            <tr><td style="color: black"><strong>Current capital :</strong></td><td><%=val.CCapital%> </td></tr>
                                            <tr><td style="color: black"><strong>Form of ownership : </strong></td><td><%=val.formofownership%></td> </tr>
                                            <tr><td style="color: black"><strong>Type of ownership :</strong></td><td><%=val.towner%> </td> </tr>
                                         
                                            <%}%>
                                     </table>
                                    </div>
                                     <div class="col-md-2 col-md-offset-2 menu_fixed" >
                                        <image height="140px" width="140px" src="assets/images/mal.jpg"/>
                                        </div>
                                     
                                     
                                  </div>
                                     

                                  <div id="Document2" class="tabcontent">
                                    <h3> <font color="black">Full Investor File </font> </h3>
                                      
                                      <div class="panel panel-primary">
                                      <div class="panel-heading"><font color="white"><i class="fa fa-files-o" aria-hidden="true"style="color: goldenrod"></i> Files </font></div>
                                      <div class="panel-body">    
                                          <table>
                                               <%
                                                InvestorInfo.investordocuments retinvdocs2 = Investor.getinvestordocs();
                                                for(InvestorInfo.investordocuments.investordocs val:retinvdocs2.ids){
                                                %>
                                        <tr><td style="color: black"><strong>Power of Attorney : </strong></td><td style="padding-left:50pt"><%=val.poa%></td></tr>
                                        <tr><td style="color: black"><strong>Memorandum (if organization) : </strong></td><td style="padding-left:50pt"><%=val.memorandum%></td></tr>
                                        <tr><td style="color: black"><strong>Regulation of establishment (if public enterprise) : </strong></td><td style="padding-left:50pt"><%=val.roe %></td></tr>                                             
                                        <tr><td style="color: black"><strong>Articles of Association (if cooperative society) : </strong></td><td style="padding-left:50pt"><%=val.aoa %></td></tr>
                                       <tr><td style="color: black"><strong>Support letter for Lease Applications :</strong></td><td style="padding-left:50pt"><%=val.slla %></td></tr> 
                                       
                                       <tr><td style="color: black"><strong>Agricultural Investment Land Identification Document :</strong></td><td style="padding-left:50pt"><%=val.ailid %></td></tr>
                                       
                                       <tr><td style="color: black"><strong>Land Transfer Letter :</strong></td><td style="padding-left:50pt"><%=val.landtl %></td></tr>
                                       
                                       <tr><td style="color: black"><strong>Work permit :</strong></td><td style="padding-left:50pt"><%=val.workpermit %></td></tr>
                                       
                                       <tr><td style="color: black"><strong>Certificate for land use by Regional State Environment, Forest & Land Administration Bureau :</strong></td><td style="padding-left:50pt"><%=val.cflrs %></td></tr>
                                       
                                       
                                       
                                       <tr><td style="color: black"><strong>Profit or Loss Balance from Business :</strong></td><td style="padding-left:50pt"><%=val.profitorloss %></td></tr>
                                       
                                       <tr><td style="color: black"><strong>Organization Law : </strong></td><td style="padding-left:50pt"><%=val.organiztionlaw %></td></tr>
                                       
                                       <tr><td style="color: black"><strong>Meeting Minutes :</strong></td><td style="padding-left:50pt"><%=val.meetingminutes %></td></tr>
                                       <tr><td style="color: black"><strong> ID (if individual) :</strong></td><td style="padding-left:50pt"><%=val.id%></td></tr>
                                       <%}%>
                                       
                                       </table>
                                              </div>
                                      </div>
                                  
                                  
                                    <div class="panel panel-primary">
                                        <div class="panel panel-heading"><font color="white"><i class="fa fa-users" aria-hidden="true"style="color: goldenrod"></i> Foreign Investor </font></div>
                                        <div class="panel panel-body"> 
                                            <table>
                                                <%
                                                InvestorInfo.investordocuments retinvdocs2f = Investor.getinvestordocs();
                                                for(InvestorInfo.investordocuments.investordocs val:retinvdocs2f.ids){
                                                %>
                                           <tr><td style="color: black"><strong> Passport (if individual) : </strong></td><td style="padding-left:50pt"><%=val.passport%></td></tr>
                                           <tr><td style="color: black"><strong>   Letter from Federal Democratic Republic of Ethiopia Ministry of Foreign Affairs for Diaspora Association :</strong></td><td style="padding-left:50pt"><%=val.lfdremofa%></td></tr>
                                       
                                      <tr><td style="color: black"><strong> Support letter to Invest as Diaspora from Foreign Minister Investor for Diaspora Activity Directorate General :</strong></td><td style="padding-left:50pt"><%=val.supportletter%></td></tr>
                                            <%}%>
                                            </table>
                                        </div> 
                                    </div>
                                    <div class="panel panel-primary">
                                      <div class="panel panel-heading"><font color="white" ><i class="fa fa-expand" aria-hidden="true"style="color: goldenrod"></i> For Expansion or Upgrading of an existing enterprise</font></div>
                                       <div class="panel panel-body"> 
                                           <table>
                                                <%
                                                InvestorInfo.investordocuments retinvdocs2b = Investor.getinvestordocs();
                                                for(InvestorInfo.investordocuments.investordocs val:retinvdocs2b.ids){
                                                %>
                                            <tr><td style="color: black"><strong>  business license : <br> </strong></td><td style="padding-left:50pt"><%=val.businesslicence%></td></tr>
                                            <%}%>
                                           </table>
                                       </div>   
                                    </div>
                                    <div class="panel panel-primary">
                                      <div class="panel panel-heading"><font color="white" ><i class="fa fa-file" aria-hidden="true"style="color: goldenrod"></i> Lease Required Documents</font></div>
                                       <div class="panel panel-body"> 
                                           <table>
                                                <%
                                                InvestorInfo.investordocuments retinvdocs2l = Investor.getinvestordocs();
                                                for(InvestorInfo.investordocuments.investordocs val:retinvdocs2l.ids){
                                                %>
                                            <tr><td style="color: black"><strong>  business plan : <br> </strong></td><td style="padding-left:50pt"><%=val.businessplan%></td></tr>
                                            <tr><td style="color: black"><strong>  The site plan of the leased land : <br> </strong></td><td style="padding-left:50pt"><%=val.siteplanofleaseland%></td></tr>
                                            <tr><td style="color: black"><strong>  Leased land delivery minute :<br> </strong></td><td style="padding-left:50pt"><%=val.leasedlanddeliveryminute%></td></tr>
                                            <tr><td style="color: black"><strong>  Environment impact assessment document : <br> </strong></td><td style="padding-left:50pt"><%=val.eiad%></td></tr>
                                            <tr><td style="color: black"><strong>  Performance reports. : <br> </strong></td><td style="padding-left:50pt"><%=val.performancereports%></td></tr>
                                            <tr><td style="color: black"><strong>  Closure plan : <br> </strong></td><td style="padding-left:50pt"><%=val.closureplan%></td></tr>
                                            <tr><td style="color: black"><strong>  Community agreement (social contract): <br> </strong></td><td style="padding-left:50pt"><%=val.communityagree%></td></tr>
                                            <tr><td style="color: black"><strong>  Land request form : <br> </strong></td><td style="padding-left:50pt"><%=val.landreqform%></td></tr>
                                            <%}%>
                                           </table>
                                       </div>   
                                    </div>
                                    <div class="panel panel-primary">
                                      <div class="panel panel-heading"><font color="white" > <i class="fa fa-archive" aria-hidden="true"style="color: goldenrod"></i> Miscellaneous </font></div>
                                       <div class="panel panel-body"> 
                                           <table>
                                                <%
                                                InvestorInfo.investordocuments retinvdocs2m = Investor.getinvestordocs();
                                                for(InvestorInfo.investordocuments.investordocs val:retinvdocs2m.ids){
                                                %>
                                            <tr><td style="color: black"><strong>  Crop Calendar : <br> </strong></td><td style="padding-left:50pt"><%=val.cropcalander%></td></tr>
                                            <tr><td style="color: black"><strong>  Employee Contract Document: <br> </strong></td><td style="padding-left:50pt"><%=val.employeecontractdoc%></td></tr>
                                            <tr><td style="color: black"><strong>  Internal Rule and regulation document :<br> </strong></td><td style="padding-left:50pt"><%=val.internalruleandregdoc%></td></tr>
                                            <tr><td style="color: black"><strong>  Record of annual crop infestations and spread of disease and measures taken : <br> </strong></td><td style="padding-left:50pt"><%=val.raciasdmt%></td></tr> 
                                            <tr><td style="color: black"><strong> Investor investment track record document</strong></td><td style="padding-left:50pt"><%=val.iitrd%></td></tr>
                                            <%}%>
                                           </table>
                                       </div>   
                                    </div>
                                    
                                    
                                    
                                  </div>
                                  <div id="PromotedLand2" class="tabcontent">
                                      <div class="col-md-6 col-sm-18 col-xs-18">
                                    <table class="table table-borderless mtable">
                                            <thead>
                                            <th></th>
                                            <th></th>
                                            </thead>
                                            <tbody>
                                                <%
                                                LandInfo.LandBankProfile retLandPro2 = Land.getLandBankProfile();
                                                for(LandInfo.LandBankProfile.LandBankProfileData val: retLandPro2.data){
                                                %>
                                                <tr class="bg-primary"><td><i class="fa fa-address-card" aria-hidden="true"style="color: goldenrod"></i> Parcel ID/Block ID/Farm ID (UPID)</td><td></td></tr>
                                                <tr><td style="color: black"><strong>Parcel Id</strong></td>
                                                    <td><%=val.parcelId%></b></td>
                                                </tr>
                                                 <tr class="bg-primary"><td><i class="fa fa-address-book" aria-hidden="true"style="color: goldenrod"></i> Address</td><td></td></tr>
                                                <tr><td style="color: black"><strong>Kebele</strong></td>
                                                    <td ><%=val.kebele%></b></td>
                                                    </tr>
                                                <tr><td style="color: black"><strong>Woreda</strong></td>
                                                    <td ><%=val.woreda%></b></td></tr>
                                                
                                                <tr><td style="color: black"><strong>Zone</strong></td>
                                                <td ><%=val.zone%></td></tr>
                                                
                                                <tr><td style="color: black"><strong>Region</strong></td>
                                                <td><%=val.region%></td></tr>
                                                
                                                <tr><td style="color: black"><strong>City</strong></td>
                                                <td><%=val.city%></td></tr>
                                                
                                                <tr><td style="color: black"><strong>Address</strong></td>
                                               	<td><%=val.address%></td></tr>
                                                 <tr><td style="color: black"><strong>Distance from point of center(region capital)</strong></td>
                                                     <td><%=val.distance%></td></tr>
                                                 <tr><td style="color: black"><strong>X Coordinate</strong></td>
                                                 <td><%=val.xCordinate%></td></tr>
                                                 <tr><td style="color: black"><strong>Y Coordinate</strong></td>
                                                   <td><%=val.yCordinate%></td></tr>
                                                
                                                 <tr class="bg-primary"><td><i class="fa fa-arrows" aria-hidden="true"style="color: goldenrod"></i> Neighbor</td><td></td></tr>
                                                  <tr><td style="color: black"><strong>From North---</strong></td>
                                                    <td><%=val.neighrbourFromNorth%></td></tr>
                                                   <tr><td style="color: black"><strong>From South--</strong></td>
                                                    <td><%=val.neighrbourFromSouth%></td></tr>
                                                    <tr><td style="color: black"><strong>From East--</strong></td>
                                                    <td><%=val.neighrbourFromWest%></td></tr>
                                                     <tr><td style="color: black"><strong>From West--</strong></td>
                                                    <td><%=val.neighrbourFromEast%></td></tr>
                                                  
                                                     <tr class="bg-primary"><td><i class="fa fa-plane" aria-hidden="true"style="color: goldenrod"></i> Accessibility</td><td></td></tr>
                                                    <tr><td style="color: black"><strong>Air</strong></td>
                                                    <td><%=val.accessibilityAir%></td></tr>
                                                     <tr><td style="color: black"><strong>Road</strong></td>
                                                    <td><%=val.accessibilityRoad%></td></tr>
                                                    <tr><td style="color: black"><strong>Rail</strong></td>
                                                    <td><%=val.accessibilityRail%></td></tr>
                                                     <tr><td style="color: black"><strong>Water</strong></td>
                                                    <td><%=val.accessibilityWater%></td></tr>
                                                 
                                                  <tr class="bg-primary"><td><i class="fa fa-square-o" aria-hidden="true"style="color: goldenrod"></i> Area</td><td></td></tr>
                                                   <tr><td style="color: black"><strong>Area(hectare)</strong></td>
                                                    <td><%=val.area%></td></tr>
                                                     <tr><td style="color: black"><strong>condition of the expansion spots</strong></td>
                                                    <td><%=val.conditionExpansion%></td></tr>
                                                    
                                                    <tr class="bg-primary"><td><i class="fa fa-line-chart" aria-hidden="true"style="color: goldenrod"></i> Status of infrastructure and services</td><td></td></tr>
                                                   <tr><td style="color: black"><strong>Road-Asphalt and Electric Line</strong></td>
                                                    <td><%=val.statusInfraRoadElec%></td></tr>
                                                     <tr><td style="color: black"><strong>Telecommunication Service</strong></td>
                                                    <td><%=val.statusInfraTele%></td></tr>
                                                      <tr><td style="color: black"><strong>Health</strong></td>
                                                    <td><%=val.statusInfraHealth%></td></tr>
                                                       <tr><td style="color: black"><strong>education</strong></td>
                                                    <td><%=val.statusInfraEducation%></td></tr>
                                                       <tr><td style="color: black"><strong>Closest administration office</strong></td>
                                                    <td><%=val.statusInfraAdmin%></td></tr>
                                                    
                                                  <% break;
                                                        }
                                                        %>
                                            </tbody>
                                        </table>
                                            </div>
                                            <div class="col-md-6 col-sm-18 col-xs-18">
                                            <table class="table table-borderless mtable">
                                            <thead>
                                            <th></th>
                                            <th></th>
                                            </thead>
                                            <tbody>
                                                <%
                                                LandInfo.LandBankProfile retLandPro21 = Land.getLandBankProfile();
                                                for(LandInfo.LandBankProfile.LandBankProfileData val: retLandPro21.data){
                                                %>
                                            
                                              
                                                 <tr class="bg-primary"><td><i class="fa fa-square" aria-hidden="true"style="color: goldenrod"></i> Land Use</td><td></td></tr>
                                                     <tr><td style="color: black"><strong>Land use</strong></td>
                                                    <td><%=val.landUse%></td></tr>
                                                    <tr><td style="color: black"><strong>Investment Type</strong></td>
                                                    <td><%=val.investmentType%></td></tr>
                                                    
                                                     <tr class="bg-primary"><td> <i class="fa fa-font-awesome" aria-hidden="true"style="color: goldenrod"></i> Soil Status</td><td></td></tr>
                                                    <tr><td style="color: black"><strong>Soil fertility test result</strong></td>
                                                    <td>testResult.docx</td></tr>
                                                   
                                                  <tr class="bg-primary"><td><i class="fa fa-tree" aria-hidden="true"style="color: goldenrod"></i> Agro-Ecology</td><td></td></tr>
                                                    <tr><td style="color: black"><strong>Socio-economic study</strong></td>
                                                    <td><%=val.socioEconomy%></td></tr>
                                                    <tr><td style="color: black"><strong>Agero-ecology</strong></td>
                                                    <td><%=val.ageroEcology%></td></tr>
                                                    
                                                     <tr class="bg-primary"><td><i class="fa fa-info" aria-hidden="true"style="color: goldenrod"></i> Lease Information</td><td></td></tr>
                                                      <tr><td style="color: black"><strong>Lease Type</strong></td>
                                                    <td><%=val.leaseType%></td></tr>
                                                      <tr><td style="color: black"><strong>Lease Duration</strong></td>
                                                    <td><%=val.leaseDuration%></td></tr>
                                                      <tr><td style="color: black"><strong>Lease amount per hectare</strong></td>
                                                    <td><%=val.leasePerHectare%></td></tr>
                                                     
                                                     <tr class="bg-primary"><td><i class="fa fa-cloud" aria-hidden="true"style="color: goldenrod"></i> Weather Condition and Others</td><td></td></tr>
                                                     <tr><td style="color: black"><strong>Surface and underground water potential</strong></td>
                                                    <td><%=val.surUnderWaterPote%></td></tr>
                                                     
                                                     <tr><td style="color: black"><strong>Water potential for irrigation services</strong></td>
                                                    <td><%=val.waterPote%></td></tr>
                                                      <tr><td style="color: black"><strong>Wind directions and speed</strong></td>
                                                    <td><%=val.windDirecSpeed%></td></tr>
                                                     <tr><td style="color: black"><strong>Rainfall distribution</strong></td>
                                                    <td><%=val.rainfallDist%></td></tr>
                                                     
                                                     <tr><td style="color: black"><strong>Atmospheric moisture content(Humidity)</strong></td>
                                                    <td><%=val.humudity%></td></tr>
                                                     
                                                      <tr><td style="color: black"><strong>The height above the sea level</strong></td>
                                                    <td><%=val.height%></td></tr>
                                                    
                                                     <tr><td style="color: black"><strong>Cover of herbs</strong></td>
                                                    <td><%=val.coverOfHerbs%></td></tr>
                                                     <tr><td style="color: black"><strong>Landscape</strong></td>
                                                    <td><%=val.landscape%></td></tr>
                                                     <tr><td style="color: black"><strong>Others</strong></td>
                                                    <td><%=val.others%></td></tr>
                                                <%
                                                    break;
            
                                                            }%>
                                            </tbody>
                                           
                                            </table>
                                    </div>
                                        </div>
                                   
                                    
                                      
                               
                                     <div id="invsup2" class="tabcontent">
                                          <div class="panel panel-group" style="margin-top: 50pt">
                                    <div class="panel panel-primary">
                                    
                                        <div class="panel panel-heading">   
                                            <font color="white"><i class="fa fa-shopping-bag" aria-hidden="true"style="color: goldenrod"></i> Duty free Privileges</font>
                                        </div>
                                        <div class="panel panel-body">
                                            <table class="table table-striped jambo_table bulk_action table-bordered">
                                                <thead>
                                                    <tr><td rowspan="2">Types of Privilege</td><td colspan="3">Status of use</td><td rowspan="2">Reason</td></tr>
                                                    <tr><td>Project use</td><td>Out of order</td><td>Used for intended purpose</td></tr>
                                                </thead>
                                    <tr><td>duty free import</td><td>15</td><td>2</td><td>13</td><td>To farm the land</td></tr>
                                    <tr><td>duty free export</td><td>15</td><td>2</td><td>13</td><td>To earn money</td></tr>
                                            </table>
                                    
                                        </div>
                                    </div>
                                              
                                              <div class="panel panel-primary">
                                    
                                        <div class="panel panel-heading">    
                                            <font color="white"><i class="fa fa-shopping-basket" aria-hidden="true"style="color: goldenrod"></i> Incentives</font>
                                        </div>
                                        <div class="panel panel-body">
                                            <table >
                                                
                                    <tr><td style="color: black"><strong>Investment Incentive Provided:</strong></td><td style="padding-left:50pt">duty free import</td></tr>
                                    <tr><td style="color: black"><strong>Technical Support at field level:</strong></td><td style="padding-left:50pt">Not Required</td></tr>
                                    <tr><td style="color: black"><strong>Grace Period:</strong></td><td style="padding-left:50pt">1 Year</td></tr>
                                    <tr><td style="color: black"><strong>Competence assurance support:</strong></td><td style="padding-left:50pt">Complete</td></tr>
                                    <tr><td style="color: black"><strong>Work Permit Renewal:</strong></td><td style="padding-left:50pt">Renewed</td></tr>
                                    <tr><td style="color: black"><strong>Administrative support:</strong></td><td style="padding-left:50pt">Not Required</td></tr>
                                            </table>
                                    
                                        </div>
                                    </div>
                                              
                                              
                                              
                                              
                                          </div>
                                </div>
                                 <div id="lease2" class="tabcontent">
                                    <div class="panel panel-group" style="margin-top: 50pt">
                                    <div class="panel panel-primary">
                                    
                                        <div class="panel panel-heading">   
                                            <font color="white"><i class="fa fa-book" aria-hidden="true"style="color: goldenrod"></i>  </font>
                                        </div>
                                        <div class="panel panel-body">
                                     <div class="col-md-2 col-md-offset-0 menu_fixed" >
                                        
                                         <a style="color:blue" href="assets/files/Lease.pdf">Download Lease Document</a> 
                                         
                                        </div>
                                
                                    
                                        </div>
                                    </div>
                                  </div>
                                 </div>
                                <div id="cadaster2" class="tabcontent">
                                    <div class="panel panel-group" style="margin-top: 50pt">
                                    <div class="panel panel-primary">
                                    
                                        <div class="panel panel-heading">   
                                            <font color="white"> <i class="fa fa-map-o" aria-hidden="true"style="color: goldenrod"></i> Cadaster</font>
                                        </div>
                                        <div class="panel panel-body">
                                     <div class="col-md-2 col-md-offset-0 menu_fixed" >
                                         <a href="assets/images/map.png"><image height="600px" width="600px" src="assets/images/map.png"/> </a>
                                        
                                        <a href="assets/images/map.png" download="assets/images/map.png"><font color="blue" >Download Cadaster</font></a> 
                                        </div>
                                    
                                    
                                        </div>
                                    </div>
                                        <div class="panel panel-primary">
                                    
                                        <div class="panel panel-heading">   
                                            <font color="white"><i class="fa fa-book" aria-hidden="true"style="color: goldenrod"></i> Green Book</font>
                                        </div>
                                        <div class="panel panel-body">
                                     <div class="col-md-6" >
                                         <a href="assets/images/Image39.jpg"> <image height="500px" width="600px" src="assets/images/Image39.jpg"/></a>
                                        
                                        
                                        </div>
                                    <div class="col-md-6 col-md-offset-0 menu_fixed" >
                                        <a href="assets/images/Image40.jpg"> <image height="500px" width="600px" src="assets/images/Image40.jpg"/> </a>
                                     
                                        
                                        </div>
                                    
                                        </div>
                                    </div>
                                </div>
                                    
                                  </div>
                                <div id="pdetail2" class="tabcontent">
                                   
                                    <div class="panel panel-group" style="margin-top: 50pt">
                                    <div class="panel panel-primary">
                                    
                                        <div class="panel panel-heading">   
                                            <font color="white"><i class="fa fa-product-hunt" aria-hidden="true"style="color: goldenrod"></i> Project</font>
                                        </div>
                                        <div class="panel panel-body">
                                            <table>
                                    <tr><td style="color: black"><strong> Project Name: </strong></td><td style="padding-left:50pt">Project one</td></tr>
                                    <tr><td style="color: black"><strong>Project Type: </strong></td><td style="padding-left:50pt">Agriculture</td></tr>
                                    <tr><td style="color: black"><strong>Brief Description of the project: </strong></td><td style="padding-left:50pt">Farming and providing for both internal and external markets</td></tr>
                                            </table>
                                    
                                        </div>
                                    </div>
                                        <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"> <i class="fa fa-map-marker" aria-hidden="true"style="color: goldenrod"></i> Project location</div>
                                            
                                        <div class="panel panel-body">
                                            <table>
                                            <tr><td style="color: black"><strong>Region : </strong></td><td style="padding-left:50pt">Tigray</td></tr>
                                            <tr><td style="color: black"><strong>Woreda :</strong></td><td style="padding-left:50pt"> 12</td></tr>
                                            <tr><td style="color: black"><strong>Zone :</strong></td><td style="padding-left:50pt">04</td></tr>
                                            <tr><td style="color: black"><strong>Kebele :</strong></td><td style="padding-left:50pt">3</td></tr>
                                            </table>
                                        </div>
                                        </div>
                                    
                                         <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"><i class="fa fa-plus-circle" aria-hidden="true"style="color: goldenrod"></i> Total Investment Cost (Birr)</div>
                                            
                                        <div class="panel panel-body">
                                            <table>
                                            <tr><td style="color: black"><strong>Land :</strong></td><td style="padding-left:50pt">25000</td></tr>
                                            <tr><td style="color: black"><strong> Building and Civil works :</strong></td><td style="padding-left:50pt">50000</td></tr>
                                            <tr><td style="color: black"><strong> Machinery/Equipment :</strong></td><td style="padding-left:50pt">30000</td></tr>
                                            <tr><td style="color: black"><strong> Other Fixed Capital cost :</strong></td><td style="padding-left:50pt">10000</td></tr>
                                            <tr><td style="color: black"><strong> Initial Working Capital :</strong></td><td style="padding-left:50pt">10000</td></tr>
                                            <tr><td style="color: black"><strong>Total :</strong></td><td style="padding-left:50pt">100000</td></tr>
                                            </table>
                                        </div>
                                        </div>
                                         <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"><i class="fa fa-plus-square" aria-hidden="true"style="color: goldenrod"></i> Infrastructure Development</div>
                                            
                                        <div class="panel panel-body">
                                            <table>
                                            <tr><td style="color: black"><strong>Road : </strong></td><td style="padding-left:50pt">Asphalt</td></tr>
                                            <tr><td style="color: black"><strong>Electricity :</strong></td><td style="padding-left:50pt">Yes</td></tr>
                                            <tr><td style="color: black"><strong>Tel communication :</strong></td><td style="padding-left:50pt">Yes</td></tr>
                                            <tr><td style="color: black"><strong>Others :</strong></td><td style="padding-left:50pt">Water supply provided</td></tr>
                                            </table>
                                        </div>
                                        </div>
                                        <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"><i class="fa fa-tree" aria-hidden="true"style="color: goldenrod"></i> Environmental Management</div>
                                            
                                        <div class="panel panel-body">
                                            <table>
                                            <tr><td style="color: black"><strong>Forest Management : </strong></td><td style="padding-left:50pt">well protected</td></tr>
                                            <tr><td style="color: black"><strong>Soil and Water Conservation :</strong></td><td style="padding-left:50pt">properly handled</td></tr>
                                            <tr><td style="color: black"><strong>Wildlife Protection :</strong></td><td style="padding-left:50pt">No wildlife around</td></tr>
                                            </table>
                                        </div>
                                        </div>  
                                        
                                        
                                        <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"><i class="fa fa-credit-card-alt" aria-hidden="true"style="color: goldenrod"></i> Source of Finance (Total in Birr)</div>
                                            
                                        <div class="panel panel-body">
                                            <table>
                                            <tr><td style="color: black"><strong>Equity : </strong></td><td style="padding-left:50pt">5000</td></tr>
                                            <tr><td style="color: black"><strong>Loan :</strong></td><td style="padding-left:50pt">2000</td></tr>
                                            <tr><td style="color: black"><strong>Another Source :</strong></td><td style="padding-left:50pt">3000</td></tr>
                                            <tr><td style="color: black"><strong>Total :</strong></td><td style="padding-left:50pt">10000</td></tr>
                                            </table>
                                        </div>
                                        </div>  
                                         <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"><i class="fa fa-dollar" aria-hidden="true"style="color: goldenrod"></i> Employment Opportunity</div>
                                            
                                        <div class="panel panel-body">
                                            <table class="table table-striped jambo_table bulk_action table-bordered">
                                                <thead>
                                                    <tr><td rowspan="2"></td><td colspan="2">Temporary Employees</td><td colspan="2">Permanent Employees</td><td rowspan="2"> Total</td><td rowspan="2">Result</td></tr>
                                                    <tr><td>Male</td><td>Female</td><td>Male</td><td>Female</td></tr>
                                                </thead>
                                                
                                                   
                                                <tr><td>Local</td><td>10</td><td>40</td><td>20</td><td>25</td><td>95</td><td>10</td></tr>
                                                <tr><td>Expatriate</td><td>10</td><td>20</td><td>30</td><td>40</td><td>100</td><td>9</td></tr>
                                            <tr><td>Total</td><td>20</td><td>60</td><td>50</td><td>65</td><td>195</td><td>9</td></tr>
                                            
                                            </table>
                                            <br><tr><td style="color: black"><strong>information if the project creates opportunity for unprivileged group :</strong></td><td style="padding-left:50pt"></td></tr>
                                        </div>
                                        </div>  
                                         <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"> <i class="fa fa-square" aria-hidden="true"style="color: goldenrod"></i> Land Requirement (in Sq. M or Ha.)</div>
                                            
                                        <div class="panel panel-body">
                                            <table>
                                                <tr><td style="color: black"><strong>Industrial : </strong></td><td style="padding-left:50pt">2500</td></tr>
                                                <tr><td style="color: black"><strong>Agricultural :</strong> </strong></td><td style="padding-left:50pt">15000</td></tr>
                                                <tr><td style="color: black"><strong>Service : </strong> </strong></td><td style="padding-left:50pt">200</td></tr>
                                            </table>
                                        </div>
                                        </div>  
                                        <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"><i class="fa fa-check-square-o" aria-hidden="true"style="color: goldenrod"></i> Production/Service</div>
                                            
                                        <div class="panel panel-body">
                                            <table class="table table-striped jambo_table bulk_action table-bordered">
                                                <thead>
                                                    <tr><td rowspan="2">No</td><td td rowspan="2">Type of product/service</td><td colspan="2">production/service capacity</td><td colspan="2">Sales Program</td><td rowspan="2">Result</td></tr>
                                                    <tr><td>Unit</td><td>Quantity</td><td>Domestic market share</td><td>Export market share</td></tr>
                                                </thead>
                                                
                                                   
                                                <tr><td>1</td><td>fruits</td><td>48</td><td>95</td><td>87</td><td>95</td><td>9</td></tr>
                                                <tr><td>2</td><td>Vegetables</td><td>48</td><td>95</td><td>87</td><td>95</td><td>10</td></tr>
                                                
                                            
                                            </table>
                                            
                                        </div>
                                        </div>
                                        <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"> <i class="fa fa-envira" aria-hidden="true"style="color: goldenrod"></i> Raw Material Requirements</div>
                                            
                                        <div class="panel panel-body">
                                            <table class="table table-striped jambo_table bulk_action table-bordered">
                                                <thead>
                                                    <tr><td rowspan="2">No</td><td rowspan="2">Type of raw material</td><td colspan="2">Source of supply</td><td rowspan="2">Result</td></tr>
                                                    <tr><td>Local</td><td>Foreign</td></tr>
                                                </thead>
                                                
                                                   
                                                <tr><td>1</td><td>fertilizers</td><td>EFA</td><td>CFA</td><td>10</td></tr>
                                                <tr><td>2</td><td>Seeds</td><td>ESA</td><td>CSA</td><td>9</td></tr>
                                            
                                            </table>
                                            
                                        </div>
                                        </div>
                                     <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"> <i class="fa fa-truck" aria-hidden="true"style="color: goldenrod"></i> Technology Used</div>
                                            
                                        <div class="panel panel-body">
                                            <table class="table table-striped jambo_table bulk_action table-bordered">
                                                <thead>
                                                    <tr><td rowspan="2">Machinery type</td><td rowspan="2">Quantity</td><td colspan="2">Source</td><td rowspan="2"> Remark</td><td rowspan="2">Result</td></tr>
                                                    <tr><td>Rent</td><td>Bought</td></tr>
                                                </thead>
                                                
                                                   
                                                <tr><td>Tractors</td><td>12</td><td>2</td><td>10</td><td></td><td>9</td></tr>
                                                <tr><td>Water pump</td><td>2</td><td>0</td><td>2</td><td></td><td>10</td></tr>
                                            
                                            
                                            </table>
                                            
                                        </div>
                                        </div>
                                    </div>
                                    
                                    <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"> <i class="fa fa-flask" aria-hidden="true"style="color: goldenrod"></i> Chemical Used</div>
                                            
                                        <div class="panel panel-body">
                                            <table class="table table-striped jambo_table bulk_action table-bordered">
                                                <thead>
                                                    <tr><td colspan="3">Herbicide</td><td colspan="3">Pesticide</td><td colspan="3">Fungicide</td><td colspan="3"> Fertilizer</td><td rowspan="2"> Others</td><td rowspan="2">Result</td></tr>
                                                    <tr><td>Type</td><td>Quantity</td><td>Source of Supply</td><td>Type</td><td>Quantity</td><td>Source of Supply</td><td>Type</td><td>Quantity</td><td>Source of Supply</td><td>Type</td><td>Quantity</td><td>Source of Supply</td>
                                                    </tr>
                                                </thead>
                                                
                                                   
                                                <tr><td>Non selective</td><td>3</td><td>EHA</td><td>insecticides</td><td>3</td><td>EHA</td><td>organic</td><td>48</td><td>EHA</td><td>Organic</td><td>3</td><td>EHA</td><td></td><td>10</td></tr>
                                            
                                            
                                            </table>
                                            
                                        </div>
                                        </div>
                                    
                                    
                                    <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"> <i class="fa fa-cubes" aria-hidden="true"style="color: goldenrod"></i> Seeds</div>
                                            
                                        <div class="panel panel-body">
                                            <table class="table table-striped jambo_table bulk_action table-bordered">
                                                <thead>
                                                    <tr><td>Crop Type</td><td>Quantity</td><td>Source of Supply</td><td>Remark</td><td>Result</td></tr>
                                                </thead>
                                                <tbody>
                                                    <tr><td>wheat </td><td>12</td><td>EHA</td><td></td><td>8</td></tr>
                                                    <tr><td>vegetables </td><td>18</td><td>EHA</td><td></td><td>10</td></tr>
                                                    <tr><td>Teff </td><td>97</td><td>EHA</td><td></td><td>10</td></tr>
                                                <tbody>                     
                                            </table>
                                            
                                        </div>
                                        </div>
                                    <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"> <i class="fa fa-square-o" aria-hidden="true"style="color: goldenrod"></i> Farm Management and Information Handling</div>
                                            
                                        <div class="panel panel-body">
                                            <table>
                                                
                                                <tr><td style="color: black"><strong>Frequency of Plowing before sowing </strong></td><td style="padding-left:50pt">5 times</td></tr>
                                                <tr><td style="color: black"><strong>Sowing Method </strong></td><td style="padding-left:50pt">line sowing</td></tr>
                                                <tr><td style="color: black"><strong>Fertilizer Quantity and Application Method </strong></td><td style="padding-left:50pt">placement</td></tr>
                                                <tr><td style="color: black"><strong>Water Management: </strong></td><td style="padding-left:50pt">efficient</td></tr>
                                                <tr><td style="color: black"><strong>Training/Technical Support </strong></td><td style="padding-left:50pt">no technical support given</td></tr>
                                                                 
                                            </table>
                                            
                                        </div>
                                        </div>
                                    <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"> <i class="fa fa-bed" aria-hidden="true"style="color: goldenrod"></i> Camp Infrastructure</div>
                                            
                                        <div class="panel panel-body">
                                            <table>
                                                
                                                <tr><td style="color: black"><strong>Worker residential house </strong></td><td style="padding-left:50pt"> huts</td></tr>
                                                <tr><td style="color: black"><strong>Water supply </strong></td><td style="padding-left:50pt">good</td></tr>
                                                <tr><td style="color: black"><strong>Medical facility </strong></td><td style="padding-left:50pt">first level clinic</td></tr>
                                                <tr><td style="color: black"><strong>Toilets and bath rooms </strong></td><td style="padding-left:50pt">two provided for each</td></tr>
                                                <tr><td style="color: black"><strong>Kitchen facilities </strong></td><td style="padding-left:50pt">provided for each</td></tr>
                                                <tr><td style="color: black"><strong>Dining hall </strong></td><td style="padding-left:50pt"></td></tr>                 
                                                <tr><td style="color: black"><strong>Refreshment and entertainment facility </strong></td><td style="padding-left:50pt">restaurant</td></tr>                 
                                                <tr><td style="color: black"><strong>Consumable items supply </strong></td><td style="padding-left:50pt">department store</td></tr>                 
                                                <tr><td style="color: black"><strong>Food supply </strong></td><td style="padding-left:50pt">department store</td></tr>                 
                                                <tr><td style="color: black"><strong>Safety outfits supplies </strong></td><td style="padding-left:50pt">two provided for each</td></tr>                 
                                            </table>
                                            
                                        </div>
                                        </div>
                                    <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"> <i class="fa fa-check" aria-hidden="true"style="color: goldenrod"></i> Creating good working environment for workers</div>
                                            
                                        <div class="panel panel-body">
                                            <table class="table table-striped jambo_table bulk_action table-bordered">
                                                
                                                <thead>
                                                    <tr><td rowspan =2>Office building</td><td rowspan="2">Machinery Shed</td><td colspan="4">Store</td><td rowspan="2">Result</td></tr>
                                                    <tr><td>production</td><td>chemicals</td><td>Fertilizers</td><td>Others</td></tr>
                                                </thead>
                                                <tr><td>3 office buildings</td><td>2 machinery sheds</td><td>5 production stores</td><td>2 chemical stores</td><td>5 fertilizer stores</td><td></td><td>10</td></tr>
                                                
                                            </table>
                                            
                                        </div>
                                        </div>
                                    <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"> <i class="fa fa-child" aria-hidden="true"style="color: goldenrod"></i> Employment</div>
                                            
                                        <div class="panel panel-body">
                                            <table class="table table-striped jambo_table bulk_action table-bordered">
                                                
                                                <thead>
                                                    <tr><td>Professionals</td><td>Quantity</td><td>Result</td></tr>
                                                </thead>
                                                <tr><td>General Manager</td><td>1</td><td>9</td></tr>
                                                <tr><td>Deputy Manager</td><td>3</td><td>10</td></tr>
                                                <tr><td>Agronomist</td><td>5</td><td>10</td>
                                                </tr>
                                            </table>
                                            
                                        </div>
                                        </div>
                                    <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"> <i class="fa fa-bullhorn" aria-hidden="true"style="color: goldenrod"></i> Social Contribution</div>
                                            
                                        <div class="panel panel-body">
                                            <table >
                                                
                                                <tr><td style="color: black"><strong>School</strong></td><td style="padding-left:50pt">1 school</td></tr>
                                                <tr><td style="color: black"><strong>Hospital</strong></td><td style="padding-left:50pt"></td></tr>
                                                <tr><td style="color: black"><strong>Road</strong></td><td style="padding-left:50pt">1 second level road</td></tr>
                                                <tr><td style="color: black"><strong>Water Facility</strong></td><td style="padding-left:50pt"></td></tr>
                                            </table>
                                            
                                        </div>
                                        </div>
                                      <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"> <i class="fa fa-circle-o-notch" aria-hidden="true"style="color: goldenrod"></i> Project Bottleneck</div>
                                            
                                        <div class="panel panel-body">
                                            <table class="table table-striped jambo_table bulk_action table-bordered">
                                                <thead>
                                                    <tr>
                                                        <td>Type</td>
                                                        <td>Remark</td>
                                                    </tr>
                                                </thead>
                                                    
                                                <tr><td>Infrastructure</td><td> No road</td></tr>
                                                <tr><td>Technology</td><td></td></tr>
                                                <tr><td>Training</td><td>No training taken</td></tr>
                                                <tr><td>Supply</td><td>no supply and demand relation created</td></tr>
                                                <tr><td>Quality Seed</td><td>no quality seed supplied</td></tr>
                                                <tr><td>Man Power</td><td>lack of agricultural irrigation experts</td></tr>
                                                <tr><td>Others</td><td></td></tr>
                                                
                                                
                                                
                                            </table>
                                            
                                        </div>
                                        </div>
                                     <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"> <i class="fa fa-plus-square" aria-hidden="true"style="color: goldenrod"></i> Project assessment total result
</div>
                                            
                                        <div class="panel panel-body">
                                            <table >
                                                
                                                <tr><td style="color: black"><strong>Result</strong></td><td style="padding-left:50pt">A</td></tr>
                                                
                                                
                                            </table>
                                            
                                        </div>
                                        </div>
                                    <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"> <i class="fa fa-expand" aria-hidden="true"style="color: goldenrod"></i> Expansion Detail</div>
                                            
                                        <div class="panel panel-body">
                                            <table >
                                                
                                                <tr><td style="color: black"><strong>Detail Expansion Request</strong></td><td style="padding-left:50pt">Expansion Request.pdf</td></tr>
                                                <tr><td style="color: black"><strong>Area of land transferred for expansion</strong></td><td style="padding-left:50pt">500 sq.M</td></tr>
                                                
                                            </table>
                                            
                                        </div>
                                        </div>
                                   
                                    
                                    
                                    
                                    
                                    
                                    </div>
                                    <div id="nott2" class="tabcontent">
                                    <div class="panel panel-group" style="margin-top: 50pt">
                                        <div class="panel panel-primary">
                                            <div class="panel panel-heading" style="color:white"><i class="fa fa-bell-o" aria-hidden="true"style="color: goldenrod"></i></div>
                                            <div class="panel panel-body">
                                                <div class="container">
  
  <ul class="nav nav-tabs" style>
      <li class="active" style="width:200pt"><a data-toggle="tab" href="#Promotion2"><i class="fa fa-tags" aria-hidden="true"></i> Promotion</a></li>
      <li style="width:200pt"><a data-toggle="tab" href="#Bid2"><i class="fa fa-envelope-open" aria-hidden="true"></i> Bid</a></li>
    <li style="width:200pt"><a data-toggle="tab" href="#Training2"><i class="fa fa-male" aria-hidden="true"></i> Training</a></li>
    <li style="width:200pt"><a data-toggle="tab" href="#Warning2"><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Warning</a></li>
    <li style="width:200pt"><a data-toggle="tab" href="#Cert2"><i class="fa fa-file-text" aria-hidden="true"></i> Certification</a></li>
    <li style="width:200pt"><a data-toggle="tab" href="#Others2"><i class="fa fa-id-card" aria-hidden="true"></i> Others</a></li>
  </ul>

  <div class="tab-content">
    <div id="Promotion2" class="tab-pane fade in active">
      
      <table class="table table-striped table-bordered table-hover table-condensed">
      <tr><td>Promotion notification</td></tr>
      <tr><td>Promotion notification</td></tr>
      <tr><td>Promotion notification</td></tr>
      </table>
    </div>
    <div id="Bid2" class="tab-pane fade">
        <script>
            
            
            console.log("Bid");
        </script>
      
       <table class="table table-striped table-bordered table-hover table-condensed">
      <tr><td>Bid notification</td></tr>
      <tr><td>Bid notification</td></tr>
      <tr><td>Bid notification</td></tr>
      </table>
    </div>
    <div id="Training2" class="tab-pane fade">
      
       <table class="table table-striped table-bordered table-hover table-condensed">
      <tr><td>Training notification</td></tr>
      <tr><td>Training notification</td></tr>
      <tr><td>Training notification</td></tr>
      </table>
    </div>
      <div id="Warning2" class="tab-pane fade">
      
       <table class="table table-striped table-bordered table-hover table-condensed">
      <tr><td>Warning notification</td></tr>
      <tr><td>Warning notification</td></tr>
      <tr><td>Warning notification</td></tr>
      </table>
    </div>
      <div id="Cert2" class="tab-pane fade">
      
      <table class="table table-striped table-bordered table-hover table-condensed">
      <tr><td>Certification notification</td></tr>
      <tr><td>Certification notification</td></tr>
      <tr><td>Certification notification</td></tr>
      </table>
    </div>
    <div id="Others2" class="tab-pane fade">
      
      <table class="table table-striped table-bordered table-hover table-condensed">
      <tr><td>Others notification</td></tr>
      <tr><td>Others notification</td></tr>
      <tr><td>Others notification</td></tr>
      </table>
    </div>
  </div>
</div>

                                                
                                                
                                            </div>
                                            
                                        </div>
                                        
                                    </div>
                                    
                                    
                                  </div>
                            </div>                        
                                  
                                            
                                            <div id="project3" class="tab-pane">
      
      
   
                                <div class="tab col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <button class="tablinks" onclick="openTab(event, 'Details3')">Company/ Investor Profile</button>
                                    <button class="tablinks" onclick="openTab(event, 'Document3')">Document</button>
                                    <button class="tablinks" onclick="openTab(event, 'lease3')">Lease Agreement</button>
                                    <button class="tablinks" onclick="openTab(event, 'cadaster3')">Cadaster Information</button>
                                    <button class="tablinks" onclick="openTab(event, 'PromotedLand3')">Promoted Land Information</button>
                                    <button class="tablinks" onclick="openTab(event, 'pdetail3')">Project Details</button>
                                    <button class="tablinks" onclick="openTab(event, 'invsup3')">Investment Support</button>
                                    <button class="tablinks" onclick="openTab(event, 'nott3')">Notifications</button>
                                </div>
                                
                                
                                <div id="Details3" class="tabcontent">
                                    <div class="col-md-8">
                                    <table class=" col-md-8 table table-borderless mtable" >
                                            <%
                                                InvestorInfo.investerAssesDetails retDetails3 = Investor.getInvestorD();
                                                for(InvestorInfo.investerAssesDetails.investorDetails val:retDetails3.ID){
                                                %>
                                            <tr class="bg-primary"> <td><font color="white"><i class="fa fa-align-justify" aria-hidden="true"style="color: goldenrod"></i><strong>  Investor Details :</strong> </strong></td><td></td></tr>
                                            <tr><td style="color: black"><strong>  Company's name :</font></strong></td><td><%=val.Company%></td></tr>
                                            <tr><td style="color: black"><strong>Company's owner name :</strong></td><td><%=val.Owner%></td></tr>
                                            <tr><td style="color: black"><strong>Company Type : </strong></td><td><%=val.CType%></td></tr>
                                            
                                            <tr><td style="color: black"><strong> Nationality / Country of incorporation, if company :</strong></td><td><%=val.Nationality%> </td></tr>
                                            <tr><td style="color: black"><strong>Tax Identification no. :</strong></td><td><%=val.taxid%></td> </tr>
                                            <tr><td style="color: black"><strong>Commercial Registration No. </strong></td><td><%=val.comreg%></td> </tr>
                                            
                                            <tr><td style="color: black"><strong>Investor type : </strong></td><td><%=val.IType%></td></tr>
                                            <tr><td style="color: black"><strong>Investment permit no : </strong></td><td><%=val.IPNo%></td></tr>
                                            
                                           
                                            
                                            <tr class="bg-primary"><td><i class="fa fa-address-book" aria-hidden="true"style="color: goldenrod"></i> <strong><font color="white"><strong>  Address :</strong> </td> <td  ></td></tr>
                                            <tr><td style="color: black"><strong>Region :</strong></td><td><%=val.Region%> </td></tr>
                                            <tr><td style="color: black"><strong>Zone :</strong></td><td><%=val.Zone%> </td></tr>
                                            <tr><td style="color: black"><strong>City :</strong></td><td><%=val.City%> </td></tr>
                                            <tr><td style="color: black"><strong>Sub-city/ Woreda :</strong></td><td><%=val.Woreda%> </td></tr>
                                            
                                            <tr><td style="color: black"><strong>Kebele :</td>  <td><%=val.kebele%> </td></tr>
                                            <tr><td style="color: black"><strong>House No.</strong></td><td><%=val.HouseNo%></td> </tr>
                                            <tr><td style="color: black"><strong>Telephone No.( Land line)</td>  <td><%=val.Telephone%> </td> </tr>
                                            <tr><td style="color: black"><strong>Address :</strong></td><td><%=val.Address%> </td></tr>
                                            <tr><td style="color: black"><strong>Post</strong></td><td><%=val.post%></td></tr>
                                            <tr><td style="color: black"><strong>Street Address</strong></td><td><%=val.sa%></td></tr>
                                            <tr><td style="color: black"><strong>Emergency Contact</strong></td><td><%=val.ec%></td></tr>
                                            
                                            <tr><td style="color: black"><strong>Phone :</strong></td><td><%=val.Phone%> </td></tr>
                                            <tr><td style="color: black"><strong>Email :</strong></td><td><%=val.Email%> </td></tr>
                                            <tr><td style="color: black"><strong>Fax : </strong></td><td> <%=val.Fax%> </td></tr>
                                            
                                             <tr class="bg-primary"><td  ><font color="white"><strong><i class="fa fa-money" aria-hidden="true"style="color: goldenrod"></i>  Finance :</strong> </td> <td  ></td></tr>
                                            <tr><td style="color: black"><strong>Starting capital :</strong></td><td><%=val.SCapital%> </td></tr>
                                            <tr><td style="color: black"><strong>Current capital :</strong></td><td><%=val.CCapital%> </td></tr>
                                            <tr><td style="color: black"><strong>Form of ownership : </strong></td><td><%=val.formofownership%></td> </tr>
                                            <tr><td style="color: black"><strong>Type of ownership :</strong></td><td><%=val.towner%> </td> </tr>
                                         
                                            <%}%>
                                     </table>
                                    </div>
                                     <div class="col-md-2 col-md-offset-2 menu_fixed" >
                                        <image height="140px" width="140px" src="assets/images/mal.jpg"/>
                                        </div>
                                     
                                     
                                  </div>
                                     

                                  <div id="Document3" class="tabcontent">
                                    <h3> <font color="black">Full Investor File </font> </h3>
                                      
                                      <div class="panel panel-primary">
                                      <div class="panel-heading"><font color="white"><i class="fa fa-files-o" aria-hidden="true"style="color: goldenrod"></i> Files </font></div>
                                      <div class="panel-body">    
                                          <table>
                                               <%
                                                InvestorInfo.investordocuments retinvdocs33 = Investor.getinvestordocs();
                                                for(InvestorInfo.investordocuments.investordocs val:retinvdocs33.ids){
                                                %>
                                        <tr><td style="color: black"><strong>Power of Attorney : </strong></td><td style="padding-left:50pt"><%=val.poa%></td></tr>
                                        <tr><td style="color: black"><strong>Memorandum (if organization) : </strong></td><td style="padding-left:50pt"><%=val.memorandum%></td></tr>
                                        <tr><td style="color: black"><strong>Regulation of establishment (if public enterprise) : </strong></td><td style="padding-left:50pt"><%=val.roe %></td></tr>                                             
                                        <tr><td style="color: black"><strong>Articles of Association (if cooperative society) : </strong></td><td style="padding-left:50pt"><%=val.aoa %></td></tr>
                                       <tr><td style="color: black"><strong>Support letter for Lease Applications :</strong></td><td style="padding-left:50pt"><%=val.slla %></td></tr> 
                                       
                                       <tr><td style="color: black"><strong>Agricultural Investment Land Identification Document :</strong></td><td style="padding-left:50pt"><%=val.ailid %></td></tr>
                                       
                                       <tr><td style="color: black"><strong>Land Transfer Letter :</strong></td><td style="padding-left:50pt"><%=val.landtl %></td></tr>
                                       
                                       <tr><td style="color: black"><strong>Work permit :</strong></td><td style="padding-left:50pt"><%=val.workpermit %></td></tr>
                                       
                                       <tr><td style="color: black"><strong>Certificate for land use by Regional State Environment, Forest & Land Administration Bureau :</strong></td><td style="padding-left:50pt"><%=val.cflrs %></td></tr>
                                       
                                       
                                       
                                       <tr><td style="color: black"><strong>Profit or Loss Balance from Business :</strong></td><td style="padding-left:50pt"><%=val.profitorloss %></td></tr>
                                       
                                       <tr><td style="color: black"><strong>Organization Law : </strong></td><td style="padding-left:50pt"><%=val.organiztionlaw %></td></tr>
                                       
                                       <tr><td style="color: black"><strong>Meeting Minutes :</strong></td><td style="padding-left:50pt"><%=val.meetingminutes %></td></tr>
                                       <tr><td style="color: black"><strong> ID (if individual) :</strong></td><td style="padding-left:50pt"><%=val.id%></td></tr>
                                       <%}%>
                                       
                                       </table>
                                              </div>
                                      </div>
                                  
                                  
                                    <div class="panel panel-primary">
                                        <div class="panel panel-heading"><font color="white"><i class="fa fa-users" aria-hidden="true"style="color: goldenrod"></i> Foreign Investor </font></div>
                                        <div class="panel panel-body"> 
                                            <table>
                                                <%
                                                InvestorInfo.investordocuments retinvdocs3f = Investor.getinvestordocs();
                                                for(InvestorInfo.investordocuments.investordocs val:retinvdocs3f.ids){
                                                %>
                                           <tr><td style="color: black"><strong> Passport (if individual) : </strong></td><td style="padding-left:50pt"><%=val.passport%></td></tr>
                                           <tr><td style="color: black"><strong>   Letter from Federal Democratic Republic of Ethiopia Ministry of Foreign Affairs for Diaspora Association :</strong></td><td style="padding-left:50pt"><%=val.lfdremofa%></td></tr>
                                       
                                      <tr><td style="color: black"><strong> Support letter to Invest as Diaspora from Foreign Minister Investor for Diaspora Activity Directorate General :</strong></td><td style="padding-left:50pt"><%=val.supportletter%></td></tr>
                                            <%}%>
                                            </table>
                                        </div> 
                                    </div>
                                    <div class="panel panel-primary">
                                      <div class="panel panel-heading"><font color="white" ><i class="fa fa-expand" aria-hidden="true"style="color: goldenrod"></i> For Expansion or Upgrading of an existing enterprise</font></div>
                                       <div class="panel panel-body"> 
                                           <table>
                                                <%
                                                InvestorInfo.investordocuments retinvdocs3b = Investor.getinvestordocs();
                                                for(InvestorInfo.investordocuments.investordocs val:retinvdocs3b.ids){
                                                %>
                                            <tr><td style="color: black"><strong>  business license : <br> </strong></td><td style="padding-left:50pt"><%=val.businesslicence%></td></tr>
                                            <%}%>
                                           </table>
                                       </div>   
                                    </div>
                                    <div class="panel panel-primary">
                                      <div class="panel panel-heading"><font color="white" ><i class="fa fa-file" aria-hidden="true"style="color: goldenrod"></i> Lease Required Documents</font></div>
                                       <div class="panel panel-body"> 
                                           <table>
                                                <%
                                                InvestorInfo.investordocuments retinvdocs3l = Investor.getinvestordocs();
                                                for(InvestorInfo.investordocuments.investordocs val:retinvdocs3l.ids){
                                                %>
                                            <tr><td style="color: black"><strong>  business plan : <br> </strong></td><td style="padding-left:50pt"><%=val.businessplan%></td></tr>
                                            <tr><td style="color: black"><strong>  The site plan of the leased land : <br> </strong></td><td style="padding-left:50pt"><%=val.siteplanofleaseland%></td></tr>
                                            <tr><td style="color: black"><strong>  Leased land delivery minute :<br> </strong></td><td style="padding-left:50pt"><%=val.leasedlanddeliveryminute%></td></tr>
                                            <tr><td style="color: black"><strong>  Environment impact assessment document : <br> </strong></td><td style="padding-left:50pt"><%=val.eiad%></td></tr>
                                            <tr><td style="color: black"><strong>  Performance reports. : <br> </strong></td><td style="padding-left:50pt"><%=val.performancereports%></td></tr>
                                            <tr><td style="color: black"><strong>  Closure plan : <br> </strong></td><td style="padding-left:50pt"><%=val.closureplan%></td></tr>
                                            <tr><td style="color: black"><strong>  Community agreement (social contract): <br> </strong></td><td style="padding-left:50pt"><%=val.communityagree%></td></tr>
                                            <tr><td style="color: black"><strong>  Land request form : <br> </strong></td><td style="padding-left:50pt"><%=val.landreqform%></td></tr>
                                            <%}%>
                                           </table>
                                       </div>   
                                    </div>
                                    <div class="panel panel-primary">
                                      <div class="panel panel-heading"><font color="white" > <i class="fa fa-archive" aria-hidden="true"style="color: goldenrod"></i> Miscellaneous </font></div>
                                       <div class="panel panel-body"> 
                                           <table>
                                                <%
                                                InvestorInfo.investordocuments retinvdocs3m = Investor.getinvestordocs();
                                                for(InvestorInfo.investordocuments.investordocs val:retinvdocs3m.ids){
                                                %>
                                            <tr><td style="color: black"><strong>  Crop Calendar : <br> </strong></td><td style="padding-left:50pt"><%=val.cropcalander%></td></tr>
                                            <tr><td style="color: black"><strong>  Employee Contract Document: <br> </strong></td><td style="padding-left:50pt"><%=val.employeecontractdoc%></td></tr>
                                            <tr><td style="color: black"><strong>  Internal Rule and regulation document :<br> </strong></td><td style="padding-left:50pt"><%=val.internalruleandregdoc%></td></tr>
                                            <tr><td style="color: black"><strong>  Record of annual crop infestations and spread of disease and measures taken : <br> </strong></td><td style="padding-left:50pt"><%=val.raciasdmt%></td></tr> 
                                            <tr><td style="color: black"><strong> Investor investment track record document</strong></td><td style="padding-left:50pt"><%=val.iitrd%></td></tr>
                                            <%}%>
                                           </table>
                                       </div>   
                                    </div>
                                    
                                    
                                    
                                  </div>
                                  <div id="PromotedLand3" class="tabcontent">
                                      <div class="col-md-6 col-sm-18 col-xs-18">
                                    <table class="table table-borderless mtable">
                                            <thead>
                                            <th></th>
                                            <th></th>
                                            </thead>
                                            <tbody>
                                                <%
                                                LandInfo.LandBankProfile retLandPro3 = Land.getLandBankProfile();
                                                for(LandInfo.LandBankProfile.LandBankProfileData val: retLandPro3.data){
                                                %>
                                                <tr class="bg-primary"><td><i class="fa fa-address-card" aria-hidden="true"style="color: goldenrod"></i> Parcel ID/Block ID/Farm ID (UPID)</td><td></td></tr>
                                                <tr><td style="color: black"><strong>Parcel Id</strong></td>
                                                    <td><%=val.parcelId%></b></td>
                                                </tr>
                                                 <tr class="bg-primary"><td><i class="fa fa-address-book" aria-hidden="true"style="color: goldenrod"></i> Address</td><td></td></tr>
                                                <tr><td style="color: black"><strong>Kebele</strong></td>
                                                    <td ><%=val.kebele%></b></td>
                                                    </tr>
                                                <tr><td style="color: black"><strong>Woreda</strong></td>
                                                    <td ><%=val.woreda%></b></td></tr>
                                                
                                                <tr><td style="color: black"><strong>Zone</strong></td>
                                                <td ><%=val.zone%></td></tr>
                                                
                                                <tr><td style="color: black"><strong>Region</strong></td>
                                                <td><%=val.region%></td></tr>
                                                
                                                <tr><td style="color: black"><strong>City</strong></td>
                                                <td><%=val.city%></td></tr>
                                                
                                                <tr><td style="color: black"><strong>Address</strong></td>
                                               	<td><%=val.address%></td></tr>
                                                 <tr><td style="color: black"><strong>Distance from point of center(region capital)</strong></td>
                                                     <td><%=val.distance%></td></tr>
                                                 <tr><td style="color: black"><strong>X Coordinate</strong></td>
                                                 <td><%=val.xCordinate%></td></tr>
                                                 <tr><td style="color: black"><strong>Y Coordinate</strong></td>
                                                   <td><%=val.yCordinate%></td></tr>
                                                
                                                 <tr class="bg-primary"><td><i class="fa fa-arrows" aria-hidden="true"style="color: goldenrod"></i> Neighbor</td><td></td></tr>
                                                  <tr><td style="color: black"><strong>From North---</strong></td>
                                                    <td><%=val.neighrbourFromNorth%></td></tr>
                                                   <tr><td style="color: black"><strong>From South--</strong></td>
                                                    <td><%=val.neighrbourFromSouth%></td></tr>
                                                    <tr><td style="color: black"><strong>From East--</strong></td>
                                                    <td><%=val.neighrbourFromWest%></td></tr>
                                                     <tr><td style="color: black"><strong>From West--</strong></td>
                                                    <td><%=val.neighrbourFromEast%></td></tr>
                                                  
                                                     <tr class="bg-primary"><td><i class="fa fa-plane" aria-hidden="true"style="color: goldenrod"></i> Accessibility</td><td></td></tr>
                                                    <tr><td style="color: black"><strong>Air</strong></td>
                                                    <td><%=val.accessibilityAir%></td></tr>
                                                     <tr><td style="color: black"><strong>Road</strong></td>
                                                    <td><%=val.accessibilityRoad%></td></tr>
                                                    <tr><td style="color: black"><strong>Rail</strong></td>
                                                    <td><%=val.accessibilityRail%></td></tr>
                                                     <tr><td style="color: black"><strong>Water</strong></td>
                                                    <td><%=val.accessibilityWater%></td></tr>
                                                 
                                                  <tr class="bg-primary"><td><i class="fa fa-square-o" aria-hidden="true"style="color: goldenrod"></i> Area</td><td></td></tr>
                                                   <tr><td style="color: black"><strong>Area(hectare)</strong></td>
                                                    <td><%=val.area%></td></tr>
                                                     <tr><td style="color: black"><strong>condition of the expansion spots</strong></td>
                                                    <td><%=val.conditionExpansion%></td></tr>
                                                    
                                                    <tr class="bg-primary"><td><i class="fa fa-line-chart" aria-hidden="true"style="color: goldenrod"></i> Status of infrastructure and services</td><td></td></tr>
                                                   <tr><td style="color: black"><strong>Road-Asphalt and Electric Line</strong></td>
                                                    <td><%=val.statusInfraRoadElec%></td></tr>
                                                     <tr><td style="color: black"><strong>Telecommunication Service</strong></td>
                                                    <td><%=val.statusInfraTele%></td></tr>
                                                      <tr><td style="color: black"><strong>Health</strong></td>
                                                    <td><%=val.statusInfraHealth%></td></tr>
                                                       <tr><td style="color: black"><strong>education</strong></td>
                                                    <td><%=val.statusInfraEducation%></td></tr>
                                                       <tr><td style="color: black"><strong>Closest administration office</strong></td>
                                                    <td><%=val.statusInfraAdmin%></td></tr>
                                                    
                                                  <% break;
                                                        }
                                                        %>
                                            </tbody>
                                        </table>
                                            </div>
                                            <div class="col-md-6 col-sm-18 col-xs-18">
                                            <table class="table table-borderless mtable">
                                            <thead>
                                            <th></th>
                                            <th></th>
                                            </thead>
                                            <tbody>
                                                <%
                                                LandInfo.LandBankProfile retLandPro31 = Land.getLandBankProfile();
                                                for(LandInfo.LandBankProfile.LandBankProfileData val: retLandPro31.data){
                                                %>
                                            
                                              
                                                 <tr class="bg-primary"><td><i class="fa fa-square" aria-hidden="true"style="color: goldenrod"></i> Land Use</td><td></td></tr>
                                                     <tr><td style="color: black"><strong>Land use</strong></td>
                                                    <td><%=val.landUse%></td></tr>
                                                    <tr><td style="color: black"><strong>Investment Type</strong></td>
                                                    <td><%=val.investmentType%></td></tr>
                                                    
                                                     <tr class="bg-primary"><td> <i class="fa fa-font-awesome" aria-hidden="true"style="color: goldenrod"></i> Soil Status</td><td></td></tr>
                                                    <tr><td style="color: black"><strong>Soil fertility test result</strong></td>
                                                    <td>testResult.docx</td></tr>
                                                   
                                                  <tr class="bg-primary"><td><i class="fa fa-tree" aria-hidden="true"style="color: goldenrod"></i> Agro-Ecology</td><td></td></tr>
                                                    <tr><td style="color: black"><strong>Socio-economic study</strong></td>
                                                    <td><%=val.socioEconomy%></td></tr>
                                                    <tr><td style="color: black"><strong>Agero-ecology</strong></td>
                                                    <td><%=val.ageroEcology%></td></tr>
                                                    
                                                     <tr class="bg-primary"><td><i class="fa fa-info" aria-hidden="true"style="color: goldenrod"></i> Lease Information</td><td></td></tr>
                                                      <tr><td style="color: black"><strong>Lease Type</strong></td>
                                                    <td><%=val.leaseType%></td></tr>
                                                      <tr><td style="color: black"><strong>Lease Duration</strong></td>
                                                    <td><%=val.leaseDuration%></td></tr>
                                                      <tr><td style="color: black"><strong>Lease amount per hectare</strong></td>
                                                    <td><%=val.leasePerHectare%></td></tr>
                                                     
                                                     <tr class="bg-primary"><td><i class="fa fa-cloud" aria-hidden="true"style="color: goldenrod"></i> Weather Condition and Others</td><td></td></tr>
                                                     <tr><td style="color: black"><strong>Surface and underground water potential</strong></td>
                                                    <td><%=val.surUnderWaterPote%></td></tr>
                                                     
                                                     <tr><td style="color: black"><strong>Water potential for irrigation services</strong></td>
                                                    <td><%=val.waterPote%></td></tr>
                                                      <tr><td style="color: black"><strong>Wind directions and speed</strong></td>
                                                    <td><%=val.windDirecSpeed%></td></tr>
                                                     <tr><td style="color: black"><strong>Rainfall distribution</strong></td>
                                                    <td><%=val.rainfallDist%></td></tr>
                                                     
                                                     <tr><td style="color: black"><strong>Atmospheric moisture content(Humidity)</strong></td>
                                                    <td><%=val.humudity%></td></tr>
                                                     
                                                      <tr><td style="color: black"><strong>The height above the sea level</strong></td>
                                                    <td><%=val.height%></td></tr>
                                                    
                                                     <tr><td style="color: black"><strong>Cover of herbs</strong></td>
                                                    <td><%=val.coverOfHerbs%></td></tr>
                                                     <tr><td style="color: black"><strong>Landscape</strong></td>
                                                    <td><%=val.landscape%></td></tr>
                                                     <tr><td style="color: black"><strong>Others</strong></td>
                                                    <td><%=val.others%></td></tr>
                                                <%
                                                    break;
            
                                                            }%>
                                            </tbody>
                                           
                                            </table>
                                    </div>
                                        </div>
                                   
                                    
                                      
                               
                                     <div id="invsup3" class="tabcontent">
                                          <div class="panel panel-group" style="margin-top: 50pt">
                                    <div class="panel panel-primary">
                                    
                                        <div class="panel panel-heading">   
                                            <font color="white"><i class="fa fa-shopping-bag" aria-hidden="true"style="color: goldenrod"></i> Duty free Privileges</font>
                                        </div>
                                        <div class="panel panel-body">
                                            <table class="table table-striped jambo_table bulk_action table-bordered">
                                                <thead>
                                                    <tr><td rowspan="2">Types of Privilege</td><td colspan="3">Status of use</td><td rowspan="2">Reason</td></tr>
                                                    <tr><td>Project use</td><td>Out of order</td><td>Used for intended purpose</td></tr>
                                                </thead>
                                    <tr><td>duty free import</td><td>15</td><td>2</td><td>13</td><td>To farm the land</td></tr>
                                    <tr><td>duty free export</td><td>15</td><td>2</td><td>13</td><td>To earn money</td></tr>
                                            </table>
                                    
                                        </div>
                                    </div>
                                              
                                              <div class="panel panel-primary">
                                    
                                        <div class="panel panel-heading">    
                                            <font color="white"><i class="fa fa-shopping-basket" aria-hidden="true"style="color: goldenrod"></i> Incentives</font>
                                        </div>
                                        <div class="panel panel-body">
                                            <table >
                                                
                                    <tr><td style="color: black"><strong>Investment Incentive Provided:</strong></td><td style="padding-left:50pt">duty free import</td></tr>
                                    <tr><td style="color: black"><strong>Technical Support at field level:</strong></td><td style="padding-left:50pt">Not Required</td></tr>
                                    <tr><td style="color: black"><strong>Grace Period:</strong></td><td style="padding-left:50pt">1 Year</td></tr>
                                    <tr><td style="color: black"><strong>Competence assurance support:</strong></td><td style="padding-left:50pt">Complete</td></tr>
                                    <tr><td style="color: black"><strong>Work Permit Renewal:</strong></td><td style="padding-left:50pt">Renewed</td></tr>
                                    <tr><td style="color: black"><strong>Administrative support:</strong></td><td style="padding-left:50pt">Not Required</td></tr>
                                            </table>
                                    
                                        </div>
                                    </div>
                                              
                                              
                                              
                                              
                                          </div>
                                </div>
                                 <div id="lease3" class="tabcontent">
                                    <div class="panel panel-group" style="margin-top: 50pt">
                                    <div class="panel panel-primary">
                                    
                                        <div class="panel panel-heading">   
                                            <font color="white"><i class="fa fa-book" aria-hidden="true"style="color: goldenrod"></i>  </font>
                                        </div>
                                        <div class="panel panel-body">
                                     <div class="col-md-2 col-md-offset-0 menu_fixed" >
                                        
                                         <a style="color:blue" href="assets/files/Lease.pdf">Download Lease Document</a> 
                                         
                                        </div>
                                
                                    
                                        </div>
                                    </div>
                                  </div>
                                 </div>
                                <div id="cadaster3" class="tabcontent">
                                    <div class="panel panel-group" style="margin-top: 50pt">
                                    <div class="panel panel-primary">
                                    
                                        <div class="panel panel-heading">   
                                            <font color="white"> <i class="fa fa-map-o" aria-hidden="true"style="color: goldenrod"></i> Cadaster</font>
                                        </div>
                                        <div class="panel panel-body">
                                     <div class="col-md-2 col-md-offset-0 menu_fixed" >
                                         <a href="assets/images/map.png"><image height="600px" width="600px" src="assets/images/map.png"/> </a>
                                        
                                        <a href="assets/images/map.png" download="assets/images/map.png"><font color="blue" >Download Cadaster</font></a> 
                                        </div>
                                    
                                    
                                        </div>
                                    </div>
                                        <div class="panel panel-primary">
                                    
                                        <div class="panel panel-heading">   
                                            <font color="white"><i class="fa fa-book" aria-hidden="true"style="color: goldenrod"></i> Green Book</font>
                                        </div>
                                        <div class="panel panel-body">
                                     <div class="col-md-6" >
                                         <a href="assets/images/Image39.jpg"> <image height="500px" width="600px" src="assets/images/Image39.jpg"/></a>
                                        
                                        
                                        </div>
                                    <div class="col-md-6 col-md-offset-0 menu_fixed" >
                                        <a href="assets/images/Image40.jpg"> <image height="500px" width="600px" src="assets/images/Image40.jpg"/> </a>
                                     
                                        
                                        </div>
                                    
                                        </div>
                                    </div>
                                </div>
                                    
                                  </div>
                                <div id="pdetail3" class="tabcontent">
                                   
                                    <div class="panel panel-group" style="margin-top: 50pt">
                                    <div class="panel panel-primary">
                                    
                                        <div class="panel panel-heading">   
                                            <font color="white"><i class="fa fa-product-hunt" aria-hidden="true"style="color: goldenrod"></i> Project</font>
                                        </div>
                                        <div class="panel panel-body">
                                            <table>
                                    <tr><td style="color: black"><strong> Project Name: </strong></td><td style="padding-left:50pt">Project one</td></tr>
                                    <tr><td style="color: black"><strong>Project Type: </strong></td><td style="padding-left:50pt">Agriculture</td></tr>
                                    <tr><td style="color: black"><strong>Brief Description of the project: </strong></td><td style="padding-left:50pt">Farming and providing for both internal and external markets</td></tr>
                                            </table>
                                    
                                        </div>
                                    </div>
                                        <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"> <i class="fa fa-map-marker" aria-hidden="true"style="color: goldenrod"></i> Project location</div>
                                            
                                        <div class="panel panel-body">
                                            <table>
                                            <tr><td style="color: black"><strong>Region : </strong></td><td style="padding-left:50pt">Tigray</td></tr>
                                            <tr><td style="color: black"><strong>Woreda :</strong></td><td style="padding-left:50pt"> 12</td></tr>
                                            <tr><td style="color: black"><strong>Zone :</strong></td><td style="padding-left:50pt">04</td></tr>
                                            <tr><td style="color: black"><strong>Kebele :</strong></td><td style="padding-left:50pt">3</td></tr>
                                            </table>
                                        </div>
                                        </div>
                                    
                                         <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"><i class="fa fa-plus-circle" aria-hidden="true"style="color: goldenrod"></i> Total Investment Cost (Birr)</div>
                                            
                                        <div class="panel panel-body">
                                            <table>
                                            <tr><td style="color: black"><strong>Land :</strong></td><td style="padding-left:50pt">25000</td></tr>
                                            <tr><td style="color: black"><strong> Building and Civil works :</strong></td><td style="padding-left:50pt">50000</td></tr>
                                            <tr><td style="color: black"><strong> Machinery/Equipment :</strong></td><td style="padding-left:50pt">30000</td></tr>
                                            <tr><td style="color: black"><strong> Other Fixed Capital cost :</strong></td><td style="padding-left:50pt">10000</td></tr>
                                            <tr><td style="color: black"><strong> Initial Working Capital :</strong></td><td style="padding-left:50pt">10000</td></tr>
                                            <tr><td style="color: black"><strong>Total :</strong></td><td style="padding-left:50pt">100000</td></tr>
                                            </table>
                                        </div>
                                        </div>
                                         <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"><i class="fa fa-plus-square" aria-hidden="true"style="color: goldenrod"></i> Infrastructure Development</div>
                                            
                                        <div class="panel panel-body">
                                            <table>
                                            <tr><td style="color: black"><strong>Road : </strong></td><td style="padding-left:50pt">Asphalt</td></tr>
                                            <tr><td style="color: black"><strong>Electricity :</strong></td><td style="padding-left:50pt">Yes</td></tr>
                                            <tr><td style="color: black"><strong>Tel communication :</strong></td><td style="padding-left:50pt">Yes</td></tr>
                                            <tr><td style="color: black"><strong>Others :</strong></td><td style="padding-left:50pt">Water supply provided</td></tr>
                                            </table>
                                        </div>
                                        </div>
                                        <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"><i class="fa fa-tree" aria-hidden="true"style="color: goldenrod"></i> Environmental Management</div>
                                            
                                        <div class="panel panel-body">
                                            <table>
                                            <tr><td style="color: black"><strong>Forest Management : </strong></td><td style="padding-left:50pt">well protected</td></tr>
                                            <tr><td style="color: black"><strong>Soil and Water Conservation :</strong></td><td style="padding-left:50pt">properly handled</td></tr>
                                            <tr><td style="color: black"><strong>Wildlife Protection :</strong></td><td style="padding-left:50pt">No wildlife around</td></tr>
                                            </table>
                                        </div>
                                        </div>  
                                        
                                        
                                        <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"><i class="fa fa-credit-card-alt" aria-hidden="true"style="color: goldenrod"></i> Source of Finance (Total in Birr)</div>
                                            
                                        <div class="panel panel-body">
                                            <table>
                                            <tr><td style="color: black"><strong>Equity : </strong></td><td style="padding-left:50pt">5000</td></tr>
                                            <tr><td style="color: black"><strong>Loan :</strong></td><td style="padding-left:50pt">2000</td></tr>
                                            <tr><td style="color: black"><strong>Another Source :</strong></td><td style="padding-left:50pt">3000</td></tr>
                                            <tr><td style="color: black"><strong>Total :</strong></td><td style="padding-left:50pt">10000</td></tr>
                                            </table>
                                        </div>
                                        </div>  
                                         <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"><i class="fa fa-dollar" aria-hidden="true"style="color: goldenrod"></i> Employment Opportunity</div>
                                            
                                        <div class="panel panel-body">
                                            <table class="table table-striped jambo_table bulk_action table-bordered">
                                                <thead>
                                                    <tr><td rowspan="2"></td><td colspan="2">Temporary Employees</td><td colspan="2">Permanent Employees</td><td rowspan="2"> Total</td><td rowspan="2">Result</td></tr>
                                                    <tr><td>Male</td><td>Female</td><td>Male</td><td>Female</td></tr>
                                                </thead>
                                                
                                                   
                                                <tr><td>Local</td><td>10</td><td>40</td><td>20</td><td>25</td><td>95</td><td>10</td></tr>
                                                <tr><td>Expatriate</td><td>10</td><td>20</td><td>30</td><td>40</td><td>100</td><td>9</td></tr>
                                            <tr><td>Total</td><td>20</td><td>60</td><td>50</td><td>65</td><td>195</td><td>9</td></tr>
                                            
                                            </table>
                                            <br><tr><td style="color: black"><strong>information if the project creates opportunity for unprivileged group :</strong></td><td style="padding-left:50pt"></td></tr>
                                        </div>
                                        </div>  
                                         <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"> <i class="fa fa-square" aria-hidden="true"style="color: goldenrod"></i> Land Requirement (in Sq. M or Ha.)</div>
                                            
                                        <div class="panel panel-body">
                                            <table>
                                                <tr><td style="color: black"><strong>Industrial : </strong></td><td style="padding-left:50pt">2500</td></tr>
                                                <tr><td style="color: black"><strong>Agricultural :</strong> </strong></td><td style="padding-left:50pt">15000</td></tr>
                                                <tr><td style="color: black"><strong>Service : </strong> </strong></td><td style="padding-left:50pt">200</td></tr>
                                            </table>
                                        </div>
                                        </div>  
                                        <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"><i class="fa fa-check-square-o" aria-hidden="true"style="color: goldenrod"></i> Production/Service</div>
                                            
                                        <div class="panel panel-body">
                                            <table class="table table-striped jambo_table bulk_action table-bordered">
                                                <thead>
                                                    <tr><td rowspan="2">No</td><td td rowspan="2">Type of product/service</td><td colspan="2">production/service capacity</td><td colspan="2">Sales Program</td><td rowspan="2">Result</td></tr>
                                                    <tr><td>Unit</td><td>Quantity</td><td>Domestic market share</td><td>Export market share</td></tr>
                                                </thead>
                                                
                                                   
                                                <tr><td>1</td><td>fruits</td><td>48</td><td>95</td><td>87</td><td>95</td><td>9</td></tr>
                                                <tr><td>2</td><td>Vegetables</td><td>48</td><td>95</td><td>87</td><td>95</td><td>10</td></tr>
                                                
                                            
                                            </table>
                                            
                                        </div>
                                        </div>
                                        <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"> <i class="fa fa-envira" aria-hidden="true"style="color: goldenrod"></i> Raw Material Requirements</div>
                                            
                                        <div class="panel panel-body">
                                            <table class="table table-striped jambo_table bulk_action table-bordered">
                                                <thead>
                                                    <tr><td rowspan="2">No</td><td rowspan="2">Type of raw material</td><td colspan="2">Source of supply</td><td rowspan="2">Result</td></tr>
                                                    <tr><td>Local</td><td>Foreign</td></tr>
                                                </thead>
                                                
                                                   
                                                <tr><td>1</td><td>fertilizers</td><td>EFA</td><td>CFA</td><td>10</td></tr>
                                                <tr><td>2</td><td>Seeds</td><td>ESA</td><td>CSA</td><td>9</td></tr>
                                            
                                            </table>
                                            
                                        </div>
                                        </div>
                                     <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"> <i class="fa fa-truck" aria-hidden="true"style="color: goldenrod"></i> Technology Used</div>
                                            
                                        <div class="panel panel-body">
                                            <table class="table table-striped jambo_table bulk_action table-bordered">
                                                <thead>
                                                    <tr><td rowspan="2">Machinery type</td><td rowspan="2">Quantity</td><td colspan="2">Source</td><td rowspan="2"> Remark</td><td rowspan="2">Result</td></tr>
                                                    <tr><td>Rent</td><td>Bought</td></tr>
                                                </thead>
                                                
                                                   
                                                <tr><td>Tractors</td><td>12</td><td>2</td><td>10</td><td></td><td>9</td></tr>
                                                <tr><td>Water pump</td><td>2</td><td>0</td><td>2</td><td></td><td>10</td></tr>
                                            
                                            
                                            </table>
                                            
                                        </div>
                                        </div>
                                    </div>
                                    
                                    <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"> <i class="fa fa-flask" aria-hidden="true"style="color: goldenrod"></i> Chemical Used</div>
                                            
                                        <div class="panel panel-body">
                                            <table class="table table-striped jambo_table bulk_action table-bordered">
                                                <thead>
                                                    <tr><td colspan="3">Herbicide</td><td colspan="3">Pesticide</td><td colspan="3">Fungicide</td><td colspan="3"> Fertilizer</td><td rowspan="2"> Others</td><td rowspan="2">Result</td></tr>
                                                    <tr><td>Type</td><td>Quantity</td><td>Source of Supply</td><td>Type</td><td>Quantity</td><td>Source of Supply</td><td>Type</td><td>Quantity</td><td>Source of Supply</td><td>Type</td><td>Quantity</td><td>Source of Supply</td>
                                                    </tr>
                                                </thead>
                                                
                                                   
                                                <tr><td>Non selective</td><td>3</td><td>EHA</td><td>insecticides</td><td>3</td><td>EHA</td><td>organic</td><td>48</td><td>EHA</td><td>Organic</td><td>3</td><td>EHA</td><td></td><td>10</td></tr>
                                            
                                            
                                            </table>
                                            
                                        </div>
                                        </div>
                                    
                                    
                                    <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"> <i class="fa fa-cubes" aria-hidden="true"style="color: goldenrod"></i> Seeds</div>
                                            
                                        <div class="panel panel-body">
                                            <table class="table table-striped jambo_table bulk_action table-bordered">
                                                <thead>
                                                    <tr><td>Crop Type</td><td>Quantity</td><td>Source of Supply</td><td>Remark</td><td>Result</td></tr>
                                                </thead>
                                                <tbody>
                                                    <tr><td>wheat </td><td>12</td><td>EHA</td><td></td><td>8</td></tr>
                                                    <tr><td>vegetables </td><td>18</td><td>EHA</td><td></td><td>10</td></tr>
                                                    <tr><td>Teff </td><td>97</td><td>EHA</td><td></td><td>10</td></tr>
                                                <tbody>                     
                                            </table>
                                            
                                        </div>
                                        </div>
                                    <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"> <i class="fa fa-square-o" aria-hidden="true"style="color: goldenrod"></i> Farm Management and Information Handling</div>
                                            
                                        <div class="panel panel-body">
                                            <table>
                                                
                                                <tr><td style="color: black"><strong>Frequency of Plowing before sowing </strong></td><td style="padding-left:50pt">5 times</td></tr>
                                                <tr><td style="color: black"><strong>Sowing Method </strong></td><td style="padding-left:50pt">line sowing</td></tr>
                                                <tr><td style="color: black"><strong>Fertilizer Quantity and Application Method </strong></td><td style="padding-left:50pt">placement</td></tr>
                                                <tr><td style="color: black"><strong>Water Management: </strong></td><td style="padding-left:50pt">efficient</td></tr>
                                                <tr><td style="color: black"><strong>Training/Technical Support </strong></td><td style="padding-left:50pt">no technical support given</td></tr>
                                                                 
                                            </table>
                                            
                                        </div>
                                        </div>
                                    <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"> <i class="fa fa-bed" aria-hidden="true"style="color: goldenrod"></i> Camp Infrastructure</div>
                                            
                                        <div class="panel panel-body">
                                            <table>
                                                
                                                <tr><td style="color: black"><strong>Worker residential house </strong></td><td style="padding-left:50pt"> huts</td></tr>
                                                <tr><td style="color: black"><strong>Water supply </strong></td><td style="padding-left:50pt">good</td></tr>
                                                <tr><td style="color: black"><strong>Medical facility </strong></td><td style="padding-left:50pt">first level clinic</td></tr>
                                                <tr><td style="color: black"><strong>Toilets and bath rooms </strong></td><td style="padding-left:50pt">two provided for each</td></tr>
                                                <tr><td style="color: black"><strong>Kitchen facilities </strong></td><td style="padding-left:50pt">provided for each</td></tr>
                                                <tr><td style="color: black"><strong>Dining hall </strong></td><td style="padding-left:50pt"></td></tr>                 
                                                <tr><td style="color: black"><strong>Refreshment and entertainment facility </strong></td><td style="padding-left:50pt">restaurant</td></tr>                 
                                                <tr><td style="color: black"><strong>Consumable items supply </strong></td><td style="padding-left:50pt">department store</td></tr>                 
                                                <tr><td style="color: black"><strong>Food supply </strong></td><td style="padding-left:50pt">department store</td></tr>                 
                                                <tr><td style="color: black"><strong>Safety outfits supplies </strong></td><td style="padding-left:50pt">two provided for each</td></tr>                 
                                            </table>
                                            
                                        </div>
                                        </div>
                                    <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"> <i class="fa fa-check" aria-hidden="true"style="color: goldenrod"></i> Creating good working environment for workers</div>
                                            
                                        <div class="panel panel-body">
                                            <table class="table table-striped jambo_table bulk_action table-bordered">
                                                
                                                <thead>
                                                    <tr><td rowspan =2>Office building</td><td rowspan="2">Machinery Shed</td><td colspan="4">Store</td><td rowspan="2">Result</td></tr>
                                                    <tr><td>production</td><td>chemicals</td><td>Fertilizers</td><td>Others</td></tr>
                                                </thead>
                                                <tr><td>3 office buildings</td><td>2 machinery sheds</td><td>5 production stores</td><td>2 chemical stores</td><td>5 fertilizer stores</td><td></td><td>10</td></tr>
                                                
                                            </table>
                                            
                                        </div>
                                        </div>
                                    <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"> <i class="fa fa-child" aria-hidden="true"style="color: goldenrod"></i> Employment</div>
                                            
                                        <div class="panel panel-body">
                                            <table class="table table-striped jambo_table bulk_action table-bordered">
                                                
                                                <thead>
                                                    <tr><td>Professionals</td><td>Quantity</td><td>Result</td></tr>
                                                </thead>
                                                <tr><td>General Manager</td><td>1</td><td>9</td></tr>
                                                <tr><td>Deputy Manager</td><td>3</td><td>10</td></tr>
                                                <tr><td>Agronomist</td><td>5</td><td>10</td>
                                                </tr>
                                            </table>
                                            
                                        </div>
                                        </div>
                                    <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"> <i class="fa fa-bullhorn" aria-hidden="true"style="color: goldenrod"></i> Social Contribution</div>
                                            
                                        <div class="panel panel-body">
                                            <table >
                                                
                                                <tr><td style="color: black"><strong>School</strong></td><td style="padding-left:50pt">1 school</td></tr>
                                                <tr><td style="color: black"><strong>Hospital</strong></td><td style="padding-left:50pt"></td></tr>
                                                <tr><td style="color: black"><strong>Road</strong></td><td style="padding-left:50pt">1 second level road</td></tr>
                                                <tr><td style="color: black"><strong>Water Facility</strong></td><td style="padding-left:50pt"></td></tr>
                                            </table>
                                            
                                        </div>
                                        </div>
                                      <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"> <i class="fa fa-circle-o-notch" aria-hidden="true"style="color: goldenrod"></i> Project Bottleneck</div>
                                            
                                        <div class="panel panel-body">
                                            <table class="table table-striped jambo_table bulk_action table-bordered">
                                                <thead>
                                                    <tr>
                                                        <td>Type</td>
                                                        <td>Remark</td>
                                                    </tr>
                                                </thead>
                                                    
                                                <tr><td>Infrastructure</td><td> No road</td></tr>
                                                <tr><td>Technology</td><td></td></tr>
                                                <tr><td>Training</td><td>No training taken</td></tr>
                                                <tr><td>Supply</td><td>no supply and demand relation created</td></tr>
                                                <tr><td>Quality Seed</td><td>no quality seed supplied</td></tr>
                                                <tr><td>Man Power</td><td>lack of agricultural irrigation experts</td></tr>
                                                <tr><td>Others</td><td></td></tr>
                                                
                                                
                                                
                                            </table>
                                            
                                        </div>
                                        </div>
                                     <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"> <i class="fa fa-plus-square" aria-hidden="true"style="color: goldenrod"></i> Project assessment total result
</div>
                                            
                                        <div class="panel panel-body">
                                            <table >
                                                
                                                <tr><td style="color: black"><strong>Result</strong></td><td style="padding-left:50pt">A</td></tr>
                                                
                                                
                                            </table>
                                            
                                        </div>
                                        </div>
                                    <div class="panel panel-primary">
                                    
                                            <div class="panel panel-heading"> <i class="fa fa-expand" aria-hidden="true"style="color: goldenrod"></i> Expansion Detail</div>
                                            
                                        <div class="panel panel-body">
                                            <table >
                                                
                                                <tr><td style="color: black"><strong>Detail Expansion Request</strong></td><td style="padding-left:50pt">Expansion Request.pdf</td></tr>
                                                <tr><td style="color: black"><strong>Area of land transferred for expansion</strong></td><td style="padding-left:50pt">500 sq.M</td></tr>
                                                
                                            </table>
                                            
                                        </div>
                                        </div>
                                   
                                    
                                    
                                    
                                    
                                    
                                    </div>
                                    <div id="nott3" class="tabcontent">
                                    <div class="panel panel-group" style="margin-top: 50pt">
                                        <div class="panel panel-primary">
                                            <div class="panel panel-heading" style="color:white"><i class="fa fa-bell-o" aria-hidden="true"style="color: goldenrod"></i></div>
                                            <div class="panel panel-body">
                                                <div class="container">
  
  <ul class="nav nav-tabs" style>
      <li class="active" style="width:200pt"><a data-toggle="tab" href="#Promotion3"><i class="fa fa-tags" aria-hidden="true"></i> Promotion</a></li>
      <li style="width:200pt"><a data-toggle="tab" href="#Bid3"><i class="fa fa-envelope-open" aria-hidden="true"></i> Bid</a></li>
    <li style="width:200pt"><a data-toggle="tab" href="#Training3"><i class="fa fa-male" aria-hidden="true"></i> Training</a></li>
    <li style="width:200pt"><a data-toggle="tab" href="#Warning3"><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Warning</a></li>
    <li style="width:200pt"><a data-toggle="tab" href="#Cert3"><i class="fa fa-file-text" aria-hidden="true"></i> Certification</a></li>
    <li style="width:200pt"><a data-toggle="tab" href="#Others3"><i class="fa fa-id-card" aria-hidden="true"></i> Others</a></li>
  </ul>

  <div class="tab-content">
    <div id="Promotion3" class="tab-pane fade in active">
      
      <table class="table table-striped table-bordered table-hover table-condensed">
      <tr><td>Promotion notification</td></tr>
      <tr><td>Promotion notification</td></tr>
      <tr><td>Promotion notification</td></tr>
      </table>
    </div>
    <div id="Bid3" class="tab-pane fade">
        <script>
            
            
            console.log("Bid");
        </script>
      
       <table class="table table-striped table-bordered table-hover table-condensed">
      <tr><td>Bid notification</td></tr>
      <tr><td>Bid notification</td></tr>
      <tr><td>Bid notification</td></tr>
      </table>
    </div>
    <div id="Training3" class="tab-pane fade">
      
       <table class="table table-striped table-bordered table-hover table-condensed">
      <tr><td>Training notification</td></tr>
      <tr><td>Training notification</td></tr>
      <tr><td>Training notification</td></tr>
      </table>
    </div>
      <div id="Warning3" class="tab-pane fade">
      
       <table class="table table-striped table-bordered table-hover table-condensed">
      <tr><td>Warning notification</td></tr>
      <tr><td>Warning notification</td></tr>
      <tr><td>Warning notification</td></tr>
      </table>
    </div>
      <div id="Cert3" class="tab-pane fade">
      
      <table class="table table-striped table-bordered table-hover table-condensed">
      <tr><td>Certification notification</td></tr>
      <tr><td>Certification notification</td></tr>
      <tr><td>Certification notification</td></tr>
      </table>
    </div>
    <div id="Others3" class="tab-pane fade">
      
      <table class="table table-striped table-bordered table-hover table-condensed">
      <tr><td>Others notification</td></tr>
      <tr><td>Others notification</td></tr>
      <tr><td>Others notification</td></tr>
      </table>
    </div>
  </div>
</div>

                                                
                                                
                                            </div>
                                            
                                        </div>
                                        
                                    </div>
                                    
                                    
                                  </div>
                            </div>
  </div>
                                  </div>
                                
                                
                                
                            </div>
                        </div>
                    </div>
                    <div class="right_col_footer">
                    </div>
                </div>
            </div>
<script>
function openTab(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}
</script>

        <script src="/assets/js/jquery.js"></script>
        <script src="/assets/js/jquery.min.js"></script>
        <script src="/assets/js/jquery-ui.js"></script>
        <script src="/assets/js/external.js"></script>
        <script src="/assets/js/Chart.min.js"></script>
        <script src="/assets/js/bootstrap-progressbar.min.js"></script>
        <script src="/assets/js/bootstrap.min.js"></script>
        <script src="/assets/js/custom.min.js"></script>
        

        
        
     
    </body>
</html>
