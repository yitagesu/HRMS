/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.security.model;

import java.util.List;

/**
 *
 * @author Tewelde
 */
public class SecurableObject {
    public String objectID;
    public String name;
    public List<String> inheritFrom;
    public String description;
}
