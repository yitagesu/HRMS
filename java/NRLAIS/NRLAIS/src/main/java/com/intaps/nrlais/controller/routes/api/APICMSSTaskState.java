/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.routes.api;


import com.google.gson.stream.JsonWriter;
import com.intaps.nrlais.Startup;
import com.intaps.nrlais.repo.TransactionFacade;
import com.intaps.nrlais.util.GSONUtil;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 *
 * @author Teweldemedhin Aberra
 */
@WebServlet("/api/task_state")

public class APICMSSTaskState extends APIBase {

    

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        GSONUtil.VoidRet ret=new GSONUtil.VoidRet();
            try
            {
                String userName=assertLogedInUser(req, resp);
                if(userName==null)
                    return;
                String task_uid=(String)req.getParameter("task_uid");
                new TransactionFacade(Startup.getSessionByRequest(req)).commitTask(task_uid);                
                ret.error=null;
            } catch (Exception ex) {
                Logger.getLogger(APICMSSTaskState.class.getName()).log(Level.SEVERE, null, ex);
                ret.error="Error"+(ex.getMessage()==null?"":ex.getMessage());
            } 
        
        try (ServletOutputStream str = resp.getOutputStream()) {
                resp.setContentType("application/json");
                JsonWriter writer=new JsonWriter(new OutputStreamWriter(str,"UTF-8"));
                GSONUtil.getAdapter(GSONUtil.VoidRet.class).write(writer,ret);
                writer.close();
            }
    }
}
