/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.model.tran;

import com.intaps.nrlais.util.RationalNum;
import java.util.List;

/**
 *
 * @author Tewelde
 */
public class Partition {
    
    public String parcelUID;
    public List<PartyParcelShare> shares;

    public boolean hasShared()
    {
         for (PartyParcelShare sh : shares) {
            if(sh.splitGeom)
                continue;
           return true;
        }
         return false;
    }
    public boolean hasSplit()
    {
         for (PartyParcelShare sh : shares) {
            if(!sh.splitGeom)
                continue;
           return true;
        }
         return false;
    }
    public boolean is100Percent() {
        RationalNum total = new RationalNum(0, 1);
        for (PartyParcelShare sh : shares) {
            if(sh.splitGeom)
                continue;
            if (sh.share.denum == 0) {
                throw new IllegalArgumentException("Share denumerator can't be zero");
            }
            if (sh.share.num == 0) {
                continue;
            }
            total.add(sh.share);
        }
        
        return total.isOne();
    }
    
}
