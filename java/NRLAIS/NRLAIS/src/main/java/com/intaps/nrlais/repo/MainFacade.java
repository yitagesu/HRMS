/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.repo;

import com.intaps.nrlais.Startup;
import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.CMSSTask;
import com.intaps.nrlais.model.CMSSTaskGeom;
import com.intaps.nrlais.model.IDName;
import com.intaps.nrlais.model.IDNameText;
import com.intaps.nrlais.model.KebeleInfo;
import com.intaps.nrlais.model.WoredaInfo;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.RegionInfo;
import com.intaps.nrlais.model.SearchApplicationPars;
import com.intaps.nrlais.model.SearchHoldingPars;
import com.intaps.nrlais.model.SourceDocument;
import com.intaps.nrlais.model.ZoneInfo;
import com.intaps.nrlais.model.map.MapData;

import com.vividsolutions.jts.io.ParseException;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import javax.crypto.Mac;

/**
 *
 * @author Tewelde
 */
public class MainFacade {

    UserSession session;

    public MainFacade(UserSession session) {
        this.session = session;
    }

    public LADM getLADM(String schema, String holdinguid) throws SQLException, IOException {
        return getLADM(schema, holdinguid, LADM.CONTENT_FULL);
    }

    public LADM getLADM(String schema, String holdinguid, int contentDepth) throws SQLException, IOException {
        try (Connection con = Startup.getNRLAISConnection()) {

            return new GetLADM(session, schema).get(con, holdinguid, LADM.CONTENT_FULL);
        }
    }

    public LADM getLADMByTx(String txuid, String holdinguid) throws SQLException, IOException {
        try (Connection con = Startup.getNRLAISConnection()) {
            return new GetLADM(session, null).getByTx(con, txuid, holdinguid);
        }
    }

    public LADM.Party getParty(String schema, String partyUID) throws SQLException {
        try (Connection con = Startup.getNRLAISConnection()) {
            return new GetLADM(session, schema).getParty(con, partyUID, LADM.CONTENT_FULL);
        }
    }

    public List<KebeleInfo> getAllKebeles() throws SQLException {
        try (Connection con = Startup.getNRLAISConnection()) {
            return new KebeleRepo(session).getAllKebeles(con);
        }
    }

    public KebeleInfo getKebele(String kebeleID) throws SQLException {
        try (Connection con = Startup.getNRLAISConnection()) {
            return new KebeleRepo(session).get(con, kebeleID);
        }
    }

    public List<WoredaInfo> getAllWoredas() throws SQLException {
        try (Connection con = Startup.getNRLAISConnection()) {
            return new WoredaRepo(session).getAllWoredas(con);
        }
    }

    public WoredaInfo getWoreda(String woredaID) throws SQLException {
        try (Connection con = Startup.getNRLAISConnection()) {
            return new WoredaRepo(session).get(con, woredaID);
        }
    }

    public ZoneInfo getZone(String zoneID) throws SQLException {
        try (Connection con = Startup.getNRLAISConnection()) {
            return new ZoneRepo(session).get(con, zoneID);
        }
    }

    public RegionInfo getRegion(String regionID) throws SQLException {
        try (Connection con = Startup.getNRLAISConnection()) {
            return new RegionRepo(session).get(con, regionID);
        }
    }

    public LATransaction getTransaction(String txuid, boolean includeDetail) throws SQLException {
        try (Connection con = Startup.getNRLAISConnection()) {
            return new GetTransaction(session).get(con, txuid, includeDetail);
        }
    }

    public List<LATransaction> searchTransaction(SearchApplicationPars pars, boolean includeDetail, int pageOffset, int pageSize) throws IOException, SQLException {
        try (Connection con = Startup.getNRLAISConnection()) {
            return new SearchApplication(session).searchTransaction(con, pars, includeDetail, pageOffset, pageSize);
        }
    }

    public List<LADM> searchHolding(SearchHoldingPars pars, int pageOffset, int pageSize) throws IOException, SQLException {
        try (Connection con = Startup.getNRLAISConnection()) {
            return new SearchHolding(session).searchHolding(con, pars, pageOffset, pageSize);
        }

    }

    public List<CMSSTask> getTaskByTranID(String tranUID) throws SQLException {
        try (Connection con = Startup.getNRLAISConnection()) {
            return new GetCMSSTask(session).getTaskforTransaction(con, tranUID, true);
        }
    }

    public List<CMSSTask> getPendingTasks(boolean includeDetail) throws SQLException {
        try (Connection con = Startup.getNRLAISConnection()) {
            return new GetCMSSTask(session).getPendingTasks(con, includeDetail);
        }
    }

    public LADM.Parcel getParcel(String schema, String parcelUID) throws SQLException {
        return getParcel(schema, parcelUID, LADM.CONTENT_FULL);
    }

    public LADM.Parcel getParcel(String schema, String parcelUID, int contentDepth) throws SQLException {
        try (Connection con = Startup.getNRLAISConnection()) {
            return new GetLADM(session, schema).getParcel(con, parcelUID, contentDepth);
        }
    }

    public CMSSTask getTask(String taskID, boolean includeDetail) throws SQLException {
        try (Connection con = Startup.getNRLAISConnection()) {
            return new GetCMSSTask(session).getTask(con, taskID, includeDetail);
        }
    }

    public List<CMSSTask> getTaskforTransaction(String txuid, boolean includeDetail) throws SQLException {
        try (Connection con = Startup.getNRLAISConnection()) {
            return new GetCMSSTask(session).getTaskforTransaction(con, txuid, includeDetail);
        }
    }

    public List<CMSSTaskGeom> getTaskGeometries(String taskuid) throws SQLException {
        try (Connection con = Startup.getNRLAISConnection()) {
            return new GetCMSSTask(session).getTaskGeometries(con, taskuid);
        }
    }

    public List<IDName> getAllLookup(String tableName) throws SQLException {
        if (LookupRepo.staticLookups.containsKey(tableName)) {
            return LookupRepo.staticLookups.get(tableName).getAll(null);
        }
        try (Connection con = Startup.getNRLAISConnection()) {
            return new LookupRepo(tableName).getAll(con);
        }
    }

    public List<IDNameText> getAllLookupText(String tableName) throws SQLException {
        List<IDNameText> ret = new ArrayList<>();
        for (IDName n : getAllLookup(tableName)) {
            ret.add(n.idNameText(session.lang()));
        }
        return ret;
    }

    public IDName getLookup(String tableName, int id) throws SQLException {
        if (LookupRepo.staticLookups.containsKey(tableName)) {
            return LookupRepo.staticLookups.get(tableName).get(null, id);
        }
        try (Connection con = Startup.getNRLAISConnection()) {
            return new LookupRepo(tableName).get(con, id);
        }
    }

    public SourceDocument getDocument(String doc_uid, boolean includeImage) throws SQLException, IOException {
        try (Connection con = Startup.getNRLAISConnection()) {
            return new GetTransaction(session).getDocument(con, doc_uid, includeImage);
        }
    }

    public void changeTransactionState(String txuid, String cmd, String note) throws SQLException, Exception {
        Connection con = Startup.getWriteConnection();
        TransactionRepo repo=new TransactionRepo(session);
        synchronized (con) {
            con.setAutoCommit(true);
            repo.loadTransaction(con, txuid);
            con.setAutoCommit(false);
            try {
                new TransactionRepo(session).changeState(con, txuid, cmd, note);
                con.commit();
            } catch (Exception ex) {
                con.rollback();
                throw ex;
            }
        }
    }

    public boolean isTransctionCMSSTaskDone(String transactionUID) throws SQLException {
        try (Connection con = Startup.getNRLAISConnection()) {
            return new GetCMSSTask(session).isTransactionDone(con, transactionUID);
        }
    }

    public LADM getFromHistoryByArchiveTx(String txuid, String holdingUID) throws SQLException, IOException {
        try (Connection con = Startup.getNRLAISConnection()) {
            return new GetLADM(session, null).getFromHistoryByArchiveTx(con, txuid, holdingUID);
        }
    }

    public LADM getFromHistoryByParcelArchiveTx(String txuid, String parcelUID) throws SQLException, IOException {
        try (Connection con = Startup.getNRLAISConnection()) {
            return new GetLADM(session, null).getFromHistoryByParcelArchiveTx(con, txuid, parcelUID);
        }
    }

    public LADM getFromHistoryBySourceTx(String txuid, String holdingUID) throws SQLException, IOException {
        try (Connection con = Startup.getNRLAISConnection()) {
            return new GetLADM(session, null).getFromHistoryBySourceTx(con, txuid, holdingUID);
        }
    }

    public LADM getByTx(String txuid, String holdingUID) throws SQLException, IOException {
        try (Connection con = Startup.getNRLAISConnection()) {
            return new GetLADM(session, null).getByTx(con, txuid, holdingUID);
        }
    }

    public LADM.Parcel getHistoryParcelBySourceTx(String txuid, String parcelUID) throws SQLException, IOException {
        try (Connection con = Startup.getNRLAISConnection()) {
            return new GetLADM(session, null).getHistoryParcelBySourceTx(con, txuid, parcelUID);
        }
    }

    public GetMap.MapSetting getInitialMapSetting() throws SQLException {
        try (Connection con = Startup.getNRLAISConnection()) {
            return new GetMap(session).getDefaultSetting(con);
        }
    }

    public MapData getHoldingParcels(String schema, String holdingUID) throws IOException, SQLException {
        try (Connection con = Startup.getNRLAISConnection()) {
            return new GetMap(session).getHoldingParcels(con, schema, holdingUID);
        }
    }

    public MapData getSingleParcel(String schema, String parcelUID) throws IOException, SQLException, ParseException {
        try (Connection con = Startup.getNRLAISConnection()) {
            return new GetMap(session).getSingleParcel(con, schema, parcelUID);
        }
    }
    
     public MapData getSingleKebele(String schema, String kebeleCode) throws IOException, SQLException, ParseException {
        try (Connection con = Startup.getNRLAISConnection()) {
            return new GetMap(session).getKebeleGeo(con, schema, kebeleCode);
        }
    }

    public MapData getTranscationParcels(String tranUID) throws IOException, SQLException, ParseException {
        try (Connection con = Startup.getNRLAISConnection()) {
            return new GetMap(session).getTranscationParcels(con, tranUID);
        }
    }

    public List<LATransaction.TranactionFlow> getTransactionFlow(String txuid) throws SQLException {
        try (Connection con = Startup.getNRLAISConnection()) {
            return new GetTransaction(session).getTransactionFlow(con, txuid);
        }
    }

    public List<String> getTransactionInputHoldings(String transactionUID) throws SQLException {
        try (Connection con = Startup.getNRLAISConnection()) {
            return new GetTransaction(session).getTransactionInputHoldings(con, transactionUID);
        }
    }

    public List<String> getTransactionOutputHoldings(String transactionUID) throws SQLException {
        try (Connection con = Startup.getNRLAISConnection()) {
            return new GetTransaction(session).getTransactionOutputHoldings(con, transactionUID);
        }
    }

    public List<LATransaction> getHoldingHistory(String tranUID, String holdingUID) throws SQLException, IOException {
        try (Connection con = Startup.getNRLAISConnection()) {
            return new GetTransaction(session).getHoldingHistory(con, tranUID, holdingUID);
        }
    }

    public List<LADM.Parcel> getAdjacentParcels(String schema, String parcelUID, int contentDepth) throws SQLException {
        try (Connection con = Startup.getNRLAISConnection()) {
            return new GetLADM(session, schema).getAdjacentParcels(con, parcelUID, contentDepth);
        }
    }

    public LADM getLADMByParcel(String schema, String parcelUID) throws SQLException, IOException {
        try (Connection con = Startup.getNRLAISConnection()) {
            return new GetLADM(session, schema).getByParcel(con, parcelUID, LADM.CONTENT_FULL);
        }
    }

    public List<LADM.Party> getPartiesByTran(String schema, String tranUID) throws SQLException {
        try (Connection con = Startup.getNRLAISConnection()) {
            return new GetLADM(session, schema).getPartiesByTran(con, tranUID);
        }
    }

    public List<LADM.Holding> getTransactionHoldings(String transactionUID, int contentDepth) throws SQLException, IOException {
        try (Connection con = Startup.getNRLAISConnection()) {
            return new GetLADM(session, "nrlais_transaction").getTransactionHoldings(con, transactionUID, contentDepth);
        }
    }

    public String getHoldingIDForParcel(String schema, String parcelUID) throws SQLException {
        try (Connection con = Startup.getNRLAISConnection()) {
            return new GetLADM(session, schema).getHoldingIDForParcel(con, parcelUID);
        }
    }

    public String getSystemLevel() throws SQLException {
         try (Connection con = Startup.getNRLAISConnection()) {
            return session.worlaisSession.getLevel(con);
        }
    }

}
