/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.model.tran;

import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.SourceDocument;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tewelde
 */
public class ExchangeTransactionData extends LATransaction.TransactionData {
    public String holdingUID1;
    public List<String> parcelUID1;
    public String holdingUID2;
    public List<String> parcelUID2;
    public List<Applicant> applicants1=new ArrayList<>();
    public List<Applicant> applicants2=new ArrayList<>();
    public SourceDocument claimResolutionDocument;
    public SourceDocument landHoldingCertifcateDocument1;
    public SourceDocument landHoldingCertifcateDocument2;
}
