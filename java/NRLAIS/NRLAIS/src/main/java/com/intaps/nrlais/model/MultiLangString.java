/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.model;

/**
 *
 * @author Tewelde
 */
public class MultiLangString {
    public String textEn;
    public String textAm;
    public String textTi;
    public String textOm;

    public MultiLangString(String en,String am, String om, String ti) {
        this.textEn=en;
        this.textAm=am;
        this.textOm=om;
        this.textTi=ti;
    }    

    public String text(String lang) {
        switch(lang)
        {
            case "am-et":return textAm;
            case "om-et":return textOm;
            case "ti-et":return textTi;
            default:return textEn;
        }
    }
}
