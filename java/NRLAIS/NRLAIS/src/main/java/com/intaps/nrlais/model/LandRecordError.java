/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.model;

/**
 *
 * @author Tewelde
 */
public class LandRecordError {

    public static final int LR_ERROR_SUR_PARTIAL = 1;
    public static final int LR_ERROR_NO_HOLDING_RIGHT = 2;
    public static final int LR_ERROR_SUR_PUR_MIXED = 3;
    public static final int LR_ERROR_NONE_UNIFORM_HOLDING_RIGHT = 4;
    public static final int LR_ERROR_PARTY_SEX_ROLE_MISMATCH = 5;
    public static final int LR_ERROR_SUM_OF_SHARE_NOT_ONE = 6;
    public static final int LR_ERROR_NO_HOLDING = 7;
    public static final int LR_ERROR_ZERO_PARCEL = 8;
    public static final int LR_ERROR_AMBIGOUS_COUPLE = 9;
    public static final int LR_ERROR_KEBLE_MISMATCH = 10;
    public static final int LR_ERROR_PARTY_HOLDING_TYPE_MISMATCH = 11;
    public static final int LR_ERROR_MUTIPLE_LEASE = 12;
    public static final int LR_ERROR_PARTIAL_LEASE = 13;
    public static final int LR_ERROR_OVER_SIZE_RENT = 14;

    public int errorCode;
    public String description;

    public LandRecordError(int erroCode, String description) {
        this.errorCode = erroCode;
        this.description = description;
    }

    public static LandRecordError create(int code) {
        switch (code) {
            case LR_ERROR_SUR_PARTIAL:
                return new LandRecordError(code, "State use right can't be partial");
            case LR_ERROR_NO_HOLDING_RIGHT:
                return new LandRecordError(code, "The holding should have at least one party with holding right");
            case LR_ERROR_SUR_PUR_MIXED:
                return new LandRecordError(code, "State use right and perpetual user right can't be mixed in a single holding");
            case LR_ERROR_NONE_UNIFORM_HOLDING_RIGHT:
                return new LandRecordError(code, "Holding rights can't be different for different parcels");
            default:
                return new LandRecordError(code, "There is an error on the land record");
        }
    }
}
