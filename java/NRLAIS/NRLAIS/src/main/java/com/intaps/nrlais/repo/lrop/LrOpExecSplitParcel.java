/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.repo.lrop;

import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.tran.ManualTransactionData;
import com.intaps.nrlais.repo.GetLADM;
import com.intaps.nrlais.repo.LRManipulator;
import com.intaps.nrlais.repo.ManualLRManipulatorFacade;
import java.io.IOException;
import java.sql.Connection;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Tewelde
 */
public class LrOpExecSplitParcel implements ManualLRManipulatorFacade.LROPExecutor<ManualTransactionData.LrOpSplitParcel> {

    
    @Override
    public Object doOperation(UserSession session, Connection con, LATransaction tran, ManualTransactionData.LrOpSplitParcel op) throws Exception {
        LRManipulator lrm = new LRManipulator(session);
        GetLADM ladm = new GetLADM(session, "nrlais_transaction");
       
        LADM.Holding holding = ladm.getByParcel(con, op.parcelUID, LADM.CONTENT_FULL).holding;
        LADM.Parcel parcel=holding.getParcel(op.parcelUID);
        if(op.n<2)
            throw new IllegalArgumentException("Invalid number of parcels");
        double minSize = session.worlaisSession.getMinAreaConfig(con,null);
        if(parcel.areaGeom==0)
            throw new IllegalStateException("The parcel doesn't have geometry yet");
        if(parcel.areaGeom/op.n<minSize)
            throw new IllegalArgumentException("This parcel is too small for the split operation");
        List<Map.Entry<String, LADM.Parcel>> holdings=new ArrayList<>();
        for(int i=0;i<op.n;i++)
        {
            LADM.Parcel splitParcel=parcel.clonedObjectNoRR();
            splitParcel.restrictions.addAll(parcel.restrictions);
            splitParcel.rights.addAll(parcel.rights);
            splitParcel.resetGeom();
            holdings.add(new AbstractMap.SimpleEntry<String, LADM.Parcel>(holding.holdingUID,splitParcel));
        }
        lrm.lrStartTransaction(con, tran, holding.holdingUID);
        lrm.lrSplitParcel(con, tran, op.parcelUID, holding.holdingUID,holdings);
        lrm.finishTransaction(con, tran,false);
        return null;        
    }

}
