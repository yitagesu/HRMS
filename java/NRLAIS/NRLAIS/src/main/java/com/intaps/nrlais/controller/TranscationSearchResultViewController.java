/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller;

import com.intaps.nrlais.Startup;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.SearchApplicationPars;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Tewelde
 */
public class TranscationSearchResultViewController extends ViewControllerBase {

    public List<LATransaction> tran;

    public TranscationSearchResultViewController(HttpServletRequest request, HttpServletResponse response) throws IOException, SQLException {
        super(request, response);

        SearchApplicationPars sPar = new SearchApplicationPars();
        String tranSeqNr = request.getParameter("tran_seq_nr");
        if (StringUtils.isEmpty(tranSeqNr)) {
            this.session = Startup.getSessionByRequest(request);
            if (session.worlaisSession.isOffier()) {
                sPar.transactionStatus.add(LATransaction.TRAN_STATUS_INITIATED);
                sPar.transactionStatus.add(LATransaction.TRAN_STATUS_ACCEPTED);
            }
            if (session.worlaisSession.isExpert()) {
                sPar.transactionStatus.add(LATransaction.TRAN_STATUS_REGISTERD);
            }
            if (session.worlaisSession.isSupper()) {
                sPar.transactionStatus.add(LATransaction.TRAN_STATUS_ACCEPT_REQUESTED);
                sPar.transactionStatus.add(LATransaction.TRAN_STATUS_CANCEL_REQUESTED);

            }
        } else {
            sPar.applicationSeqNo=Integer.parseInt(tranSeqNr);
        }
        tran = mainFacade.searchTransaction(sPar, false, 0, 100);
    }

}
