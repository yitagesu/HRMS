/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.tran;

import com.intaps.nrlais.model.IDNameText;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.SourceDocument;
import com.intaps.nrlais.model.tran.Applicant;
import com.intaps.nrlais.model.tran.ParcelTransfer;
import com.intaps.nrlais.model.tran.RestrictiveInterestTransactionData;
import com.intaps.nrlais.util.GSONUtil;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Tewelde
 */
public class RestrictiveInterestViewController extends RestrictionViewController<RestrictiveInterestTransactionData> {

    
    public RestrictiveInterestViewController(HttpServletRequest request, HttpServletResponse response) throws Exception {
        super(request, response, RestrictiveInterestTransactionData.class, LATransaction.TRAN_TYPE_RESTRICTIVE_INTEREST);
    }

    public RestrictiveInterestViewController(HttpServletRequest request, HttpServletResponse response, boolean loadData) throws Exception {
        super(request, response, RestrictiveInterestTransactionData.class, LATransaction.TRAN_TYPE_RESTRICTIVE_INTEREST, loadData);
        if (loadData) {
            if (data.holdingUID == null) {
                holding = new LADM.Holding();
            } else {
                holding = mainFacade.getLADM("nrlais_inventory", data.holdingUID).holding;
            }
            kebele = mainFacade.getKebele(holding.nrlais_kebeleid);
            woreda = mainFacade.getWoreda(holding.nrlais_woredaid);
        }
    }

    public String agreementRef() {
        return super.showDocumentText(this.data.agreementDoc);
    }

    public String agreementDesc() {
        return super.showDocumentDescription(this.data.agreementDoc);
    }

    @Override
    public List<IDNameText> restrictionTypes() throws SQLException {
        return mainFacade.getAllLookupText("static.restrictive_interest");
    }

    @Override
    public String tranTypeText() {
        return "Restriction";
    }
    
}
