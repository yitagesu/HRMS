/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.tran;

import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.SourceDocument;
import com.intaps.nrlais.model.tran.Applicant;
import com.intaps.nrlais.model.tran.GiftTransactionData;
import com.intaps.nrlais.model.tran.ParcelTransfer;
import com.intaps.nrlais.model.tran.PartyItem;
import com.intaps.nrlais.model.tran.TransferTransactionData;
import com.intaps.nrlais.repo.DocumentTypeRepo;
import com.intaps.nrlais.repo.GetLADM;
import com.intaps.nrlais.repo.LRManipulator;
import com.intaps.nrlais.repo.TransactionRepo;
import com.intaps.nrlais.util.EthiopianCalendar;
import com.intaps.nrlais.util.INTAPSDateUtils;
import com.intaps.nrlais.util.RationalNum;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 *
 * @author Tewelde
 */
public abstract class TransferTransactionHandler extends TransactionHandlerBase {

    protected abstract SourceDocument[] sourceDocs(TransferTransactionData data);

    protected abstract int[] sourceDocTypes(TransferTransactionData data);
void validateTransfer(UserSession session,LADM.Holding source,LADM.Holding dest)
{
    
}
    void registerTransaction(UserSession session, Connection con, LATransaction theTransaction, TransferTransactionData tran) throws SQLException, Exception {
        LRManipulator lrm = new LRManipulator(session);
        TransactionRepo tranRep = new TransactionRepo(session);
        GetLADM repo = new GetLADM(session, "nrlais_transaction");
        LADM ladm = repo.get(con, tran.holdingUID, LADM.CONTENT_FULL);
        LADM.Holding holding = ladm.holding;

        List<LADM.Right> holders = holding.getHolders();
        //check subjects

        HashMap<String, Applicant> applicants = new HashMap<>();
        HashMap<String, LADM.Right> applicantRights = new HashMap<>();

        for (Applicant ap : tran.applicants) {
            if (applicants.containsKey(ap.selfPartyUID)) {
                throw new IllegalArgumentException("Duplicate applicant " + ap.selfPartyUID);
            }
            LADM.Right r = null;

            for (LADM.Right thisR : holders) {
                if (thisR.partyUID.equals(ap.selfPartyUID)) {
                    r = thisR;
                    break;
                }
            }

            if (r == null) {
                throw new IllegalArgumentException("Transfer subject is not one of the holders.");
            }

            applicantRights.put(ap.selfPartyUID, r);
            applicants.put(ap.selfPartyUID, ap);

        }

        //save documents
        SourceDocument[] allDocs = this.sourceDocs(tran);
        int[] allTypes = this.sourceDocTypes(tran);
        DocumentTypeRepo docTypeRepo = new DocumentTypeRepo(session);
        for (int i = 0; i < allDocs.length; i++) {
            SourceDocument srcDoc = allDocs[i];
            if (srcDoc == null) {
                throw new IllegalArgumentException("Source document of type: " + docTypeRepo.getDocumentType(con, allTypes[i]).name.textEn + " is not provided");
            }
            srcDoc.uid = UUID.randomUUID().toString();
            srcDoc.sourceType = allTypes[i];
            tranRep.saveDocument(con, theTransaction, srcDoc);
        }

        lrm.lrStartTransaction(con, theTransaction, holding.holdingUID);

        HashMap<String, ParcelTransfer> transfers = new HashMap<String, ParcelTransfer>();
        tran.transfers.forEach((p) -> {
            transfers.put(p.parcelUID, p);
        });

        if (tran.tranferType == GiftTransactionData.TRANSFER_TYPE_NEW_HOLDING
                || tran.tranferType == GiftTransactionData.TRANSFER_TYPE_OTHER_HOLDING) {
            if (tran.beneficiaries.isEmpty()) {
                throw new IllegalArgumentException("The beneficiaries are not registered");
            }
            String destHoldingUID = null;

            if (tran.tranferType == GiftTransactionData.TRANSFER_TYPE_OTHER_HOLDING) {
                LADM dest = repo.get(con, tran.otherHoldingUID, LADM.CONTENT_FULL);
                if (dest == null) {
                    throw new IllegalArgumentException("Destination holding not found in transaction schema. uid: " + tran.otherHoldingUID);
                }

                destHoldingUID = dest.holding.holdingUID;

                HashMap<String, PartyItem> partyItems = new HashMap<String, PartyItem>();
                tran.beneficiaries.forEach((p) -> {
                    if (p.existingPartyUID == null) {
                        throw new IllegalArgumentException("All beneficiaries must be existing parties");
                    }
                    partyItems.put(p.existingPartyUID, p);
                });
                for (LADM.Right r : dest.holding.getHolders()) {
                    if (!partyItems.containsKey(r.partyUID)) {
                        throw new IllegalArgumentException("All holders of the destination holding must apply for this transcation.");
                    }
                    partyItems.get(r.partyUID).share = r.share;
                }
                validateTransfer(session,holding, dest.holding);
            } else {
                LADM.Holding newHolding = new LADM.Holding();
                newHolding.holdingType = LADM.Holding.HOLDING_TYPE_PRIVATE;
                validateTransfer(session,holding, newHolding);
                destHoldingUID = lrm.lrCreateNewHolding(con, theTransaction, newHolding);
            }
            RationalNum total = new RationalNum();
            for (PartyItem beneficiary : tran.beneficiaries) {
                if (beneficiary.idDoc != null) {
                    beneficiary.idDoc.uid = UUID.randomUUID().toString();
                    tranRep.saveDocument(con, theTransaction, beneficiary.idDoc);
                } else {
                    throw new IllegalArgumentException("ID information must be provided for all the beneficiary(ies)");
                }

                if (beneficiary.existingPartyUID != null) {
                    beneficiary.party = repo.getParty(con, beneficiary.existingPartyUID, LADM.CONTENT_FULL);
                }
                if (INTAPSDateUtils.calculateAgeEth(beneficiary.party.dateOfBirth, theTransaction.time) >= 18) {
                    if (beneficiary.proofOfMaritalStatus != null) {
                        beneficiary.proofOfMaritalStatus.uid = UUID.randomUUID().toString();
                        tranRep.saveDocument(con, theTransaction, beneficiary.proofOfMaritalStatus);
                    } else {
                        throw new IllegalArgumentException("Proof of marital status information must be provided for all the beneficiary(ies)");
                    }
                }
                if (beneficiary.representative != null) {
                    if (beneficiary.letterOfAttorneyDocument != null) {
                        beneficiary.letterOfAttorneyDocument.uid = UUID.randomUUID().toString();
                        tranRep.saveDocument(con, theTransaction, beneficiary.letterOfAttorneyDocument);
                    } else {
                        throw new IllegalArgumentException("Letter of attorney information must be provided for all representative(s)");
                    }
                }
                total.add(beneficiary.share);
            }
            if (!total.isOne()) {
                throw new IllegalArgumentException("Total share of the beneficiaries should be 100%");
            }

            Iterator<LADM.Parcel> parcelIter = holding.parcels.iterator();
            double minSize = session.worlaisSession.getMinAreaConfig(con, ladm);

            while (parcelIter.hasNext()) {
                LADM.Parcel parcel = parcelIter.next();
                if (!transfers.containsKey(parcel.parcelUID)) {
                    continue;
                }
                ParcelTransfer transfer = transfers.get(parcel.parcelUID);

                if (transfer.splitParcel) {
                    if (parcel.areaGeom < minSize) {
                        throw new IllegalArgumentException("This parcel can't be split because it is too small");
                    }

                    LADM.Parcel splitParcel = parcel.clonedObjectNoRR();
                    List<Map.Entry<String, LADM.Parcel>> toHoldings = new ArrayList<>();

                    splitParcel.resetGeom();
                    splitParcel.mreg_actype = LADM.Parcel.AC_TYPE_GIFT;
                    splitParcel.mreg_acyear = EthiopianCalendar.ToEth(theTransaction.time).Year;
                    for (PartyItem beneficiary : tran.beneficiaries) {
                        LADM.Right r = new LADM.Right();

                        r.party = beneficiary.party;

                        r.share = beneficiary.share;
                        r.rightType = r.party.partyType == LADM.Party.PARTY_TYPE_STATE ? LADM.Right.RIGHT_SUR : LADM.Right.RIGHT_PUR;
                        splitParcel.rights.add(r);
                    }
                    toHoldings.add(new AbstractMap.SimpleEntry<>(destHoldingUID, splitParcel));

                    splitParcel = parcel.clonedObjectNoRR();
                    splitParcel.resetGeom();
                    splitParcel.rights.addAll(parcel.rights);
                    toHoldings.add(new AbstractMap.SimpleEntry<>(holding.holdingUID, splitParcel));

                    lrm.lrSplitParcel(con, theTransaction, parcel.parcelUID, holding.holdingUID, toHoldings);
                } else {
                    List<LADM.Right> rights = new ArrayList<>();
                    for (PartyItem beneficiary : tran.beneficiaries) {
                        LADM.Right r = new LADM.Right();
                        if (beneficiary.existingPartyUID != null) {
                            r.party = repo.getParty(con, beneficiary.existingPartyUID, LADM.CONTENT_FULL);
                        } else {
                            r.party = beneficiary.party;
                        }
                        r.share = beneficiary.share;
                        r.rightType = r.party.partyType == LADM.Party.PARTY_TYPE_STATE ? LADM.Right.RIGHT_SUR : LADM.Right.RIGHT_PUR;
                        rights.add(r);
                    }
                    lrm.lrTransferParcel(con, theTransaction, parcel.parcelUID, holding.holdingUID, destHoldingUID, LADM.Parcel.AC_TYPE_GIFT, EthiopianCalendar.ToEth(theTransaction.time).Year, rights);
                }
            }//parcels iterator

        }//if transfer to new or exsting
        else {

            RationalNum givenAway = new RationalNum(0, 1);
            for (Applicant ap : tran.applicants) {
                LADM.Right right = applicantRights.get(ap.selfPartyUID);
                RationalNum given = RationalNum.multiply(ap.share, right.share);
                givenAway.add(given);
                given.num = -given.num;
                right.share.add(given);

            }
            if (givenAway.isOne()) {
                throw new IllegalArgumentException("You are tranfering all the rights of the current holders, use the transfer to new holding option");
            }
            if (givenAway.isZero()) {
                throw new IllegalArgumentException("This applicatoin is invalid becuase no share is transfered to the beneficiaries.");
            }
            Iterator<LADM.Parcel> parcelIter = holding.parcels.iterator();
            while (parcelIter.hasNext()) {

                LADM.Parcel parcel = parcelIter.next();
                List<LADM.Right> newRights = new ArrayList<LADM.Right>();
                newRights.addAll(applicantRights.values());
                for (LADM.Right r : parcel.rights) {
                    if (!applicantRights.containsKey(r.partyUID)) {
                        newRights.add(r);
                    }
                }
                for (PartyItem beneficiary : tran.beneficiaries) {
                    LADM.Right r = new LADM.Right();
                    r.party = beneficiary.party;
                    r.share = RationalNum.multiply(givenAway, beneficiary.share);
                    r.rightType = holding.holdingType == LADM.Holding.HOLDING_TYPE_STATE ? LADM.Right.RIGHT_SUR : LADM.Right.RIGHT_PUR;
                    newRights.add(r);
                }
                lrm.lrUpateRights(con, theTransaction, holding.holdingUID, parcel.parcelUID, newRights);
            }

        }
        lrm.finishTransaction(con, theTransaction);
    }

    public void saveTransaction(UserSession session, Connection con,
            LATransaction theTransaction, boolean register) throws Exception {
        try {
            TransferTransactionData tran = (TransferTransactionData) theTransaction.data;
            LADM existing = new GetLADM(session, "nrlais_inventory").get(con, tran.holdingUID, LADM.CONTENT_FULL);

            String err = existing.getRuleErrorsAsString(theTransaction.time);
            if (!org.apache.commons.lang3.StringUtils.isEmpty(err)) {
                throw new IllegalStateException(err);
            }

            if (!register) {
                return;
            }

            super.transferLandRecordToTransaction(session, con, theTransaction.transactionUID, existing);
            if (tran.tranferType == GiftTransactionData.TRANSFER_TYPE_OTHER_HOLDING) {
                LADM holding2 = new GetLADM(session, "nrlais_inventory").get(con, tran.otherHoldingUID, LADM.CONTENT_FULL);
                if (holding2 == null) {
                    throw new IllegalArgumentException("Destination holding not found. uid: " + tran.otherHoldingUID);
                }
                super.transferLandRecordToTransaction(session, con, theTransaction.transactionUID, holding2);
            }
            registerTransaction(session, con, theTransaction, tran);

        } catch (CloneNotSupportedException | IOException ex) {
            throw new SQLException("Can't execute transaction", ex);
        }
    }
}
