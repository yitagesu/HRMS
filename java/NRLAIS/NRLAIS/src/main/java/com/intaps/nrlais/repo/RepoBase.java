/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.repo;

import com.intaps.nrlais.model.NRLAISEntity;
import com.intaps.nrlais.model.NRLAISTransactionEntity;
import com.intaps.nrlais.Startup;
import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.DocumentTypeInfo;
import com.intaps.nrlais.model.IDName;
import com.intaps.nrlais.model.KebeleInfo;
import com.intaps.nrlais.model.NRLAISHistoryEntity;
import com.intaps.nrlais.util.GSONUtil;
import com.intaps.nrlais.util.NamedPreparedStatement;
import com.intaps.nrlais.util.SynchronizedConnection;
import java.rmi.AccessException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;
import java.util.UUID;

/**
 *
 * @author Tewelde
 */
public class RepoBase {

    protected UserSession session;

    public RepoBase(UserSession session) {
        this.session = session;
    }

    protected int getSeqNo(Connection con, String kebele, String table) throws SQLException {
        int lastVal;
        boolean update;
        try (ResultSet rs = con.prepareStatement("Select max_seqnr from nrlais_tx_pkg." + table + " WHERE kebeleid = '" + KebeleInfo.getKebeleCode(kebele) + "'").executeQuery()) {
            if (rs.next()) {
                update = true;
                lastVal = rs.getInt(1);
            } else {
                update = false;
                lastVal = 0;
            }
        }
        if (update) {
            con.prepareStatement("Update nrlais_tx_pkg." + table + " set max_seqnr=" + (lastVal + 1) + " where kebeleid = '" + KebeleInfo.getKebeleCode(kebele) + "'").execute();
        } else {
            if (table.equals("t_tx_seq")) {
                con.prepareStatement("insert into nrlais_tx_pkg.t_tx_seq (kebeleid,max_seqnr,year) values('"+KebeleInfo.getKebeleCode(kebele)+"'," + (lastVal + 1) + ",2010)").execute();
            } else {
                con.prepareStatement("insert into nrlais_tx_pkg." + table + " (kebeleid,max_seqnr) values('"+KebeleInfo.getKebeleCode(kebele)+"'," + (lastVal + 1) + ")").execute();
            }
        }
        return lastVal + 1;
    }

    public static int lookupSex(String sex) {
        if (org.apache.commons.lang3.StringUtils.isEmpty(sex)) {
            return -1;
        }
        if ("M".equals(sex.toUpperCase())) {
            return 1;
        }
        if ("F".equals(sex.toUpperCase())) {
            return 2;
        }
        if ("U".equals(sex.toUpperCase())) {
            return 3;
        }
        return -1;
    }

    public static String reverseLookupSex(int sex) {
        switch (sex) {
            case 1:
                return "m";
            case 2:
                return "f";
            case 3:
                return "u";
            default:
                return null;
        }
    }

    /*public Connection getNRLAISConnection() throws SQLException {
        String url = Startup.getAppConfig("worlais-db");
        String user = Startup.getAppConfig("worlais-user-name");
        String password = Startup.getAppConfig("worlais-password");
        System.out.format("Url:%s un:%s pw:%s ", url, user, password);
        return DriverManager.getConnection(url, user, password);
    }*/
    static public void populateNRLAISEntityFields(ResultSet rs, NRLAISEntity e) throws SQLException {
        e.csaregionid = rs.getString("csaregionid");
        e.nrlais_zoneid = rs.getString("nrlais_zoneid");
        e.nrlais_woredaid = rs.getString("nrlais_woredaid");
        e.nrlais_kebeleid = rs.getString("nrlais_kebeleid");
        e.syscreateby = rs.getString("syscreateby");
        Calendar cal = Calendar.getInstance();
        e.syscreatedate = rs.getTimestamp("syscreatedate", cal).getTime();
        e.syslastmodby = rs.getString("syslastmodby");
        e.syslastmoddate = rs.getTimestamp("syslastmoddate", cal).getTime();
    }

    static public void populateNRLAISTransactionEntityFields(ResultSet rs, NRLAISTransactionEntity e, boolean includeSourceTxField) throws SQLException {
        populateNRLAISEntityFields(rs, e);
        if (includeSourceTxField) {
            e.sourcetxuid = (rs.getString("sourcetxuid"));
        } else {
            e.editStatus = rs.getString("editStatus");
        }
        e.currenttxuid = (rs.getString("currenttxuid"));
    }

    protected <T extends IDName> List<T> loadIDDescriptionRecord(Connection con, String table, Class<T> c) throws SQLException {
        ArrayList<T> rec = new ArrayList<>();
        try (ResultSet rs = con.prepareStatement("Select * from " + table + " order by codeid").executeQuery()) {
            while (rs.next()) {
                T r;
                try {
                    r = c.newInstance();
                } catch (InstantiationException e1) {
                    throw new IllegalArgumentException(c.toString() + " instance couldn't be created", e1);
                } catch (IllegalAccessException e2) {
                    throw new IllegalArgumentException(c.toString() + " instance couldn't be created", e2);
                }
                r.id = rs.getInt("codeid");
                r.setName(rs.getString("en"), rs.getString("am"), rs.getString("om"), rs.getString("ti"));
                rec.add(r);
            }
        }
        return rec;
    }

    <T extends IDName> T getIDName(Connection con, String table, Class<T> c, int id) throws SQLException {
        try (ResultSet rs = con.prepareStatement("Select * from " + table + " where codeid=" + id).executeQuery()) {
            if (rs.next()) {
                T r;
                try {
                    r = c.newInstance();
                } catch (InstantiationException e1) {
                    throw new IllegalArgumentException(c.toString() + " instance couldn't be created", e1);
                } catch (IllegalAccessException e2) {
                    throw new IllegalArgumentException(c.toString() + " instance couldn't be created", e2);
                }
                r.id = rs.getInt("codeid");
                r.setName(rs.getString("en"), rs.getString("am"), rs.getString("om"), rs.getString("ti"));
                return r;
            } else {
                return null;
            }
        }
    }

    static public void setNRLAISEntityFields(NamedPreparedStatement st, NRLAISEntity e) throws SQLException {
        setNRLAISEntityFields(st, e, true);
    }

    static public String setNRLAISHistoryEntityFields(NamedPreparedStatement st, String txuid, NRLAISHistoryEntity e, String objectUID) throws SQLException {
        e.id = UUID.randomUUID().toString();
        e.archiveDate = new java.util.Date().getTime();
        st.setString("@id", e.id);
        st.setString("@csaregionid", e.csaregionid);
        st.setString("@nrlais_zoneid", e.nrlais_zoneid);
        st.setString("@nrlais_woredaid", e.nrlais_woredaid);
        st.setString("@nrlais_kebeleid", e.nrlais_kebeleid);
        st.setString("@inventoryid", objectUID);
        st.setString("@sourcetx", e.data.sourcetxuid);
        st.setString("@archivetx", txuid);
        st.setTimestamp("@archivedate", new java.sql.Timestamp(e.archiveDate));
        st.setString("@data", GSONUtil.toJson(e.data));
        return e.id;
    }

    static public void setNRLAISEntityFields(NamedPreparedStatement st, NRLAISEntity e, boolean insert) throws SQLException {
        st.setString("@csaregionid", e.csaregionid);
        st.setString("@nrlais_zoneid", e.nrlais_zoneid);
        st.setString("@nrlais_woredaid", e.nrlais_woredaid);
        st.setString("@nrlais_kebeleid", e.nrlais_kebeleid);
        long now = new java.util.Date().getTime();
        if (insert) {
            st.setString("@syscreateby", e.syscreateby);
            st.setTimestamp("@syscreatedate", new java.sql.Timestamp(now));
        }
        st.setString("@syslastmodby", e.syslastmodby);
        st.setTimestamp("@syslastmoddate", new java.sql.Timestamp(now));
    }

    static public void setNRLAISTransactionEntityFields(NamedPreparedStatement st, NRLAISTransactionEntity e) throws SQLException {
        setNRLAISTransactionEntityFields(st, e, true);
    }

    static public void setNRLAISTransactionEntityFields(NamedPreparedStatement st, NRLAISTransactionEntity e, boolean insert) throws SQLException {
        setNRLAISEntityFields(st, e, insert);
        st.setString("@currenttxuid", e.currenttxuid);

    }
}
