/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.model.tran;

import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.SourceDocument;
import com.intaps.nrlais.util.RationalNum;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tewelde
 */
public class DivorceTransactionData extends LATransaction.TransactionData {
    public String holdingUID=null;
    public List<Applicant> applicants=new ArrayList<Applicant>();
    public List<Partition> partitions=new ArrayList<Partition>();
    public SourceDocument proofOfDivorceDocument=null; 
    public SourceDocument claimResolutionDocument=null;
    public SourceDocument landHoldingCertifcateDocument=null;

    public PartyParcelShare getPartyPartition(String parcelUID,String partyUID) {
        for(Partition p:this.partitions)
        {
            if(p.parcelUID.equals(parcelUID))
            {
                for(PartyParcelShare sh:p.shares)
                {
                    if(parcelUID.equals(sh.parcelUID))
                        return sh;
                }
                return null;
            }
        }
        return null;
    }
   
}
