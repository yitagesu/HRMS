/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.routes.map;

import com.intaps.nrlais.Startup;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.map.MapperEngineModel;
import com.intaps.nrlais.repo.MainFacade;
import com.intaps.nrlais.util.GISUtils;
import com.intaps.nrlais.util.ResourceUtils;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.postgis.PGgeometry;

/**
 *
 * @author Teweldemedhin Aberra
 */
@WebServlet("/api/map/cert")
public class APICertificateMap extends HttpServlet {

    public static class MapLegendModel {

        public boolean showParcel = true;
        public boolean showOtherParcel = true;
        public boolean showKebeleBound = false;
        public boolean showRoad = false;
        public boolean showRiver = false;
    }

    public static class MapFrameModel {

        public static class GridMarker {

            private double x1;
            private double y1;
            private double x2;
            private double y2;
            private double x0;
            private double y0;

            public GridMarker(double x0, double y0, double w, double h) {
                this.x0 = x0;
                this.y0 = y0;
                this.x1 = this.x0 - w / 2;
                this.y1 = this.y0 - h / 2;
                this.x2 = this.x0 + w / 2;
                this.y2 = this.y0 + h / 2;
            }

            public double getX1() {return x1;}
            public double getY1() {return y1;}
            public double getX2() {return x2;}
            public double getY2() {return y2;}
            public double getX0() {return x0;}
            public double getY0() {return y0;}

        }

        public static class CoordinateMarker {

            private MapperEngineModel.MEPoint point;
            private String label;

            private CoordinateMarker(String label, double x, double y) {
                this.point = new MapperEngineModel.MEPoint(x, y);
                this.label = label;
            }

            /**
             * @return the point
             */
            public MapperEngineModel.MEPoint getPoint() {
                return point;
            }

            /**
             * @param point the point to set
             */
            public void setPoint(MapperEngineModel.MEPoint point) {
                this.point = point;
            }

            /**
             * @return the label
             */
            public String getLabel() {
                return label;
            }

            /**
             * @param label the label to set
             */
            public void setLabel(String label) {
                this.label = label;
            }
        }
        private int seqNo;
        private String upi;
        private int imageW;
        private int imageH;
        private MapperEngineModel.MEBox outerBox;
        private MapperEngineModel.MEBox innerBox;
        private ArrayList<GridMarker> gridMarkers = new ArrayList<>();
        private ArrayList<CoordinateMarker> axisMarkersLeft = new ArrayList<>();
        private ArrayList<CoordinateMarker> axisMarkersRight = new ArrayList<>();
        private ArrayList<CoordinateMarker> axisMarkersBottom = new ArrayList<>();
        private ArrayList<CoordinateMarker> axisMarkersTop = new ArrayList<>();

        private ArrayList<CoordinateMarker> scaleBarMarkers = new ArrayList<>();

        private ArrayList<MapperEngineModel.MEBox> darkScaleBarItems = new ArrayList<>();
        private ArrayList<MapperEngineModel.MEBox> whiteScaleBarItems = new ArrayList<>();

        //MASSREG
        public MapperEngineModel.MEBox getOuterBox() {
            return outerBox;
        }

        //MASSREG
        public void setOuterBox(MapperEngineModel.MEBox outerBox) {
            this.outerBox = outerBox;
        }

        //MASSREG
        public MapperEngineModel.MEBox getInnerBox() {
            return innerBox;
        }

        //MASSREG
        public void setInnerBox(MapperEngineModel.MEBox innerBox) {
            this.innerBox = innerBox;
        }

        //MASSREG
        public ArrayList<GridMarker> getGridMarkers() {
            return gridMarkers;
        }

        //MASSREG
        public ArrayList<CoordinateMarker> getAxisMarkersLeft() {
            return axisMarkersLeft;
        }

        //MASSREG
        public ArrayList<CoordinateMarker> getAxisMarkersRight() {
            return axisMarkersRight;
        }

        //MASSREG
        public ArrayList<CoordinateMarker> getAxisMarkersBottom() {
            return axisMarkersBottom;
        }

        //MASSREG
        public ArrayList<CoordinateMarker> getScaleBarMarkers() {
            return scaleBarMarkers;
        }

        //MASSREG
        public ArrayList<MapperEngineModel.MEBox> getDarkScaleBarItems() {
            return darkScaleBarItems;
        }

        //MASSREG
        public ArrayList<MapperEngineModel.MEBox> getWhiteScaleBarItems() {
            return whiteScaleBarItems;
        }

        //MASSREG
        public int getImageW() {
            return imageW;
        }

        //MASSREG
        public void setImageW(int imageW) {
            this.imageW = imageW;
        }

        //MASSREG
        public int getImageH() {
            return imageH;
        }

        //MASSREG
        public void setImageH(int imageH) {
            this.imageH = imageH;
        }

        //MASSREG
        public String getUpi() {
            return upi;
        }

        //MASSREG
        public void setUpi(String upi) {
            this.upi = upi;
        }

        /**
         * @return the axisMarkersTop
         */
        public ArrayList<CoordinateMarker> getAxisMarkersTop() {
            return axisMarkersTop;
        }

        /**
         * @return the seqNo
         */
        public int getSeqNo() {
            return seqNo;
        }

        /**
         * @param seqNo the seqNo to set
         */
        public void setSeqNo(int seqNo) {
            this.seqNo = seqNo;
        }

    }

    static final int GUTTER_TOP = 30;
    static final int GUTTER_BOTTOM = 100;
    static final int GUTTER_LEFT = 30;
    static final int GUTTER_RIGHT = 30;
    static final int SCALE_BAR_LEFT_OFFSET = 30;
    static final int SCALE_BAR_BOTTOM_OFFSET = 15;
    static final int SCALE_BAR_HEIGHT = 20;

    static final double SCALE_BAR_MAX_SIZE = 0.5;
    static final double N_GRID_POINTS = 3;
    static final int GRID_MARKER_SIZE = 20;

    static final int MAP_DEFAULT_WIDTH = 932;
    static final int MAP_DEFAULT_HEIGHT = 932;

    static String buildWMSRequest(MapFrameModel model) throws IOException {
        return ResourceUtils.evaluateTemplate(
                new File("res/file/map/CertificateWMSTemplate.xml")
                , model);
    }

    static double maxFullNumberLessThan(double x) {
        if (x < 0.1) {
            return 0;
        }
        double base = 0.1;
        while (base * 10 < x) {
            base = base * 10;
        }
        double ret = base;
        double []factors=new double[]{5,2.5,1};
        for(int i=0;i<factors.length;i++)
            if(factors[i]*ret<x)
                return factors[i]*ret;
        return ret;
    }

    MapperEngineModel.MEBox getMapBox(PGgeometry g, int totalWidth, int totalHeight) {
        MapperEngineModel.MEBox mapBox = GISUtils.toMEPolygon(g).boundingBox();
        mapBox.expand(1.5);
        int mapWidth = totalWidth - GUTTER_LEFT - GUTTER_RIGHT;
        int mapHeight = totalHeight - GUTTER_TOP - GUTTER_BOTTOM;

        if (mapWidth * mapBox.height() > mapHeight * mapBox.width()) {
            mapBox.setWidth((double) mapWidth * mapBox.height() / (double) mapHeight);
        } else if (mapWidth * mapBox.height() < mapHeight * mapBox.width()) {
            mapBox.setHeight((double) mapHeight * mapBox.width() / (double) mapWidth);
        }
        return mapBox;
    }

    MapLegendModel getLegendModel(String _upi, PGgeometry g, int totalWidth, int totalHeight) {
        MapLegendModel ret = new MapLegendModel();
        
        return ret;
    }

    MapFrameModel getFrameModel(int seqNo,String upin, PGgeometry g, int totalWidth, int totalHeight) {

        MapperEngineModel.MEBox mapBox = getMapBox(g, totalWidth, totalHeight);
        
        int mapWidth = totalWidth - GUTTER_LEFT - GUTTER_RIGHT;
        int mapHeight = totalHeight - GUTTER_TOP - GUTTER_BOTTOM;

        double mapUnitPerPixel = mapBox.width() / mapWidth;

        MapperEngineModel.MEBox outerBox = new MapperEngineModel.MEBox(
                mapBox.getX1() - GUTTER_LEFT * mapUnitPerPixel,
                mapBox.getY1() - GUTTER_BOTTOM * mapUnitPerPixel,
                mapBox.getX2() + GUTTER_RIGHT * mapUnitPerPixel,
                mapBox.getY2() + GUTTER_TOP * mapUnitPerPixel
        );

        Map valuesMap = new HashMap();
        valuesMap.put("upi", upin);

        MapFrameModel model = new MapFrameModel();
        model.setOuterBox(outerBox);
        model.setInnerBox(mapBox);
        model.setImageW(totalWidth);
        model.setImageH(totalHeight);
        model.setUpi(upin);
        model.setSeqNo(seqNo);
        //scale bar
        double scaleBarLength = maxFullNumberLessThan(SCALE_BAR_MAX_SIZE * mapBox.width());
        MapperEngineModel.MEBox box1, box2;
        model.whiteScaleBarItems.add(new MapperEngineModel.MEBox(
                outerBox.x1 + SCALE_BAR_LEFT_OFFSET * mapUnitPerPixel,
                outerBox.y1 + SCALE_BAR_BOTTOM_OFFSET * mapUnitPerPixel,
                outerBox.x1 + SCALE_BAR_LEFT_OFFSET * mapUnitPerPixel + scaleBarLength / 2,
                outerBox.y1 + SCALE_BAR_BOTTOM_OFFSET * mapUnitPerPixel + SCALE_BAR_HEIGHT * mapUnitPerPixel / 2
        ));

        model.whiteScaleBarItems.add(box2 = new MapperEngineModel.MEBox(
                outerBox.x1 + SCALE_BAR_LEFT_OFFSET * mapUnitPerPixel + scaleBarLength / 2,
                outerBox.y1 + SCALE_BAR_BOTTOM_OFFSET * mapUnitPerPixel + SCALE_BAR_HEIGHT * mapUnitPerPixel / 2,
                outerBox.x1 + SCALE_BAR_LEFT_OFFSET * mapUnitPerPixel + scaleBarLength,
                outerBox.y1 + SCALE_BAR_BOTTOM_OFFSET * mapUnitPerPixel + SCALE_BAR_HEIGHT * mapUnitPerPixel
        ));

        model.darkScaleBarItems.add(box1 = new MapperEngineModel.MEBox(
                outerBox.x1 + SCALE_BAR_LEFT_OFFSET * mapUnitPerPixel,
                outerBox.y1 + SCALE_BAR_BOTTOM_OFFSET * mapUnitPerPixel + SCALE_BAR_HEIGHT * mapUnitPerPixel / 2,
                outerBox.x1 + SCALE_BAR_LEFT_OFFSET * mapUnitPerPixel + scaleBarLength / 2,
                outerBox.y1 + SCALE_BAR_BOTTOM_OFFSET * mapUnitPerPixel + SCALE_BAR_HEIGHT * mapUnitPerPixel
        ));

        model.darkScaleBarItems.add(new MapperEngineModel.MEBox(
                outerBox.x1 + SCALE_BAR_LEFT_OFFSET * mapUnitPerPixel + scaleBarLength / 2,
                outerBox.y1 + SCALE_BAR_BOTTOM_OFFSET * mapUnitPerPixel,
                outerBox.x1 + SCALE_BAR_LEFT_OFFSET * mapUnitPerPixel + scaleBarLength,
                outerBox.y1 + SCALE_BAR_BOTTOM_OFFSET * mapUnitPerPixel + SCALE_BAR_HEIGHT * mapUnitPerPixel / 2
        ));

        model.scaleBarMarkers.add(new MapFrameModel.CoordinateMarker("0",
                box1.x1, box1.y2
        ));
        model.scaleBarMarkers.add(new MapFrameModel.CoordinateMarker(Integer.toString((int)Math.round(scaleBarLength / 2)),
                box1.x2, box1.y2
        ));
        model.scaleBarMarkers.add(new MapFrameModel.CoordinateMarker(Integer.toString((int)Math.round(scaleBarLength)),
                box2.x2, box2.y2
        ));

        //grid and axis markers
        double gridDelta = maxFullNumberLessThan(Math.min(mapBox.width(), mapBox.height()) / N_GRID_POINTS);

        for (double y = Math.ceil(mapBox.y1 / gridDelta) * gridDelta; y < mapBox.y2; y += gridDelta) {
            for (double x = Math.ceil(mapBox.x1 / gridDelta) * gridDelta; x < mapBox.x2; x += gridDelta) {
                model.gridMarkers.add(new MapFrameModel.GridMarker(x, y, GRID_MARKER_SIZE * mapUnitPerPixel, GRID_MARKER_SIZE * mapUnitPerPixel));
            }
        }

        for (double x = Math.ceil(mapBox.x1 / gridDelta) * gridDelta; x < mapBox.x2; x += gridDelta) {
            model.axisMarkersBottom.add(new MapFrameModel.CoordinateMarker(Integer.toString((int)Math.round(x)), x, mapBox.y1));
            model.axisMarkersTop.add(new MapFrameModel.CoordinateMarker(Integer.toString((int)Math.round(x)), x, mapBox.y2));
        }

        for (double y = Math.ceil(mapBox.y1 / gridDelta) * gridDelta; y < mapBox.y2; y += gridDelta) {
            model.axisMarkersLeft.add(new MapFrameModel.CoordinateMarker(Integer.toString((int)Math.round(y)), mapBox.x1, y));
            model.axisMarkersRight.add(new MapFrameModel.CoordinateMarker(Integer.toString((int)Math.round(y)), mapBox.x2, y));

        }
        return model;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String parcelUID = req.getParameter("parcel_uid");
            String tranUID= req.getParameter("tran_uid");
            LADM.Parcel parcel= new MainFacade(Startup.getSessionByRequest(req)).getHistoryParcelBySourceTx(tranUID, parcelUID);
            if (parcel == null) {
                throw new IllegalStateException("Parcel geometry doesn't exist");
            }
            PGgeometry g=new PGgeometry(parcel.geometry);
            try {
                URL url = new URL(Startup.getAppConfig("map-server") + "/ows?service=WMS");
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setDoOutput(true);
                urlConnection.setRequestMethod("POST");
                urlConnection.setRequestProperty("Content-Type", "text/xml");
                int totalWidth;
                int totalHeight;

                if (StringUtils.isEmpty(req.getParameter("width"))) {
                    totalWidth = MAP_DEFAULT_WIDTH;
                } else {
                    totalWidth = Integer.parseInt(req.getParameter("width"));
                }
                if (StringUtils.isEmpty(req.getParameter("height"))) {
                    totalHeight = MAP_DEFAULT_HEIGHT;
                } else {
                    totalHeight = Integer.parseInt(req.getParameter("height"));
                }

                MapFrameModel model = getFrameModel(parcel.seqNo, parcel.upid, g, totalWidth, totalHeight);

                String reqstr = buildWMSRequest(model);

                OutputStream out = urlConnection.getOutputStream();
                out.write(reqstr.getBytes());
                InputStream inCon = urlConnection.getInputStream();
                InputStream in = new BufferedInputStream(inCon);
                ServletOutputStream slos = resp.getOutputStream();

                for (Map.Entry<String, List<String>> kv : urlConnection.getHeaderFields().entrySet()) {
                    String hv = "";
                    for (String h : kv.getValue()) {
                        if (hv.equals("")) {
                            hv = h;
                        } else {
                            hv = hv + ";" + h;
                        }
                    }
                    resp.setHeader(kv.getKey(), hv);
                }
                byte[] buf = new byte[4096];
                int len;
                while ((len = in.read(buf)) > 0) {
                    slos.write(buf, 0, len);
                }
                in.close();
                slos.close();
            } catch (Exception ex) {
                ex.printStackTrace();
                throw ex;
            }
        } catch (Exception ex) {
            throw new ServletException(ex.getMessage());
        }
    }
}
