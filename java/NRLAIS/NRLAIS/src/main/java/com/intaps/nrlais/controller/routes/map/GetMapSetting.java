/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.routes.map;

import com.google.gson.stream.JsonReader;
import com.intaps.nrlais.Startup;
import com.intaps.nrlais.controller.routes.RouteBase;
import com.intaps.nrlais.repo.GetMap;
import com.intaps.nrlais.repo.MainFacade;
import com.intaps.nrlais.util.GSONUtil;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Tewelde
 */
@WebServlet("/api/map/get_setting")
public class GetMapSetting extends RouteBase<GetMap.MapSetting, GSONUtil.EmptyReturn, GSONUtil.EmptyReturn>{

    @Override
    public GetMap.MapSetting createGetResponse(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        return new MainFacade(Startup.getSessionByRequest(req)).getInitialMapSetting();
    }

    @Override
    public GSONUtil.EmptyReturn deserializePostBody(JsonReader reader) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public GSONUtil.EmptyReturn processPost(HttpServletRequest req, HttpServletResponse resp, GSONUtil.EmptyReturn data) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}