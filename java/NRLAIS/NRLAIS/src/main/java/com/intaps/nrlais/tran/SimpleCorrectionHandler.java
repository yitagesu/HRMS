/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.tran;

import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.CMSSCreateParcelsTask;
import com.intaps.nrlais.model.CMSSEditParcelTask;
import com.intaps.nrlais.model.DocumentTypeInfo;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.SourceDocument;
import com.intaps.nrlais.model.tran.Applicant;
import com.intaps.nrlais.model.tran.SimpleCorrectionData;
import com.intaps.nrlais.model.tran.ExpropriationTransactionData;
import com.intaps.nrlais.model.tran.ParcelItem;
import com.intaps.nrlais.model.tran.PartyItem;
import com.intaps.nrlais.model.tran.SpecialCaseTransactionData;
import com.intaps.nrlais.repo.CMSSTaskManager;
import com.intaps.nrlais.repo.DocumentTypeRepo;
import com.intaps.nrlais.repo.GetLADM;
import com.intaps.nrlais.repo.LRManipulator;
import com.intaps.nrlais.repo.ManualLRManipulatorFacade;
import com.intaps.nrlais.repo.TransactionRepo;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.UUID;
/**
 *
 * @author yitagesu
 */
public class SimpleCorrectionHandler extends TransactionHandlerBase implements TransactionHandler{
    
    void registerTransaction(UserSession session, Connection con, LATransaction theTransaction, SimpleCorrectionData tran) throws SQLException, Exception {

        TransactionRepo tranRep = new TransactionRepo(session);

        SourceDocument[] allDocs = new SourceDocument[]{tran.landHoldingCertifcateDocument};
        int[] allTypes = new int[]{DocumentTypeInfo.DOC_TYPE_LAND_HOLDING_CERTIFICATE};
        DocumentTypeRepo docTypeRepo = new DocumentTypeRepo(session);
        for (int i = 0; i < allDocs.length; i++) {
            SourceDocument srcDoc = allDocs[i];
            if (srcDoc == null) {
                throw new IllegalArgumentException("Source document of type:" + docTypeRepo.getDocumentType(con, srcDoc.sourceType) + " isnot provided");
            }
            srcDoc.uid = UUID.randomUUID().toString();
            srcDoc.sourceType = allTypes[i];
            tranRep.saveDocument(con, theTransaction, srcDoc);
        }

    }
    
     @Override
    public void saveTransaction(UserSession session, Connection con, LATransaction theTransaction, boolean register) throws Exception {
        try {
            SimpleCorrectionData tran = (SimpleCorrectionData) theTransaction.data;
            LADM existing = new GetLADM(session, "nrlais_inventory").get(con, tran.holdingUID, LADM.CONTENT_FULL);
            String error = existing.getRuleErrorsAsString(theTransaction.time);
            if (!org.apache.commons.lang3.StringUtils.isEmpty(error)) {
                throw new IllegalArgumentException(error);
            }
            if (existing.holding.hasNoneHoldingRight()) {
                throw new IllegalAccessException("expropration transaction can't be execute on a holding with none-holding rights");

            }
            if (!register) {
                return;
            }
            super.transferLandRecordToTransaction(session, con, theTransaction.transactionUID, existing);
            registerTransaction(session, con, theTransaction, tran);
        } catch (CloneNotSupportedException | IOException ex) {
            throw new SQLException("Cant Excute Transaction", ex);
        }
    }

  
    @Override
    public void filterStateChange(UserSession session, Connection con, String transactionUID, String stateChange) throws SQLException {
        if("requestApproval".equals(stateChange))
        {
            if(!new ManualLRManipulatorFacade(session).transactionHasOperations(transactionUID))
                throw new IllegalStateException("This transaction can't be approved because no change yet been made");
            
        }
    }
}
