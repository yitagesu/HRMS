/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller;

import com.intaps.nrlais.Startup;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Tewelde
 */
public class HeaderViewController extends ViewControllerBase{
    
    public boolean showNewTranButton=false;
    public HeaderViewController(HttpServletRequest request, HttpServletResponse response,boolean showNewTranButton) {
        super(request, response);
        this.showNewTranButton=showNewTranButton;
    }
    public String userFullName()
    {
        return session.userName;
    }
    public String wipssSessionID()
    {
        return Startup.getSessionByRequest(request).worlaisSession.getSesionID();
    }
    public String wipssUrl()
    {
        return Startup.getAppConfig("worlais-url");
    }
    
}
