package com.intaps.nrlais.replication;

import com.intaps.nrlais.util.GSONUtil;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.Proxy;
import java.net.URL;
import java.util.List;
import java.util.Map;
import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

/**
 * Created by Teweldemedhin Aberra on 3/27/2017.
 */
public class WORLAISUPSClient {

    void logout() {
    }

    public enum ConnectionStatus {
        DISCONNECTED,
        CONNECTED,
        ERROR,
    }
    String _url;

    public static class UserNamePasword {

        public String userName;
        public String password;
        public String lang = "en-us";
    }

    public static class LoginRes {

        public boolean success;
        public String message;
    }

    public WORLAISUPSClient(String url) {
        this._url = url;
    }

    public void setUrl(String url) {
        this._url = url;
    }
    String sessionID = null;

    public void login(String un, String pw) throws MalformedURLException, IOException {
        UserNamePasword up = new UserNamePasword();
        up.userName = un;
        up.password = pw;
        GSONUtil.StringRet res = invokeJSON("/api/login", up, GSONUtil.StringRet.class);
        if (res.error != null) {
            throw new IllegalArgumentException(res.error == null ? "Login failed" : res.error);
        }
    }

    public <RetType> RetType invokeJSON(String pathAndQuery, Object post, Class<RetType> c) throws MalformedURLException, IOException {
        URL url = new URL(_url + pathAndQuery);
        return invokeJSON(url, post, c, post == null ? "GET" : "POST");
    }

    void setRequestCookies(HttpRequestBase request) {
        if (sessionID != null) {

            request.setHeader("Cookie", "JSESSIONID=" + sessionID);
        }
    }

    void readSessionKey(CloseableHttpResponse response) {
        Header h = response.getFirstHeader("set-cookie");
        if (h != null) {
            HeaderElement[] vals = h.getElements();
            for (HeaderElement nv : vals) {
                if (nv.getName().equalsIgnoreCase("JSESSIONID")) {
                    this.sessionID = nv.getValue();
                }
            }
        }
    }

    <RetType> RetType readJSONResponse(CloseableHttpResponse response, Class<RetType> c) throws IOException {
        HttpEntity entity = response.getEntity();
        try (InputStream in = new BufferedInputStream(entity.getContent());) {
            String retStr = new String(org.apache.commons.io.IOUtils.toByteArray(in));
            RetType ret = GSONUtil.fromJson(c, retStr);
            readSessionKey(response);
            return ret;
        }
    }

    public <RetType> RetType invokeJSON(URL url, Object post, Class<RetType> c, String postMethod) throws ProtocolException, IOException {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        if (postMethod.equals("GET")) {
            HttpGet httpGet = new HttpGet(url.toString());
            setRequestCookies(httpGet);
            CloseableHttpResponse response = httpclient.execute(httpGet);
            try {
                return readJSONResponse(response, c);
            } finally {
                response.close();
            }
        } else {

            HttpEntityEnclosingRequestBase httpPost;
            if (postMethod.equals("PATCH")) {
                httpPost = new HttpPatch(url.toString());
            } else if (postMethod.equals("POST")) {
                httpPost = new HttpPost(url.toString());
            } else {
                throw new IllegalArgumentException("HTTP Method " + postMethod + " not supported");
            }
            setRequestCookies(httpPost);
            String jsonString = GSONUtil.toJson(post);
            StringEntity requestEntity = new StringEntity(jsonString, ContentType.APPLICATION_JSON);
            httpPost.setEntity(requestEntity);
            CloseableHttpResponse response = httpclient.execute(httpPost);
            try {
                return readJSONResponse(response, c);

            } finally {
                response.close();
            }
        }

    }

}
