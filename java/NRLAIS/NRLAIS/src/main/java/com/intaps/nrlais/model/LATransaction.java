/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.model;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tewelde
 */
public class LATransaction extends NRLAISEntity {

    public static final int TRAN_STATUS_INITIATED = 1;
    public static final int TRAN_STATUS_REGISTERD = 2;
    public static final int TRAN_STATUS_ACCEPT_REQUESTED = 3;
    public static final int TRAN_STATUS_ACCEPTED = 4;
    public static final int TRAN_STATUS_FINISHED = 5;
    public static final int TRAN_STATUS_CANCEL_REQUESTED = 6;
    public static final int TRAN_STATUS_CANCELED = 7;
    public static final int TRAN_STATUS_ERROR = 99;

    public static final int TRAN_TYPE_INHERITANCE_WITHWILL = 1;
    public static final int TRAN_TYPE_INHERITANCE = 2;
    public static final int TRAN_TYPE_DIVORCE = 3;
    public static final int TRAN_TYPE_GIFT = 4;
    public static final int TRAN_TYPE_EXCHANGE = 5;
    public static final int TRAN_TYPE_EXPROPRIATION = 6;
    public static final int TRAN_TYPE_REALLOCATION = 7;
    public static final int TRAN_TYPE_SPECIAL_CASE = 8;
    public static final int TRAN_TYPE_RENT = 9;
    public static final int TRAN_TYPE_SERVITUDE = 10;
    public static final int TRAN_TYPE_RESTRICTIVE_INTEREST = 11;
    public static final int TRAN_TYPE_SPLIT = 12;
    public static final int TRAN_TYPE_CONSOLIDATION = 13;
    public static final int TRAN_TYPE_BOUNDARY_CORRECTION = 14;
    public static final int TRAN_TYPE_SIMPLE_CORRECTION = 15;
    public static final int TRAN_TYPE_REPLACE_CERTIFICATE = 17;
    public static final int TRAN_TYPE_EX_OFFICIO = 18;
    public static final int TRAN_TYPE_DATA_MIGRATION = 100;

    public boolean isCommited() {
        return this.status == TRAN_STATUS_ACCEPTED || this.status == TRAN_STATUS_FINISHED;
    }

    public LATransaction getTransaction(Connection con, String tranUID, boolean b) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public static class TransactionData {
        public List<SourceDocument> otherDocument=new ArrayList<SourceDocument>();
    }
    public String transactionUID;
    public String notes = "";
    public int transactionType;
    public int status;
    public int year;
    public int seqNo;
    public long time = new java.util.Date().getTime();
    public String syssessionid;
    public int spatialTask = 0;
    public TransactionData data;

    public LATransaction() {
        this.syscreatedate = this.syslastmoddate = new java.util.Date().getTime();
    }

    public static String getStatusString(int status) {
        switch (status) {
            case TRAN_STATUS_INITIATED:
                return "Initiated";
            case TRAN_STATUS_REGISTERD:
                return "Registered";
            case TRAN_STATUS_ACCEPT_REQUESTED:
                return "Waiting Approval";
            case TRAN_STATUS_ACCEPTED:
                return "Approved";
            case TRAN_STATUS_FINISHED:
                return "Finished";
            case TRAN_STATUS_CANCEL_REQUESTED:
                return "Waiting Cancelation Approval";
            case TRAN_STATUS_CANCELED:
                return "Canceled";
            case TRAN_STATUS_ERROR:
                return "Error";
            default:
                return "";
        }
    }

    public String transactionID() {
        return this.nrlais_kebeleid + "/" + seqNo;
    }

    public static class GiftApplication extends TransactionData {

    }

    public static class TranactionFlow extends NRLAISEntity {

        public String uid;
        public long time;
        public String timeStr;
        public String notes;
        public int newState;
        public int oldState;

        public TranactionFlow() {
        }

    }
}
