/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.repo;

import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.*;
import com.intaps.nrlais.util.INTAPSLangUtils;
import com.intaps.nrlais.util.ResourceUtils;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Tewelde
 */
public class SearchHolding  extends RepoBase{
    public SearchHolding(UserSession session)
    {
        super(session);
    }
    String getSql(SearchHoldingPars pars, boolean count) throws IOException
    {
        String cr=null;
        if(!StringUtils.isEmpty(pars.name1))
        {
            cr=INTAPSLangUtils.appendStringList(cr, " and ", "(t_party.name like  '"+pars.name1+"%' or gparty.name like '"+pars.name1+"%')");
        }
        if(!StringUtils.isEmpty(pars.name2))
        {
            cr=INTAPSLangUtils.appendStringList(cr, " and ", "(t_party.fathersname like  '"+pars.name2+"%' or gparty.fathersname like '"+pars.name2+"%')");
        }
        if(!StringUtils.isEmpty(pars.name3))
        {
            cr=INTAPSLangUtils.appendStringList(cr, " and ", "t_party.grandfathersname like  '"+pars.name3+"%' or gparty.grandfathersname like '"+pars.name3+"%')");
        }
        if(!StringUtils.isEmpty(pars.kebele))
        {
            cr=INTAPSLangUtils.appendStringList(cr, " and ", "t_holdings.nrlais_kebeleid like  '"+pars.kebele+"%'");
        }
        if(pars.parcelSeqNo>0)
        {
            cr=INTAPSLangUtils.appendStringList(cr, " and ", "t_parcels.seqnr="+pars.parcelSeqNo);
        }
        if(pars.holdingSeqNo>0)
        {
            cr=INTAPSLangUtils.appendStringList(cr, " and ", "t_holdings.seqnr="+pars.holdingSeqNo);
        }
        String sql=ResourceUtils.getSql("search_holding.sql");
        sql=StringUtils.replaceEach(sql, new String[]{"@select","@schema","@cr"}, new String[]{
            count?"count(distinct t_holdings.uid)":"distinct t_holdings.uid","nrlais_inventory",cr
        });
        return sql;
    }
    public int count(final Connection con,SearchHoldingPars pars) throws SQLException, IOException
    {
            String sql=getSql(pars, true);
            try(ResultSet rs=con.prepareStatement(sql).executeQuery())
            {
                if(rs.next())
                    return rs.getInt(1);
                return 0;
            }
    }
    public List<LADM> searchHolding(final Connection con,SearchHoldingPars pars,int pageOffset,int pageSize) throws IOException, SQLException
    {
            List<LADM> ret=new ArrayList<>();
            String sql=getSql(pars, false)+" offset "+pageOffset+" limit "+pageSize;
            try(ResultSet rs=con.prepareStatement(sql).executeQuery())
            {
                while(rs.next())
                    ret.add(new GetLADM(session, "nrlais_inventory").get(con,rs.getString(1),LADM.CONTENT_FULL));
            }
            return ret;
    }
}
