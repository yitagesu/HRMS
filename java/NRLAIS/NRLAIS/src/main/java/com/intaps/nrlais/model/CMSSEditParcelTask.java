/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tewelde
 */
public class CMSSEditParcelTask extends CMSSTask {

    public List<String> parcels=new ArrayList<>();

    public CMSSEditParcelTask() {

    }

    public CMSSEditParcelTask(CMSSTask baseRet) {
        this.taskUID = baseRet.taskUID;
        this.status = baseRet.status;
        this.transactionUID = baseRet.transactionUID;
        this.taskType = TASK_TYPE_EDIT;
    }
}
