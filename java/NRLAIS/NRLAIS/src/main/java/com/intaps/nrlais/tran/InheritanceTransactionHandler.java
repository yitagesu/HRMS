/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.tran;

import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.CMSSParcelSplitTask;
import com.intaps.nrlais.model.DocumentTypeInfo;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LADM.Parcel;
import com.intaps.nrlais.model.LADM.Party;
import com.intaps.nrlais.model.SourceDocument;
import com.intaps.nrlais.model.tran.Applicant;
import com.intaps.nrlais.model.tran.DivorceTransactionData;
import com.intaps.nrlais.model.tran.InheritanceTransactionData;
import com.intaps.nrlais.model.tran.Partition;
import com.intaps.nrlais.model.tran.PartyItem;
import com.intaps.nrlais.model.tran.PartyParcelShare;
import com.intaps.nrlais.repo.CMSSTaskManager;
import com.intaps.nrlais.repo.DocumentTypeRepo;
import com.intaps.nrlais.repo.GetLADM;
import com.intaps.nrlais.repo.GetTransaction;
import com.intaps.nrlais.repo.LRManipulator;
import com.intaps.nrlais.repo.TransactionRepo;
import com.intaps.nrlais.util.EthiopianCalendar;
import com.intaps.nrlais.util.RationalNum;
import com.intaps.nrlais.worlais.WORLAISClient;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.AbstractList;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Mac;

/**
 *
 * @author Tewelde
 */
public class InheritanceTransactionHandler extends TransactionHandlerBase implements TransactionHandler {

    /**
     *
     * @param con
     * @param theTransaction
     * @throws SQLException
     */
    //the logic for modifying the land register goes here.
    void registerTransaction(UserSession session, Connection con, LATransaction theTransaction, InheritanceTransactionData tran) throws SQLException, Exception {
        LRManipulator lrm = new LRManipulator(session);
        CMSSTaskManager cmss = new CMSSTaskManager(session);
        TransactionRepo tranRep = new TransactionRepo(session);
        GetLADM repo = new GetLADM(session, "nrlais_transaction");
        LADM ladm = repo.get(con, tran.holdingUID, LADM.CONTENT_FULL);
        LADM.Holding holding = ladm.holding;

        //check holderBeneficiaries
        List<LADM.Right> allHolders = holding.getHolders();
        HashMap<String, Applicant> holderBeneficiaries = new HashMap<>();
        HashMap<String, LADM.Right> holderBeneficiaryRights = new HashMap<>();
        HashMap<String, Applicant> deceased = new HashMap<>();
        HashMap<LADM.Party,String> originalPartyUID=new HashMap<>();
        RationalNum shareOfDeceased = new RationalNum(0, 1);
        for(PartyItem p:tran.beneficiaries)
        {
            originalPartyUID.put(p.party, p.party.partyUID);
        }
        for(Applicant p:tran.holderBeneficiaries)
        {
            originalPartyUID.put(holding.getParty(p.selfPartyUID),p.selfPartyUID);
        }
        for (Applicant ap : tran.holderBeneficiaries) {
            if (holderBeneficiaries.containsKey(ap.selfPartyUID)) {
                throw new IllegalArgumentException(session.text("Duplicate applicant")+" " + ap.selfPartyUID);
            }
            
            if (ap.inheritancRole == Applicant.INHERITANCE_ROLE_NEUTRAL) {
                continue;
            }

            LADM.Right r = null;
            for (LADM.Right thisR : allHolders) {
                if (thisR.partyUID.equals(ap.selfPartyUID)) {
                    r = thisR;
                    break;
                }
            }

            if (r == null) {
                throw new IllegalArgumentException(session.text("Applicant is not one of the holders")+".");
            }

            if (ap.inheritancRole == Applicant.INHERITANCE_ROLE_DECEASED) {
                deceased.put(ap.selfPartyUID, ap);
                shareOfDeceased.add(r.share);
            }

            holderBeneficiaryRights.put(ap.selfPartyUID, r);
            holderBeneficiaries.put(ap.selfPartyUID, ap);
            if (ap.inheritancRole == Applicant.INHERITANCE_ROLE_BENEFICIARY) {
                if (ap.idDocument == null) {
                    throw new IllegalArgumentException(session.text("ID document not provided for")+" " + r.party.getFullName());
                } else {
                    ap.idDocument.uid = UUID.randomUUID().toString();
                    tranRep.saveDocument(con, theTransaction, ap.idDocument);
                }
                if (ap.representative != null) {
                    lrm.createParty(con, theTransaction, ap.representative);
                    if (ap.letterOfAttorneyDocument == null) {
                        throw new IllegalArgumentException(session.text("Letter of attorney document not provided for")+" " + ap.representative.getFullName());
                    }
                    ap.letterOfAttorneyDocument.uid = UUID.randomUUID().toString();
                    ap.letterOfAttorneyDocument.sourceType = DocumentTypeInfo.DOC_TYPE_LETTER_OF_ATTORNEY;
                    tranRep.saveDocument(con, theTransaction, ap.letterOfAttorneyDocument);
                }
            }
        }
        if(deceased.size()==0)
            throw new IllegalArgumentException(session.text("At least one of the holders must be deceased"));
        //process beneficiaries
        for (PartyItem ben : tran.beneficiaries) {
            if (ben.idDoc == null) {  //code for validating id of beneficiaries commented out becuase the UI doesn't support it
                throw new IllegalArgumentException(session.text("ID document not provided for")+" " + ben.party.getFullName());
            } else {
                ben.idDoc.uid = UUID.randomUUID().toString();
                tranRep.saveDocument(con, theTransaction, ben.idDoc);
            }
            if (ben.representative != null) {
                lrm.createParty(con, theTransaction, ben.representative);
                if (ben.letterOfAttorneyDocument == null) {
                    throw new IllegalArgumentException(session.text("Letter of attorney  document not provided for")+" " + ben.representative.getFullName());
                }
                ben.letterOfAttorneyDocument.uid = UUID.randomUUID().toString();
                ben.letterOfAttorneyDocument.sourceType = DocumentTypeInfo.DOC_TYPE_LETTER_OF_ATTORNEY;
                tranRep.saveDocument(con, theTransaction, ben.letterOfAttorneyDocument);
            }
        }
        //save documents
        SourceDocument[] allDocs = new SourceDocument[]{
            tran.proofOfDeath,
            tran.claimResolutionDocument,
            tran.courtDocument,
            tran.willDocument,
            tran.landHoldingCertifcateDocument};
        int[] allTypes = new int[]{
            DocumentTypeInfo.DOC_TYPE_PROOF_OF_DEATH,
            DocumentTypeInfo.DOC_TYPE_DIVORCE_CLAIM_RESOLUTION,
            DocumentTypeInfo.DOC_TYPE_INHERITANCE_COURT_DECISION,
            DocumentTypeInfo.DOC_TYPE_WILL,
            DocumentTypeInfo.DOC_TYPE_LAND_HOLDING_CERTIFICATE};
        DocumentTypeRepo docTypeRepo = new DocumentTypeRepo(session);
        for (int i = 0; i < allDocs.length; i++) {
            if (allTypes[i] == DocumentTypeInfo.DOC_TYPE_WILL && !tran.withWill) {
                continue;
            }
            if (allTypes[i] == DocumentTypeInfo.DOC_TYPE_INHERITANCE_COURT_DECISION && tran.withWill) {
                continue;
            }
            SourceDocument srcDoc = allDocs[i];
            if (srcDoc == null) {
                throw new IllegalArgumentException(session.text("Source document of type")+":" + docTypeRepo.getDocumentType(con, srcDoc.sourceType) +" "+session.text("is not provided"));
            }
            srcDoc.uid = UUID.randomUUID().toString();
            srcDoc.sourceType = allTypes[i];
            tranRep.saveDocument(con, theTransaction, srcDoc);
        }

        HashMap<String, Partition> partitions = new HashMap<String, Partition>();
        tran.partitions.forEach((p) -> {
            partitions.put(p.parcelUID, p);
        });

        lrm.lrStartTransaction(con, theTransaction, holding.holdingUID);

        HashMap<String, String> splitHoldings = new HashMap<>();

        Iterator<Parcel> parcelIter = holding.parcels.iterator();

        while (parcelIter.hasNext()) {
            LADM.Parcel parcel = parcelIter.next();
            if (!partitions.containsKey(parcel.parcelUID)) {
                throw new IllegalArgumentException(session.text("Partition information for parcel")+" " + parcel.upid + " "+session.text("is not set")+".");
            }
            Partition parition = partitions.get(parcel.parcelUID);
            HashMap<String, PartyParcelShare> shares = new HashMap<>();
            parition.shares.forEach((s)
                    -> {
                shares.put(s.partyUID, s);
            });
            if (parition.hasShared() && !parition.is100Percent()) {
                throw new IllegalArgumentException(session.text("Total share of shard parcels for the divorcing partners should be 100%"));
            }
            double minSize = session.worlaisSession.getMinAreaConfig(con,ladm);
            boolean splitable = parcel.areaGeom/shares.size() > minSize;

                   
            ArrayList<Map.Entry<String, LADM.Parcel>> splitParcels = new ArrayList<>();
            boolean transfered = false;
            HashMap<LADM.Parcel, PartyParcelShare> parcelToShare = new HashMap<>(); 
            
            List<LADM.Right> parcelRights = parcel.getHolders();    
            Iterator<LADM.Right> iter = parcelRights.iterator();
            while (iter.hasNext()) {
                LADM.Right r = iter.next();
                if (!holderBeneficiaries.containsKey(r.partyUID)) //keep the rights of the holders that are not beneficiaries
                {
                    continue;
                }
                Applicant holderBen = holderBeneficiaries.get(r.partyUID);
                if (holderBen.inheritancRole == Applicant.INHERITANCE_ROLE_DECEASED) //remove the rights of the deceased
                {                    
                    iter.remove();
                    continue;
                }
                if (holderBen.inheritancRole == Applicant.INHERITANCE_ROLE_NEUTRAL) {
                    continue;
                }
                if (!shares.containsKey(r.partyUID)) {
                    throw new IllegalArgumentException(session.text("Right not specified for")+" " + r.party.getFullName());
                }

                PartyParcelShare share = shares.get(r.partyUID);
                if(share.splitGeom)
                {
                    if (!splitable) {
                        throw new IllegalArgumentException(session.text("This parcel can not be split"));
                    }
                    String sh;
                    if (splitHoldings.containsKey(r.partyUID)) {
                        sh = splitHoldings.get(r.partyUID);
                    } else {
                        LADM.Holding h = holding.cloneHolding();
                        sh = lrm.lrCreateNewHolding(con, theTransaction, h);
                        splitHoldings.put(r.partyUID, sh);
                    }
                    LADM.Parcel splitParcel = parcel.clonedObjectNoRR();
                    splitParcel.resetGeom();
                    splitParcel.mreg_actype = LADM.Parcel.AC_TYPE_INHERITANCE;
                    splitParcel.mreg_acyear = EthiopianCalendar.ToEth(theTransaction.time).Year;
                    LADM.Right splitRight = new LADM.Right();
                    splitRight.rightType = LADM.Right.RIGHT_PUR;
                    splitRight.share.set(1, 1);
                    splitRight.party = r.party;
                    splitParcel.rights.add(splitRight);
                    splitParcels.add(new AbstractMap.SimpleEntry<>(sh, splitParcel));
                    parcelToShare.put(splitParcel, share);
                    iter.remove();
                }
                else
                {
                    r.share.add(RationalNum.multiply(shareOfDeceased, share.share));
                    share.parcelUID = parcel.parcelUID;
                }

            }//rights of parcel iterator

            Iterator<PartyItem> benIter = tran.beneficiaries.iterator();
            while (benIter.hasNext()) {
                PartyItem pi = benIter.next();
                if(pi.existingPartyUID!=null && pi.party.maritalstatus==LADM.Party.MARITAL_STATUS_MARRIED)
                    pi.party.maritalstatus=LADM.Party.MARITAL_STATUS_WIDOWED;

                PartyParcelShare share = shares.get(originalPartyUID.get(pi.party));
                if (share.splitGeom) {
                    if (!splitable) {
                        throw new IllegalArgumentException(session.text("This parcel can not be split"));
                    }
                    String sh;
                    if (splitHoldings.containsKey(originalPartyUID.get(pi.party))) {
                        sh = splitHoldings.get(originalPartyUID.get(pi.party));
                    } else {
                        LADM.Holding h = holding.cloneHolding();
                        sh = lrm.lrCreateNewHolding(con, theTransaction, h);
                        splitHoldings.put(originalPartyUID.get(pi.party), sh);
                    }
                    LADM.Parcel splitParcel = parcel.clonedObjectNoRR();
                    splitParcel.resetGeom();
                    splitParcel.mreg_actype = LADM.Parcel.AC_TYPE_INHERITANCE;
                    splitParcel.mreg_acyear = EthiopianCalendar.ToEth(theTransaction.time).Year;
                    LADM.Right splitRight = new LADM.Right();
                    splitRight.rightType = LADM.Right.RIGHT_PUR;
                    splitRight.share.set(1, 1);
                    splitRight.party = pi.party;
                    splitParcel.rights.add(splitRight);
                    splitParcels.add(new AbstractMap.SimpleEntry<>(sh, splitParcel));
                    parcelToShare.put(splitParcel, share);
                
                } else if (share.share.greaterThanZero()) {
                    LADM.Right inheritedRight = new LADM.Right();
                    inheritedRight.rightType = LADM.Right.RIGHT_PUR;
                    inheritedRight.share = RationalNum.multiply(shareOfDeceased, share.share);
                    inheritedRight.party = pi.party;
                    if(pi.party.maritalstatus==LADM.Party.MARITAL_STATUS_MARRIED)
                        pi.party.maritalstatus=LADM.Party.MARITAL_STATUS_WIDOWED;
                    parcelRights.add(inheritedRight);
                }
            }//rights of parcel iterator

            if (transfered) {
                if (splitParcels.size() > 0) {
                    throw new IllegalArgumentException(session.text(session.text("A transfered parcel can not be split")));
                }
                continue;
            }

            if (splitParcels.size() > 0) {
                if (!parcelRights.isEmpty()) {
                    throw new IllegalArgumentException("Inheritance transaction don't support parcel partial split");
                }
                
                if(splitParcels.size()<2)
                    throw new IllegalAccessException(session.text("Invalid split operation")+".");
                lrm.lrSplitParcel(con, theTransaction, parcel.parcelUID, holding.holdingUID, splitParcels);
                for (Map.Entry<String, LADM.Parcel> s : splitParcels) {
                    parcelToShare.get(s.getValue()).parcelUID = s.getValue().parcelUID;
                }

            } else {
                lrm.lrUpateRights(con, theTransaction, holding.holdingUID, parcel.parcelUID, parcelRights);
            }

            //add cmss split parcel task if necessary
        }//parcels iterator

        lrm.finishTransaction(con, theTransaction);
    }

    @Override
    public void saveTransaction(UserSession session, Connection con,
            LATransaction theTransaction, boolean register) throws Exception {
        try {
            InheritanceTransactionData tran = (InheritanceTransactionData) theTransaction.data;
            LADM existing = new GetLADM(session, "nrlais_inventory").get(con, tran.holdingUID, LADM.CONTENT_FULL);

            String err = existing.getRuleErrorsAsString(theTransaction.time);
            if (!org.apache.commons.lang3.StringUtils.isEmpty(err)) {
                throw new IllegalStateException(err);
            }
            //reject hodling that has none holding right
            if (existing.holding.hasNoneHoldingRight()) {
                throw new IllegalArgumentException(session.text("Inheritance transaction can not be execute on a holding with none-holding rights"));
            }
            //reject state land 
            if (existing.holding.isStateLand()) {
                throw new IllegalArgumentException(session.text("Inheritance transaction can not be exectued on state land"));
            }

            if (!register) {
                return;
            }

            super.transferLandRecordToTransaction(session, con, theTransaction.transactionUID, existing);
            registerTransaction(session, con, theTransaction, tran);

        } catch (CloneNotSupportedException | IOException ex) {
            throw new SQLException(session.text("Can not execute transaction"), ex);
        }
    }

}
