/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.tran;

import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.tran.ManualTransactionData;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Tewelde
 */
public class ManualTransactionViewController extends TransactionViewController<ManualTransactionData> {

    public LADM.Holding holding;

    public ManualTransactionViewController(HttpServletRequest request, HttpServletResponse response, Class<ManualTransactionData> c, int tranTypeID) throws Exception {
        super(request, response, c, tranTypeID, true);
        if (data.holdingUID == null) {
            holding = new LADM.Holding();
        } else {
            if (tran.isCommited()) {
                holding = mainFacade.getFromHistoryByArchiveTx(tran.transactionUID, data.holdingUID).holding;
            } else {
                holding = mainFacade.getLADM("nrlais_inventory", data.holdingUID).holding;
            }

        }
        kebele = mainFacade.getKebele(holding.nrlais_kebeleid);
        woreda = mainFacade.getWoreda(holding.nrlais_woredaid);
    }


}
