/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.routes.api;

import com.google.gson.stream.JsonWriter;
import com.intaps.nrlais.Startup;
import com.intaps.nrlais.controller.tran.CMSSBoundaryCorrectionViewController;
import com.intaps.nrlais.controller.tran.CMSSCreateParcelViewController;
import com.intaps.nrlais.controller.tran.CMSSParcelSplitViewController;
import com.intaps.nrlais.model.CMSSCreateParcelsTask;
import com.intaps.nrlais.model.CMSSEditParcelTask;
import com.intaps.nrlais.model.CMSSTask;
import com.intaps.nrlais.model.CMSSTaskGeom;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.repo.MainFacade;
import com.intaps.nrlais.repo.TransactionFacade;
import com.intaps.nrlais.util.GSONUtil;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Teweldemedhin Aberra
 */
@WebServlet("/api/create_task")
public class APICMSSCreateTaskData extends APIBase {

    public static class CreateTaskData {

        public String error = null;
        public CMSSCreateParcelsTask task = null;
        public List<LADM.Parcel> parcels = null;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        CreateTaskData ret = new CreateTaskData();
        try {

            CMSSCreateParcelViewController controller = new CMSSCreateParcelViewController(req, resp);
            ret.task = controller.task;
            ret.parcels = controller.parcels;

        } catch (Exception ex) {
            Logger.getLogger(APICMSSTaskGeom.class.getName()).log(Level.SEVERE, null, ex);
            ret.error = ex.getMessage();

        }
        try (ServletOutputStream str = resp.getOutputStream()) {
            resp.setContentType("application/json");
            JsonWriter writer = new JsonWriter(new OutputStreamWriter(str, "UTF-8"));
            GSONUtil.getAdapter(CreateTaskData.class).write(writer, ret);
            writer.close();
        }
    }

}
