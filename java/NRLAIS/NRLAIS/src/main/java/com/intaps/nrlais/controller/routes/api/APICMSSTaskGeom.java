/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.routes.api;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.intaps.nrlais.Startup;
import com.intaps.nrlais.model.CMSSTask;
import com.intaps.nrlais.model.CMSSTaskGeom;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.repo.GetLADM;
import com.intaps.nrlais.repo.MainFacade;
import com.intaps.nrlais.repo.TransactionFacade;
import com.intaps.nrlais.util.GSONUtil;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author yitagesu
 */
@WebServlet("/api/taskgeom")
public class APICMSSTaskGeom extends APIBase {

    static class TaskGeomRet extends GSONUtil.JSONRet<CMSSTaskGeom[]> {
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        TaskGeomRet ret = new TaskGeomRet();
        try {
            String task_uid = req.getParameter("task_uid");
            List<CMSSTaskGeom> g=new MainFacade(Startup.getSessionByRequest(req)).getTaskGeometries(task_uid);
            for(CMSSTaskGeom gg:g)
            {
                 gg.geom=gg.geom.replace("SRID=20137;", "");
            }
            ret.res = g.toArray(new CMSSTaskGeom[0]);
            ret.error = null;
        } catch (Exception ex) {
            Logger.getLogger(APICMSSTaskGeom.class.getName()).log(Level.SEVERE, null, ex);
            ret.error = ex.getMessage();
            ret.res = null;
        }
        try (ServletOutputStream str = resp.getOutputStream()) {
            resp.setContentType("application/json");
            JsonWriter writer = new JsonWriter(new OutputStreamWriter(str, "UTF-8"));
            GSONUtil.getAdapter(TaskGeomRet.class).write(writer, ret);
            writer.close();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        GSONUtil.VoidRet ret = new GSONUtil.VoidRet();
        try (
                ServletInputStream ins = req.getInputStream();
                JsonReader reader = new JsonReader(new InputStreamReader(ins, "UTF-8"));) {
            try {
                assertLogedInUser(req, resp);
                CMSSTask.TaskGeomAssignment data = GSONUtil.getAdapter(CMSSTask.TaskGeomAssignment.class).read(reader);                
                new TransactionFacade(Startup.getSessionByRequest(req)).assignGeometry(data,true);
            } catch (Exception ex) {
                Logger.getLogger(APICMSSTaskGeom.class.getName()).log(Level.SEVERE, null, ex);
                ret.error = "Error: " + ex.getMessage();
            }
        }
        try (ServletOutputStream str = resp.getOutputStream()) {
            resp.setContentType("application/json");
            JsonWriter writer = new JsonWriter(new OutputStreamWriter(str, "UTF-8"));
            GSONUtil.getAdapter(GSONUtil.VoidRet.class).write(writer, ret);
            writer.close();
        }
    }

}
