/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais;

import com.intaps.nrlais.controller.tran.DivorceViewController;
import com.intaps.nrlais.model.IDName;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.tran.Applicant;
import com.intaps.nrlais.repo.TransactionFacade;
import com.intaps.nrlais.util.GSONUtil;
import com.intaps.nrlais.util.INTAPSDateUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Tewelde
 */
public class LiveTest {
    public static void postTransactionTest(UserSession session)
    {
        File f=new File("C:\\trash\\NRLAIS\\divorce.json");
        try
        {
            String json=org.apache.commons.io.IOUtils.toString(f.toURI(), "UTF-8");
            LATransaction d=Startup.readTransaction(json);
            new TransactionFacade(session).saveTransaction(d,true);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(LiveTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(LiveTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(LiveTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(LiveTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
}
