/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.routes.api;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.intaps.nrlais.Startup;
import com.intaps.nrlais.replication.ReplicationData;
import com.intaps.nrlais.replication.ReplicationFacade;
import com.intaps.nrlais.util.GSONUtil;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.jar.JarInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 *
 * @author Tewelde
 */
@WebServlet("/api/replication_upload")
@MultipartConfig
public class APIDReplicationUploader extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            Part filePart = req.getPart("file");
            if (filePart == null) {
                throw new ServletException("File not uploaded");
            }
            try (InputStream instr = filePart.getInputStream()) {
                ZipInputStream zin = new JarInputStream(instr);
                ZipEntry ent = null;
                TypeAdapter<ReplicationData> t = GSONUtil.getAdapter(ReplicationData.class);
                ReplicationFacade facade = new ReplicationFacade(Startup.getSessionByRequest(req));
                while ((ent = zin.getNextEntry()) != null) {
                    //String json=org.apache.commons.io.IOUtils.toString(instr,"UTF-8");
                    ReplicationData repData = t.fromJson(new InputStreamReader(zin));
                    facade.uploadReplicationData(repData);
                }
            }
            resp.sendRedirect("/replication/replication.jsp?msg=upload_success");
        } catch (Exception ex) {
            throw new ServletException(ex);
        }
    }
}
