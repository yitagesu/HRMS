/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.repo;

import com.google.gson.Gson;
import com.intaps.nrlais.Startup;
import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.KebeleInfo;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.SourceDocument;
import com.intaps.nrlais.model.TransactionFlow;
import com.intaps.nrlais.util.EthiopianCalendar;
import com.intaps.nrlais.util.GSONUtil;
import com.intaps.nrlais.util.NamedPreparedStatement;
import com.intaps.nrlais.util.ResourceUtils;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.apache.catalina.Globals;
import org.apache.commons.lang3.StringUtils;
import org.postgis.java2d.Java2DWrapper;

/**
 *
 * @author Tewelde
 */
public class TransactionRepo extends RepoBase {

    public TransactionRepo(UserSession session) {
        super(session);
    }

    public String createTransaction(Connection con, String kebele, int transactionType, String note, long time, boolean spatial) throws IOException, SQLException {
        String sql = ResourceUtils.getSql("insert_transaction.sql");
        NamedPreparedStatement st = new NamedPreparedStatement(session, con, sql);
        LATransaction tran = new LATransaction();
        KebeleInfo.fixNRLAISEntityFileds(session, kebele, tran, true);
        super.setNRLAISEntityFields(st, tran);
        String txuid = UUID.randomUUID().toString();
        String flowuid = UUID.randomUUID().toString();

        st.setString("@uid", txuid);
        st.setString("@notes", note);
        st.setInt("@transactiontype", transactionType);
        st.setInt("@txstatus", LATransaction.TRAN_STATUS_INITIATED);
        st.setInt("@txyear", EthiopianCalendar.ToEth(time).Year);
        st.setInt("@txseqnr", super.getSeqNo(con, kebele, "t_tx_seq"));
        st.setTimestamp("@txdate", new java.sql.Timestamp(time));
        String wsid = super.session.worlaisSession.getSesionID();
        /*int i = wsid.indexOf("-");
        wsid = wsid.substring(i - 8, i - 8 + 36);
        st.setString("@syssessionid", wsid);*/
        st.setString("@syssessionid", null);
        st.setInt("@spatialtask", spatial ? 1 : 0);
        st.setString("@flow_uid", flowuid);
        st.preparedStatement().execute();
        return txuid;
    }

    public String updateTransaction(Connection con, LATransaction tran) throws IOException, SQLException {
        String sql = ResourceUtils.getSql("update_transaction.sql");
        NamedPreparedStatement st = new NamedPreparedStatement(session, con, sql);
        KebeleInfo.fixNRLAISEntityFileds(session, tran.nrlais_kebeleid, tran, true);
        super.setNRLAISEntityFields(st, tran, false);

        st.setString("@uid", tran.transactionUID);
        st.setString("@notes", tran.notes);
        st.setInt("@txyear", EthiopianCalendar.ToEth(tran.time).Year);
        st.setTimestamp("@txdate", new java.sql.Timestamp(tran.time));
        String wsid = super.session.worlaisSession.getSesionID();
        /*int i = wsid.indexOf("-");
        wsid = wsid.substring(i - 8, i - 8 + 36);
        st.setString("@syssessionid", wsid);*/
        st.setString("@syssessionid", null);
        st.setInt("@spatialtask", tran.spatialTask);
        st.preparedStatement().execute();
        return tran.transactionUID;
    }

    public void changeTransactionState(Connection con, String txuid, int oldStatus, int newStatus, String notes) throws IOException, SQLException {
        NamedPreparedStatement st = new NamedPreparedStatement(session, con, ResourceUtils.getSql("insert_transaction_flow.sql"));
        LATransaction tran = new GetTransaction(session).get(con, txuid, false);
        TransactionFlow flow = new TransactionFlow();
        flow.flowUID = UUID.randomUUID().toString();
        KebeleInfo.fixNRLAISEntityFileds(session, tran.nrlais_kebeleid, flow, true);
        super.setNRLAISEntityFields(st, flow);
        st.setString("@uid", flow.flowUID);
        st.setString("@notes", notes);
        st.setString("@txuid", txuid);
        st.setInt("@oldstatus", oldStatus);
        st.setInt("@newstatus", newStatus);
        st.preparedStatement().execute();
    }

    public void updateTransactionData(Connection con, LATransaction tran) throws SQLException {
        PreparedStatement ps = con.prepareStatement("UPDATE nrlais_inventory.t_transaction_data SET tx_data=?::json WHERE tx_uid=?::uuid");
        ps.setString(1, GSONUtil.toJson(tran));
        ps.setString(2, tran.transactionUID);
        ps.execute();
    }

    public String saveDocument(Connection con, LATransaction tran, SourceDocument doc) throws IOException, SQLException {
        if (doc.uid == null) {
            doc.uid = UUID.randomUUID().toString();
        }
        doc.txuid = tran.transactionUID;
        doc.notes = doc.description;
        doc.voidStatus = false;
        if (doc.archiveType == SourceDocument.ARCHIVE_TYPE_DIGITAL) {
            if (doc.fileImage == null) {
                throw new IllegalArgumentException("Fileimage is not provided");
            }
            String folder = Startup.getAppConfig("docFolder", "data/docFolder");
            File f = new File(folder);
            if (!f.exists()) {
                f.mkdir();
            }
            org.apache.commons.io.FileUtils.writeByteArrayToFile(new File(folder + "/" + doc.uid), doc.fileImage);
        }

        NamedPreparedStatement st = new NamedPreparedStatement(session, con, ResourceUtils.getSql("insert_source_document.sql"));
        KebeleInfo.fixNRLAISEntityFileds(session, tran.nrlais_kebeleid, doc, true);
        setNRLAISEntityFields(st, doc);
        st.setString("@uid", doc.uid);
        st.setString("@notes", doc.notes);
        st.setInt("@adminsourcetype", doc.sourceType);
        st.setString("@adminsourceref", doc.refText);
        st.setInt("@adminsourcearchivetype", doc.archiveType);
        st.setString("@adminsourcedescription", doc.description);
        st.setString("@txuid", tran.transactionUID);
        st.setString("@mimetype", doc.mimeType);
        st.setBoolean("@voidstatus", false);
        st.preparedStatement().execute();
        doc.fileImage = null;
        return doc.txuid;
    }

    public void loadTransaction(Connection con, String txuid) throws SQLException {
        con.prepareStatement("Update nrlais_inventory.t_transaction set syssessionid='" + session.getSessionGUID() + "' where uid='" + txuid + "'").execute();

    }

    public void unloadTransaction(Connection con, String txuid) throws SQLException {
        con.prepareStatement("Update nrlais_inventory.t_transaction set syssessionid=null  where uid='" + txuid + "'").execute();
    }

    List<String> getTransctionHoldings(Connection con, String txuid) throws SQLException {
        List<String> ret = new ArrayList<String>();
        try (ResultSet rs = con.prepareStatement("Select uid from nrlais_inventory.t_holdings where currenttxuid='" + txuid + "'").executeQuery()) {
            while (rs.next()) {
                ret.add(rs.getString(1));
            }
        }
        return ret;
    }

    void checkLADMError(Connection con, LATransaction tran) throws Exception {
        for (LADM.Holding h : new GetLADM(session, "nrlais_transaction").getTransactionHoldings(con, tran.transactionUID, LADM.CONTENT_FULL)) {
            String error = new LADM(h).getRuleErrorsAsString(tran.time);
            if (!StringUtils.isEmpty(error)) {
                throw new IllegalStateException(error);
            }

        }
    }

    void fixReplicationDMLAndTime(Connection con) throws IOException, SQLException {
        con.prepareStatement("Update nrlais_inventory.t_replicationlog set log_time=" + (new java.util.Date().getTime()) + " where log_time is null").execute();
        con.prepareStatement(ResourceUtils.getSql("fix_replication_dml.sql")).execute();
    }

    void acceptTranscation(Connection con, LATransaction tran, String note) throws SQLException, Exception {
        checkLADMError(con, tran);
        session.worlaisSession.runCheck(tran.transactionUID);
        LRManipulator lrm = new LRManipulator(session);
        for (String h : getTransctionHoldings(con, tran.transactionUID)) {
            lrm.logToHistory(con, tran, h);
        }

        con.prepareStatement("SELECT nrlais_tx_pkg.write_rep_log('" + session.worlaisSession.getWoredaID(con) + "', '" + tran.transactionUID + "'::uuid,23,'n')").execute();

        con.prepareStatement("SELECT nrlais_tx_pkg.update_inv('" + tran.transactionUID + "')").execute();

        changeTransactionState(con, tran.transactionUID, tran.status, LATransaction.TRAN_STATUS_ACCEPTED, note);

        fixReplicationDMLAndTime(con);

    }

    void acceptDataMigrationTranscation(Connection con, LATransaction tran, String note) throws SQLException, Exception {
        //session.worlaisSession.runCheck(tran.transactionUID);
        con.prepareStatement("SELECT nrlais_tx_pkg.update_inv('" + tran.transactionUID + "')").execute();
        changeTransactionState(con, tran.transactionUID, LATransaction.TRAN_STATUS_ACCEPT_REQUESTED, LATransaction.TRAN_STATUS_ACCEPTED, note);
        String sql = StringUtils.replaceEach(ResourceUtils.getSql("close_transaction.sql"), new String[]{"@txuid"},
                new String[]{tran.transactionUID});
        con.prepareStatement(sql).execute();
        changeTransactionState(con, tran.transactionUID, LATransaction.TRAN_STATUS_ACCEPTED, LATransaction.TRAN_STATUS_FINISHED, note);
        fixReplicationDMLAndTime(con);

    }

    void closeTransaction(Connection con, LATransaction tran, String note) throws IOException, SQLException {
        String sql = StringUtils.replaceEach(ResourceUtils.getSql("close_transaction.sql"), new String[]{"@txuid"},
                new String[]{tran.transactionUID});
        con.prepareStatement(sql).execute();
        changeTransactionState(con, tran.transactionUID, LATransaction.TRAN_STATUS_ACCEPTED, LATransaction.TRAN_STATUS_FINISHED, note);
    }

    void changeState(Connection con, String txuid, String cmd, String note) throws SQLException, Exception {
        loadTransaction(con, txuid);
        GetTransaction repo = new GetTransaction(session);
        LATransaction tran = repo.get(con, txuid, false);
        GetTransaction.createTransactionHandler(tran.transactionType).filterStateChange(session, con, txuid, cmd);
        boolean invalid = false;
        switch (cmd) {
            case "requestApproval":
                if (tran.status != LATransaction.TRAN_STATUS_REGISTERD) {
                    invalid = true;
                    break;
                }
                if (!new GetCMSSTask(session).isTransactionDone(con, txuid)) {
                    invalid = true;
                    break;
                }
                checkLADMError(con, tran);
                changeTransactionState(con, tran.transactionUID, tran.status, LATransaction.TRAN_STATUS_ACCEPT_REQUESTED, note);
                return;
            case "acceptApproval":
                if (tran.status != LATransaction.TRAN_STATUS_ACCEPT_REQUESTED) {
                    invalid = true;
                    break;
                }
                //acccept approval is done without PRSS
                acceptTranscation(con, tran, note);
                return;
            case "issueTitle":
                if (tran.status != LATransaction.TRAN_STATUS_ACCEPTED) {
                    invalid = true;
                    break;
                }
                closeTransaction(con, tran, note);
                return;
            case "rejectApproval":
                if (tran.status != LATransaction.TRAN_STATUS_ACCEPT_REQUESTED) {
                    invalid = true;
                    break;
                }
                changeTransactionState(con, tran.transactionUID, tran.status, LATransaction.TRAN_STATUS_REGISTERD, note);
                return;
            case "requestAbortion":
                if (tran.status != LATransaction.TRAN_STATUS_REGISTERD && tran.status != LATransaction.TRAN_STATUS_INITIATED) {
                    invalid = true;
                    break;
                }
                changeTransactionState(con, tran.transactionUID, tran.status, LATransaction.TRAN_STATUS_CANCEL_REQUESTED, note);
                return;
            case "acceptAbortion":
            case "rejectAbortion":
                if (tran.status != LATransaction.TRAN_STATUS_CANCEL_REQUESTED) {
                    invalid = true;
                    break;
                }
                if (cmd.equals("acceptAbortion")) {
                    changeTransactionState(con, tran.transactionUID, tran.status, LATransaction.TRAN_STATUS_CANCELED, note);
                } else {
                    changeTransactionState(con, tran.transactionUID, tran.status, LATransaction.TRAN_STATUS_INITIATED, note);
                    new LRManipulator(session).clearHoldingFromTranSchema(con, tran.transactionUID);
                }
                return;
            default:
                invalid = true;
        }
        if (invalid) {
            throw new IllegalArgumentException("Invalid status change:" + cmd);
        }
        session.worlaisSession.transactionChangeSate(txuid, cmd, note);
        unloadTransaction(con, txuid);
    }

}
