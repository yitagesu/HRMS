/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.tran;

import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.CMSSParcelSplitTask;
import com.intaps.nrlais.model.DocumentTypeInfo;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LADM.Parcel;
import com.intaps.nrlais.model.LADM.Party;
import com.intaps.nrlais.model.SourceDocument;
import com.intaps.nrlais.model.tran.Applicant;
import com.intaps.nrlais.model.tran.DivorceTransactionData;
import com.intaps.nrlais.model.tran.ParcelTransfer;
import com.intaps.nrlais.model.tran.Partition;
import com.intaps.nrlais.model.tran.PartyItem;
import com.intaps.nrlais.model.tran.PartyParcelShare;
import com.intaps.nrlais.model.tran.ExpropriationTransactionData;
import com.intaps.nrlais.repo.CMSSTaskManager;
import com.intaps.nrlais.repo.DocumentTypeRepo;
import com.intaps.nrlais.repo.GetLADM;
import com.intaps.nrlais.repo.GetTransaction;
import com.intaps.nrlais.repo.LRManipulator;
import com.intaps.nrlais.repo.TransactionRepo;
import com.intaps.nrlais.util.EthiopianCalendar;
import com.intaps.nrlais.util.RationalNum;
import com.intaps.nrlais.worlais.WORLAISClient;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.AbstractList;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Mac;
import org.apache.commons.lang.IllegalClassException;

/**
 *
 * @author yitagesu
 */
public class ExpropriationTransactionHandler extends TransactionHandlerBase implements TransactionHandler {

    void registerTransaction(UserSession session, Connection con, LATransaction theTransaction, ExpropriationTransactionData tran) throws SQLException, Exception {
        LRManipulator lrm = new LRManipulator(session);
        TransactionRepo tranRepo = new TransactionRepo(session);
        GetLADM repo = new GetLADM(session, "nrlais_transaction");
        LADM ladm = repo.get(con, tran.holdingUID, LADM.CONTENT_FULL);
        LADM.Holding holding = ladm.holding;

         List<LADM.Right> holders = holding.getHolders();
        //check subjects

        HashMap<String, Applicant> applicants = new HashMap<>();
        HashMap<String, LADM.Right> applicantRights = new HashMap<>();
        for (Applicant ap : tran.applicants) {
            if (applicants.containsKey(ap.selfPartyUID)) {
                throw new IllegalArgumentException(session.text("Duplicate applicant") + ap.selfPartyUID);
            }
            LADM.Right r = null;

            for (LADM.Right thisR : holders) {
                if (thisR.partyUID.equals(ap.selfPartyUID)) {
                    r = thisR;
                    break;
                }
            }

            if (r == null) {
                throw new IllegalArgumentException(session.text("Transfer subject is not one of the holders"));
            }

            applicantRights.put(ap.selfPartyUID, r);
            applicants.put(ap.selfPartyUID, ap);
        }
        SourceDocument[] allDocs = new SourceDocument[]{tran.decisionOfWoredaAdministration, tran.landHoldingCertificate, tran.proofOfpaymentOfCompensation};
        int[] allTypes = new int[]{DocumentTypeInfo.DOC_TYPE_EXPROPRATION_DECISION, DocumentTypeInfo.DOC_TYPE_LAND_HOLDING_CERTIFICATE, DocumentTypeInfo.DOC_TYPE_PROOF_OF_COMPENSATION};
        DocumentTypeRepo docTypeRepo = new DocumentTypeRepo(session);
        for (int i = 0; i < allDocs.length; i++) {
            SourceDocument srcDoc = allDocs[i];
            if (srcDoc == null) {
                throw new IllegalArgumentException(session.text("Required Document of ") + docTypeRepo.getDocumentType(con, srcDoc.sourceType) + session.text("is not provided"));
            }
            srcDoc.uid = UUID.randomUUID().toString();
            srcDoc.sourceType = allTypes[i];
            tranRepo.saveDocument(con, theTransaction, srcDoc);
        }

        HashMap<String, ParcelTransfer> transfers = new HashMap<String, ParcelTransfer>();
        tran.transfers.forEach((p) -> {
            transfers.put(p.parcelUID, p);
        });

        lrm.lrStartTransaction(con, theTransaction, holding.holdingUID);
        String destHoldingUID = null;
        if (tran.wordaAdministrtion == null) {
            throw new IllegalArgumentException(session.text("State holder are not registered"));
        } else {
            LADM.Holding newHolding = new LADM.Holding();
            newHolding.holdingType = LADM.Holding.HOLDING_TYPE_STATE;
            destHoldingUID = lrm.lrCreateNewHolding(con, theTransaction, newHolding);
        }

        Iterator<Parcel> parcelIter = holding.parcels.iterator();
        double minSize = session.worlaisSession.getMinAreaConfig(con, ladm);
        while (parcelIter.hasNext()) {
            LADM.Parcel parcel = parcelIter.next();
            if (!transfers.containsKey(parcel.parcelUID)) {
                continue;
            }
            ParcelTransfer transfer = transfers.get(parcel.parcelUID);
            if (transfer.splitParcel) {
                if (parcel.areaGeom < minSize) {
                        throw new IllegalArgumentException(session.text("This parcel can not be split because it is too small"));
                    }

                LADM.Parcel splitParcel = parcel.clonedObjectNoRR();
                List<Map.Entry<String, LADM.Parcel>> toHoldings = new ArrayList<>();
                splitParcel.resetGeom();
                splitParcel.mreg_actype = Parcel.AC_TYPE_EXPROPRIATION;
                splitParcel.mreg_acyear = EthiopianCalendar.ToEth(theTransaction.time).Year;
                for (PartyItem woredaAdmin : tran.wordaAdministrtion) {
                    LADM.Right r = new LADM.Right();
                    r.party = woredaAdmin.party;
                    r.share=new RationalNum(1,1);
                    r.rightType = LADM.Right.RIGHT_SUR;
                    splitParcel.rights.add(r);
                }
                toHoldings.add(new AbstractMap.SimpleEntry<>(destHoldingUID, splitParcel));
                splitParcel = parcel.clonedObjectNoRR();
                splitParcel.resetGeom();
                splitParcel.rights.addAll(parcel.rights);
                toHoldings.add(new AbstractMap.SimpleEntry<>(holding.holdingUID, splitParcel));

                lrm.lrSplitParcel(con, theTransaction, parcel.parcelUID, holding.holdingUID, toHoldings);
            } else {
                List<LADM.Right> rights = new ArrayList<>();
                for (PartyItem woredaAdmin : tran.wordaAdministrtion) {
                    LADM.Right r = new LADM.Right();
                    r.party = woredaAdmin.party;
                    r.rightType = LADM.Right.RIGHT_SUR;
                    r.share=new RationalNum(1, 1);
                    rights.add(r);
                }
                lrm.lrTransferParcel(con, theTransaction, parcel.parcelUID, holding.holdingUID, destHoldingUID, LADM.Parcel.AC_TYPE_GIFT, EthiopianCalendar.ToEth(theTransaction.time).Year, rights);
            }
            
        }
         lrm.finishTransaction(con, theTransaction);
    }

    @Override
    public void saveTransaction(UserSession session, Connection con, LATransaction theTransaction, boolean register) throws Exception {
        try {
            ExpropriationTransactionData tran = (ExpropriationTransactionData) theTransaction.data;
            LADM existing = new GetLADM(session, "nrlais_inventory").get(con, tran.holdingUID, LADM.CONTENT_FULL);
            String error = existing.getRuleErrorsAsString(theTransaction.time);
            if (!org.apache.commons.lang3.StringUtils.isEmpty(error)) {
                throw new IllegalArgumentException(error);
            }
            if (existing.holding.hasNoneHoldingRight()) {
                throw new IllegalAccessException(session.text("expropration transaction can not be execute on a holding with none-holding rights"));

            }
            if (!register) {
                return;
            }
            super.transferLandRecordToTransaction(session, con, theTransaction.transactionUID, existing);
            registerTransaction(session, con, theTransaction, tran);
        } catch (CloneNotSupportedException | IOException ex) {
            throw new SQLException(session.text("Can not Excute Transaction"), ex);
        }
    }
}
