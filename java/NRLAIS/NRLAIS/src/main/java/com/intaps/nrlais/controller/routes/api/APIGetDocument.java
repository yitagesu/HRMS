/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.routes.api;

import com.intaps.nrlais.Startup;
import com.intaps.nrlais.model.SourceDocument;
import com.intaps.nrlais.repo.MainFacade;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Teweldemedhin Aberra
 */
@WebServlet("/api/get_doc")
public class APIGetDocument extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String doc_uid = req.getParameter("doc_uid");
        try {
            SourceDocument per = new MainFacade(Startup.getSessionByRequest(req)).getDocument(doc_uid, false);
            String mime = per.mimeType;
            resp.setContentType(mime);
            File f = new File("data/docFolder/"+doc_uid );
            if (f.exists()) {
                try (FileInputStream infile = new FileInputStream(f)) {
                    try (ServletOutputStream output = resp.getOutputStream();) {
                        org.apache.commons.io.IOUtils.copy(infile, output, 4096);
                    }
                }
            }

        } catch (Exception ex) {
            Logger.getLogger(APIGetDocument.class.getName()).log(Level.SEVERE, null, ex);
            super.doGet(req, resp);
        }
        return;
    }

}
