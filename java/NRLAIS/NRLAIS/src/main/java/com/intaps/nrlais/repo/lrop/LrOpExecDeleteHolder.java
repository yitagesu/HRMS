/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.repo.lrop;

import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.tran.ManualTransactionData;
import com.intaps.nrlais.repo.GetLADM;
import com.intaps.nrlais.repo.LRManipulator;
import com.intaps.nrlais.repo.ManualLRManipulatorFacade;
import java.io.IOException;
import java.sql.Connection;

/**
 *
 * @author Tewelde
 */
public class LrOpExecDeleteHolder implements ManualLRManipulatorFacade.LROPExecutor<ManualTransactionData.LrOpRemoveHolder> {

    @Override
    public Object doOperation(UserSession session, Connection con, LATransaction tran, ManualTransactionData.LrOpRemoveHolder op) throws Exception {
        LRManipulator lrm = new LRManipulator(session);
        GetLADM ladm = new GetLADM(session, "nrlais_transaction");
       
        LADM.Holding holding = ladm.get(con, op.holdingUID, LADM.CONTENT_FULL).holding;
        lrm.lrStartTransaction(con, tran, op.holdingUID);
        for (LADM.Parcel p : holding.parcels) {
            for (LADM.Right r : p.getHolders()) 
            {
            if (r.partyUID.equals(op.partyUID)) {
                    lrm.lrRemoveRight(con, tran, r.holdingUID, r.parcelUID, r);
                }
            }
        }
        lrm.finishTransaction(con, tran,false);
        return null;
    }

}
