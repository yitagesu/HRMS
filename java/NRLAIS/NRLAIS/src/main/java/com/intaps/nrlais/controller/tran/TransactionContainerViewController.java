/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.tran;

import com.intaps.nrlais.controller.ViewControllerBase;
import com.intaps.nrlais.model.LATransaction;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Tewelde
 */
public class TransactionContainerViewController extends ViewControllerBase {

    public TransactionViewController typedController = null;
    public int tranType = -1;
    public String txuid;

    public TransactionContainerViewController(HttpServletRequest request, HttpServletResponse response) throws SQLException, Exception {
        super(request, response);
        txuid = request.getParameter("tran_uid");
        if (!StringUtils.isEmpty(txuid)) {
            LATransaction tran = mainFacade.getTransaction(txuid, false);
            tranType = tran.transactionType;
        } else {
            tranType = Integer.parseInt(request.getParameter("tran_type"));
        }
        typedController=TransactionViewController.createController(tranType,request, response,true);
    }

}
