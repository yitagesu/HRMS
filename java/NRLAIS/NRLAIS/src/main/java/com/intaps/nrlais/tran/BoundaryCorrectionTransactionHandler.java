/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.tran;

import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.CMSSEditParcelTask;
import com.intaps.nrlais.model.DocumentTypeInfo;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.SourceDocument;
import com.intaps.nrlais.model.tran.Applicant;
import com.intaps.nrlais.model.tran.BoundaryCorrectionTransactionData;
import com.intaps.nrlais.repo.CMSSTaskManager;
import com.intaps.nrlais.repo.DocumentTypeRepo;
import com.intaps.nrlais.repo.GetLADM;
import com.intaps.nrlais.repo.LRManipulator;
import com.intaps.nrlais.repo.TransactionRepo;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.UUID;

/**
 *
 * @author Tewelde
 */
public class BoundaryCorrectionTransactionHandler extends TransactionHandlerBase implements TransactionHandler {
    
    void registerTransaction(UserSession session, Connection con, LATransaction theTransaction, BoundaryCorrectionTransactionData tran) throws SQLException, Exception {
        LRManipulator lrm = new LRManipulator(session);
        CMSSTaskManager cmss = new CMSSTaskManager(session);
        TransactionRepo tranRepo = new TransactionRepo(session);
        GetLADM repo = new GetLADM(session, "nrlais_transaction");
        LADM ladm = repo.get(con, tran.parcel.holdingUID, LADM.CONTENT_FULL);
        LADM.Holding holding = ladm.holding;

        HashMap<String, Applicant> applicants = new HashMap<>();
        for (Applicant ap : tran.applicants) {
            if (applicants.containsKey(ap.selfPartyUID)) {
                throw new IllegalArgumentException(session.text("Duplicate applicant")+" "+ ap.selfPartyUID);
            }
            applicants.put(ap.selfPartyUID, ap);
        }
        for (LADM.Right r : holding.getHolders()) {
            if (!applicants.containsKey(r.partyUID)) {
                throw new IllegalArgumentException(session.text("Please Provide applicant information for")+" " + r.party.getFullName());
            }
            Applicant ap = applicants.get(r.partyUID);
            if (ap.idDocument == null) {
                throw new IllegalArgumentException(session.text("ID Document not provided for")+" " + r.party.getFullName());
            } else {
                ap.idDocument.uid = UUID.randomUUID().toString();
                tranRepo.saveDocument(con, theTransaction, ap.idDocument);
            }
            if (ap.representative != null) {
                lrm.createParty(con, theTransaction, ap.representative);
                if (ap.letterOfAttorneyDocument == null) {
                    throw new IllegalArgumentException(session.text("Autorized Representation Document not provided for")+" " + ap.representative.getFullName());

                }
                ap.letterOfAttorneyDocument.uid = UUID.randomUUID().toString();
                ap.letterOfAttorneyDocument.sourceType = DocumentTypeInfo.DOC_TYPE_LETTER_OF_ATTORNEY;
                tranRepo.saveDocument(con, theTransaction, ap.letterOfAttorneyDocument);
            }
            if (ap.pomDocument != null) {
                ap.pomDocument.uid = UUID.randomUUID().toString();
                if (r.party.maritalstatus == LADM.Party.MARITAL_STATUS_MARRIED) {
                    ap.pomDocument.sourceType = DocumentTypeInfo.DOC_TYPE_PROOF_OF_MARRAIGE;
                }else{
                    ap.pomDocument.sourceType=DocumentTypeInfo.DOC_TYPE_PROOF_OF_CELEBACY;
                }
            }
        }
        
        
//        SourceDocument[] allDocs=new SourceDocument[]{tran.landHoldingCertificateDoc};
//        int[] allTypes=new int[]{DocumentTypeInfo.DOC_TYPE_LAND_HOLDING_CERTIFICATE};
//        DocumentTypeRepo docTypeRepo=new DocumentTypeRepo(session);
//        for (int i=0; i<allDocs.length; i++){
//            SourceDocument srcDoc=allDocs[i];
//            if(srcDoc==null){
//                throw new IllegalArgumentException("Required Document of "+docTypeRepo.getDocumentType(con, srcDoc.sourceType)+" is not provided");
//            }
//            srcDoc.uid=UUID.randomUUID().toString();
//            srcDoc.sourceType=allTypes[i];
//            tranRepo.saveDocument(con, theTransaction, srcDoc);
//        }
        CMSSEditParcelTask edit=new CMSSEditParcelTask();
        edit.transactionUID=theTransaction.transactionUID;
        edit.parcels.add(tran.parcel.parcelUID);
        for(BoundaryCorrectionTransactionData.HoldingParcel p:tran.adjucentParcels)
            edit.parcels.add(p.parcelUID);
        cmss.addEditParcelTask(con, edit);
    }

    @Override
    public void saveTransaction(UserSession session, Connection con, LATransaction theTransaction, boolean register) throws Exception {
        try{
            BoundaryCorrectionTransactionData tran=(BoundaryCorrectionTransactionData) theTransaction.data;            
            if(!register){
                return;
            }
            GetLADM g=new GetLADM(session, "nrlais_inventory");
            LADM ladm=g.getByParcel(con, tran.parcel.parcelUID, LADM.CONTENT_FULL);
            tran.parcel.holdingUID=ladm.holding.holdingUID;
            super.transferLandRecordToTransaction(session, con, theTransaction.transactionUID, ladm);
            HashSet<String> locked=new HashSet<>();
            locked.add(tran.parcel.holdingUID);
            for(BoundaryCorrectionTransactionData.HoldingParcel p:tran.adjucentParcels)
            {
                ladm=g.getByParcel(con, p.parcelUID, LADM.CONTENT_FULL);
                p.holdingUID=ladm.holding.holdingUID;
                if(!locked.contains(p.holdingUID))
                {
                    super.transferLandRecordToTransaction(session, con, theTransaction.transactionUID, ladm);
                    locked.add(p.holdingUID);
                }
            }
            registerTransaction(session, con, theTransaction, tran);
        }
        catch(CloneNotSupportedException | IOException ex){
            throw new SQLException(session.text("Can not Excute Transaction"), ex);
        }
    }
}
