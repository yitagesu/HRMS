/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.tran;

import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.tran.Applicant;
import com.intaps.nrlais.model.tran.DivorceTransactionData;
import com.intaps.nrlais.model.tran.Partition;
import com.intaps.nrlais.model.tran.PartyParcelShare;
import com.intaps.nrlais.model.tran.SplitHoldingTransactionData;
import com.intaps.nrlais.util.GSONUtil;
import com.intaps.nrlais.util.INTAPSLangUtils;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author user
 */
public class SplitViewController extends TransactionViewController<SplitHoldingTransactionData> {
    
   public LADM.Holding holding;

    public SplitViewController(HttpServletRequest request, HttpServletResponse response) throws Exception {
        super(request, response, SplitHoldingTransactionData.class, LATransaction.TRAN_TYPE_SPLIT);
    }

    public SplitViewController(HttpServletRequest request, HttpServletResponse response, boolean loadData) throws Exception {
        super(request, response, SplitHoldingTransactionData.class, LATransaction.TRAN_TYPE_SPLIT, loadData);
        if (loadData) {
            if (data.holdingUID == null) {
                holding = new LADM.Holding();
            } else {
                if (tran.isCommited()) {
                    holding = mainFacade.getFromHistoryByArchiveTx(tran.transactionUID, data.holdingUID).holding;
                } else {
                    holding = mainFacade.getLADM("nrlais_inventory", data.holdingUID).holding;
                }

            }
            kebele = mainFacade.getKebele(holding.nrlais_kebeleid);
            woreda = mainFacade.getWoreda(holding.nrlais_woredaid);
        }
    }

    public Applicant partyApplicant(String partyUID) {
        for (Applicant ap : this.data.applicants) {
            if (partyUID.equals(ap.selfPartyUID)) {
                return ap;
            }
        }
        return null;
    }

    public String letterOfAttroneyText(Applicant ap) {
        if (ap == null) {
            return "";
        }

        if (ap.letterOfAttorneyDocument == null) {
            return "";
        }
        return ap.letterOfAttorneyDocument.refText;
    }

    public String letterOfAttroneyLink(Applicant ap) {
        if (ap == null) {
            return "";
        }
        return showDocumentLink(ap.letterOfAttorneyDocument);
    }

    public Partition partition(String parcelUID) {
        for (Partition partition : this.data.partitions) {
            if (partition.parcelUID.equals(parcelUID)) {
                return partition;
            }
        }
        return null;
    }

    public PartyParcelShare share(Partition partition, String partyUID) {
        for (PartyParcelShare sh : partition.shares) {
            if (sh.partyUID.equals(partyUID)) {
                return sh;
            }
        }
        return null;
    }
    public String landHoldingCertifcateText() {
        if (this.data.landHoldingCertifcateDocument == null) {
            return "";
        }
        return this.data.landHoldingCertifcateDocument.refText;
    }

    public String landHoldingCertifcateLink() {
        return showDocumentLink(this.data.landHoldingCertifcateDocument);
    }

    public String holdingJson() {
        return GSONUtil.toJson(this.holding);
    }

    
//    @Override
//    public List<CertificateLink> certficateLinks() throws SQLException, IOException {
//        List<TransactionViewController.CertificateLink> ret = new ArrayList<TransactionViewController.CertificateLink>();
//        HashSet<String> done = new HashSet<String>();
//        for (Partition p : this.data.partitions) {
//            for (PartyParcelShare sh : p.shares) {
//                if(sh.parcelUID==null)
//                    continue;
//                if (done.contains(sh.parcelUID)) {
//                    continue;
//                }
//                done.add(sh.parcelUID);
//                CertificateLink link = new CertificateLink();
//                LADM.Parcel l = mainFacade.getHistoryParcelBySourceTx(this.tran.transactionUID, sh.parcelUID);
//                if (l==null || l.rights.size() == 0) {
//                    continue;
//                }
//                for(LADM.Right r:l.getHolders())
//                {
//                    link.owner=INTAPSLangUtils.appendStringList(link.owner, ", ", r.party.getFullName());
//                }
//                link.url = "javascript:transcation_showCertiricate('" + tran.transactionUID + "','" + l.rights.get(0).holdingUID + "','" + sh.parcelUID + "')";
//                link.upid=l.upid;
//                ret.add(link);
//            }
//        }
//        ret.sort(new Comparator<CertificateLink>() {
//            @Override
//            public int compare(CertificateLink o1, CertificateLink o2) {
//                return o1.owner.compareTo(o2.owner);
//            }
//        });
//        return ret;
//    }

    public boolean showCerticateLink(String oldParcelUID, String partyUID) {

        return (tran.status == LATransaction.TRAN_STATUS_ACCEPTED || tran.status == LATransaction.TRAN_STATUS_FINISHED)
                && this.data.getPartyPartition(oldParcelUID, partyUID) != null;
    }

    public String showCertificateLink(String oldParcelUID, String partyUID) throws SQLException, IOException {
        String newParcelUID = this.data.getPartyPartition(oldParcelUID, partyUID).parcelUID;
        String holdingUID = null;
        LADM.Parcel l = mainFacade.getHistoryParcelBySourceTx(this.tran.transactionUID, newParcelUID);
        if (l.rights.size() == 0) {
            return "";
        }
        return "javascript:transcation_showCertiricate('" + tran.transactionUID + "','" + l.rights.get(0).holdingUID + "','" + newParcelUID + "')";
    }
    public String holdingLink()
    {
        String ret="/holding/holding_information_viewer_page.jsp?holdingUID="+this.holding.holdingUID;
        if(tran.isCommited())
            ret+="&tran_uid="+tran.transactionUID;
        return ret;
    }

    
}
