/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.tran;

import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.tran.Applicant;
import com.intaps.nrlais.model.tran.BoundaryCorrectionTransactionData;
import com.intaps.nrlais.model.tran.SpatialOperationTransactionData;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Tewelde
 */
public class SpatialOperationViewController<T extends SpatialOperationTransactionData> extends TransactionViewController<T>{
    public static class HoldingParcelView {

        public LADM.Holding holding = new LADM.Holding();
        public LADM.Parcel parcel = new LADM.Parcel();
    }
    public HoldingParcelView parcel;
    public List<HoldingParcelView> adjecentParcels;

    public SpatialOperationViewController(HttpServletRequest request, HttpServletResponse response,Class c,int tranType) throws Exception {
        super(request, response, c, tranType);
    }

    public SpatialOperationViewController(HttpServletRequest request, HttpServletResponse response,Class c,int tranType, boolean loadData) throws Exception {
        super(request, response, c,tranType, loadData);
        if (loadData) {
            if (data.parcel == null) {
                parcel = new HoldingParcelView();
                adjecentParcels = new ArrayList<HoldingParcelView>();
            } else {
                
                if (tran.isCommited()) {
                    parcel = new HoldingParcelView();
                    parcel.holding = mainFacade.getFromHistoryByParcelArchiveTx(tran.transactionUID, data.parcel.parcelUID).holding;
                    parcel.parcel = parcel.holding.getParcel(data.parcel.parcelUID);

                } else {
                    parcel = new HoldingParcelView();
                    parcel.holding = mainFacade.getLADMByParcel("nrlais_inventory", data.parcel.parcelUID).holding;
                    parcel.parcel = parcel.holding.getParcel(data.parcel.parcelUID);
                }
                
                adjecentParcels = new ArrayList<HoldingParcelView>();
                if (tran.isCommited()) {
                    for (BoundaryCorrectionTransactionData.HoldingParcel p : this.data.adjucentParcels) {
                        HoldingParcelView parcel = new HoldingParcelView();
                        parcel.holding = mainFacade.getFromHistoryByParcelArchiveTx(tran.transactionUID, p.parcelUID).holding;
                        parcel.parcel = parcel.holding.getParcel(p.parcelUID);
                        adjecentParcels.add(parcel);
                    }
                } else {

                    for (BoundaryCorrectionTransactionData.HoldingParcel p : this.data.adjucentParcels) {
                        HoldingParcelView parcel = new HoldingParcelView();
                        parcel.holding = mainFacade.getLADMByParcel("nrlais_inventory", p.parcelUID).holding;
                        parcel.parcel = parcel.holding.getParcel(p.parcelUID);
                        adjecentParcels.add(parcel);

                    }
                }
            }
            kebele = mainFacade.getKebele(parcel.parcel.nrlais_kebeleid);
            woreda = mainFacade.getWoreda(parcel.parcel.nrlais_woredaid);
        }
    }

    public Applicant partyApplicant(String partyUID) {
        for (Applicant ap : this.data.applicants) {
            if (partyUID.equals(ap.selfPartyUID)) {
                return ap;
            }
        }
        return null;
    }

    public String letterOfAttroneyText(Applicant ap) {
        if (ap == null) {
            return "";
        }

        if (ap.letterOfAttorneyDocument == null) {
            return "";
        }
        return ap.letterOfAttorneyDocument.refText;
    }

    public String letterOfAttroneyLink(Applicant ap) {
        if (ap == null) {
            return "";
        }
        return showDocumentLink(ap.letterOfAttorneyDocument);
    }
}
