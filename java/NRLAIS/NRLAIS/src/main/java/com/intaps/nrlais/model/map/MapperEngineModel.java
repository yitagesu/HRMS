package com.intaps.nrlais.model.map;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

public class MapperEngineModel {

    public static class MEPoint {

        static int next_id = 1;
        public int id;
        public double x;
        public double y;
        private MEPoint detachedClone;

        private transient HashMap<MEPointSeries, ArrayList<MEPoint>> connectedTo = new HashMap<MEPointSeries, ArrayList<MEPoint>>();
        private transient ArrayList<MEPoint> simpleConnection = new ArrayList<>();

        public HashMap<MEPointSeries, ArrayList<MEPoint>> getConnectionsList() {
            return connectedTo;
        }

        public MEPoint(int id, double x, double y) {
            this.id = id == -1 ? next_id++ : id;
            this.x = x;
            this.y = y;
        }

        public HashSet<MEPoint> getUniqeConnections() {
            HashSet<MEPoint> ret = new HashSet<>();
            for (Map.Entry<MEPointSeries, ArrayList<MEPoint>> con : connectedTo.entrySet()) {
                for (MEPoint p : con.getValue()) {
                    if (ret.contains(p)) {
                        continue;
                    }
                    ret.add(p);
                }
            }
            return ret;
        }

        public void addConnection(MEPointSeries series, MEPoint p) {

            ArrayList<MEPoint> cons;
            if (connectedTo.containsKey(series)) {
                cons = connectedTo.get(series);
            } else {
                connectedTo.put(series, cons = new ArrayList<MEPoint>());
            }
            if (cons.contains(p)) {
                return;
            }
            cons.add(p);
        }

        public void removeConnection(MEPointSeries series, MEPoint p) {
            ArrayList<MEPoint> cons;
            if (connectedTo.containsKey(series)) {
                cons = connectedTo.get(series);
                if (!cons.contains(p)) {
                    throw new IllegalArgumentException("Connection doesn't exist");
                }
                cons.remove(p);
                if (cons.size() == 0) {
                    connectedTo.remove(series);
                }
            } else {
                throw new IllegalArgumentException("Connection doesn't exist");
            }
        }

        public MEPoint(double x, double y) {
            this(-1, x, y);
        }

        public MEPoint() {
            this(-1, 0, 0);
        }

        public MEPoint getDetachedClone() {
            MEPoint ret = new MEPoint();
            ret.id = this.id;
            ret.setX(this.getX());
            ret.setY(this.getY());
            return ret;
        }

        public void copyCoordinateFrom(MEPoint clonePoint) {
            this.setX(clonePoint.getX());
            this.setY(clonePoint.getY());
        }

        public double distanceToSquare(MEPoint p) {
            return (p.getX() - getX()) * (p.getX() - getX()) + (p.getY() - getY()) * (p.getY() - getY());
        }

        public double distanceTo(MEPoint p) {
            return Math.sqrt((p.getX() - getX()) * (p.getX() - getX()) + (p.getY() - getY()) * (p.getY() - getY()));
        }

        public MEPoint add(MEPoint point) {
            /*this.x+=point.x;
            this.y+=point.y;
            return this;*/
            return new MEPoint(this.getX() + point.getX(), this.getY() + point.getY());
        }

        public MEPoint sub(MEPoint p) {
            return new MEPoint(this.getX() - p.getX(), this.getY() - p.getY());
        }

        public double abs() {
            return Math.sqrt(getX() * getX() + getY() * getY());
        }

        public MEPoint mult(double k) {
            return new MEPoint(k * getX(), k * getY());
        }

        private double cross(MEPoint p) {
            return getX() * p.getY() - getY() * p.getX();
        }

        private MEPoint pixlate(double factor) {
            return new MEPoint(Math.round(factor * getX()), Math.round(factor * getY()));
        }

        /**
         * @return the x
         */
        public double getX() {
            return x;
        }

        /**
         * @return the y
         */
        public double getY() {
            return y;
        }

        /**
         * @param x the x to set
         */
        public void setX(double x) {
            this.x = x;
        }

        /**
         * @param y the y to set
         */
        public void setY(double y) {
            this.y = y;
        }
    }

    public static enum LineFeatureType {
        ParcelBorder,
        KebeleBorder,
        WoredaBorder,
        RegionBorder,
        Unknown,
    }

    public static class MEBox {

        public double x1;
        public double y1;
        public double x2;
        public double y2;

        public MEBox(MEPoint p) {
            this(p.getX(), p.getY(), p.getX(), p.getY());
        }

        public MEBox(double x1, double y1, double x2, double y2) {
            this.x1 = Math.min(x1, x2);
            this.y1 = Math.min(y1, y2);
            this.x2 = Math.max(x1, x2);
            this.y2 = Math.max(y1, y2);
        }

        public MEBox(MEPoint p1, MEPoint p2) {
            this(p1.getX(), p1.getY(), p2.getX(), p2.getY());
        }

        public void union(MEBox other) {
            this.setX1(Math.min(this.getX1(), other.getX1()));
            this.setY1(Math.min(this.getY1(), other.getY1()));
            this.setX2(Math.max(this.getX2(), other.getX2()));
            this.setY2(Math.max(this.getY2(), other.getY2()));
        }

        public double width() {
            return getX2() - getX1();
        }

        public double height() {
            return getY2() - getY1();
        }

        public MEPoint bottomLeft() {
            return new MEPoint(getX1(), getY1());
        }

        public MEPoint bottomRight() {
            return new MEPoint(getX2(), getY1());
        }

        public MEPoint topRight() {
            return new MEPoint(getX2(), getY2());
        }

        public MEPoint topLeft() {
            return new MEPoint(getX1(), getY2());
        }

        //MASSREG
        public void expand(double f) {
            double x1p = (getX2() * (1 - f) + getX1() * (1 + f)) / 2;
            double x2p = (getX2() * (1 + f) + getX1() * (1 - f)) / 2;
            setX1(x1p);
            setX2(x2p);

            double y1p = (getY2() * (1 - f) + getY1() * (1 + f)) / 2;
            double y2p = (getY2() * (1 + f) + getY1() * (1 - f)) / 2;
            setY1(y1p);
            setY2(y2p);

        }

        //MASSREG
        public void setWidth(double w) {
            double mid = (getX1() + getX2()) / 2;
            setX1(mid - w / 2);
            setX2(mid + w / 2);
        }

        //MASSREG
        public void setHeight(double h) {
            double mid = (getY1() + getY2()) / 2;
            setY1(mid - h / 2);
            setY2(mid + h / 2);
        }

        //MASSREG
        public double getX1() {
            return x1;
        }

        //MASSREG
        public void setX1(double x1) {
            this.x1 = x1;
        }

        //MASSREG
        public double getY1() {
            return y1;
        }

        //MASSREG
        public void setY1(double y1) {
            this.y1 = y1;
        }

        //MASSREG
        public double getX2() {
            return x2;
        }

        //MASSREG
        public void setX2(double x2) {
            this.x2 = x2;
        }

        //MASSREG
        public double getY2() {
            return y2;
        }

        //MASSREG
        public void setY2(double y2) {
            this.y2 = y2;
        }

    }

    public static class MEPointSeries {

        public int id = MEPoint.next_id++;
        public LineFeatureType lineType = LineFeatureType.Unknown;
        public boolean polygon;
        public ArrayList<MEPoint> points = new ArrayList<>();

        public MEPoint getPointByID(int id) {
            for (MEPoint p : points) {
                if (p.id == id) {
                    return p;
                }
            }
            return null;
        }

        public MEBox boundingBox() {
            if (this.points.size() == 0) {
                return null;
            }
            MEBox ret = new MEBox(this.points.get(0));
            for (int i = 1; i < points.size(); i++) {
                ret.union(new MEBox(this.points.get(i)));
            }
            return ret;
        }

        public MEPointSeries() {

        }

        public MEPointSeries(ArrayList<MEPoint> points) {
            this.points = points;
        }

        public MEPointSeries(ArrayList<MEPoint> points, boolean polygon) {
            this.points = points;
            this.polygon = true;
        }

        public void fixConnections() {
            for (int i = 0; i < this.points.size(); i++) {
                if (this.polygon || i > 0) {
                    this.points.get(i).addConnection(this, this.points.get(i > 0 ? i - 1 : this.points.size() - 1));
                }
                if (this.polygon || i < this.points.size() - 1) {
                    this.points.get(i).addConnection(this, this.points.get((i + 1) % this.points.size()));
                }
            }
        }

        public void releaseConnections() {
            for (int i = 0; i < this.points.size(); i++) {
                if (this.polygon || i > 0) {
                    this.points.get(i).removeConnection(this, this.points.get(i > 0 ? i - 1 : this.points.size() - 1));
                }
                if (this.polygon || i < this.points.size() - 1) {
                    this.points.get(i).removeConnection(this, this.points.get((i + 1) % this.points.size()));
                }
            }
        }

        public void insertPoint(MEPoint p1, MEPoint p2, MEPoint p) {
            int i1 = this.points.indexOf(p1);
            int i2 = this.points.indexOf(p2);
            insertPoint(i1, i2, p);
        }

        int nextIndex(int i) {
            if (polygon) {
                return (i + 1) % +points.size();
            }
            return i == points.size() - 1 ? -1 : i + 1;
        }

        int prevIndex(int i) {
            if (polygon) {
                return (points.size() + i - 1) % +points.size();
            }
            return i == 0 ? -1 : i - 1;
        }

        public int indexDiff(int i1, int i2) {
            if (polygon) {
                return Math.min((i1 - i2 + points.size()) % points.size(), (i2 - i1 + points.size()) % points.size());
            }
            return Math.abs(i2 - i1);
        }

        public int insertPoint(int i1, int i2, MEPoint p) {
            int ret;
            MEPoint p1 = i1 == -1 ? null : this.points.get(i1);
            MEPoint p2 = i2 == -1 ? null : this.points.get(i2);

            if (i1 == -1 || i2 == -1) {
                if (polygon) {
                    throw new IllegalArgumentException("Both right and left indexes should be none negative for insertPoint operation on polygon");
                }
                int i = Math.max(i1, i2);
                if (i == 0) {
                    points.add(0, p);
                    ret = 0;
                } else if (i == points.size() - 1) {
                    points.add(p);
                    ret = points.size() - 1;
                } else {
                    throw new IllegalArgumentException("If either of the right and left index are negative the other index should be at end points for insert operation");
                }

            } else {
                if (polygon) {
                    if (indexDiff(i1, i2) != 1) {
                        throw new IllegalArgumentException("Points are note consecutive");
                    }
                }

                if (i1 == prevIndex(i2)) {
                    ret = i2;
                } else {
                    ret = i1;
                }

                this.points.add(ret, p);
            }

            if (p1 != null) {
                if (p2 != null) {
                    p1.removeConnection(this, p2);
                }
                p.addConnection(this, p1);
                p1.addConnection(this, p);
            }
            if (p2 != null) {
                if (p1 != null) {
                    p2.removeConnection(this, p1);
                }
                p.addConnection(this, p2);
                p2.addConnection(this, p);
            }
            return ret;
        }

        public void removePoint(MEPoint p) {
            int index = this.points.indexOf(p);
            if (index == -1) {
                throw new IllegalArgumentException("Point not member of originalSeries");
            }
            removePoint(index);
        }

        public void removePoint(int index) {
            MEPoint p = this.points.get(index);
            MEPoint leftPoint = null, rightPoint = null;
            if (this.points.size() > 0) {
                if (polygon || index > 0) {
                    leftPoint = this.points.get(index == 0 ? points.size() - 1 : index - 1);
                }
                if (polygon || index < this.points.size() - 1) {
                    rightPoint = this.points.get(index == points.size() - 1 ? 0 : index + 1);
                }
            }
            if (leftPoint != null) {
                leftPoint.removeConnection(this, p);
                if (rightPoint != null) {
                    rightPoint.addConnection(this, leftPoint);
                }
            }
            if (rightPoint != null) {
                rightPoint.removeConnection(this, p);
                if (leftPoint != null) {
                    leftPoint.addConnection(this, rightPoint);
                }
            }

            this.points.remove(index);
        }

        public MEPointSeries getDetachedClone(HashMap<MEPoint, MEPoint> cache) {
            MEPointSeries ret = new MEPointSeries();
            ret.id = this.id;
            ret.lineType = this.lineType;
            ret.polygon = this.polygon;
            for (MEPoint pnt : points) {
                if (cache.containsKey(pnt)) {
                    ret.points.add(cache.get(pnt));
                } else {
                    MapperEngineModel.MEPoint clonePoint = pnt.getDetachedClone();
                    ret.points.add(clonePoint);
                    cache.put(pnt, clonePoint);
                }
            }
            ret.fixConnections();
            return ret;
        }

        public void insertPoint(int i, MEPoint point) {
            int i1 = i - 1;
            if (i1 == -1 && polygon) {
                i1 = this.points.size() - 1;
            }
            insertPoint(i1, i, point);
        }

        public void replacePoint(int index, MEPoint newPoint) {
            MEPoint oldPoint = this.points.get(index);
            int prev = prevIndex(index);
            int next = nextIndex(index);
            MEPoint p1 = prev == -1 ? null : this.points.get(prev);
            MEPoint p2 = next == -1 ? null : this.points.get(next);
            if (p1 != null) {
                oldPoint.removeConnection(this, p1);
                p1.removeConnection(this, oldPoint);
                newPoint.addConnection(this, p1);
                p1.addConnection(this, newPoint);
            }
            if (p2 != null) {
                oldPoint.removeConnection(this, p2);
                p2.removeConnection(this, oldPoint);
                newPoint.addConnection(this, p2);
                p2.addConnection(this, newPoint);
            }
            this.points.set(index, newPoint);
        }

        private void reduceToResolution(double factor) {
            ArrayList<MEPoint> ps = new ArrayList<>();
            //first two point are added
            for (int i = 0; i < 2 && i < this.points.size(); i++) {
                ps.add(this.points.get(i));
            }
            //all subsequent check for reduction
            for (int i = 2; i < this.points.size(); i++) {
                //check if this point is collinear with the previous two points
                MEPoint p = this.points.get(i).pixlate(factor);
                int sz = ps.size();
                MEPoint p2 = ps.get(sz - 1).pixlate(factor);
                MEPoint p1 = ps.get(sz - 2).pixlate(factor);
                if (Math.abs(p.sub(p1).cross(p2.sub(p1))) < 0.001) {
                    ps.set(sz - 1, this.points.get(i));
                } else {
                    ps.add(this.points.get(i));
                }
            }
//            if(polygon && ps.size()>2)
//            {
//                //last point first point second point case
//                MEPoint p1=ps.get(ps.size()-1).pixlate(factor);
//                MEPoint p2=ps.get(0).pixlate(factor);
//                MEPoint p3=ps.get(1).pixlate(factor);
//                if(Math.abs(p3.sub(p1).cross(p2.sub(p1)))<0.001)
//                {
//                    ps.remove(ps.size() -1);
//                    ps.set(0,ps.get(ps.size()-1));
//                }
//                if(ps.size()>2)
//                {
//                    p1=ps.get(ps.size()-2).pixlate(factor);
//                    p2=ps.get(ps.size()-1).pixlate(factor);
//                    p3=ps.get(0).pixlate(factor);
//                    if(Math.abs(p3.sub(p1).cross(p2.sub(p1)))<0.001)
//                    {
//                        ps.remove(ps.size() -1);
//                    }
//                }
//            }
            this.points = ps;
        }

        public double area() {
            double num = 0.0;

            int length = this.points.size();
            if (length >= 3) {
                MEPoint p = this.points.get(0);
                MEPoint point2 = this.points.get(1);
                for (int j = 2; j < length; j++) {
                    MEPoint point3 = this.points.get(j);
                    num += point2.sub(p).cross(point3.sub(p));
                    point2 = point3;
                }
            }
            return (Math.abs(num) / 2.0);
        }

    }

    public static class MEMultiPointSeries {

        public int id = MEPoint.next_id++;
        public ArrayList<MEPointSeries> rings = new ArrayList<>();

        public MEBox boundingBox() {
            if (this.rings.size() == 0) {
                return null;
            }
            MEBox ret = null;
            for (MEPointSeries ring : this.rings) {
                if (ret == null) {
                    ret = ring.boundingBox();
                } else {
                    ret.union(ring.boundingBox());
                }
            }
            return ret;
        }

        public void reduceToResolution(double factor) {
            for (MEPointSeries rng : rings) {
                //rng.reduceToResolution(factor);
            }
        }

        public MEPoint centroid() {
            MEPoint p = null;
            int n = 0;
            for (MEPointSeries s : rings) {
                for (MEPoint pp : s.points) {
                    if (p == null) {
                        p = pp;
                    } else {
                        p.setX(p.x + pp.getX());
                        p.setY(p.y + pp.getY());
                    }
                    n++;
                }
            }
            p.setX(p.getX() / n);
            p.setY(p.getY() / n);
            return p;
        }

        public double area() {
            double ret=0;
            boolean first=true;
            for(MEPointSeries pol:rings)
            {
                if(first)
                {
                    ret+=pol.area();
                    first=false;
                }
                else
                    ret-=pol.area();
            }
            return ret;
        }

    }

    public static class MEParcel {

        public String upi;
        public int parcelNo;
        public int[] polygonIDs;
        public ArrayList<MEMultiPointSeries> polygons = new ArrayList<>();

        public MEBox boundingBox() {
            if (this.polygons.size() == 0) {
                return null;
            }
            MEBox ret = null;
            for (MEMultiPointSeries parcel : this.polygons) {
                if (ret == null) {
                    ret = parcel.boundingBox();
                } else {
                    ret.union(parcel.boundingBox());
                }
            }
            return ret;
        }
    }

    public static class METileMipMapItem {

        public int fileSize;
        public int pw, ph;
    }

    public static class METile {

        public int id;
        public String tileTag;
        public double x1, y1;
        public double x2, y2;
        public int pw, ph;

        public ArrayList<METileMipMapItem> mipMaps = new ArrayList<>();
    }

    public static class MappingTeam {

        public String name1;
        public String name2;
    }

    public static class MEOrthoPhoto {

        public ArrayList<METile> tiles = new ArrayList<>();
    }

    public static class METeamParcelRange {

        public int from;
        public int to;
    }

    public static class METeam {

        public int id;
        public String teamName;
        public String teamDescription;
        public ArrayList<MEPointSeries> assignment = new ArrayList<>();
        public ArrayList<METeamParcelRange> parcelRanges = new ArrayList<>();
    }

    public static class MESetting {

        public byte[] settingID;
        public ArrayList<METeam> teams = new ArrayList<>();
        public METeam team = null;
        public String utmZone = "UTM37";
        public MEOrthoPhoto orthophoto = new MEOrthoPhoto();
        public MEPoint initialZoomBottomLeft = new MEPoint(0, 0);
        public MEPoint initialZoomTopRight = new MEPoint(500, 200);
    }

    public static class CombinedIterable<T> implements Iterable<T> {

        Iterable<Iterable<T>> iterables;
        Iterator<T> currentIter = null;
        Iterator<Iterable<T>> mainIter;

        public class CombinedIterator implements Iterator<T> {

            public boolean hasNext() {
                if (currentIter == null) {
                    return false;
                }
                if (!currentIter.hasNext()) {
                    if (!mainIter.hasNext()) {
                        return false;
                    }
                    currentIter = mainIter.next().iterator();
                    return hasNext();
                }
                return true;
            }

            public T next() {
                return currentIter.next();
            }

            public void remove() {

            }
        }

        public CombinedIterable(Iterable<Iterable<T>> iterables) {
            this.iterables = iterables;
            mainIter = this.iterables.iterator();
            if (mainIter.hasNext()) {
                currentIter = mainIter.next().iterator();
            }
        }

        @Override
        public Iterator<T> iterator() {
            return null;
        }
    }

    public static interface MEPointProcessor {

        boolean process(MEPoint point);
    }

    public static interface MEPointSeriesProcessor {

        boolean process(MEPointSeries series);
    }

    public static interface MEPolygonProcessor {

        boolean process(MEMultiPointSeries polygon);
    }

    public static class MEMultipleEdgePoint {

        public ArrayList<MEEdgePoint> edges = new ArrayList<>();

        public void merge(MEPointSeries series, MEPoint p1, MEPoint p2, MEPoint np) {
            for (MEEdgePoint e : edges) {
                if (e.series == series
                        && ((p1 == e.p1 && p2 == e.p2) || (p1 == e.p2 && p2 == e.p1))
                        && e.p == np) {
                    return;
                }
            }
            edges.add(new MEEdgePoint(series, p1, p2, np));
        }

        public void fixConnection() {
            for (MEEdgePoint edge : edges) {
                edge.addPointToSeries();
            }
        }

        public void releaseConnection() {
            for (MEEdgePoint edge : edges) {
                edge.removePointFromSeries();
            }
        }

        public MEEdgePoint reduced() {
            MapperEngineModel.MEEdgePoint ep = null;
            for (MapperEngineModel.MEEdgePoint e : this.edges) {
                if (ep == null) {
                    ep = new MEEdgePoint(e.series, e.p1, e.p2, null);
                } else {
                    if (ep.series != e.series) {
                        ep.series = null;
                    }
                    if ((ep.p1 == e.p1 && ep.p2 == e.p2)
                            || (ep.p1 == e.p2 && ep.p2 == e.p1)) {
                        continue;
                    }
                    ep.p1 = null;
                    ep.p2 = null;
                }
            }
            return ep;

        }

        public MEEdgePoint getForSeries(MEPointSeries series) {
            for (MEEdgePoint e : this.edges) {
                if (e.series == series) {
                    return e;
                }
            }
            return null;
        }

        public void setLinePoint(MEPoint linePoint) {
            for (MapperEngineModel.MEEdgePoint e : this.edges) {
                e.p = linePoint;
            }
        }
    }

    public static class MEEdgePoint {

        public MEPoint p1;
        public MEPoint p2;
        public MEPoint p;
        public MEPointSeries series;

        public boolean isComplete() {
            return p1 != null && p2 != null;
        }

        public MEEdgePoint(MEPointSeries series, MEPoint p1, MEPoint p2, MEPoint p) {
            this.p1 = p1;
            this.p2 = p2;
            this.p = p;
            this.series = series;
            if (this.series == null) {
                throw new IllegalArgumentException("Series can't be null");
            }
        }

        public void addPointToSeries() {
            series.insertPoint(this.p1, this.p2, this.p);
        }

        public void removePointFromSeries() {
            series.removePoint(this.p);
        }
    }

    public static class MEEdge {

        public MEPoint p1;
        public MEPoint p2;

        public MEEdge(MEPoint p1, MEPoint p2) {
            this.p1 = p1;
            this.p2 = p2;
        }
    }

    public static class MEData {

        public ArrayList<MEPoint> points = new ArrayList<>();
        public ArrayList<MEPointSeries> pointSerieses = new ArrayList<>();
        public ArrayList<MEMultiPointSeries> polygons = new ArrayList<>();
        public ArrayList<MEParcel> parcels = new ArrayList<>();

        public void processPoints(MEPointProcessor proc) {
            {
                for (MEPoint p : points) {
                    proc.process(p);
                }
            }
            for (MEPointSeries series : pointSerieses) {
                for (MEPoint p : series.points) {
                    proc.process(p);
                }
            }

            for (MEMultiPointSeries poly : this.polygons) {
                for (MEPointSeries series : poly.rings) {
                    for (MEPoint p : series.points) {
                        proc.process(p);
                    }
                }
            }
        }

        public void processSerieses(MEPointSeriesProcessor proc) {

            {
                for (MEPointSeries series : pointSerieses) {
                    proc.process(series);

                }
            }

            for (MEMultiPointSeries poly : this.polygons) {
                for (MEPointSeries series : poly.rings) {
                    proc.process(series);
                }
            }

        }

        public void processPolygons(MEPolygonProcessor proc) {
            for (MEMultiPointSeries poly : this.polygons) {
                proc.process(poly);
            }
        }

        public void clear() {
            points.clear();
            pointSerieses.clear();
            polygons.clear();
            parcels.clear();
        }

        public ArrayList<MEPointSeries> getPointReferences(final MEPoint p) {
            final ArrayList<MEPointSeries> ret = new ArrayList<>();
            processSerieses(new MEPointSeriesProcessor() {
                @Override
                public boolean process(MEPointSeries series) {
                    if (series.points.contains(p)) {
                        ret.add(series);
                    }
                    return true;
                }
            });
            return ret;
        }

        public HashSet<MEPoint> getAllPoints() //can be optimized
        {
            final HashSet<MEPoint> ret = new HashSet<>();
            this.processPoints(new MEPointProcessor() {
                @Override
                public boolean process(MEPoint point) {
                    if (!ret.contains(point)) {
                        ret.add(point);
                    }
                    return true;
                }
            });
            return ret;
        }

        public ArrayList<MEPointSeries> getAllPointSeries() //can be optimized
        {
            final ArrayList<MEPointSeries> ret = new ArrayList<>();
            this.processSerieses(new MEPointSeriesProcessor() {
                @Override
                public boolean process(MEPointSeries series) {
                    ret.add(series);
                    return true;
                }
            });
            return ret;
        }

        public ArrayList<MEEdgeLink> getEdgeLinks(final MEPointSeries target) {
            final ArrayList<MEEdgeLink> ret = new ArrayList<>();
            this.processSerieses(new MEPointSeriesProcessor() {
                @Override
                public boolean process(MEPointSeries series) {
                    if (series == target) {
                        return true;
                    }
                    MEEdgeLink link = new MEEdgeLink();
                    link.linked = series;
                    for (int i = 0; i < target.points.size(); i++) {
                        int dst;
                        if ((dst = series.points.indexOf(target.points.get(i))) == -1) {
                            continue;
                        }
                        link.links.add(new MEEdgeLinkItem(i, dst));
                    }
                    if (link.links.size() > 0) {
                        ret.add(link);
                    }
                    return true;
                }
            });
            return ret;
        }

        public MEMultiPointSeries getPolygon(MEPointSeries series) {
            for (MEMultiPointSeries ps : this.polygons) {
                if (ps.rings.contains(series)) {
                    return ps;
                }
            }
            return null;
        }

        void appendErrorString(StringBuilder sb, String err) {
            if (sb.length() > 0) {
                sb.append("\n");
            }
            sb.append(err);
        }

        public void fixParcelPolygonIDs() {
            for (MapperEngineModel.MEParcel par : this.parcels) {
                par.polygonIDs = new int[par.polygons.size()];
                for (int i = 0; i < par.polygons.size(); i++) {
                    par.polygonIDs[i] = par.polygons.get(i).id;
                }
            }
        }

        public void restoreReferences() {
            final HashMap<Integer, MapperEngineModel.MEPoint> pointsByID = new HashMap<>();
            int maxID = 0;
            for (MapperEngineModel.MEPoint p : this.points) {
                pointsByID.put(p.id, p);
                maxID = Math.max(p.id, maxID);
            }
            for (MapperEngineModel.MEPointSeries s : this.pointSerieses) {
                int sz = s.points.size();
                for (int i = 0; i < sz; i++) {
                    MapperEngineModel.MEPoint p = s.points.get(i);
                    if (pointsByID.containsKey(p.id)) {
                        s.points.set(i, pointsByID.get(p.id));
                    } else {
                        pointsByID.put(p.id, p);
                        maxID = Math.max(p.id, maxID);
                    }
                }
                s.fixConnections();
            }

            for (MapperEngineModel.MEMultiPointSeries ms : this.polygons) {
                for (MapperEngineModel.MEPointSeries s : ms.rings) {
                    int sz = s.points.size();
                    for (int i = 0; i < sz; i++) {
                        MapperEngineModel.MEPoint p = s.points.get(i);
                        if (pointsByID.containsKey(p.id)) {
                            s.points.set(i, pointsByID.get(p.id));
                        } else {
                            pointsByID.put(p.id, p);
                            maxID = Math.max(p.id, maxID);
                        }
                    }
                    s.fixConnections();
                }
            }
            MEPoint.next_id = Math.max(maxID + 1, MEPoint.next_id);
            HashMap<Integer, MEMultiPointSeries> polygonIndex = new HashMap<>();
            for (MEMultiPointSeries p : this.polygons) {
                polygonIndex.put(p.id, p);
            }

            for (MapperEngineModel.MEParcel par : this.parcels) {
                for (int i = 0; i < par.polygonIDs.length; i++) {
                    par.polygons.add(polygonIndex.get(par.polygonIDs[i]));
                }
            }
        }
    }//MEData

    public static class MEEdgeLinkItem {

        public int sourceIndex;
        public int destIndex;

        public MEEdgeLinkItem(int sourceIndex, int destIndex) {
            this.sourceIndex = sourceIndex;
            this.destIndex = destIndex;
        }
    }

    public static class MEEdgeLink {

        public MEPointSeries linked = null;
        public ArrayList<MEEdgeLinkItem> links = new ArrayList<>();
    }

}
