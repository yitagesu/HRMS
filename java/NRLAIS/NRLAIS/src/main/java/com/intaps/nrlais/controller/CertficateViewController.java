/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller;

import com.intaps.nrlais.Startup;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.util.EthiopianCalendar;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.postgis.MultiPolygon;

/**
 *
 * @author Tewelde
 */
public class CertficateViewController extends ViewControllerBase {

    public String woreda = "N/A";
    public String zone = "N/A";
    public String region = "N/A";
    public String kebele = "N/A";
    public String centerX = "N/A";
    public String centerY = "N/A";
    public double area = 0;
    public LADM.Holding holding;
    public LADM.Parcel parcel;
    
    public String approval = null;
    public String approvedDate="";
    
    public String intiator = null;
    public String intiatedDate = "";
    
    public String checker = null;
    public String checkeDate="";
    
    public String issuedDate = null;
    
    public LATransaction tran = null;
    public String currentLandUse="N/A";
    public String soilFertility="N/A";

    public CertficateViewController(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
        super(request, response);
        String holdingUID = request.getParameter("holding_uid");
        String parcelUID = request.getParameter("parcel_uid");
        String txuid = request.getParameter("tran_uid");

        LADM data = mainFacade.getByTx(txuid, holdingUID);
        if (data != null) {
            holding = data.holding;

            woreda = mainFacade.getWoreda(holding.nrlais_woredaid).name.text(Startup.regionToLang(holding.csaregionid));
            zone = mainFacade.getZone(holding.nrlais_zoneid).name.text(Startup.regionToLang(holding.csaregionid));
            region = mainFacade.getRegion(holding.csaregionid).name.text(Startup.regionToLang(holding.csaregionid));
            kebele = mainFacade.getKebele(holding.nrlais_kebeleid).name.text(Startup.regionToLang(holding.csaregionid));
            parcel = holding.getParcel(parcelUID);
            if (parcel != null) {
                area = parcel.areaGeom;

                org.postgis.MultiPolygon m = new MultiPolygon(parcel.geometry);
                int n = m.numPoints();
                double totalX = 0, totalY = 0;
                for (int i = 0; i < n; i++) {
                    totalX += m.getPoint(i).x;
                    totalY += m.getPoint(i).y;
                }
                if (n > 0) {
                    centerX = Double.toString(totalX / n);
                    centerY = Double.toString(totalY / n);
                }
                this.currentLandUse=super.lookupText("nrlais_sys.t_cl_landusetype", parcel.landUse);
                this.soilFertility=super.lookupText("nrlais_sys.t_cl_soilfertilitytype", parcel.soilfertilityType);
                
            }
        }
        tran = mainFacade.getTransaction(txuid, false);
        if (tran != null) {
            for (LATransaction.TranactionFlow f : mainFacade.getTransactionFlow(txuid)) {
                switch (f.newState) {
                    case LATransaction.TRAN_STATUS_INITIATED:
                        if(intiator == null){
                          intiator = session.getUserFullName(f.syscreateby);
                          intiatedDate = EthiopianCalendar.ToEth(f.syscreatedate).toString() + " EC";
                        }
                        break;
                    case LATransaction.TRAN_STATUS_ACCEPTED:
                        if (approval == null) {
                            approval = session.getUserFullName(f.syscreateby);
                            approvedDate = EthiopianCalendar.ToEth(f.syslastmoddate).toString() + " EC";
                        }
                        break;
                    case LATransaction.TRAN_STATUS_CANCELED:
                        if (approval == null) {
                            approval = "Not Approved";
                            
                        }
                        break;
                    case LATransaction.TRAN_STATUS_ACCEPT_REQUESTED:
                        if (checker == null) {
                            checker = session.getUserFullName(f.syscreateby);
                            checkeDate = EthiopianCalendar.ToEth(f.syscreatedate).toString() + " EC";
                        }
                        break;
                    case LATransaction.TRAN_STATUS_FINISHED:
                        if (issuedDate == null) {
                            issuedDate = EthiopianCalendar.ToEth(f.syscreatedate).toString() + " EC";
                        }
                        break;

                }
            }
            if (approval == null) {
                approval = "Not Approved";
            }
            if (checker == null) {
                checker = "Not Checked";
            }
            if (issuedDate == null) {
                issuedDate = "";
            }
        }
    }

    public List<String> nameList() {
        List<String> ret = new ArrayList<String>();
        for (LADM.Right r : holding.getHolders()) {
            ret.add(r.party.getFullName());
        }
        return ret;
    }
    
    public List<String> tenantNameList(){
        List<String> ret = new ArrayList<>();
        for(LADM.Right r : parcel.getTenants())
            ret.add(r.party.getFullName()); 
        return ret;
    }
}
