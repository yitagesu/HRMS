/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.tran;

import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.tran.Applicant;
import com.intaps.nrlais.model.tran.InheritanceTransactionData;
import com.intaps.nrlais.model.tran.ParcelTransfer;
import com.intaps.nrlais.model.tran.Partition;
import com.intaps.nrlais.model.tran.PartyItem;
import com.intaps.nrlais.model.tran.PartyParcelShare;
import com.intaps.nrlais.util.GSONUtil;
import com.intaps.nrlais.util.INTAPSLangUtils;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Tewelde
 */
public class InheritanceViewController extends HoldingTransactionViewController<InheritanceTransactionData> {


    public InheritanceViewController(HttpServletRequest request, HttpServletResponse response,Class c,int tranType,  boolean loadData) throws Exception {
        super(request, response, c,tranType, loadData);
        
        if (loadData) {
            if (data.holdingUID == null) {
                holding = new LADM.Holding();
            } else {
                if (tran.status == LATransaction.TRAN_STATUS_ACCEPTED || tran.status == LATransaction.TRAN_STATUS_FINISHED) {
                    holding = mainFacade.getFromHistoryByArchiveTx(tran.transactionUID, data.holdingUID).holding;
                } else {
                    holding = mainFacade.getLADM("nrlais_inventory", data.holdingUID).holding;
                }

            }
            kebele = mainFacade.getKebele(holding.nrlais_kebeleid);
            woreda = mainFacade.getWoreda(holding.nrlais_woredaid);
        }
    }
    public boolean isWill(){
        
        String tranType = request.getParameter("withwill");
        if(StringUtils.isEmpty(tranType)){
            return this.data.withWill;
        }
        return tranType.equals("true");
    }
    public List<PartyItem> beneficiaries() {
        return this.data.beneficiaries;
    }

    public Partition partition(String parcelUID) {
        for (Partition partition : this.data.partitions) {
            if (partition.parcelUID.equals(parcelUID)) {
                return partition;
            }
        }
        return null;
    }

    public PartyParcelShare share(Partition partition, String partyUID) {
        if (partition != null) {
            for (PartyParcelShare sh : partition.shares) {
                if (sh.partyUID.equals(partyUID)) {
                    return sh;
                }
            }
        }
        return null;
    }

    public String holdingJson() {
        return GSONUtil.toJson(this.holding);
    }

    public boolean showCerticateLink(String oldParcelUID, String partyUID) {

        return (tran.status == LATransaction.TRAN_STATUS_ACCEPTED || tran.status == LATransaction.TRAN_STATUS_FINISHED)
                && this.data.getPartyPartition(oldParcelUID, partyUID) != null;
    }

    public String showCertificateLink(String oldParcelUID, String partyUID) throws SQLException, IOException {
        String newParcelUID = this.data.getPartyPartition(oldParcelUID, partyUID).parcelUID;
        String holdingUID = null;
        LADM.Parcel l = mainFacade.getHistoryParcelBySourceTx(this.tran.transactionUID, newParcelUID);
        if (l.rights.size() == 0) {
            return "";
        }
        return "javascript:transcation_showCertiricate('" + tran.transactionUID + "','" + l.rights.get(0).holdingUID + "','" + newParcelUID + "')";
    }

    public String holdingLink() {
        String ret = "/holding/holding_information_viewer_page.jsp?holdingUID=" + this.holding.holdingUID;
        if (tran.isCommited()) {
            ret += "&tran_uid=" + tran.transactionUID;
        }
        return ret;
    }

    public String letterOfAttroneyLink(Applicant ap) {
        if (ap == null) {
            return "";
        }
        return showDocumentLink(ap.letterOfAttorneyDocument);
    }

    public String letterOfAttroneyText(Applicant ap) {
        if (ap == null) {
            return "";
        }

        if (ap.letterOfAttorneyDocument == null) {
            return "";
        }
        return ap.letterOfAttorneyDocument.refText;
    }

    public Applicant partyApplicant(String partyUID) {
        for (Applicant ap : this.data.holderBeneficiaries) {
            if (partyUID.equals(ap.selfPartyUID)) {
                return ap;
            }
        }
        return null;
    }

    public boolean isSplit(PartyParcelShare sh) {
        if (sh == null) {
            return false;
        }
        return sh.splitGeom;
    }

    public String shareText(PartyParcelShare sh) {
        if (sh == null) {
            return "";
        }
        return sh.share.toString();
    }
}
