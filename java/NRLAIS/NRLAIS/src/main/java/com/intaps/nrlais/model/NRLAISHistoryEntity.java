/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.model;

/**
 *
 * @author Tewelde
 */
public class NRLAISHistoryEntity {
    public String id;
    public String csaregionid;
    public String nrlais_zoneid;
    public String nrlais_woredaid;
    public String nrlais_kebeleid;
    public String sourcetx;
    public String archivetx;
    public long archiveDate;
    public NRLAISTransactionEntity data;
}
