/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.util;

import java.io.IOException;
import java.util.Properties;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Tewelde
 */
public class Translations {
    
    String lang;
    Properties table;
    public Translations(String lang) throws IOException
    {
        this.lang=lang;
        table=ResourceUtils.getLangFile(lang);
    }
    public String getText(String textKey,String ... args) {
        String text;
        if(table.containsKey(textKey))
            text=table.getProperty(textKey);
        else
            text=textKey;
        if(args.length==0)
            return text;
        text=StringUtils.replaceEach(text,new String[]{"/{","/}"},new String[]{"{","}"});
        String[] indexList=new String[args.length];
       
        for(int i=0;i<indexList.length;i++)
            indexList[i]="{"+i+"}";
        return StringUtils.replaceEach(text, indexList, args);
    }    

    public String lang() {
        return lang;
    }
}
