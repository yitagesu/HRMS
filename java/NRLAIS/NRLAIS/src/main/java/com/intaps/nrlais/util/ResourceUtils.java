/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.util;

import com.intaps.nrlais.UserSession;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

/**
 *
 * @author Tewelde
 */
public class ResourceUtils {

    public static String getSql(String fileName) throws IOException {
        File f = new File("res/file/sql/" + fileName);
        if (!f.exists()) {
            throw new IllegalArgumentException(fileName + " doesn't exits");
        }
        String sql = org.apache.commons.io.IOUtils.toString(f.toURI(), "UTF-8");
        return sql;
    }

    public static File getSqlFile(String fileName) throws IOException {
        File f = new File("res/file/sql/" + fileName);
        if (!f.exists()) {
            throw new IllegalArgumentException(fileName + " doesn't exits");
        }
        return f;
    }

    public static NamedPreparedStatement getPreparedStatementFromResource(UserSession session, Connection con, String fileName) throws IOException, SQLException {

        return new NamedPreparedStatement(session,con, getSql(fileName));
    }

    public static String evaluateSQLTemplate(String fileName, Object model) throws FileNotFoundException, IOException {
        return evaluateTemplate(getSqlFile(fileName), model);
    }

    public static String evaluateTemplate(File template, Object model) throws FileNotFoundException, IOException {
        VelocityContext context = new VelocityContext();
        context.put("model", model);

        try (FileInputStream input = new FileInputStream(template);
                InputStreamReader reader = new InputStreamReader(input, "UTF-8")) {
            StringWriter sw = new StringWriter();
            Velocity.evaluate(context, sw, "temp_template", reader);
            return sw.toString();
        }
    }

    public static Properties getLangFile(String lang) throws FileNotFoundException, IOException {
        Properties ret = new Properties();
        File configFile = new File("res/lang/"+lang+".properties");
        if (!configFile.exists()) {
            return new Properties();
        }
        try (InputStream resourceAsStream = new FileInputStream(configFile)) {
            ret.load(resourceAsStream);
        }
        return ret;
    }
}
