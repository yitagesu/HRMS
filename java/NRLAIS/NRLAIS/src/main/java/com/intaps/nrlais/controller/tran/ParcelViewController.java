/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.tran;

import com.intaps.nrlais.Startup;
import com.intaps.nrlais.controller.ViewControllerBase;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.tran.ParcelItem;
import com.intaps.nrlais.model.tran.PartyItem;
import com.intaps.nrlais.util.GSONUtil;
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.net.URLCodec;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Tewelde
 */
public class ParcelViewController extends ViewControllerBase{
    
    public static class Settings
    {
        public String containerEl;
    }
    public Settings setting;
    public ParcelItem parcel;
    boolean editMode=false;
    public ParcelViewController(HttpServletRequest request, HttpServletResponse response) throws IOException, DecoderException, SQLException {
        super(request, response);
        String s = request.getParameter("setting");
        if (StringUtils.isEmpty(s)) {
            this.setting = new ParcelViewController.Settings();
        } else {
            this.setting = GSONUtil.getAdapter(ParcelViewController.Settings.class).fromJson(s);
        }
        if ("POST".equals(request.getMethod())) {
            this.parcel = Startup.readPostedObject(request, ParcelItem.class);
            editMode=true;
        } else {
            String partyUID = request.getParameter("partyUID");
            String schema = request.getParameter("schema");
            if (StringUtils.isEmpty(schema)) {
                schema = "nrlais_transaction";
            }
            if (partyUID != null) {
                parcel = new ParcelItem(super.mainFacade.getParcel(schema, partyUID));
                editMode=true;
            } else {
                parcel = new ParcelItem();
                editMode=false;
            }
        }
    }
    public String saveButtonLabel()
    {
        if(editMode)
            return "Save Parcel";
        return "Add Parcel";
    }
        public String surveyDate()
    {
        if(parcel.parcel==null)
            return LADM.Parcel.formatSurveyDate(new java.util.Date().getTime());
        return LADM.Parcel.formatSurveyDate(parcel.parcel.mreg_surveyDate);
    }
}
