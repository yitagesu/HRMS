/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.model.tran;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.SourceDocument;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author user
 */
public class CertificateReplacementTransactionData extends LATransaction.TransactionData {
    public SourceDocument claimResolution=null;
    public SourceDocument landHoldingCertificateDoc=null;
    public String holdingUID=null;
    public List<Applicant> applicant=new ArrayList<>();
    public List<ParcelTransfer> parcelUID=new ArrayList<>();

}
