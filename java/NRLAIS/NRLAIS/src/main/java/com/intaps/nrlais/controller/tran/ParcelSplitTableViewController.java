/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.tran;

import com.intaps.nrlais.controller.ViewControllerBase;
import com.google.gson.reflect.TypeToken;
import com.intaps.nrlais.Startup;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.tran.Applicant;
import com.intaps.nrlais.model.tran.Partition;
import com.intaps.nrlais.model.tran.PartyItem;
import com.intaps.nrlais.model.tran.PartyParcelShare;
import com.intaps.nrlais.util.GSONUtil;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Tewelde
 */
public class ParcelSplitTableViewController extends ViewControllerBase {

    public String holdingID;
    public LADM.Holding holding;
    Partition[] partitions = null;
  
    public static class Settings {

        public List<PartyItem> additionalParties = null;
        public List<String> holderFilter = null;
    }
    public Settings settings;

    public ParcelSplitTableViewController(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
        super(request, response);
        holdingID = request.getParameter("holding_uid");
        holding = mainFacade.getLADM("nrlais_inventory", holdingID, LADM.CONTENT_FULL).holding;

        if ("POST".equals(request.getMethod())) {
            this.partitions = Startup.readPostedArrayObject(request, new TypeToken<Partition>() {
            }.getType());
        }
        String json = request.getParameter("setting");
        if (json == null) {
            this.settings = new Settings();
        } else {
            this.settings = GSONUtil.getAdapter(ParcelSplitTableViewController.Settings.class).fromJson(json);
        }
    }
    public boolean showSplit()
    {
        return true;
    }
    public String shareValue(LADM.Parcel parcel, LADM.Party party) {
        if (partitions != null) {
            for (Partition p : partitions) {
                if (p.parcelUID.equals(parcel.parcelUID)) {
                    for (PartyParcelShare share : p.shares) {
                        if (share.partyUID.equals(party.partyUID)) {
                            return share.share.toString();
                        }
                    }
                }
            }
        }
        for (LADM.Parcel p : holding.parcels) {
            if (p.parcelUID.equals(parcel.parcelUID)) {
                for (LADM.Right r : p.getHolders()) {
                    if (r.partyUID.equals(party.partyUID)) {
                        return r.share.toString();
                    }
                }
            }
        }
        return "";
    }

    public boolean isSplit(LADM.Parcel parcel) {
        if (partitions != null) {
            for (Partition p : partitions) {
                if (p.parcelUID.equals(parcel.parcelUID)) {
                    boolean split = true;
                    for (PartyParcelShare share : p.shares) {
                        split = share.splitGeom;
                    }
                    return split;
                }
            }
        }
        return true;
    }

    public Iterable<LADM.Party> beneficiaries() {
        ArrayList<LADM.Party> ret = new ArrayList<>();
        for (LADM.Right r : holding.getHolders()) {
            if (settings.holderFilter != null && !settings.holderFilter.contains(r.partyUID)) {
                continue;
            }
            ret.add(r.party);
        }
        if (settings.additionalParties != null) {
            for (PartyItem i : settings.additionalParties) {
                ret.add(i.party);
            }
        }
        return ret;
    }

    public Iterable<LADM.Parcel> parcels() {
        ArrayList<LADM.Parcel> ret = new ArrayList<>();
        ret.addAll(holding.parcels);
        return ret;
    }
}
