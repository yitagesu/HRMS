/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.model;

/**
 *
 * @author Tewelde
 */
public class SourceDocument extends NRLAISEntity{
    public static final int ARCHIVE_TYPE_DIGITAL=1;
    public static final int ARCHIVE_TYPE_PAPER=2;
    public String uid;
    public String txuid;
    public String notes;
    public int sourceType=0;
    public String refText;
    public int archiveType=0;
    public String description;
    public String mimeType;
    public boolean voidStatus=false;
    public byte[] fileImage=null;
    
    
}
