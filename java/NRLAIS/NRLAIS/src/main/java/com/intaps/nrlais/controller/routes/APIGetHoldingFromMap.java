/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.routes;

import com.google.gson.stream.JsonReader;
import com.intaps.nrlais.Startup;
import com.intaps.nrlais.repo.MainFacade;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author user
 */
@WebServlet("/api/holding_from_map")
public class APIGetHoldingFromMap extends RouteBase<String,Object,Object>{

    @Override
    public String createGetResponse(HttpServletRequest req,HttpServletResponse resp) throws Exception {
        String uid = req.getParameter("uid");
        return new MainFacade(Startup.getSessionByRequest(req)).getHoldingIDForParcel("nrlais_inventory", uid);
    }

    @Override
    public Object deserializePostBody(JsonReader reader) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object processPost(HttpServletRequest req, HttpServletResponse resp, Object data) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    
}
