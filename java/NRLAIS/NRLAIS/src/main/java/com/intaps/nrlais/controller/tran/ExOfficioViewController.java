    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.tran;

import com.intaps.nrlais.Startup;
import com.intaps.nrlais.model.IDName;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LADM.Parcel;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.PartyShare;
import com.intaps.nrlais.model.SourceDocument;
import com.intaps.nrlais.model.tran.Applicant;
import com.intaps.nrlais.model.tran.ExOfficioTransactionData;
import com.intaps.nrlais.model.tran.ManualTransactionData;
import com.intaps.nrlais.model.tran.ParcelTransfer;
import com.intaps.nrlais.model.tran.Partition;
import com.intaps.nrlais.model.tran.PartyItem;
import com.intaps.nrlais.model.tran.PartyParcelShare;
import com.intaps.nrlais.repo.GetLADM;
import com.intaps.nrlais.repo.MainFacade;
import com.intaps.nrlais.repo.ManualLRManipulatorFacade;
import com.intaps.nrlais.repo.RepoBase;
import com.intaps.nrlais.repo.TransactionFacade;
import com.intaps.nrlais.util.GSONUtil;
import com.intaps.nrlais.util.INTAPSDateUtils;
import com.intaps.nrlais.util.INTAPSLangUtils;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
/**
 *
 * @author yitagesu
 */
public class ExOfficioViewController extends HoldingTransactionViewController<ExOfficioTransactionData> {
    
    public ExOfficioViewController(HttpServletRequest request, HttpServletResponse response, boolean loadData) throws Exception {
        super(request, response, ExOfficioTransactionData.class, LATransaction.TRAN_TYPE_EX_OFFICIO, loadData);
        
    }
    
    public PartyItem applicant(String partyUID){
        for (PartyItem ap : this.data.applicant) {
            if (partyUID.equals(ap.party.partyUID)) {
                return ap;
            }
        }
        return null;
    }
    public String woredaAdministrationDecisionText(){
        if(this.data.woredaAdministrationDecission==null){
            return "";
        }
        return this.data.woredaAdministrationDecission.refText;
    }
    public String woredaAdministrationDecisionLink(){
        return showDocumentLink(this.data.woredaAdministrationDecission);
    }

   
   
}

