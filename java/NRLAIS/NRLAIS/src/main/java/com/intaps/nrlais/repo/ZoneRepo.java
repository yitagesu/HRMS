/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.repo;

import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.MultiLangString;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.intaps.nrlais.model.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author yitagesu
 */
public class ZoneRepo extends RepoBase{
    public ZoneRepo(UserSession session) {
        super(session);
    }
 
    
    public ZoneInfo get(final Connection con, String zoneid) throws SQLException {
        try (ResultSet rs = con.prepareStatement("Select nrlais_zoneid,csazonenameeng,csazonenameamharic,csazonenametigrinya,csazonenameoromifya from nrlais_sys.t_zones where nrlais_zoneid='" + zoneid + "'").executeQuery()) {
            if (rs.next()) {
                ZoneInfo w = new ZoneInfo();
                w.zoneID = rs.getString("nrlais_zoneid");
                w.name = new MultiLangString(rs.getString("csazonenameeng"), rs.getString("csazonenameamharic"), rs.getString("csazonenameoromifya"),
                        rs.getString("csazonenametigrinya"));
                return w;
            }
            return null;
        }
    }
}
