/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.repo;

import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.MultiLangString;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.intaps.nrlais.model.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author yitagesu
 */
public class RegionRepo extends RepoBase{
    public RegionRepo(UserSession session) {
        super(session);
    }
 
     public RegionInfo get(final Connection con, String regionid) throws SQLException {
        try (ResultSet rs = con.prepareStatement("Select csaregionid,csaregionnameeng,csaregionnameamharic,csaregionnametigrinya,csaregionnameoromifya from nrlais_sys.t_regions where csaregionid='" + regionid + "'").executeQuery()) {
            if (rs.next()) {
                RegionInfo w = new RegionInfo();
                w.regionID = rs.getString("csaregionid");
                w.name = new MultiLangString(rs.getString("csaregionnameeng"), rs.getString("csaregionnameamharic"), rs.getString("csaregionnameoromifya"),
                        rs.getString("csaregionnametigrinya"));
                return w;
            }
            return null;
        }
    }
}
