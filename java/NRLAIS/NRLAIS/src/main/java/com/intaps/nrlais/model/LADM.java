/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.model;

import com.intaps.nrlais.model.map.GeoJSON;
import com.intaps.nrlais.model.tran.PartyItem;
import com.intaps.nrlais.util.INTAPSLangUtils;
import com.intaps.nrlais.util.RationalNum;
import com.vividsolutions.jts.io.ParseException;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Tewelde
 */
public class LADM {

    public LADM(Holding holding) {
        this.holding = holding;
    }

    public LADM() {
    }

    public static class HistHolding extends NRLAISHistoryEntity {

        public String uhid;
        public int holdingtype;
    }

    public static class HistParcel extends NRLAISHistoryEntity {

        public String holdingUID;
        public String upid;
        public String geometry;
    }

    public static class HistParty extends NRLAISHistoryEntity {

        public String name;
        public int partyType;
        public int isOrphan;
        public int disability;
    }

    public static class HistRestriction extends NRLAISHistoryEntity {

        public int restrictionType;
        public long startDate;
        public long endDate;
        public String holdingUID;
        public String partyUID;
    }

    public static class HistRight extends NRLAISHistoryEntity {

        public int rightType;
        public String holdingUID;
        public String partyUID;
    }

    public void generateFreshUID() {
        LADM.Party p;
        this.holding.holdingUID = UUID.randomUUID().toString();
        HashMap<String, String> partyUIDMap = new HashMap<>();
        for (LADM.Parcel parcel : this.holding.parcels) {
            parcel.parcelUID = UUID.randomUUID().toString();
            for (LADM.Right r : parcel.rights) {
                r.rightUID = UUID.randomUUID().toString();
                generateFrashUID(r.party, partyUIDMap);
            }

            for (LADM.Restriction r : parcel.restrictions) {
                r.restrictionUID = UUID.randomUUID().toString();
                generateFrashUID(r.party, partyUIDMap);
            }
        }
    }

    private void generateFrashUID(Party party, HashMap<String, String> partyUIDMap) {
        if (party.partyUID == null) {
            party.partyUID = UUID.randomUUID().toString();
        } else if (partyUIDMap.containsKey(party.partyUID)) {
            party.partyUID = partyUIDMap.get(party.partyUID);
        } else {
            String newuuid = UUID.randomUUID().toString();
            partyUIDMap.put(party.partyUID, newuuid);
            party.partyUID = newuuid;
        }
        for (LADM.GroupPartyMembership m : party.members) {
            generateFrashUID(m.member, partyUIDMap);
        }
        if (party.guardian != null) {
            generateFrashUID(party.guardian, partyUIDMap);
        }
    }

    public static enum ORMDelta {
        Add,
        Update,
        Delete
    }

    public static class Holding extends NRLAISTransactionEntity implements Cloneable {

        public static final int HOLDING_TYPE_PRIVATE = 1;
        public static final int HOLDING_TYPE_STATE = 3;
        public static final int HOLDING_TYPE_COMMUNAL = 2;

        public String holdingUID = null;
        public String uhid = null;
        public int holdingSeqNo = -1;
        public int holdingType = -1;
        public String notes = "";

        public boolean recordEquals(Holding other) {
            return StringUtils.equals(this.uhid, other.uhid)
                    && this.holdingSeqNo == other.holdingSeqNo
                    && this.holdingType == other.holdingType
                    && StringUtils.equals(this.notes, other.notes);
        }
        public List<Parcel> parcels = new ArrayList<Parcel>();

        public List<Right> getHolders() {
            HashMap<String, Right> ret = new HashMap<String, Right>();
            int right = 0;
            for (Parcel p : this.parcels) {
                for (Right r : p.rights) {
                    if (r.rightType == Right.RIGHT_PUR || r.rightType == Right.RIGHT_SUR) {

                        if (right == 0) {
                            right = r.rightType;
                        } else if (right != r.rightType) {
                            throw new IllegalStateException("Invalid hoilding record uid:" + holdingUID + ". State user right and perpetual use right are mixed");
                        }
                        if (!ret.containsKey(r.partyUID)) {
                            ret.put(r.partyUID, r);
                        }
                    }
                }
            }
            return new ArrayList<Right>(ret.values());
        }

        public boolean isFullyOwnedByCouples() {
            int n = 0;
            for (LADM.Right r : this.getHolders()) {
                if (r.party.mreg_familyrole == LADM.Party.FAMILY_ROLE_HUSBAND || r.party.mreg_familyrole == LADM.Party.FAMILY_ROLE_WIFE) {
                    n++;
                } else {
                    return false;
                }
            }
            if (n < 2) {
                return false;
            }
            return true;
        }

        public Holding cloneHolding() throws CloneNotSupportedException {
            Holding ret = (Holding) this.clone();
            ret.holdingUID = null;
            ret.uhid = null;
            ret.holdingSeqNo = -1;
            ret.parcels = new ArrayList<>();
            return ret;
        }

        public GeoJSON.BBox box() throws ParseException {
            GeoJSON gj = new GeoJSON();
            List<GeoJSON.GJFeature> features = new ArrayList<>();
            for (LADM.Parcel p : this.parcels) {
                features.add(new GeoJSON.GJFeature(null, p.geometry, new GeoJSON.GJProperty[0]));
            }
            gj.features = features;
            return gj.box();
        }

        public Party getParty(String existingPartyUID) throws Exception {
            final PartyItem pi = new PartyItem();
            pi.party = null;
            this.forEachParty((p) -> {
                if (p.partyUID.equals(existingPartyUID)) {
                    pi.party = p;
                    return false;
                }
                return true;
            });
            return pi.party;
        }

        public boolean hasSameHolderAs(Holding holding) {
            List<LADM.Right> thisHolder = this.getHolders();
            List<LADM.Right> otherHolder = holding.getHolders();
            if (thisHolder.size() != otherHolder.size()) {
                return false;
            }
            for (LADM.Right thisR : thisHolder) {
                boolean found = false;
                for (LADM.Right otherR : otherHolder) {
                    if (thisR.party.isSamePerson(otherR.party)) {
                        found = true;
                    }
                }
                if (!found) {
                    return false;
                }
            }
            return true;
        }

        public static interface ProcessItem<T> {

            public boolean process(T p) throws Exception;
        }

        public void forEachParty(ProcessItem<LADM.Party> pr) throws Exception {
            for (Parcel p : this.parcels) {
                for (Right r : p.rights) {
                    if (!pr.process(r.party)) {
                        return;
                    }
                }
                for (Restriction r : p.restrictions) {
                    if (!pr.process(r.party)) {
                        return;
                    }
                }
            }
        }

        public void forEachRight(ProcessItem<LADM.Right> pr) throws Exception {
            for (Parcel p : this.parcels) {
                for (Right r : p.rights) {
                    pr.process(r);
                }
            }
        }

        public void forEachRestriction(ProcessItem<LADM.Restriction> pr) throws Exception {
            for (Parcel p : this.parcels) {
                for (Restriction r : p.restrictions) {
                    pr.process(r);
                }
            }
        }

        public boolean isStateLand() {
            int right = 0;
            for (Right r : this.getHolders()) {
                if (r.rightType == Right.RIGHT_PUR || r.rightType == Right.RIGHT_SUR) {
                    if (right == 0) {
                        right = r.rightType;
                    } else if (right != r.rightType) {
                        throw new IllegalStateException("Invalid hoilding record uid:" + holdingUID + ". State user right and perpetual use right are mixed");
                    }
                }
            }
            return right == Right.RIGHT_SUR;
        }

        public boolean hasNoneHoldingRight() {
            for (Parcel p : this.parcels) {
                if (p.hasNoneHoldingRight()) {
                    return true;
                }
            }
            return false;
        }

        public Parcel getParcel(String parcelUID) {
            for (Parcel p : this.parcels) {
                if (p.parcelUID.equals(parcelUID)) {
                    return p;
                }
            }
            return null;
        }
        static final DecimalFormat holdingSeqNoFormat = new DecimalFormat("00000");

        public static String formatHoldingSeqNo(int seqNo) {
            return holdingSeqNoFormat.format(seqNo);
        }

        public void setUHID(String kebele, int seqNo) {
            this.holdingSeqNo = seqNo;
            this.uhid = KebeleInfo.getRegionCode(kebele) + "-"
                    + KebeleInfo.getZoneCode(kebele) + "-"
                    + KebeleInfo.getWoredaCode(kebele) + "-"
                    + KebeleInfo.getKebeleCode(kebele) + "-"
                    + formatHoldingSeqNo(seqNo);
        }

        public void clearPartyUIDs() {
            for (Parcel p : parcels) {
                for (Right r : p.rights) {
                    r.partyUID = null;
                    r.party.clearPartyUIDs();
                }
            }
        }

    }

    public static class GroupPartyMembership {

        public String membershipUID;
        public String memberPartyUID;
        public RationalNum share = new RationalNum();
        public String notes;
        public Party member;

        public boolean recordEquals(GroupPartyMembership other) {
            return this.share.equals(other.share)
                    && StringUtils.equals(this.notes, other.notes);
        }
    }

    public static class PartyContact extends NRLAISTransactionEntity implements Cloneable {

        public static final int CONTACT_TYPE_ADDRESS = 2;
        public static final int CONTACT_TYPE_OTHER = 3;
        public String uid;
        public String notes;
        public String contactData;
        public int contactdataType;
        public String contactDataTypeOther;

        public boolean recordEquals(PartyContact other) {
            return StringUtils.equals(this.notes, other.notes)
                    && StringUtils.equals(this.contactData, other.contactData)
                    && StringUtils.equals(this.contactDataTypeOther, other.contactDataTypeOther);
        }
    }

    public static class PartyRole extends NRLAISTransactionEntity implements Cloneable {

        public String uid;
        public String notes;
        public int roleType;
        public String roleTypeOther;

        public boolean recordEquals(PartyRole other) {
            return StringUtils.equals(this.notes, other.notes)
                    && this.roleType == other.roleType
                    && StringUtils.equals(this.roleTypeOther, other.roleTypeOther);
        }
    }

    public static class Party extends NRLAISTransactionEntity implements Cloneable {

        public static final int RELATION_GUARDIAN = 1;

        public static final int FAMILY_ROLE_FEMALE_HOUSEHOLD_HEAD = 5;
        public static final int FAMILY_ROLE_MALE_HOUSEHOLD_HEAD = 6;
        public static final int FAMILY_ROLE_HUSBAND = 1;
        public static final int FAMILY_ROLE_WIFE = 2;

        public static final int SEX_MALE = 1;
        public static final int SEX_FEMALE = 2;
        public static final int SEX_UNKNOWN = 3;

        public static final int MARITAL_STATUS_MARRIED = 2;
        public static final int MARITAL_STATUS_DIVORCED = 3;
        public static final int MARITAL_STATUS_WIDOWED = 3;

        public static final int PARTY_TYPE_NATURAL_PERSON = 1;
        public static final int PARTY_TYPE_STATE = 3;
        public static final int PARTY_TYPE_GROUP_PARTY = 6;

        public static final int PARTY_ROLE_NONE = 0;
        public static final int PARTY_ROLE_LAND_HOLDER = 1;
        public static final int PARTY_ROLE_TENANT = 6;
        public static final int PARTY_ROLE_RESTRICTOR = 7;
        public static final int PARTY_ROLE_SERVITUDE_BENEFICIARY = 8;

        public static final int ORG_TYPE_NA = 7;

        static SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");

        public static String formatBirthDate(long time) {
            return df.format(new java.util.Date(time));
        }

        public String partyUID = null;
        public String notes = null;
        public int partyType = -1;
        public String name1 = null;
        public String name2 = null;
        public String name3 = null;
        public int sex = -1;
        public long dateOfBirth;
        public byte[] photo;
        public String idText;
        public int idType;
        public int maritalstatus;
        public int isorphan = 0;
        public int disability = 0;
        public int mreg_familyrole = -1;
        public int orgatype = -1;
        public Party guardian = null;
        public List<PartyContact> contacts = new ArrayList<PartyContact>();
        public List<PartyRole> roles = new ArrayList<PartyRole>();
        public List<GroupPartyMembership> members = new ArrayList<GroupPartyMembership>();

        public boolean recordEquals(Party other) {
            return StringUtils.equals(this.notes, other.notes)
                    && this.partyType == other.partyType
                    && StringUtils.equals(this.name1, other.name1)
                    && StringUtils.equals(this.name2, other.name2)
                    && StringUtils.equals(this.name3, other.name3)
                    && this.sex == other.sex
                    && this.dateOfBirth == other.dateOfBirth
                    && ArrayUtils.isEquals(this.photo, other.photo)
                    && StringUtils.equals(this.idText, other.idText)
                    && this.idType == other.idType
                    && this.maritalstatus == other.maritalstatus
                    && this.isorphan == other.isorphan
                    && this.disability == other.disability
                    && this.mreg_familyrole == other.mreg_familyrole
                    && this.orgatype == other.orgatype;
        }

        public String getFullName() {
            return name1 + (StringUtils.isEmpty(name2) ? "" : " " + name2) + (StringUtils.isEmpty(name3) ? "" : " " + name3);
        }

        public Party cloneParty() throws CloneNotSupportedException {
            Party ret = (Party) super.clone();
            ret.partyUID = null;
            return ret;
        }

        private void clearPartyUIDs() {
            this.partyUID = null;
            for (GroupPartyMembership m : this.members) {
                m.memberPartyUID = null;
                m.member.clearPartyUIDs();
            }
        }

        public void clearUIDForChilds() {
            for (GroupPartyMembership m : this.members) {
                m.membershipUID = null;
                m.member.partyUID = null;
                m.member.clearPartyUIDs();
            }
            if (this.guardian != null) {
                this.guardian.partyUID = null;
                this.guardian.clearPartyUIDs();
            }
            for (PartyContact contact : this.contacts) {
                contact.uid = null;
            }
            for (PartyRole role : this.roles) {
                role.uid = null;
            }
        }

        public PartyContact contact(int contactType) {
            for (PartyContact c : this.contacts) {
                if (c.contactdataType == contactType) {
                    return c;
                }
            }
            return null;
        }

        private boolean isSamePerson(Party party) {
            if (this.partyType != party.partyType) {
                return false;
            }
            if (!StringUtils.equals(this.name1, party.name1)) {
                return false;
            }
            if (!StringUtils.equals(this.name2, party.name2)) {
                return false;
            }
            if (!StringUtils.equals(this.name3, party.name3)) {
                return false;
            }
            if (this.partyType == PARTY_TYPE_NATURAL_PERSON) {
                if (this.sex != party.sex) {
                    return false;
                }
                if (this.dateOfBirth != party.dateOfBirth) {
                    return false;
                }
                if (this.maritalstatus != party.maritalstatus) {
                    return false;
                }
                if (this.mreg_familyrole != party.mreg_familyrole) {
                    return false;
                }
                if (this.disability != party.disability) {
                    return false;
                }
                if (this.isorphan != party.isorphan) {
                    return false;
                }
                if (this.guardian != null && party.guardian == null) {
                    return false;
                }
                if (this.guardian == null && party.guardian != null) {
                    return false;
                }
                if (this.guardian != null) {
                    if (!this.guardian.isSamePerson(party.guardian)) {
                        return false;
                    }
                }
            } else if (this.partyType == PARTY_TYPE_NATURAL_PERSON) {
                if (this.members.size() != this.members.size()) {
                    return false;
                }
                for (LADM.GroupPartyMembership thisMember : this.members) {
                    boolean found = false;
                    for (LADM.GroupPartyMembership otherMembership : party.members) {
                        if (thisMember.share.equals(otherMembership.share)
                                && thisMember.member.isSamePerson(otherMembership.member)) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        return false;
                    }
                }

            } else {
                if (this.orgatype != party.orgatype) {
                    return false;
                }
            }
            return true;
        }

    }
    public static final double areaPrecision = 0.0001; //1 cm square

    public static boolean areaEquals(double a1, double a2) {
        return Math.abs(a1 - a2) < areaPrecision;
    }
    public static final double moneyPrecision = 0.0001;

    public static boolean moneyEquals(double m1, double m2) {
        return Math.abs(m1 - m2) < moneyPrecision;
    }

    public static class Parcel extends NRLAISTransactionEntity implements Cloneable {

        public final static int STAGE_CERTIFICATE_ISSUED = 13;

        public final static int AC_TYPE_DIVORCE = 4;
        public final static int AC_TYPE_GIFT = 3;
        public final static int AC_TYPE_INHERITANCE = 2;
        public final static int AC_TYPE_EXPROPRIATION = 6;
        public final static int AC_TYPE_EXCHANGE = 8;
        public final static int AC_TYPE_SPLIT = 9;
        public final static int AC_TYPE_OTHER = 9;

        public final static int SOIL_FERTILITY_NA = 5;
        static SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");

        public static String formatSurveyDate(long time) {
            return df.format(new java.util.Date(time));
        }
        public String parcelUID = null;
        public int seqNo = -1;
        public String upid = null;
        public double areaGeom = 0;
        public double areaLegal = 0;
        public double areaSurveyed = 0;
        public String adjudicationID = null;
        public int adjudicatedBy = 0;
        public String geometry = "SRID=20137;" + new org.postgis.MultiPolygon().toString();
        public String referencePoint = "SRID=20137;" + new org.postgis.Point().toString();
        public int landUse = -1;
        public int level;
        public String notes;
        public boolean isPrimary = true;
        public int mreg_teamid = -1;
        public String mreg_mapsheetNo = null;
        public int mreg_stage = STAGE_CERTIFICATE_ISSUED;
        public int mreg_actype;
        public int mreg_acyear;
        public int soilfertilityType = SOIL_FERTILITY_NA;
        public long mreg_surveyDate;

        public void resetGeom() {
            geometry = "SRID=20137;" + new org.postgis.MultiPolygon().toString();
            referencePoint = "SRID=20137;" + new org.postgis.Point().toString();
        }

        public GeoJSON.BBox box() throws ParseException {
            GeoJSON.GJFeature f = new GeoJSON.GJFeature("", this.geometry, new GeoJSON.GJProperty[0]);
            if (f.geometry instanceof GeoJSON.GJMultiPolygon) {
                return ((GeoJSON.GJMultiPolygon) f.geometry).box();
            } else {
                return new GeoJSON.BBox();
            }
        }

        public boolean recordEquals(Parcel other) {
            return this.seqNo == other.seqNo
                    && StringUtils.equals(this.upid, other.upid)
                    && LADM.areaEquals(this.areaGeom, other.areaGeom)
                    && LADM.areaEquals(this.areaLegal, other.areaLegal)
                    && LADM.areaEquals(this.areaSurveyed, other.areaSurveyed)
                    && StringUtils.equals(this.adjudicationID, other.adjudicationID)
                    && this.adjudicatedBy == other.adjudicatedBy
                    && StringUtils.equals(this.geometry, other.geometry)
                    && StringUtils.equals(this.referencePoint, other.referencePoint)
                    && this.landUse == other.landUse
                    && this.level == other.level
                    && StringUtils.equals(this.notes, other.notes)
                    && this.mreg_teamid == other.mreg_teamid
                    && StringUtils.equals(mreg_mapsheetNo, other.mreg_mapsheetNo)
                    && this.mreg_stage == other.mreg_stage
                    && this.mreg_actype == other.mreg_actype
                    && this.mreg_acyear == other.mreg_acyear
                    && this.soilfertilityType == other.soilfertilityType
                    && this.mreg_surveyDate == other.mreg_surveyDate;
        }
        public List<Right> rights = new ArrayList<Right>();
        public List<Restriction> restrictions = new ArrayList<Restriction>();

        public List<Right> getHolders() {
            List<Right> ret = new ArrayList<Right>();
            int right = 0;
            for (Right r : this.rights) {
                if (r.rightType == Right.RIGHT_PUR || r.rightType == Right.RIGHT_SUR) {

                    if (right == 0) {
                        right = r.rightType;
                    } else if (right != r.rightType) {
                        throw new IllegalStateException("Invalid parcelrecord uid:" + this.parcelUID + ". State user right and perpetual use right are mixed");
                    }
                    ret.add(r);
                }
            }
            return ret;
        }

        public List<Right> getTenants() {
            List<Right> ret = new ArrayList<Right>();
            for (Right r : this.rights) {
                if (r.rightType == Right.RIGHT_RENT || r.rightType == Right.RIGHT_LEASE) {
                    ret.add(r);
                }
            }
            return ret;
        }

        public List<Restriction> getServitude() {
            List<Restriction> ret = new ArrayList<Restriction>();
            for (Restriction r : this.restrictions) {
                if (r.restrictionType == Restriction.RESTRICTION_TYPE_IRRIGATION || r.restrictionType == Restriction.RESTRICTION_TYPE_RIGHT_OF_WAY || r.restrictionType == Restriction.RESTRICTION_TYPE_OTHER_SERVITUDE) {
                    ret.add(r);
                }
            }
            return ret;
        }

        public List<Restriction> getRestriction() {
            List<Restriction> ret = new ArrayList<Restriction>();
            for (Restriction r : this.restrictions) {
                if (r.restrictionType == Restriction.RESTRICTION_TYPE_COURT_RESTRAINING_ORDER || r.restrictionType == Restriction.RESTRICTION_TYPE_OTHER_RESTRAINING_ORDER || r.restrictionType == Restriction.RESTRICTION_TYPE_REVENUE_AUTHORITY_RESTRAINING_ORDER || r.restrictionType == Restriction.RESTRICTION_TYPE_OTHER) {
                    ret.add(r);
                }
            }
            return ret;
        }

        public boolean hasNoneHoldingRight() {
            for (Right r : this.rights) {
                if (r.rightType == Right.RIGHT_PUR || r.rightType == Right.RIGHT_SUR) {
                    continue;
                } else {
                    return true;
                }
            }
            return false;
        }

        public Parcel clonedObjectNoRR() throws CloneNotSupportedException {
            Parcel ret = (Parcel) this.clone();
            ret.rights = new ArrayList<Right>();
            ret.restrictions = new ArrayList<Restriction>();
            ret.parcelUID = null;
            ret.upid = null;
            ret.seqNo = -1;
            return ret;
        }
        static final DecimalFormat parcelSeqNoFormat = new DecimalFormat("00000");

        public void setUPID(String kebele, int seqNo) {
            this.seqNo = seqNo;
            this.upid = KebeleInfo.getRegionCodeAlpha(kebele) + "/"
                    + KebeleInfo.getZoneCode(kebele) + "/"
                    + KebeleInfo.getWoredaCode(kebele) + "/"
                    + KebeleInfo.getKebeleCode(kebele) + "/"
                    + parcelSeqNoFormat.format(seqNo);
        }

        public static String formatParcelSeqNo(int seqNo) {
            return parcelSeqNoFormat.format(seqNo);
        }
    }

    public static class Right extends NRLAISTransactionEntity implements Cloneable {

        public final static int RIGHT_PUR = 1;
        public final static int RIGHT_LEASE = 2;
        public final static int RIGHT_RENT = 3;
        public final static int RIGHT_SUR = 4;
        public final static int RIGHT_SHARED_CROPPING = 5;

        public String rightUID;
        public String notes;
        public int rightType;
        public RationalNum share = new RationalNum(0, 1);
        public long startLeaseDate;
        public long endLeaseDate;
        public double leaseAmount;
        public String leaseRef;
        public long startRentDate;
        public long endRentDate;

        public long startRentLeaseDate() {
            if (rightType == RIGHT_RENT) {
                return startRentDate;
            }
            return startLeaseDate;
        }

        public long endRentLeaseDate() {
            if (rightType == RIGHT_RENT) {
                return endRentDate;
            }
            return endLeaseDate;
        }
        public double rentSize;
        public long startSharedCroppingDate;
        public long endCroppingDate;
        public String holdingUID;
        public String partyUID;
        public String parcelUID;

        public boolean recordEquals(Right other) {
            return StringUtils.equals(this.notes, other.notes)
                    && this.rightType == other.rightType
                    && this.share.equals(other.share)
                    && this.startLeaseDate == other.startLeaseDate
                    && this.endLeaseDate == other.endLeaseDate
                    && LADM.moneyEquals(this.leaseAmount, other.leaseAmount)
                    && StringUtils.equals(this.leaseRef, other.leaseRef)
                    && this.startRentDate == other.startLeaseDate
                    && this.endRentDate == other.endLeaseDate
                    && LADM.areaEquals(this.rentSize, other.rentSize)
                    && this.startSharedCroppingDate == other.startSharedCroppingDate
                    && this.endCroppingDate == other.endCroppingDate
                    && StringUtils.equals(this.holdingUID, other.holdingUID)
                    && StringUtils.equals(this.partyUID, other.partyUID);
        }
        public Party party = null;

        public Right clonedObject(boolean cloneParty) throws CloneNotSupportedException {

            Right r = (Right) super.clone();
            r.rightUID = null;
            if (cloneParty) {
                r.party = (Party) r.party.cloneParty();
                r.partyUID = null;
            } else {
                r.party = this.party;
            }
            return r;
        }

        public double rentLeaseAmount() {
            return this.leaseAmount;
        }
    }

    public static class Restriction extends NRLAISTransactionEntity implements Cloneable {

        public static final int RESTRICTION_TYPE_IRRIGATION = 6;
        public static final int RESTRICTION_TYPE_RIGHT_OF_WAY = 3;
        public static final int RESTRICTION_TYPE_OTHER_SERVITUDE = 9;

        public static final int RESTRICTION_TYPE_COURT_RESTRAINING_ORDER = 11;
        public static final int RESTRICTION_TYPE_REVENUE_AUTHORITY_RESTRAINING_ORDER = 12;
        public static final int RESTRICTION_TYPE_OTHER_RESTRAINING_ORDER = 19;

        public static final int RESTRICTION_TYPE_OTHER = 100;
        public String restrictionUID = null;
        public Party party = null;
        public String notes;
        public int restrictionType;
        public String restrictionTypeOther;
        public String holdingUID;
        public String partyUID;
        public boolean hasDate;
        public long startDate;
        public long endDate;
        public String parcelUID;

        public long startRestrictionDate() {
            return startDate;
        }

        public long endRestrictionDate() {
            return endDate;
        }

        public boolean recordEquals(Restriction other) {
            return StringUtils.equals(this.notes, other.notes)
                    && this.restrictionType == other.restrictionType
                    && StringUtils.equals(this.restrictionTypeOther, other.restrictionTypeOther)
                    && StringUtils.equals(this.holdingUID, other.holdingUID)
                    && StringUtils.equals(this.partyUID, other.partyUID)
                    && this.hasDate == other.hasDate
                    && this.startDate == other.startDate
                    && this.endDate == other.endDate;
        }

        public Restriction clonedObject(boolean cloneParty) throws CloneNotSupportedException {
            Restriction r = (Restriction) super.clone();
            r.restrictionUID = null;
            if (cloneParty) {
                r.party = (Party) r.party.cloneParty();
                r.partyUID = null;
            } else {
                r.party = this.party;
            }

            return r;
        }

    }
    public final static int CONTENT_HOLDING = 1;
    public final static int CONTENT_PARCELS = 2;
    public final static int CONTENT_RR = 4;
    public final static int CONTENT_PARTY = 8;

    public final static int CONTENT_PHOTO = 16;
    public final static int CONTENT_GEOM = 32;
    public final static int CONTENT_FULL = CONTENT_HOLDING | CONTENT_PARCELS | CONTENT_RR | CONTENT_PARTY | CONTENT_PHOTO | CONTENT_GEOM;
    public int content = 0;

    public Holding holding = new Holding();

    public List<LandRecordError> getRuleErrors(long time) throws Exception {
        HashMap<Integer, LandRecordError> ret = new HashMap<>();

        //check weather the holding has at least one parcel
        if (this.holding.parcels.size() == 0) {
            ret.put(LandRecordError.LR_ERROR_ZERO_PARCEL, new LandRecordError(LandRecordError.LR_ERROR_ZERO_PARCEL,
                    "There holding has no parcel"));
        }

        //check family role, gender and martial status conflict
        class Counter {

            public int nhusband = 0;
            public int nwife = 0;
        };
        final Counter c = new Counter();
        this.holding.forEachParty((party) -> {

            if (party.mreg_familyrole == Party.FAMILY_ROLE_HUSBAND) {
                c.nhusband++;
            }
            if (party.mreg_familyrole == Party.FAMILY_ROLE_WIFE) {
                c.nwife++;
            }
            if ((party.mreg_familyrole == Party.FAMILY_ROLE_HUSBAND && party.sex != Party.SEX_MALE)
                    || (party.mreg_familyrole == Party.FAMILY_ROLE_WIFE && party.sex != Party.SEX_FEMALE)) {
                ret.put(LandRecordError.LR_ERROR_PARTY_SEX_ROLE_MISMATCH, new LandRecordError(LandRecordError.LR_ERROR_PARTY_SEX_ROLE_MISMATCH, party.getFullName() + "'s family role conflicts with his/her gender"));
            }

            if ((party.mreg_familyrole == Party.FAMILY_ROLE_HUSBAND || party.mreg_familyrole == Party.FAMILY_ROLE_WIFE) && party.maritalstatus != Party.MARITAL_STATUS_MARRIED) {
                ret.put(LandRecordError.LR_ERROR_PARTY_SEX_ROLE_MISMATCH, new LandRecordError(LandRecordError.LR_ERROR_PARTY_SEX_ROLE_MISMATCH, party.getFullName() + "'s family role conflicts with his/her marital status"));
            }
            return true;
        });

        //check if all holding rights are the same
        // and check summation of holding right shares
        //check if there are holdings
        HashSet<String> holdingRights = new HashSet<>();
        int right = 0;
        boolean firstParcel = true;

        for (Parcel p : this.holding.parcels) {
            if (!p.nrlais_kebeleid.equals(this.holding.nrlais_kebeleid)) {
                ret.put(LandRecordError.LR_ERROR_KEBLE_MISMATCH, new LandRecordError(LandRecordError.LR_ERROR_KEBLE_MISMATCH, "It is not allowed to have mixed kebele in a holding"));
            }

            List<Right> parcelHolders = p.getHolders();
            if (!firstParcel) {

                if (holdingRights.size() != parcelHolders.size()) {
                    ret.put(LandRecordError.LR_ERROR_NONE_UNIFORM_HOLDING_RIGHT, new LandRecordError(LandRecordError.LR_ERROR_NONE_UNIFORM_HOLDING_RIGHT, "Parcels in the holding have none-uniform holders"));
                }
                continue;
            }
            RationalNum totalShare = new RationalNum(0, 1);
            double totalRent = 0;
            double totalLease = 0;
            int nlease = 0;
            for (Right r : p.rights) {
                if (r.rightType == Right.RIGHT_PUR || r.rightType == Right.RIGHT_SUR) {
                    if ((this.holding.holdingType == LADM.Holding.HOLDING_TYPE_STATE
                            && r.party.partyType != LADM.Party.PARTY_TYPE_STATE)
                            || (this.holding.holdingType != LADM.Holding.HOLDING_TYPE_STATE
                            && r.party.partyType == LADM.Party.PARTY_TYPE_STATE)) {
                        ret.put(LandRecordError.LR_ERROR_PARTY_HOLDING_TYPE_MISMATCH, new LandRecordError(LandRecordError.LR_ERROR_PARTY_HOLDING_TYPE_MISMATCH, "Party type and holding type don't match."));
                    }

                    totalShare.add(r.share);

                    if (right == 0) {
                        right = r.rightType;
                    } else if (right != r.rightType) {
                        ret.put(LandRecordError.LR_ERROR_SUR_PUR_MIXED, new LandRecordError(LandRecordError.LR_ERROR_SUR_PUR_MIXED, "State and perpetual rights mixed in a single holding"));
                    }
                    if (firstParcel) {
                        holdingRights.add(r.partyUID);
                    } else {
                        if (!holdingRights.contains(r.partyUID)) {
                            ret.put(LandRecordError.LR_ERROR_SUR_PUR_MIXED, new LandRecordError(LandRecordError.LR_ERROR_SUR_PUR_MIXED, "State and perpetual rights mixed in a single holding"));
                        }
                    }
                }
                if (r.rightType == Right.RIGHT_LEASE) {
                    nlease++;
                    totalLease += r.rentSize;
                }
                if (r.rightType == Right.RIGHT_RENT) {
                    totalRent += r.rentSize;
                }
            }
            if (nlease > 1) {
                ret.put(LandRecordError.LR_ERROR_MUTIPLE_LEASE, new LandRecordError(LandRecordError.LR_ERROR_MUTIPLE_LEASE, "Only one lease per parcel is allowed"));
            }
            if (nlease == 1 && Math.abs(totalLease-p.areaGeom) > 0.01d) {
                ret.put(LandRecordError.LR_ERROR_PARTIAL_LEASE, new LandRecordError(LandRecordError.LR_ERROR_PARTIAL_LEASE, "Only one lease per parcel is allowed"));
            }
            if (totalRent > 0.01d+p.areaGeom) {
                ret.put(LandRecordError.LR_ERROR_OVER_SIZE_RENT, new LandRecordError(LandRecordError.LR_ERROR_OVER_SIZE_RENT, "Tolal rent area is larger that parcel size"));
            }            
            if (!totalShare.isOne()) {
                ret.put(LandRecordError.LR_ERROR_SUM_OF_SHARE_NOT_ONE, new LandRecordError(LandRecordError.LR_ERROR_SUM_OF_SHARE_NOT_ONE, "Sum of share of holding is not 100%"));
            }
            firstParcel = false;
        }

        if (holdingRights.isEmpty()) {
            ret.put(LandRecordError.LR_ERROR_NO_HOLDING, new LandRecordError(LandRecordError.LR_ERROR_NO_HOLDING, "No holding registered for the holder"));
        }

        return new ArrayList<LandRecordError>(ret.values());
    }

    public String getRuleErrorsAsString(long time) throws Exception {
        String ret = null;
        for (LandRecordError err : getRuleErrors(time)) {
            ret = INTAPSLangUtils.appendStringList(ret, "\n", err.description);
        }
        return ret;
    }
}
