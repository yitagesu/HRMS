/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.routes.api;

import com.google.gson.stream.JsonWriter;
import com.intaps.nrlais.Startup;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.repo.GetLADM;
import com.intaps.nrlais.repo.MainFacade;
import com.intaps.nrlais.util.GSONUtil;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author yitagesu
 */
@WebServlet("/api/get_tran_holdings")
public class APIGetTransactionHoldings extends APIBase {

    
    static class HoldingsRet extends GSONUtil.JSONRet<List<LADM.Holding>>
    {
        
    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HoldingsRet ret=new HoldingsRet();
        try {
            //String userName=assertLogedInUser(req, resp);
            String tranUID=req.getParameter("tran_uid");
            int contentDepth=Integer.parseInt(req.getParameter("content_depth"));
            ret.res=new MainFacade(Startup.getSessionByRequest(req)).getTransactionHoldings(tranUID,contentDepth);
            ret.res.sort(new Comparator<LADM.Holding>() {
                @Override
                public int compare(LADM.Holding o1, LADM.Holding o2) {  
                    return o1.uhid.compareTo(o2.uhid);
                }
            });
            ret.error=null;
            
        } catch (Exception ex) {
            Logger.getLogger(HoldingsRet.class.getName()).log(Level.SEVERE, null, ex);
            ret.error=ex.getMessage();
            ret.res=null;    
        } 
        try (ServletOutputStream str = resp.getOutputStream()) {
                resp.setContentType("application/json");
                JsonWriter writer=new JsonWriter(new OutputStreamWriter(str,"UTF-8"));
                GSONUtil.getAdapter(HoldingsRet.class).write(writer,ret);
                writer.close();
            }    
    }    
    
}
