/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.model.map;

import com.google.gson.internal.LinkedTreeMap;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Teweldemedhin Aberra
 */
public class GeoJSON {

    public static class BBox {

        public double x1 = 0, y1 = 0, x2 = 0, y2 = 0;

        public double centerX() {
            return (x1 + x2) / 2;
        }

        public double centerY() {
            return (y1 + y2) / 2;
        }

        public double width() {
            return Math.abs(x2-x1);
        }

        public double  height() {

            return Math.abs(y2-y1);        
        }
    }

    static double[] fromJtsPoint(Point p) {
        return new double[]{p.getX(), p.getY()};
    }

    public BBox box() {
        BBox ret = new BBox();
        ret.x1 = 1;
        ret.x2 = -1;
        for (GJFeature f : this.features) {
            if (f.geometry instanceof GJGeometry) {
                ((GJGeometry) f.geometry).processCoordinates((c) -> {
                    if (ret.x1 > ret.x2) {
                        ret.x1 = c[0];
                        ret.x2 = c[0];
                        ret.y1 = c[1];
                        ret.y2 = c[1];
                    } else {
                        ret.x1 = Math.min(c[0], ret.x1);
                        ret.x2 = Math.max(c[0], ret.x2);
                        ret.y1 = Math.min(c[1], ret.y1);
                        ret.y2 = Math.max(c[1], ret.y2);
                    }
                    return true;
                });
            }
        }
        return ret;
    }

    public static interface GJCoordinateProcessor {

        public boolean processCoordinate(double[] cord);
    }

    public static class GJCoordinate {

        public double x, y, z;

        public GJCoordinate() {

        }

        private GJCoordinate(Point p) {
            this.x = p.getX();
            this.y = p.getY();
            this.z = 0;
        }
    }

    public static abstract class GJGeometry {

        public String type;

        public abstract void processCoordinates(GJCoordinateProcessor processor);
    }

    public static class GJLineString extends GJGeometry {

        public double[][] coordinates;

        @Override
        public void processCoordinates(GJCoordinateProcessor processor) {
            for (double[] c : coordinates) {
                if (!processor.processCoordinate(c)) {
                    return;
                }
            }
        }
    }

    public static class GJPolygon extends GJGeometry {

        public double[][][] coordinates;

        public GJPolygon() {
            super.type = "Polygon";
        }

        @Override
        public void processCoordinates(GJCoordinateProcessor processor) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

    }

    public static class GJMultiPolygon extends GJGeometry {

        public double[][][][] coordinates;

        public GJMultiPolygon() {
            super.type = "MultiPolygon";
        }

        public BBox box() {
            BBox ret = new BBox();
            ret.x1 = 1;
            ret.x2 = -1;
            this.processCoordinates((c) -> {
                if (ret.x1 > ret.x2) {
                    ret.x1 = c[0];
                    ret.x2 = c[0];
                    ret.y1 = c[1];
                    ret.y2 = c[1];
                } else {
                    ret.x1 = Math.min(c[0], ret.x1);
                    ret.x2 = Math.max(c[0], ret.x2);
                    ret.y1 = Math.min(c[1], ret.y1);
                    ret.y2 = Math.max(c[1], ret.y2);
                }
                return true;
            });
            return ret;
        }

        public GJMultiPolygon(com.vividsolutions.jts.geom.MultiPolygon mp) {
            this();
            coordinates = new double[mp.getNumGeometries()][][][];
            for (int i = 0; i < coordinates.length; i++) {
                Geometry g = mp.getGeometryN(i);
                if (!(g instanceof com.vividsolutions.jts.geom.Polygon)) {
                    throw new IllegalArgumentException("Geometry not supported by GJMultipolygon");
                }
                com.vividsolutions.jts.geom.Polygon p = (com.vividsolutions.jts.geom.Polygon) g;

                coordinates[i] = new double[1 + p.getNumInteriorRing()][][];

                com.vividsolutions.jts.geom.LineString ls = p.getExteriorRing();
                coordinates[i][0] = new double[ls.getNumPoints()][];
                for (int j = 0; j < coordinates[i][0].length; j++) {
                    coordinates[i][0][j] = fromJtsPoint(ls.getPointN(j));
                }
                for (int k = 1; k < coordinates[i].length; k++) {
                    ls = p.getInteriorRingN(k);
                    coordinates[i][k] = new double[ls.getNumPoints()][];
                    for (int j = 0; j < coordinates[i][k].length; j++) {
                        coordinates[i][k][j] = fromJtsPoint(ls.getPointN(j));
                    }
                }
            }
        }

        @Override
        public void processCoordinates(GJCoordinateProcessor processor) {
            for (double[][][] pol : this.coordinates) {
                for (double[][] ring : pol) {
                    for (double[] c : ring) {
                        if (!processor.processCoordinate(c)) {
                            return;
                        }
                    }
                }
            }
        }
    }

    public static class GJProperty {

        public String name;
        public Object value;

        public GJProperty(String name, Object val) {
            this.name = name;
            this.value = val;
        }
    }

    public static class GJFeature {

        public String type;
        public String id;
        public Object geometry;
        public String geometry_name;
        public LinkedTreeMap<String, Object> properties;

        public GJFeature(String id, String geomWKT, GJProperty[] properties) throws ParseException {
            com.vividsolutions.jts.io.WKTReader r = new WKTReader();
            String[] parts = geomWKT.split(";");
            Geometry geom = r.read(parts[parts.length - 1]);
            if (geom instanceof com.vividsolutions.jts.geom.MultiPolygon) {
                geometry = new GJMultiPolygon((com.vividsolutions.jts.geom.MultiPolygon) geom);
            } else {
                throw new IllegalArgumentException("Unsported conversion to GEOJson from wkt");
            }
            this.properties = new LinkedTreeMap<>();
            for (GJProperty p : properties) {
                this.properties.put(p.name, p.value);
            }
            this.id = id;
            this.type = "Feature";
        }
    }

    public static class GJCRS {

        public static GJCRS createByCRS(String crs) {
            GJCRS ret = new GJCRS();
            ret.type = "name";
            ret.properties = new LinkedTreeMap();
            ret.properties.put("name", "urn:ogc:def:crs:" + crs);
            return ret;
        }
        public String type;
        public LinkedTreeMap<String, Object> properties;
    }
    public String type;//FeatureCollection", 
    public int totalFeatures;//: 102, 
    public List<GJFeature> features;
    public GJCRS crs;

}
