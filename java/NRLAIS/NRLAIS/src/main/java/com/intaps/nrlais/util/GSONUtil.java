/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import java.io.IOException;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Teweldemedhin Aberra
 */
public class GSONUtil {

   
    public static class VoidRet
    {
        public String error;
    }
    public static class EmptyReturn
    {
        
    }
    public static class JSONRet<RetType>
    {
        public String error;
        public RetType res;
    }
    public static class StringRet extends JSONRet<String>
    {

    }
    public static class IntegerRet extends JSONRet<Integer>
    {

    }
    
    
    public static Gson gson=null;
    public static final HashMap<Class,Object> adapters=new HashMap<Class, Object>();
    public static <T> TypeAdapter<T> getAdapter(Class<T> c)
    {
        synchronized(adapters)
        {
            if(adapters.containsKey(c))
                return (TypeAdapter<T>) adapters.get(c);
            TypeAdapter<T> ret= gson.getAdapter(c);
            adapters.put(c,ret);
            return  ret;
        }
    }
    static
    {
        gson=new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
        
    }
    public static String toJson(Object obj)
    {
        synchronized (GSONUtil.gson)
        {
            return gson.toJson(obj);
        }
    }
     
    public static JsonElement parse(JsonReader reader) {
        JsonParser parser=new JsonParser();
        return parser.parse(reader);
    }
    public static JsonElement parse(String json) {
        JsonParser parser=new JsonParser();
        return parser.parse(json);
    }
    public static <T> T fromJson(Class<T> c,String str) throws IOException
    {
        synchronized (GSONUtil.gson)
        {
            return getAdapter(c).fromJson(str);
        }
    }
     
}
