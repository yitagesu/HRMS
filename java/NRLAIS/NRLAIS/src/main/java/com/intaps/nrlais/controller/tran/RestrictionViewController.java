/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.tran;

import com.intaps.nrlais.model.IDNameText;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.SourceDocument;
import com.intaps.nrlais.model.tran.Applicant;
import com.intaps.nrlais.model.tran.PartyItem;
import com.intaps.nrlais.model.tran.ParcelTransfer;
import com.intaps.nrlais.model.tran.RestrictionTransactionData;
import com.intaps.nrlais.util.GSONUtil;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Tewelde
 */
public abstract class RestrictionViewController<T extends RestrictionTransactionData> extends TransactionViewController<T> {

    public LADM.Holding holding;

    public RestrictionViewController(HttpServletRequest request, HttpServletResponse response, Class c, int tranType) throws Exception {
        super(request, response, c, tranType);
    }

    public RestrictionViewController(HttpServletRequest request, HttpServletResponse response, Class c, int tranType, boolean loadData) throws Exception {
        super(request, response, c, tranType, loadData);
        if (loadData) {
            if (data.holdingUID == null) {
                holding = new LADM.Holding();
            } else {
                holding = mainFacade.getLADM("nrlais_inventory", data.holdingUID).holding;
            }
            kebele = mainFacade.getKebele(holding.nrlais_kebeleid);
            woreda = mainFacade.getWoreda(holding.nrlais_woredaid);
        }
    }

    public int restrictionType() {
        return this.data.restrictionType;
    }

    public Applicant partyApplicant(String partyUID) {
        for (Applicant ap : this.data.applicants) {
            if (partyUID.equals(ap.selfPartyUID)) {
                return ap;
            }
        }
        return null;
    }

    public List<PartyItem> thirdParty() {
        return this.data.restrictionParties;
    }

    public String letterOfAttroneyText(Applicant ap) {
        if (ap == null) {
            return "";
        }

        if (ap.letterOfAttorneyDocument == null) {
            return "";
        }
        return ap.letterOfAttorneyDocument.refText;
    }

    public String letterOfAttroneyLink(Applicant ap) {
        if (ap == null) {
            return "";
        }
        return showDocumentLink(ap.letterOfAttorneyDocument);
    }

    public ParcelTransfer restriction(String parcelUID) {
        for (ParcelTransfer t : this.data.restrictedParcels) {
            if (t.parcelUID.equals(parcelUID)) {
                return t;
            }
        }

        return null;
    }

    public List<ParcelTransfer> transfers() {
        return this.data.restrictedParcels;
    }

    public LADM.Parcel restrictedParcel(ParcelTransfer t) {
        return holding.getParcel(t.parcelUID);
    }

    public String documentText(SourceDocument doc) {
        if (doc == null) {
            return "";
        }
        return doc.refText;
    }

    public String landHoldingCertificateText() {
        if (this.data.landHoldingCertificateDoc == null) {
            return "";
        }
        return this.data.landHoldingCertificateDoc.refText;
    }

    public String landHoldingCertificateLink() {
        return showDocumentLink(this.data.landHoldingCertificateDoc);
    }

    public String documentDescription(SourceDocument doc) {
        if (doc == null) {
            return "";
        }
        return doc.description;
    }

    public String holdingJson() {
        return GSONUtil.toJson(this.holding);
    }

    public abstract List<IDNameText> restrictionTypes() throws SQLException;

    public abstract String agreementRef();

    public abstract String agreementDesc();

    public String startDate() {
        return LADM.Party.formatBirthDate(data.startDate);
    }

    public String endDate() {
        return LADM.Party.formatBirthDate(data.endDate);
    }

    public abstract String tranTypeText();

    public static RestrictionViewController createController(HttpServletRequest request, HttpServletResponse response, int type) throws Exception {
        if (type == LATransaction.TRAN_TYPE_RESTRICTIVE_INTEREST) {
            return new RestrictiveInterestViewController(request, response, true);
        } else {
            return new ServitudeViewController(request, response, true);
        }
    }

    public static RestrictionViewController createController(HttpServletRequest request, HttpServletResponse response) throws Exception {
        int tranType = Integer.parseInt(request.getParameter("tran_type"));
        return createController(request, response, tranType);
    }

}
