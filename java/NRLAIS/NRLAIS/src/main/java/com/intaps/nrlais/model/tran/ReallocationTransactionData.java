/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.model.tran;

import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.SourceDocument;
import static com.intaps.nrlais.model.tran.GiftTransactionData.TRANSFER_TYPE_NEW_HOLDING;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tewelde
 */
public class ReallocationTransactionData extends TransferTransactionData{
    public SourceDocument adminDecisionDoc=null;
    public SourceDocument claimResolution=null;
}
