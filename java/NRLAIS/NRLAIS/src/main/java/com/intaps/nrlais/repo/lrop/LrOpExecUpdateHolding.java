/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.repo.lrop;

import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.tran.ManualTransactionData;
import com.intaps.nrlais.repo.LRManipulator;
import com.intaps.nrlais.repo.ManualLRManipulatorFacade;
import java.io.IOException;
import java.sql.Connection;

/**
 *
 * @author Tewelde
 */
public class LrOpExecUpdateHolding implements ManualLRManipulatorFacade.LROPExecutor<ManualTransactionData.LrOpUpdateHoldingInfo> {

    @Override
    public Object doOperation(UserSession session, Connection con, LATransaction tran, ManualTransactionData.LrOpUpdateHoldingInfo op) throws Exception {
        LRManipulator lrm = new LRManipulator(session);
        if (op.holding.holdingUID == null) {
            lrm.createHolding(con, tran, new LADM(op.holding),null);
        } else {
            lrm.updateHolding(con, tran, new LADM(op.holding));
        }
        return op.holding.holdingUID;
    }

}
