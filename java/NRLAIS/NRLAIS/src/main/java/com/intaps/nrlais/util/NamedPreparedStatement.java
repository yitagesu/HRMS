/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.util;

import com.intaps.nrlais.UserSession;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.postgis.MultiPolygon;

/**
 *
 * @author Tewelde
 */
public class NamedPreparedStatement {

    static final Pattern parPattern;

    static {
        parPattern = Pattern.compile("@[_a-zA-Z][_a-zA-Z0-9]{0,}");
    }
    PreparedStatement ps;

static class MapEntry {

        public int index;
        public int start;
        public int end;

    }
    HashMap<String, ArrayList<MapEntry>> varMap = new HashMap<>();
    PreparedStatement st;
    UserSession session;
    public NamedPreparedStatement(UserSession session,Connection con, String sql) throws SQLException {

        this.session=session;
        Matcher m = parPattern.matcher(sql);
        int index = 1;
        ArrayList<MapEntry> orderedEntry = new ArrayList<>();
        while (m.find()) {

            MapEntry map = new MapEntry();
            map.index = index;
            map.start = m.start();
            map.end = m.end();
            String varName = m.group(0);
            if (varMap.containsKey(varName)) {
                varMap.get(varName).add(map);
            } else {
                ArrayList<MapEntry> inds = new ArrayList<>();
                inds.add(map);
                varMap.put(m.group(0), inds);
            }
            orderedEntry.add(map);
            index++;
        }
        for (int i = orderedEntry.size() - 1; i >= 0; i--) {
            MapEntry map = orderedEntry.get(i);
            sql = sql.substring(0, map.start) + "?" + sql.substring(map.end);
        }

        st = con.prepareStatement(sql);
    }

    public void setObject(String par, Object val) throws SQLException {
        ArrayList<MapEntry> inds = varMap.get(par);
        if (inds == null) {
            throw new IllegalArgumentException(session.translations().getText("Parameter") + par + session.translations().getText("not_found"));
        }
        for (MapEntry i : inds) {
            st.setObject(i.index, val);
        }
    }

    public void setString(String par, String val) throws SQLException {
        ArrayList<MapEntry> inds = varMap.get(par);
        if (inds == null) {
            throw new IllegalArgumentException(session.translations().getText("Parameter") + par + session.translations().getText("not_found"));
        }
        for (MapEntry i : inds) {
            st.setString(i.index, val);
        }
    }

    public void setBoolean(String par, boolean val) throws SQLException {
        ArrayList<MapEntry> inds = varMap.get(par);
        if (inds == null) {
            throw new IllegalArgumentException(session.translations().getText("Parameter") + par + session.translations().getText("not_found"));
        }
        for (MapEntry i : inds) {
            st.setBoolean(i.index, val);
        }
    }

    public void setInt(String par, int val) throws SQLException {
        ArrayList<MapEntry> inds = varMap.get(par);
        if (inds == null) {
            throw new IllegalArgumentException(session.translations().getText("Parameter") + par + session.translations().getText("not_found"));
        }
        for (MapEntry i : inds) {
            st.setInt(i.index, val);
        }
    }

    public void setLong(String par, long val) throws SQLException {
        ArrayList<MapEntry> inds = varMap.get(par);
        if (inds == null) {
            throw new IllegalArgumentException(session.translations().getText("Parameter") + par + session.translations().getText("not_found"));
        }
        for (MapEntry i : inds) {
            st.setLong(i.index, val);
        }
    }

    public void setBytes(String par, byte[] val) throws SQLException {
        ArrayList<MapEntry> inds = varMap.get(par);
        if (inds == null) {
            throw new IllegalArgumentException(session.translations().getText("Parameter") + par + session.translations().getText("not_found"));
        }
        for (MapEntry i : inds) {
            st.setBytes(i.index, val);
        }
    }

    public void setNull(String par, int type) throws SQLException {
        ArrayList<MapEntry> inds = varMap.get(par);
        if (inds == null) {
            throw new IllegalArgumentException(session.translations().getText("Parameter") + par + session.translations().getText("not_found"));
        }
        for (MapEntry i : inds) {
            st.setNull(i.index, type);
        }
    }

    public void setDouble(String par, double val) throws SQLException {
        ArrayList<MapEntry> inds = varMap.get(par);
        if (inds == null) {
            throw new IllegalArgumentException(session.translations().getText("Parameter") + par + session.translations().getText("not_found"));
        }
        for (MapEntry i : inds) {
            st.setDouble(i.index, val);
        }
    }

    public void setDate(String par, Date val) throws SQLException {
        ArrayList<MapEntry> inds = varMap.get(par);
        if (inds == null) {
            throw new IllegalArgumentException(session.translations().getText("Parameter") + par + session.translations().getText("not_found"));
        }
        for (MapEntry i : inds) {
            st.setDate(i.index, val);
        }
    }
    public void setTimestamp(String par, java.sql.Timestamp val) throws SQLException {
        ArrayList<MapEntry> inds = varMap.get(par);
        if (inds == null) {
            throw new IllegalArgumentException(session.translations().getText("Parameter") + par + session.translations().getText("not_found"));
        }
        for (MapEntry i : inds) {
            st.setTimestamp(i.index, val);
        }
}

    
    public PreparedStatement preparedStatement() {
        return st;
    }

}
