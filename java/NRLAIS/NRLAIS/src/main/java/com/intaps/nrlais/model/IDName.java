/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.model;

/**
 *
 * @author Tewelde
 */
public class IDName {
    public int id;
    public MultiLangString name;
    
    public void setName(String en,String am,String om,String ti) {
        this.name = new MultiLangString(en,am,om,ti);
    }
    public IDName()
    {
        
    }
    public IDName(int id,String en,String am,String om,String ti)
    {
        this.id=id;
        this.setName(en, am, om, ti);
    }
     public IDName(int id,String en)
    {
        this.id=id;
        this.setName(en, en,en,en);
    }
     public IDNameText idNameText(String lang)
     {
         switch(lang)
         {
             case "en-us": return new IDNameText(this.id,this.name.textEn);
             case "am-et": return new IDNameText(this.id,this.name.textAm);
             case "om-et": return new IDNameText(this.id,this.name.textOm);
             case "ti-et": return new IDNameText(this.id,this.name.textTi);
             default: return new IDNameText(this.id,this.name.textEn);
         }
     }
}
