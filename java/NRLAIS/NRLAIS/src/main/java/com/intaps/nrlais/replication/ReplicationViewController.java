/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.replication;

import com.intaps.nrlais.Startup;
import com.intaps.nrlais.controller.ViewControllerBase;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Tewelde
 */
public class ReplicationViewController extends ViewControllerBase{
    ReplicationFacade repFacade;
    public ReplicationViewController(HttpServletRequest request, HttpServletResponse response) throws SQLException {
        super(request, response);
        repFacade=new ReplicationFacade(Startup.getSessionByRequest(request));
    }
    public List<WoredaItem> woredas() throws SQLException
    {
        return repFacade.getAllWoredas();
    }
    public String repNo(WoredaItem w)
    {
        return w.replicationNo==-1?"":Integer.toString(w.replicationNo);
    }
    public String repDate(WoredaItem w)
    {
        if(w.lastRepTime<0)
            return "";
        return new java.util.Date(w.lastRepTime).toString();
    }
        public String replicationStatus() throws SQLException
    {
        ReplicationAction action=repFacade.getLastReplicationAction();
        if(action==null)
            return "";
        return action.toString();
    }
    
}
