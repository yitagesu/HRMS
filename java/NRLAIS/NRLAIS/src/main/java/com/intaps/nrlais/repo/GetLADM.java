/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.repo;

import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.KebeleInfo;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LADM.Parcel;
import com.intaps.nrlais.model.MultiLangString;
import com.intaps.nrlais.util.GSONUtil;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author Tewelde
 */
public class GetLADM extends RepoBase {

    String schema = null;
    boolean loadFromHistory = true;

    public GetLADM(UserSession session, String schema) {
        this(session, schema, true);
    }

    public GetLADM(UserSession session, String schema, boolean loadFromHistory) {
        super(session);
        this.schema = schema;
        this.loadFromHistory = loadFromHistory;
    }

    String deletedFilter() {
        if (schema.equals("nrlais_transaction")) {
            return " and editstatus<>'d'";
        }
        return "";
    }

    String deletedFilter(String table) {
        if (schema.equals("nrlais_transaction")) {
            return " and " + table + ".editstatus<>'d'";
        }
        return "";
    }

    private boolean populateHolding(Connection con, String holdinguid, LADM.Holding holding) throws SQLException {

        try (ResultSet rs = con.prepareStatement("Select * from " + schema + ".t_holdings where uid='" + holdinguid + "'" + deletedFilter()).executeQuery()) {
            if (!rs.next()) {
                return false;
            }
            super.populateNRLAISTransactionEntityFields(rs, holding, schema.equals("nrlais_inventory"));
            holding.holdingUID = (rs.getString("uid"));
            holding.holdingSeqNo = (rs.getInt("seqnr"));
            holding.uhid = (rs.getString("uhid"));
            holding.notes = (rs.getString("notes"));
            holding.holdingType = rs.getInt("holdingtype");
            return true;
        }
    }

    private void populateRights(Connection con, String holdinguid, String parcelUID, List<LADM.Right> rights, int contentDepth) throws SQLException {

        try (ResultSet rs = con.prepareStatement("Select * from " + schema + ".t_rights where holdinguid='"
                + holdinguid + "' and parceluid='" + parcelUID + "'" + deletedFilter()).executeQuery()) {
            while (rs.next()) {
                LADM.Right r = new LADM.Right();
                setRightFields(con, r, rs,  contentDepth);
                rights.add(r);
            }
        }
    }

    private void setRightFields(Connection con, LADM.Right r, final ResultSet rs, int contentDepth) throws IllegalStateException, SQLException {
        r.rightUID = (rs.getString("uid"));
        r.notes = (rs.getString("notes"));
        r.rightType = (rs.getInt("righttype"));
        r.share.num = (rs.getInt("sharenumerator"));
        r.share.denum = (rs.getInt("sharedenominator"));
        java.sql.Date d = rs.getDate("startleasedate");
        r.startLeaseDate = (d == null ? 0 : d.getTime());
        d = rs.getDate("endleasedate");
        r.endLeaseDate = (d == null ? 0 : d.getTime());
        r.leaseAmount = (rs.getDouble("leaseamount"));
        r.leaseRef = (rs.getString("leaseref"));
        d = rs.getDate("startrentdate");
        r.startRentDate = (d == null ? 0 : d.getTime());
        d = rs.getDate("endrentdate");
        r.endRentDate = (d == null ? 0 : d.getTime());
        r.rentSize = (rs.getDouble("rentsize"));
        d = rs.getDate("startcroppingdate");
        r.startSharedCroppingDate = (d == null ? 0 : d.getTime());
        d = rs.getDate("endcroppingdate");
        r.endCroppingDate = (d == null ? 0 : d.getTime());
        r.holdingUID = (rs.getString("holdinguid"));
        r.partyUID = (rs.getString("partyuid"));
        r.parcelUID = rs.getString("parceluid");
        super.populateNRLAISTransactionEntityFields(rs, r, schema.equals("nrlais_inventory"));
        if ((contentDepth & LADM.CONTENT_PARTY) == LADM.CONTENT_PARTY) {
            LADM.Party party = new LADM.Party();
            if (!populateParty(con, r.partyUID, party, contentDepth)) {
                throw new IllegalStateException("Database integrity problem-party uid refered from t_rights:" + r.partyUID + " is not valid");
            }
            r.party = (party);
        } else {
            r.party = (null);
        }
    }

    private void populateRestrictions(Connection con, String holdinguid, String parcelUID, List<LADM.Restriction> restrictions, int contentDepth) throws SQLException {

        try (ResultSet rs = con.prepareStatement("Select * from " + schema + ".t_restrictions where holdinguid='"
                + holdinguid + "' and parceluid='" + parcelUID + "'" + deletedFilter()).executeQuery()) {
            while (rs.next()) {
                LADM.Restriction r = new LADM.Restriction();
                setRestrictionFields(con, r, rs, contentDepth);
                restrictions.add(r);
            }
        }
    }

    private void setRestrictionFields(Connection con, LADM.Restriction r, final ResultSet rs, int contentDepth) throws IllegalStateException, SQLException {
        r.restrictionUID = (rs.getString("uid"));
        r.notes = (rs.getString("notes"));
        r.restrictionType = (rs.getInt("restrictiontype"));
        r.restrictionTypeOther = (rs.getString("restrictiontypeother"));
        r.holdingUID = (rs.getString("holdinguid"));
        r.partyUID = (rs.getString("partyuid"));
        
        java.sql.Date d = rs.getDate("startdate");
        if (rs.wasNull()) {
            r.hasDate = (false);
        } else {
            r.startDate = d.getTime();
            r.hasDate = true;
            r.endDate = rs.getDate("enddate").getTime();
        }
        r.parcelUID = rs.getString("parceluid");
        super.populateNRLAISTransactionEntityFields(rs, r, schema.equals("nrlais_inventory"));
        if ((contentDepth & LADM.CONTENT_PARTY) == LADM.CONTENT_PARTY) {
            LADM.Party party = new LADM.Party();
            if (!populateParty(con, r.partyUID, party, contentDepth)) {
                throw new IllegalStateException("Database integrity problem-party uid refered from t_restrictions:" + r.partyUID + " is not valid");
            }
            r.party = (party);
        } else {
            r.party = (null);
        }
    }

    private void populateParcels(Connection con, String holdinguid, List<LADM.Parcel> parcels, int contentDepth) throws SQLException {

        String sql = "Select * from " + schema + ".t_parcels where uid in \n"
                + "(Select wfsid from " + schema + ".fdconnector inner join\n"
                + "" + schema + ".t_sys_fc_holding on \n"
                + " fdconnector.uid=t_sys_fc_holding.fdc_uid\n"
                + " where t_sys_fc_holding.holdinguid='" + holdinguid + "'" + deletedFilter("t_parcels") + ")  order by upid";
        try (ResultSet rs = con.prepareStatement(sql).executeQuery()) {
            while (rs.next()) {
                LADM.Parcel p = new LADM.Parcel();
                populateParcelFields(p, rs, contentDepth);
                parcels.add(p);
            }
        }
    }

    private void populateParcelFields(Parcel p, final ResultSet rs, int contentDepth) throws SQLException {
        p.parcelUID = rs.getString("uid");
        p.seqNo = rs.getInt("seqnr");
        p.upid = rs.getString("upid");
        p.areaGeom = rs.getDouble("areageom");
        p.areaLegal = rs.getDouble("arealegal");
        p.areaSurveyed = rs.getDouble("areasurveyed");
        p.adjudicationID = rs.getString("adjudicationid");
        p.adjudicatedBy = rs.getInt("adjudicatedby");
        if ((contentDepth & LADM.CONTENT_GEOM) == LADM.CONTENT_GEOM) {
            p.geometry = rs.getObject("geometry").toString();
        }
        p.referencePoint = rs.getObject("referencepoint").toString();
        p.landUse = rs.getInt("landuse");
        p.level = rs.getInt("level");
        p.notes = rs.getString("notes");
        p.mreg_teamid = rs.getInt("mreg_teamid");
        p.mreg_mapsheetNo = rs.getString("mreg_mapsheet");

        int intVal = rs.getInt("mreg_stage");
        if (!rs.wasNull()) {
            p.mreg_stage = intVal;
        }

        intVal = rs.getInt("mreg_actype");
        if (!rs.wasNull()) {
            p.mreg_actype = intVal;
        }

        p.mreg_acyear = rs.getInt("mreg_acyear");

        intVal = rs.getInt("soilfertilitytype");
        if (!rs.wasNull()) {
            p.soilfertilityType = intVal;
        }

        java.util.Date d = rs.getDate("mreg_surveydate");
        p.mreg_surveyDate = (d == null ? new java.util.Date() : d).getTime();
        super.populateNRLAISTransactionEntityFields(rs, p, schema.equals("nrlais_inventory"));
    }

    private boolean populateParty(Connection con, String partyuid, LADM.Party party, int contentDepth) throws SQLException {
        try (ResultSet rs = con.prepareStatement("Select * from " + schema + ".t_party where uid='" + partyuid + "'" + deletedFilter()).executeQuery()) {
            if (!rs.next()) {
                return false;
            }
            party.partyUID = (partyuid);
            party.notes = (rs.getString("notes"));
            party.partyType = (rs.getInt("partytype"));
            party.name1 = (rs.getString("name"));
            party.name2 = (rs.getString("fathersname"));
            party.name3 = (rs.getString("grandfathersname"));
            party.sex = (RepoBase.lookupSex(rs.getString("gender")));
            java.util.Date d = rs.getDate("dateofbirth");
            party.dateOfBirth = (d == null ? new java.util.Date() : d).getTime();
            if ((contentDepth & LADM.CONTENT_PHOTO) == LADM.CONTENT_PHOTO) {
                party.photo = (rs.getBytes("photo"));
            }
            party.idText = (rs.getString("extident"));
            party.idType = (rs.getInt("extidenttype"));
            party.maritalstatus = (rs.getInt("maritalstatus"));
            party.isorphan = (rs.getInt("isorphan"));
            party.disability = (rs.getInt("disability"));
            party.mreg_familyrole = (rs.getInt("mreg_familyrole"));
            if (rs.wasNull()) {
                party.mreg_familyrole = -1;
            }
            party.orgatype = (rs.getInt("orgatype"));
            if (rs.wasNull()) {
                party.orgatype = -1;
            }
            super.populateNRLAISTransactionEntityFields(rs, party, schema.equals("nrlais_inventory"));

        }
        try (ResultSet rs = con.prepareStatement("Select * from " + schema + ".t_groupparty where grouppartyuid='" + party.partyUID + "'" + deletedFilter()).executeQuery()) {
            while (rs.next()) {
                LADM.GroupPartyMembership member = new LADM.GroupPartyMembership();
                member.member = new LADM.Party();
                member.membershipUID = rs.getString("uid");
                member.memberPartyUID = rs.getString("memberpartyuid");
                member.share.num = rs.getInt("sharenumerator");
                member.share.denum = rs.getInt("sharedenominator");
                member.notes = rs.getString("notes");
                populateParty(con, member.memberPartyUID, member.member, contentDepth);
                party.members.add(member);
            }
        }

        try (ResultSet rs = con.prepareStatement("Select * from " + schema + ".t_partyrelationship where relationshiptype=" + LADM.Party.RELATION_GUARDIAN + " and frompartyuid='" + party.partyUID + "'" + deletedFilter()).executeQuery()) {
            if (rs.next()) {
                String guardianPartyUID = rs.getString("topartyuid");
                party.guardian = new LADM.Party();
                populateParty(con, guardianPartyUID, party.guardian, contentDepth);
            } else {
                party.guardian = null;
            }
        }

        try (ResultSet rs = con.prepareStatement("Select * from " + schema + ".t_partycontactdata where partyuid='" + party.partyUID + "'" + deletedFilter()).executeQuery()) {
            while (rs.next()) {
                LADM.PartyContact contact = new LADM.PartyContact();
                contact.uid = rs.getString("uid");
                super.populateNRLAISTransactionEntityFields(rs, party, schema.equals("nrlais_inventory"));
                contact.notes = rs.getString("notes");
                contact.contactData = rs.getString("contactdata");
                contact.contactdataType = rs.getInt("contactdatatype");
                contact.contactDataTypeOther = rs.getString("contactdatatypeother");
                party.contacts.add(contact);
            }
        }

        try (ResultSet rs = con.prepareStatement("Select * from " + schema + ".t_partyrole where partyuid='"
                + party.partyUID + "'" + deletedFilter()).executeQuery()) {
            while (rs.next()) {
                LADM.PartyRole contact = new LADM.PartyRole();
                contact.uid = rs.getString("uid");
                super.populateNRLAISTransactionEntityFields(rs, party, schema.equals("nrlais_inventory"));
                contact.notes = rs.getString("notes");
                contact.roleType = rs.getInt("roletype");
                contact.roleTypeOther = rs.getString("roletypeother");
                party.roles.add(contact);
            }
        }
        return true;
    }

    LADM.Party loadPartyFormHistory(Connection con, String txuid, String partyUID, boolean byArchive) throws SQLException, IOException {
        try (ResultSet rs = con.prepareStatement("Select * from nrlais_historic.t_parties where " + (byArchive ? "archivetx" : "sourcetx") + "='" + txuid + "' and inventoryid='" + partyUID + "'").executeQuery()) {
            if (!rs.next()) {
                return null;
            }
            LADM.Party party = GSONUtil.getAdapter(LADM.Party.class).fromJson(rs.getString("party_attr"));
            return party;
        }
    }

    LADM loadDataFromHistory(Connection con, String txuid, String holdinguid, boolean byArchive) throws SQLException, IOException {
        LADM.Holding ret;
        String hisHoldingUID;
        try (ResultSet rs = con.prepareStatement("Select * from nrlais_historic.t_holdings where " + (byArchive ? "archivetx" : "sourcetx") + "='" + txuid + "' and  inventoryid='" + holdinguid + "'").executeQuery()) {
            if (!rs.next()) {
                return null;
            }
            ret = GSONUtil.getAdapter(LADM.Holding.class).fromJson(rs.getString("holding_attr"));
            hisHoldingUID = rs.getString("id");
        }
        ret.parcels = new ArrayList<LADM.Parcel>();
        HashMap<String, LADM.Parcel> parcels = new HashMap<>();
        try (ResultSet rs = con.prepareStatement("Select * from nrlais_historic.t_parcels where holdingid='" + hisHoldingUID + "' order by upid").executeQuery()) {
            while (rs.next()) {
                LADM.Parcel parcel = GSONUtil.getAdapter(LADM.Parcel.class).fromJson(rs.getString("parcels_attr"));
                parcels.put(parcel.parcelUID, parcel);
            }
        }
        HashMap<String, LADM.Party> parties = new HashMap<>();

        try (ResultSet rs = con.prepareStatement("Select * from nrlais_historic.t_rights where holdingid='" + hisHoldingUID + "'").executeQuery()) {
            while (rs.next()) {

                LADM.Right right = GSONUtil.getAdapter(LADM.Right.class).fromJson(rs.getString("right_attr"));
                if (!parties.containsKey(right.partyUID)) {
                    LADM.Party party = loadPartyFormHistory(con, txuid, right.partyUID, byArchive);
                    parties.put(right.partyUID, party);
                }
                right.party = parties.get(right.partyUID);
                if (parcels.containsKey(right.parcelUID)) {
                    parcels.get(right.parcelUID).rights.add(right);
                }
            }
        }
        try (ResultSet rs = con.prepareStatement("Select * from nrlais_historic.t_restrictions where holdingid='" + hisHoldingUID + "'").executeQuery()) {
            while (rs.next()) {
                LADM.Restriction restriction = GSONUtil.getAdapter(LADM.Restriction.class).fromJson(rs.getString("restriction_attr"));
                if (!parties.containsKey(restriction.partyUID)) {
                    LADM.Party party = loadPartyFormHistory(con, txuid, restriction.partyUID, byArchive);
                    parties.put(restriction.partyUID, party);
                }
                restriction.party = parties.get(restriction.partyUID);
                if (parcels.containsKey(restriction.parcelUID)) {
                    parcels.get(restriction.parcelUID).restrictions.add(restriction);
                }
            }
        }
        ret.parcels.addAll(parcels.values());
        ret.parcels.sort(new Comparator<Parcel>() {
            @Override
            public int compare(Parcel o1, Parcel o2) {
                return o1.upid.compareTo(o2.upid);
            }
        });
        return new LADM(ret);
    }

    public LADM get(final Connection con, String holdinguid, int contentDepth) throws SQLException, IOException {

        LADM ret = new LADM();
        if (!populateHolding(con, holdinguid, ret.holding)) {
            return null;
        }
        if ((contentDepth & LADM.CONTENT_PARCELS) == LADM.CONTENT_PARCELS) {
            populateParcels(con, holdinguid, ret.holding.parcels, contentDepth);
        }
        if ((contentDepth & LADM.CONTENT_RR) == LADM.CONTENT_RR) {
            for (LADM.Parcel p : ret.holding.parcels) {
                populateRights(con, holdinguid, p.parcelUID, p.rights, contentDepth);
                populateRestrictions(con, holdinguid, p.parcelUID, p.restrictions, contentDepth);
            }
        }
        ret.content = contentDepth;
        return ret;
    }

    public LADM.Party getParty(final Connection con, String partyuid, int contentDepth) throws SQLException {
        LADM.Party ret = new LADM.Party();
        if (this.populateParty(con, partyuid, ret, contentDepth)) {
            return ret;
        }
        return null;
    }

    String getParcelGeometry(Connection con, String parcelUID) throws SQLException {
        try (ResultSet rs = con.prepareStatement("Select geometry from " + schema + ".t_parcels where uid='" + parcelUID + "'").executeQuery()) {
            if (rs.next()) {
                return rs.getObject(1).toString();
            }
            return null;
        }
    }

    public String getHoldingIDForParcel(Connection con, String parcelUID) throws SQLException {
        try (ResultSet rs = con.prepareStatement("Select holdinguid from " + schema + ".t_sys_fc_holding where fdc_uid=(Select uid from " + schema + ".fdconnector where wfsid='" + parcelUID + "')").executeQuery()) {
            if (rs.next()) {
                return rs.getString(1);
            }
            return null;
        }
    }

    public LADM getByParcel(Connection con, String parcelUID, int contentDepth) throws SQLException, IOException {
        String hid = getHoldingIDForParcel(con, parcelUID);
        if (hid == null) {
            return null;
        }
        return get(con, hid, contentDepth);
    }

    public LADM.Parcel getParcel(Connection con, String parcelUID, int contentDepth) throws SQLException {
        try (ResultSet rs = con.prepareStatement("Select * from " + schema + ".t_parcels where uid='" + parcelUID + "'").executeQuery()) {
            if (rs.next()) {
                LADM.Parcel parcel = new LADM.Parcel();
                this.populateParcelFields(parcel, rs, contentDepth);
                String holdingID = getHoldingIDForParcel(con, parcelUID);
                if ((contentDepth & LADM.CONTENT_RR) == LADM.CONTENT_RR) {
                    this.populateRights(con, holdingID, parcelUID, parcel.rights, contentDepth);
                    this.populateRestrictions(con, holdingID, parcelUID, parcel.restrictions, contentDepth);
                }
                return parcel;
            }
            return null;
        }
    }
    
    
    public KebeleInfo getKeb(Connection con, String kebeleCode) throws SQLException{
        
            try (ResultSet rs = con.prepareStatement("Select * from " + schema + ".t_kebeles where nrlais_kebeleid='" + kebeleCode + "'").executeQuery()) {
            if (rs.next()) {
                KebeleInfo keb = new KebeleInfo();
                keb.kebeleID = rs.getString("nrlais_kebeleid");
                keb.name = new MultiLangString(rs.getString("kebelenameeng"), rs.getString("kebelenameamharic"), rs.getString("kebelenameoromifya"),
                        rs.getString("kebelenametigrinya"));
                keb.geometry = rs.getObject("geometry").toString();
                return keb;
            }
            
        }
        
        
        
        return null;
    }
    
    
    
    
    

    public String getParcelUPID(Connection con, String parcelUID) throws SQLException {
        return getParcel(con, parcelUID, LADM.CONTENT_PARCELS).upid;
    }

    LADM getByTx(Connection con, String txuid, String holdinguid) throws SQLException, IOException {
        try (ResultSet rs = con.prepareStatement("Select uid from nrlais_inventory.t_holdings where sourcetxuid='" + txuid + "' and uid='" + holdinguid + "'").executeQuery()) {
            if (rs.next()) {
                this.schema = "nrlais_inventory";
                return get(con, holdinguid, LADM.CONTENT_FULL);
            }
        }
        return getFromHistoryBySourceTx(con, txuid, holdinguid);
    }

    public LADM.Parcel getHistoryParcelBySourceTx(Connection con, String txuid, String parcelUID) throws SQLException, IOException {

        return getHistoryParcel(con, txuid, parcelUID, false);
    }

    public LADM.Parcel getHistoryParcel(Connection con, String txuid, String parcelUID, boolean archveTx) throws SQLException, IOException {
        LADM.Parcel parcel = null;
        String histHoldingUID = null;
        try (ResultSet rs = con.prepareStatement("Select * from nrlais_historic.t_parcels where " + (archveTx ? "archivetx" : "sourcetx") + "='" + txuid + "' and inventoryid='" + parcelUID + "'").executeQuery()) {
            if (rs.next()) {
                parcel = GSONUtil.getAdapter(LADM.Parcel.class).fromJson(rs.getString("parcels_attr"));
                histHoldingUID = rs.getString("holdingid");
            }
        }
        if (parcel != null) {
            HashMap<String, LADM.Party> parties = new HashMap<>();
            try (ResultSet rs = con.prepareStatement("Select * from nrlais_historic.t_rights where holdingid='" + histHoldingUID + "'").executeQuery()) {
                while (rs.next()) {

                    LADM.Right right = GSONUtil.getAdapter(LADM.Right.class).fromJson(rs.getString("right_attr"));
                    if (!parties.containsKey(right.partyUID)) {
                        LADM.Party party = loadPartyFormHistory(con, txuid, right.partyUID, false);
                        parties.put(right.partyUID, party);
                    }
                    right.party = parties.get(right.partyUID);
                    if (parcel.parcelUID.equals(right.parcelUID)) {
                        parcel.rights.add(right);
                    }
                }
            }
            try (ResultSet rs = con.prepareStatement("Select * from nrlais_historic.t_restrictions where holdingid='" + histHoldingUID + "'").executeQuery()) {
                while (rs.next()) {
                    LADM.Restriction restriction = GSONUtil.getAdapter(LADM.Restriction.class).fromJson(rs.getString("restriction_attr"));
                    if (!parties.containsKey(restriction.partyUID)) {
                        LADM.Party party = loadPartyFormHistory(con, txuid, restriction.partyUID, false);
                        parties.put(restriction.partyUID, party);
                    }
                    restriction.party = parties.get(restriction.partyUID);
                    if (parcel.parcelUID.equals(restriction.parcelUID)) {
                        parcel.restrictions.add(restriction);
                    }
                }
            }
            return parcel;
        }
        this.schema = "nrlais_inventory";
        return getParcel(con, parcelUID, LADM.CONTENT_FULL);
    }

    public LADM getFromHistoryByArchiveTx(final Connection con, String txuid, String holdinguid) throws SQLException, IOException {
        return loadDataFromHistory(con, txuid, holdinguid, true);
    }

    public LADM getFromHistoryBySourceTx(final Connection con, String txuid, String holdinguid) throws SQLException, IOException {
        return loadDataFromHistory(con, txuid, holdinguid, false);
    }

    public List<Parcel> getAdjacentParcels(Connection con, String parcelUID, int contentDepth) throws SQLException {
        String sql = "Select * from " + schema + ".t_parcels where ST_Touches(geometry, "
                + "(Select geometry from " + schema + ".t_parcels where uid='" + parcelUID + "'))";
        List<Parcel> ret = new ArrayList<Parcel>();
        try (ResultSet rs = con.prepareStatement(sql).executeQuery()) {
            while (rs.next()) {
                LADM.Parcel p = new LADM.Parcel();
                populateParcelFields(p, rs, contentDepth);
                ret.add(p);
            }
        }
        return ret;
    }

    LADM getFromHistoryByParcelArchiveTx(Connection con, String txuid, String parcelUID) throws SQLException, IOException {
        String sql="Select h.inventoryid from nrlais_historic.t_holdings h inner join  nrlais_historic.t_parcels p on h.id=p.holdingid where p.archivetx='"+txuid+"' and p.inventoryid='"+parcelUID+"'"; 
        try(ResultSet rs=con.prepareStatement(sql).executeQuery())
        {
            if(rs.next())
            {
                return getFromHistoryByArchiveTx(con, txuid, rs.getString(1));
            }
        }
        return  null;
    }

    public LADM.Right getRight(Connection con,String rightUID) throws SQLException {
        try (ResultSet rs = con.prepareStatement("Select * from " + schema + ".t_rights where uid='"+ rightUID + "'").executeQuery()) {
            if (rs.next()) {
                LADM.Right r = new LADM.Right();
                setRightFields(con, r, rs,  LADM.CONTENT_FULL);
                return r;
            }
            return null;
        }
    }
    public LADM.Restriction getRestriction(Connection con,String restrictionUID) throws SQLException {
        try (ResultSet rs = con.prepareStatement("Select * from " + schema + ".t_restrictions where uid='"+ restrictionUID + "'").executeQuery()) {
            if (rs.next()) {
                LADM.Restriction r = new LADM.Restriction();
                setRestrictionFields(con, r, rs,  LADM.CONTENT_FULL);
                return r;
            }
            return null;
        }
    }

    public List<LADM.Party> getPartiesByTran(Connection con,String tranUID) throws SQLException 
    {  
        List<String> parties=new ArrayList<>();
        try(ResultSet rs=con.prepareStatement("Select uid from "+schema+".t_party where editStatus<>'d' and currenttxuid='"+tranUID+"'").executeQuery())
        {
            while(rs.next())
                parties.add(rs.getString(1));
        }
        List<LADM.Party> ret=new ArrayList<>(0);
        for(String uid:parties)
        {
            ret.add(getParty(con, uid, LADM.CONTENT_FULL));
        }
        return ret;
    }

    List<LADM.Holding> getTransactionHoldings(Connection con, String transactionUID,int contentDepth) throws SQLException, IOException {
        List<String> huids=new ArrayList<>();
        try(ResultSet rs=con.prepareStatement("Select uid from "+schema+".t_holdings where editStatus<>'d' and currenttxuid='"+transactionUID+"'").executeQuery())
        {
            while(rs.next())
                huids.add(rs.getString(1));
        }
        List<LADM.Holding> ret=new ArrayList<>();
        for(String huid:huids)
        {
            ret.add(get(con, huid, contentDepth).holding);
        }
        return ret;
    }
}
