/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.repo.lrop;

import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.tran.ManualTransactionData;
import com.intaps.nrlais.repo.GetLADM;
import com.intaps.nrlais.repo.LRManipulator;
import com.intaps.nrlais.repo.ManualLRManipulatorFacade;
import java.io.IOException;
import java.sql.Connection;

/**
 *
 * @author Tewelde
 */
public class LrOpExecDeleteParty implements ManualLRManipulatorFacade.LROPExecutor<ManualTransactionData.LrOpDeleteParty> {

    @Override
    public Object doOperation(UserSession session, Connection con, LATransaction tran, ManualTransactionData.LrOpDeleteParty op) throws Exception {
        LRManipulator lrm = new LRManipulator(session);
        
        lrm.lrStartTransaction(con, tran, op.holdingUID);
        lrm.lrDeleteParty(con,tran,op.holdingUID,op.partyUID);
        lrm.finishTransaction(con, tran,false);
        return null;
    }

}
