/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller;

import com.intaps.nrlais.Startup;
import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.CodeText;
import com.intaps.nrlais.model.IDName;
import com.intaps.nrlais.model.IDNameText;
import com.intaps.nrlais.model.KebeleInfo;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.SourceDocument;
import com.intaps.nrlais.model.tran.Applicant;
import com.intaps.nrlais.model.tran.PartyItem;
import com.intaps.nrlais.repo.MainFacade;
import com.intaps.nrlais.util.EthiopianCalendar;
import com.intaps.nrlais.util.GSONUtil;
import com.intaps.nrlais.util.INTAPSDateUtils;
import com.intaps.nrlais.util.RationalNum;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Tewelde
 */
public class ViewControllerBase {

    protected HttpServletRequest request;
    protected HttpServletResponse response;
    protected UserSession session;
    protected MainFacade mainFacade;

    public ViewControllerBase(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
        this.session = Startup.getSessionByRequest(request);
        this.mainFacade = new MainFacade(session);
    }

    public int age(long time) {
        return INTAPSDateUtils.calculateAgeEth(time, new Date().getTime());
    }

    public String escapeHtml(String html) {
        return org.apache.commons.lang3.StringEscapeUtils.escapeHtml4(html);
    }

    public String adminSourceText(int code) throws SQLException {
        return lookupText("nrlais_sys.t_cl_adminsourcetype", code);
    }

    public String applicantIDLink(Applicant ap) {
        if (ap == null) {
            return "#";
        }
        return this.showDocumentLink(ap.idDocument);
    }
    public String applicantPOMLink(Applicant ap){
        if(ap==null){
            return "#";
        }
        return this.showDocumentLink(ap.pomDocument);
    }
    public String applicantPOMText(Applicant ap){
        if (ap == null) {
            return "";
        }
        if (ap.pomDocument == null) {
            return "No POM Attached";
        }
        return ap.pomDocument.refText;
    }

    public String applicantIDText(Applicant ap) {
        if (ap == null) {
            return "";
        }
        if (ap.idDocument == null) {
            return "";
        }
        return ap.idDocument.refText;
    }
    

    public String applicantIDTypeText(Applicant ap) throws SQLException {
        if (ap == null) {
            return "";
        }
        if (ap.idDocument == null) {
            return "";
        }
        return this.adminSourceText(ap.idDocument.sourceType);
    }
    public boolean hasSpplicantIDLink(Applicant ap)
    {
        return ap!=null && ap.idDocument!=null && ap.idDocument.fileImage!=null;
    }
    public String applicantIDLink(PartyItem pi) {
        if (pi == null) {
            return "#";
        }
        return this.showDocumentLink(pi.idDoc);
    }
    public String applicantPOMLink(PartyItem pi){
        if(pi==null){
            return "#";
        }
        return this.showDocumentLink(pi.proofOfMaritalStatus);
    }
    public String applicantPOMText(PartyItem pi){
        if (pi == null) {
            return "";
        }
        if (pi.proofOfMaritalStatus == null) {
            return "No POM Attached";
        }
        return pi.proofOfMaritalStatus.refText;
    }

    public String applicantIDText(PartyItem pi) {
        if (pi == null) {
            return "";
        }
        if (pi.idDoc == null) {
            return "No ID Attached";
        }
        return pi.idDoc.refText;
    }
    

    public String applicantIDTypeText(PartyItem pi) throws SQLException {
        if (pi == null) {
            return "";
        }
        if (pi.idDoc == null) {
            return "No ID Attached";
        }
        return this.adminSourceText(pi.idDoc.sourceType);
    }


    public String ethiopianDate(long time) {
        return EthiopianCalendar.ToEth(time).toString();
    }

    public String grigDate(long time) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(time);
        return (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.DATE) + "/" + cal.get(Calendar.YEAR);
    }

    public String lookupText(String table, int code) throws SQLException {
        IDName ret = mainFacade.getLookup(table, code);
        if (ret == null) {
            return "";
        }
        return ret.name.text(session.lang());
    }

    public String roleText(int role) throws SQLException {
        String ret=lookupText("static.family_role", role);
        if(ret==null)
            return "";
        return ret;
    }
    public String restrictionTypeText(int restriction) throws SQLException{
        return lookupText("nrlais_sys.t_cl_restrictiontype", restriction);
    }

    public String sexText(int sex) throws SQLException {
        return lookupText("static.sex", sex);
    }

    public String disablityText(int disablity) throws SQLException {
        return lookupText("nrlais_sys.t_cl_disability", disablity);
    }

    public String holdingTypeText(int holdingType) throws SQLException {
            return lookupText("nrlais_sys.t_cl_holdingtype", holdingType);
    }

    public String isOrphanText(int isOrphan) throws SQLException {
        return lookupText("nrlais_sys.t_cl_orphan", isOrphan);
    }

    public String maritalStatusText(int maritalStatus) throws SQLException {
        return lookupText("nrlais_sys.t_cl_maritalstatus", maritalStatus);
    }

    public String showDocumentLink(SourceDocument doc) {
        if (doc == null) {
            return "#";
        }
        if (StringUtils.isEmpty(doc.uid)) {
            if (ArrayUtils.isEmpty(doc.fileImage)) {
                return "#";
            }
            return "javascript:documentPicker.showAttachment('" + Base64.getEncoder().encodeToString(doc.fileImage) + "','" + doc.mimeType + "')";
        } else {
            return "javascript:showByUID('" + doc.uid + "')";
        }
    }

    public String showDocumentText(SourceDocument doc) {
        if (doc == null) {
            return "";
        }
        return doc.refText;
    }
   
    public String showDocumentDescription(SourceDocument doc) {
        if (doc == null || doc.description==null) {
            return "";
        }
        return doc.description;
    }

    public String json(Object obj) {
        if (obj == null) {
            return "null";
        }
        return GSONUtil.toJson(obj);
    }

    public String mapServerUrl() {
        return Startup.getAppConfig("map-server");
    }

    public List<CodeText> allKebeles() throws SQLException {
        List<CodeText> ret = new ArrayList<>();
        for (KebeleInfo i : mainFacade.getAllKebeles()) {
            ret.add(i.codeText(session.lang()));
        }
        return ret;
    }
    public List<IDNameText> holdingTypeList() throws SQLException {
        List<IDNameText> ret = new ArrayList<>();
        for (IDName name : this.mainFacade.getAllLookup("nrlais_sys.t_cl_holdingtype")) {
            ret.add(name.idNameText(session.lang()));
        }
        return ret;
    }
    public boolean showNewTransactionButton() {
        return this.session.worlaisSession.isOffier();
    }

    public String transactionTypeName(int type) throws SQLException {
        return mainFacade.getLookup("nrlais_sys.t_cl_transactiontype", type).name.text(this.session.lang());
    }

    public String transactionStatusName(int status) throws SQLException {
        return LATransaction.getStatusString(status);
    }

    public String partyTypeText(int partyType) throws SQLException {
        return lookupText("nrlais_sys.t_cl_partytype",partyType);
    }
    public String shareText(RationalNum share)
    {
        if(share==null)
            return "";
        return share.toString();
    }
    public List<IDNameText> meansOfAcquisition() throws SQLException{
        List<IDNameText> ret = new ArrayList<>();
        for (IDName name: this.mainFacade.getAllLookup("nrlais_sys.t_cl_acquisitiontype")){
            ret.add(name.idNameText(session.lang()));
        }
        return ret;
    }
    public List<IDNameText> landUse() throws SQLException{
        List<IDNameText> ret = new ArrayList<>();
        for (IDName name: this.mainFacade.getAllLookup("nrlais_sys.t_cl_landusetype")){
            ret.add(name.idNameText(session.lang()));
        }
        return ret;
    }
    public List<IDNameText> soilFertiltiy() throws SQLException{
         List<IDNameText> ret = new ArrayList<>();
        for (IDName name: this.mainFacade.getAllLookup("nrlais_sys.t_cl_soilfertilitytype")){
            ret.add(name.idNameText(session.lang()));
        }
        return ret;
    }
    public String text(String text,Object ... pars)
    {
        return this.session.text(text,pars);
    }
}
