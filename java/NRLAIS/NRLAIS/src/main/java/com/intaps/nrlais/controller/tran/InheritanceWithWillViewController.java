/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.tran;

import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.tran.InheritanceWithWillTransactionData;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Tewelde
 */
public class InheritanceWithWillViewController extends InheritanceViewController{
    
    public InheritanceWithWillViewController(HttpServletRequest request, HttpServletResponse response, boolean loadData) throws Exception {
        super(request, response, InheritanceWithWillTransactionData.class, LATransaction.TRAN_TYPE_INHERITANCE_WITHWILL, loadData);
    }
    
}
