/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.tran;

import com.intaps.nrlais.model.tran.PartyItem;
import com.google.gson.reflect.TypeToken;
import com.intaps.nrlais.Startup;
import com.intaps.nrlais.controller.ViewControllerBase;
import com.intaps.nrlais.util.GSONUtil;
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.net.URLCodec;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Tewelde
 */
public class PartyListViewController extends ViewControllerBase{
    
    public static class Settings
    {
        public boolean share=false;
        public boolean idDoc=false;
        public String containerEl="";
        public boolean edit=false;
        public boolean delete=false;
        public int partyType=-1;
        public boolean pom=false;
        public boolean representative=false;
        public boolean member=false;
        public boolean guardian=false;
        public boolean select=false;
    }
    public Settings setting;
    PartyItem[] partyItems=null;
    public PartyListViewController(HttpServletRequest request, HttpServletResponse response) throws DecoderException, IOException {
        super(request, response);
        String s=request.getParameter("setting");
        if(StringUtils.isEmpty(s))
            this.setting=new Settings();
        else
            this.setting=GSONUtil.getAdapter(Settings.class).fromJson(s);
        
        if ("POST".equals(request.getMethod())) {
            this.partyItems = Startup.readPostedArrayObject(request, new TypeToken<PartyItem>(){}.getType());
        }
        else
        {
            this.partyItems=new PartyItem[0];
        }

    }
    
    public PartyItem[] items()
    {
        return this.partyItems;
    }
    public String itemName(PartyItem item)
    {
        if(item==null)
            return "";
        return item.party.getFullName();
    }
    public String itemSex(PartyItem item) throws SQLException
    {
        if(item==null)
            return "";
        
        return super.sexText(item.party.sex);
    }
    public String itemAge(PartyItem item) throws SQLException
    {
        if(item==null)
            return "";
        return Integer.toString(super.age(item.party.dateOfBirth));
    }
    public String itemMaritalStatus(PartyItem item) throws SQLException
    {
        if(item==null)
            return "";
        return super.maritalStatusText(item.party.maritalstatus);
    }
    public String itemFamilyRole(PartyItem item) throws SQLException
    {
        if(item==null)
            return "";
        return super.roleText(item.party.mreg_familyrole);
    }
    public String itemGuardian(PartyItem item) throws SQLException
    {
        if(item==null)
            return "";
        if(item.party.guardian==null)
            return "";
        return item.party.guardian.getFullName();
    }
    public String itemShare(PartyItem item) throws SQLException
    {
        if(item==null)
            return "";
        if(item.share==null)
            return "";
        return item.share.toString();
    }
    public String itemIDType(PartyItem item) throws SQLException
    {
        if(item==null)
            return "";
        if(item.idDoc==null)
            return "";
        return super.adminSourceText(item.idDoc.sourceType);
    }
    public String itemIDRef(PartyItem item) throws SQLException
    {
        if(item==null)
            return "";
        if(item.idDoc==null)
            return "";
        return item.idDoc.refText;
    }
}
