/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.repo;

import com.intaps.nrlais.repo.lrop.*;
import com.intaps.nrlais.Startup;
import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.tran.ManualTransactionData;
import com.intaps.nrlais.util.GSONUtil;
import com.intaps.nrlais.util.NamedPreparedStatement;
import com.intaps.nrlais.util.ResourceUtils;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Tewelde
 */
public class ManualLRManipulatorFacade {

    public static interface LROPExecutor<T extends ManualTransactionData.LrOperation> {

        Object doOperation(UserSession session, Connection con, LATransaction tran, T op) throws Exception;
    }

    public LROPExecutor getExecutor(Class opDataType) {
        if (opDataType == ManualTransactionData.LrOpUpdateHoldingInfo.class) {
            return new LrOpExecUpdateHolding();
        }
        if (opDataType == ManualTransactionData.LrOpRemoveHolder.class) {
            return new LrOpExecDeleteHolder();
        }
        if (opDataType == ManualTransactionData.LrOpUpdateHolder.class) {
            return new LrOpExecUpdateHolder();
        }
        if (opDataType == ManualTransactionData.LrOpSplitParcel.class) {
            return new LrOpExecSplitParcel();
        }
        if (opDataType == ManualTransactionData.LrOpUpateParcel.class) {
            return new LrOpExecUpdateParcel();
        }
        if (opDataType == ManualTransactionData.LrOpStartBoundaryCorrection.class) {
            return new LrOpExecStartBoundaryCorrection();
        }
        if (opDataType == ManualTransactionData.LrOpDeleteParcel.class) {
            return new LrOpExecDeleteParcel();
        }
        if (opDataType == ManualTransactionData.LrOpDeleteParty.class) {
            return new LrOpExecDeleteParty();
        }
        if (opDataType == ManualTransactionData.LrOpSavePartyInfo.class) {
            return new LrOpExecSavePartyInfo();
        }
        if (opDataType == ManualTransactionData.LrOpSaveRent.class) {
            return new LrOpExecSaveRent();
        }
        if (opDataType == ManualTransactionData.LrOpDeleteRent.class) {
            return new LrOpExecDeleteRent();
        }
        if (opDataType == ManualTransactionData.LrOpSaveRestriction.class) {
            return new LrOpExecSaveRestriction();
        }
        if (opDataType == ManualTransactionData.LrOpDeleteRestriction.class) {
            return new LrOpExecDeleteRestriction();
        }
        if (opDataType == ManualTransactionData.LrOpRemoveHolding.class) {
            return new LrOpExecDeleteHolding();
        }
        if (opDataType == ManualTransactionData.LrOpTransferParcels.class) {
            return new LrOpExecTransferParcels();
        }
        if (opDataType == ManualTransactionData.LrOpTransferHolders.class) {
            return new LrOpExecTransferHolders();
        }
        return null;
    }
    UserSession session;

    public ManualLRManipulatorFacade(UserSession session) {
        this.session = session;
    }

    public boolean transactionHasOperations(String tranUID) throws SQLException {
        try (Connection con = Startup.getNRLAISConnection()) {
            try (ResultSet rs = con.prepareStatement("Select count(*) from nrlais_transaction.t_transaction_op where txuid='" + tranUID + "'").executeQuery()) {
                if (rs.next()) {
                    return rs.getInt(1) > 0;
                }
                return false;
            }
        }
    }

    public Object executeOpertion(String tranUID, ManualTransactionData.LrOperation op) throws SQLException, Exception {
        Connection con = Startup.getWriteConnection();
        synchronized (con) {
            LROPExecutor exec = getExecutor(op.getClass());
            LATransaction tran = new GetTransaction(session).get(con, tranUID, true);
            if (exec == null) {
                throw new UnsupportedOperationException("Operation executor not implementd for " + op.getClass());
            }
            con.setAutoCommit(false);
            try {
                Object ret = exec.doOperation(session, con, tran, op);
                NamedPreparedStatement ps = ResourceUtils.getPreparedStatementFromResource(session, con, "insert_tran_op.sql");
                ps.setString("@txuid", tranUID);
                ps.setString("@operation", GSONUtil.toJson(op));
                ps.setString("@userName", session.userName);
                ps.setLong("@time", new java.util.Date().getTime());
                ps.setString("@opType", op.getClass().toString());
                ps.preparedStatement().execute();
                try (ResultSet rs = con.prepareStatement("SELECT currval('nrlais_transaction.seq_tran_op')").executeQuery()) {
                    rs.next();
                    op.serialNo = rs.getInt(1);
                }
                con.commit();
                return ret;
            } catch (Exception ex) {
                con.rollback();
                throw ex;
            }
        }
    }
}
