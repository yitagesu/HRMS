/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.routes;

import com.google.gson.JsonElement;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.intaps.nrlais.controller.routes.api.APIBase;
import com.intaps.nrlais.util.GSONUtil;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Tewelde
 */
public abstract class RouteBase<GetResponseType,PostRequestType,PostResponseType> extends APIBase {

    public static class VoidType
    {
        
    }
    static class RespWithError
    {
        public JsonElement res;
        public String error;
    }
    public abstract GetResponseType createGetResponse(HttpServletRequest req,HttpServletResponse resp) throws Exception;
    public abstract PostRequestType deserializePostBody(JsonReader reader) throws Exception;
    public abstract PostResponseType processPost(HttpServletRequest req,HttpServletResponse resp,PostRequestType data) throws Exception;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RespWithError ret=new RespWithError();
        try 
        {
            assertLogedInUser(req, resp);
            GetResponseType data=createGetResponse(req,resp);
            ret.res=GSONUtil.gson.toJsonTree(data);
            ret.error=null;

        } catch (Exception ex) {
            Logger.getLogger(Transaction.class.getName()).log(Level.SEVERE, null, ex);
            ret.error = ex.getMessage();
            ret.res = null;
        }
        try (ServletOutputStream str = resp.getOutputStream()) {
            resp.setContentType("application/json");
            JsonWriter writer = new JsonWriter(new OutputStreamWriter(str, "UTF-8"));
            GSONUtil.getAdapter(RespWithError.class).write(writer, ret);
            writer.close();
        }
    }
   
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        RespWithError ret=new RespWithError();
        try (ServletInputStream ins = req.getInputStream();
                JsonReader reader = new JsonReader(new InputStreamReader(ins, "UTF-8"));) {
            try {
                assertLogedInUser(req, resp);
                PostRequestType data = deserializePostBody(reader);
                ret.res=GSONUtil.gson.toJsonTree(this.processPost(req,resp, data));
                ret.error=null;
            } catch (Exception ex) {
                Logger.getLogger(Transaction.class.getName()).log(Level.SEVERE, null, ex);
                ret.error = "Error: " + ex.getMessage();
                ret.res = null;
            }
        }
        try (ServletOutputStream str = resp.getOutputStream()) {
            resp.setContentType("application/json");
            JsonWriter writer = new JsonWriter(new OutputStreamWriter(str, "UTF-8"));
            GSONUtil.getAdapter(RespWithError.class).write(writer, ret);
            writer.close();
        }
    }
}
