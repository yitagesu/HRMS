/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.routes.api;

import com.google.gson.stream.JsonWriter;
import com.intaps.nrlais.Startup;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.repo.MainFacade;
import com.intaps.nrlais.util.GSONUtil;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author user
 */
@WebServlet("/api/get_parties")
public class APIGetParty extends APIBase {
    
    static class PartyRet extends GSONUtil.JSONRet<LADM.Party>
    {
        
    }
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PartyRet pr = new PartyRet();
        try {
            //String userName=assertLogedInUser(req, resp);
            String schema=req.getParameter("schema");
            String partyUID=req.getParameter("party_uid");            
            LADM.Party data= new MainFacade(Startup.getSessionByRequest(req)).getParty(schema,partyUID);
            pr.res=data;
            pr.error=null;
            
        } catch (Exception ex) {
            Logger.getLogger(APIGetParcel.ParcelRet.class.getName()).log(Level.SEVERE, null, ex);
            pr.error=ex.getMessage();
            pr.res=null;    
        } 
        try (ServletOutputStream str = resp.getOutputStream()) {
                resp.setContentType("application/json");
                JsonWriter writer=new JsonWriter(new OutputStreamWriter(str,"UTF-8"));
                GSONUtil.getAdapter(PartyRet.class).write(writer,pr);
                writer.close();
            }    
    } 
}
