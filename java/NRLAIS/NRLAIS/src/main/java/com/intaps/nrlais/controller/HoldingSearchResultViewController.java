/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller;

import com.intaps.nrlais.Startup;
import com.intaps.nrlais.controller.tran.ApplicantsViewController;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.SearchHoldingPars;
import com.intaps.nrlais.model.map.GeoJSON;
import com.intaps.nrlais.util.GSONUtil;
import com.vividsolutions.jts.io.ParseException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Tewelde
 */
public class HoldingSearchResultViewController extends ViewControllerBase {

    public List<LADM> result = null;
    public String object_id = null;

    public static class Settings {

        public boolean mapLinke = true;
        public boolean pickHolding = true;
        public boolean pickParcel = false;
        public boolean pickParty = false;
        public String containerEl="";
        
    }
    public Settings settings = new Settings();

    public HoldingSearchResultViewController(HttpServletRequest request, HttpServletResponse response) throws IOException, SQLException {
        super(request, response);
        SearchHoldingPars pars = Startup.readPostedObject(request, SearchHoldingPars.class);
        result = mainFacade.searchHolding(pars, 0, 100);
        
        String json = request.getParameter("setting");
        if (json != null) {
            this.settings = GSONUtil.getAdapter(HoldingSearchResultViewController.Settings.class).fromJson(json);
            object_id = "#"+this.settings.containerEl;
        }

    }
    public String parcelMapLink(LADM.Parcel parcel) throws ParseException {
        GeoJSON.BBox box = parcel.box();
        return StringUtils.replaceEach("javascript:mapview_zoomBox($x,$y,$w,$h)",
                new String[]{"$x", "$y", "$w", "$h"}, new String[]{Double.toString(box.centerX()),
                    Double.toString(box.centerY()), Double.toString(box.width()), Double.toString(box.height())});
    }

    public String holdingMapLink(LADM.Holding holding) throws ParseException {
        GeoJSON.BBox box = holding.box();
        return StringUtils.replaceEach("javascript:mapview_zoomBox($x,$y,$w,$h)",
                new String[]{"$x", "$y", "$w", "$h"}, new String[]{Double.toString(box.centerX()),
                    Double.toString(box.centerY()), Double.toString(box.width()), Double.toString(box.height())});
    }

}
