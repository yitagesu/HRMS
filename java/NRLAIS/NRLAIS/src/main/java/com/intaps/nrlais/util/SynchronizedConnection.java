/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.util;

import java.sql.Connection;

/**
 *
 * @author Tewelde
 */
public class SynchronizedConnection implements AutoCloseable {
    Connection connection;
    boolean closable;
    public SynchronizedConnection(Connection con,boolean closable)
    {
        this.connection=con;
        this.closable=closable;
    }
    public Connection con()
    {
        return this.connection;
    }
    @Override
    public void close() throws Exception {
        if(closable)
            connection.close();;
    }
    
}
