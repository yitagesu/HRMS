/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.tran;

import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.SourceDocument;
import com.intaps.nrlais.model.tran.Applicant;
import com.intaps.nrlais.model.tran.ExchangeTransactionData;
import com.intaps.nrlais.model.tran.GiftTransactionData;
import com.intaps.nrlais.model.tran.ParcelTransfer;
import com.intaps.nrlais.model.tran.Partition;
import com.intaps.nrlais.model.tran.PartyItem;
import com.intaps.nrlais.model.tran.PartyParcelShare;
import com.intaps.nrlais.model.tran.ReallocationTransactionData;
import com.intaps.nrlais.model.tran.TransferTransactionData;
import com.intaps.nrlais.util.GSONUtil;
import com.intaps.nrlais.util.INTAPSLangUtils;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Tewelde
 */
public class ExchangeViewController extends TransactionViewController<ExchangeTransactionData> {

    public LADM.Holding holding1;
    public LADM.Holding holding2 = null;

    public ExchangeViewController(HttpServletRequest request, HttpServletResponse response) throws Exception {
        super(request, response, ExchangeTransactionData.class, LATransaction.TRAN_TYPE_EXCHANGE);
    }

    public ExchangeViewController(HttpServletRequest request, HttpServletResponse response, boolean loadData) throws Exception {
        super(request, response, ExchangeTransactionData.class, LATransaction.TRAN_TYPE_EXCHANGE, loadData);
        if (loadData) {
            if (data.holdingUID1 == null) {
                holding1 = new LADM.Holding();
            } else {
                if (tran.isCommited()) {
                    holding1 = mainFacade.getFromHistoryByArchiveTx(this.tran.transactionUID, data.holdingUID1).holding;
                } else {
                    holding1 = mainFacade.getLADM("nrlais_inventory", data.holdingUID1).holding;
                }
            }
            if (data.holdingUID2 == null) {
                holding2 = new LADM.Holding();
            } else {
                if (tran.isCommited()) {
                    this.holding2 = mainFacade.getFromHistoryByArchiveTx(this.tran.transactionUID, data.holdingUID2).holding;
                } else {
                    this.holding2 = mainFacade.getLADM("nrlais_inventory", data.holdingUID2).holding;
                }
            }

            kebele = mainFacade.getKebele(holding1.nrlais_kebeleid);
            woreda = mainFacade.getWoreda(holding1.nrlais_woredaid);
        }
    }

    public Applicant partyApplicant(String partyUID) {
        for (Applicant ap : this.data.applicants1) {
            if (partyUID.equals(ap.selfPartyUID)) {
                return ap;
            }
        }
        for (Applicant ap : this.data.applicants2) {
            if (partyUID.equals(ap.selfPartyUID)) {
                return ap;
            }
        }
        return null;
    }

    public String letterOfAttroneyText(Applicant ap) {
        if (ap == null) {
            return "";
        }

        if (ap.letterOfAttorneyDocument == null) {
            return "";
        }
        return ap.letterOfAttorneyDocument.refText;
    }

    public String letterOfAttroneyLink(Applicant ap) {
        if (ap == null) {
            return "";
        }
        return showDocumentLink(ap.letterOfAttorneyDocument);
    }

    public String documentText(SourceDocument doc) {
        if (doc == null) {
            return "";
        }
        return doc.refText;
    }

    public String holdingJson() {
        return GSONUtil.toJson(this.holding1);
    }

    public LADM.Parcel exchangeParcel(String parcelUID) {
        LADM.Parcel ret = holding1.getParcel(parcelUID);
        if (ret != null) {
            return ret;
        }
        return holding2.getParcel(parcelUID);
    }

    public String claimResolutionText() {
        if (this.data.claimResolutionDocument == null) {
            return "";
        }
        return this.data.claimResolutionDocument.refText;
    }

    public String claimResolutionLink() {
        return showDocumentLink(this.data.claimResolutionDocument);
    }

    public String landHoldingCertifcateText1() {
        if (this.data.landHoldingCertifcateDocument1 == null) {
            return "";
        }
        return this.data.landHoldingCertifcateDocument1.refText;
    }

    public String landHoldingCertifcateLink1() {
        return showDocumentLink(this.data.landHoldingCertifcateDocument1);
    }

    public String landHoldingCertifcateText2() {
        if (this.data.landHoldingCertifcateDocument2 == null) {
            return "";
        }
        return this.data.landHoldingCertifcateDocument2.refText;
    }

    public String landHoldingCertifcateLink2() {
        return showDocumentLink(this.data.landHoldingCertifcateDocument2);
    }

//    @Override
//    public List<CertificateLink> certficateLinks() throws SQLException, IOException {
//        List<TransactionViewController.CertificateLink> ret = new ArrayList<>();
//        ArrayList<String> done = new ArrayList<>();
//        this.data.parcelUID2.stream().filter((p) -> !(p == null)).filter((p) -> !(done.contains(p))).forEachOrdered((p) -> {
//            done.add(p);
//        });
//
//        this.data.parcelUID1.stream().filter((p) -> !(p == null)).filter((p) -> !(done.contains(p))).forEachOrdered((p) -> {
//            done.add(p);
//        });
//
//        for (String p : done) {
//            System.out.println("output");
//            CertificateLink link = new CertificateLink();
//            LADM.Parcel l = mainFacade.getHistoryParcelBySourceTx(this.tran.transactionUID, p);
//            if (l == null || l.rights.size() == 0) {
//                continue;
//            }
//            for (LADM.Right r : l.getHolders()) {
//                link.owner = INTAPSLangUtils.appendStringList(link.owner, ", ", r.party.getFullName());
//            }
//            link.url = "javascript:transcation_showCertiricate('" + tran.transactionUID + "','" + l.rights.get(0).holdingUID + "','" + p + "')";
//            link.upid = l.upid;
//            ret.add(link);
//
//        }
//
//        ret.sort(new Comparator<CertificateLink>() {
//            @Override
//            public int compare(CertificateLink o1, CertificateLink o2) {
//                return o1.owner.compareTo(o2.owner);
//            }
//        });
//        return ret;
//    }

}
