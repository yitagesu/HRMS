/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.model.tran;

import com.intaps.nrlais.model.DocumentTypeInfo;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.SourceDocument;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tewelde
 */
public class GiftTransactionData extends TransferTransactionData {
    public static final int TRANSFER_TYPE_NEW_HOLDING=1;
    public static final int TRANSFER_TYPE_OTHER_HOLDING=2;
    public static final int TRANSFER_TYPE_THIS_HOLDING=3;
    
    public SourceDocument claimResolutionDocument=null;
    public boolean manipulateManually=false;
}
