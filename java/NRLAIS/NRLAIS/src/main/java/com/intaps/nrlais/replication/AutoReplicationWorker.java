/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.replication;

import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.util.GSONUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.List;

/**
 *
 * @author Tewelde
 */
public class AutoReplicationWorker {

    static class ReplicationConfigServer {

        public boolean enable = false;
        public String server = null;
        public int sleepOnError = 10000;
        private long sleepOnNoChange = 20000;
    }

    static class ReplicationConfig {

        public ReplicationConfigServer[] servers = null;
    }
    ReplicationConfig config = null;

    void log(String msg)
    {
        System.out.println("Replication "+ new java.util.Date().toString()+": "+msg);
    }
    public AutoReplicationWorker(UserSession session) {
        log("Reaplication: loading configuration");
        File file = new File("res/file/replication.json");
        if (!file.exists()) {
            log("Couldn't find " + file.getPath());
            return;
        }
        try (FileReader reader = new FileReader(file)) {
            config = GSONUtil.getAdapter(ReplicationConfig.class).fromJson(reader);
        } catch (Exception e) {
            log("Exception loading replicaiton configuraiton");
            e.printStackTrace();
            config = null;
        }
        int n = 0;
        if (config.servers != null) {
            for (ReplicationConfigServer server : config.servers) {
                if (server.enable) {
                    n++;
                    runServer(session, server);
                }
            }
        }
        if (n == 0) {
            log("Replication configuration have no active entry");
        } else {
            log(n + " reaplication configurations loaded");
        }
    }

    void runServer(final UserSession session, ReplicationConfigServer server) {
        try {
            new Thread(() -> {
                try {
                    ReplicationFacade facade = new ReplicationFacade(session);

                    WORLAISUPSClient client = null;
                    while (true) {

                        try {

                            if (client == null) {
                                client = new WORLAISUPSClient(server.server);
                                client.login("admin", "app_m@ster");
                            }
                            int nrep = 0;
                            for (WoredaItem wi : facade.getAllWoredas()) {
                                GSONUtil.IntegerRet ret = client.invokeJSON("/api/replication?wid=" + wi.woredaID, null, GSONUtil.IntegerRet.class);
                                if (!org.apache.commons.lang3.StringUtils.isEmpty(ret.error)) {
                                    throw new Exception("Eror trying to get upstream server replication status for " + server.server + " " + ret.error);
                                }
                                if (ret.res < wi.replicationNo) {
                                    List<ReplicationData> rdata = facade.getReplicationData(wi.woredaID, ret.res, 100);
                                    for (ReplicationData data : rdata) {
                                        ret = client.invokeJSON("/api/replication", data, GSONUtil.IntegerRet.class);
                                        if (!org.apache.commons.lang3.StringUtils.isEmpty(ret.error)) {
                                            throw new Exception("Eror trying to upload to upstream server for " + server.server + " " + ret.error);
                                        }
                                        nrep += data.data.size();
                                    }
                                }
                            }
                            if (nrep == 0) {
                                log("Upstream server " + server.server + " is upto date.");
                                Thread.sleep(server.sleepOnNoChange);
                                facade.logReplicationAction("Uptream upto date", server.server);
                            }
                            facade.logReplicationAction("Uptream updated", server.server);

                        } catch (Exception ex) {
                            facade.logReplicationAction("Automatic replication failed", server.server);
                            log("Replication error: " + server.server);
                            ex.printStackTrace();
                            if (client != null) {

                                try {
                                    client.logout();
                                } catch (Exception exx) {
                                }
                                client = null;
                            }
                            Thread.sleep(server.sleepOnError);
                        }
                    }
                } catch (Exception ex) {
                    log("Fatal error: " + server.server);
                    ex.printStackTrace();
                }
            }).start();
        } catch (Exception ex) {
            log("Thread for " + server.server + " couldn't start");
            ex.printStackTrace();
        }
    }
}
