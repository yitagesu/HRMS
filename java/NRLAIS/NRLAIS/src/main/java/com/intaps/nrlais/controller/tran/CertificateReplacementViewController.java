/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.tran;

import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.tran.Applicant;
import com.intaps.nrlais.model.tran.CertificateReplacementTransactionData;
import com.intaps.nrlais.model.tran.ParcelTransfer;
import com.intaps.nrlais.model.tran.Partition;
import com.intaps.nrlais.model.tran.PartyParcelShare;
import com.intaps.nrlais.util.GSONUtil;
import com.intaps.nrlais.util.INTAPSLangUtils;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author user
 */
public class CertificateReplacementViewController extends TransactionViewController<CertificateReplacementTransactionData> {

    public LADM.Holding holding;

    public CertificateReplacementViewController(HttpServletRequest request, HttpServletResponse response) throws Exception {
        super(request, response, CertificateReplacementTransactionData.class, LATransaction.TRAN_TYPE_REPLACE_CERTIFICATE);
    }

    public CertificateReplacementViewController(HttpServletRequest request, HttpServletResponse response, boolean loadData) throws Exception {
        super(request, response, CertificateReplacementTransactionData.class, LATransaction.TRAN_TYPE_REPLACE_CERTIFICATE, loadData);
        if (loadData) {
            if (data.holdingUID == null) {
                holding = new LADM.Holding();
            } else {
                if (tran.isCommited()) {
                    holding = mainFacade.getFromHistoryByArchiveTx(this.tran.transactionUID, data.holdingUID).holding;
                } else {
                    holding = mainFacade.getLADM("nrlais_inventory", data.holdingUID).holding;
                }
            }
            kebele = mainFacade.getKebele(holding.nrlais_kebeleid);
            woreda = mainFacade.getWoreda(holding.nrlais_woredaid);
        }
    }

    public Applicant partyApplicant(String partyUID) {
        for (Applicant ap : this.data.applicant) {
            if (partyUID.equals(ap.selfPartyUID)) {
                return ap;
            }
        }
        return null;
    }

    public String letterOfAttroneyText(Applicant ap) {
        if (ap == null) {
            return "";
        }

        if (ap.letterOfAttorneyDocument == null) {
            return "";
        }
        return ap.letterOfAttorneyDocument.refText;
    }

    public String letterOfAttroneyLink(Applicant ap) {
        if (ap == null) {
            return "";
        }
        return showDocumentLink(ap.letterOfAttorneyDocument);
    }

    public String claimResolution() {
        if (this.data.claimResolution == null) {
            return "";
        }
        return this.data.claimResolution.refText;
    }

    public String claimResolutionLink() {
        return showDocumentLink(this.data.claimResolution);
    }
    
    public String landHoldingText() {
        if (this.data.landHoldingCertificateDoc == null) {
            return "";
        }
        return this.data.landHoldingCertificateDoc.refText;
    }

    public String landHoldingLink() {
        return showDocumentLink(this.data.landHoldingCertificateDoc);
    }
    public String holdingJson() {
        return GSONUtil.toJson(this.holding);
    }

    public ParcelTransfer transfer(String parcelUID) {
        for (ParcelTransfer t : this.data.parcelUID) {
            if (t.parcelUID.equals(parcelUID)) {
                return t;
            }
        }

        return null;
    }

    public LADM.Parcel transferParcel(ParcelTransfer t) {
        return holding.getParcel(t.parcelUID);
    }

    @Override
    public List<CertificateLink> certficateLinks() throws SQLException, IOException {
        List<TransactionViewController.CertificateLink> ret = new ArrayList<TransactionViewController.CertificateLink>();
        for (ParcelTransfer p : this.data.parcelUID) {
            CertificateLink link = new CertificateLink();
            LADM.Parcel l = mainFacade.getHistoryParcelBySourceTx(this.tran.transactionUID, p.parcelUID);
            if (l == null || l.rights.size() == 0) {
                continue;
            }
            for (LADM.Right r : l.getHolders()) {
                link.owner = INTAPSLangUtils.appendStringList(link.owner, ", ", r.party.getFullName());
            }
            link.url = "javascript:transcation_showCertiricate('" + holding.sourcetxuid + "','" + l.rights.get(0).holdingUID + "','" + p.parcelUID + "')";
            link.upid = l.upid;
            ret.add(link);

        }
        ret.sort(new Comparator<CertificateLink>() {
            @Override
            public int compare(CertificateLink o1, CertificateLink o2) {
                return o1.owner.compareTo(o2.owner);
            }
        });
        return ret;
    }

    @Override
    public ChangeSummary summary() throws SQLException {
        ChangeSummary ret=new ChangeSummary();
        return ret;
    }
    

}
