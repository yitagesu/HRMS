/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.tran;

import com.intaps.nrlais.UserSession;
import java.sql.Connection;
import java.sql.SQLException;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.worlais.WORLAISClient;

/**
 *
 * @author Tewelde
 */
/**
 *
 * @author Tewelde
 */
public interface TransactionHandler {

    public void saveTransaction(UserSession session, Connection con, LATransaction app, boolean register) throws Exception;

    public LATransaction.TransactionData getData(UserSession session, Connection con, String transactionUID) throws SQLException;

    public void filterStateChange(UserSession session, Connection con, String transactionUID, String statChange) throws SQLException;

}
