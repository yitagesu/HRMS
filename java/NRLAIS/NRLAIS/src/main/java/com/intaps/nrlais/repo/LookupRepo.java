/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.repo;

import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.DocumentTypeInfo;
import com.intaps.nrlais.model.IDName;
import com.intaps.nrlais.model.LADM;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Tewelde
 */
public class LookupRepo extends RepoBase {

    public static final String[] lookupTables = {
        "static.sex",
        "static.family_role",
        "nrlais_sys.t_cl_landusetype",
        "nrlais_sys.t_cl_acquisitiontype",
        "nrlais_sys.t_cl_maritalstatus",
        "nrlais_sys.t_cl_organizationtype",
        "static.restrictive_interest",
        "nrlais_sys.t_cl_holdingtype",
        "static.servitude"
    };

    static abstract class StaticLookup extends LookupRepo {

        public StaticLookup(String table) {
            super(table);
        }
        protected List<IDName> items = null;

        protected abstract void loadItems();

        @Override
        public List<IDName> getAll(Connection con) throws SQLException {
            loadItems();
            return items;
        }

        @Override
        public IDName get(Connection con, int id) throws SQLException {
            loadItems();;
            for (IDName n : items) {
                if (n.id == id) {
                    return n;
                }
            }
            return null;
        }
    }
    static LookupRepo sexLookup = new StaticLookup("static.sex") {
        protected void loadItems() {
            if (items == null) {
                items = new ArrayList<IDName>();
                items.add(new IDName(LADM.Party.SEX_MALE, "Male","ወንድ","",""));
                items.add(new IDName(LADM.Party.SEX_FEMALE, "Female", "ሴት","",""));
                items.add(new IDName(LADM.Party.SEX_UNKNOWN, "Unknown","የማይታወቅ","",""));
            }
        }
    };
    static LookupRepo familyRoleLookup = new StaticLookup("static.family_role") {
        protected void loadItems() {
            if (items == null) {
                items = new ArrayList<IDName>();
                items.add(new IDName(LADM.Party.FAMILY_ROLE_HUSBAND, "Husband","ባል","",""));
                items.add(new IDName(LADM.Party.FAMILY_ROLE_WIFE, "Wife","ሚስት","",""));
                items.add(new IDName(LADM.Party.FAMILY_ROLE_MALE_HOUSEHOLD_HEAD, "Independent Male Holder","ወንደላጤ","",""));
                items.add(new IDName(LADM.Party.FAMILY_ROLE_FEMALE_HOUSEHOLD_HEAD, "Independent Female Holder","ሰተላጤ","",""));
            }
        }

    };
    static LookupRepo restrictiveInterestLookup = new StaticLookup("static.restrictive_interest") {
        protected void loadItems() {
            if (items == null) {
                items = new ArrayList<IDName>();
                items.add(new IDName(LADM.Restriction.RESTRICTION_TYPE_COURT_RESTRAINING_ORDER, "Court Restraining Order"));
                items.add(new IDName(LADM.Restriction.RESTRICTION_TYPE_REVENUE_AUTHORITY_RESTRAINING_ORDER, "Revenue Authority Restraining Order"));
                items.add(new IDName(LADM.Restriction.RESTRICTION_TYPE_OTHER_RESTRAINING_ORDER, "Other Restrictions"));
            }
        }
    };
    static LookupRepo servitudeLookup = new StaticLookup("static.servitude") {
        protected void loadItems() {
            if (items == null) {
                items = new ArrayList<IDName>();
                items.add(new IDName(LADM.Restriction.RESTRICTION_TYPE_RIGHT_OF_WAY, "Right of Way"));
                items.add(new IDName(LADM.Restriction.RESTRICTION_TYPE_IRRIGATION, "Irregation Route"));
                items.add(new IDName(LADM.Restriction.RESTRICTION_TYPE_OTHER_SERVITUDE, "Other Servitude/Easement"));
            }
        }
    };
    static HashMap<String, LookupRepo> staticLookups = new HashMap<>();

    static {
        staticLookups.put(sexLookup.table, sexLookup);
        staticLookups.put(familyRoleLookup.table, familyRoleLookup);
        staticLookups.put(restrictiveInterestLookup.table, restrictiveInterestLookup);
        staticLookups.put(servitudeLookup.table, servitudeLookup);
    }

    String table;

    public LookupRepo(String table) {
        super(null);
        this.table = table;
    }

    public List<IDName> getAll(Connection con) throws SQLException {

        return super.loadIDDescriptionRecord(con, table, IDName.class);

    }

    public IDName get(Connection con, int id) throws SQLException {

        return super.getIDName(con, table, IDName.class, id);

    }
}
