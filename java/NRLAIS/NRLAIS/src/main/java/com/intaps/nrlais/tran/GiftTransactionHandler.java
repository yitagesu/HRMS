/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.tran;

import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.DocumentTypeInfo;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LADM.Parcel;
import com.intaps.nrlais.model.SourceDocument;
import com.intaps.nrlais.model.tran.Applicant;
import com.intaps.nrlais.model.tran.GiftTransactionData;
import com.intaps.nrlais.model.tran.ParcelTransfer;
import com.intaps.nrlais.model.tran.PartyItem;
import com.intaps.nrlais.model.tran.TransferTransactionData;
import com.intaps.nrlais.repo.DocumentTypeRepo;
import com.intaps.nrlais.repo.GetLADM;
import com.intaps.nrlais.repo.LRManipulator;
import com.intaps.nrlais.repo.TransactionRepo;
import com.intaps.nrlais.util.EthiopianCalendar;
import com.intaps.nrlais.util.RationalNum;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 *
 * @author Tewelde
 */
public class GiftTransactionHandler extends TransferTransactionHandler implements TransactionHandler {

    @Override
    protected SourceDocument[] sourceDocs(TransferTransactionData data) {
        GiftTransactionData tran=(GiftTransactionData)data;
        return new SourceDocument[]{
            tran.claimResolutionDocument,
            tran.landHoldingCertificateDoc};
        
    }

    @Override
    protected int[] sourceDocTypes(TransferTransactionData data) {
       return new int[]{DocumentTypeInfo.DOC_TYPE_ELDERS_WOREDA_RESOLUTION,DocumentTypeInfo.DOC_TYPE_LAND_HOLDING_CERTIFICATE};
    }

}
