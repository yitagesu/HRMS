    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.tran;

import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.DocumentTypeInfo;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LADM.Parcel;
import com.intaps.nrlais.model.SourceDocument;
import com.intaps.nrlais.model.tran.Applicant;
import com.intaps.nrlais.model.tran.DataMigrationTransactionData;
import com.intaps.nrlais.model.tran.GiftTransactionData;
import com.intaps.nrlais.model.tran.ParcelTransfer;
import com.intaps.nrlais.model.tran.PartyItem;
import com.intaps.nrlais.model.tran.ReallocationTransactionData;
import com.intaps.nrlais.repo.DocumentTypeRepo;
import com.intaps.nrlais.repo.GetLADM;
import com.intaps.nrlais.repo.LRManipulator;
import com.intaps.nrlais.repo.TransactionRepo;
import com.intaps.nrlais.util.EthiopianCalendar;
import com.intaps.nrlais.util.RationalNum;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 *
 * @author Tewelde
 */
public class DataMigrationTransactionHandler extends TransactionHandlerBase implements TransactionHandler {

    /**
     *
     * @param con
     * @param theTransaction
     * @throws SQLException
     */
    //the logic for modifying the land register goes here.
    void registerTransaction(UserSession session, Connection con, LATransaction theTransaction, DataMigrationTransactionData tran) throws SQLException, Exception {
        LRManipulator lrm = new LRManipulator(session);
        
        lrm.lrStartTransaction(con, theTransaction, new ArrayList<String>());

        String holdingUID=lrm.lrCreateNewHolding(con, theTransaction,tran.holding);
        for(LADM.Parcel p:tran.holding.parcels)
            lrm.lrAddParcel(con, theTransaction, holdingUID, p);
                
        lrm.finishTransaction(con, theTransaction);
    }

    @Override
    public void saveTransaction(UserSession session, Connection con,
            LATransaction theTransaction, boolean register) throws Exception {
        try {
            DataMigrationTransactionData tran = (DataMigrationTransactionData) theTransaction.data;            

            if (!register) {
                return;
            }           
            
            registerTransaction(session, con, theTransaction, tran);

        } catch (CloneNotSupportedException | IOException ex) {
            throw new SQLException("Can't execute transaction", ex);
        }
    }
}