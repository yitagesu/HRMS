/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.repo;

import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.*;
import com.intaps.nrlais.model.map.GeoJSON;
import com.intaps.nrlais.util.GISUtils;
import com.intaps.nrlais.util.NamedPreparedStatement;
import com.intaps.nrlais.worlais.WORLAISClient;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;
import com.vividsolutions.jts.io.WKTWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.apache.commons.lang3.StringUtils;
import org.postgis.Geometry;

/**
 *
 * @author Tewelde
 */
public class CMSSTaskManager extends RepoBase {

    public final static double OVERLAP_TOL = 0.0001;
    public final static double GRID_TOL = 0.01;

    public CMSSTaskManager(UserSession session) {
        super(session);
    }

    public void nodeTaskGeometry(Connection con, String txuid) {

    }

    boolean checkOverLap(Connection con, String txuid, boolean checkInventory) throws SQLException {
        List<String> tranParcels = new ArrayList<String>();
        try (ResultSet rs = con.prepareStatement("Select uid from nrlais_transaction.t_parcels where currenttxuid='" + txuid + "' and editStatus<>'d'").executeQuery()) {
            while (rs.next()) {
                tranParcels.add(rs.getString(1));
            }
        }
        String sql = "Select * from nrlais_transaction.t_parcels where editStatus<>'d' and uid<>?::uuid and "
                + "St_Area(ST_intersection(geometry,(Select geometry from nrlais_transaction.t_parcels where uid=?::uuid)))>?";
        PreparedStatement psTran = con.prepareStatement(sql);
        sql = "Select * from nrlais_inventory.t_parcels where uid not in (Select uid from nrlais_transaction.t_parcels where currenttxuid=?::uuid) and "
                + "St_Area(ST_intersection(geometry,(Select geometry from nrlais_transaction.t_parcels where  uid=?::uuid)))>?";
        PreparedStatement psInv = con.prepareStatement(sql);
        for (String parcel : tranParcels) {
            psTran.setString(1, parcel);
            psTran.setString(2, parcel);
            psTran.setDouble(3, OVERLAP_TOL);
            try (ResultSet rs = psTran.executeQuery()) {
                if (rs.next()) {
                    return true;
                }
            }
            if (checkInventory) {
                psInv.setString(1, txuid);
                psInv.setString(2, parcel);
                psInv.setDouble(3, OVERLAP_TOL);
                try (ResultSet rs = psInv.executeQuery()) {
                    if (rs.next()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    void addTaskGeometry(Connection con, String taskID, int id, String parcelUID, String label) throws SQLException {
        String geom = new GetLADM(session, "nrlais_transaction").getParcelGeometry(con, parcelUID);
        if (geom == null) {
            throw new IllegalStateException("Parcel geometry for parcelUID:" + parcelUID + " not found");
        }
        PreparedStatement insertGeom = con.prepareStatement("INSERT INTO nrlais_transaction.cmss_task_geom(id,task_uid, geom,label,parcel_uid) VALUES (?,?::uuid, ST_GeomFromText(?),?,?::uuid);");
        insertGeom.setInt(1, id);
        insertGeom.setString(2, taskID);
        insertGeom.setString(3, geom);
        insertGeom.setString(4, label);
        insertGeom.setString(5, parcelUID);

        insertGeom.execute();

    }

    String addTask(Connection con, CMSSTask task) throws SQLException {
        PreparedStatement insertEditParcelTask = con.prepareStatement("INSERT INTO nrlais_transaction.cmss_task(uid, transaction_uid,status,task_type) VALUES (?::uuid, ?::uuid, ?,?);");
        task.taskUID = UUID.randomUUID().toString();
        insertEditParcelTask.setString(1, task.taskUID);
        insertEditParcelTask.setString(2, task.transactionUID);
        insertEditParcelTask.setInt(3, task.status);
        insertEditParcelTask.setInt(4, task.taskType);
        insertEditParcelTask.execute();
        return task.taskUID;
    }

    public String addEditParcelTask(Connection con, CMSSEditParcelTask task) throws SQLException {
        PreparedStatement insertEditParcelTask = con.prepareStatement("INSERT INTO nrlais_transaction.cmss_edit_task_parcels(task_uid, parcel_uid) VALUES (?::uuid, ?::uuid);");
        task.taskType = CMSSTask.TASK_TYPE_EDIT;
        task.taskUID = addTask(con, task);
        int id = 1;
        for (String p : task.parcels) {
            insertEditParcelTask.setString(1, task.taskUID);
            insertEditParcelTask.setString(2, p);
            insertEditParcelTask.execute();
            addTaskGeometry(con, task.taskUID, id++, p, new GetLADM(session, "nrlais_transaction").getParcelUPID(con, p));
        }

        return task.taskUID;
    }

    public String addSplitParcelTask(Connection con, CMSSParcelSplitTask task) throws SQLException {
        PreparedStatement insertSplitParcelTask = con.prepareStatement("INSERT INTO nrlais_transaction.cmss_split_task(task_uid, parcel_uid) VALUES (?::uuid, ?::uuid);");
        task.status = CMSSTask.TASK_STATUS_PENDING;
        task.taskType = CMSSTask.TASK_TYPE_SPLIT;
        task.taskUID = addTask(con, task);
        //CMSSTask taskTest=new GetCMSSTask(session).getTask(con, task.taskUID, false);

        insertSplitParcelTask.setString(1, task.taskUID);
        insertSplitParcelTask.setString(2, task.parcelUID);
        insertSplitParcelTask.execute();
        PreparedStatement insertSplitParcelTaskParcels = con.prepareStatement("INSERT INTO nrlais_transaction.cmss_split_task_parcels(task_uid, target_parcel_uid) VALUES (?::uuid, ?::uuid);");
        for (String p : task.newParcelUIDs) {
            insertSplitParcelTaskParcels.setString(1, task.taskUID);
            insertSplitParcelTaskParcels.setString(2, p);
            insertSplitParcelTaskParcels.execute();
        }
        addTaskGeometry(con, task.taskUID, 1, task.parcelUID, new GetLADM(session, "nrlais_transaction").getParcelUPID(con, task.parcelUID));
        return task.taskUID;
    }

    public String addCreateParcelsTask(Connection con, CMSSCreateParcelsTask task) throws SQLException {
        task.taskType = CMSSTask.TASK_TYPE_CREATE;
        task.taskUID = addTask(con, task);

        PreparedStatement insertCreateParcelsTaskParcels = con.prepareStatement("INSERT INTO nrlais_transaction.cmss_create_task_parcels(task_uid, target_parcel_uid) VALUES (?::uuid, ?::uuid);");
        for (String p : task.newParcelUIDs) {
            insertCreateParcelsTaskParcels.setString(1, task.taskUID);
            insertCreateParcelsTaskParcels.setString(2, p);
            insertCreateParcelsTaskParcels.execute();
        }
        return task.taskUID;
    }

    public void commitTask(Connection con, String task_uid) throws SQLException {
        con.prepareStatement("Update nrlais_transaction.cmss_task set status=" + CMSSTask.TASK_STATUS_COMMITED + " where uid='" + task_uid + "'").execute();
    }

    public void assignGeometry(Connection con, CMSSTask.TaskGeomAssignment a) throws SQLException, ParseException, IOException {
        CMSSTask task = new GetCMSSTask(session).getTask(con, a.task_uid, true);
        if (task == null) {
            throw new IllegalArgumentException("Task " + a.task_uid + " not found");
        }
        LATransaction tran = new GetTransaction(session).get(con, task.transactionUID, false);
        if (tran.status != LATransaction.TRAN_STATUS_REGISTERD) {
            throw new IllegalStateException("It is not allowed to periform CMSS task for this application becuase it in " + LATransaction.getStatusString(tran.status) + ", to do CMSS task transactions should be in " + LATransaction.getStatusString(LATransaction.TRAN_STATUS_REGISTERD) + " state");
        }
        convertPolygonToMulti(a);

        NamedPreparedStatement ps = new NamedPreparedStatement(session,con,
                "Update nrlais_transaction.t_parcels set geometry=ST_GeomFromText(@g),editstatus=@pe where uid=@puid::uuid;"
                + "Update nrlais_transaction.t_parcels set areageom=ST_Area(geometry) where uid=@puid::uuid;"
                + "Update nrlais_transaction.t_holdings set editstatus=@he where uid=@huid::uuid;");
        GetLADM ladm = new GetLADM(session, "nrlais_transaction");
        if (task instanceof CMSSParcelSplitTask) {
            CMSSParcelSplitTask split = (CMSSParcelSplitTask) task;
            String holdingUID = ladm.getHoldingIDForParcel(con, split.parcelUID);
            if (a.assignments.length != split.newParcelUIDs.size()) {
                throw new IllegalArgumentException("The parcel must be split into exactly " + split.newParcelUIDs.size() + " parts");
            }
            for (String p : split.newParcelUIDs) {
                CMSSTask.TaskParcelGeom t = null;
                for (CMSSTask.TaskParcelGeom tt : a.assignments) {
                    if (tt.parcel_uid.equals(p)) {
                        t = tt;
                        break;
                    }
                }
                if (t == null) {
                    throw new IllegalArgumentException("All parcels must be assigned");
                }
                ps.setString("@g", t.geom);
                ps.setString("@puid", p);
                ps.setString("@huid", holdingUID);
                ps.setString("@pe", "n");
                LADM oldHolding = ladm.get(con, holdingUID, LADM.CONTENT_FULL);
                ps.setString("@he", oldHolding != null ? "u" : "d");

                ps.preparedStatement().execute();
            }
            String verifySQL = "Select st_area((Select ST_SymDifference  ((Select geom from nrlais_transaction.cmss_task_geom where task_uid='@t'::uuid),\n"
                    + "(Select st_union(geometry) from nrlais_transaction.t_parcels\n"
                    + "where uid in (Select target_parcel_uid from nrlais_transaction.cmss_split_task_parcels where task_uid='@t'::uuid)))))";
            verifySQL = StringUtils.replaceEach(verifySQL, new String[]{"@t"}, new String[]{((CMSSParcelSplitTask) task).taskUID});
            try (ResultSet rs = con.prepareStatement(verifySQL).executeQuery()) {
                if (rs.next()) {
                    double diff = rs.getDouble(1);
                    if (rs.wasNull()) {
                        throw new IllegalStateException("Failed to periform verification of the split operation");
                    }
                    if (Math.abs(diff) > 0.01) {
                        throw new IllegalArgumentException("It is not allowed to modify the boundaries of parcels during split operation");
                    }
                } else {
                    throw new IllegalStateException("Failed to periform verification of the split operation");
                }
            }
            //TOPO Check: check if the union of the parces is the same as the original
            //1. compare the total area of the split parcels with orginal parcel area, it should be equal within a tollerance
            //2. union the geometries and see if they are equal, us ST_Union and ST_Equals

        } else if (task instanceof CMSSEditParcelTask) {
            CMSSEditParcelTask editTask = (CMSSEditParcelTask) task;
            for (CMSSTask.TaskParcelGeom g : a.assignments) {
                boolean found = false;
                for (String p : editTask.parcels) {
                    if (p.equals(g.parcel_uid)) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    throw new IllegalArgumentException("Parcel " + g.parcel_uid + " is not part of the boundary correction operation");
                }
                String holdingUID = ladm.getHoldingIDForParcel(con, g.parcel_uid);

                ps.setString("@g", g.geom);
                ps.setString("@puid", g.parcel_uid);
                ps.setString("@huid", holdingUID);
                ps.setString("@pe", "u");
                ps.setString("@he", "u");
                ps.preparedStatement().execute();
            }
        } else if (task instanceof CMSSCreateParcelsTask) {
            CMSSCreateParcelsTask creatTask = (CMSSCreateParcelsTask) task;
            for (CMSSTask.TaskParcelGeom g : a.assignments) {
                boolean found = false;
                for (String p : creatTask.newParcelUIDs) {
                    if (p.equals(g.parcel_uid)) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    throw new IllegalArgumentException("Parcel " + g.parcel_uid + " is not part of the boundary correction operation");
                }
                String holdingUID = ladm.getHoldingIDForParcel(con, g.parcel_uid);

                ps.setString("@g", g.geom);
                ps.setString("@puid", g.parcel_uid);
                ps.setString("@huid", holdingUID);
                LADM test=new GetLADM(session, "nrlais_inventory").get(con, holdingUID,LADM.CONTENT_HOLDING);
                ps.setString("@he",test==null? "n":"u");
                ps.setString("@pe", "n");
                ps.preparedStatement().execute();

            }
        }
        if (checkOverLap(con, task.transactionUID, true)) {
            throw new IllegalStateException("The change you have made will cause overlap, it can't be commited");
        }
    }

    private void convertPolygonToMulti(CMSSTask.TaskGeomAssignment a) throws ParseException {
        GeometryFactory gf = new GeometryFactory();
        com.vividsolutions.jts.io.WKTWriter writer = new WKTWriter();
        for (CMSSTask.TaskParcelGeom tt : a.assignments) {
            com.vividsolutions.jts.io.WKTReader r = new WKTReader();
            String[] parts = tt.geom.split(";");
            com.vividsolutions.jts.geom.Geometry geom = r.read(parts[parts.length - 1]);
            String sridPrefix = parts.length == 0 ? "" : parts[0] + ";";
            if (geom instanceof com.vividsolutions.jts.geom.Polygon) {
                com.vividsolutions.jts.geom.Polygon p = (com.vividsolutions.jts.geom.Polygon) geom;
                com.vividsolutions.jts.geom.MultiPolygon mp = new com.vividsolutions.jts.geom.MultiPolygon(
                        new com.vividsolutions.jts.geom.Polygon[]{p}, gf);
                tt.geom = sridPrefix + writer.write(mp);
            }
        }
    }

}
