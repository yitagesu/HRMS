/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.util;

import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

/**
 *
 * @author Teweldemedhin Aberra
 */
public class EthiopianCalendar {

    static final int c1 = 0x23ab1;
    static final int c2 = 0x8eac;
    static final int c3 = 0x5b5;
    static final int dd = 0x97e;
    public int Day;
    public int Month;
    public int Year;

    private static int Months(int index) {
        switch (index) {
            case 0:
            case 2:
            case 4:
            case 6:
            case 7:
            case 9:
            case 11:
                return 0x1f;

            case 1:
                return 0x1c;
        }
        return 30;
    }

    private static int AddMonths(int m, int y) {
        int num2 = 0;
        for (int i = 0; i < (m - 1); i++) {
            num2 += Months(i);
            if ((i == 1) && IsLeapYearGr(y)) {
                num2++;
            }
        }
        return num2;
    }

    static class MonthDay {

        public int m;
        public int d;

        public MonthDay(int m, int d) {
            this.m = m;
            this.d = d;
        }
    }

    private static void GetMonthAndDayGrig(int n, int y, MonthDay md) {
        int num = 1;
        while (n >= GetMonthLengthGrig(num, y)) {
            n -= GetMonthLengthGrig(num, y);
            num++;
        }
        md.m = num;
        md.d = n + 1;
    }

    public static int GetMonthLengthGrig(int m, int y) {
        if ((m == 2) && IsLeapYearGr(y)) {
            return 0x1d;
        }
        return Months(m - 1);
    }

    public static int GetMonthLengthEt(int m, int y) {
        if (m == 13) {
            return (IsLeapYearEt(y) ? 6 : 5);
        }
        return 30;
    }

    public static boolean IsLeapYearGr(int y) {
        return (((y % 4) == 0) && (((y % 100) != 0) || ((y % 400) == 0)));
    }

    public int DayNoEt() {

        int num = this.Year / 4;
        int num2 = this.Year % 4;
        return (((((num * 0x5b5) + (num2 * 0x16d)) + ((this.Month - 1) * 30)) + this.Day) - 1);
    }

    public int GetDayOfWeekEt() {

        return (((this.DayNoEt() + 1) % 7) + 1);

    }

    public EthiopianCalendar(int dn, boolean Ethiopian) {
        int num;
        int num2;
        int num3;
        int num4;
        if (Ethiopian) {
            num = dn / 0x5b5;
            num2 = dn % 0x5b5;
            num3 = num2 / 0x16d;
            num4 = num2 % 0x16d;
            if (num2 != 0x5b4) {
                this.Year = (num * 4) + num3;
                this.Month = (num4 / 30) + 1;
                this.Day = (num4 % 30) + 1;
            } else {
                this.Year = ((num * 4) + num3) - 1;
                this.Month = 13;
                this.Day = 6;
            }
        } else {
            num = dn / 0x23ab1;
            num2 = dn % 0x23ab1;
            num3 = num2 / 0x8eac;
            num4 = num2 % 0x8eac;
            int num5 = num4 / 0x5b5;
            int num6 = num4 % 0x5b5;
            int num7 = num6 / 0x16d;
            int n = num6 % 0x16d;
            this.Year = ((((num * 400) + (num3 * 100)) + (num5 * 4)) + num7) + 1;
            this.Month = 0;
            this.Day = 0;
            MonthDay md = new MonthDay(this.Month, this.Day);
            GetMonthAndDayGrig(n, this.Year, md);
            this.Month = md.m;
            this.Day = md.d;
        }
    }

    public int DayNoGrig() {
        int num = (this.Year - 1) / 400;
        int num2 = (this.Year - 1) % 400;
        int num3 = num2 / 100;
        int num4 = num2 % 100;
        int num5 = num4 / 4;
        int num6 = num4 % 4;
        int num7 = AddMonths(this.Month, this.Year);
        return (((((((num * 0x23ab1) + (num3 * 0x8eac)) + (num5 * 0x5b5)) + (num6 * 0x16d)) + num7) + this.Day) - 1);
    }

    public int DayOfWeekGrig() {
        return ((this.DayNoGrig() % 7) + 1);
    }

    public EthiopianCalendar(int d, int m, int y) {
        this.Day = d;
        this.Month = m;
        this.Year = y;
    }

    public EthiopianCalendar(java.util.Date dt) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(dt);
        this.Day = cal.get(Calendar.DAY_OF_MONTH);
        this.Month = cal.get(Calendar.MONTH)+1;
        this.Year = cal.get(Calendar.YEAR);
    }

    public java.util.Date GridDate() {

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, this.Year);
        cal.set(Calendar.MONTH, this.Month-1);
        cal.set(Calendar.DAY_OF_MONTH, this.Day);
        return cal.getTime();
    }

    public static EthiopianCalendar ToEth(EthiopianCalendar gr) {
        return new EthiopianCalendar(gr.DayNoGrig() - 0x97e, true);
    }

    public static EthiopianCalendar ToEth(java.util.Date dt) {
        return ToEth(new EthiopianCalendar(dt));
    }
    public static EthiopianCalendar ToEth(long time) {
        return ToEth(new EthiopianCalendar(new java.util.Date(time)));
    }

    public static String ToEthWithTime(java.util.Date dt) {
        //return (ToEth(dt).ToString() + " " + dt.ToShortTimeString());
        throw new UnsupportedOperationException();
    }

    public static EthiopianCalendar ToGrig(EthiopianCalendar et) {
        return new EthiopianCalendar(et.DayNoEt() + 0x97e, false);
    }

    public static String GetEtMonthName(int m) {
        switch (m) {
            case 1:
                return "áˆ˜áˆµáŠ¨áˆ¨áˆ�";

            case 2:
                return "áŒ¥á‰…áˆ�á‰µ";

            case 3:
                return "áˆ…á‹³áˆ­";

            case 4:
                return "á‰³áˆ…áˆ³áˆµ";

            case 5:
                return "áŒ¥áˆ­";

            case 6:
                return "á‹¨áŠ«á‰²á‰µ";

            case 7:
                return "áˆ˜áŒ‹á‰¢á‰µ";

            case 8:
                return "áˆšá‹«á‹šá‹«";

            case 9:
                return "áŒ�áŠ•á‰¦á‰µ";

            case 10:
                return "áˆ°áŠ”";

            case 11:
                return "áˆ�áˆ�áˆŒ";

            case 12:
                return "áŠ�áˆ€áˆ´";

            case 13:
                return "áŒ³áŒ‰áˆœ";
        }
        return "";
    }

    public static String GetEtMonthNameEng(int m) {
        return GetEtMonthNameEng(m, 0);
    }

    public static String GetEtMonthNameEng(int m, int langagueID) {
        throw new UnsupportedOperationException();
    }

    public static String GetDayOfWeekNameEt(int d) {
        switch (d) {
            case 1:
                return "áˆ°áŠž";

            case 2:
                return "áˆ›áŠ­áˆ°áŠž";

            case 3:
                return "áˆ¨á‰¡á‹•";

            case 4:
                return "áˆ€áˆ™áˆµ";

            case 5:
                return "áŠ áˆ­á‰¥";

            case 6:
                return "á‰…á‹³áˆœ";

            case 7:
                return "áŠ¥áˆ�á‹µ";
        }
        return "";
    }

    public static String GetGrigMonthName(int m) {
        switch (m) {
            case 1:
                return "January";

            case 2:
                return "February";

            case 3:
                return "March";

            case 4:
                return "April";

            case 5:
                return "May";

            case 6:
                return "June";

            case 7:
                return "July";

            case 8:
                return "August";

            case 9:
                return "September";

            case 10:
                return "October";

            case 11:
                return "Novomber";

            case 12:
                return "December";
        }
        return "";
    }

    public static String GetDayOfWeekNameGrig(int d) {
        switch (d) {
            case 1:
                return "Mon";

            case 2:
                return "Tue";

            case 3:
                return "Wed";

            case 4:
                return "Thu";

            case 5:
                return "Fri";

            case 6:
                return "Sat";

            case 7:
                return "Sun";
        }
        return "";
    }

    public static boolean IsLeapYearEt(int y) {
        return ((y % 4) == 3);
    }

    @Override
    public String toString() {
        return Integer.toString(this.Day) + "/" + Integer.toString(this.Month) + "/" + Integer.toString(this.Year);
    }

   
    static Pattern digits=Pattern.compile("\\d{1,}");
    public static EthiopianCalendar TryParse(String date, boolean ethiopian) {
        
        String[] strArray = date.split("[,\\\\\\/]");
        if (strArray.length < 3) {
            return null;
        }
        for(String p:strArray)
            if(!digits.matcher(p.trim()).matches())
                return null;
        int d = Integer.parseInt(strArray[0]);
        int m = Integer.parseInt(strArray[1]);
        int y=Integer.parseInt(strArray[2]);
        if(!IsValidDMY(d, m, y, ethiopian))
            return null;
        return new EthiopianCalendar(d, m,y );
    }

    public static boolean IsValidDMY(int day, int month, int year, boolean _eth) {
        if ((year < 0x3e8) || (year > 0xbb8)) {
            return false;
        }
        if (month < 1) {
            return false;
        }
        if (day < 1) {
            return false;
        }
        if (_eth) {
            if (month > 13) {
                return false;
            }
            if (day > GetMonthLengthEt(month, year)) {
                return false;
            }
        } else {
            if (month > 12) {
                return false;
            }
            if (day > GetMonthLengthGrig(month, year)) {
                return false;
            }
        }
        return true;
    }
    public static int getAge(long dob)
    {
        Date currdate = new Date();
                     
                     Calendar cal = Calendar.getInstance();
                     cal.setTime(currdate);
                     int cmonth = cal.get(Calendar.MONTH);
                     int cyear = cal.get(Calendar.YEAR);
                     int cdate = cal.get(Calendar.DATE);
                   // currdate.compareTo(dob);
                   Calendar dcal = Calendar.getInstance();
                   dcal.setTimeInMillis(dob);
                   
                   int dmonth = dcal.get(Calendar.MONTH);
                   int dyear = dcal.get(Calendar.YEAR);
                   int ddate = dcal.get(Calendar.DATE);
                   
                   int age = cyear-dyear;
                   if(cmonth<dmonth || (cmonth==dmonth && cdate<ddate))
                       age--;
                   return age;
    }
}
