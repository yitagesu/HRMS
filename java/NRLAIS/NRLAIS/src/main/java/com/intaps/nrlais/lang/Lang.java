/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.lang;

import com.intaps.nrlais.util.ResourceUtils;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;
import org.apache.commons.lang3.StringUtils;


/**
 *
 * @author Tewelde
 */
public class Lang {

    static HashMap<String, String> am= new HashMap<String, String>();
    static HashMap<String, String> or= new HashMap<String, String>();
    static HashMap<String, String> ti= new HashMap<String, String>();
    static {
        try
        {
            loadTable("am_header_jsp","am-et");
            loadTable("am_boundary_correction_jsp","am-et");
            loadTable("am_consolidation_jsp","am-et");
            loadTable("am_transction_common","am-et");
            loadTable("am_transaction_search_result_jsp","am-et");
            loadTable("am_register_divorce_jsp","am-et");
            loadTable("am_register_ex_officio_jsp","am-et");
            loadTable("am_inheritance_jsp","am-et");
            loadTable("am_rent_lease_jsp","am-et");
            loadTable("am_register_exchange_jsp","am-et");
            loadTable("am_register_expropriation_jsp.js","am-et");
            loadTable("am_full_expropriation_viewer_content_jsp","am-et");
            loadTable("am_full_exchange_viewer_content_jsp","am-et");
            loadTable("am_full_divorce_viewer_content_jsp","am-et"); 
            loadTable("am_full_boundary_correction_content_jsp","am-et"); 
            loadTable("am_party_jsp","am-et");
            loadTable("am_parcel_jsp","am-et");
            loadTable("am_gift_jsp","am-et");
            loadTable("am_split_jsp","am-et");
            loadTable("am_special_case_jsp","am-et");
            loadTable("am_full_ex_officio_viewer_content_jsp","am-et");
            loadTable("handlers","am-et");
            loadTable("am_certificate_replacement_jsp","am-et");
            loadTable("am_reallocation_jsp","am-et");
            loadTable("am_simple_correction_jsp","am-et");
            loadTable("am_servitude_jsp","am-et");
            loadTable("am_restrictive_interest_jsp","am-et");
            loadTable("am_lr_manipulator_jsp","am-et");
            loadTable("miscellanious_jsp","am-et");
        }
        catch(Exception ex)
        {
            System.out.println("Error loading language files");
            ex.printStackTrace();
        }
    }
    
    static void loadTable(String file,String lang) throws IOException
    {
        HashMap<String, String> table=getTable(lang);
        if(table==null)
            return;
        Properties props=ResourceUtils.getLangFile(file);
        for(String key:props.stringPropertyNames())
        {
            table.put(key.toString(), props.getProperty(key));
        }
    }
    public static HashMap<String,String> getTable(String lang)
    {
        switch(lang)
        {
            case "am-et":
                return am;
            case "or-et":
                return or;
            case "ti-et":
                return ti;
        }
        return null;
    }
    public static String tran(String lang,String textKey, Object... args) {
        String text;
        HashMap<String, String> table=getTable(lang);
        if (table!=null && table.containsKey(textKey)) {
            text = table.get(textKey);
        } else {
            text = textKey;
        }
        if (args.length == 0) {
            return text;
        }
        if(args.length==0)
            return text;
        
        text = StringUtils.replaceEach(text, new String[]{"/{", "/}"}, new String[]{"{", "}"});
        String[] indexList = new String[args.length];

        for (int i = 0; i < indexList.length; i++) {
            indexList[i] = "{" + i + "}";
        }
        String[] replist = new String[args.length];
        for (int i = 0; i < replist.length; i++) {
            if (args[i] == null) {
                replist[i] = "";
            } else if (args[i] instanceof String) {
                replist[i] = (String) args[i];
            } else {
                replist[i] = args[i].toString();
            }
        }
        return StringUtils.replaceEach(text, indexList, replist);
    }
    
}
