/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.model.tran;

import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.SourceDocument;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tewelde
 */
public class SpecialCaseTransactionData  extends LATransaction.TransactionData{
    public LADM.Holding holding=new LADM.Holding();
    public List<PartyItem> applicants=new ArrayList<>();
    public List<PartyItem> restrictors=new ArrayList<>();
    public List<ParcelItem> parcels=new ArrayList<>();
    public SourceDocument claimResolutionDoc=null;
    public SourceDocument firtLevelLandHoldingBook=null;
    
}
