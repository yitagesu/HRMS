/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.tran;

import com.intaps.nrlais.model.IDNameText;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.SourceDocument;
import com.intaps.nrlais.model.tran.Applicant;
import com.intaps.nrlais.model.tran.ParcelTransfer;
import com.intaps.nrlais.model.tran.RestrictiveInterestTransactionData;
import com.intaps.nrlais.model.tran.ServitudeTransactionData;
import com.intaps.nrlais.util.GSONUtil;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Tewelde
 */
public class ServitudeViewController extends RestrictionViewController<ServitudeTransactionData> {    

    public ServitudeViewController(HttpServletRequest request, HttpServletResponse response) throws Exception {
        super(request, response, ServitudeTransactionData.class, LATransaction.TRAN_TYPE_SERVITUDE);
    }
    public ServitudeViewController(HttpServletRequest request, HttpServletResponse response, boolean loadData) throws Exception {
        super(request, response, ServitudeTransactionData.class, LATransaction.TRAN_TYPE_SERVITUDE, loadData);
        if (loadData) {
            if (data.holdingUID == null) {
                holding = new LADM.Holding();
            } else {
                holding = mainFacade.getLADM("nrlais_inventory", data.holdingUID).holding;
            }
            kebele = mainFacade.getKebele(holding.nrlais_kebeleid);
            woreda = mainFacade.getWoreda(holding.nrlais_woredaid);
        }
    }
    @Override
    public List<IDNameText> restrictionTypes() throws SQLException {
        return mainFacade.getAllLookupText("static.servitude");
    }

    @Override
    public String tranTypeText() {
        return "Servitude/Easement";
    }

    @Override
    public String agreementRef() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String agreementDesc() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
