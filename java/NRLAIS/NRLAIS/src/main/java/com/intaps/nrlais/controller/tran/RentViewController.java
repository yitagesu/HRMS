/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.tran;

import com.intaps.nrlais.Startup;
import com.intaps.nrlais.model.IDName;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.SourceDocument;
import com.intaps.nrlais.model.tran.Applicant;
import com.intaps.nrlais.model.tran.DivorceTransactionData;
import com.intaps.nrlais.model.tran.ParcelTransfer;
import com.intaps.nrlais.model.tran.Partition;
import com.intaps.nrlais.model.tran.PartyParcelShare;
import com.intaps.nrlais.model.tran.RentTransactionData;
import com.intaps.nrlais.repo.MainFacade;
import com.intaps.nrlais.repo.RepoBase;
import com.intaps.nrlais.repo.TransactionFacade;
import com.intaps.nrlais.util.GSONUtil;
import com.intaps.nrlais.util.INTAPSDateUtils;
import com.intaps.nrlais.util.INTAPSLangUtils;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Tewelde
 */
public class RentViewController extends TransactionViewController<RentTransactionData> {

    public LADM.Holding holding;

    public RentViewController(HttpServletRequest request,HttpServletResponse response) throws Exception {
        super(request,response, RentTransactionData.class, LATransaction.TRAN_TYPE_RENT);
    }

    public RentViewController(HttpServletRequest request,HttpServletResponse response, boolean loadData) throws Exception {
        super(request,response, RentTransactionData.class, LATransaction.TRAN_TYPE_RENT, loadData);
        if (loadData) {
            if (data.holdingUID == null) {
                holding = new LADM.Holding();
            } else {
                    holding = mainFacade.getLADM("nrlais_inventory", data.holdingUID).holding;
            }
            kebele = mainFacade.getKebele(holding.nrlais_kebeleid);
            woreda = mainFacade.getWoreda(holding.nrlais_woredaid);
        }
    }


    public Applicant partyApplicant(String partyUID) {
        for (Applicant ap : this.data.applicants) {
            if (partyUID.equals(ap.selfPartyUID)) {
                return ap;
            }
        }
        return null;
    }

    public String letterOfAttroneyText(Applicant ap) {
        if (ap == null) {
            return "";
        }

        if (ap.letterOfAttorneyDocument == null) {
            return "";
        }
        return ap.letterOfAttorneyDocument.refText;
    }

    public String letterOfAttroneyLink(Applicant ap) {
        if (ap == null) {
            return "";
        }
        return showDocumentLink(ap.letterOfAttorneyDocument);
    }


    


    public ParcelTransfer transfer(String parcelUID) {
        for(ParcelTransfer t: this.data.transfers)
            if(t.parcelUID.equals(parcelUID))
                return t;
        
        return null;
    }
    public LADM.Parcel transferParcel(ParcelTransfer t)
    {
        return holding.getParcel(t.parcelUID);
    }
    
    public String documentText(SourceDocument doc) {
        if (doc==null) {
            return "";
        }
        return doc.refText;
    }

    public String documentDescription(SourceDocument doc) {
        if (doc==null) {
            return "";
        }
        return doc.description;
    }
    
   
    public String holdingJson()
    {
        return GSONUtil.toJson(this.holding);
    }
    
        @Override
    public List<CertificateLink> certficateLinks() throws SQLException, IOException {
        List<TransactionViewController.CertificateLink> ret = new ArrayList<TransactionViewController.CertificateLink>();
        HashSet<String> done = new HashSet<String>();
        if(!this.data.isRent)
        {
        for (ParcelTransfer p : this.data.transfers) {
                CertificateLink link = new CertificateLink();
                LADM.Parcel l = mainFacade.getHistoryParcelBySourceTx(this.tran.transactionUID, p.parcelUID);
                if (l==null || l.rights.size() == 0) {
                    continue;
                }
                for(LADM.Right r:l.getTenants())
                {
                    link.owner=INTAPSLangUtils.appendStringList(link.owner, ", ", r.party.getFullName());
                }
                link.url = "javascript:transcation_showCertiricate('" + tran.transactionUID + "','" + l.rights.get(0).holdingUID + "','" + p.parcelUID + "')";
                link.upid=l.upid;
                ret.add(link);
            
        }
        ret.sort(new Comparator<CertificateLink>() {
            @Override
            public int compare(CertificateLink o1, CertificateLink o2) {
                return o1.owner.compareTo(o2.owner);
            }
        });
        
        }
        return ret;
    }
    

}
