/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.util;

import java.util.Date;

/**
 *
 * @author Tewelde
 */
public class INTAPSDateUtils {
    public static int calculateAgeEth(long d1,long d2)
    {
        EthiopianCalendar date1=EthiopianCalendar.ToEth(new Date(d1));
        EthiopianCalendar date2=EthiopianCalendar.ToEth(new Date(d2));
        int age=date2.Year-date1.Year;
        if(date2.Month<date1.Month)
            age--;
        else if(date2.Month==date1.Month && date2.Day<date1.Day)
            age--;
        return age;            
    }   
}
