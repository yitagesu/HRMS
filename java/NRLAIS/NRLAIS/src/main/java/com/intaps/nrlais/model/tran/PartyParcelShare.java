/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.model.tran;

import com.intaps.nrlais.util.RationalNum;

/**
 *
 * @author Tewelde  
 */
public class PartyParcelShare {
    
    public String partyUID;
    public RationalNum share = new RationalNum();
    public boolean splitGeom = false;
    public String geometry = null;
    public String parcelUID=null;
    
}
