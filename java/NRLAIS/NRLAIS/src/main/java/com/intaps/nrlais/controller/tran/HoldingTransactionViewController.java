/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.tran;

import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.tran.ExOfficioTransactionData;
import com.intaps.nrlais.model.tran.GiftTransactionData;
import com.intaps.nrlais.model.tran.HoldingTranscationData;
import com.intaps.nrlais.model.tran.InheritanceTransactionData;
import com.intaps.nrlais.util.GSONUtil;
import com.intaps.nrlais.util.INTAPSLangUtils;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author yitagesu
 */
public class HoldingTransactionViewController<T extends HoldingTranscationData> extends TransactionViewController<T> {

    public LADM.Holding holding;

    public HoldingTransactionViewController(HttpServletRequest request, HttpServletResponse response, Class c, int tranType, boolean loadData) throws Exception {
        super(request, response, c, tranType, loadData);
        if (loadData) {
            if (data.holdingUID == null) {
                holding = new LADM.Holding();
            } else {
                if (tran.isCommited()) {
                    holding = mainFacade.getFromHistoryByArchiveTx(tran.transactionUID, data.holdingUID).holding;
                } else {
                    holding = mainFacade.getLADM("nrlais_inventory", data.holdingUID).holding;
                }

            }
            kebele = mainFacade.getKebele(holding.nrlais_kebeleid);
            woreda = mainFacade.getWoreda(holding.nrlais_woredaid);
        }
    }

    public String holdingJson() {
        return GSONUtil.toJson(this.holding);
    }

    public String holdingLink() {
        String ret = "/holding/holding_information_viewer_page.jsp?holdingUID=" + this.holding.holdingUID;
        if (tran.isCommited()) {
            ret += "&tran_uid=" + tran.transactionUID;
        }
        return ret;
    }
    private List<String> newParcels(LADM before, LADM after) {
        List<String> lp = new ArrayList<>();
        List<String> pUID = new ArrayList<>();
        List<String> pGeom = new ArrayList<>();
        for (LADM.Parcel p : before.holding.parcels) {
            pUID.add(p.parcelUID);
            pGeom.add(p.geometry);
        }
        for (LADM.Parcel p : after.holding.parcels) {
            if (!pUID.contains(p.parcelUID) || !pGeom.contains(p.geometry)) {
                lp.add(p.parcelUID);
            }
        }
        return lp;
    }

    public boolean showAdd() {
        return tran.transactionType == LATransaction.TRAN_TYPE_EX_OFFICIO;
    }
    
    public boolean showHolders() {
        switch(tran.transactionType)
        {
            case LATransaction.TRAN_TYPE_EX_OFFICIO:
                return true;
            case LATransaction.TRAN_TYPE_SIMPLE_CORRECTION:
                return false;
            case LATransaction.TRAN_TYPE_GIFT:
                return ((GiftTransactionData)data).manipulateManually;
            default:return false;
        }
    }
    
    public boolean showRenters() {
        return tran.transactionType == LATransaction.TRAN_TYPE_EX_OFFICIO;
    }

    public boolean showRestriction() {
        return tran.transactionType == LATransaction.TRAN_TYPE_EX_OFFICIO;
    }

    public boolean showServitude() {
        return tran.transactionType == LATransaction.TRAN_TYPE_EX_OFFICIO;
    }
    public boolean allowParcelTransfer()
    {
        switch(tran.transactionType)
        {
            case LATransaction.TRAN_TYPE_EX_OFFICIO:
                return true;
            case LATransaction.TRAN_TYPE_SIMPLE_CORRECTION:
                return false;
            case LATransaction.TRAN_TYPE_GIFT:
                return ((GiftTransactionData)data).manipulateManually;
            case LATransaction.TRAN_TYPE_INHERITANCE:
            case LATransaction.TRAN_TYPE_INHERITANCE_WITHWILL:
                return ((InheritanceTransactionData)data).manipulateManually;
            default:return false;
        }
    }
    public boolean allowBoundaryCorrection()
    {
        return tran.transactionType == LATransaction.TRAN_TYPE_EX_OFFICIO;
    }
    public boolean allowAddParcel()
    {
        return tran.transactionType == LATransaction.TRAN_TYPE_EX_OFFICIO;
    }
    public boolean allowAddParty()
    {
        return tran.transactionType == LATransaction.TRAN_TYPE_EX_OFFICIO;
    }
    public boolean showPartyTab()
    {
        return tran.transactionType == LATransaction.TRAN_TYPE_EX_OFFICIO;
    }
    public boolean showHoldersTab()
    {
        switch(tran.transactionType)
        {
            case LATransaction.TRAN_TYPE_EX_OFFICIO:
                return true;
            case LATransaction.TRAN_TYPE_SIMPLE_CORRECTION:
                return false;
            case LATransaction.TRAN_TYPE_GIFT:
                return ((GiftTransactionData)data).manipulateManually;
            case LATransaction.TRAN_TYPE_INHERITANCE:
            case LATransaction.TRAN_TYPE_INHERITANCE_WITHWILL:
                return ((InheritanceTransactionData)data).manipulateManually;
            default:return false;
        }
    }
    public boolean allowAddHolder()
    {
        return tran.transactionType == LATransaction.TRAN_TYPE_EX_OFFICIO;
    }
    public boolean allowNewHolding()
    {
        switch(tran.transactionType)
        {
            case LATransaction.TRAN_TYPE_EX_OFFICIO:
                return true;
            case LATransaction.TRAN_TYPE_SIMPLE_CORRECTION:
                return false;
            case LATransaction.TRAN_TYPE_GIFT:
                return ((GiftTransactionData)data).manipulateManually;
            case LATransaction.TRAN_TYPE_INHERITANCE:
            case LATransaction.TRAN_TYPE_INHERITANCE_WITHWILL:
                return ((InheritanceTransactionData)data).manipulateManually;
            default:return false;
        }
    }
    public boolean allowDeleteHolding()
    {
        switch(tran.transactionType)
        {
            case LATransaction.TRAN_TYPE_EX_OFFICIO:
                return true;
            case LATransaction.TRAN_TYPE_SIMPLE_CORRECTION:
                return false;
            case LATransaction.TRAN_TYPE_GIFT:
                return ((GiftTransactionData)data).manipulateManually;
            case LATransaction.TRAN_TYPE_INHERITANCE:
            case LATransaction.TRAN_TYPE_INHERITANCE_WITHWILL:
                return ((InheritanceTransactionData)data).manipulateManually;
            default:return false;
        }
    }
}
