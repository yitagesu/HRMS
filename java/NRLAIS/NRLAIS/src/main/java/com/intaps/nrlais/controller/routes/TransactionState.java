/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.routes;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.intaps.nrlais.Startup;
import com.intaps.nrlais.controller.routes.api.APIBase;
import com.intaps.nrlais.controller.tran.DivorceViewController;
import com.intaps.nrlais.controller.tran.TransactionViewController;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.tran.DivorceTransactionData;
import com.intaps.nrlais.repo.MainFacade;
import com.intaps.nrlais.repo.TransactionFacade;
import com.intaps.nrlais.util.GSONUtil;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Tewelde
 */

@WebServlet("/api/transaction_state")
public class TransactionState extends RouteBase<GSONUtil.EmptyReturn,TransactionState.ChangeStateRequest,GSONUtil.EmptyReturn> {

    @Override
    public GSONUtil.EmptyReturn createGetResponse(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    public static class ChangeStateRequest
    {
        public String txuid;
        public String cmd;
        public String note;
    }
    
    @Override
    public GSONUtil.EmptyReturn processPost(HttpServletRequest req,HttpServletResponse resp, ChangeStateRequest data) throws Exception {        LATransaction tran= new MainFacade(Startup.getSessionByRequest(req)).getTransaction(data.txuid, false);
        if(tran==null)
            throw new IllegalArgumentException("Transaction "+data.txuid+" not found");
        TransactionViewController.createController(tran.transactionType,req,resp,false).processStateChangeCommand(data.txuid,data.cmd,data.note);
        return new GSONUtil.EmptyReturn();
    }

    @Override
    public ChangeStateRequest deserializePostBody(JsonReader reader) throws IOException {
        return GSONUtil.getAdapter(ChangeStateRequest.class).read(reader);
    }    
}

