/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.repo.lrop;

import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.CMSSEditParcelTask;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.tran.ManualTransactionData;
import com.intaps.nrlais.repo.CMSSTaskManager;
import com.intaps.nrlais.repo.GetLADM;
import com.intaps.nrlais.repo.LRManipulator;
import com.intaps.nrlais.repo.ManualLRManipulatorFacade;
import java.io.IOException;
import java.sql.Connection;

/**
 *
 * @author Tewelde
 */
public class LrOpExecStartBoundaryCorrection implements ManualLRManipulatorFacade.LROPExecutor<ManualTransactionData.LrOpStartBoundaryCorrection> {

    @Override
    public Object doOperation(UserSession session, Connection con, LATransaction tran, ManualTransactionData.LrOpStartBoundaryCorrection op) throws Exception {
        CMSSTaskManager cmss=new CMSSTaskManager(session);
        CMSSEditParcelTask task=new CMSSEditParcelTask();
        GetLADM ladm=new GetLADM(session, "nrlais_transaction");
        for(String p:op.parcels)
        {
            LADM.Parcel parcel=ladm.getParcel(con, p, LADM.CONTENT_FULL);
            if(parcel.areaGeom==0)
                throw new IllegalStateException("Parcel doesn't have geometry yet");
        }
        task.transactionUID=tran.transactionUID;
        
        task.parcels.addAll(op.parcels);
        return cmss.addEditParcelTask(con, task);
    }

}
