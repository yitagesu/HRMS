/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.repo.lrop;

import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.tran.ManualTransactionData;
import com.intaps.nrlais.repo.GetLADM;
import com.intaps.nrlais.repo.LRManipulator;
import com.intaps.nrlais.repo.ManualLRManipulatorFacade;
import java.io.IOException;
import java.sql.Connection;
import java.util.List;

/**
 *
 * @author Tewelde
 */
public class LrOpExecUpdateHolder implements ManualLRManipulatorFacade.LROPExecutor<ManualTransactionData.LrOpUpdateHolder> {

    @Override
    public Object doOperation(UserSession session, Connection con, LATransaction tran, ManualTransactionData.LrOpUpdateHolder op) throws Exception {
        LRManipulator lrm = new LRManipulator(session);
        GetLADM ladm = new GetLADM(session, "nrlais_transaction");

        LADM.Holding holding = ladm.get(con, op.holdingUID, LADM.CONTENT_FULL).holding;
        if(holding.parcels.size()==0)
            throw new IllegalStateException("To add a holder the holding should have at least one parcel.");    
        lrm.lrStartTransaction(con, tran, op.holdingUID);
        if (op.party != null && op.share ==null) {  //update only party information
            op.party.partyUID=op.partyUID;
            lrm.lrAddOrUpdateParty(con, tran, op.holdingUID, op.party);

        }
        if(op.party!=null)
        {
            op.party.partyUID=op.partyUID;
        }
        if (op.share != null) {
            for (LADM.Parcel p : holding.parcels) {
                List<LADM.Right> rights = p.getHolders();
                boolean existingParty=false;
                for (LADM.Right r : rights) {
                    if(r.partyUID.equals(op.partyUID))
                    {
                        existingParty=true;
                        r.share = op.share;
                        if(op.party!=null)
                            r.party=op.party;
                        break;
                    }
                }
                if(!existingParty)
                {
                    LADM.Right newRight=new LADM.Right();
                    newRight.rightType=op.party.partyType==LADM.Party.PARTY_TYPE_STATE?LADM.Right.RIGHT_SUR:LADM.Right.RIGHT_PUR;
                    newRight.share=op.share;
                    newRight.party=op.party;
                    rights.add(newRight);
                }
                lrm.lrUpateRights(con, tran, op.holdingUID, p.parcelUID, rights);
            }
        }

        lrm.finishTransaction(con, tran,false);
        return op.party.partyUID;
    }

}
