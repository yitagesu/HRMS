/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.tran;

import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.DocumentTypeInfo;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.SourceDocument;
import com.intaps.nrlais.model.tran.ReallocationTransactionData;
import com.intaps.nrlais.model.tran.TransferTransactionData;
import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author Tewelde
 */
public class ReallocationTransactionHandler extends TransferTransactionHandler implements TransactionHandler {
    
    @Override
    protected SourceDocument[] sourceDocs(TransferTransactionData data) {
        ReallocationTransactionData tran=(ReallocationTransactionData)data;
        return new SourceDocument[]{
            tran.adminDecisionDoc,
            tran.claimResolution,
            tran.landHoldingCertificateDoc};
        
    }

    @Override
    protected int[] sourceDocTypes(TransferTransactionData data) {
       
       return new int[]{
            DocumentTypeInfo.DOC_TYPE_REALLOC_DECISION,
            DocumentTypeInfo.DOC_TYPE_DIVORCE_CLAIM_RESOLUTION,
            DocumentTypeInfo.DOC_TYPE_LAND_HOLDING_CERTIFICATE};
    }
    @Override
    public void validateTransfer(UserSession session,LADM.Holding source,LADM.Holding dest)
    {
        if(!(source.holdingType==LADM.Holding.HOLDING_TYPE_STATE ^ dest.holdingType==LADM.Holding.HOLDING_TYPE_STATE))
            throw new IllegalArgumentException(session.text("Invalid reallocation operation"));
    }
    
    
}
