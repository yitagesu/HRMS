/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.model;

/**
 *
 * @author yitagesu
 */
public class IDNameText {
    public int id;
    public String name;

    public IDNameText(int id, String name) {
        this.id=id;
        this.name=name;
    }
}
