/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.tran;

import com.intaps.nrlais.controller.ViewControllerBase;
import com.google.gson.reflect.TypeToken;
import com.intaps.nrlais.Startup;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.tran.Applicant;
import com.intaps.nrlais.model.tran.ParcelTransfer;
import com.intaps.nrlais.model.tran.Partition;
import com.intaps.nrlais.model.tran.PartyParcelShare;
import com.intaps.nrlais.util.GSONUtil;
import com.intaps.nrlais.util.RationalNum;
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.net.URLCodec;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.utils.URIUtils;

/**
 *
 * @author Tewelde
 */
public class ParcelTransferViewController extends ViewControllerBase {

    public static final int TRANSFER_TYPE_RENT = 1;
    public static final int TRANSFER_TYPE_LEASE = 2;
    public static final int TRNASFER_TYPE_GIFT = 3;

    
    public Settings settings;
    public String holdingID;
    public LADM.Holding holding;
    
    ParcelTransfer[] transfers = null;
    public static class Settings
    {
        public boolean split = false;
        public boolean transferArea=true;
        public boolean transferShare=false;
        public String transferLabel="Rent/Lease";
        public String areaLabel="Rented/Leased Area";
        public String shareLabel="Transfered Share";
        public boolean singleParcel=false;
    }
    public ParcelTransferViewController(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException, DecoderException {
        super(request, response);
        String s=request.getParameter("settings");
        if(!StringUtils.isEmpty(s))
        {
            this.settings=GSONUtil.getAdapter(Settings.class).fromJson(s);
        }
        else
            this.settings=new Settings();
        
        holdingID = request.getParameter("holding_uid");
        holding = mainFacade.getLADM("nrlais_inventory", holdingID, LADM.CONTENT_FULL).holding;
        if ("POST".equals(request.getMethod())) {
            this.transfers = Startup.readPostedArrayObject(request, new TypeToken<ParcelTransfer>() {
            }.getType());
        }
        else
        {
            transfers=new ParcelTransfer[0];
//            transfers=new ParcelTransfer[holding.parcels.size()];
//            for(int i=0;i<transfers.length;i++)
//            {
//                transfers[i]=new ParcelTransfer();                
//                transfers[i].splitParcel=false;
//                transfers[i].area=holding.parcels.get(i).areaGeom;
//                transfers[i].share=null;
//                transfers[i].parcelUID=holding.parcels.get(i).parcelUID;
//            }
        }
    }
    
    
    public String shareValue(LADM.Parcel parcel) {
        if (transfers != null) {
            for (ParcelTransfer t : transfers) {
                if (t.parcelUID.equals(parcel.parcelUID)) {
                    return t.share.toString();
                }
            }
        }
        return "";
    }

    public ParcelTransfer transfer(LADM.Parcel parcel)
    {
        if (transfers != null) {
            for (ParcelTransfer t : transfers) {
                if (t.parcelUID.equals(parcel.parcelUID)) {
                    return t;
                }
            }
        }
        return null;
    }
    public boolean transfered(LADM.Parcel p)
    {
        ParcelTransfer t=transfer(p);
        if(t==null)
            return false;
        return true;
    }
    public boolean split(LADM.Parcel p)
    {
        if (!this.settings.split) {
            return false;
        }
        ParcelTransfer t=transfer(p);
        if(t==null)
            return false;
        return t.splitParcel;
    }
    public double area(LADM.Parcel p)
    {
        if (!this.settings.transferArea) {
            return 0;
        }
        ParcelTransfer t=transfer(p);
        if(t==null)
            return 0;
        return t.area;
    }
    public RationalNum share(LADM.Parcel p)
    {
        if (!this.settings.transferArea) {
            return new RationalNum(0,1);
        }
        ParcelTransfer t=transfer(p);
        if(t==null)
            return new RationalNum(0,1);
        return t.share;
    }
}
