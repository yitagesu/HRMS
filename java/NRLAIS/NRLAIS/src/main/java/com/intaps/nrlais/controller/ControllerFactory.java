/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tewelde
 */
public class ControllerFactory<T> {
    final List<T> instances=new ArrayList<T>();
    public int addInstance(T inst)
    {
        synchronized(instances)
        {
            instances.add(inst);
            return instances.size()-1;
        }
    }
    public T getInstance(int index)
    {
        synchronized(instances)           
        {
            if(index<0 || index>=instances.size())
                throw new IllegalArgumentException("Instance "+index+" doesn't exist");
            return instances.get(index);
        }
    }
}
