/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.model;

/**
 *
 * @author Tewelde
 */
public class DocumentTypeInfo extends IDName {

    public static final int DOC_TYPE_PROOF_OF_DIVORCE = 19;
    public static final int DOC_TYPE_DIVORCE_CLAIM_RESOLUTION = 15;
    public static final int DOC_TYPE_LAND_HOLDING_CERTIFICATE = 16;
    public static final int DOC_TYPE_LETTER_OF_ATTORNEY = 17;
    public static final int DOC_TYPE_PROOF_OF_CELEBACY = 21;
    public static final int DOC_TYPE_PROOF_OF_MARRAIGE = 22;
    public static final int DOC_TYPE_PASSPORT = 31;
    public static final int DOC_TYPE_KEBELE_ID = 32;
    public static final int DOC_TYPE_RENT_AGREEMENT = 33;
    public static final int DOC_TYPE_REALLOC_DECISION = 14;
    public static final int DOC_TYPE_EXPROPRATION_DECISION = 13;
    public static final int DOC_TYPE_PROOF_OF_COMPENSATION = 23;
    public static final int DOC_TYPE_FIRST_LEVEL_CERTIFICATE = 2;

    public static final int DOC_TYPE_PROOF_OF_DEATH = 18;
    public static final int DOC_TYPE_INHERITANCE_COURT_DECISION = 25;
    public static final int DOC_TYPE_WILL = 11;
    public static final int DOC_TYPE_EX_OFFICIO_STATE_DECISION = 1;
    public static final int DOC_TYPE_ELDERS_WOREDA_RESOLUTION = 12;
    public static final int DOC_TYPE_COURT_ORDER = 3;
    public static final int DOC_TYPE_OTHER = 0;

}
