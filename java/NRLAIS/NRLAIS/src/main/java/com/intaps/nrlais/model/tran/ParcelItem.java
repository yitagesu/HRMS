/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.model.tran;

import com.intaps.nrlais.model.LADM;

/**
 *
 * @author Tewelde
 */
public class ParcelItem {
    public String existingParcelUID=null;
    public LADM.Parcel parcel=null;
    public ParcelItem()
    {
        parcel=new LADM.Parcel();
    }
    public ParcelItem(LADM.Parcel parcel) {
        this.parcel=parcel;
    }
}
