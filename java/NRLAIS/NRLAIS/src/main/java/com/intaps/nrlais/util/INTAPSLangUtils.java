/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.util;

import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Tewelde
 */
public class INTAPSLangUtils {
    public static class StringReplacement
    {
        public String find;
        public String replace;
        public StringReplacement(String f,String r)
        {
            this.find=f;
            this.replace=r;
        }
    }
    public static String replace(String template,StringReplacement[] replacements)
    {
        for(StringReplacement r:replacements)
        {
            template=StringUtils.replace(template, r.find, r.replace);
        }
        return template;
    }
    public static String appendStringList(String list,String sep,String val)
    {
        if(org.apache.commons.lang3.StringUtils.isEmpty(list))
            return val;
        return list+sep+val;
    }
}
