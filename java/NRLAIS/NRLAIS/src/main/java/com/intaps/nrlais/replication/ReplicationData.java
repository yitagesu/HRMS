/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.replication;

import java.util.List;

/**
 *
 * @author Tewelde
 */
public class ReplicationData {
    public static class Item
    {
        public long time;
        public int repNo;
        public int prevSeqNo;
        public String dml;
    }
    public String woredaID;
    public List<Item> data;
}
