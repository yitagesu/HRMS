/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.repo.lrop;

import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.tran.ManualTransactionData;
import com.intaps.nrlais.repo.GetLADM;
import com.intaps.nrlais.repo.LRManipulator;
import com.intaps.nrlais.repo.ManualLRManipulatorFacade;
import com.intaps.nrlais.util.RationalNum;
import java.io.IOException;
import java.sql.Connection;

/**
 *
 * @author Tewelde
 */
public class LrOpExecSaveRent implements ManualLRManipulatorFacade.LROPExecutor<ManualTransactionData.LrOpSaveRent> {

    @Override
    public Object doOperation(UserSession session, Connection con, LATransaction tran, ManualTransactionData.LrOpSaveRent op) throws Exception {
        LRManipulator lrm = new LRManipulator(session);
        lrm.lrStartTransaction(con, tran, op.rent.holdingUID);
        if(!(op.rent.rightType==LADM.Right.RIGHT_RENT || op.rent.rightType==LADM.Right.RIGHT_LEASE))
            throw new IllegalArgumentException("Invalid right type. It must be either rent or lease.");
        op.rent.share=new RationalNum(1,1);
        if(op.rent.rightUID==null)
        {
            op.rent.party=new GetLADM(session, "nrlais_transaction").getParty(con, op.rent.partyUID,LADM.CONTENT_FULL);
            op.rent.rightUID=lrm.lrAddRight(con, tran, op.rent.holdingUID, op.rent.parcelUID, op.rent);            
        }
        else
            lrm.lrUpdateRent(con, tran, op.rent);
        lrm.finishTransaction(con, tran,false);
        return op.rent.rightUID;
    }

}
