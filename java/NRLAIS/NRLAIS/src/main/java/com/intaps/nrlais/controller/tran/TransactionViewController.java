/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.tran;

import com.intaps.nrlais.controller.ViewControllerBase;
import com.intaps.nrlais.Startup;
import com.intaps.nrlais.model.CMSSTask;
import com.intaps.nrlais.model.IDName;
import com.intaps.nrlais.model.KebeleInfo;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.TransactionFlow;
import com.intaps.nrlais.model.WoredaInfo;
import com.intaps.nrlais.model.tran.Applicant;
import com.intaps.nrlais.model.tran.CertificateReplacementTransactionData;
import com.intaps.nrlais.model.tran.BoundaryCorrectionTransactionData;
import com.intaps.nrlais.model.tran.ConsolidationTransactionData;
import com.intaps.nrlais.model.tran.DataMigrationTransactionData;
import com.intaps.nrlais.model.tran.DivorceTransactionData;
import com.intaps.nrlais.model.tran.ExOfficioTransactionData;
import com.intaps.nrlais.model.tran.ExchangeTransactionData;
import com.intaps.nrlais.model.tran.ExpropriationTransactionData;
import com.intaps.nrlais.model.tran.GiftTransactionData;
import com.intaps.nrlais.model.tran.InheritanceTransactionData;
import com.intaps.nrlais.model.tran.InheritanceWithOutWillTransactionData;
import com.intaps.nrlais.model.tran.InheritanceWithWillTransactionData;
import com.intaps.nrlais.model.tran.ReallocationTransactionData;
import com.intaps.nrlais.model.tran.RentTransactionData;
import com.intaps.nrlais.model.tran.RestrictiveInterestTransactionData;
import com.intaps.nrlais.model.tran.ServitudeTransactionData;
import com.intaps.nrlais.model.tran.SimpleCorrectionData;
import com.intaps.nrlais.model.tran.SpecialCaseTransactionData;
import com.intaps.nrlais.model.tran.SplitHoldingTransactionData;
import com.intaps.nrlais.repo.TransactionFacade;
import com.intaps.nrlais.util.GSONUtil;
import com.intaps.nrlais.util.INTAPSLangUtils;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.codec.net.URLCodec;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Tewelde
 */
public class TransactionViewController<TranDataType extends LATransaction.TransactionData> extends ViewControllerBase {

    public static class CertificateLink {

        public String url;
        public String text;
        public String upid;
        public String owner;
    }

    public static class AddDeleteItem {

        public String holdingUID;
        public String label;
        public String dataUrl;
        private String tranUID;
    }

    public static class UpdateItem {

        public String holdingUID;
        public String label;
        public String oldDataUrl;
        public String newDataUrl;
        private String tranUID;
    }

    public static class ChangeSummary {

        public List<AddDeleteItem> deletedHoldings = new ArrayList<AddDeleteItem>();
        public List<UpdateItem> updatedHoldings = new ArrayList<>();
        public List<AddDeleteItem> newHoldings = new ArrayList<>();
    }
    static class Settings
    {
        public boolean noManipulate=false;
    }
    public static TransactionViewController createController(HttpServletRequest request, HttpServletResponse response, boolean loadData) throws Exception {
        return createController(Integer.parseInt(request.getParameter("tran_type")), request, response, loadData);
    }

    public List<LATransaction.TranactionFlow> workFlowItems() throws SQLException {
        return mainFacade.getTransactionFlow(this.tran.transactionUID);
    }

    public String oldStatus(LATransaction.TranactionFlow flow) {
        return LATransaction.getStatusString(flow.oldState);
    }

    public String newStatus(LATransaction.TranactionFlow flow) {
        return LATransaction.getStatusString(flow.newState);
    }

    public static TransactionViewController createController(int transactionType, HttpServletRequest request, HttpServletResponse response, boolean loadData) throws Exception {
        switch (transactionType) {
            case LATransaction.TRAN_TYPE_DIVORCE:
                return new DivorceViewController(request, response, loadData);
            case LATransaction.TRAN_TYPE_RENT:
                return new RentViewController(request, response, loadData);
            case LATransaction.TRAN_TYPE_GIFT:
                return new GiftViewController(request, response, loadData);
            case LATransaction.TRAN_TYPE_SPLIT:
                return new SplitViewController(request, response, loadData);
            case LATransaction.TRAN_TYPE_INHERITANCE:
                return new InheritanceWithOutWillViewController(request, response, loadData);
            case LATransaction.TRAN_TYPE_INHERITANCE_WITHWILL:
                return new InheritanceWithWillViewController(request, response, loadData);
            case LATransaction.TRAN_TYPE_REALLOCATION:
                return new ReallocationViewController(request, response, loadData);
            case LATransaction.TRAN_TYPE_REPLACE_CERTIFICATE:
                return new CertificateReplacementViewController(request, response, loadData);
            case LATransaction.TRAN_TYPE_EXPROPRIATION:
                return new ExpropriationViewController(request, response, loadData);
            case LATransaction.TRAN_TYPE_BOUNDARY_CORRECTION:
                return new BoundaryCorrectionViewController(request, response, loadData);
            case LATransaction.TRAN_TYPE_SPECIAL_CASE:
                return new SpecialCaseViewController(request, response, loadData);
            case LATransaction.TRAN_TYPE_CONSOLIDATION:
                return new ConsolidationViewController(request, response, loadData);
            case LATransaction.TRAN_TYPE_EX_OFFICIO:
                return new ExOfficioViewController(request, response, loadData);
            case LATransaction.TRAN_TYPE_EXCHANGE:
                return new ExchangeViewController(request, response, loadData);
            case LATransaction.TRAN_TYPE_RESTRICTIVE_INTEREST:
                return new RestrictiveInterestViewController(request, response, loadData);
            case LATransaction.TRAN_TYPE_SERVITUDE:
                return new ServitudeViewController(request, response, loadData);
            case LATransaction.TRAN_TYPE_DATA_MIGRATION:
                return new TransactionViewController(request, response, DataMigrationTransactionData.class, LATransaction.TRAN_TYPE_DATA_MIGRATION, true);
            case LATransaction.TRAN_TYPE_SIMPLE_CORRECTION:
                return new SimpleCorrectionViewController(request, response, loadData);
            default:
                throw new UnsupportedOperationException("Controller for transaction type:" + transactionType + " not implemented");
        }
    }

    public static int javaTypeToTranscationType(Class c) {
        if (c == DivorceTransactionData.class) {
            return LATransaction.TRAN_TYPE_DIVORCE;
        }
        if (c == RentTransactionData.class) {
            return LATransaction.TRAN_TYPE_RENT;
        }
        if (c == GiftTransactionData.class) {
            return LATransaction.TRAN_TYPE_GIFT;
        }
        if (c == InheritanceWithWillTransactionData.class) {
            return LATransaction.TRAN_TYPE_INHERITANCE_WITHWILL;
        }
        if (c == InheritanceWithOutWillTransactionData.class) {
            return LATransaction.TRAN_TYPE_INHERITANCE;
        }
        if (c == ReallocationTransactionData.class) {
            return LATransaction.TRAN_TYPE_REALLOCATION;
        }
        if (c == ExpropriationTransactionData.class) {
            return LATransaction.TRAN_TYPE_EXPROPRIATION;
        }
        if (c == BoundaryCorrectionTransactionData.class) {
            return LATransaction.TRAN_TYPE_BOUNDARY_CORRECTION;
        }
        if (c == SpecialCaseTransactionData.class) {
            return LATransaction.TRAN_TYPE_SPECIAL_CASE;
        }
        if (c == ConsolidationTransactionData.class) {
            return LATransaction.TRAN_TYPE_CONSOLIDATION;
        }
        if (c == ExOfficioTransactionData.class) {
            return LATransaction.TRAN_TYPE_EX_OFFICIO;
        }
        if (c == ExchangeTransactionData.class) {
            return LATransaction.TRAN_TYPE_EXCHANGE;
        }
        if (c == CertificateReplacementTransactionData.class) {
            return LATransaction.TRAN_TYPE_REPLACE_CERTIFICATE;
        }
        if (c == RestrictiveInterestTransactionData.class) {
            return LATransaction.TRAN_TYPE_RESTRICTIVE_INTEREST;
        }
        if (c == ServitudeTransactionData.class) {
            return LATransaction.TRAN_TYPE_SERVITUDE;
        }
        if (c == SimpleCorrectionData.class) {
            return LATransaction.TRAN_TYPE_SIMPLE_CORRECTION;
        }
        if (c == SplitHoldingTransactionData.class) {
            return LATransaction.TRAN_TYPE_SPLIT;
        }
        return -1;
    }

    public static TransactionViewController createController(HttpServletRequest request, HttpServletResponse response, Class c, boolean loadData) throws Exception {
        return createController(javaTypeToTranscationType(c), request, response, loadData);
    }

    public LATransaction tran;
    public TranDataType data;
    public IDName tranType = null;
    public KebeleInfo kebele = null;
    public WoredaInfo woreda = null;
    Class<TranDataType> c;
    Settings setting;
    public TransactionViewController(HttpServletRequest request, HttpServletResponse response, Class<TranDataType> c, int tranTypeID) throws Exception {
        this(request, response, c, tranTypeID, true);
    }

    public TransactionViewController(HttpServletRequest request, HttpServletResponse response, Class<TranDataType> c, int tranTypeID, boolean loadData) throws Exception {
        super(request, response);
        String s = request.getParameter("setting");
        if (StringUtils.isEmpty(s)) {
            this.setting = new TransactionViewController.Settings();
        } else {
            this.setting = GSONUtil.getAdapter(TransactionViewController.Settings.class).fromJson(s);
        }
        
        this.c = c;
        if (loadData) {
            if (request.getMethod().equals("POST"))//preview
            {
                LATransaction d = Startup.readPostedObject(request, LATransaction.class);
                tran = d;
                this.data = (TranDataType) d.data;
            } else//from db
            {

                String txuid = request.getParameter("tran_uid");
                if (!StringUtils.isEmpty(txuid)) {
                    tran = mainFacade.getTransaction(txuid, true);
                    if (tran == null) {
                        throw new IllegalArgumentException("Transaction " + txuid + " doesn't exist");
                    }
                    if (tran.transactionType != tranTypeID) {
                        throw new IllegalArgumentException("Transaction " + txuid + " is invalid transaction type");
                    }
                    if (tran.data == null) {
                        tran.data = c.newInstance();
                    }
                } else {
                    tran = new LATransaction();
                    tran.transactionType = tranTypeID;
                    tran.data = c.newInstance();
                }
                data = (TranDataType) tran.data;
            }
            tranType = mainFacade.getLookup("nrlais_sys.t_cl_transactiontype", tran.transactionType);
            this.kebele = mainFacade.getKebele(tran.nrlais_kebeleid);
            this.woreda = mainFacade.getWoreda(tran.nrlais_woredaid);
        }
    }

    public String saveData(LATransaction tran) throws Exception {
        boolean saveInitial = "true".equals(this.request.getParameter("initial"));
        return new TransactionFacade(Startup.getSessionByRequest(this.request)).saveTransaction(tran, !saveInitial);
    }

    public String tranTypeText() throws SQLException {
        return mainFacade.getLookup("nrlais_sys.t_cl_transactiontype", this.tran.transactionType).name.text(session.lang());
    }

    public boolean showEdit() {
        if (tran.status != LATransaction.TRAN_STATUS_INITIATED) {
            return false;
        }
        return session.worlaisSession.isOffier();
    }

    public boolean editExOfficeTransaction() {
        if(setting.noManipulate)
            return false;
        if (tran.status != LATransaction.TRAN_STATUS_REGISTERD) {
            return false;
        }
        if (tran.transactionType != LATransaction.TRAN_TYPE_EX_OFFICIO
                && tran.transactionType != LATransaction.TRAN_TYPE_SIMPLE_CORRECTION
                && tran.transactionType != LATransaction.TRAN_TYPE_GIFT
                ) {
            return false;
        } else {
            return session.worlaisSession.isExpert();
        }
    }

    public boolean showReject() throws SQLException {
        switch (tran.status) {
            case LATransaction.TRAN_STATUS_INITIATED:
                if (!session.worlaisSession.isOffier()) {
                    return false;
                }
                return true;
            case LATransaction.TRAN_STATUS_REGISTERD:
                if (!session.worlaisSession.isExpert()) {
                    return false;
                }
                return true;
            case LATransaction.TRAN_STATUS_ACCEPT_REQUESTED:
                if (!session.worlaisSession.isSupper()) {
                    return false;
                }
                return true;
            case LATransaction.TRAN_STATUS_CANCEL_REQUESTED:
                if (!session.worlaisSession.isSupper()) {
                    return false;
                }
                return true;

        };
        return false;
    }

    public boolean showApprove() throws SQLException {
        switch (tran.status) {
            case LATransaction.TRAN_STATUS_REGISTERD:
                if (!session.worlaisSession.isExpert()) {
                    return false;
                }
                return mainFacade.isTransctionCMSSTaskDone(this.tran.transactionUID);
            case LATransaction.TRAN_STATUS_ACCEPT_REQUESTED:
                if (!session.worlaisSession.isSupper()) {
                    return false;
                }
                return true;
            case LATransaction.TRAN_STATUS_CANCEL_REQUESTED:
                if (!session.worlaisSession.isSupper()) {
                    return false;
                }
                return true;
            case LATransaction.TRAN_STATUS_ACCEPTED:
                if (!session.worlaisSession.isOffier()) {
                    return false;
                }
                return true;

        };
        return false;
    }

    public String rejectText() {
        switch (tran.status) {
            case LATransaction.TRAN_STATUS_INITIATED:
            case LATransaction.TRAN_STATUS_REGISTERD:
                return "Cancel Transaction";
            case LATransaction.TRAN_STATUS_ACCEPT_REQUESTED:
                return "Reject Transaction";
            case LATransaction.TRAN_STATUS_CANCEL_REQUESTED:
                return "Restart Application";
        };
        return "Reject";
    }

    public String approveText() {
        switch (tran.status) {
            case LATransaction.TRAN_STATUS_REGISTERD:
                return "Send to Supervior";
            case LATransaction.TRAN_STATUS_ACCEPT_REQUESTED:
                return "Accept Transaction";
            case LATransaction.TRAN_STATUS_CANCEL_REQUESTED:
                return "Cancel Transaction";
            case LATransaction.TRAN_STATUS_ACCEPTED:
                return "Close Transaction";
        };
        return "Reject";
    }

    public String rejectCommand() {
        switch (tran.status) {
            case LATransaction.TRAN_STATUS_INITIATED:
            case LATransaction.TRAN_STATUS_REGISTERD:
                return "requestAbortion";
            case LATransaction.TRAN_STATUS_ACCEPT_REQUESTED:
                return "rejectApproval";
            case LATransaction.TRAN_STATUS_CANCEL_REQUESTED:
                return "rejectAbortion";
        };
        return "Reject";
    }

    public String approveCommand() {
        switch (tran.status) {
            
            case LATransaction.TRAN_STATUS_REGISTERD:
                return "requestApproval";
            case LATransaction.TRAN_STATUS_ACCEPT_REQUESTED:
                return "acceptApproval";                
            case LATransaction.TRAN_STATUS_CANCEL_REQUESTED:
                return "acceptAbortion";
            case LATransaction.TRAN_STATUS_ACCEPTED:
                return "issueTitle";
        };
        return "Reject";
    }

    public boolean requiresCertificate() {
        return true;
    }

    public boolean showIssueCertifcate() {
        if (tran.status == LATransaction.TRAN_STATUS_ACCEPTED) {
            return requiresCertificate();
        }
        return false;
    }

    public boolean showClose() {
        if (tran.status == LATransaction.TRAN_STATUS_ACCEPTED) {
            return true;
        }
        return false;
    }

    public boolean showApproveCancel() {
        if (tran.status != LATransaction.TRAN_STATUS_CANCEL_REQUESTED) {
            return false;
        }
        if (!session.worlaisSession.isSupper()) {
            return false;
        }
        return true;
    }

    public boolean showRejectCancel() {
        return showApproveCancel();
    }

    public String statusMessage() throws SQLException {
        String message = LATransaction.getStatusString(this.tran.status);
        if (this.tran.status == LATransaction.TRAN_STATUS_REGISTERD) {
            List<CMSSTask> tasks = mainFacade.getTaskforTransaction(this.tran.transactionUID, false);
            if (tasks.size() > 0) {
                int ncomplete = 0;
                for (CMSSTask t : tasks) {
                    if (t.status == CMSSTask.TASK_STATUS_COMMITED) {
                        ncomplete++;
                    }
                }
                if (ncomplete == tasks.size()) {
                    message += " " + (ncomplete > 1 ? "All " + ncomplete + " CMSS tasks complete." : "The CMSS task is complete.");
                } else {
                    message += " " + (tasks.size() > 1 ? ncomplete + " of " + tasks.size() + " CMSS tasks complete." : "The CMSS task is not complete.");
                }
            }
        }
        return message;
    }

    public void processStateChangeCommand(String txuid, String cmd, String note) throws SQLException, Exception {
        mainFacade.changeTransactionState(txuid, cmd, note);
    }

    public List<TransactionViewController.CertificateLink> certficateLinks() throws SQLException, IOException {
        List<TransactionViewController.CertificateLink> ret = new ArrayList<>();
        ChangeSummary sum = this.summary();
        for (AddDeleteItem item : sum.newHoldings) {
            LADM ladm = mainFacade.getLADMByTx(item.tranUID, item.holdingUID);
            for (LADM.Parcel l : ladm.holding.parcels) {
                CertificateLink link = new CertificateLink();
                if (l == null || l.rights.size() == 0) {
                    continue;
                }
                for (LADM.Right r : l.getHolders()) {
                    link.owner = INTAPSLangUtils.appendStringList(link.owner, ", ", r.party.getFullName());
                }
                link.url = "javascript:transcation_showCertiricate('" + tran.transactionUID + "','" + l.rights.get(0).holdingUID + "','" + l.parcelUID + "')";
                link.upid = l.upid;
                ret.add(link);
            }
        }
        for (UpdateItem item : sum.updatedHoldings) {
            LADM before = mainFacade.getFromHistoryByArchiveTx(item.tranUID, item.holdingUID);
            LADM after = mainFacade.getByTx(item.tranUID, item.holdingUID);
            HashMap<String, LADM.Parcel> beforeParcels = new HashMap<>();
            before.holding.parcels.forEach((LADM.Parcel p) -> {
                beforeParcels.put(p.parcelUID, p);
            });

            boolean holderChanged = !before.holding.hasSameHolderAs(after.holding);
            for (LADM.Parcel p : after.holding.parcels) {
                boolean addLink;
                if (holderChanged) {
                    addLink = true;
                } else {
                    if (beforeParcels.containsKey(p.parcelUID)) {
                        addLink = !beforeParcels.get(p.parcelUID).geometry.equals(p.geometry);
                    } else {
                        addLink = true;
                    }
                }
                if (addLink) {
                    CertificateLink link = new CertificateLink();
                    if (p == null || p.rights.size() == 0) {
                        continue;
                    }
                    for (LADM.Right r : p.getHolders()) {
                        link.owner = INTAPSLangUtils.appendStringList(link.owner, ", ", r.party.getFullName());
                    }
                    link.url = "javascript:transcation_showCertiricate('" + tran.transactionUID + "','" + p.rights.get(0).holdingUID + "','" + p.parcelUID + "')";
                    link.upid = p.upid;
                    ret.add(link);
                }
            }

        }
        return ret;
    }

    public boolean certficateSection() throws SQLException, IOException {
        return this.tran.status == LATransaction.TRAN_STATUS_ACCEPTED && certficateLinks().size() > 0;
    }
    ChangeSummary summary = null;

    public boolean summarySection() {
        return tran.status != LATransaction.TRAN_STATUS_INITIATED;
    }

    public ChangeSummary summary() throws SQLException {
        if (summary == null) {
            summary = new ChangeSummary();
            List<String> input = mainFacade.getTransactionInputHoldings(tran.transactionUID);
            List<String> output = mainFacade.getTransactionOutputHoldings(tran.transactionUID);
            HashSet<String> inputSet = new HashSet<>();
            inputSet.addAll(input);
            HashSet<String> outputSet = new HashSet<>();
            outputSet.addAll(output);

            if (tran.isCommited()) {
                for (String ip : input) {
                    if (output.contains(ip)) {
                        UpdateItem updated = new UpdateItem();
                        updated.label = "Updated";
                        updated.holdingUID = ip;
                        updated.tranUID = tran.transactionUID;
                        updated.oldDataUrl = "/holding/min_holding_search_result.jsp?schema=nrlais_historic&holding_uid=" + ip + "&tran_uid=" + tran.transactionUID;
                        updated.newDataUrl = "/holding/min_holding_search_result.jsp?holding_uid=" + ip + "&tran_uid=" + tran.transactionUID;
                        summary.updatedHoldings.add(updated);
                    } else {
                        AddDeleteItem deleted = new AddDeleteItem();
                        deleted.holdingUID = ip;
                        deleted.tranUID = tran.transactionUID;
                        deleted.label = "Deleted";
                        deleted.dataUrl = "/holding/min_holding_search_result.jsp?schema=nrlais_historic&holding_uid=" + ip + "&tran_uid=" + tran.transactionUID;
                        summary.deletedHoldings.add(deleted);
                    }
                }
                for (String op : output) {
                    if (!inputSet.contains(op)) {
                        AddDeleteItem created = new AddDeleteItem();
                        created.holdingUID = op;
                        created.label = "Created";
                        created.tranUID = tran.transactionUID;
                        created.dataUrl = "/holding/min_holding_search_result.jsp?holding_uid=" + op + "&tran_uid=" + tran.transactionUID;
                        summary.newHoldings.add(created);
                    }
                }
            } else {
                for (String ip : input) {
                    if (output.contains(ip)) {
                        UpdateItem updated = new UpdateItem();
                        updated.holdingUID = ip;
                        updated.tranUID = tran.transactionUID;
                        updated.label = "Updated";
                        updated.oldDataUrl = "/holding/min_holding_search_result.jsp?holding_uid=" + ip;
                        updated.newDataUrl = "/holding/min_holding_search_result.jsp?schema=nrlais_transaction&holding_uid=" + ip;
                        summary.updatedHoldings.add(updated);
                    } else {
                        AddDeleteItem deleted = new AddDeleteItem();
                        deleted.holdingUID = ip;
                        deleted.label = "Deleted";
                        deleted.dataUrl = "/holding/min_holding_search_result.jsp?holding_uid=" + ip;
                        deleted.tranUID = tran.transactionUID;
                        summary.deletedHoldings.add(deleted);
                    }
                }
                for (String op : output) {
                    if (!inputSet.contains(op)) {
                        AddDeleteItem created = new AddDeleteItem();
                        created.holdingUID = op;
                        created.tranUID = tran.transactionUID;
                        created.label = "Created";
                        created.dataUrl = "/holding/min_holding_search_result.jsp?schema=nrlais_transaction&holding_uid=" + op;
                        summary.newHoldings.add(created);
                    }
                }
            }
        }
        return summary;
    }
}
