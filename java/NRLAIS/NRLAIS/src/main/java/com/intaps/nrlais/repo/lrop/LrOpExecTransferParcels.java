/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.repo.lrop;

import com.intaps.nrlais.UserSession;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.LATransaction;
import com.intaps.nrlais.model.tran.ManualTransactionData;
import com.intaps.nrlais.repo.GetLADM;
import com.intaps.nrlais.repo.LRManipulator;
import com.intaps.nrlais.repo.ManualLRManipulatorFacade;
import java.io.IOException;
import java.sql.Connection;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Tewelde
 */
public class LrOpExecTransferParcels implements ManualLRManipulatorFacade.LROPExecutor<ManualTransactionData.LrOpTransferParcels> {

    @Override
    public Object doOperation(UserSession session, Connection con, LATransaction tran, ManualTransactionData.LrOpTransferParcels op) throws Exception {
        LRManipulator lrm = new LRManipulator(session);
        GetLADM ladm = new GetLADM(session, "nrlais_transaction");

        LADM.Holding holding = ladm.get(con, op.sourceHolding, LADM.CONTENT_FULL).holding;
        LADM.Holding destHolding = ladm.get(con, op.destinationHolding, LADM.CONTENT_FULL).holding;
        List<String> holdings = new ArrayList<>();
        holdings.add(op.sourceHolding);
        holdings.add(op.destinationHolding);
        List<LADM.Right> destHolders=destHolding.getHolders();
        lrm.lrStartTransaction(con, tran, holdings);
        for (String parcelUID : op.parcels) {
            LADM.Parcel parcel = holding.getParcel(parcelUID);
            if (parcel == null) {
                throw new IllegalArgumentException("Parcel " + parcelUID + " not found in holding " + holding.holdingUID);
            }
            lrm.lrTransferParcel(con,
                    tran,
                    parcelUID,
                    op.sourceHolding,
                    op.destinationHolding,
                    parcel.mreg_actype,
                    parcel.mreg_acyear,destHolders
            );
        }
        lrm.finishTransaction(con, tran,false);
        return null;
    }
}
