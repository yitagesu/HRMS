/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.model.tran;

import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.SourceDocument;
import com.intaps.nrlais.util.RationalNum;

/**
 *
 * @author Tewelde
 */
public class PartyItem {
    public String existingPartyUID=null;
    public LADM.Party party=null;
    public RationalNum share=null;
    public SourceDocument idDoc=null;
    public SourceDocument letterOfAttorneyDocument=null;
    public SourceDocument proofOfMaritalStatus=null;
    public LADM.Party representative=null;
    public LADM.Right right;
    public LADM.Restriction restriction;
    public PartyItem(LADM.Party party) {
        this.party = party;
    }
    public PartyItem()
    {
        
    }

}
