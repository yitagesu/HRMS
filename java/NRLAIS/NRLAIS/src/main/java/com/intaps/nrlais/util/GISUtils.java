/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.util;

import com.intaps.nrlais.model.map.MapperEngineModel;
import com.vividsolutions.jts.geom.Geometry;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import org.postgis.LinearRing;
import org.postgis.MultiPolygon;
import org.postgis.PGgeometry;
import org.postgis.Point;
import org.postgis.Polygon;

/**
 *
 * @author Teweldemedhin Aberra
 */
public class GISUtils {

    public static MapperEngineModel.MEMultiPointSeries toMEPolygon(PGgeometry g) {
        MapperEngineModel.MEMultiPointSeries ret = new MapperEngineModel.MEMultiPointSeries();
        if (g == null) {
            return ret;
        }
        if (g.getGeometry() instanceof Polygon) {

            Polygon pol = (Polygon) g.getGeometry();
            if (pol.numRings() > 0) {
                int nr = pol.numRings();
                for (int j = 0; j < nr; j++) {
                    LinearRing rng = pol.getRing(j);
                    int n = rng.numPoints();
                    if (n > 3) {
                        MapperEngineModel.MEPointSeries ps = new MapperEngineModel.MEPointSeries();
                        ps.polygon = true;
                        for (int i = 0; i < n - 1; i++) {
                            Point p = rng.getPoint(i);
                            ps.points.add(new MapperEngineModel.MEPoint(p.x, p.y));
                        }
                        ret.rings.add(ps);
                    }
                }
            }
        } else if (g.getGeometry() instanceof MultiPolygon) {

            MultiPolygon mult = (MultiPolygon) g.getGeometry();
            for (Polygon pol : mult.getPolygons()) {
                if (pol.numRings() > 0) {
                    int nr = pol.numRings();
                    for (int j = 0; j < nr; j++) {
                        LinearRing rng = pol.getRing(j);
                        int n = rng.numPoints();
                        if (n > 3) {
                            MapperEngineModel.MEPointSeries ps = new MapperEngineModel.MEPointSeries();
                            ps.polygon = true;
                            for (int i = 0; i < n - 1; i++) {
                                Point p = rng.getPoint(i);
                                ps.points.add(new MapperEngineModel.MEPoint(p.x, p.y));
                            }
                            ret.rings.add(ps);
                        }
                    }
                }
            }
        }
        return ret;
    }

    public static PGgeometry toPGGeometry(com.vividsolutions.jts.geom.Geometry geom) throws SQLException {
        PGgeometry ret = new PGgeometry();
        ret = new PGgeometry(geom.toText());
        return ret;
    }

    public static PGgeometry toPGGeometry(MapperEngineModel.MEMultiPointSeries mps) {
        LinearRing[] rings = new LinearRing[mps.rings.size()];
        int i = 0;
        for (MapperEngineModel.MEPointSeries s : mps.rings) {
            Point[] ps = new Point[s.points.size() + 1];
            for (int j = 0; j < s.points.size(); j++) {
                ps[j] = new Point(s.points.get(j).getX(), s.points.get(j).getY());
            }
            ps[s.points.size()] = new Point(s.points.get(0).getX(), s.points.get(0).getY());
            LinearRing lr = new LinearRing(ps);
            rings[i++] = lr;
        }
        return new PGgeometry(new MultiPolygon(new Polygon[]{ new Polygon(rings)}));
    }
}
