/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intaps.nrlais.controller.tran;

import com.intaps.nrlais.model.tran.PartyItem;
import com.google.gson.reflect.TypeToken;
import com.intaps.nrlais.Startup;
import com.intaps.nrlais.controller.ViewControllerBase;
import com.intaps.nrlais.model.LADM;
import com.intaps.nrlais.model.tran.ParcelItem;
import com.intaps.nrlais.util.GSONUtil;
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.net.URLCodec;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Tewelde
 */
public class ParcelListViewController extends ViewControllerBase{
    
    public static class Settings
    {
        public String containerEl="";
        public boolean allowExisting=true;
        public boolean allowNew=false;
        public boolean idDoc=false;
        public boolean edit=false;
        public boolean delete=false;
        public boolean split=false;
        public boolean select=false;
        public boolean mapLink=false;
    }
    public Settings setting;
    ParcelItem[] parcelItems=null;
    public ParcelListViewController(HttpServletRequest request, HttpServletResponse response) throws DecoderException, IOException {
        super(request, response);
        String s=request.getParameter("setting");
        if(StringUtils.isEmpty(s))
            this.setting=new Settings();
        else
            this.setting=GSONUtil.getAdapter(Settings.class).fromJson(s);
        
        if ("POST".equals(request.getMethod())) {
            this.parcelItems = Startup.readPostedArrayObject(request, new TypeToken<PartyItem>(){}.getType());
        }
        else
        {
            this.parcelItems=new ParcelItem[0];
        }

    }
    
    public ParcelItem[] items()
    {
        return this.parcelItems;
    }
    public String itemSeqNo(ParcelItem item)
    {
        if(item==null || item.parcel==null)
            return "";
        return LADM.Parcel.formatParcelSeqNo(item.parcel.seqNo);
    }
    public String itemArea(ParcelItem item) throws SQLException
    {
        if(item==null || item.parcel==null)
            return "";
        
        return Double.toString(item.parcel.areaGeom);
    }
    public String itemLandUse(ParcelItem item) throws SQLException
    {
        if(item==null || item.parcel==null)
            return "";
        return super.lookupText("nrlais_sys.t_cl_landusetype",item.parcel.landUse);
    }
    public String itemAcqusitionType(ParcelItem item) throws SQLException{
        if(item==null || item.parcel==null)
            return "";
        return super.lookupText("nrlais_sys.t_cl_acquisitiontype",item.parcel.mreg_actype);
    }
    public String itemAcqusitionYear(ParcelItem item){
        if(item==null || item.parcel==null)
            return "";
        return Integer.toString(item.parcel.mreg_acyear);
    }
}
