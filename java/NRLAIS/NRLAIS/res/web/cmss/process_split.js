
var split_state = 1;//0:initial, 1:waiting split 2: assignment 3: confirmation
var split_task=null;// =<%=GSONUtil.toJson(task)%>;

var split_new_parcels=null;// =<%=GSONUtil.toJson(newParcels)%>;
var split_current_parcel_index = -1;
var split_selected = [];
var split_final_data = {};
function splitInit(taskUID)
{
    $.ajax({url:'/api/split_task?task_uid='+taskUID+ "&session_id=" + sessionid,
        dataType:'json',
        success:function(data)
        {
            split_task=data.task;
            split_new_parcels=data.newParcels;
            split_state = 1;
            split_selected = [];
        }
    });

    layerChanged = function (type,id,count)
    {
        
        if (split_state != 1)
            return;
        if (count == split_task.newParcelUIDs.length)
        {
            gotoSplitParcelAssignment();
        }
    };
    featureSelected = function (id)
    {
        if (split_state != 2)
            return;
        if (split_selected.indexOf(id) != -1)
        {
            alert('Parcel already selected. Try again');
            setSelectionTool();
            return;
        }
        split_selected.push(id);
        if (split_selected.length == split_task.newParcelUIDs.length - 1)
        {
            getGeomData(function (data)
            {
                var as = {
                    task_uid: split_task.taskUID,
                    assignments: []
                };
                var labels = [];
                for (var i = 0; i < data.length; i++)
                {
                    var index = split_selected.indexOf(data[i].id);
                    if (index == -1)
                        index = split_task.newParcelUIDs.length - 1;
                    var puid = split_task.newParcelUIDs[index];
                    as.assignments.push(
                            {
                                geom: 'SRID=20137;' + data[i].wkt,
                                parcel_uid: puid
                            });
                    $("#split_area_" + puid).text(data[i].area);
                    labels.push({id: data[i].id, label: split_new_parcels[index].upid});
                }
                labelGeometry(labels);
                split_final_data = as;
                split_state = 3;
                split_showReviewPage();
            });
        } else
        {
            split_showParcelPage(split_current_parcel_index+1);
        }
    };
}

function split_showParcelPage(index)
{
    if (split_current_parcel_index != -1)
    {
        $('#split_assign_' + split_task.newParcelUIDs[split_current_parcel_index]).hide();
    }
    $('#split_assign_' + split_task.newParcelUIDs[index]).show();
    
    split_current_parcel_index = index;
    setSelectionTool();

    $('#split_buttons').show();
    $('#split_button_cancel').show();
    $('#split_button_commit').hide();
}
function gotoSplitParcelAssignment()
{
    split_state = 2;
    $('#split_wait_for_split').hide();
    $('#split_assign_parcel').show();
    $('#split_buttons').show();
    $('#split_button_cancel').show();
    $('#split_button_commit').hide();
    split_current_parcel_index=-1;
    split_showParcelPage(0);
}
function split_showReviewPage()
{
    $('#split_assign_parcel').hide();
    $('#split_buttons').show();
    $('#split_button_cancel').show();
    $('#split_button_commit').show();
    $('#split_wait_for_split').show();
    $('#split_title_first').hide();
    $('#split_title_final').show();
}
function split_cancel()
{
    split_state = 0;
    unloadTask();
    openHomePage();
}
function split_comit()
{
    $.ajax({
        url: '/api/taskgeom?task_uid=' + split_task.taskUID + "&session_id=" + sessionid,
        method: 'POST',
        data: JSON.stringify(split_final_data),
        contentType: 'application/json',
        dataType: 'json',
        success: function (res)
        {
            if (res.error)
            {
                alert('Error trying to save change.\n' + res.error);
            } else
            {
                alert('succesfully saved');
                split_state = 0;
                unloadTask();
                openHomePage();
            }
        },
        error: function (err)
        {
            alert('failed to save geometry to the database');
        }
    });

}