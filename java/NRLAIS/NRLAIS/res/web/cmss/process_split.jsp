<%@page import="com.intaps.nrlais.repo.*"%>
<%@page import="com.intaps.nrlais.*"%>
<%@page import="com.intaps.nrlais.util.*"%>
<%@page import="com.intaps.nrlais.controller.tran.*"%>
<%@page import="com.intaps.nrlais.model.*"%>
<%@page import="java.util.*"%>
<%@page pageEncoding="UTF-8" %>
<%
    CMSSParcelSplitViewController controller=new CMSSParcelSplitViewController(request,response);
%>
<h4>Original Parcel</h4>
<hr>
<ul>
    <li><label>Unique Parcel ID: </label> <%=controller.original.upid%></li>
    <li><label>Area: </label> <%=controller.original.areaGeom%></li>
</ul>
<div id="split_wait_for_split">
    <h4 id="split_title_first"><label class="faa-flash animated">Split the parcel in to <%=controller.newParcels.size()%> parts</label></h4>
    <h4 id="split_title_final" style="display:none"><label class="faa-flash animated">Review changes and confirm</label></h4>
    <table class="table table-striped cmss_table bulk_action">
        <thead>
            <tr>
                <td>UPID</td>
                <td>Owner</td>
                <td>Area</td>
            </tr>
        </thead>
        <tbody>
            <% 
            for(LADM.Parcel p:controller.newParcels){%>

            <tr>
                <td><%=p.upid%></td>
                <td><%=controller.parcelOwner(p)%></td>
                <td id="split_area_<%=p.parcelUID%>" class="align-right"></td>
            </tr>

            <%}%>
        </tbody>
    </table>
</div>
<div id="split_assign_parcel" style='display:none'>
    <%for(int j=0;j<controller.task.newParcelUIDs.size()-1;j++){
    %>
    <div id="split_assign_<%=controller.task.newParcelUIDs.get(j)%>" style='display:none'>
        <h4 id="assignParcel"><label class="faa-flash animated">Click on the parcel that will get the UPID: <br> <%=controller.newParcels.get(j).upid%></label></h4>
        <label>Owner:</label> <%=controller.parcelOwner(controller.newParcels.get(j))%>
    </div>
    <%}%>
</div>
<div id="split_buttons" class="align-right">
    <input id="split_button_commit" style="display:none" class="btn btn-primary" type="button" value="Commit" onclick="split_comit()" />
    <input id="split_button_cancel" type="button" class="btn btn-default" value="Cancel" onclick="split_cancel()" />
</div>
