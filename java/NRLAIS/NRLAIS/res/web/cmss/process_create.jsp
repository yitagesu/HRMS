<%@page import="com.intaps.nrlais.repo.*"%>
<%@page import="com.intaps.nrlais.*"%>
<%@page import="com.intaps.nrlais.model.*"%>
<%@page import="com.intaps.nrlais.model.tran.*"%>
<%@page import="com.intaps.nrlais.util.*"%>
<%@page import="com.intaps.nrlais.controller.*"%>
<%@page import="com.intaps.nrlais.controller.tran.*"%>
<%@page import="java.util.*"%>
<%@page pageEncoding="UTF-8" %>
<%
    CMSSCreateParcelViewController controller=new CMSSCreateParcelViewController(request,response);
    
%>
<h4>Create <%=controller.parcels.size()%> Parcels</h4>
<div id="create_plarcels">
    <table class="table table-striped cmss_table bulk_action">
        <thead>
            <tr>
                <td>UPID</td>
                <td>Owner</td>
                <td>Edited Area</td>
            </tr>
        </thead>
        <tbody>
            <% int i=0;
        for(LADM.Parcel p:controller.parcels){%>
            <tr>
                <td><%=p.upid%></td>
                <td><%=controller.parcelOwner(p)%></td>
                <td id="create_area_<%=p.parcelUID%>" class="align-right"></td>
            </tr>

            <%}%>
        </tbody>
    </table>
    <div id="split_buttons" class="align-right">
        <input id="create_button_commit" style="display:none" class="btn btn-primary" type="button" value="Commit" onclick="create_comit()" />
        <input id="create_button_cancel" type="button" class="btn btn-default" value="Cancel" onclick="create_cancel()" />
    </div>
</div>