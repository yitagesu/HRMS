<%@page import="com.intaps.nrlais.repo.*"%>
<%@page import="com.intaps.nrlais.*"%>
<%@page import="com.intaps.nrlais.model.*"%>
<%@page import="java.util.*"%>
<%@page pageEncoding="UTF-8" %>
<%
    MainFacade facade=new MainFacade(Startup.getSessionByRequest(request));
    
    List<CMSSTask> tasks=facade.getPendingTasks(true);
    if(tasks.size()==0) {
%>
<h2>No pending CMSS Tasks. Everything is clear. May be you can relax.</h2>
<% } else {%>
<div class="table-responsive current-task">
    <table class="table table-striped cmss_table bulk_action">
        <thead>
            <tr>
                <td>Action</td> <td>Application</td><td>Task</td><td>Parcel</td> 
            </tr>
        </thead>
        <%for(CMSSTask task:tasks){
            LATransaction tran=facade.getTransaction(task.transactionUID,false);
            IDName tranType=facade.getLookup("nrlais_sys.t_cl_transactiontype", tran.transactionType);
        %>
        <tr>
            <td>
                <input type="button" onclick="loadTask('<%=task.taskUID%>',<%=task.taskType%>)" value="<%=task.status==CMSSTask.TASK_STATUS_PENDING?"Process":"Amend"%>" class="btn btn-primary"/>
            </td>
            <td>
                <a href="javascript:openTask('<%=tran.transactionUID%>')"><%=tran.transactionID()%><br><%=tranType.name.textEn%></td>
            
                    <td><%=task.getTypeString()%> <br>(<%=task.status==CMSSTask.TASK_STATUS_PENDING?"Pending":"Processed"%>)</td>
                            
            <td>
                <%
                    if(task instanceof CMSSParcelSplitTask)
                    {
                        CMSSParcelSplitTask split=(CMSSParcelSplitTask)task;                           
                        LADM.Parcel parcel=facade.getParcel("nrlais_transaction",split.parcelUID);
                        out.print(parcel.upid.substring("00-000-000-".length()));
                    }
                    else
                         out.print("N/A");
                %>
            </td>
            

        </tr>
        <%}%>
    </table>
</div>
<%}%>