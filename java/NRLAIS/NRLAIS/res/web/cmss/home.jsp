<%@page import="com.intaps.nrlais.repo.*"%>
<%@page import="com.intaps.nrlais.*"%>
<%@page import="com.intaps.nrlais.model.*"%>
<%@page import="java.util.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    String sessionID=request.getParameter("session_id");
    List<KebeleInfo> kebeles=new MainFacade(Startup.getSessionByRequest(request)).getAllKebeles();
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

        <link rel="stylesheet" href="/assets/CSS/bootstrap.min.css"/>
        <link rel="stylesheet" href="/assets/CSS/custom.css"/>

        <script src="/assets/JS/jquery.min.js"></script>
        <script src="/assets/JS/bootbox.js"></script>
        <script src="/cmss/process_split.js"></script>
        <script src="/cmss/process_edit.js"></script>
        <script src="/cmss/process_create.js"></script>
        <title>CMSS Task Manager</title>
    </head>
    <body>

        <div id="select_app" class="align-center">
            <h4>CMSS Task Manager</h4>
            <div id='search_panel' style="display: none">
                Select Kebele:
                <select type="text" class="form-control" id="sea_kebele">
                    <option value="">Select Kebele</option>
                    <%for(KebeleInfo k:kebeles){%>
                    <option value="<%=k.kebeleID%>"><%=k.name.textEn%></option>
                    <%}%>
                </select>
                Enter Application Number
                <input type="text" id='sea_seqNo'/>
                <input  type="button" value="Search" onclick="searchTaskByApp()" />
            </div>
        </div>
        <div id='search_result' style="display:none">

        </div>
        <div id='task_panel' style="display:none">

        </div>
    </body>
    <script>
        $(document).ready(function ()
        {
            loadCurrentTasks();
        });
        var layerChanged = null;      //called from python 
        var featureSelected = null;   //called from python

        var setFeatureData_done = null;
        function setGeomData(data)  //called from python
        {
            setFeatureData_done(data);
        }
        function getGeomData(done,id)
        {
            setFeatureData_done = done;
            var url='/cmss/qcmd/get_data/';
            if(id)
                url=url+"?id="+id;
            window.location = url;
        }
        var sessionid = '<%=sessionID==null?"":sessionID%>';
        function setSelectionTool()
        {
            window.location = '/cmss/qcmd/set_selection_mod';
        }
        function openUrl(url)
        {
            window.location = '/cmss/qcmd/open_url?url=' + encodeURIComponent(url)
        }
        function labelGeometry(labels)
        {
            var url = null;
            for (var i = 0; i < labels.length; i++)
            {
                if (url)
                    url = url + '&id' + i + '=' + labels[i].id + '&lb' + i + '=' + encodeURIComponent(labels[i].label);
                else
                    url = 'id' + i + '=' + labels[i].id + '&lb' + i + '=' + encodeURIComponent(labels[i].label);
            }
            window.location = '/cmss/qcmd/label_geom?' + url;
        }
        function openTaskPage()
        {
            $('#search_result').hide();
            $('#task_panel').show();
        }
        function unloadTask()
        {
            window.location = '/cmss/qcmd/unloadTask';
        }
        function openHomePage()
        {
            $('#search_result').hide();
            $('#task_panel').hide();
            $('#select_app').show();
            loadCurrentTasks();
        }
        function loadCurrentTasks()
        {
            var cal = {
                method: "GET",
                url: "/cmss/current_tasks.jsp?session_id=" + sessionid,
                contentType: "application/json",
                success: function (data)
                {
                    $("#search_result").show();
                    $("#search_result").html(data);
                },
                error: function (err)
                {
                    alert('failed\n' + err);
                }
            };

            $.ajax(cal);
        }
        function searchTaskByApp()
        {
            var keb = $('#sea_kebele').val();
            if (keb == "")
            {
                alert('Select kebele');
                return;
            }
            var seq = parseInt($('#sea_seqNo').val());

            if (seq < 1 || seq > 99999)
            {
                alert('Enter valid sequence no');
                return;
            }

            var cal = {
                method: "POST",
                url: "/cmss/application_search_result.jsp?session_id=" + sessionid,
                contentType: "application/json",
                data: JSON.stringify({
                    'kebele': keb,
                    'applicationSeqNo': seq
                }),
                success: function (data)
                {
                    $("#search_result").show();
                    $("#search_result").html(data);
                },
                error: function (err)
                {
                    alert('failed');
                }
            };

            $.ajax(cal);


        }
        function showMessageInQGIS()
        {
            alert('call back');
        }
        function openTask(taskuid)
        {
            var url = '/view_transaction_detail.jsp?tran_uid=' + taskuid;
            openUrl(url);
        }
        function loadTask(taskid, type)
        {
            var url = null;
            if (type ==<%=CMSSTask.TASK_TYPE_SPLIT%>)
            {
                url = "/cmss/process_split.jsp";
            }
            if (type ==<%=CMSSTask.TASK_TYPE_EDIT%>)
            {
                url = "/cmss/process_edit.jsp";
            }
            if (type ==<%=CMSSTask.TASK_TYPE_CREATE%>)
            {
                url = "/cmss/process_create.jsp";
            }
            if (url)
            {
                var cal = {
                    method: "GET",
                    url: url + "?session_id=" + sessionid + "&task_uid=" + taskid,
                    success: function (data)
                    {
                        $("#task_panel").html(data);
                        if (type ==<%=CMSSTask.TASK_TYPE_SPLIT%>)
                        {
                            splitInit(taskid);
                        }
                        if (type ==<%=CMSSTask.TASK_TYPE_EDIT%>)
                        {
                            bcInit(taskid);
                        }
                        if (type ==<%=CMSSTask.TASK_TYPE_CREATE%>)
                        {
                            createInit(taskid);
                        }
                        openTaskPage();
                        window.location = '/cmss/qcmd/loadSplit?task_uid=' + taskid;
                    },
                    error: function (err)
                    {
                        alert('failed\n' + err);
                    }
                };

                $.ajax(cal);
            }
        }
    </script>
</html>
