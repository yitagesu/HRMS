/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var create_parcels = null;
var create_task = null;
function createInit(taskUID)
{
    $.ajax({url: '/api/create_task?task_uid=' + taskUID + "&session_id=" + sessionid,
        dataType: 'json',
        success: function (data)
        {
            create_task = data.task;
            create_parcels = data.parcels;
        }
    });
    layerChanged = function (type, id, count)
    {

        if (count == create_parcels.length)
            $("#create_button_commit").show();
        else
            $("#create_button_commit").hide();

    };
}
function create_getParcelUID(id)
{
    for (var i in create_geom)
        if (create_geom[i].id == id)
            return create_geom[i].parcelUID;
    return null;
}

function create_comit()
{
    getGeomData(function (data)
    {
        var as = {
            task_uid: create_task.taskUID,
            assignments: []
        };

        for (var i = 0; i < data.length; i++)
        {
            var puid = create_parcels[i].parcelUID;
            as.assignments.push(
                    {
                        geom: 'SRID=20137;' + data[i].wkt,
                        parcel_uid: puid
                    });
        }
        $.ajax({
            url: '/api/taskgeom?task_uid=' + create_task.taskUID + "&session_id=" + sessionid,
            method: 'POST',
            data: JSON.stringify(as),
            contentType: 'application/json',
            dataType: 'json',
            success: function (res)
            {
                if (res.error)
                {
                    alert('Error trying to save change.\n' + res.error);
                } else
                {
                    alert('succesfully saved');
                    unloadTask();
                    openHomePage();
                }
            },
            error: function (err)
            {
                alert('failed to save geometry to the database');
            }
        });
    });

}
function create_cancel()
{
    unloadTask();
    openHomePage();
}