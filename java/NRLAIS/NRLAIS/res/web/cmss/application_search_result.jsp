<%@page import="com.intaps.nrlais.repo.*"%>
<%@page import="com.intaps.nrlais.*"%>
<%@page import="com.intaps.nrlais.model.*"%>
<%@page import="java.util.*"%>
<%@page pageEncoding="UTF-8" %>
<%
    SearchApplicationPars pars=Startup.readPostedObject(request,SearchApplicationPars.class);
    MainFacade facade=new MainFacade(Startup.getSessionByRequest(request));
    List<LATransaction> result=facade.searchTransaction(pars,false,0,100);
    if(result.size()==0){
%>
<h2>Transaction not found</h2>
<% } else if(result.size()>1){ %>
<h2><%=result.size()%> transactions found, but only one transaction is expected. Check your search criteria</h2>
<% } else { 
LATransaction tran=result.get(0);
%>

<br/>Kebele:<%=tran.nrlais_kebeleid%>
<br/>Application Number:<%=tran.seqNo%>
<br/>Application Type:<%=tran.transactionType%>
<%
    List<CMSSTask> tasks=facade.getTaskByTranID(tran.transactionUID);
    if(tasks.size()==0)
    {
        %>
        <h2>This application has no task</h2>
        <%
    }
    else
    {%>
        <h2>Tasks for this transaction<h2>
        <table>
            <tr>
                <td>Task</td><td>Status</td><td>Parcel</td><td><td>  
            </tr>
            <%for(CMSSTask task:tasks){%>
               <tr>
                   <td><%=task.getTypeString()%></td>
                   <td>
                       <%=task.status==CMSSTask.TASK_STATUS_PENDING?"Pending":"Processed"%>
                   </td>                  
                   <td>
                       <%
                           if(task instanceof CMSSParcelSplitTask)
                           {
                               CMSSParcelSplitTask split=(CMSSParcelSplitTask)task;                           
                               LADM.Parcel parcel=facade.getParcel("nrlais_transaction",split.parcelUID);
                               out.print(parcel.upid);
                           }
                           else
                                out.print("N/A");
                       %>
                   </td>
                   <td>
                       <input type="button" onclick="loadTask('<%=task.taskUID%>',<%=task.taskType%>)" value="<%=task.status==CMSSTask.TASK_STATUS_PENDING?"Process":"Amend"%>"/>
                   </td>

            </tr>
            <%}%>
        </table>
    <%}
}%>
<script>
    function loadTask(taskid,type)
    {
        var url=null;
        if(type==<%=CMSSTask.TASK_TYPE_SPLIT%>)
        {
            url="/cmss/process_split.jsp";
        }
        if(url)
        {
            var cal = {
                method: "GET", 
                url: url +"?session_id="+ sessionid+"&task_uid="+taskid,
                success: function (data)
                {
                    $("#task_panel").html(data);
                    openTaskPage();
                    window.location='/cmss/qcmd/loadSplit?task_uid='+taskid;
                },
                error: function (err)
                {
                    alert('failed\n'+err);
                }
            };
            
            $.ajax(cal);
        }
    }
</script>