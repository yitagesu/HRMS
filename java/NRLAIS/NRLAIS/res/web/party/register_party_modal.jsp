<%@page import="com.intaps.nrlais.repo.*"%>
<%@page import="com.intaps.nrlais.*"%>
<%@page import="com.intaps.nrlais.model.*"%>
<%@page import="com.intaps.nrlais.controller.*"%>
<%@page import="com.intaps.nrlais.controller.tran.*"%>
<%@page import="java.util.*"%>
<%@page pageEncoding="UTF-8" %>
<%
    PartyViewController controller=new PartyViewController(request,response);
%>

    <div class="modal-dialog" id="party">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <ul class="nav nav-tabs tabs-left sideways">
                    <li class="active " id="<%=controller.setting.containerEl%>_holder-tap"><a href="#<%=controller.setting.containerEl%>_party_info" data-toggle="tab"><%=controller.text("Party Information")%></a></li>
                    <%if(controller.setting.guardian){%>
                    <li class="" id="<%=controller.setting.containerEl%>_guardian-tab"><a href="#<%=controller.setting.containerEl%>_guardian" data-toggle="tab"><%=controller.text("Guardian Information")%></a></li>
                    <%} if(controller.setting.representative){%>
                    <li class="" id="<%=controller.setting.containerEl%>_representative-tab"><a href="#<%=controller.setting.containerEl%>_representative" data-toggle="tab"><%=controller.text("Representative")%></a></li>
                    <%} if(controller.setting.representative || controller.setting.pom || controller.setting.idDoc){%>
                    <li class="" id="<%=controller.setting.containerEl%>_attachement-tab"><a href="#<%=controller.setting.containerEl%>_attachment" data-toggle="tab"><%=controller.text("Attachment")%></a></li>
                    <%}if(controller.setting.member){%>
                    <li class="" id="<%=controller.setting.containerEl%>_member-tab"><a href="#<%=controller.setting.containerEl%>_member" data-toggle="tab"><%=controller.text("Member")%></a></li>
                    <%}%>
                </ul>

            </div>            <!-- /modal-header -->
            <div class="modal-body has-scroll">
                <%@include  file="register_party_content.jsp"%>
            </div>
            <div class="modal-footer">
                <div class="pull-left party-error-panel">

                </div>
                <div class="form-group pull-left" style="display:inline">
                    
            <input type="button" value="Add Existing Party" id="add_Existing" class="btn btn-nrlais"/>
                </div>
                <div class="form-group pull-right" style="display:inline">
                <button class="btn btn-nrlais" type="button" onclick="partyPicker.validateAndSaveParty('<%=controller.setting.containerEl%>')"><%=controller.text("Add Party")%></button>
                </div>
            </div>
        </div>
    </div>      



