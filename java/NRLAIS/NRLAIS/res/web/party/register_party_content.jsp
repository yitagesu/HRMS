<form id="party_form">
    <div class="tab-content row">
        <div id="<%=controller.setting.containerEl%>_party_info" class="tab-pane fade active in">
            <div class="form-group col-md-12">
                <label><%=controller.text("Party Type")%></label>
                <%if(controller.setting.partyType==-1){%>
                <select class="form-control" id="party_type" onchange="partyPicker.changePartyType('<%=controller.setting.containerEl%>')">
                    <%for(IDNameText p:controller.partyTypeList()){%>
                    <option value="<%=p.id%>" <%=controller.partyType()==p.id?"selected":""%>><%=p.name%></option>
                    <%}%>
                </select>
                <%}else{%>
                <select class="form-control" id="party_type" onchange="partyPicker.changePartyType('<%=controller.setting.containerEl%>')" disabled="disabled">
                    <%for(IDNameText p:controller.partyTypeList()){%>
                    <option value="<%=p.id%>" <%=controller.setting.partyType==p.id?"selected":""%> ><%=p.name%></option>
                    <%}%>
                </select>
                <%}%>
            </div>
            <div class="align-center" id="undifined"></div>
            <div class="natual_person " id="natural_person">
                <div class="form-group col-md-12">
                    <label><%=controller.text("First Name")%>(*)</label>
                    <input type="text" id="party_firstname" class="form-control" value="<%=controller.name1()%>"/>
                </div>
                <div class="form-group col-md-12">
                    <label><%=controller.text("Father Name  ")%>(*)</label>
                    <input type="text" id="party_fathername" class="form-control" value="<%=controller.name2()%>"/>
                </div> 
                <div class="form-group col-md-12">
                    <label><%=controller.text("Grand Father Name")%> </label>
                    <input type="text" id="party_gfathername" class="form-control" value="<%=controller.name3()%>"/>
                </div>

                <div class="form-group col-md-12">
                    <div class="row">
                        <div class="col-lg-6">
                            <label><%=controller.text("Sex")%>(*)</label>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group" >
                                        <span class="input-group-addon" id="addon">
                                            <input type="radio" class="radio radio-primary" name="party_sex" id="party_sex_male" <%=controller.sex()==LADM.Party.SEX_MALE?"checked='checked'":""%>  >
                                        </span>
                                        <label class="form-control" for="party_sex_male" id="party_male" ><%=controller.text("Male")%></label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group" >
                                        <span class="input-group-addon" id="addon">
                                            <input type="radio" class="radio radio-primary" name="party_sex" id="party_sex_female" <%=controller.sex()==LADM.Party.SEX_FEMALE?"checked='checked'":""%>>
                                        </span>
                                        <label class="form-control" for="party_sex_female" id="party_female"><%=controller.text("Female")%></label>
                                    </div>
                                </div>
                            </div>    
                        </div>
                        <div class="col-lg-6">
                            <label><%=controller.text("Date of Birth")%>(*)</label>
                            <input class="form-control calander" id="party_dateOfBirth" dateval="<%=controller.birthDate()%>" />
                        </div>
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <label><%=controller.text("Marital Status")%>(*)</label>
                            <select class="form-control" id="party_martial_status">
                                <option value=""><%=controller.text("Please Select one")%></option>
                                <%for(IDNameText ms:controller.maritalStatusList()){%>
                                <option value="<%=ms.id%>" <%=controller.martialStatus()==ms.id?"selected":""%>><%=ms.name%></option>
                                <%}%>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label><%=controller.text("Family Role")%>(*)</label>
                            <select class="form-control" id="party_familyr">
                                <option value=""><%=controller.text("Please Select One")%></option>
                                <%for(IDNameText fr:controller.familyRoleList()){%>
                                <option value="<%=fr.id%>" <%=controller.familyRole()==fr.id?"selected":""%>><%=fr.name%></option>
                                <%}%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <label><%=controller.text("Address")%></label>
                    <textarea  rows="" class="form-control" id="parcel_form_person_address"><%=controller.address()%></textarea>
                </div>
                <div class="form-group col-md-12">
                    <div class="row">
                        <div class="col-lg-6">
                            <label><%=controller.text("Physical Disability")%>:</label>
                            <select class="form-control" id="party_phyimpairme">
                                <option value="0" <%=!controller.hasDisablity()?"selected":""%>><%=controller.text("No")%></option>
                                <option value="1" <%=controller.hasDisablity()?"selected":""%>><%=controller.text("Yes")%></option>
                            </select>
                        </div>
                        <div class="col-lg-6">
                            <label><%=controller.text("Orphan")%>:</label>
                            <select class="form-control" id="party_isorphan">
                                <option value="0" <%=!controller.isOrphan()?"selected":""%>><%=controller.text("No")%></option>
                                <option value="1" <%=controller.isOrphan()?"selected":""%>><%=controller.text("Yes")%>Yes</option>
                            </select>
                        </div>
                    </div>

                </div>

            </div>
            <div class="non_natural_person" id="non_natural_person" style="display: none">
                <div class="form-group col-md-12">
                    <label><%=controller.text("Party Name")%>(*)</label>
                    <input type="text" id="non_natural_party_name" class="form-control" value="<%=controller.name1()%>"/>
                </div>
                <div class="form-group col-md-12 org_type" id="non_natural_party_holding_purpose_panel">
                    <label><%=controller.text("Organization Type")%></label>
                    <select class="form-control" id="non_natural_party_holding_purpose">
                        <option value=""><%=controller.text("Please Select One")%></option>
                        <%for(IDNameText hp:controller.holdingPurposeList()){%>
                        <option value="<%=hp.id%>" <%=controller.orgType()==hp.id?"selected":""%>><%=hp.name%></option>
                        <%}%>
                    </select>
                </div>
                <div class="form-group col-md-12">
                    <label><%=controller.text("Address")%></label>
                    <textarea  rows="5" class="form-control" id="non_natural_party_address"></textarea>
                </div>
            </div>
            <%if(controller.setting.share){%>
            <div class="form-group col-md-12 share">
                <label><%=controller.text("Share")%></label>
                <input type="text" class="form-control" id="party_share" value="<%=controller.share()%>"/>
            </div>
            <%}%>
        </div>
        <div id="<%=controller.setting.containerEl%>_guardian" class="tab-pane fade">
            <div class="form-group col-md-12">
                <label><%=controller.text("First Name")%>(*)</label>
                <input type="text" id="party_g_firstname" class="form-control" value="<%=controller.gName1()%>"/>
            </div>
            <div class="form-group col-md-12">
                <label><%=controller.text("Father Name")%>(*)</label>
                <input type="text" id="party_g_fathername" class="form-control" value="<%=controller.gName2()%>""/>
            </div> 
            <div class="form-group col-md-12">
                <label><%=controller.text("Grand Father Name")%> </label>
                <input type="text" id="parcel_form_g_gfathername" class="form-control" value="<%=controller.gName3()%>"/>
            </div>
            <div class="form-group col-md-12">
                <div class="row">
                    <div class="col-lg-6">
                        <label><%=controller.text("Sex")%> (*)</label>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-group" >
                                    <span class="input-group-addon" id="addon">
                                        <input type="radio" class="radio radio-primary" name="g_sex" id="party_g_sex_male" <%=controller.gSex()==LADM.Party.SEX_MALE?"checked":""%> >
                                    </span>
                                    <label class="form-control" for="party_g_sex_male" id="party_g_male"><%=controller.text("Male")%></label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group" >
                                    <span class="input-group-addon" id="addon">
                                        <input type="radio" class="radio radio-primary" name="g_sex" id="party_g_sex_female">
                                    </span>
                                    <label class="form-control" for="party_g_sex_female" id="party_g_female" <%=controller.gSex()==LADM.Party.SEX_MALE?"checked":""%>><%=controller.text("Female")%></label>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div class="col-lg-6">
                        <label><%=controller.text("Date of Birth")%> (*)</label>
                        <input class="form-control calander" id="party_g_dateOfBirth" dateval="<%=controller.gBirthDate()%>"/>
                    </div>
                </div>
            </div>
            <div class="form-group col-md-12">
                <label><%=controller.text("Address")%>  (*)</label>
                <textarea  rows="5" class="form-control" id="party_g_address"><%=controller.gAddress()%></textarea>
            </div>
        </div>
        <div id="<%=controller.setting.containerEl%>_representative"  class="tab-pane fade">
            <div class="input-form col-md-12">
                <label class="control-label"><%=controller.text("First Name")%></label>
                <input type="text" class="form-control" id="app_name" placeholder="Enter First Name" value="<%=controller.rName1()%>" />
            </div>
            <div class="input-form col-md-12">
                <label class="control-label"><%=controller.text("Father Name")%></label>
                <input type="text" class="form-control" id="app_father_name" placeholder="Enter Father Name"  value="<%=controller.rName2()%>"/>
            </div>
            <div class="input-form col-md-12">
                <label class="control-label"><%=controller.text("Grand Father Name")%></label>
                <input type="text" class="form-control" id="app_gfather_name" placeholder="Enter Father Name"  value="<%=controller.rName3()%>"/>
            </div>

            <div class="input-form col-md-6">
                <label class="control-label col-md-12"><%=controller.text("Sex")%>:</label>
                <div class="col-md-6">
                    <div class="input-group" >
                        <span class="input-group-addon" id="addon">
                            <input type="radio" class="radio radio-primary" name="sex" id="rep_sex_male" checked="checked" <%=controller.rSex()==LADM.Party.SEX_MALE?"checked":""%> />
                        </span>
                        <label class="form-control" for="rep_sex_male" id="app_male" ><%=controller.text("Male")%></label>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="input-group" >
                        <span class="input-group-addon" id="addon">
                            <input type="radio" class="radio radio-primary" name="sex" id="rep_sex_female" <%=controller.rSex()==LADM.Party.SEX_FEMALE?"checked":""%> >
                        </span>
                        <label class="form-control" for="rep_sex_female" id="app_female"><%=controller.text("Female")%></label>
                    </div>
                </div>
            </div>
            <div class="input-form col-md-6">
                <label class="control-label"><%=controller.text("Date of Birth")%></label>
                <input type="text" class="form-control" placeholder="pick DOB"  id="party_rep_dob" dateval="<%=controller.rBirthDate()%>"/>
            </div>
            <div class="input-form col-md-12">
                <label class="control-label"><%=controller.text("Phone No")%></label>
                <input type="phone" class="form-control"   id="app_phone" value="<%=controller.rPhone()%>"/>
            </div>
            <div class="input-form col-md-12">
                <label class="control-label"><%=controller.text("Address")%></label>
                <textarea class="form-control"   id="app_address"><%=controller.rAddress()%></textarea>
            </div>
        </div>
        <div id="<%=controller.setting.containerEl%>_member" class="tab-pane fade">
            <div class="col-md-12" id="<%=controller.setting.containerEl%>_member-table"></div>
            <div class="col-md-12 align-right">
                <input type="button" value="<%=controller.text("Add Memeber")%>" id="<%=controller.setting.containerEl%>_addMemberBtn" class="btn btn-nrlais"/>
            </div>
        </div>
        <div class="tab-pane fade" id="<%=controller.setting.containerEl%>_attachment">            
            <div class="col-md-12">
                <div class="form-group col-md-6" panel="id_file">
                    <label><%=controller.text("Upload ID")%>:</label>
                    <input type="file" name="uploadimg" class="uploadimg" id="id_doc" onchange="attachment.openFile(event, 'partyPicker.idAttached(\'<%=controller.setting.containerEl%>\')')" />
                </div>
                <div class="form-group col-md-6" panel="id_ref">
                    <label><%=controller.text("ID Reference")%></label>
                    <input type="text" id="id_doc_ref" class="form-control" value="<%=controller.idRef()%>"/>
                </div>
                <div class="form-group col-md-6" panel="pom_file">
                    <label><%=controller.text("Upload Proof of Marriage/Celibacy")%></label>
                    <input type="file"  class="uploadimg" id="pom_doc" onchange="attachment.openFile(event, 'partyPicker.representativeAttached(\'<%=controller.setting.containerEl%>\')')" />
                </div>
                <div class="form-group col-md-6" panel="pom_ref">
                    <label><%=controller.text("Marriage/celibacy Reference")%></label>
                    <input type="text" class="form-control" id="pom_doc_ref" value="<%=controller.pomRef()%>"/>
                </div>
                <div class="form-group col-md-6" panel="rep_file">
                    <label><%=controller.text("Upload Representative Doc")%></label>
                    <input type="file" name="uploadimg" class="uploadimg" id="rep_doc" onchange="attachment.openFile(event, 'partyPicker.pomAttached(\'<%=controller.setting.containerEl%>\')')" />
                </div>
                <div class="form-group col-md-6" panel="rep_ref">
                    <label><%=controller.text("Representative Doc Reference")%></label>
                    <input type="text" id="rep_doc_ref" class="form-control" value="<%=controller.laterOfAutorizationRef()%>"/>
                </div>
            </div>
        </div>
       
    </div>   
</form>
