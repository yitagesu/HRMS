/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var partyPicker = {
    instances: [],
    changePartyType: function (containerEl) {
        this.instances[containerEl].changePartyType();
    },
    validateAndSaveParty: function (containerEl) {
        this.instances[containerEl].validateAndSave();
    },
    idAttached: function (containerEl) {
        this.instances[containerEl].idAttached();
    },
    representativeAttached: function (containerEl) {
        this.instances[containerEl].representativeAttached();
    },
    pomAttached: function (containerEl) {
        this.instances[containerEl].pomAttached();
    },
};
function getPartyPicker(setting)
{
    var state = {};
    function configureForm()
    {
        var container = $("#" + setting.containerEl);
        var partyType = container.find("#party_type").val();
        var showNaturalPerson = partyType == PARTY_TYPE_NATURAL_PERSON;
        var showNoneNaturalPerson = !showNaturalPerson;
        var showOrgSelector = partyType != PARTY_TYPE_NATURAL_PERSON && partyType != PARTY_TYPE_STATE;
        var dob = container.find("#party_dateOfBirth").attr("dateval");
        var showGuardianTab = getAgeFromCal(dob) < 18 && partyType == PARTY_TYPE_NATURAL_PERSON && setting.guardian;
        var showIdAttachment = partyType == PARTY_TYPE_NATURAL_PERSON && setting.idDoc;
        var showPOMAttachment = partyType == PARTY_TYPE_NATURAL_PERSON && setting.pom;
        var showRepresentative = partyType == PARTY_TYPE_NATURAL_PERSON && setting.representative;
        var showMemeber = partyType == PARTY_TYPE_GROUP_PARTY && setting.guardian;
        var showRepresenativeAttachment = showRepresentative && container.find("#app_name").val() != "";
        //show hide natural person, none natural person panels

        if (showNaturalPerson)
            container.find("#natural_person").show();
        else
            container.find("#natural_person").hide();

        if (showNoneNaturalPerson)
            container.find("#non_natural_person").show();
        else
            container.find("#non_natural_person").hide();
        //show/hide tabs

        if (showGuardianTab)
            state.showTab("guardian-tab");
        else
            state.hideTab("guardian-tab");

        if (showRepresentative)
            state.showTab("representative-tab");
        else
            state.hideTab("representative-tab");

        if (showIdAttachment || showPOMAttachment || showRepresenativeAttachment)
            state.showTab("representative-tab");
        else
            state.hideTab("representative-tab");
        if (showMemeber)
        {
            state.showTab("member-tab");
            state.memeberPicker = getPartyListPicker({
                containerEl: setting.containerEl + "_member-table",
                addButtonEl: setting.containerEl + "_addMemberBtn",
                partyType: 1,
                member: false,
                representative: false,
                pom: false,
                idDoc: false,
                share: true,
                edit: true,
                delete: true
            });
        } else
            state.hideTab("member-tab");
        //show hide org type selector
        if (showOrgSelector)
            container.find("#non_natural_party_holding_purpose_panel").show();
        else
            container.find("#non_natural_party_holding_purpose_panel").hide();

        //show hide document pickers

        if (showIdAttachment)
        {
            container.find("[panel=id_file]").show();
            container.find("[panel=id_ref]").show();
        } else
        {
            container.find("[panel=id_file]").hide();
            container.find("[panel=id_ref]").hide();
        }

        if (showIdAttachment)
        {
            container.find("[panel=id_file]").show();
            container.find("[panel=id_ref]").show();
        } else
        {
            container.find("[panel=id_file]").hide();
            container.find("[panel=id_ref]").hide();
        }

        if (showPOMAttachment)
        {
            container.find("[panel=pom_file]").show();
            container.find("[panel=pom_ref]").show();
        } else
        {
            container.find("[panel=pom_file]").hide();
            container.find("[panel=pom_ref]").hide();
        }

        if (showRepresenativeAttachment)
        {
            container.find("[panel=rep_file]").show();
            container.find("[panel=rep_ref]").show();
        } else
        {
            container.find("[panel=rep_file]").hide();
            container.find("[panel=rep_ref]").hide();
        }

    }

    var state = {
        onPartySave: null,
        setting: setting,
        party_dob: null,
        party_g_don: null,
        party_rep_dob: null,
        party_idDoc: {archiveType: 2, mimeType: null, fileImage: null},
        party_representativeDoc: {archiveType: 2, mimeType: null, fileImage: null},
        party_pomDoc: {archiveType: 2, mimeType: null, fileImage: null},
        partyModal: null,
        memeberPicker: null,
        beforeSave: null,
        existingPartyUID: null,
        showTab: function (id)
        {
            $("#" + setting.containerEl + "_" + id).show();
        },
        hideTab: function (id)
        {
            $("#" + setting.containerEl + "_" + id).hide();
        },
        idAttached: function ()
        {
            if (attachment.hasAttachment()) {
                this.party_idDoc.archiveType = 1;
                this.party_idDoc.mimeType = attachment.mimeType;
                this.party_idDoc.fileImage = attachment.imageBytes;
            } else {
                this.party_idDoc.archiveType = 2;
                this.party_idDoc.mimeType = null;
            }
        },
        representativeAttached: function ()
        {
            if (attachment.hasAttachment()) {
                this.party_representativeDoc.archiveType = 1;
                this.party_representativeDoc.mimeType = attachment.mimeType;
                this.party_representativeDoc.fileImage = attachment.imageBytes;
            } else {
                this.party_representativeDoc.archiveType = 2;
                this.party_representativeDoc.mimeType = null;
            }
        },
        pomAttached: function ()
        {
            if (attachment.hasAttachment()) {
                this.party_pomDoc.archiveType = 1;
                this.party_pomDoc.mimeType = attachment.mimeType;
                this.party_pomDoc.fileImage = attachment.imageBytes;
            } else {
                this.party_pomDoc.archiveType = 2;
                this.party_pomDoc.mimeType = null;
            }
        },
        changePartyType: function () {
            configureForm();
        },
        initDatePickers: function ()
        {
            this.party_dob = new EthiopianDualCalendar("#" + setting.containerEl + " #party_dateOfBirth");
            this.party_g_don = new EthiopianDualCalendar("#" + setting.containerEl + " #party_g_dateOfBirth");
            this.party_rep_dob = new EthiopianDualCalendar("#" + setting.containerEl + " #party_rep_dob");
            this.party_dob.onchange = function ()
            {
                configureForm();
            };
        },
        existingModal: null,
        holdingPicker:null,
        setupAddExisting: function ()
        {
            var that=this;
            $('#add_Existing').on('click', function ()
            {                
                if (this.existingModal)
                {
                    this.existingModal.showModal();
                    return;
                }
                $.ajax('/holding/holding_search.jsp?setting='+encodeURIComponent(JSON.stringify({containerEl:"existing_party_search_container",pickHolding:false,pickParty:true}))
                , {

                    'method': 'GET',
                    'success': function (data) {
                      

                        var html = "<div id='existing_party' class='modal-dialog'><div id='existing_party_search_container' class='modal-content'>"
                            +data+"</div></div>";        
                        that.existingModal = modalController.createModal(html,"existing_party_search_container_modal");
                        that.existingModal.showModal();
                        that.holdingPicker=holdingBrowser("#existing_party_search_container",true,{containerEl:"existing_party_search_container",pickHolding:false,pickParty:true});
                        that.holdingPicker.pickParty=function(partyuid)
                        {
                            that.existingModal.hideModal();
                                  $.ajax({url: '/api/get_parties?schema=nrlais_inventory'+'&party_uid=' + partyuid, dataType: 'json', success: function (data) {
                                          console.log(data.res);
                                          that.populateData({existingPartyUID:data.res.partyUID, party:data.res});
                                      }});
                          
                            
                            //console.log(getPartyListPicker(setting));
                        };
                        
                    },
                    'error': function (error) {
                        bootbox.alert("error occord:" + error);
                    }
                });

            });
        },
        populateData:function(data)
        {
            var that=this;
            this.existingPartyUID = data.existingPartyUID;
                $.ajax({
                    url: '/party/register_party_modal.jsp?setting=' + encodeURIComponent(JSON.stringify(setting)),
                    method: "POST",
                    data: JSON.stringify(data),
                    contentType: "application/json",
                    success: function (html)
                    {
                        //$("#" + setting.containerEl).html(html);
                        if(that.partyModal)
                            $("#"+setting.containerEl).html(html);
                        else
                            that.partyModal = modalController.createModal(html, setting.containerEl)
                        that.initDatePickers();
                        configureForm();
                        //that.changePartyType();
                        if (data.party.partyType == PARTY_TYPE_GROUP_PARTY)
                        {
                            var partyItems = [];
                            for (var i in data.party.members)
                                partyItems.push({party: data.party.members[i].member, share: data.party.members[i].share});
                            that.memeberPicker.setParyItems(partyItems);
                        }
                        that.setupAddExisting();
                    }
                });
        },
        showModal: function (data)
        {
            var that = this;
            if (data == null)
            {
                $.ajax({
                    url: '/party/register_party_modal.jsp?setting=' + encodeURIComponent(JSON.stringify(setting)),
                    success: function (html)
                    {
                        that.partyModal = modalController.createModal(html, setting.containerEl)
                        //$("#" + setting.containerEl).html(html);
                        that.initDatePickers();
                        configureForm();
                        //$("#" + setting.containerEl + '_modal').modal("show");
                        that.partyModal.showModal();
                        that.setupAddExisting();
                    }
                });

            } else
            {
                this.populateData(data);
                that.partyModal.showModal();
                that.setupAddExisting();
            }
        },
        load: function ()
        {

        },
        saveParty: function ()
        {
            if (this.beforeSave)
            {
                var that = this;
                var data = that.getPartyData();

                this.beforeSave(data, function (success)
                {
                    if (success)
                    {
                        if (that.onPartySave)
                            that.onPartySave(data);
                        that.partyModal.hideModal();
                    }
                });
            } else
            {
                if (this.onPartySave)
                    this.onPartySave(this.getPartyData());
                this.partyModal.hideModal();
            }
        },
        showParty: function (party) {
            var hasGuardian = "";
            if (party.guardian != null) {
                hasGuardian = 'Yes';
            } else {
                hasGuardian = 'No';
            }

            var htmlTD =
                    $("#tenant_party").append('<tr>' + htmlTD + '</tr>');
        },
        displayParty: function () {
            var partyObject = getPartyData();
            showParty(partyObject);
            this.partyModal.hideModal();
            $("#party_form").trigger('reset');

        },
        validateAndSave: function ()
        {
            var validator = validatorPanel('.party-error-panel');

            var container = $("#" + setting.containerEl);
            var partyType = container.find("#party_type").val();
            var valid = true;
            var validParty = true;
            if (partyType == '1') {
                var martialStatus = container.find("#party_martial_status").val();
                var familyRole = container.find("#party_familyr").val();
                var male = container.find("#party_sex_male").is(":checked");
                var female = container.find("#party_sex_female").is(":checked");
                var requiredFiled = ['party_firstname', 'party_fathername', 'party_dateOfBirth', 'party_martial_status', 'party_familyr'];
                for (var i = 0; i < requiredFiled.length; i++) {
                    if (!validTextInputNoEmpty(container.find('#' + requiredFiled[i])))
                    {
                        validParty = false;
                        valid = false;
                    }

                }
                if (familyRole != "") {
                    if ((male && (familyRole == "2" || familyRole == "5")) || (female && (familyRole == "1" || familyRole == "6"))) {
                        validator.addError("familyRole", lang("Sex and Family Role Mismatch"));
                        valid = false;
                        validParty = false;
                    } else {
                        validator.removeError("familyRole");
                    }
                }
                if (martialStatus != "" && familyRole != "") {
                    if (((familyRole == "1" || familyRole == "2") && martialStatus != "2") || ((familyRole == "5" || familyRole == "6") && (martialStatus == "2" || martialStatus == "0"))) {
                        validator.addError("martialStatus", lang("Martial Status and Family Role Mismatch"));
                        valid = false;
                        validParty = false;
                    } else {
                        validator.removeError("martialStatus");
                    }
                }

                if ($("#party_dateOfBirth").val() != "") {
                    if (!isValidDate($("#party_dateOfBirth"))) {
                        validator.addError("partyinvaDate", lang("Invalid Date Please Fix the Date"));
                        valid = false;
                        validParty = false;
                    } else {
                        validator.removeError("partyinvaDate");
                        var dob = container.find("#party_dateOfBirth").attr("dateval");
                        if (getAgeFromCal(dob) < 18 && (martialStatus == "2" || martialStatus == "3" || martialStatus == "4")) {
                            validator.addError("martialStatus_DOB", lang("Martial Status and Date of Birth Mismatch"));
                            valid = false;
                            validParty = false;
                        } else {
                            validator.removeError("martialStatus_DOB");
                        }
                        if (getAgeFromCal(dob) < 18 && validParty) {
                            container.find("#guardian-tap >a").trigger('click');
                            validator.addError("partyunderAge", lang("The party required guardian"));
                            var valideGurdian = true;
                            var g_male = container.find("#party_g_sex_male").is(":checked");
                            var g_female = container.find("#party_g_sex_female").is(":checked");
                            var requiredFileds = ['party_g_firstname', 'party_g_fathername', 'party_g_dateOfBirth', 'party_g_address'];
                            for (var i = 0; i < requiredFileds.length; i++) {
                                if (!validTextInputNoEmpty(container.find('#' + requiredFileds[i])))
                                {
                                    validParty = false;
                                    valid = false;
                                }
                            }
                            if (container.find("#party_g_dateOfBirth").val() != "") {
                                if (!isValidDate(container.find("#party_g_dateOfBirth"))) {
                                    validator.addError("partyinvaDate", lang("Invalid Date Please Fix the Date"));
                                    valid = false;
                                    valideGurdian = false;
                                } else {
                                    validator.removeError("partyinvaDate");
                                    var dob = container.find("#party_g_dateOfBirth").attr("dateval");
                                    if (getAgeFromCal(dob) < 18 && valideGurdian) {
                                        validator.removeError("partyunderAge");
                                        validator.addError("partygardunderAge", lang("The Guardian is under age"));
                                        valid = false;
                                    } else {
                                        validator.removeError("partygardunderAge");
                                    }
                                }
                            }
                        } else {
                            validator.removeError("partyunderAge");
                        }
                    }
                }

                if (container.find("#app_name").val() != "") {
                    var requiredFiled = ['app_name', 'app_father_name', 'app_address', 'app_phone'];
                    for (var i = 0; i < requiredFiled.length; i++) {
                        if (!validTextInputNoEmpty(container.find('#' + requiredFiled[i])))
                        {
                            valid = false;
                        }
                    }
                }
            } else if (partyType == "0") {
                valid = false;
            } else {
                if (!validTextInputNoEmpty(container.find("#non_natural_party_name"))) {
                    valid = false;
                }
            }
            if (container.find("[panel=id_ref]").css('display') != 'none')   //validate share if share option is on
            {
                if (!validTextInputNoEmpty(container.find("#id_doc_ref"))) {
                    validator.removeError("attachment_error");
                    validator.addError("attachment_error", lang("Please attached required documents using Attachment tab"));
                    valid = false;
                } else {
                    validator.removeError("attachment_error");
                }
            }
            if (container.find("[panel=rep_ref]").css('display') != 'none')   //validate share if share option is on
            {
                if (!validTextInputNoEmpty(container.find("#rep_doc_ref"))) {
                    validator.removeError("attachment_error");
                    validator.addError("attachment_error", lang("Please attached required documents using Attachment tab"));
                    valid = false;
                } else {
                    validator.removeError("attachment_error");
                }
            }

            if (container.find("[panel=pom_ref]").css('display') != 'none')   //validate share if share option is on
            {
                if (!validTextInputNoEmpty(container.find("#pom_doc_ref"))) {
                    validator.removeError("attachment_error");
                    validator.addError("attachment_error", lang("Please attached required documents using Attachment tab"));
                    valid = false;
                } else {
                    validator.removeError("attachment_error");
                }
            }
            if (valid)
            {
                this.saveParty();
            }
        }, //function validateAndSave        
        getPartyData: function () {
            var container = $("#" + setting.containerEl);
            var party = {};
            var representative = {};
            var partySex;

            if (container.find("#party_sex_male").is(':checked')) {
                partySex = SEX_MALE;
            } else if (container.find("#party_sex_female").is(':checked')) {
                partySex = SEX_FEMALE;
            } else {
                partySex = SEX_UNKNOWN;
            }
            var reprSex;
            if (container.find("#rep_sex_male").is(':checked')) {
                reprSex = SEX_MALE;
            } else if (container.find("#rep_sex_female").is(':checked')) {
                reprSex = SEX_FEMALE;
            } else {
                reprSex = SEX_UNKNOWN;
            }

            var repD = new Date(container.find("#party_rep_dob").attr("dateval"));
            var repdob = repD.getTime();

            var d = new Date(container.find("#party_dateOfBirth").attr("dateval"));
            var dob = d.getTime();
            if (container.find("#party_type").val() == "1")
            {
                party.partyType = container.find("#party_type").val();
                party.name1 = container.find("#party_firstname").val();
                party.name2 = container.find("#party_fathername").val();
                party.name3 = container.find("#party_gfathername").val();
                party.sex = partySex;
                party.dateOfBirth = dob;
                party.maritalstatus = container.find("#party_martial_status").val();
                party.mreg_familyrole = container.find("#party_familyr").val();
                party.disability = container.find("#party_phyimpairme").val();
                party.isorphan = container.find("#party_isorphan").val();

                var gardian = {};
                var gardianSex;
                if (container.find("#party_sex_male").attr('checked', 'checked')) {
                    gardianSex = 1;
                } else if (container.find("#party_sex_female").attr('checked', 'checked')) {
                    gardianSex = 2;
                } else {
                    gardianSex = "";
                }
                var date = new Date(container.find("#party_g_dateOfBirth").attr("dateval"));
                var gardianDOB = date.getTime();
                gardian.name1 = container.find("#party_g_firstname").val();
                gardian.name2 = container.find("#party_g_fathername").val();
                gardian.name3 = container.find("#parcel_form_g_gfathername").val();
                gardian.sex = gardianSex;
                gardian.dateOfBirth = gardianDOB;
                gardian.contacts = [];
                if (container.find("#party_g_address").val() != "") {
                    var gardianContact = {
                        contactData: container.find("#party_g_address").val(),
                        contactdataType: CONTACT_TYPE_ADDRESS
                    }
                    gardian.contacts.push(gardianContact);
                }
                gardian.partyType = PARTY_TYPE_NATURAL_PERSON;
                if (gardian.name1 != "") {
                    party.guardian = gardian;
                }
                party.contacts = [];
                if (container.find("#parcel_form_person_address").val() != "") {
                    var partyContact = {
                        contactData: container.find("#parcel_form_person_address").val(),
                        contactdataType: CONTACT_TYPE_ADDRESS
                    }
                    party.contacts.push(partyContact);
                }

                representative.partyType = container.find("#party_type").val();
                representative.name1 = container.find("#app_name").val();
                representative.name2 = container.find("#app_father_name").val();
                representative.name3 = container.find("#app_gfather_name").val();
                representative.sex = reprSex;
                representative.dateOfBirth = repdob;
                representative.contacts = [];
                var contactList = ['app_phone', 'app_address'];
                for (var i = 0; i < contactList.length; i++) {
                    var arraylist = container.find('#' + contactList[i]);
                    if (arraylist != "" && contactList[i] == "app_phone") {
                        var representativeContact = {
                            contactData: container.find("#app_phone").val(),
                            contactdataType: CONTACT_TYPE_OTHER,
                            contactDataTypeOther: "Phone"
                        }
                        representative.contacts.push(representativeContact);
                    } else if (arraylist != "" && contactList[i] == "app_address") {
                        var representativeContact = {
                            contactData: container.find("#app_address").val(),
                            contactdataType: CONTACT_TYPE_ADDRESS
                        }
                        representative.contacts.push(representativeContact);
                    }
                }

            } else if (container.find("#party_type").val() == PARTY_TYPE_GROUP_PARTY)
            {
                party.partyType = container.find("#party_type").val();
                party.name1 = container.find("#non_natural_party_name").val();
                party.orgatype = container.find("#non_natural_party_holding_purpose").val();
                party.contacts = [];
                if (container.find("#non_natural_party_address").val() != "") {
                    var orgContact = {
                        contactData: container.find("#non_natural_party_address").val(),
                        contactdataType: CONTACT_TYPE_ADDRESS
                    }
                    party.contacts.push(orgContact);
                }
                party.members = [];
                for (var i in this.memeberPicker.parties)
                {
                    var oneMember =
                            {
                                share: this.memeberPicker.parties[i].share,
                                member: this.memeberPicker.parties[i].party
                            };
                    party.members.push(oneMember);
                }

            } else if (container.find("#party_type").val() != "1" && container.find("#party_type").val() != "0") {
                party.partyType = container.find("#party_type").val();
                party.name1 = container.find("#non_natural_party_name").val();
                party.orgatype = container.find("#non_natural_party_holding_purpose").val();
                party.contacts = [];
                if (container.find("#non_natural_party_address").val() != "") {
                    var orgContact = {
                        contactData: container.find("#non_natural_party_address").val(),
                        contactdataType: CONTACT_TYPE_ADDRESS
                    }
                    party.contacts.push(orgContact);
                }
            }
            var share = null;
            if (this.setting.share)
                share = common_parseFraction(container.find("#party_share").val());
            if (this.setting.idDoc)
            {
                this.party_idDoc.refText = container.find('#id_doc_ref').val();
            }
            if (this.setting.representative)
            {
                this.party_representativeDoc.refText = container.find('#rep_doc_ref').val();
            }
            if (this.setting.pom)
            {
                this.party_pomDoc.refText = container.find('#pom_doc_ref').val();
            }
            if (this.existingPartyUID)
                party.partyUID = this.existingPartyUID;
            else
                party.partyUID = uuidv1();

            return {
                existingPartyUID: this.existingPartyUID,
                party: party,
                share: share,
                idDoc: this.party_idDoc,
                letterOfAttorneyDocument: this.party_representativeDoc,
                proofOfMaritalStatus: this.party_pomDoc,
                representative: representative
            };
        }//function getPartyData
    };
    state.load();
    partyPicker.instances[setting.containerEl] = state;
    return state;
}

