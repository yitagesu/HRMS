<%@page import="com.intaps.nrlais.repo.*"%>
<%@page import="com.intaps.nrlais.*"%>
<%@page import="com.intaps.nrlais.model.*"%>
<%@page import="com.intaps.nrlais.model.tran.*"%>
<%@page import="com.intaps.nrlais.controller.*"%>
<%@page import="com.intaps.nrlais.controller.tran.*"%>
<%@page import="java.util.*"%>
<%@page pageEncoding="UTF-8" %>
<%
    PartyListViewController listController=new PartyListViewController(request,response);
%>
<table  class="table table-striped jambo_table">
    <thead>
        <tr>
            <% if(listController.setting.select){%>
            <td><%=listController.text("Select")%></td>
            <%}%>
            <%if(listController.setting.partyType == -1 || listController.setting.partyType == 1){%>
            <td <%=listController.setting.idDoc?"rowspan='2'":""%> ><%=listController.text("Full Name")%></td>
            <td <%=listController.setting.idDoc?"rowspan='2'":""%> ><%=listController.text("Sex")%></td>
            <td <%=listController.setting.idDoc?"rowspan='2'":""%> ><%=listController.text("Age")%></td>
            <td <%=listController.setting.idDoc?"rowspan='2'":""%> ><%=listController.text("Martial Status")%></td>
            <td <%=listController.setting.idDoc?"rowspan='2'":""%> ><%=listController.text("Family Role")%></td>
            <td <%=listController.setting.idDoc?"rowspan='2'":""%> ><%=listController.text("Guardian")%></td>
            <%if(listController.setting.share){%>
            <td <%=listController.setting.idDoc?"rowspan='2'":""%> ><%=listController.text("Share")%></td>
            <%}%>
            <%if(listController.setting.idDoc){%>
            <td colspan="2"><%=listController.text("ID Document")%></td>
            <%}%>
            
            <%if(listController.setting.edit||listController.setting.delete){%>
            <td>    
            </td>
            <%}}else{%>
            <td <%=listController.setting.idDoc?"rowspan='2'":""%> ><%=listController.text("Name")%></td>
            <td <%=listController.setting.idDoc?"rowspan='2'":""%> ><%=listController.text("Organization Type")%></td>
            <td <%=listController.setting.idDoc?"rowspan='2'":""%> ><%=listController.text("Address")%></td>
            <%if(listController.setting.edit||listController.setting.delete){%>
            <td>    
            </td>
            <%}}%>
        </tr>
    </thead>
    <tbody id="<%=listController.setting.containerEl%>_party_list">                    
        <%for(PartyItem item:listController.items()){%>
        <tr>
            <td><%=listController.itemName(item)%></td>
            <td><%=listController.itemSex(item)%></td>
            <td><%=listController.itemAge(item)%></td>
            <td><%=listController.itemMaritalStatus(item)%></td>
            <td><%=listController.itemFamilyRole(item)%></td>
            <td><%=listController.itemGuardian(item)%></td>
            <%if(listController.setting.share){%>
            <td><%=listController.itemShare(item)%></td>
            <%}%>
            <%if(listController.setting.idDoc){%>
            <td><%=listController.itemIDType(item)%></td>
            <td><%=listController.itemIDRef(item)%></td>
            <%}%>
        </tr>
        <%}//for%>
    </tbody>
</table>
<div id="<%=listController.setting.containerEl%>_party_picker">
   
</div>