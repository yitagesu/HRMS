/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var partyListPicker =
        {
            instances: [],
            settings: null,
            addButtonClicked: function (container)
            {
                this.instances[container].addButtonClicked();
            },
            removeItem: function (container, tag)
            {
                this.instances[container].removeItem(tag);
            },
            editItem: function (container, tag)
            {
                this.instances[container].editItem(tag);
            },
            selectionChanged: function (container, tag) {
                this.instances[container].selectionChanged(tag);
            }
        };

function getPartyListPicker(setting)
{
    //if (partyListPicker.instances[setting.containerEl])
    //    return partyListPicker.instances[setting.containerEl];
    $("#" + setting.addButtonEl).attr("onclick", "partyListPicker.addButtonClicked('" + setting.containerEl + "')");
    var row_template = "";
    var state =
            {
                ready: false,
                partyEditor: null,
                setting: setting,
                parties: [],
                parties_tag: [],
                tag_counter: 0,
                edit_row_index: -1,
                onChanged: null,
                beforeSave: null,
                beforeDelete: null,
                onSelectionChanged:null,
                callOnChanged: function ()
                {
                    if (this.onChanged)
                        this.onChanged();
                },
                selectionChanged:function()
                {
                    if(this.onSelectionChanged)
                        this.onSelectionChanged();
                },
                addPartyRow: function (party)
                {
                    this.tag_counter++;
                    this.parties_tag.push(this.tag_counter);
                    var html = ejs.render(row_template, {locals: {p: party, setting: setting, tag: this.tag_counter}});
                    $("#" + setting.containerEl + "_party_list").append(html);
                },
                setPartyRow: function (index, party)
                {
                    var html = ejs.render(row_template, {locals: {p: party, setting: setting, tag: this.parties_tag[index]}});
                    $("#" + setting.containerEl + "_party_list").children().eq(index).replaceWith(html);
                },
                getSelected: function ()
                {
                    var ret = [];
                    var that=this;
                    var i=0;
                    $("#" + setting.containerEl+" tbody [party_uid]").each(function()
                    {
                        if($(this).find("input[type=checkbox]").is(":checked"))
                        {
                            ret.push(that.parties[i]);
                        }
                        i++;
                    });
                    return ret;

                },
                removeItem: function (tag)
                {
                    var i = this.parties_tag.indexOf(parseInt(tag));
                    if (i == -1)
                    {
                        bootbox.alert(lang('Element not found'));
                        return;
                    }
                    if (this.beforeDelete)
                    {
                        var that=this;
                        this.beforeDelete(this.parties[i],function(success)
                        {
                            if(success)
                            {
                                that.removeItemInternal(i);
                            }
                        });

                    } else{
                        this.removeItemInternal(i);
                    }
                },
                removeItemInternal: function (i)
                {
                    $("#" + setting.containerEl + "_party_list").children().eq(i).remove();
                    this.parties.splice(i,1);
                    this.parties_tag.splice(i,1);
                    this.callOnChanged();
                },
                editItem: function (tag)
                {
                    this.edit_row_mode = true;
                    var i = this.parties_tag.indexOf(parseInt(tag));
                    
                    this.edit_row_index = i;
                    this.partyEditor.showModal(this.parties[i]);
                },
                onPartySave: function (party)
                {
                    if (this.edit_row_index > -1)
                    {
                        this.setPartyRow(this.edit_row_index, party);
                        this.parties[this.edit_row_index] = party;
                    } else
                    {
                        this.addPartyRow(party);
                        this.parties.push(party);
                    }
                    this.callOnChanged();
                    return true;
                },
                populate: function ()
                {
                    $('#'+this.setting.containerEl+" tbody tr").remove();
                    for (var i in this.parties)
                        this.addPartyRow(this.parties[i]);
                },
                setParyItems: function (items)
                {
                    this.parties = items;
                    this.parties_tag=[];
                    if (this.ready)
                        this.populate();
                },
                loadPartyPicker: function (done)
                {
                    this.partyEditor = getPartyPicker({containerEl: this.setting.containerEl + '_party_picker', share: setting.share, idDoc: setting.idDoc, partyType: setting.partyType, pom: setting.pom, representative: setting.representative, guardian: setting.guardian, member: setting.member});
                    var b = this.onPartySave.bind(this);
                    this.partyEditor.onPartySave = b;
                    this.partyEditor.beforeSave = this.beforeSave;
                },
                addButtonClicked: function ()
                {
                    this.edit_row_index = -1;
                    this.partyEditor.showModal();
                },
                load: function ()
                {
                    var that = this;
                    $.ajax({
                        url: '/party/party_list.jsp?setting=' + encodeURIComponent(JSON.stringify(setting)),
                        success: function (html)
                        {
                            $("#" + setting.containerEl).html(html);
                            $.ajax({
                                url: '/party/party_list_row.ejs',
                                success: function (html)
                                {
                                    row_template = html;
                                    that.ready = true;
                                    that.populate();
                                    that.loadPartyPicker();
                                }
                            });
                        }
                    });
                }
            };
    state.load();
    partyListPicker.instances[setting.containerEl] = state;
    return state;
}
