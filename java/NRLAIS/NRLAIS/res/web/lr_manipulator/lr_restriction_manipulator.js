/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function restrictionManipulator(settings)
{
    var parent;
    function getRestrictionFormController(data) {
        return {
            setting: {
                containerEl: "",

            },
            fullName:function()
            {
                if(data)
                    return this.fullNameOf(data.party);
            },
            isRestriction: function () {
                if(data)
                    return data.isRestriction;
                return true;
            },
            sex: function () {
                if(data)
                {
                    return lookUPCode('static.sex',data.party.sex)
                }
                return "";
            },
            age: function () {
                if(data)
                {
                    return getAgeFromCal(data.party.dateOfBirth);
                }
                return "";
            },
            maritalStatus:function()
            {
                if(data)
                    return lookUPCode('nrlais_sys.t_cl_maritalstatus', data.party.maritalstatus);
                return "";
            },
            familyRole:function()
            {
                if(data)
                    return lookUPCode('static.family_role',data.party.mreg_familyrole);
                return "";
            },
            guardian:function()
            {
                if(data && data.party.guardian)
                    return this.fullNameOf(data.party.guardian);
                return "";
            },
            parcel:function()
            {
                if(data)
                    for(var i in parent.parcelItems) {
                        if (parent.parcelItems[i].parcel.parcelUID == data.parcelUID)
                            return formatParcelSeqNo(parent.parcelItems[i].parcel.seqNo);
                    }
                return "";
            },
            area:function()
            {
                if(data)
                    for(var i in parent.parcelItems) {
                        if (parent.parcelItems[i].parcel.parcelUID == data.parcelUID)
                            return formatParcelArea(parent.parcelItems[i].parcel.areaGeom);
                    }
                return "";
            },
            fromDate:function()
            {
                if(data) {
                    return toEthDateStr(data.startDate);
                }
                return "";
            },
            toDate:function()
            {
                if(data) {
                    return toEthDateStr(data.endDate);
                }
                return "";
            },
            fromDateVal:function()
            {
                if(data) {
                    return getDateString(data.startDate);
                }
                return "";
            },
            toDateVal:function()
            {
                if(data) {
                    return getDateString(data.endDate);
                }
                return "";
            },
            fullNameOf: function (person)
            {
                if (person)
                {
                    var fl = person.name1;
                    if (person.name2)
                        fl += " " + person.name2;
                    if (person.name3)
                        fl += " " + person.name3;
                    return fl;
                }
                return "";
            },
            persons: function ()
            {
                return settings.persons();
            }            
            ,party:function()
            {
                if(data)
                    return data.party;
                return {};
            },
            parcels:function()
            {
                return parent.parcelItems;
            },
            seqNo:function()
            {
                if(data)
                    return this.seqNoOf(this.parcel());
                 return "";
            },
            seqNoOf:function(parcel) {
                if (!parcel)
                    return "";
                if (parcel.seqNo == 0)
                    return "";
                return formatParcelSeqNo(parcel.seqNo);
            },
            parcel:function()
            {
                if(data)
                    for(var i in parent.parcelItems) {
                        if (parent.parcelItems[i].parcel.parcelUID == data.parcelUID)
                            return parent.parcelItems[i].parcel;
                    }
                return {};
            },
            restrictionUID:function()
            {
                if(data && data.restrictionUID)
                    return data.restrictionUID;
                return "";
            },
            types:function()
            {
                return settings.types();
            },
            typeID:function()
            {
                if(data)
                    return data.restrictionType;
                return RESTRICTION_TYPE_OTHER_RESTRAINING_ORDER;
            },
            type:function()
            {
                if(data)
                    return lookUPCode("static.restrictive_interest",data.restrictionType);
                return "";
            }
            
        }
    }

    var restriction_start_date_picker;
    var restriction_end_date_picker;
    var restriction_list = listManager(
            {
                tableBodySelector: settings.tableBodySelector,
                addButtonSelector: settings.addButtonSelector,
                row_template_url: "/lr_manipulator/lrm_restriction_row.ejs",
                form_template_url: "/lr_manipulator/lrm_restriction_form.ejs",
                getNewRecordTemplateData: function () {
                    return {
                        controller: getRestrictionFormController(null)
                    };
                },
                getEditFormTemplateData: function (restriction_data) {
                    return {
                        controller:getRestrictionFormController(restriction_data)
                    };
                },
                getRowTemplateData: function (restriction_data) {
                    return {
                        controller:getRestrictionFormController(restriction_data)
                    };
                },
                validateAndGetData: function () {
                    return false;
                },
                formDOMAttached:function () {
                    restriction_start_date_picker = new EthiopianDualCalendar("#restriction_form [field=restrictionFrom]");
                    restriction_end_date_picker = new EthiopianDualCalendar("#restriction_form [field=restrictionTo]");
                    $("#restriction_form [field=save_restriction]").click(
                        function()
                        {
                            var record=collectRestrictionRecord();
                            parent.sendCommand({
                                opType: "com.intaps.nrlais.model.tran.ManualTransactionData$LrOpSaveRestriction",
                                restriction: record,
                            }, function (data) {
                                if (data.error) {
                                    alert(data.error);
                                } else {
                                    alert("Restriction Saved");                                    
                                    parent.loadHolding();
                                    restriction_list.hideEditor();                                    
                                }
                            }, function () {
                                alert('Communication error');
                            });
                        });
                }
            }
    );
    restriction_list.beforeDelete=function(restriction,then)
    {
        bootbox.confirm("Are you sure you want to delete this restriction?",
            function (result) {
                if (result) {
                    parent.sendCommand({
                            opType: "com.intaps.nrlais.model.tran.ManualTransactionData$LrOpDeleteRestriction",
                            restrictionUID: restriction.restrictionUID
                        },
                        function (data) {
                            if (data.error) {
                                alert(data.error);
                                then(false);
                            } else {
                                alert("Restriction Deleted");
                                then(true);
                            }

                        }, function () {
                            alert('Communication error');
                            then(false);
                        });
                } else
                    then(false);
            });
    };
    function collectRestrictionRecord()
    {
        var container=$("#restriction_form");
        var restrictionUID=container.find("[field=restriction_uid]").val();

        var ret={
            restrictionType:container.find("[field=restrictionType]").val(),
            startDate:getLongDateFromCal(container.find("[field=restrictionFrom]")),
            endDate:getLongDateFromCal(container.find("[field=restrictionTo]")),
            parcelUID:container.find("[field=parcel]").val(),
            partyUID:container.find("[field=party]").val(),
            holdingUID:parent.tranData.data.holdingUID,
        };  
        if(restrictionUID && restrictionUID.length>3)
            ret.restrictionUID=restrictionUID;
        return ret;
    }
    
    return  {        
        loadData:function(par)
        {
            parent=par;
            restriction_list.populateData(parent.restrictions);
        }
    };
}
