/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function rentManipulator()
{
    var parent;
    function getRentFormController(data) {
        return {
            setting: {
                containerEl: "",

            },
            fullName:function()
            {
                if(data)
                    return this.fullNameOf(data.party);
            },
            isRent: function () {
                if(data)
                    return data.isRent;
                return true;
            },
            sex: function () {
                if(data)
                {
                    return lookUPCode('static.sex',data.party.sex)
                }
                return "";
            },
            age: function () {
                if(data)
                {
                    return getAgeFromCal(data.party.dateOfBirth);
                }
                return "";
            },
            maritalStatus:function()
            {
                if(data)
                    return lookUPCode('nrlais_sys.t_cl_maritalstatus', data.party.maritalstatus);
                return "";
            },
            familyRole:function()
            {
                if(data)
                    return lookUPCode('static.family_role',data.party.mreg_familyrole);
                return "";
            },
            guardian:function()
            {
                if(data && data.party.guardian)
                    return this.fullNameOf(data.party.guardian);
                return "";
            },
            parcel:function()
            {
                if(data)
                    for(var i in parent.parcelItems) {
                        if (parent.parcelItems[i].parcel.parcelUID == data.parcelUID)
                            return formatParcelSeqNo(parent.parcelItems[i].parcel.seqNo);
                    }
                return "";
            },
            area:function()
            {
                if(data)
                    for(var i in parent.parcelItems) {
                        if (parent.parcelItems[i].parcel.parcelUID == data.parcelUID)
                            return formatParcelArea(parent.parcelItems[i].parcel.areaGeom);
                    }
                return "";
            },
            rentSize:function()
            {
                if(data)
                    return data.rentSize;
                return "";
            },
            fromDate:function()
            {
                if(data) {
                    return toEthDateStr(data.rightType==RIGHT_TYPE_RENT?data.startRentDate:data.startLeaseDate);
                }
                return "";
            },
            toDate:function()
            {
                if(data) {
                    return toEthDateStr(data.rightType==RIGHT_TYPE_RENT?data.endRentDate:data.endLeaseDate);
                }
                return "";
            },
            fromDateVal:function()
            {
                if(data) {
                    return getDateString(data.rightType==RIGHT_TYPE_RENT?data.startRentDate:data.startLeaseDate);
                }
                return "";
            },
            toDateVal:function()
            {
                if(data) {
                    return getDateString(data.rightType==RIGHT_TYPE_RENT?data.endRentDate:data.endLeaseDate);
                }
                return "";
            },
            leaseAmount:function()
            {
                if(data)
                    return data.leaseAmount;
                return "";
            },
            fullNameOf: function (person)
            {
                if (person)
                {
                    var fl = person.name1;
                    if (person.name2)
                        fl += " " + person.name2;
                    if (person.name3)
                        fl += " " + person.name3;
                    return fl;
                }
                return "";
            },
            persons: function ()
            {
                    var ret=[];
                    for(var i in parent.parties)
                        if(parent.parties[i].role==PARTY_ROLE_NONE||parent.parties[i].role==PARTY_ROLE_TENANT)
                            ret.push(parent.parties[i]);
                 return ret;
            }            
            ,party:function()
            {
                if(data)
                    return data.party;
                return {};
            },
            parcels:function()
            {
                return parent.parcelItems;
            },
            seqNo:function()
            {
                if(data)
                    return this.seqNoOf(this.parcel());
                 return "";
            },
            seqNoOf:function(parcel) {
                if (!parcel)
                    return "";
                if (parcel.seqNo == 0)
                    return "";
                return formatParcelSeqNo(parcel.seqNo);
            },
            parcel:function()
            {
                if(data)
                    for(var i in parent.parcelItems) {
                        if (parent.parcelItems[i].parcel.parcelUID == data.parcelUID)
                            return parent.parcelItems[i].parcel;
                    }
                return {};
            },
            rightUID:function()
            {
                if(data && data.rightUID)
                    return data.rightUID;
                return "";
            }
        }
    }

    var rent_start_date_picker;
    var rent_end_date_picker;
    var rent_list = listManager(
            {
                tableBodySelector: "#rent_rows",
                addButtonSelector: "#rents_add_btn",
                formSaveButtonSelector: "#rent_save",
                row_template_url: "/lr_manipulator/lrm_rent_row.ejs",
                form_template_url: "/lr_manipulator/lrm_rent_form.ejs",
                getNewRecordTemplateData: function () {
                    return {
                        controller: getRentFormController(null)
                    };
                },
                getEditFormTemplateData: function (rent_data) {
                    return {
                        controller:getRentFormController(rent_data)
                    };
                },
                getRowTemplateData: function (rent_data) {
                    return {
                        controller:getRentFormController(rent_data)
                    };
                },
                validateAndGetData: function () {
                    return false;
                },
                formDOMAttached:function () {
                    rent_start_date_picker = new EthiopianDualCalendar("#rent_form [field=rentFrom]");
                    rent_end_date_picker = new EthiopianDualCalendar("#rent_form [field=rentTo]");
                    $("#rent_form [field=save_rent]").click(
                        function()
                        {
                            var record=collectRentRecord();
                            parent.sendCommand({
                                opType: "com.intaps.nrlais.model.tran.ManualTransactionData$LrOpSaveRent",
                                rent: record,
                            }, function (data) {
                                if (data.error) {
                                    alert(data.error);
                                } else {
                                    alert("Rent Saved");                                    
                                    parent.loadHolding();
                                    rent_list.hideEditor();
                                    
                                }

                            }, function () {
                                alert('Communication error');
                            });
                        });
                }
            }
    );
    rent_list.beforeDelete=function(rent,then)
    {
        bootbox.confirm("Are you sure you want to delete this rent.",
            function (result) {
                if (result) {
                    parent.sendCommand({
                            opType: "com.intaps.nrlais.model.tran.ManualTransactionData$LrOpDeleteRent",
                            rightUID: rent.rightUID
                        },
                        function (data) {
                            if (data.error) {
                                alert(data.error);
                                then(false);
                            } else {
                                alert("Rent Deleted");
                                then(true);
                            }

                        }, function () {
                            alert('Communication error');
                            then(false);
                        });
                } else
                    then(false);
            });
    };
    function collectRentRecord()
    {
        var container=$("#rent_form");
        var rightUID=container.find("[field=right_uid]").val();

        var ret={
            rightType:container.find("[field=rent_type_rent]").is(":checked")?RIGHT_TYPE_RENT:RIGHT_TYPE_LEASE,
            leaseAmount:parseFloat(container.find("[field=rentAmount]").val()),
            startLeaseDate:getLongDateFromCal(container.find("[field=rentFrom]")),
            endLeaseDate:getLongDateFromCal(container.find("[field=rentTo]")),
            startRentDate:getLongDateFromCal(container.find("[field=rentFrom]")),
            endRentDate:getLongDateFromCal(container.find("[field=rentTo]")),
            rentSize:parseFloat(container.find("[field=rentArea]").val()),
            parcelUID:container.find("[field=parcel]").val(),
            partyUID:container.find("[field=party]").val(),
            holdingUID:parent.tranData.data.holdingUID,
        };
        if(rightUID && rightUID.length>3)
            ret.rightUID=rightUID;
        return ret;
    }
    
    return {
        loadData:function(par)
        {
            parent=par;
            rent_list.populateData(parent.rents);
        }
    };
}
