<%-- 
    Document   : LR_manipulator
    Created on : Feb 15, 2018, 3:08:51 PM
    Author     : yitagesu
--%>

<%@page import="com.intaps.nrlais.repo.*"%>
<%@page import="com.intaps.nrlais.*"%>
<%@page import="com.intaps.nrlais.controller.*"%>
<%@page import="com.intaps.nrlais.controller.tran.*"%>
<%@page import="com.intaps.nrlais.model.*"%>
<%@page import="java.util.*"%>
<%@page pageEncoding="UTF-8" %>
<%

   HeaderViewController headerController=new HeaderViewController(request,response,false);
   int tranType=Integer.parseInt(request.getParameter("tran_type"));
   HoldingTransactionViewController controller=(HoldingTransactionViewController)TransactionViewController.createController(request,response,true);
   
%>
<html>
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>NRLAIS</title>
        <link rel="stylesheet" href="/assets/CSS/bootstrap.min.css"/>
        <link rel="stylesheet" href="/assets/CSS/bootstrap-popover-x.min.css"/>
        <link rel="stylesheet" href="/assets/CSS/custom.css"/>
        <link rel="stylesheet" href="/assets/ol/ol.css"/>
        <!--        third party-->
        <script src="/assets/JS/jquery.min.js"></script>
        <script src="/assets/JS/jquery-ui.js"></script>
        <script src="/assets/JS/bootstrap.min.js"></script>
        <script src="/assets/JS/bootstrap-popover-x.min.js"></script>
        <script src="/assets/JS/bootbox.js"></script>
        <script src="/assets/JS/string.js"></script>
        <script src="/assets/JS/uuid-gen.js"></script>

        <!--        INTAPS common-->
        <script src="/assets/JS/et_dual_cal.js"></script>
        <script src="/assets/JS/general.js"></script>

        <!--        NRLAIS general-->
        <script src="/assets/JS/js_constants.jsp"></script>
        <script src="/assets/JS/document_picker.js"></script>        
        <script src="/holding/searching_holding.js"></script>
        <script src="/assets/JS/transaction.js"></script>
        <script src="/assets/JS/attachment.js"></script>
        <script src="/holding/applicants_picker.js"></script>
        <script src="/holding/parcel_split_table.js"></script>
        <script src="/holding/parcel_transfer_table.js"></script>
        <script src="/party/register_party.js"></script>
        <script src="/assets/JS/nrlais_map.js"></script>
        <script src="/assets/ol/ol-debug.js"></script>
        <script src="/assets/ol/proj4.js"></script>
        <script src="/assets/JS/lookups.js.jsp"></script>

        <!--        NRLAIS transaction specific-->
        <script src="/tran/divorce/register_divorce.js"></script>
        <script src="/tran/rent/register_rent.js"></script>
        <script src="/tran/gift/register_gift.js"></script>
        <script src="/tran/inheritance/register_inheritance.js"></script>
        <script src="/tran/reallocation/register_reallocation.js"></script>
        <script src="/tran/boundary_correction/register_boundary_correction.js"></script>
        <script src="/tran/expropriation/register_expropriation.js"></script>
        <script src="/tran/boundary_correction/register_boundary_correction.js"></script>
        <script src="/tran/special_case/register_special_case.js"></script>
        <script src="/tran/consolidation/register_consolidation.js"></script>
        <script src="/tran/exchange/register_exchange.js"></script>
        <script src="/tran/ex_officio/register_ex_officio.js"></script>

        <script src="/lr_manipulator/lr_manipulator.js"></script>
        <script src="/lr_manipulator/lr_rent_manipulator.js"></script>
        <script src="/lr_manipulator/lr_restriction_manipulator.js"></script>


        <script src="/assets/JS/modal_controler.js"></script>
        <script src="/assets/JS/ejs.js"></script>
        <script src="/party/party_list.js"></script>
        <script src="/party/register_party.js"></script>
        <script src="/parcel/parcel_list.js"></script>
        <script src="/parcel/register_parcel.js"></script>
        <script src="/assets/JS/list_manager.js"></script>
        <script>
            var tranData =<%=controller.json(controller.tran)%>;
            var map_server = '<%=controller.mapServerUrl()%>';
            $(document).ready(function () {
                modalController.load('body');
                lrManipulatorInit(tranData);
            });
        </script>
    </head>

    <body id="body">
        <%@ include file="../../header.jsp" %>
        <div class="dashboard-body">
            <div class="list-of-transc col-md-8" style='height:95%'>
                <div id="reg_application" style='height:100%'>
                    <div class="x_panel " id="" style='height:100%'>
                        <div class="x_title">
                            <table class="table">
                                <tr>
                                    <td><%=controller.text("Woreda")%>: <%=controller.woreda.name.textEn%></td>
                                    <td><%=controller.text("Kebele")%>: <%=controller.kebele.name.textEn%></td>

                                    <td>
                                        <div class="form-inline">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <%=controller.text("Holding ID")%>:
                                                </span>
                                                <Select field="holding" class="form-control">
                                                </Select>
                                            </div>
                                            <div class="btn-group">
                                                <% if(controller.allowNewHolding()){%>
                                                <button type="button" id="new_holding_btn" class="btn btn-nrlais" onclick="$('#holding_type_modal').modal('show')"><%=controller.text("New Holding")%> </button>
                                                <%} if(controller.allowDeleteHolding()){%>
                                                <button type="button" id="new_holding_btn" class="btn btn-nrlais" onclick="lrManipulatorInit.deleteHolding()"><%=controller.text("Delete Holding")%> </button>
                                                <%}%>
                                            </div>
                                        </div>
                                    </td>

                                    <td><%=controller.text("Holding Type")%>: <span id="holding_type_label"><%=controller.holdingTypeText(controller.holding.holdingType)%></span></td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="button" id="go_to_application" class="btn btn-nrlais" onclick="transaction_showTranDetail('<%=controller.tran.transactionUID%>',<%=controller.tran.transactionType%>, {noManipulate: true})" value=<%=controller.text("View Application")%>/>                                                
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="x_content has-scroll" style='height:95%'>
                            <div id="appContent" class="col-md-12">
                                <div class="col-md-12 bhoechie-tab-container">
                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 bhoechie-tab-menu">
                                        <div class="list-group">                  
                                            <a href="#" class="list-group-item active"><h4 class="fa fa-map-marker"></h4><%=controller.text("Parcels")%></a>
                                                <% if(controller.showHolders()){%>
                                            <a href="#" class="list-group-item"><h4 class="fa fa-user"></h4><%=controller.text("Holder")%> </a>
                                            <%} if(controller.showRenters()){%>
                                            <a href="#" class="list-group-item"><h4 class="fa fa-registered"></h4><%=controller.text("Rent")%></a>
                                                <%} if(controller.showRestriction()){%>
                                            <a href="#" class="list-group-item"><h4 class="fa fa-minus-circle"></h4><%=controller.text("Restriction")%></a>
                                                <%} if(controller.showServitude()){%>
                                            <a href="#" class="list-group-item"><h4 class="fa fa-minus-circle"></h4><%=controller.text("Servitude/Easement")%></a>
                                                <%}%>
                                            <a href="#" class="list-group-item"><h4 class="fa fa-users"></h4><%=controller.text("Parties List")%></a>
                                        </div>

                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 bhoechie-tab has-scroll">

                                        <div class="bhoechie-tab-content active">
                                            <div id="parcels_table">

                                            </div>

                                            <div class="align-right">
                                                <% if(controller.allowParcelTransfer()){%>
                                                <input type="button" class="btn btn-nrlais" id="parcel_tranfer" value=<%=controller.text("Transfer")%> style="display:none"/>
                                                <%} if(controller.allowBoundaryCorrection()){%>
                                                <input type="button" class="btn btn-nrlais" id="parcel_boundary_correction" value=<%=controller.text("Edit Boundary")%> style="display:none"/>
                                                <%} if(controller.allowAddParcel()){%>
                                                <input type="button" class="btn btn-nrlais" id="parcel_add_btn" value=<%=controller.text("Add Parcel")%>/>
                                                <%}%>
                                            </div>

                                        </div>   
                                        <% if(controller.showHoldersTab()){%>
                                        <div class="bhoechie-tab-content">
                                            <div id="holders_table">

                                            </div>
                                            <div class="align-right">
                                                <% if(controller.allowParcelTransfer()){%>
                                                <input type="button" class="btn btn-nrlais" id="transfer_holder" value=<%=controller.text("Transfer Holder")%> style="display:none"/>
                                                <%} if(controller.allowAddHolder()){%>
                                                <input type="button" id="holders_add_btn" class="btn btn-nrlais" value=<%=controller.text("Add Party")%>/>
                                                <%}%>
                                            </div>
                                        </div>
                                        <div class="bhoechie-tab-content">
                                            <div id="rents_table">
                                                <table  class="table table-striped jambo_table">
                                                    <thead>
                                                        <tr>
                                                            <td><%=controller.text("Full Name")%></td>
                                                            <td><%=controller.text("Sex")%></td>
                                                            <td><%=controller.text("Age")%></td>
                                                            <td><%=controller.text("Martial Status")%></td>
                                                            <td><%=controller.text("Family Role")%></td>
                                                            <td><%=controller.text("Guardian")%></td>
                                                            <td><%=controller.text("Parcel")%></td>
                                                            <td><%=controller.text("Parcel Area")%></td>
                                                            <td><%=controller.text("Rented/Leaed Area")%></td>
                                                            <td><%=controller.text("From")%></td>
                                                            <td><%=controller.text("To")%></td>
                                                            <td><%=controller.text("Lease/Rent Value")%></td>
                                                            <td></td>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="rent_rows"> 
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="align-right">
                                                <input type="button" id="rents_add_btn" class="btn btn-nrlais" value=<%=controller.text("Add Rent")%>/>
                                            </div>
                                        </div>
                                        <div class="bhoechie-tab-content">
                                            <div id="restriction_table">
                                                <table  class="table table-striped jambo_table">
                                                    <thead>
                                                        <tr>
                                                            <td><%=controller.text("Full Name")%></td>
                                                            <td><%=controller.text("Sex")%></td>
                                                            <td><%=controller.text("Age")%></td>
                                                            <td><%=controller.text("Martial Status")%></td>
                                                            <td><%=controller.text("Family Role")%></td>
                                                            <td><%=controller.text("Guardian")%></td>
                                                            <td><%=controller.text("Parcel")%></td>
                                                            <td><%=controller.text("Parcel Area")%></td>
                                                            <td><%=controller.text("Restriction Type")%></td>
                                                            <td><%=controller.text("From")%></td>
                                                            <td><%=controller.text("To")%></td>                          
                                                            <td></td>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="restriction_rows"> 
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="align-right">
                                                <input type="button" id="restrictions_add_btn" class="btn btn-nrlais" value=<%=controller.text("Add Restriction")%>/>
                                            </div>
                                        </div>
                                        <div class="bhoechie-tab-content">
                                            <div id="servitude_table">
                                                <table  class="table table-striped jambo_table">
                                                    <thead>
                                                        <tr>
                                                            <td><%=controller.text("Full Name")%></td>
                                                            <td><%=controller.text("Sex")%></td>
                                                            <td><%=controller.text("Age")%></td>
                                                            <td><%=controller.text("Martial Status")%></td>
                                                            <td><%=controller.text("Family Role")%></td>
                                                            <td><%=controller.text("Guardian")%></td>
                                                            <td><%=controller.text("Parcel")%></td>
                                                            <td><%=controller.text("Parcel Area")%></td>
                                                            <td><%=controller.text("Servitude Type")%></td>
                                                            <td><%=controller.text("From")%></td>
                                                            <td><%=controller.text("To")%></td>                               
                                                            <td></td>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="servitude_rows"> 
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="align-right">
                                                <input type="button" id="servitude_add_btn" class="btn btn-nrlais" value=<%=controller.text("Add Servitude")%>/>
                                            </div>
                                        </div>
                                        <%}%>
                                        <div class="bhoechie-tab-content">
                                            <div id="parties_table">

                                            </div>
                                            <% if(controller.allowAddParty()){%>
                                            <div class="align-right">
                                                <input id="parties_add_btn" type="button" class="btn btn-nrlais" id="parties_table" value=<%=controller.text("Add Party")%>/>
                                            </div>
                                            <%}%>
                                        </div>  
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="map col-md-4" style='height:95%'>
                <div class="x_panel" style='height:100%'>
                    <div class="x_content has-scroll" id="map" style='height:100%'>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade " id="holding_type_modal" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body" >
                        <input type="button"  onclick="lrManipulatorInit.createHolding(HOLDING_TYPE_PRIVATE)" value=<%=controller.text("Private")%> />
                        <input type="button" onclick="lrManipulatorInit.createHolding(HOLDING_TYPE_STATE)" value=<%=controller.text("State")%> />
                        <input type="button" onclick="lrManipulatorInit.createHolding(HOLDING_TYPE_COMMUNAL)" value=<%=controller.text("Commune")%> />

                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade " id="holding_selector_modal" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body" id="holdings_list">



                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade " id="busy_clue_modal" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <img src="/assets/IMAGES/busy.gif"/>
                        <span id="busy_clue_message"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade " id="application_detail" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <button type="button" class="btn btn-primary btn-circle" onclick='refresh()' titel="refresh"><i class="fa fa-refresh"></i></button>
                    </div>            <!-- /modal-header -->

                    <div class="modal-body" id="transactionContent">
                        <iframe id="transactionContent-iframe" name="iframe"></iframe>

                    </div>
                    <div class="modal-footer"> </div>
                </div>
            </div>
        </div>
    </body>

</html>



