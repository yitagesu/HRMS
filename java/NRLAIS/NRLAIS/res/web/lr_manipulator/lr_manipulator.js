function lrManipulatorInit(tranData) {
    var parcels_picker;
    var holders_picker;
    var parties_picker;
    var currentHoldingUID = tranData.data.holdingUID;
    var holdingPartyItems;
    var parties = [];
    var parcelItems = [];
    var holders = [];
    var rents = [];
    var restrictions = [];
    var servitudes = [];
    
    var simpleCorrection=window.location.href.indexOf("tran_type="+TRAN_TYPE_SIMPLE_CORRECTION)>0;
    

    $("[field=holding]").click(function ()
    {
        currentHoldingUID = $("[field=holding]").val();
        loadHolding();
    });

    lrManipulatorInit.holdingSelected=function(ht)
    {
        $("#holding_selector_modal").modal('hide');
        lrManipulatorInit.selectHolding_then(ht);
    }
    var holding_button_template='<input type="button" onclick="lrManipulatorInit.holdingSelected(\'<%=holdingUID%>\')" value="<%=holding%>" />';
    function selectHoldingType(then)
    {
        lrManipulatorInit.selectHolding_then = then;
        $("#holdings_list").html("");

        $("[field=holding] option").each(function ()
        {
            var huid=$(this).attr("value");
            if(huid==currentHoldingUID)
                return;
            var html=ejs.render(holding_button_template,{locals:{holdingUID:huid,holding:$(this).text()}});
            $("#holdings_list").append(html);
        });
       
        $("#holding_selector_modal").modal('show');
    }
    ;
    lrManipulatorInit.deleteHolding = function ()
    {
        if (currentHoldingUID == tranData.data.holdingUID)
        {
            alert('It is not allowed to delete the main holding');
            return;
        }
        bootbox.confirm("Are you sure you want to delete this holding?",
                function (result) {
                    if (result) {
                        sendCommand({
                            opType: "com.intaps.nrlais.model.tran.ManualTransactionData$LrOpRemoveHolding",
                            holdingUID: currentHoldingUID
                        },
                                function (data) {
                                    if (data.error) {
                                        alert(data.error);
                                    } else {
                                        currentHoldingUID = tranData.data.holdingUID;
                                        loadHolding();
                                    }

                                }, function () {
                            alert('Communication error');
                            then(false);
                        });
                    } 
                });
    };
    lrManipulatorInit.createHolding = function (ht)
    {
        $('#holding_type_modal').modal('hide')
        sendCommand({
            opType: "com.intaps.nrlais.model.tran.ManualTransactionData$LrOpUpdateHoldingInfo",
            holding: {holdingType: ht}
        },
                function (data) {
                    if (data.error) {
                        alert(data.error);
                    } else {
                        currentHoldingUID = data.res;
                        loadHolding();
                    }

                }, function () {
            alert('Communication error');
            then(false);
        });
    };

    function sendCommand(cmd, success, error) {
        $.ajax({
            url: "/api/lr_operation?tran_uid=" + tranData.transactionUID,
            method: "POST",
            data: JSON.stringify(cmd),
            contentType: "application/json",
            dataType: "json",
            success: success,
            error: error
        });
    }

    function isServitude(restriction) {
        return lookUPCode("static.servitude", restriction) != ""
    }
    var rentMan = rentManipulator();
    var restrictionMan = restrictionManipulator({tableBodySelector: "#restriction_rows", addButtonSelector: "#restrictions_add_btn"
        , persons: function ()
        {
            var ret = [];
            for (var i in parties)
                if (parties[i].role == PARTY_ROLE_NONE || parties[i].role == PARTY_ROLE_RESTRICTOR)
                    ret.push(parties[i]);
            return ret;
        }, types: function ()
        {
            return LU_TABLE["static.restrictive_interest"];
        }
    });
    var servitudeMan = restrictionManipulator({tableBodySelector: "#servitude_rows", addButtonSelector: "#servitude_add_btn"
        , persons: function ()
        {
            var ret = [];
            for (var i in parties)
                if (parties[i].role == PARTY_ROLE_NONE || parties[i].role == PARTY_ROLE_SERVITUDE_BENEFICIARY)
                    ret.push(parties[i]);
            return ret;
        }, types: function ()
        {
            return LU_TABLE["static.servitude"];
        }});
    function loadControls()
    {
        var pi = [];
        for (var i in parties)
        {
            pi.push({party: parties[i].party, existingPartyUID: parties[i].party.partyUID});
        }
        parties_picker.setParyItems(pi);
        holders_picker.setParyItems(holdingPartyItems);
        parcels_picker.setParcelItems(parcelItems);
        rentMan.loadData({tranData: tranData, parcelItems: parcelItems, parties: parties, loadHolding: loadHolding, rents: rents, sendCommand: sendCommand});
        restrictionMan.loadData({tranData: tranData, parcelItems: parcelItems, parties: parties, loadHolding: loadHolding, restrictions: restrictions, sendCommand: sendCommand});
        servitudeMan.loadData({tranData: tranData, parcelItems: parcelItems, parties: parties, loadHolding: loadHolding, restrictions: servitudes, sendCommand: sendCommand});
    }

    function loadParties()
    {
        $.ajax('/api/get_tran_parties?schema=nrlais_transaction&tran_uid=' + tranData.transactionUID,
                {
                    method: 'GET',
                    success: function (data) {
                        for (var i in data.res)
                        {
                            var p = data.res[i];
                            if (!parties[p.partyUID])
                                parties[p.partyUID] = {party: p, role: PARTY_ROLE_NONE};
                        }
                        loadControls();

                    }
                });
    }
    function loadHoldings()
    {
        $.ajax('/api/get_tran_holdings?schema=nrlais_transaction&tran_uid=' + tranData.transactionUID + "&content_depth=" + LADM_CONTENT_HOLDING,
                {
                    method: 'GET',
                    success: function (data) {
                        var sel = $("[field=holding]");
                        sel.html("");

                        for (var i in data.res)
                        {
                            var h = data.res[i];
                            var option = "<option value='" + h.holdingUID + "'>" + formatHoldingSeqNo(h.holdingSeqNo) + "</option>";
                            sel.append(option);
                        }

                        sel.val(currentHoldingUID);

                    }
                });
    }
    function loadHolding() {

        $.ajax('/api/get_holding?schema=nrlais_transaction&holding_uid=' + currentHoldingUID,
                {
                    method: 'GET',
                    success: function (data) {
                        var holding = data.res;
                        parties = [];
                        rents = [];
                        restrictions = [];
                        servitudes = [];
                        holders = [];
                        parcelItems = [];
                        holdingPartyItems = [];
                         $('#holding_type_label').text(lookUPCode('nrlais_sys.t_cl_holdingtype',holding.holdingType));
                        holding.parcels.forEach(function (p) {
                            parcelItems.push({parcel: p, existingParcelUID: p.parcelUID});
                            p.rights.forEach(function (right) {

                                if (right.rightType == RIGHT_TYPE_RENT || right.rightType == RIGHT_TYPE_LEASE) {
                                    rents.push(right);
                                    if (!parties[right.partyUID])
                                        parties[right.partyUID] = {party: right.party, role: PARTY_ROLE_TENANT};
                                } else if (right.rightType == RIGHT_TYPE_SUR || right.rightType == RIGHT_TYPE_PUR) {
                                    if (!parties[right.partyUID]) {
                                        holdingPartyItems.push({
                                            party: right.party,
                                            share: right.share,
                                            existingPartyUID: right.partyUID
                                        });
                                        holders[right.partyUID] = right;

                                        parties[right.partyUID] = {party: right.party, role: PARTY_ROLE_LAND_HOLDER};
                                    }
                                } else
                                    alert('Unsupported right type:' + right.rightType);

                            });
                            p.restrictions.forEach(function (restriction) {
                                if (isServitude(restriction.restrictionType)) {
                                    servitudes.push(restriction);
                                    if (!parties[restriction.partyUID])
                                        parties[restriction.partyUID] = {party: restriction.party, role: PARTY_ROLE_SERVITUDE_BENEFICIARY};
                                } else {
                                    restrictions.push(restriction);
                                    if (!parties[restriction.partyUID])
                                        parties[restriction.partyUID] = {party: restriction.party, role: PARTY_ROLE_RESTRICTOR};
                                }
                            });
                        });
                        loadParties();
                        loadHoldings();
                    },
                    dataType: 'json'
                });
    }

    var map = NrlaisMap(map_server, 'map');
    map.load();
    //<editor-fold desc="parties list">
    parties_picker = getPartyListPicker({
        containerEl: "parties_table",
        addButtonEl: "parties_add_btn",
        share: false,
        idDoc: false,
        edit: true,
        delete: simpleCorrection?false:true,
        pom: false,
        representative: false,
        member: true,
        guardian: true
    });
    parties_picker.beforeDelete = function (party, then) {
        bootbox.confirm("Are you sure you want to delete this party.",
                function (result) {
                    if (result) {
                        sendCommand({
                            opType: "com.intaps.nrlais.model.tran.ManualTransactionData$LrOpDeleteParty",
                            holdingUID: currentHoldingUID,
                            partyUID: party.party.partyUID
                        },
                                function (data) {
                                    if (data.error) {
                                        alert(data.error);
                                        then(false);
                                    } else {
                                        alert("Parcels Deleted");
                                        then(true);
                                    }

                                }, function () {
                            alert('Communication error');
                            then(false);
                        });
                    } else
                        then(false);
                });
    }
    parties_picker.beforeSave = function (party, then) {
        if (!party.existingPartyUID)
            party.party.partyUID = null;
        sendCommand({
            opType: "com.intaps.nrlais.model.tran.ManualTransactionData$LrOpSavePartyInfo",
            holdingUID: currentHoldingUID,
            party: party.party,
        }, function (data) {
            if (data.error) {
                alert(data.error);
                then(false);
            } else
            {
                loadHolding();
                then(true);
            }

        }, function () {
            alert('Communication error');
            then(false);
        });
    };
    //</editor-fold>

    //<editor-fold desc="configure parcel section">
    parcels_picker = getParcelListPicker({
        containerEl: "parcels_table",
        addNewButtonEl: "parcel_add_btn",
        allowExisting: true,
        allowNew: true,
        edit: true,
        delete: simpleCorrection?false:true,
        split: simpleCorrection?false:true,
        select: simpleCorrection?false:true,
        mapLink: true,
        showOnMap: function (parcelUID) {
            map.zoomParcel("nrlais_transaction", parcelUID);
        },
    });
    parcels_picker.onSelectionChanged = function () {
        if (parcels_picker.getSelected().length > 0)
        {
            $("#parcel_tranfer").show();
            $("#parcel_boundary_correction").show();
        } else
        {
            $("#parcel_boundary_correction").hide();
            $("#parcel_tranfer").hide();
        }
    };
    $('#parcel_boundary_correction').click(function () {

        var parcels = [];
        parcels_picker.getSelected().forEach(function(p){
            parcels.push(p.parcel.parcelUID);
        });
        sendCommand({
            opType: "com.intaps.nrlais.model.tran.ManualTransactionData$LrOpStartBoundaryCorrection",
            parcels: parcels,
        }, function (data) {
            if (data.error) {
                alert(data.error);
            } else {
                alert("CMSS boundary correction task created. Go to CMSS and periform the boundary correction.");
            }

        }, function () {
            alert('Communication error');
            then(false);
        });
        parcels_picker.clearSelection();
        $("#parcel_boundary_correction").hide();
    });

    $('#parcel_tranfer').click(function () {

        var parcels = [];
        parcels_picker.getSelected().forEach(function(p){
            parcels.push(p.parcel.parcelUID);
        });
        selectHoldingType(function (huid) {
            sendCommand({
                opType: "com.intaps.nrlais.model.tran.ManualTransactionData$LrOpTransferParcels",
                parcels: parcels,
                sourceHolding: currentHoldingUID,
                destinationHolding: huid
            }, function (data) {
                if (data.error) {
                    alert(data.error);
                } else 
                {
                    loadHolding();
                }

            }, function () {
                alert('Communication error');
                then(false);
            });
            parcels_picker.clearSelection();
            $("#parcel_tranfer").hide();
        });
    });
    parcels_picker.beforeDelete = function (parcel, then) {
        bootbox.confirm("Are you sure you want to delete this parcel.",
                function (result) {
                    if (result) {
                        sendCommand({
                            opType: "com.intaps.nrlais.model.tran.ManualTransactionData$LrOpDeleteParcel",
                            parcelUID: parcel.parcel.parcelUID,
                        },
                                function (data) {
                                    if (data.error) {
                                        alert(data.error);
                                        then(false);
                                    } else {
                                        alert("Parcels Deleted");
                                        then(true);
                                    }

                                }, function () {
                            alert('Communication error');
                            then(false);
                        });
                    } else
                        then(false);
                });
    }
    parcels_picker.beforeSave = function (parcel, then) {
        sendCommand({
            opType: "com.intaps.nrlais.model.tran.ManualTransactionData$LrOpUpateParcel",
            parcel: parcel.parcel,
            holdingUID: currentHoldingUID,
        }, function (data) {
            if (data.error) {
                alert(data.error);
                then(false);
            } else {
                alert("Parcel updated");
                then(true);
            }

        }, function () {
            alert('Communication error');
            then(false);
        });
    };
    parcels_picker.beforeSplit = function (parcel, n, then) {
        sendCommand({
            opType: "com.intaps.nrlais.model.tran.ManualTransactionData$LrOpSplitParcel",
            parcelUID: parcel.existingParcelUID,
            n: n,
        }, function (data) {
            if (data.error) {
                alert(data.error);
                then(false);
            } else {
                loadHolding();
                alert("CMSS parcel split task created. Open CMSS and periform the split operation.");
                then(true);
            }

        }, function () {
            alert('Communication error');
            then(false);
        });
    };
    //end: configure parcel section
    //</editor-fold>
    //<editor-fold desc="configure holding section">
    //configure holders section
    holders_picker = getPartyListPicker({
        containerEl: "holders_table",
        addButtonEl: "holders_add_btn",
        share: true,
        idDoc: false,
        edit: true,
        delete: true,
        pom: false,
        representative: false,
        member: true,
        guardian: true,
        select: simpleCorrection?false:true,
    });
    holders_picker.onSelectionChanged= function () {
        if (holders_picker.getSelected().length > 0)
        {
            $("#transfer_holder").show();
        } else
        {
            $("#transfer_holder").hide();
        }
    };
    $('#transfer_holder').click(function () {

        var parties = [];
        holders_picker.getSelected().forEach(function(p){
            parties.push(p.party.partyUID);
        });
        selectHoldingType(function (huid) {
            sendCommand({
                opType: "com.intaps.nrlais.model.tran.ManualTransactionData$LrOpTransferHolders",
                parties: parties,
                sourceHolding: currentHoldingUID,
                destinationHolding: huid
            }, function (data) {
                if (data.error) {
                    alert(data.error);
                } else 
                {
                    loadHolding();
                }

            }, function () {
                alert('Communication error');
                then(false);
            });
            $("#transfer_holder").hide();
        });
    });
    holders_picker.beforeDelete = function (party, then) {
        sendCommand({
            opType: "com.intaps.nrlais.model.tran.ManualTransactionData$LrOpRemoveHolder",
            partyUID: party.existingPartyUID,
            holdingUID: currentHoldingUID,
        }, function (data) {
            if (data.error) {
                alert(data.error);
                then(false);
            } else {
                alert("Holder deleted");
                then(true);
            }

        }, function () {
            alert('Communication error');
            then(false);
        });
    }
    holders_picker.beforeSave = function (party, then) {
        sendCommand({
            opType: "com.intaps.nrlais.model.tran.ManualTransactionData$LrOpUpdateHolder",
            partyUID: party.existingPartyUID,
            holdingUID: currentHoldingUID,
            party: party.party,
            share: party.share,
        }, function (data) {
            if (data.error) {
                alert(data.error);
                then(false);
            } else {
                loadHolding();
                then(true);
            }

        }, function () {
            alert('Communication error');
            then(false);
        });
    };
    //end: configure holders section


    loadHolding();
}
