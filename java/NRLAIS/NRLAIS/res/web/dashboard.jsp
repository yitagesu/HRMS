<%-- 
    Document   : dashboard
    Created on : Dec 22, 2017, 11:04:56 AM
    Author     : yitagesu
--%>
<%@page import="com.intaps.nrlais.*"%>
<%@page import="com.intaps.nrlais.api.*"%>
<%@page import="com.intaps.nrlais.model.*"%>
<%@page import="com.intaps.nrlais.viewmodel.*"%>
<%@page import="com.intaps.nrlais.model.tran.*"%>
<%@page import="com.intaps.nrlais.repo.*"%>
<%@page import="com.intaps.nrlais.tran.*"%>
<%@page import="com.intaps.nrlais.util.*"%>
<%@page import="com.intaps.nrlais.controller.*"%>
<%@page import="java.util.*"%>
<%@page pageEncoding="UTF-8" %>
<%
   
    DashboardViewController controller=new DashboardViewController(request,response);
    HeaderViewController headerController=new HeaderViewController(request,response,controller.showNewTransactionButton());
    
%>
<html>
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>NRLAIS</title>
        <link rel="stylesheet" href="/assets/CSS/bootstrap.min.css"/>
        <link rel="stylesheet" href="/assets/CSS/bootstrap-popover-x.min.css"/>
        <link rel="stylesheet" href="/assets/CSS/custom.css"/>
        
        <link href="/assets/ol/ol.css" type="text/css" rel="stylesheet">        
        <script src="/assets/ol/ol-debug.js"></script>
        <script src="/assets/JS/nrlais_map.js"></script>
        <script src="/assets/ol/proj4.js"></script>
        
        <script src="/assets/JS/jquery.min.js"></script>
        <script src="/assets/JS/jquery-ui.js"></script>
        <script src="/assets/JS/bootstrap.min.js"></script>
        <script src="/assets/JS/bootstrap-popover-x.min.js"></script>
        <script src="/assets/JS/bootbox.js"></script>        
        <script src="assets/JS/string.js"></script>
        
        <script src="assets/JS/lang.js.jsp"></script>
        
        <script src="assets/JS/et_dual_cal.js"></script>

        <script src="/assets/JS/js_constants.jsp"></script>
        <script src="/assets/JS/general.js"></script>
        <script src="/assets/JS/transaction.js"></script>
        <script src="/assets/JS/dashboard.js"></script>
        <script src="/assets/JS/data_browser.js"></script>
    
    </head>

    <body id="body">
        <%@ include file="header.jsp" %>
        <div class="dashboard-body">
            <div class="list-of-transc col-md-6" style='height:95%'>
                <div id="list_application" style='height:100%'>
                    <div class="x_panel " id="" style='height:100%'>
                        <div class="x_title">
                            <div class="input-group">
                                <input id='db_tran_search_text' type="text" type="button" class="form-control" placeholder="Search...">

                                <span class="input-group-addon"><a href="javascript:dashboard.searchTransaction()"><i class="fa fa-search"></i></a></span>
                            </div>
                        </div>
                        <div id='db_search_result' class="x_content has-scroll" style='height:92%'>
                            
                        </div>
                    </div>
                </div>
            </div>

            <div class="map col-md-6" style="height: 95%" >
                <div class="x_panel" style="height: 100%" >
                    <div class="x_content has-scroll" id="map" style="height: 100%" >
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade " id="application_detail" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <button type="button" class="btn btn-primary btn-circle" onclick='refresh()' titel="refresh"><i class="fa fa-refresh"></i></button>
                    </div>            <!-- /modal-header -->

                    <div class="modal-body" id="transactionContent">
                        <iframe id="transactionContent-iframe" name="iframe"></iframe>

                    </div>
                    <div class="modal-footer"> </div>
                </div>
            </div>
        </div>
        
                <div class="modal fade " id="holding_view_modal" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                    </div>            <!-- /modal-header -->

                    <div class="modal-body">
                        <div id="holdingContent">

                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="input-form">
                        </div>
                    </div>
                </div>
            </div>
        </div>
                <div class="modal fade " id="application_detail" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>            <!-- /modal-header -->

                    <div class="modal-body" id="transactionContent">
                        <iframe id="transactionContent-iframe" name="iframe"></iframe>

                    </div>
                    <div class="modal-footer"> </div>
                </div>
            </div>
        </div>
        <script>
        var map;
         var map_server = '<%=controller.mapServerUrl()%>';
           $(document).ready(function()
           {
                
               map=NrlaisMap('<%=controller.mapServerUrl()%>','map');
               map.load();
               
                               $(document).on('click', "div.bhoechie-tab-menu>div.list-group>a", function (e) {
                    e.preventDefault();
                    $(this).siblings('a.active').removeClass("active");
                    $(this).addClass("active");
                    var index = $(this).index();
                    $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
                    $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
                });
               
               dashboard.searchTransaction();
           })
        </script>
    </body>
</html>
