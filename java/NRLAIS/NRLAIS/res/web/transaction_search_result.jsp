<%@page import="com.intaps.nrlais.repo.*"%>
<%@page import="com.intaps.nrlais.*"%>
<%@page import="com.intaps.nrlais.controller.*"%>
<%@page import="com.intaps.nrlais.model.*"%>
<%@page import="com.intaps.nrlais.util.*"%>
<%@page import="java.util.*"%>
<%@page import ="java.text.DecimalFormat" %>
<%@page pageEncoding="UTF-8" %>
<%
        TranscationSearchResultViewController controller=new TranscationSearchResultViewController(request,response);    
%>
<table class="table table-striped jambo_table table-responsive" id="transcation_table">
    <thead>
        <tr class="headings">
            <th class="column-title"><%=controller.text("Transaction ID")%></th>
            <th class="column-title"><%=controller.text("Application Type")%></th>
            <th class="column-title"><%=controller.text("Registered Date")%></th>
            <th class="column-title"><%=controller.text("Status")%></th>
        </tr>
    </thead>
    <tbody class="">
        <%for(LATransaction t:controller.tran){
        %>
        <tr id="<%=t.transactionUID%>" onclick="transaction_showTranDetail('<%=t.transactionUID%>','<%=t.transactionType%>');">
            <td><%=t.nrlais_kebeleid%>/<%=t.seqNo%></td>
            <td><%=controller.transactionTypeName(t.transactionType)%></td>
            <td id="appDate"><%=EthiopianCalendar.ToEth(t.time).toString()+" EC"%></td>
            <td><%=controller.transactionStatusName(t.status)%></td>
        </tr>
        <%}%>
    </tbody>
</table>

