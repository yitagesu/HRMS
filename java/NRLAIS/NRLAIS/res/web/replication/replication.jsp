<%@page import="com.intaps.nrlais.*"%>
<%@page import="com.intaps.nrlais.api.*"%>
<%@page import="com.intaps.nrlais.model.*"%>
<%@page import="com.intaps.nrlais.viewmodel.*"%>
<%@page import="com.intaps.nrlais.model.tran.*"%>
<%@page import="com.intaps.nrlais.repo.*"%>
<%@page import="com.intaps.nrlais.tran.*"%>
<%@page import="com.intaps.nrlais.util.*"%>
<%@page import="com.intaps.nrlais.controller.*"%>
<%@page import="com.intaps.nrlais.replication.*"%>
<%@page import="java.util.*"%>
<%@page pageEncoding="UTF-8" %>
<%
    synchronized(ReplicationFileGeneratorWorker.worker)
    {
        if(!ReplicationFileGeneratorWorker.worker.isIdle())
        {
            response.sendRedirect("/replication/download.jsp");
            return;
        }
    }
    ReplicationViewController controller=new ReplicationViewController(request,response);
    HeaderViewController headerController=new HeaderViewController(request,response,false);
    ReplicationFacade facade=new ReplicationFacade(Startup.getSessionByRequest(request));
%>
<html>
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>NRLAIS</title>
        <link rel="stylesheet" href="/assets/CSS/bootstrap.min.css"/>
        <link rel="stylesheet" href="/assets/CSS/bootstrap-popover-x.min.css"/>
        <link rel="stylesheet" href="/assets/CSS/custom.css"/>

        <link href="/assets/ol/ol.css" type="text/css" rel="stylesheet">        
        <script src="/assets/ol/ol-debug.js"></script>
        <script src="/assets/JS/nrlais_map.js"></script>
        <script src="/assets/ol/proj4.js"></script>

        <script src="/assets/JS/jquery.min.js"></script>
        <script src="/assets/JS/jquery-ui.js"></script>
        <script src="/assets/JS/bootstrap.min.js"></script>
        <script src="/assets/JS/bootstrap-popover-x.min.js"></script>
        <script src="/assets/JS/bootbox.js"></script>        


        <script src="/assets/JS/js_constants.jsp"></script>
        <script src="/assets/JS/general.js"></script>
        <script src="/assets/JS/transaction.js"></script>
        <script src="/assets/JS/dashboard.js"></script>
        <script src="/assets/JS/data_browser.js"></script>
        <script>
            function selectAllChanged()
            {
                var checked = $("#select_all").is(":checked");
                $("tbody tr").each(function ()
                {
                    if (checked)
                        $(this).find("input[type=checkbox]").prop("checked", true);
                    else
                        $(this).find("input[type=checkbox]").prop("checked",false);
                });
                showHideDownloadButton();
            }
            function showHideDownloadButton()
            {
                var hasChecked = false;
                var hasUnchecked=false;
                $("tbody tr").each(function ()
                {


                    if($(this).find("input[type=checkbox]").is(":checked"))
                    {
                        if (!hasChecked )   hasChecked = true;
                    }   
                    else
                    {
                        if (!hasUnchecked )   hasUnchecked = true;
                    }
                    $("#select_all").prop("checked",hasChecked && !hasUnchecked);
                });
                if (hasChecked)
                    $("#btn_download").show();
                else
                    $("#btn_download").hide();
            }
            function selectChanged(woredaID)
            {
                showHideDownloadButton();
            }
            function download()
            {
                var par = {
                    items: []
                };
                $("tbody tr").each(function ()
                {
                    if ($(this).find("input[type=checkbox]").is(":checked"))
                    {
                        var txt = $(this).find("input[type = text]").val();
                        par.items.push({
                            woredaID: $(this).attr('id'),
                            repNo: (txt == null || txt.length == 0) ? 0 : parseInt(txt)
                        });
                    }
                });
                $.post({
                    url: "/api/replication_job",
                    contentType: "application/json",
                    data: JSON.stringify(par),
                    success: function ()
                    {
                        window.location = "/replication/download.jsp";
                    }
                    , error: function ()
                    {
                        alert('Error');
                    }
                });
            }
            function uploadData()
            {
                document.getElementById("upload_form").submit();                
            }
        </script>
    </head>
    <body id="body">
        <%@ include file="/header.jsp" %>
            <div class="replication">
            <table class="table table-striped jambo_table">
                <thead>
                    <tr class="headings">
                        <th><input id="select_all" type="checkbox"onchange="selectAllChanged()"/></th>
                        <th class="column-title">Woreda ID</th>
                        <th class="column-title">Replication No</th>
                        <th class="column-title">Last Log Time (GC)</th>
                        <th class="column-title"></th>
                    </tr>
                </thead>
                <tbody class="">
                    <%for(WoredaItem w:controller.woredas()){
                    %>
                    <tr id="<%=w.woredaID%>">
                        <td><input type="checkbox" onchange="selectChanged('<%=w.woredaID%>')"/></td>
                        <td><%=w.woredaID%></td>
                        <td><%=controller.repNo(w)%></td>
                        <td><%=controller.repDate(w)%></td>
                        <td><input type="text" placeholder="Replication No"/></td>
                    </tr>
                    <%}%>
                </tbody>
            </table>
                <div class="col-md-4">
            <input id="btn_download" style="display:none" type="button" value="Download" onclick="download()" class="btn btn-nrlais"/>
            <%if(!facade.isLowestLevel()){%>
            <form id="upload_form" method="POST" action="/api/replication_upload" enctype="multipart/form-data">
                <input name="file" type="file" class="uploadimg"/>
                <input id="btn_upload" type="button" value="Upload" onclick="uploadData()" class="btn btn-nrlais"/>
            </form>
            <%}%>
            </div>
            <div class="replication-status align-center">
                <h4>Replication Status</h4>
                <div><%=controller.replicationStatus()%></div>
            </div>
        </div>
        
    </body>

</html>
