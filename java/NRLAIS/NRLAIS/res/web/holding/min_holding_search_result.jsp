<%@page import="com.intaps.nrlais.repo.*"%>
<%@page import="com.intaps.nrlais.*"%>
<%@page import="com.intaps.nrlais.controller.*"%>
<%@page import="com.intaps.nrlais.controller.tran.*"%>
<%@page import="com.intaps.nrlais.model.*"%>
<%@page import="com.intaps.nrlais.util.*"%>
<%@page import="java.util.*"%>
<%@page import ="java.text.DecimalFormat" %>
<%@page pageEncoding="UTF-8" %>
<%
    HoldingViewController controller=new HoldingViewController(request,response);    
    
%>
<table class="table table-striped jambo_table">
    <thead>
        <tr>
            <td><%=controller.text("Kebele")%>: <%=controller.holding.nrlais_kebeleid%></td>
            <td><%=controller.text("Holding Seq.No")%>: <%=controller.holding.holdingSeqNo%></td>
            <td><%=controller.text("Holding Type")%>: <%=controller.holdingTypeText(controller.holding.holdingType)%></td>
        </tr>
    </thead>
    <tr>
        <td colspan="2">
            <table class="internal_table internal_table_border">
                <tr>
                    <td colspan="6">
                        <div class="pull-left"><label><%=controller.text("Holder")%></label></div>
                        <div class="pull-right"><a href="<%=controller.holdingMapLink()%>" class="btn btn-default btn-xs" title="Show Map"><i class="fa fa-map-marker"></i></a></div>
                    </td>

                </tr>
                <tr>
                    <td><%=controller.text("Name")%></td>
                    <td><%=controller.text("ID")%></td>
                    <td><%=controller.text("Sex")%></td>
                    <td><%=controller.text("Age")%></td>
                    <td><%=controller.text("Martial Status")%></td>
                    <td><%=controller.text("Share")%></td>
                </tr>
                <%for(LADM.Right r:controller.holding.getHolders()){
                
                %>
                <tr id="<%=r.party.partyUID%>">
                    <td><%=r.party.getFullName()%></td>
                    <td><%=r.party.idText==null?"":r.party.idText%></td>
                    <td><%=controller.sexText(r.party.sex)%></td>
                    <td id="minAgeOfHolder"><%=controller.age(r.party.dateOfBirth)%></td>
                    <td><%=controller.maritalStatusText(r.party.maritalstatus)%></td>
                    <td><%=r.share.num%>/<%=r.share.denum%></td>
                </tr>
                <%}%>

            </table>
        </td>

        <td>
            <table class="internal_table internal_table_border">
                <% double total=0.0;
                   DecimalFormat dec = new DecimalFormat("0.00");
                   for(LADM.Parcel p:controller.holding.parcels){
                        total += p.areaGeom;
                   } %>
                <tr>
                    <td colspan="3"><%=controller.holding.parcels.size()%> <%=controller.text("Parcels")%>, <%=controller.text("Total")%>: <%=dec.format(total)%> <%=controller.text("M")%><sup>2</sup></td>
                </tr>
                <tr>
                    <td><%=controller.text("Parcel No")%></td>
                    <td colspan="2"><%=controller.text("Area")%></td>
                </tr>
                <%for(LADM.Parcel p:controller.holding.parcels){%>
                <tr>
                    <td><%=controller.parcelNo(p)%></td>
                    <td><%=p.areaGeom%></td>
                    <td class="align-center"><a href="<%=controller.parcelMapLink(p)%>" class="btn btn-default btn-xs" title="Show Map"><i class="fa fa-map-marker"></i></a></td>
                </tr>
                <%}%>

            </table>
        </td>
    </tr>

    <%for(HoldingViewController.Rent rent:controller.rents()){%>
    <tr>
        <td colspan="2">
            <table class="internal_table internal_table_border">
                <tr>
                    <td colspan="6"><div class="pull-left"><label>Tenant/Leasee</label></div>
                        <div class="pull-right"><a href="<%=controller.rentMapLink(rent)%>" class="btn btn-default btn-xs" title="Show Map"><i class="fa fa-map-marker"></i></a></div></td>
                </tr>
                <tr>
                    <td>Name</td>
                    <td>ID</td>
                    <td>Sex</td>
                    <td>DOB</td>
                    <td>Martial Status</td>
                    <td>Share</td>
                </tr>

                <% for (LADM.Right tenat:rent.renter){ %>
                <tr id="<%=tenat.party.partyUID%>">
                    <td><%=tenat.party.getFullName()%></td>
                    <td><%=tenat.party.idText==null?"":tenat.party.idText%></td>
                    <td><%=controller.sexText(tenat.party.sex)%></td>
                    <td id="minAgeOfHolder"><%=controller.age(tenat.party.dateOfBirth)%></td>
                    <td><%=controller.maritalStatusText(tenat.party.maritalstatus)%></td>
                    <td><%=tenat.share.num%>/<%=tenat.share.denum%></td>
                </tr>
                <%}%>
                <tr><td colspan="6"><label>Rent/Lease Detail</label></td></tr>
                <tr>

                    <td>Type</td>
                    <td colspan="2">Start Date</td>
                    <td colspan="2">End Date</td>
                    <td>Amount</td>
                </tr>
                <% for (LADM.Right rents:rent.renter){ %>
                <tr>

                    <%if(rents.rightType==2){%>
                    <td>Lease</td>
                    <td colspan="2"><%=EthiopianCalendar.ToEth(rents.startLeaseDate).toString()+" EC"%></td>
                    <td colspan="2"><%=EthiopianCalendar.ToEth(rents.endLeaseDate).toString()+" EC"%></td>
                    <td><%=rents.leaseAmount%></td>
                    <%}else{%>
                    <td>Rent</td>
                    <td colspan="2"><%=EthiopianCalendar.ToEth(rents.startRentDate).toString()+" EC"%></td>
                    <td colspan="2"><%=EthiopianCalendar.ToEth(rents.endRentDate).toString()+" EC"%></td>
                    <td><%=rents.leaseAmount%></td>
                    <%}%>

                </tr>
                <%}%>
            </table>
        </td>

        <td>
            <table class="internal_table internal_table_border">
                <% double total_r=0.0;
                   DecimalFormat decimal = new DecimalFormat("0.00");
                   for(LADM.Parcel p:rent.parcels){
                        total_r += p.areaGeom;
                   } %>
                <tr>
                    <td colspan="3"><%=rent.parcels.size()%> Parcel's, Total: <%=decimal.format(total_r)%> M<sup>2</sup></td>
                </tr>
                <tr>
                    <td>Parcel No</td>
                    <td colspan="2">Rented Area</td>
                </tr>
                <%for(LADM.Parcel p:rent.parcels){%>
                <tr>
                    <td><%=p.seqNo%></td>   
                    <td><%=controller.rentArea(rent,p)%></td>
                    <td class="align-center"><a href="<%=controller.parcelMapLink(p)%>" class="btn btn-default btn-xs" title="Show Map"><i class="fa fa-map-marker"></i></a></td>
                </tr>
                <%}%>

            </table>
        </td>
    </tr>
    <%}%>
    <%for(HoldingViewController.Servitude serv:controller.servitudes()){%>
    <tr>
        <td colspan="2">
            <table class="internal_table internal_table_border">
                <tr>
                    <% for (LADM.Restriction ser:serv.servitude){ %>
                    <td colspan="6"><div class="pull-left"><label>Servitude/Easement </label>  (<%=controller.restrictionTypeText(ser.restrictionType)%>)</div>
                        <div class="pull-right"><a href="<%=controller.servitudeMapLink(serv)%>" class="btn btn-default btn-xs" title="Show Map"><i class="fa fa-map-marker"></i></a></div></td>
                                <%}%>
                </tr>
                <tr>
                    <td>Name</td>
                    <td>ID</td>
                    <td>Sex</td>
                    <td>DOB</td>
                    <td>Martial Status</td>
                </tr>

                <% for (LADM.Restriction servetude:serv.servitude){ %>
                <tr id="<%=servetude.party.partyUID%>">
                    <td><%=servetude.party.getFullName()%></td>
                    <td><%=servetude.party.idText==null?"":servetude.party.idText%></td>
                    <td><%=controller.sexText(servetude.party.sex)%></td>
                    <td id="minAgeOfHolder"><%=controller.age(servetude.party.dateOfBirth)%></td>
                    <td><%=controller.maritalStatusText(servetude.party.maritalstatus)%></td>
                </tr>
                <%}%>

            </table>
        </td>

        <td>
            <table class="internal_table internal_table_border">
                <% double total_r=0.0;
                   DecimalFormat decimal = new DecimalFormat("0.00");
                   for(LADM.Parcel p:serv.parcels){
                        total_r += p.areaGeom;
                   } %>
                <tr>
                    <td colspan="3"><%=serv.parcels.size()%> Parcel's, Total: <%=decimal.format(total_r)%> M<sup>2</sup></td>
                </tr>
                <tr>
                    <td>Parcel No</td>
                    <td colspan="2">Area</td>
                </tr>
                <%for(LADM.Parcel p:serv.parcels){%>
                <tr>
                    <td><%=p.seqNo%></td>
                    <td><%=p.areaGeom%></td>
                    <td class="align-center"><a href="<%=controller.parcelMapLink(p)%>" class="btn btn-default btn-xs" title="Show Map"><i class="fa fa-map-marker"></i></a></td>
                </tr>
                <%}%>

            </table>
        </td>
    </tr>
    <%}%>

    <%for(HoldingViewController.Restriction rest:controller.restrictions()){%>
    <tr>
        <td colspan="2">
            <table class="internal_table internal_table_border">
                <tr>
                    <% for (LADM.Restriction res:rest.restrict){ %>
                    <td colspan="6"><div class="pull-left"><label>Restriction</label> (<%=controller.restrictionTypeText(res.restrictionType)%>)</div>
                        <div class="pull-right"><a href="<%=controller.restrictionMapLink(rest)%>" class="btn btn-default btn-xs" title="Show Map"><i class="fa fa-map-marker"></i></a></div></td>
                                <%}%>
                </tr>
                <tr>
                    <td>Name</td>
                    <td>ID</td>
                    <td>Sex</td>
                    <td>DOB</td>
                    <td>Martial Status</td>
                </tr>

                <% for (LADM.Restriction res:rest.restrict){ %>
                <tr id="<%=res.party.partyUID%>">
                    <td><%=res.party.getFullName()%></td>
                    <td><%=res.party.idText==null?"":res.party.idText%></td>
                    <td><%=controller.sexText(res.party.sex)%></td>
                    <td id="minAgeOfHolder"><%=controller.age(res.party.dateOfBirth)%></td>
                    <td><%=controller.maritalStatusText(res.party.maritalstatus)%></td>
                </tr>
                <%}%>

            </table>
        </td>

        <td>
            <table class="internal_table internal_table_border">
                <% double total_r=0.0;
                   DecimalFormat decimal = new DecimalFormat("0.00");
                   for(LADM.Parcel p:rest.parcels){
                        total_r += p.areaGeom;
                   } %>
                <tr>
                    <td colspan="3"><%=rest.parcels.size()%> Parcel's, Total: <%=decimal.format(total_r)%> M<sup>2</sup></td>
                </tr>
                <tr>
                    <td>Parcel No</td>
                    <td colspan="2">Area</td>
                </tr>
                <%for(LADM.Parcel p:rest.parcels){%>
                <tr>
                    <td><%=p.seqNo%></td>
                    <td><%=p.areaGeom%></td>
                    <td class="align-center"><a href="<%=controller.parcelMapLink(p)%>" class="btn btn-default btn-xs" title="Show Map"><i class="fa fa-map-marker"></i></a></td>
                </tr>
                <%}%>

            </table>
        </td>
    </tr>
    <%}%>
</table>    