<%@page import="com.intaps.nrlais.repo.*"%>
<%@page import="com.intaps.nrlais.*"%>
<%@page import="com.intaps.nrlais.model.*"%>
<%@page import="com.intaps.nrlais.model.tran.*"%>
<%@page import="com.intaps.nrlais.util.*"%>
<%@page import="com.intaps.nrlais.controller.*"%>
<%@page import="com.intaps.nrlais.controller.tran.*"%>
<%@page import="java.util.*"%>
<%@page pageEncoding="UTF-8" %>
<%
    ApplicantsViewController controller=new ApplicantsViewController(request,response);   
    int rowSpan=(controller.settings.pickRepresentative || controller.settings.pickID)?2:1;
%>

<table class="table table-striped jambo_table app_table">
    <thead>
        <tr>
            <%if(controller.settings.inheritance){%>
            <td rowspan="<%=rowSpan%>"></td>
            <%}%>
            <%if(controller.settings.selectable){%>
            <td rowspan="<%=rowSpan%>"><%=controller.text("Select")%></td>
            <%}%>
            <td rowspan="<%=rowSpan%>"><%=controller.text("Name")%></td>
            <td rowspan="<%=rowSpan%>"><%=controller.text("Sex")%></td>
            <td rowspan="<%=rowSpan%>"><%=controller.text("Age")%></td>
            <td rowspan="<%=rowSpan%>"><%=controller.text("Share")%></td>
            <%if(controller.settings.pickRepresentative){%>
            <td colspan="4"><%=controller.text("Representative")%></td>
            <%}%>
            <%if(controller.settings.pickID){%>
            <td colspan="3"><%=controller.text("ID")%></td>
            <%}%>
            <%if(controller.settings.pickPOM){%>
            <td colspan="3"><%=controller.text("Proof of Marital Status")%></td>
            <%}%>
            <%if(controller.settings.pickShare){%>
            <td rowspan="<%=rowSpan%>"><%=controller.settings.shareLabel%></td>
            <%}%>
        </tr>
        <%if(controller.settings.pickRepresentative || controller.settings.pickID){%>
        <tr>
            <%if(controller.settings.pickRepresentative){%>
            <td><%=controller.text("Name")%></td>
            <td><%=controller.text("Sex")%></td>
            <td><%=controller.text("Age")%></td>
            <td></td>
            <%}%>
            <%if(controller.settings.pickID){%>
            <td><%=controller.text("ID Type")%></td>
            <td><%=controller.text("ID No")%></td>
            <td></td>
            <%}%>
            <%if(controller.settings.pickPOM){%>
            <td><%=controller.text("Refernce")%></td>
            <td><%=controller.text("Description")%></td>
            <td></td>
            <%}%>
        </tr>
        <%}%>
    </thead>
    <tbody id="applicant_row">
        <%for(LADM.Right r:controller.holding.getHolders()){              
            Applicant ap=controller.applicant(r.partyUID);
        %>
        <tr id="rep_<%=r.party.partyUID%>" party_uid="<%=r.party.partyUID%>">
            <%if(controller.settings.inheritance){%>
            <td>
                <input type="checkbox" id="ap_inheritance_deceased" name="inheritance" onchange="applicantPicker.inheritanceDeceasedChecked('<%=r.party.partyUID%>')"/> <label for="deceased"> <%=controller.text("Deceased")%></label><br/>
                <input type="checkbox" id="ap_inheritance_beneficiary" name="inheritance" onchange="applicantPicker.inheritanceBeneficiaryChecked('<%=r.party.partyUID%>')"/> <label for="beneficiary"> <%=controller.text("Beneficiary")%></label> 
            </td>
            <%}%>
            <%if(controller.settings.selectable){%>
            <td>
                <input type="checkbox" id="ap_select" <%=controller.selected(r.party.partyUID)?"checked":""%> onchange="applicantPicker.selectChecked('<%=r.party.partyUID%>')" />
            </td>
            <%}%>
            <td field="name"><%=r.party.getFullName()%></td>
            <td><%=controller.sexText(r.party.sex)%></td>
            <td id="ageOfwithRep"><%=controller.age(r.party.dateOfBirth)%></td>
            <td><%=r.share.num%>/<%=r.share.denum%></td>
            <%if(controller.settings.pickRepresentative){%>
            <td id="rep_name"><%=controller.repName(ap)%></td>
            <td id="rep_sex"><%=controller.repSex(ap)%></td>
            <td id="rep_dob"><%=controller.repAge(ap)%></td>
            <td id="rep_btn">
                <a class="btn btn-nrlais" href="javascript:applicantPicker.showRepPopup('<%=r.party.partyUID%>')"><i class="fa fa-plus"></i></a>

            </td>
            <%}%>
            <%if(controller.settings.pickID){%>
            <td id="idrep_type"><%=controller.idType(ap)%></td>
            <td id="idrep_text"><%=controller.idRef(ap)%></td>
            <td id="idrep_btn">
                <a class="btn btn-nrlais popper" data-placement='left' id="add_id" href="javascript:applicantPicker.showIDPopup('<%=r.party.partyUID%>')"><i class="fa fa-plus"></i></a>
            </td>
            <%}%>
            <%if(controller.settings.pickPOM){%>
            <td id="pom_ref"><%=controller.pomRef(ap)%></td>
            <td id="pom_text"><%=controller.pomText(ap)%></td>
            <td id="pom_btn">
                <a class="btn btn-nrlais popper" data-placement='left' id="add_id" href="javascript:applicantPicker.showPOMPopup('<%=r.party.partyUID%>','<%=r.party.maritalstatus%>')"><i class="fa fa-plus"></i></a>
            </td>

            <%}%>

            <%if(controller.settings.pickShare){%>
            <td>
                <div class="col-md-12">
                    <input type="text" class="form-control" id="ap_share" value="<%=controller.share(ap)%>"/>
                </div>
            </td>
            <%}%>
        </tr>
        <%}%>
    </tbody>
</table>
