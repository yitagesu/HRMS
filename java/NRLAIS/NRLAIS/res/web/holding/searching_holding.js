/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var searchHolding =
        {
            objects: [],
            showSearchContent: function (objectID, holdingUID)
            {
                this.objects[objectID].showSearchContent(holdingUID);
            },
            pickHolding: function (objectID, holdingUID)
            {
                this.objects[objectID].pickHolding(holdingUID);
                
            },
            pickParty: function (objectID, partyUID)
            {
                this.objects[objectID].pickParty(partyUID);
                
            },
            pickParcel: function (objectID, parcelUID)
            {
                this.objects[objectID].pickParcel(parcelUID);
                
            },
            doSearch: function (objectID)
            {
                if (objectID)
                    this.objects[objectID].doSearch();
                else
                {
                    for (var key in this.objects)
                    {
                        this.objects[key].doSearch();
                    }
                }
            },
            doReset:function (){
              $('#sea_form').trigger('reset'); 
            },

        };
function holdingBrowser(container_element,modalMode,setting)
{
   
    //searchHolding.currentID++;
    var state = {
        object_id: container_element,
        showSearchContent: function (holdingUID)
        {
            $.ajax('/holding/holding_information_viewer.jsp?holding_uid=' + holdingUID, {

                'method': 'GET',
                'success': function (data) {
                    if(modalMode)
                        $(container_element).modal('hide');
                    $("#holding_view_modal").modal('show');
                    $('#holdingContent').html(data);
                    $('#holdingContent [parcel_uid]').each(function()
                    {
                       var mapid=$(this).attr("id");
                       var m=NrlaisMap(map_server,mapid);
                    m.load();
                    var parcel_uid=$(this).attr("parcel_uid");
                    m.onMapReady=function()
                    {
                        m.zoomParcel("nrlais_inventory",parcel_uid);
                    };
                    });
                },
                'error': function (error) {
                    bootbox.alert("error occord:" + error);
                }
            });
        },
        onHoldingPicked: null,
        onParcelPicked: null,
        onPartyPicked: null,
        pickHolding: function (holdingUID)
        {
            if (this.onHoldingPicked)
                this.onHoldingPicked(holdingUID);
            $('#sea_form').trigger('reset'); 
            $('#sea_holding_result').html('');
        },
        pickParcel: function (parcelUID)
        {
            if (this.onParcelPicked)
                this.onParcelPicked(parcelUID);
            $('#sea_form').trigger('reset'); 
            $('#sea_holding_result').html('');
        },
        pickParty: function (partyUID)
        {
            if (this.onPartyPicked)
                this.onPartyPicked(partyUID);
        },
        doSearch: function ()
        {
            var name1 = $(container_element + " #sea_name").val();
            var name2 = $(container_element + " #sea_father").val();
            var name3 = $(container_element + " #sea_gfather").val();
            var kebele = $(container_element + " #sea_kebele").val();
            var holdingSeqNo = $(container_element + " #sea_holdingno").val();
            var parcelSeqNo = $(container_element + " #sea_parcelno").val();
            if (name1 == "" && name2 == "" && name3 == "" && kebele == "" && holdingSeqNo == "" && parcelSeqNo == "") {
                $(container_element + " #sea_holding_result").html("<h4 class='align-center'><p>"+lang("No Parameter Selected")+"</p></h4>");
            } else {
                $(container_element + " #sea_holding_result").html("<h2 class='align-center'><i class='fa fa-spinner faa-spin animated'></i><p>"+lang("Searching")+"</p></h2>");
                var url="/holding/holding_search_result.jsp"
                if(setting)
                    url=url+"?setting="+encodeURIComponent(JSON.stringify(setting));
                $.ajax({
                    type: 'POST',
                    url: url,
                    contentType: "application/json",
                    data: JSON.stringify({
                        "name1": $(container_element + " #sea_name").val(),
                        "name2": $(container_element + " #sea_father").val(),
                        "name3": $(container_element + " #sea_gfather").val(),
                        "kebele": $(container_element + " #sea_kebele").val(),
                        "holdingSeqNo": parseInt($(container_element + " #sea_holdingno").val()),
                        "parcelSeqNo": parseInt($(container_element + " #sea_parcelno").val())
                    }),
                    error: function (errorThrown) {
                        $(container_element + " #sea_holding_result").html("<h4 class='align-center'><p>Searching Error Occured</p></h4>");
                        $(container_element + " #sea_holding_result").on('click', '#hsr')
                    },
                    success: function (data) {
                        $(container_element + " #sea_holding_result").html(data);
                    }
                });
            }
        }
    };
    searchHolding.objects[container_element] = state;
    return state;
}
