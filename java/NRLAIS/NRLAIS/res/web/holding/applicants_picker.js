/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var applicantPicker = {
    repPickerPartyUID: null,
    repCallBack: null,
    validateAndSaveRep: function () {
        var valid = true;
        var male = document.getElementById("app_sex_male").checked;
        var female = document.getElementById("app_sex_female").checked;
        var requiredField = ['app_name', 'app_father_name', 'app_relation', 'app_address', 'app_dob', 'doc_reference'];
        for (var i = 0; i < requiredField.length; i++) {
            var arraylist = $('#' + requiredField[i]).val();
            if (arraylist == "" || arraylist == -1) {
                $('#' + requiredField[i]).removeClass('form-control');
                $('#' + requiredField[i]).addClass('form-error')
                $('.' + requiredField[i] + '_error').show();
                valid = false;
            } else {
                $('#' + requiredField[i]).removeClass('form-error');
                $('#' + requiredField[i]).addClass('form-control');
                $('.' + requiredField[i] + '_error').hide();
            }
        }
        if ((male) || (female)) {
            $('#app_male').removeClass('form-error');
            $('#app_male').addClass('form-control')
            $('#app_female').removeClass('form-error');
            $('#app_female').addClass('form-control')
        } else {

            $('#app_male').removeClass('form-control');
            $('#app_male').addClass('form-error')
            $('#app_female').removeClass('form-control');
            $('#app_female').addClass('form-error')
            valid = false;
        }
        if ($("#doc_reference").val() != "")
        {
            if (!isValidDate($("#app_dob"))) {
                $('#appinvaDate').remove();
                $('.app-error-panel').append("<span id='appinvaDate'><i class='fa fa-warning faa-flash animated'></i> "+lang("Invalid Date Please Fix the Date")+"<br/></span>");
                $('#app_dob').removeClass('form-control');
                $('#app_dob').addClass('form-error');
                valid = false;
            } else {
                $('#appinvaDate').remove();
                $('#app_dob').removeClass('form-error');
                $('#app_dob').addClass('form-control');
                var dob = $("#app_dob").attr("dateval");
                if (getAgeFromCal(dob) < 18) {
                    $('#underAge').remove();
                    $('.app-error-panel').append("<span id='underAge'><i class='fa fa-warning faa-flash animated'></i>"+lang("Applicant is under age")+"<br/></span>");
                    $('#app_dob').removeClass('form-control');
                    $('#app_dob').addClass('form-error');
                    valid = false;
                } else {
                    $('#underAge').remove();
                    $('#app_dob').removeClass('form-error');
                    $('#app_dob').addClass('form-control');
                }
            }


        }
        if (valid)
        {
            this.repCallBack(this.getRepData());
            $("#register_representative").modal("hide");
            $("#applicant_form").trigger("reset");
        }
    },
    getRepData: function () {
        var representative = {};
        var appSex;
        if ($("#app_sex_male").attr('checked', 'checked')) {
            appSex = 1;
        } else if ($("#app_sex_female").attr('checked', 'checked')) {
            appSex = 2;
        } else {
            appSex = "";
        }

        var d = new Date($("#app_dob").attr("dateval"));
        var dob = d.getTime();
        representative.name1 = $("#app_name").val();
        representative.name2 = $("#app_father_name").val();
        representative.name3 = $("#app_gfather_name").val();
        representative.dateOfBirth = dob;
        representative.sex = appSex;
        representative.partyType = 1;
        representative.contacts = [];
        var contactList = ['app_phone', 'app_address'];
        for (var i = 0; i < contactList.length; i++) {
            var arraylist = $('#' + contactList[i]);
            if (arraylist != "" && contactList[i] == "app_phone") {
                var representativeContact = {
                    contactData: $("#app_phone").val(),
                    contactdataType: CONTACT_TYPE_OTHER,
                    contactDataTypeOther: "Phone"
                }
                representative.contacts.push(representativeContact);
            } else if (arraylist != "" && contactList[i] == "app_address") {
                var representativeContact = {
                    contactData: $("#app_address").val(),
                    contactdataType: CONTACT_TYPE_ADDRESS
                }
                representative.contacts.push(representativeContact);
            }
        }
        var applicant =
                {
                    selfPartyUID: this.repPickerPartyUID,
                    representative: representative,
                    letterOfAttorneyDocument:
                            {
                                refText: $("#doc_reference").val(),
                                fileImage: attachment.hasAttachment() ? attachment.imageBytes : null,
                                mimeType: attachment.hasAttachment() ? attachment.mimeType : null,
                                archiveTyp: attachment.hasAttachment() ? 1 : 2
                            },
                    relationWithParty: $("#app_relation").val()
                };
        return applicant;
    },
    idPickerPartyUID: null,
    idPickerCallBack: null,
    validateAndSaveID: function () {
        var valid = true;
        var requiredField = ['id_type', 'id_text'];
        for (var i = 0; i < requiredField.length; i++) {
            var arraylist = $('#' + requiredField[i]).val();
            if (arraylist == "" || arraylist == -1) {
                $('#' + requiredField[i]).removeClass('form-control');
                $('#' + requiredField[i]).addClass('form-error');
                valid = false;
            } else {
                $('#' + requiredField[i]).removeClass('form-error');
                $('#' + requiredField[i]).addClass('form-control');
            }
        }
        if (valid) {
            this.idPickerCallBack(this.getIDData());
            $("#add_id_content").modal("hide");
            $("#add_ID_form").trigger("reset");
        }
    },
    getIDData: function () {
        var idData = {};
        idData.sourceType = $("#id_type").val();
        idData.refText = $("#id_text").val();
        idData.idFile = $("#id_file").val();
        if (attachment.hasAttachment())
        {
            idData.fileImage = attachment.imageBytes;
            idData.mimeType = attachment.mimeType;
            idData.archiveType = 1;
        } else
        {
            idData.fileImage = null;
            idData.mimeType = null;
            idData.archiveType = 2;
        }
        return idData;
    },
    setting: null,
    containerElement: null,
    load: function (containerElement, holdingID, data, setting)
    {
        this.containerElement = containerElement;
        this.setting = setting;
        var url = '/holding/applicants_picker.jsp?holding_uid=' + holdingID;
        if (setting)
            url = url + "&setting=" + encodeURIComponent(JSON.stringify(setting));
        if (data)
        {
            var that = this;
            $.ajax({
                url: url,
                method: 'POST',
                contentType: "application/json",
                data: JSON.stringify(data),
                success: function (html) {
                    $(containerElement).html(html);
                    that.setApplicants(data);
                }
            });
        } else
        {
            var that = this;
            that.setApplicants([]);
            $.ajax({
                url: url,
                method: 'GET',
                success: function (data) {
                    $(containerElement).html(data);
                    that.setupDefaults();
                }
            });
        }

    },
    setupDefaults: function ()
    {
        if (this.setting.inheritance)
        {
            var that = this;
            $(this.containerElement + " tbody tr").each(function (index) {
                var id = $(this).attr('id');
                if (id)
                {
                    var partyUID = id.substring(4);
                    that.enableRow(partyUID, false);
                }
            });
        }
        if (this.setting.selectable)
            this.disableAll();
    },
    partyItemsToApplicants: function (partyItems)
    {
        var ap = [];
        for (var i in partyItems)
        {
            var p = partyItems[i];
            ap.push({
                selfPartyUID: p.existingPartyUID,
                representative: p.representative,
                idDocument: p.idDoc,
                letterOfAttorneyDocument: p.letterOfAttorneyDocument,
                pomDocument: p.proofOfMaritalStatus,
                share: p.share,
            });
        }
        return ap;
    },
    getPartyItems()
    {
        var ap = this.getApplicants();
        var ret = [];
        for (var i in ap)
        {
            var a = ap[i];
            ret.push({
                existingPartyUID: a.selfPartyUID,
                representative: a.representative,
                idDoc: a.idDocument,
                letterOfAttorneyDocument: a.letterOfAttorneyDocument,
                proofOfMaritalStatus: a.pomDocument,
                share: a.share,
            });
        }
        return ret;
    },
    setApplicants: function (applicants)
    {

        this.setupDefaults();
        this.party_applicant = [];
        for (var i = 0; i < applicants.length; i++)
        {
            if (this.setting.selectable)
            {
                $("#rep_" + applicants[i].selfPartyUID + " #ap_select").attr("checked", "checked");
                this.enableRow(applicants[i].selfPartyUID, true);
            } else if (this.setting.inheritance)
            {
                if (applicants[i].inheritancRole == INHERITANCE_ROLE_DECEASED)
                    $("#rep_" + applicants[i].selfPartyUID + " #ap_inheritance_deceased").attr("checked", "checked");
                else if (applicants[i].inheritancRole == INHERITANCE_ROLE_BENEFICIARY)
                    $("#rep_" + applicants[i].selfPartyUID + " #ap_inheritance_beneficiary").attr("checked", "checked");
                this.enableRow(applicants[i].selfPartyUID, applicants[i].inheritancRole == INHERITANCE_ROLE_BENEFICIARY);
            }

            this.party_applicant[applicants[i].selfPartyUID] = applicants[i];
            if (this.setting.pickShare)
                $("#rep_" + applicants[i].selfPartyUID + " #ap_share").val(common_fractionToString(applicants[i].share));
        }
    },
    validate: function ()
    {
        var ret = true;
        var that = this;
        var validator = validatorPanel(".id-error-panel");
        validator.clear();
        $(this.containerElement + " tbody tr").each(function ()
        {
            var partyUID = $(this).attr("party_uid");
            if (that.setting.selectable && !$(this).find("#ap_select").is(":checked"))
                return;
            if (that.setting.inheritance && !$(this).find("#ap_inheritance_beneficiary").is(":checked"))
                return;
            if (!that.party_applicant[partyUID])
            {
                validator.addError('all_' + partyUID, lang("Please set applicant document for") + $(this).find("[field=name]").text());
                ret = false;
            } else {

                validator.removeError('all_' + partyUID);
                var ap = that.party_applicant[partyUID];
                if (!ap.idDocument) {
                    validator.addError('id_' + partyUID, lang('Please set id document for') + $(this).find("[field=name]").text());
                    ret = false;
                } else
                    validator.removeError('id_' + partyUID);
                if (that.setting.pickPOM == true) {
                    if (!ap.pomDocument) {
                        validator.addError('pom_' + partyUID, lang('Please set proof of martial status for') + $(this).find("[field=name]").text());
                        ret = false;
                    } else
                        validator.removeError('pom_' + partyUID);
                }
            }
        });
        return ret;
    },
    getApplicants: function ()
    {

        var ret = [];
        var that = this;
        $(this.containerElement + " tbody tr").each(function ()
        {
            var id = $(this).attr('id');
            if (id)
            {
                var partyUID = id.substring(4);
                var val = that.party_applicant[partyUID];
                if (!val)
                {
                    if (that.setting.selectable && $("#rep_" + partyUID + " #ap_select").is(":checked"))
                    {
                        val = {
                            selfPartyUID: partyUID
                        };
                    } else if (that.setting.inheritance && (
                            $("#rep_" + partyUID + " #ap_inheritance_deceased").is(":checked")
                            || $("#rep_" + partyUID + " #ap_inheritance_beneficiary").is(":checked")))
                    {
                        val = {
                            selfPartyUID: partyUID
                        };
                    } else
                        return;
                }
                if (that.setting.selectable && !$("#rep_" + val.selfPartyUID + " #ap_select").is(":checked"))
                    return;
                if (that.setting.inheritance)
                {
                    if ($("#rep_" + partyUID + " #ap_inheritance_deceased").is(":checked"))
                        val.inheritancRole = INHERITANCE_ROLE_DECEASED;
                    else if ($("#rep_" + partyUID + " #ap_inheritance_beneficiary").is(":checked"))
                        val.inheritancRole = INHERITANCE_ROLE_BENEFICIARY;
                    else
                        return;
                }
                if (that.setting.pickShare)
                    val.share = common_parseFraction($("#rep_" + val.selfPartyUID + " #ap_share").val());
                ret.push(val);
            }
        });
        return ret;
    },
    onChanged: null,
    callOnChanged: function ()
    {
        if (this.onChanged)
            this.onChanged();
    },
    showRepPopup: function (partyUID)
    {
        attachment.clear();
        $('#register_representative').modal('show');
        this.repPickerPartyUID = partyUID;
        this.repCallBack = function (repdata)
        {
            $("#rep_" + this.repPickerPartyUID + " #rep_name").text(repdata.representative.name1 + " " + repdata.representative.name2 + " " + repdata.representative.name3);
            var sex = "";
            if (repdata.representative.sex == 1) {
                sex = lang("Male");
            } else {
                sex = lang("Female");
            }
            $("#rep_" + this.repPickerPartyUID + " #rep_sex").text(sex);
            var holderDOB = EthiopianDate.prototype.dateToEthiopian(new Date(repdata.representative.dateOfBirth));
            var dob = EthiopianDate.prototype.ethiopianDateString(holderDOB) + lang("EC");
            $("#rep_" + this.repPickerPartyUID + " #rep_dob").text(dob);
            if (!this.party_applicant[this.repPickerPartyUID])
            {
                this.party_applicant[this.repPickerPartyUID] = repdata;
            } else
            {
                var app = this.party_applicant[this.repPickerPartyUID];
                app.representative = repdata.representative;
                app.letterOfAttorneyDocument = repdata.letterOfAttorneyDocument;
                app.relationWithParty = repdata.relationWithParty;
            }
            this.callOnChanged();
        };
    },
    party_applicant: [],
    showIDPopup: function (partyUID)
    {
        attachment.clear();
        $('#add_id_content').modal('show');
        this.idPickerPartyUID = partyUID;
        this.idPickerCallBack = function (iddata)
        {
            var typeText = "";
            if (iddata.sourceType == 31) {
                typeText = lang("Passport");
            } else if (iddata.sourceType == 32) {
                typeText = lang("Kebele ID");
            }
            $("#rep_" + this.idPickerPartyUID + " #idrep_type").text(typeText);
            $("#rep_" + this.idPickerPartyUID + " #idrep_text").text(iddata.refText);
            if (!this.party_applicant[this.idPickerPartyUID])
            {
                this.party_applicant[this.idPickerPartyUID] = {selfPartyUID: this.idPickerPartyUID, idDocument: iddata};
            } else
            {
                this.party_applicant[this.idPickerPartyUID].idDocument = iddata;
            }
            this.callOnChanged();
        };
    },
    pomPickerPartyUID: null,
    pomPickeCallBack: null,
    validateAndSavePOM: function () {
        var valid = true;
        var requiredField = ['pom_ref', 'pom_text'];
        for (var i = 0; i < requiredField.length; i++) {
            var arraylist = $('#add_pom_form #' + requiredField[i]).val();
            if (arraylist == "" || arraylist == -1) {
                $('#add_pom_form #' + requiredField[i]).removeClass('form-control');
                $('#add_pom_form #' + requiredField[i]).addClass('form-error');
                valid = false;
            } else {
                $('#add_pom_form #' + requiredField[i]).removeClass('form-error');
                $('#add_pom_form #' + requiredField[i]).addClass('form-control');
            }
        }
        if (valid) {
            this.pomPickeCallBack(this.getPOMData());
            $("#add_pom_content").modal("hide");
            $("#add_pom_form").trigger("reset");
        }
    },
    getPOMData: function () {
        var pomData = {};
        pomData.refText = $("#add_pom_form #pom_ref").val();
        pomData.description = $("#add_pom_form #pom_text").val();
        pomData.idFile = $("#pom_file").val();
        if (attachment.hasAttachment())
        {
            pomData.fileImage = attachment.imageBytes;
            pomData.mimeType = attachment.mimeType;
            pomData.archiveType = 1;
        } else
        {
            pomData.fileImage = null;
            pomData.mimeType = null;
            pomData.archiveType = 2;
        }
        return pomData;
    }, showPOMPopup: function (partyUID, martialStatus)
    {
        attachment.clear();
        if (martialStatus == MARITAL_STATUS_MARRIED)
            $('#add_pom_title').text(lang('Add Proof of Marraige'));
        else
            $('#add_pom_title').text(lang('Add Proof of Celebacy'));
        $('#add_pom_content').modal('show');
        this.pomPickerPartyUID = partyUID;
        this.pomPickeCallBack = function (pomdata)
        {
            $("#rep_" + this.pomPickerPartyUID + " #pom_ref").text(pomdata.refText);
            $("#rep_" + this.pomPickerPartyUID + " #pom_text").text(pomdata.description);
            if (!this.party_applicant[this.pomPickerPartyUID])
            {
                this.party_applicant[this.pomPickerPartyUID] = {selfPartyUID: this.pomPickerPartyUID, pomDocument: pomdata};
            } else
            {
                this.party_applicant[this.pomPickerPartyUID].pomDocument = pomdata;
            }
            this.callOnChanged();
        };
    }
    , disableAll: function ()
    {
        $(this.containerElement + " input[type=text]").removeClass("new_share_enable");
        $(this.containerElement + " input[type=text]").addClass("new_share_disable");
        $(this.containerElement + " a").hide();
    }
    , enableRow: function (partyUID, enabled)
    {
        if (enabled)
        {
            $("#rep_" + partyUID + " input[type=text]").removeClass("new_share_disable");
            $("#rep_" + partyUID + " input[type=text]").addClass("new_share_enable");
        } else {
            $(this.containerElement + " input[type=text]").removeClass("new_share_enable");
            $(this.containerElement + " input[type=text]").addClass("new_share_disable");
        }
        if (enabled)
            $("#rep_" + partyUID + " a").show();
        else
            $("#rep_" + partyUID + " a").hide();
        $("#rep_" + partyUID + " input[type=text]").prop("disabled", !enabled);
    }
    , selectChecked: function (partyUID)
    {
        var selected = $("#rep_" + partyUID + " #ap_select").is(":checked");
        this.enableRow(partyUID, selected);
        this.callOnChanged();
    },
    inheritanceDeceasedChecked: function (partyUID)
    {
        var selected = $("#rep_" + partyUID + " #ap_inheritance_deceased").is(":checked");
        var otherSelected;
        if (selected)
        {
            $("#rep_" + partyUID + " #ap_inheritance_beneficiary").removeAttr("checked");
            otherSelected = false;
        } else
            otherSelected = $("#rep_" + partyUID + " #ap_inheritance_beneficiary").is(":checked");
        this.enableRow(partyUID, !selected && otherSelected);
        this.callOnChanged();
    },
    inheritanceBeneficiaryChecked: function (partyUID)
    {
        var selected = $("#rep_" + partyUID + " #ap_inheritance_beneficiary").is(":checked");
        var otherSelected;
        if (selected)
        {
            $("#rep_" + partyUID + " #ap_inheritance_deceased").removeAttr("checked");
            otherSelected = false;
        } else
            otherSelected = $("#rep_" + partyUID + " #ap_inheritance_beneficiary").is(":checked");
        this.enableRow(partyUID, selected && !otherSelected);
        this.callOnChanged();
    },
};
