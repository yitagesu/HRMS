<%@page import="com.intaps.nrlais.repo.*"%>
<%@page import="com.intaps.nrlais.*"%>
<%@page import="com.intaps.nrlais.model.*"%>
<%@page import="com.intaps.nrlais.controller.*"%>
<%@page import="com.intaps.nrlais.controller.tran.*"%>
<%@page import="java.util.*"%>
<%@page pageEncoding="UTF-8" %>
<%
    ParcelTransferViewController controller=new ParcelTransferViewController(request,response);
%>
<table class="table table-striped jambo_table">
    <thead>
        <tr>
            <td><%=controller.settings.transferLabel%></td>
            <td><%=controller.text("Parcel")%></td>
            <td><%=controller.text("Area")%> (<%=controller.text("M")%><sup>2</sup>)</td>
            <%if(controller.settings.transferArea){%>
            <td><%=controller.settings.areaLabel%></td>
            <%}%>
            <%if(controller.settings.transferShare){%>
            <td><%=controller.settings.shareLabel%></td>
            <%}%>
            <%if(controller.settings.split){%>
            <td><%=controller.text("Split Parcel")%></td>
            <%}%>
        </tr>
    </thead>
    <tbody>
        <%for(LADM.Parcel p:controller.holding.parcels){%>
        <tr id="tran_<%=p.parcelUID%>" parcel_uid="<%=p.parcelUID%>">
            <td><input type="checkbox" field="select" <%=controller.transfered(p)?"checked":""%> onchange="parcelTransferTable.tansferCheckChanged('<%=p.parcelUID%>')" /></td>
            <td><%=LADM.Parcel.formatParcelSeqNo(p.seqNo)%></td>
            <td><%=p.areaGeom%></td>                    

            <%if(controller.settings.transferArea){%>
            <td>
                <input type="text" class="form-control" field="area" value="<%=controller.area(p)%>"/>
            </td>
            <%}%>
            <%if(controller.settings.transferShare){%>
            <td>
                <input type="text" class="form-control" field="share" value="<%=controller.share(p)%>"/>
            </td>
            <%}%>
            <%if(controller.settings.split){%>
            <td>
                <input type="checkbox" field="split" <%=controller.split(p)?"checked":""%> onchange="parcelTransferTable.tansferCheckChanged('<%=p.parcelUID%>')" />
            </td>
            <%}%>
        </tr>
        <%}%>
    </tbody>
</table>
