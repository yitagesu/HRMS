<%@page import="com.intaps.nrlais.repo.*"%>
<%@page import="com.intaps.nrlais.*"%>
<%@page import="com.intaps.nrlais.model.*"%>
<%@page import="com.intaps.nrlais.controller.*"%>
<%@page import="com.intaps.nrlais.controller.tran.*"%>
<%@page import="java.util.*"%>
<%@page pageEncoding="UTF-8" %>
<%
    ParcelSplitTableViewController controller=new ParcelSplitTableViewController(request,response);
%>
<table class="table table-striped jambo_table">
    <thead>
        <tr>
            <td><%=controller.text("Parcel")%></td>
            <td><%=controller.text("Area")%> (<%=controller.text("M")%><sup>2</sup>)</td>
            <%for(LADM.Party p:controller.beneficiaries()){%>
            <td><%=p.getFullName()%></td>
            <%}%>
            <td> <%=controller.text("Split parcel")%></td>
        </tr>
    </thead>
    <tbody>
        <%for(LADM.Parcel p:controller.parcels()){%>
        <tr id="sh_<%=p.parcelUID%>" parcel_uid="<%=p.parcelUID%>">
            <td><%=LADM.Parcel.formatParcelSeqNo(p.seqNo)%></td>
            <td><%=p.areaGeom%></td>
            <%for(LADM.Party party:controller.beneficiaries()){%>
            <td >
                <input <%=controller.isSplit(p)?"disabled":""%> 
                    style='color:<%=controller.isSplit(p)?"transparent":"black"%>' 
                    type="text" 
                    class="form-control" 
                    id="sh_party_<%=party.partyUID%>" 
                    party_uid="<%=party.partyUID%>"
                    value="<%=controller.shareValue(p,party)%>"/></td>
                <%}%>
            <td class="align-center"><input  type="checkbox" onchange="parcelSplitTable.splitCheckChanged('<%=p.parcelUID%>')"   id="shSplit" <%=controller.isSplit(p)?"checked":""%>/></td>
        </tr>
        <%}%>
    </tbody>
</table>
