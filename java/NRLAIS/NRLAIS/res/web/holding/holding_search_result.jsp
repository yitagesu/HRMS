<%@page import="com.intaps.nrlais.repo.*"%>
<%@page import="com.intaps.nrlais.*"%>
<%@page import="com.intaps.nrlais.controller.*"%>
<%@page import="com.intaps.nrlais.model.*"%>
<%@page import="com.intaps.nrlais.util.*"%>
<%@page import="java.util.*"%>
<%@page import ="java.text.DecimalFormat" %>
<%@page pageEncoding="UTF-8" %>
<%
        HoldingSearchResultViewController controller=new HoldingSearchResultViewController(request,response);
    
%>


<table class="table table-striped jambo_table">
    <thead>
        <tr>
            <%
                int records=0;
                for(LADM l:controller.result){
                    records=controller.result.size();
                }%>
            <td><%=controller.text("Search Result")%>: <%=records%> <%=controller.text("Record Found")%></td>

        </tr>
    </thead>
    <tbody>
        <% if(records < 1){%>
        <tr><td class="align-center"><h2><%=controller.text("No Records Found")%> </h2><p><%=controller.text("Try Again by Changing Your Search Parameter")%></p></td></tr>
        <%} else {
            for(LADM l:controller.result){
        %>

        <tr>
            <td>
                <table class="internal_table">
                    <tr>
                        <td><%=controller.text("Kebele")%>: <%=l.holding.nrlais_kebeleid%></td>
                        <td><%=controller.text("Holding Seq.No")%>: <%=l.holding.holdingSeqNo%></td>
                        <td><%=controller.text("Holding Type")%>: <%=controller.holdingTypeText(l.holding.holdingType)%></td>
                        <% if(controller.settings.pickHolding){%>
                        <td class="align-right"><div class="btn-group">

                                <a class="btn btn-nrlais"  href="javascript:searchHolding.showSearchContent('<%=controller.object_id%>','<%=l.holding.holdingUID%>')">
                                    <i class="fa fa-eye"></i>
                                </a>
                                <%if(controller.settings.pickHolding){%>
                                <a class="btn btn-nrlais" href="javascript:searchHolding.pickHolding('<%=controller.object_id%>','<%=l.holding.holdingUID%>')">
                                    <i class="fa fa-plus"></i>
                                </a>
                                <%}}%>
                            </div></td>
                    </tr>
                    <tr>
                        <td colspan="3" style="vertical-align: top;">
                            <table class="internal_table internal_table_border">
                                <tr>
                                    <td colspan="6">
                                    <div class="pull-left"><label><%=controller.text("Holder")%></label></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td><%=controller.text("Name")%></td>
                                    <td><%=controller.text("ID")%></td>
                                    <td><%=controller.text("Sex")%></td>
                                    <td><%=controller.text("Age")%></td>
                                    <td><%=controller.text("Martial Status")%></td>
                                    <td><%=controller.text("Share")%></td>
                                </tr>
                                <%for(LADM.Right r:l.holding.getHolders()){%>

                                <tr id="<%=r.party.partyUID%>">
                                    
                                    <% if(controller.settings.pickParty){%>
                        <td class="align-right"><div class="btn-group">

          
                                <a class="btn btn-nrlais" href="javascript:searchHolding.pickParty('<%=controller.object_id%>','<%=r.party.partyUID%>')">
                                    <i class="fa fa-plus"></i>
                                </a>
                                <%}%>
                            </div></td>
                                    
                                    <td><%=r.party.getFullName()%></td>
                                    <td><%=(r.party.idText==null?"":r.party.idText)%></td>
                                    <td><%=controller.sexText(r.party.sex)%></td>
                                    <td id="ageOfHolder"><%=controller.age(r.party.dateOfBirth)%></td>
                                    <td><%=controller.maritalStatusText(r.party.maritalstatus)%></td>
                                    <td><%=r.share.num%>/<%=r.share.denum%></td>
                                </tr>
                                <%}%>

                            </table>
                        </td>

                        <td style="vertical-align: top;">
                            <table class="internal_table internal_table_border">
                                <% double total=0.0;
                                   DecimalFormat dec = new DecimalFormat("0.00");
                                    for(LADM.Parcel p:l.holding.parcels){
                                            total += p.areaGeom;
                                        } %>
                                <tr>
                                    <td colspan="3"><%=l.holding.parcels.size()%> <%=controller.text("Parcels")%>, <%=controller.text("Total")%>: <%=dec.format(total)%> <%=controller.text("M")%><sup>2</sup></td>
                                </tr>
                                <tr>
                                    <td><%=controller.text("Parcel No")%></td>
                                    <td><%=controller.text("Area")%></td>
                                    <td></td>
                                </tr>
                                <%for(LADM.Parcel p:l.holding.parcels){%>
                                <tr>
                                    <td><%=p.seqNo%></td>
                                    <td><%=p.areaGeom%></td>
                                    <td>
                                        <%if(controller.settings.pickParcel){%>
                                        <a class="btn btn-nrlais" href="javascript:searchHolding.pickParcel('<%=controller.object_id%>','<%=p.parcelUID%>')">
                                            <i class="fa fa-plus"></i>
                                        </a>
                                        <%}%>
                                        <a href="<%=controller.parcelMapLink(p)%>" class="btn btn-default btn-xs" title="Show Map"><i class="fa fa-map-marker"></i></a>
                                    </td>
                                </tr>
                                <%}%>

                            </table>
                        </td>
                    </tr>
                    <tr>

                    </tr>
                </table>
            </td>
        </tr>
        <%}}%>
    </tbody>
</table>
