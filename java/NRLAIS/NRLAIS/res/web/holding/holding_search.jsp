<%@page import="com.intaps.nrlais.repo.*"%>
<%@page import="com.intaps.nrlais.*"%>
<%@page import="com.intaps.nrlais.controller.*"%>
<%@page import="com.intaps.nrlais.controller.tran.*"%>
<%@page import="com.intaps.nrlais.model.*"%>
<%@page import="java.util.*"%>
<%@page pageEncoding="UTF-8" %>
<%
    
    HoldingSearchViewController searchController=new HoldingSearchViewController(request,response);
%>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4><%=searchController.text("Search Holding")%></h4>
    <hr>
    <div class="row">
        <form id="sea_form">
            <div class="col-md-12">
                <div class="col-md-4">
                    <input type="text" class="form-control" id="sea_name" placeholder="<%=searchController.text("Enter First Name")%>"  />
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="sea_father" placeholder="<%=searchController.text("Enter Father Name")%>"/>
                </div>
                <div class="col-md-4">
                    <div class="input-group">
                        <input type="text" class="form-control" id="sea_gfather" placeholder="<%=searchController.text("Enter Grand Father Name")%>"/>
                        <span class="input-group-addon hand" id="addon" onclick="searchHolding.doReset()"><i class="fa fa-refresh"></i>   <%=searchController.text("Reset")%></span>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-4">
                    <select type="text" class="form-control" id="sea_kebele">
                        <option value=""><%=searchController.text("Select Kebele")%></option>
                        <%for(CodeText k:searchController.allKebeles()){%>
                        <option value="<%=k.code%>"><%=k.name%> (<%=k.code%>)</option>
                        <%}%>
                    </select>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="sea_holdingno" placeholder="<%=searchController.text("Holding Seq.No")%>"/>
                </div>
                <div class="col-md-4">
                    <div class="input-group">
                        <input type="text" class="form-control" id="sea_parcelno" placeholder="<%=searchController.text("Parcel Seq.No")%>"/>
                        <span field="search_button" class="input-group-addon hand" id="addon" onclick="searchHolding.doSearch(<%=searchController.objectID()%>)"><i class="fa fa-search"></i> <%=searchController.text("Search")%></span>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>            <!-- /modal-header -->

<div class="modal-body has-scroll">
    <div class="row">
        <div id="sea_holding_result" class="col-md-12">

        </div>
    </div>
</div>
<div class="modal-footer">
</div>