<%@page import="com.intaps.nrlais.repo.*"%>
<%@page import="com.intaps.nrlais.*"%>
<%@page import="com.intaps.nrlais.model.*"%>
<%@page import="java.util.*"%>
<%@page pageEncoding="UTF-8" %>
<html>
    <head>
        <link rel="stylesheet" href="/assets/CSS/bootstrap.min.css"/>
        <link rel="stylesheet" href="/assets/CSS/custom.css"/>
        <script src="/assets/JS/jquery.min.js"></script>
        <script src="/assets/JS/jquery-ui.js"></script>
        <script src="/assets/JS/bootstrap.min.js"></script>
        <script src="/assets/JS/bootbox.js"></script>        
        <script src="/assets/JS/string.js"></script>

        <script src="/assets/JS/et_dual_cal.js"></script>
        <script src="/assets/JS/application_page_load.js"></script>
        <script src="/assets/JS/transaction.js"></script>
    </head>
    <body>
        <%@ include file="/holding/holding_information_viewer.jsp" %>        
    </body>
    <script>
        $(document).ready(function ()
        {
            $("div.bhoechie-tab-menu>div.list-group>a").click(function (e) {
                e.preventDefault();
                $(this).siblings('a.active').removeClass("active");
                $(this).addClass("active");
                var index = $(this).index();
                $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
                $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
            });
        });
    </script>
</html>