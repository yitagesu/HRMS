<%-- 
    Document   : register_rent
    Created on : Jan 24, 2018, 3:29:54 PM
    Author     : yitagesu
--%>
<%@page import="com.intaps.nrlais.model.*" %>
<%@page import="com.intaps.nrlais.repo.*" %>
<%@page import="com.intaps.nrlais.controller.*" %>
<%@page import="com.intaps.nrlais.controller.tran.*" %>
<script src="/assets/JS/wizard_pages.js"></script>
<script src="/assets/JS/ejs.js"></script>
<script src="/party/party_list.js"></script>
<script src="/party/register_party.js"></script>
<%
    GiftViewController controller=new GiftViewController(request,response,true);
    if(controller.holding.holdingUID!=null){ 
%>
<script>
    var gift_holdingID = '<%=controller.holding.holdingUID%>';
    var gift_combined =<%=controller.json(controller.tran)%>;
</script>
<%}%>
<div class="stepwizard">
    <div class="stepwizard-row setup-panel" id='tab-buttons'>

    </div>
</div>
<form role="form" action="" method="post">
    <div class="row setup-content" id="step-app">
        <div class="col-md-12">
            <h4><small><%=controller.text("Set Application")%></small></h4>
            <hr>
            <div class="form-group col-md-6">
                <label class="control-label"><%=controller.text("Application Type")%>:</label>
                <input  type="text"  class="form-control" disabled="disabled" id="app_type" value="<%=controller.tranTypeText()%>"/>
            </div>
            <div class="form-group col-md-6">
                <label class=""><%=controller.text("Application Date")%>:</label>
                <input max="5" type="text" class="form-control" placeholder="pick application Date"  id="app_date" dateVal="<%=controller.grigDate(controller.tran.time)%>"/>
            </div>
            <div class="form-group col-md-6">
                <label class=""><%=controller.text("Manipulate Holding Manually")%>:</label>
                <input max="5" type="checkbox" class="form-control"  id="man_holding" <%=controller.data.manipulateManually?"checked":""%> />
            </div>
            <div class="form-group col-md-12">
                <label class=""><%=controller.text("Application Description")%>:</label>
                <textarea rows="4"  class="form-control" placeholder="<%=controller.text("Write application description")%>" id="app_description"><%=controller.escapeHtml(controller.tran.notes)%></textarea>
            </div>
            <div class="form-group col-md-12" id="holder_content">

            </div>
            <div class="col-md-12">
                <div class="pull-left">
                    <div class="error-panel">

                    </div>
                </div>
                <div class="pull-right">
                    <div class="btn-group">
                        <button class="btn btn-nrlais" type="button" data-toggle='modal' data-target='#pick_person' data-backdrop='static' ><i class="fa fa-hand-o-up"></i> <%=controller.text("Load Parcel")%></button>
                        <button class="btn btn-nrlais" type="button" onclick="wizardPages.gotoNext()" id="nextToSecond"><%=controller.text("Next")%> <i class="fa fa-angle-double-right"></i></button>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="row setup-content" id="step-type">
        <div class="col-md-12">
            <h4><small><%=controller.text("Select Transfer Type")%></small></h4>
            <hr>
            <div>
                <button type='button' onclick="gift_setGiftType(TRANSFER_TYPE_NEW_HOLDING, true)" class="btn btn-nrlais"><%=controller.text("Transfer to New Holder")%>  <span id='gift_type_marker_nnew' class="fa fa-check" style="display: none"></span></button>
                <button type='button' onclick="gift_setGiftType(TRANSFER_TYPE_OTHER_HOLDING, true)" class="btn btn-nrlais"><%=controller.text("Transfer to Existing Holder")%>  <span id='gift_type_marker_other' class="fa fa-check" style="display: none"></span></button>
                <button type='button' onclick="gift_setGiftType(TRANSFER_TYPE_THIS_HOLDING, true)" class="btn btn-nrlais"><%=controller.text("Share this Holding")%>  <span id='gift_type_marker_this' class="fa fa-check" style="display: none"></span></button>
            </div>
            <div class="pull-left">
                <div class="id-error-panel">

                </div>
            </div>
            <div class="pull-right">
                <div class="btn-group">

                </div>
            </div>
        </div>
    </div>

    <div class="row setup-content" id="step-parcel">
        <div class="col-md-12">
            <h4><small><%=controller.text("Select Parcels to Transfer")%></small></h4>
            <hr>
            <div id="holder_parcel">
            </div>

            <div class="pull-right">
                <div class="btn-group">
                    <button class="btn btn-nrlais" type="button" onclick="wizardPages.gotoNext()"><%=controller.text("Next")%> <i class="fa fa-angle-double-right"></i></button>
                </div>
            </div>
                <div class="pull-left">
                    <div class="parcel-error-panel error_panel"></div>
                </div>
        </div>
    </div>
    <div class="row setup-content" id="step-beneficiary">
        <div class="col-md-12">
            <h4 id="step-beneficiary-title"><small><%=controller.text("Enter Gift Recipient Parties")%></small></h4>
            <hr/>
            <div id="gift_beneficiaries">
            </div>
            <div class="align-right pull-right">
                <div class="btn-group">
                    <button id="gift_beneficiary_add" class="btn btn-nrlais" type="button"><i class="fa fa-plus"></i><%=controller.text("Add Party")%></button>
                    <button class="btn btn-nrlais nextBtn" type="button" id="doc_nextbtn" onclick="wizardPages.gotoNext()"><%=controller.text("Next")%> <i class="fa fa-angle-double-right"></i></button>
                </div>
            </div>
            <div class="pull-left">
                <div class="btn-group">
                    <div class="error_gift_recipient_panel error_panel">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row setup-content" id="step-this-holding">
        <div class="col-md-12">
            <h4><small><%=controller.text("Set Holder(s) Making the Gift Information")%></small></h4>
            <hr/>
            <div id="gift_this_holding">
            </div>
            <div class="align-right pull-right">
                <div class="btn-group">
                    <button class="btn btn-nrlais nextBtn" type="button" id="doc_nextbtn" onclick="wizardPages.gotoNext()"><%=controller.text("Next")%> <i class="fa fa-angle-double-right"></i></button>
                </div>
            </div>
            <div class="pull-left">
                <div class="btn-group">
                    <div class="id-error-panel">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row setup-content" id="step-existing-holding">
        <div class="col-md-12">
            <h4><small><%=controller.text("Select Gift Recipient")%></small></h4>
            <hr/>
            <div id="gift_exiting_holding">
            </div>
            <div id="gift_exiting_holding_applicant">
            </div>
            <button class="btn btn-nrlais" type="button" data-toggle='modal' data-target='#pick_person' data-backdrop='static' ><i class="fa fa-hand-o-up"></i><%=controller.text("Pick Recipient Holding")%></button>

            <div class="align-right pull-right">
                <div class="btn-group">
                    <button class="btn btn-nrlais nextBtn" type="button" id="doc_nextbtn" onclick="wizardPages.gotoNext()"><%=controller.text("Next")%> <i class="fa fa-angle-double-right"></i></button>
                </div>
            </div>
            <div class="pull-left">
                <div class="btn-group">
                    <div class="error-tenant-party-panel">

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row setup-content" id="step-docs">
        <div class="col-md-12">
            <h4><small><%=controller.text("Add Required Documents")%></small></h4>
            <hr/>
            <table class="table table-striped jambo_table">
                <thead>
                    <tr>
                        <td><%=controller.text("Document Type")%></td>
                        <td><%=controller.text("Document Reference")%></td>
                        <td><%=controller.text("Document Description")%></td>
                        <td><%=controller.text("Document File")%></td>
                        <td></td>
                    </tr>
                </thead>
                <tbody id="documentTableBody">
                    <tr doc_type="<%=DocumentTypeInfo.DOC_TYPE_DIVORCE_CLAIM_RESOLUTION%>">
                        <td><%=controller.text("Elders committee or Woreda court statement on claim resolution")%>:</td>
                        <td id="doc_ref"></td>
                        <td id="doc_desc"></td>
                        <td id="doc_File"></td>
                        <td><a  class="btn btn-nrlais"><i class="fa fa-plus"></i></a> </td>
                    </tr>                    
                    <tr doc_type="<%=DocumentTypeInfo.DOC_TYPE_LAND_HOLDING_CERTIFICATE%>">
                        <td><%=controller.text("Land holding certificate")%>:</td>
                        <td id="doc_ref"></td>
                        <td id="doc_desc"></td>
                        <td id="doc_File"></td>
                        <td><a  class="btn btn-nrlais"><i class="fa fa-plus"></i></a> </td>
                    </tr>
                </tbody>
            </table>

            <div class="align-right pull-right">
                <div class="btn-group">
                    <button class="btn btn-nrlais" type="button" data-toggle='modal' data-target='#add_document' data-backdrop='static'><i class="fa fa-plus"></i><%=controller.text("Add")%></button>
                    <button class="btn btn-nrlais nextBtn" type="button" id="doc_nextbtn" onclick="wizardPages.gotoNext()"><%=controller.text("Next")%> <i class="fa fa-angle-double-right"></i></button>
                </div>
            </div>
            <div class="pull-left">
                <div class="btn-group">
                    <div class="error-req-doc-panel">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row setup-content" id="step-finish">
        <div class="col-md-12">
            <h4><small><%=controller.text("Review and Submit Application")%></small></h4>
            <hr>
            <div class="print_slip" id="print_slip">
                <iframe id="printSlip"  frameborder="0" width="100%" height="150px" scrolling="no"></iframe>
            </div>
            <div class="align-right">
                <div class="btn-group">
                    <button class="btn btn-nrlais" type="button" id="print" onclick="pritSlip();"><i class="fa fa-print"></i><%=controller.text("Print")%></button>
                    <button class="btn btn-nrlais nextBtn" type="button" id="doc_nextbtn" onclick="gift_SaveToServer()"><%=controller.text("Register Transaction")%> <i class="fa fa-exclamation"></i></button>
                </div>
            </div>
        </div>
    </div>
</form>
