<table class="table table-striped">
    <tbody id="">
        <%@ include file="/tran/tran_viewer_header.jsp" %>
        <tr><td colspan="2" class="table_separetor"><label><%=controller.text("Information of Gift Giver")%></label></td></tr>
        <%for(LADM.Right r:controller.holding.getHolders()){
            String holderROLE=controller.roleText(r.party.mreg_familyrole);
            String holderSEX=controller.sexText(r.party.sex);
            Applicant ap=controller.partyApplicant(r.party.partyUID);
        %>
        <tr id="final_<%=controller.holding.holdingUID%>" class="holder">
            <td class="many">
                <label><%=holderROLE%> Info</label>
                <ul>
                    <li><label><%=controller.text("Name")%>: </label> <%=r.party.getFullName()%></li>
                    <li><label><%=controller.text("Sex")%>: </label> <%=holderSEX%></li>
                    <li><label><%=controller.text("Age")%>: </label> <span id="ageOfApp"><%=controller.age(r.party.dateOfBirth)%> (<%=EthiopianCalendar.ToEth(r.party.dateOfBirth).toString()+" "+controller.text("EC")%>)</span></li>
                    <li><label><%=controller.text("Share")%>: </label> <%=r.share.num%>/<%=r.share.denum%></li>
                    <li><label><%=controller.text("ID")%>: </label> <%if(ap==null || ap.idDocument==null||ap.idDocument.fileImage ==null){%>
                        <%=controller.applicantIDTypeText(ap)%> (<%=controller.applicantIDText(ap)%>)
                        <%}else{%>
                        <a href="<%=controller.applicantIDLink(ap)%>"><%=controller.applicantIDTypeText(ap)%> (<%=controller.applicantIDText(ap)%>)</a>
                        <%}%>
                    </li>
                    <li><label><%=controller.text("Proof of Marital Status")%>: </label>  <%if(ap==null || ap.pomDocument==null || ap.pomDocument.fileImage ==null){%>
                        <%=controller.applicantPOMText(ap)%> 
                        <%}else{%>
                        <a href="<%=controller.applicantPOMLink(ap)%>"><%=controller.applicantPOMText(ap)%></a>
                        <%}%>
                    </li>
                </ul>
            </td>
            <td class="many">
                <label><%=holderROLE%><%=controller.text("Representative info")%></label>
                <%
                            String message="";
                            
                            if(ap!=null && ap.representative!=null)
                            {
                                String repSEX="";
                                repSEX=controller.sexText(ap.representative.sex);
                            
            
                %>
                <ul>
                    <li> <label><%=controller.text("Name")%>: </label> <%=ap.representative.getFullName()%></li>
                    <li> <label><%=controller.text("Relationship")%>: </label> <%=ap.relationWithParty%></li>
                    <li> <label><%=controller.text("Age")%>: </label> <span id="ageOfRep"><%=controller.age(ap.representative.dateOfBirth)%> (<%=EthiopianCalendar.ToEth(ap.representative.dateOfBirth).toString()+" "+controller.text("EC")%>)</span></li>
                    <li><label><%=controller.text("Sex")%>: </label> <%=repSEX%></li>
                        <%for(LADM.PartyContact a:ap.representative.contacts){
                        if(a.contactdataType==LADM.PartyContact.CONTACT_TYPE_OTHER){%>
                    <li><label><%=controller.text("Phone")%>: </label> <%=a.contactData%></li>
                        <%}if(a.contactdataType==LADM.PartyContact.CONTACT_TYPE_ADDRESS){%>
                    <li><label><%=controller.text("Address")%>: </label> <%=a.contactData%></li> 
                        <%}}%>
                    <li><label><%=controller.text("Letter of Attorney")%>:</label> <%if(ap.letterOfAttorneyDocument==null||ap.letterOfAttorneyDocument.fileImage ==null){%>
                        <%=controller.letterOfAttroneyText(ap)%>
                        <%}else{%>
                        <a href="<%=controller.letterOfAttroneyLink(ap)%>"><%=controller.letterOfAttroneyText(ap)%></a>
                        <%}%>
                    </li>                      
                </ul>

                <%}else{ message=controller.text("No Representative");%>

                <p><%=message%></p>

                <%}//if-else%>


            </td>
        </tr>
        <%}//for%>
        <tr><td colspan="2" class="table_separetor"><label><%=controller.text("Information of Gift Recipient(s)")%></label></td></tr>
        <%for(PartyItem b:controller.beneficiaries()){
            String benfiROLE=controller.roleText(b.party.mreg_familyrole);
            String benfiSEX=controller.sexText(b.party.sex);
        %>
        <tr id="benfi_">
            <td class="many">
                <ul>
                    <li><label><%=controller.text("Name")%>: </label><%=b.party.getFullName()%></li>
                    <li><label><%=controller.text("Sex")%>: </label><%=benfiSEX%></li>
                    <li><label><%=controller.text("Age")%>: </label> <%=controller.age(b.party.dateOfBirth)%> (<%=EthiopianCalendar.ToEth(b.party.dateOfBirth).toString()+" "+controller.text("EC")%>)</li>
                    <li><label><%=controller.text("Family Role")%>: </label> <%=benfiROLE%></li>
                    <li><label><%=controller.text("Share")%>: </label> <%=controller.shareText(b.share)%></li>
                    <li><label><%=controller.text("ID")%>: </label>  <%if(b.idDoc==null|| b.idDoc.fileImage ==null){%>
                        <%=controller.applicantIDTypeText(b)%> (<%=controller.applicantIDText(b)%>)
                        <%}else{%>
                        <a href="<%=controller.applicantIDLink(b)%>"><%=controller.applicantIDTypeText(b)%> (<%=controller.applicantIDText(b)%>)</a>
                        <%}%>
                    </li>
                    <li><label><%=controller.text("Proof of Marital Status")%>: </label>  <%if(b.proofOfMaritalStatus.fileImage ==null){%>
                        <%=controller.applicantPOMText(b)%> 
                        <%}else{%>
                        <a href="<%=controller.applicantPOMLink(b)%>"><%=controller.applicantPOMText(b)%></a>
                        <%}%>
                    </li>
                </ul>
            </td>
            <td></td>
        </tr>
        <%}%>

        <tr><td colspan="2" class="table_separetor"><label><%=controller.text("Parcel for Transfer")%></label> </td></tr>
        <%for(ParcelTransfer p:controller.transfers()){
               LADM.Parcel pr=controller.transferParcel(p);
        %>
        <tr>
            <td class="many" colspan="2">
                <ul>
                    <li><label><%=controller.text("Parcel No")%>: </label> <%=pr.seqNo%></li>

                    <%if(p.splitParcel==true){%>
                    <li><label><%=controller.text("Area")%>: </label>  <%=pr.areaGeom%> <%=controller.text("M")%><sup>2</sup> (<%=controller.text("Parcel Split")%>)</li>
                        <%}else{%>
                    <li><label><%=controller.text("Area")%>: </label>  <%=pr.areaGeom%> <%=controller.text("M")%><sup>2</sup></li>
                        <%}%>
                </ul>
            </td>
        </tr>
        <%}//for parcel%>
        <tr><td colspan="2" class="table_separetor"><label><%=controller.text("Document Provides")%></label></td></tr>
        <tr>
            <td colspan="2">
                <ol>
                    <li><%=controller.adminSourceText(DocumentTypeInfo.DOC_TYPE_ELDERS_WOREDA_RESOLUTION)%>: (<a href="<%=controller.showDocumentLink(controller.data.claimResolutionDocument)%>"><%=controller.showDocumentText(controller.data.claimResolutionDocument)%></a>)</li>
                    <li><%=controller.adminSourceText(DocumentTypeInfo.DOC_TYPE_LAND_HOLDING_CERTIFICATE)%>: (<a href="<%=controller.showDocumentLink(controller.data.landHoldingCertificateDoc)%>"><%=controller.showDocumentText(controller.data.landHoldingCertificateDoc)%></a>)</li>
                        <%for(SourceDocument doc:controller.data.otherDocument){%>
                    <li><%=controller.adminSourceText(DocumentTypeInfo.DOC_TYPE_OTHER)%>: <%=doc.description%> (<%=doc.refText%>)</li>
                        <%}%>
                </ol>
            </td>
        </tr>
    </tbody>

</table>

