/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var gift_holding_data;
var gift_holders;
var gift_holdingID;
var gift_combined;
var gift_beneficiaries;
var gift_type = null;
var gift_existing_holdingID;
var gift_giver_applicant = null;
var gift_reciever_applicant = null;
function gift_buildTransactionObject()
{
    var rentData = {};

    rentData.tranferType = gift_type;
    rentData.holdingUID = gift_holdingID;
    rentData.otherHoldingUID = gift_existing_holdingID;
    rentData.applicants = gift_giver_applicant;
    rentData.transfers = parcelTransferTable.getTransfers(gift_holding_data);
    rentData.otherDocument = documentPicker.getAllAddtionalDocument();
    rentData.manipulateManually=$("#man_holding").is(":checked");
    if (rentData.tranferType == TRANSFER_TYPE_OTHER_HOLDING)
        rentData.beneficiaries = gift_reciever_applicant;
    else
        rentData.beneficiaries = gift_beneficiaries.parties;
    rentData.claimResolutionDocument = documentPicker.getRequiredDocument(0)
    rentData.landHoldingCertificateDoc = documentPicker.getRequiredDocument(1);

    var tranData = transaction_getTransactionInfo(TRAN_TYPE_GIFT);
    tranData.nrlais_kebeleid = gift_holding_data.nrlais_kebeleid;
    tranData.data = rentData;
    gift_combined = tranData;
}
function gift_configureApplicantPicker()
{
    if (!gift_type || gift_type == -1)
        return;
    var setting = {
        pickPOM: true,
        pickShare: gift_type == TRANSFER_TYPE_THIS_HOLDING,
        selectable: gift_type == TRANSFER_TYPE_THIS_HOLDING,
        shareLabel: lang("Transfer Proportion")};
    applicantPicker.load("#gift_this_holding", gift_holdingID, gift_giver_applicant, setting);
}
function gift_configureBeneficiaryApplicantPicker()
{
    if (gift_type != TRANSFER_TYPE_OTHER_HOLDING || gift_existing_holdingID == null)
    {
        $("#gift_exiting_holding_applicant").html("");
        return;
    }
    var setting = {
        pickPOM: true,
        pickShare: false,
        selectable: false,
    };

    applicantPicker.load("#gift_exiting_holding_applicant", gift_existing_holdingID, applicantPicker.partyItemsToApplicants(gift_reciever_applicant), setting);

}
function gift_setExistingHolding(holdingUID)
{
    gift_existing_holdingID = holdingUID;
    gift_configureBeneficiaryApplicantPicker();
    transaction_map.zoomHolding('nrlais_inventory', holdingUID);
    //show holding information
    $.ajax('/holding/min_holding_search_result.jsp?holding_uid=' + holdingUID, {
        'data': {'method': 'GET'},
        'success': function (data, textStatus, jqXHR) {
            $('#gift_exiting_holding').html(data);
            $('#pick_person').modal('hide');

        }
    });
}
function gift_addGiftHolding(holdingUID)
{
    gift_holdingID = holdingUID;
    transaction_map.zoomHolding('nrlais_inventory', holdingUID);
    //show holding information
    $.ajax('/holding/min_holding_search_result.jsp?holding_uid=' + holdingUID, {
        'data': {'method': 'GET'},
        'success': function (data, textStatus, jqXHR) {
            $('#holder_content').html(data);
            $('#pick_person').modal('hide');
        }
    });


    //load holding data from database
    $.ajax('/api/get_holding?schema=nrlais_inventory&holding_uid=' + holdingUID,
            {
                method: 'GET',
                success: function (data)
                {
                    //alert(JSON.stringify(data));
                    gift_holding_data = data.res;
                    var party_done = [];
                    gift_holders = [];
                    gift_holding_data.parcels.forEach(function (parcel)
                    {
                        parcel.rights.forEach(function (right)
                        {
                            if (right.rightType != RIGHT_TYPE_PUR)
                                return;
                            if (party_done[right.partyUID])
                                return;
                            party_done[right.partyUID] = true;
                            gift_holders.push(right);
                        });
                    });
                },
                dataType: 'json'
            });

    //setup applicant table
    if (gift_combined)
    {
        gift_giver_applicant = gift_combined.data.applicants;
        if (gift_combined.data.tranferType == TRANSFER_TYPE_OTHER_HOLDING)
            gift_reciever_applicant = gift_combined.data.beneficiaries;
        else
            gift_reciever_applicant = null;
    } else
    {
        gift_giver_applicant = null;

    }
    //setup parcel tansfer table
    var parcelSetting = {
        split: true,
        transferArea: false,
        transferShare: false,
        transferLabel: lang('Transfer')
    }
    if (gift_combined == null)
        parcelTransferTable.load('#holder_parcel', holdingUID, null, parcelSetting);
    else
    {
        parcelTransferTable.load('#holder_parcel', holdingUID, gift_combined.data.transfers, parcelSetting);
    }




    //load required documents

    gift_holdingID = holdingUID;
}
function gift_validateApplicantPage() {
    return applicantPicker.validate(gift_holders);
}
function gift_validateApplicationPage()
{
    return transaction_validateApplication() && validatePageOne();
}
function gift_validateParcel() {
    var valid = true;
    var validator = validatorPanel('.parcel-error-panel');
    var parcel = $('#holder_parcel input[field=select]:checked').length > 0;
    if (!parcel) {
        validator.removeError("selectParcel");
        validator.addError("selectParcel", lang("Please select at least one parcel"));
        valid = false;
    } else {
        validator.removeError("selectParcel");
    }
    return valid;
}
function gift_validateParty() {
    var validApplication = true;
    var validator = validatorPanel(".error_gift_recipient_panel");
    if (!$.trim($('#gift_beneficiaries tbody').html()).length) {
        validator.removeError("partyPanel");
        validator.addError("partyPanel", lang("Please add gift recipient party"));
        validApplication = false;
    } else {
        validator.removeError("partyPanel");
    }
    return validApplication;
}
function gift_validateDocument() {
    var valid = true;
    var validator = validatorPanel('.error-req-doc-panel');

    if (!documentPicker.getRequiredDocument(0))
    {
        validator.removeError("claimResolution");
        validator.addError("claimResolution", lang("Please add claim resolution document"));
        valid = false;
    } else {
        validator.removeError("claimResolution");
    }

    if (!documentPicker.getRequiredDocument(1))
    {
        validator.removeError("landHoldingCertificate");
        validator.addError("landHoldingCertificate", lang("Please add land holding certificate"));
        valid = false;
    } else {
        validator.removeError("landHoldingCertificate");
    }
    return valid;
}
function gift_SaveToServer()
{
    transaction_saveTransaction(gift_combined, false, function (ternID) {
        bootbox.alert({
            message: lang("You Have Successfully Registered a Transaction")+"!",
            callback: function () {
                window.location.href = "/dashboard.jsp";
            }
        });
    }, function (err) {
        bootbox.alert(lang("Transaction Registration Failed")+". \n" + err);
    });
}
function gift_showPreview() {
    gift_buildTransactionObject();
    $.ajax({type: 'POST',
        url: "/tran/gift/full_gift_viewer_embeded.jsp",
        contentType: "application/json",
        data: JSON.stringify(gift_combined),
        async: true,
        error: function (errorThrown) {
            bootbox.alert("error");
        },
        success: function (data) {
            var iframe = $('iframe#printSlip').contents();
            iframe.find('body').html(data);
            $('#printSlip').attr('height', document.getElementById("printSlip").contentWindow.document.body.scrollHeight + "px");
        }

    });
}

function gift_setGiftType(type, advance)
{
    var all = ["#step-app", "#step-app", "#step-parcel"
                , "#step-beneficiary", "#step-existing-holding", "#step-docs", "#step-finish"
                , "#step-this-holding"];
    if (gift_type == type)
        return;
    gift_type = type;
    var newTabSetup;
    $("#gift_type_marker_nnew").hide();
    $("#gift_type_marker_other").hide();
    $("#gift_type_marker_this").hide();
    switch (type)
    {
        case TRANSFER_TYPE_NEW_HOLDING:
            newTabSetup = [{tab: "#step-app-tab", cont: "#step-app", enable: true},
                {tab: "#step-type-tab", cont: "#step-type", enable: true},
                {tab: "#step-this-holding-tab", cont: "#step-this-holding", enable: true},
                {tab: "#step-parcel-tab", cont: "#step-parcel", enable: true},
                {tab: "#step-beneficiary-tab", cont: "#step-beneficiary", enable: true},
                {tab: "#step-docs-tab", cont: "#step-docs", enable: true},
                {tab: "#step-finish-tab", cont: "#step-finish", enable: true},
            ];
            $("#gift_type_marker_nnew").show();
            break;
        case TRANSFER_TYPE_OTHER_HOLDING:
            newTabSetup = [{tab: "#step-app-tab", cont: "#step-app", enable: true},
                {tab: "#step-type-tab", cont: "#step-type", enable: true},
                {tab: "#step-this-holding-tab", cont: "#step-this-holding", enable: true},
                {tab: "#step-parcel-tab", cont: "#step-parcel", enable: true},
                {tab: "#step-existing-holding-tab", cont: "#step-existing-holding", enable: true},
                {tab: "#step-docs-tab", cont: "#step-docs", enable: true},
                {tab: "#step-finish-tab", cont: "#step-finish", enable: true},
            ];
            $("#gift_type_marker_other").show();
            break;
        case TRANSFER_TYPE_THIS_HOLDING:
            newTabSetup = [{tab: "#step-app-tab", cont: "#step-app", enable: true},
                {tab: "#step-type-tab", cont: "#step-type", enable: true},
                {tab: "#step-this-holding-tab", cont: "#step-this-holding", enable: true},
                {tab: "#step-beneficiary-tab", cont: "#step-beneficiary", enable: true},
                {tab: "#step-docs-tab", cont: "#step-docs", enable: true},
                {tab: "#step-finish-tab", cont: "#step-finish", enable: true},
            ];
            $("#gift_type_marker_this").show();
            break;
        default:
            newTabSetup = [{tab: "#step-app-tab", cont: "#step-app", enable: true},
                {tab: "#step-type-tab", cont: "#step-type", enable: true},
            ];
            break;
    }
    for (var i in all)
    {
        var hide = true;
        for (var j in newTabSetup)
        {
            if (newTabSetup[j].cont == all[i])
            {
                hide = false;
                break;
            }
        }
        if (hide)
            $(all[i]).hide();
    }

    wizardPages.buildNavHtml("#tab-buttons", newTabSetup);
    if (advance)
        wizardPages.gotoNext();
}
function gift_initGift() {
    //setup wizard
    if (gift_combined)
        gift_setGiftType(gift_combined.data.tranferType);
    else
        gift_setGiftType(-1);
    wizardPages.beforePageChange = function (show, hide)
    {
        var ret = true;
        if (hide == "#step-app-tab") {
            ret = gift_validateApplicationPage();
        } else if (hide == "#step-type-tab") {
            ret = true;
        } else if (hide == "#step-parcel-tab") {
            ret = gift_validateParcel();
        } else if (hide == "#step-beneficiary-tab") {
            ret = gift_validateParty();
        } else if (hide == "#step-existing-holding-tab") {
            ret = true;
        } else if (hide == "#step-this-holding-tab") {
            ret = gift_validateApplicantPage();
        } else if (hide == "#step-docs-tab") {
            ret = gift_validateDocument();
        }

        if (ret)
        {
            if (show == "#step-finish-tab") {
                gift_showPreview();
            }
            if (hide == "#step-this-holding-tab")
            {
                gift_giver_applicant = applicantPicker.getApplicants();
            } else if (hide == "#step-existing-holding-tab")
            {
                gift_reciever_applicant = applicantPicker.getPartyItems();
            }
            if (show == "#step-app-tab")
            {
                app_holdingSearch.onHoldingPicked = gift_addGiftHolding;
            }
            if (show == "#step-this-holding-tab")
            {
                gift_configureApplicantPicker();
            }
            if (show == "#step-existing-holding-tab")
            {
                gift_configureBeneficiaryApplicantPicker();
                app_holdingSearch.onHoldingPicked = gift_setExistingHolding;
            }

        }
        return ret;
    }
    $("#app_type").val(lang("Gift Transaction"));

    app_holdingSearch.onHoldingPicked = gift_addGiftHolding;

    documentPicker.load("#documentTableBody");

    //load existing data
    if (gift_holdingID != null)
    {
        gift_addGiftHolding(gift_holdingID);
    }
    if (gift_combined != null)
    {
        gift_setExistingHolding(gift_combined.data.otherHoldingUID);
    }
    transaction_saveInitial = function ()
    {
        if (!gift_holding_data)
        {
            bootbox.alert(lang('You must at least select holding to save the rent transaction'));
            return;
        }
        gift_buildTransactionObject();
        transaction_saveTransaction(gift_combined, true, function (tranUID) {
            bootbox.alert({
                message: lang("You Have Saved a Transaction Data Temporarily")+"!",
                callback: function () {
                    window.location.href = "/dashboard.jsp";
                }
            });
        }, function (err) {
            bootbox.alert(lang("Saving Transaction Data Failed")+".\n" + err);
        });
    }

    //initialize beneficiaries party list picker
    gift_beneficiaries = getPartyListPicker({
        containerEl: "gift_beneficiaries",
        addButtonEl: "gift_beneficiary_add",
        share: true,
        idDoc: true,
        edit: true,
        delete: true,
        pom: true,
        representative: true,
        member: true,
        guardian: true
    });
    if (gift_combined)
    {
        gift_beneficiaries.setParyItems(gift_combined.data.beneficiaries);
    }


    //load required documents
    if (gift_combined)
    {
        documentPicker.setRequiredDocument(0, gift_combined.data.claimResolutionDocument);
        documentPicker.setRequiredDocument(1, gift_combined.data.landHoldingCertificateDoc);
    }

}
