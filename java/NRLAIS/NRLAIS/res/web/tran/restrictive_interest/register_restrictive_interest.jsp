<%-- 
    Document   : register_rent
    Created on : Jan 24, 2018, 3:29:54 PM
    Author     : yitagesu
--%>
<%@page import="com.intaps.nrlais.model.*" %>
<%@page import="com.intaps.nrlais.repo.*" %>
<%@page import="com.intaps.nrlais.controller.*" %>
<%@page import="com.intaps.nrlais.controller.tran.*" %>
<script src="/assets/JS/wizard_pages.js"></script>
<script src="/assets/JS/ejs.js"></script>
<script src="/party/party_list.js"></script>
<script src="/party/register_party.js"></script>
<%
    RestrictionViewController controller=RestrictionViewController.createController(request,response,containerController.tranType);
%>
<script>
<%if(controller.holding.holdingUID!=null){%>
    var retriction_tran_data =<%=controller.json(controller.tran)%>;
<%}else{%>
    var retriction_tran_data = null;
<%}%>
</script>
<div class="stepwizard">
    <div class="stepwizard-row setup-panel">
        <div class="stepwizard-step">
            <a id='step-1-tab' href="javascript:wizardPages.openPage('#step-1-tab')" type="button" class="btn btn-default btn-circle">1</a>
        </div>
        <div class="stepwizard-step">
            <a id='step-2-tab' href="javascript:wizardPages.openPage('#step-2-tab')" type="button" class="btn btn-default btn-circle">2</a>

        </div>
        <div class="stepwizard-step">
            <a id='step-3-tab' href="javascript:wizardPages.openPage('#step-3-tab')" type="button" class="btn btn-default btn-circle">3</a>

        </div>
        <div class="stepwizard-step">
            <a id='step-4-tab' href="javascript:wizardPages.openPage('#step-4-tab')" type="button" class="btn btn-default btn-circle">4</a>

        </div>
        <div class="stepwizard-step">
            <a id='step-5-tab' href="javascript:wizardPages.openPage('#step-5-tab')" type="button" class="btn btn-default btn-circle">5</a>

        </div>
        <div class="stepwizard-step">
            <a id='step-6-tab' href="javascript:wizardPages.openPage('#step-6-tab')" type="button" class="btn btn-default btn-circle">6</a>

        </div>
        <div class="stepwizard-step">
            <a id='step-7-tab' href="javascript:wizardPages.openPage('#step-7-tab')" type="button" class="btn btn-default btn-circle">7</a>

        </div>
    </div>
</div>
<form role="form" action="" method="post">
    <div class="row setup-content" id="step-1">
        <div class="col-md-12">
            <h4><small><%=controller.text("Set Application")%></small></h4>
            <hr>
            <div class="form-group col-md-6">
                <label class="control-label"><%=controller.text("Application Type")%>:</label>
                <input  type="text"  class="form-control" disabled="disabled" id="app_type" value="<%=controller.tranTypeText()%>"/>
            </div>
            <div class="form-group col-md-6">
                <label class=""><%=controller.text("Application Date")%>:</label>
                <input max="5" type="text" class="form-control" placeholder="<%=controller.text("pick application Date")%>"  id="app_date" dateVal="<%=controller.grigDate(controller.tran.time)%>"/>
            </div>
            <div class="form-group col-md-12">
                <label class="">Application Description:</label>
                <textarea rows="4"  class="form-control" placeholder="<%=controller.text("Write application description")%>" id="app_description"><%=controller.escapeHtml(controller.tran.notes)%></textarea>
            </div>
            <div class="form-group col-md-12" id="holder_content">

            </div>
            <div class="col-md-12">
                <div class="pull-left">
                    <div class="error-panel">

                    </div>
                </div>
                <div class="pull-right">
                    <div class="btn-group">
                        <button class="btn btn-nrlais" type="button" data-toggle='modal' data-target='#pick_person' data-backdrop='static' ><i class="fa fa-hand-o-up"></i><%=controller.text("Load Parcel")%> </button>
                        <button class="btn btn-nrlais" type="button" onclick="wizardPages.gotoNext()" id="nextToSecond"><%=controller.text("Next")%> <i class="fa fa-angle-double-right"></i></button>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="row setup-content" id="step-2">
        <div class="col-md-12">
            <h4><small><%=controller.text("Set Applicant Information")%></small></h4>
            <hr>
            <div id="holder_with_representative">

            </div>
            <div class="pull-left">
                <div class="id-error-panel">

                </div>
            </div>
            <div class="pull-right">
                <div class="btn-group">
                    <button class="btn btn-nrlais" type="button" onclick="wizardPages.gotoNext()" id="nextToThird"><%=controller.text("Next")%> <i class="fa fa-angle-double-right"></i></button>
                </div>
            </div>
        </div>
    </div>
    <div class="row setup-content" id="step-3">
        <div class="col-md-12">
            <h4><small><%=controller.text("Select Parcels")%></small></h4>
            <hr>
            <div id="holder_parcel">
            </div>

            <div class="pull-right">
                <div class="btn-group">
                    <button class="btn btn-nrlais" type="button" onclick="wizardPages.gotoNext()"><%=controller.text("Next")%> <i class="fa fa-angle-double-right"></i></button>
                </div>
            </div>
                <div class="pull-left">
                    <div class="parcel-error-panel error_panel">
                        
                    </div>
                    
                </div>
        </div>
    </div>
    <div class="row setup-content" id="step-4">
        <div class="col-md-12">
            <h4><small><%=controller.text("Third Parties Information")%></small></h4>
            <hr/>
            <div id="restrictive_interest_parties">
            </div>
            <div class="align-right pull-right">
                <div class="btn-group">
                    <button id="restrictive_interest_add" class="btn btn-nrlais" type="button"><i class="fa fa-plus"></i><%=controller.text("Add")%> </button>
                    <button class="btn btn-nrlais nextBtn" type="button" id="doc_nextbtn" onclick="wizardPages.gotoNext()"><%=controller.text("Next")%> <i class="fa fa-angle-double-right"></i></button>
                </div>
            </div>
            <div class="pull-left">
                <div class="btn-group">
                    <div class="error-tenant-party-panel">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row setup-content" id="step-5">
        <div class="col-md-12">
            <h4><small><%=controller.tranTypeText()%><%=controller.text("Detail")%> </small></h4>
            <hr/>
            <div id="restrictive_interest_agreement">
                <div class="form-group">
                    <div class="row">
                        <div class="col-lg-6">
                            <label><%=controller.text("Restriction Type")%></label>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group" >
                                        <select type="text" class="form-control" id="restriction_type">
                                            <option value=""><%=controller.text("Select Type of")%> <%=controller.tranTypeText()%></option>
                                            <%for(Object obj:controller.restrictionTypes()){
                                                IDNameText k=(IDNameText)obj;
                                            %>
                                            <option value="<%=k.id%>" <%=k.id==controller.restrictionType()?"selected":""%>><%=k.name%></option>
                                            <%}%>
                                        </select>
                                    </div>
                                </div>                                
                            </div>    
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-lg-6">
                            <label><%=controller.tranTypeText()%><%=controller.text("Start Date")%> </label>
                            <input type="text" id="restrictive_interest_date_start" class="form-control" dateval="<%=controller.startDate()%>"/>    
                        </div>
                        <div class="col-lg-6">
                            <label><%=controller.tranTypeText()%><%=controller.text("End Date")%> </label>
                            <input type="text" id="restrictive_interest_date_end" class="form-control" dateval="<%=controller.endDate()%>"/>
                        </div>
                    </div>
                </div>
            </div>
            <hr/>
            <%if(containerController.tranType==LATransaction.TRAN_TYPE_RESTRICTIVE_INTEREST){%>   
            <div class="row">
                <div class="input-form col-md-6">
                    <label class="control-label"><%=controller.text("Agreement Document")%></label>
                    <input  type="file" name="uploadimg" class="uploadimg" id="restrictive_interest_agreement_doc" onchange="attachment.openFile(event, 'restrictiveIntersetController.onContractAttached()')"/>
                </div>
                <div class="input-form col-md-6">
                    <label class="control-label"><%=controller.text("Document Reference")%></label>
                    <input  type="text" class="form-control" id="restrictive_interest_agreement_doc_ref" value="<%=controller.agreementRef()%>" />
                </div>
                <div class="input-form col-md-12">
                    <label class="control-label"><%=controller.text("Document Description")%></label>
                    <textarea  rows="5" class="form-control" id="restrictive_interest_agreement_disc"><%=controller.agreementDesc()%></textarea>
                </div>
            </div>
            <%}%>
            <div class="align-right pull-right">
                <div class="btn-group">
                    <button class="btn btn-nrlais nextBtn" type="button" id="doc_nextbtn" onclick="wizardPages.gotoNext()"><%=controller.text("Next")%> <i class="fa fa-angle-double-right"></i></button>
                </div>
            </div>
            <div class="pull-left">
                <div class="btn-group">
                    <div class="error-tenant-party-panel">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row setup-content" id="step-6">
        <div class="col-md-12">
            <h4><small><%=controller.text("Add Required Documents")%></small></h4>
            <hr/>
            <table class="table table-striped jambo_table">
                <thead>
                    <tr>
                        <td><%=controller.text("Document Type")%></td>
                        <td><%=controller.text("Document Reference")%></td>
                        <td><%=controller.text("Document Description")%></td>
                        <td><%=controller.text("Document File")%></td>
                        <td></td>
                    </tr>
                </thead>
                <tbody id="documentTableBody">
                    <%if(containerController.tranType==LATransaction.TRAN_TYPE_RESTRICTIVE_INTEREST){%>
                    <tr doc_type="<%=DocumentTypeInfo.DOC_TYPE_COURT_ORDER%>">
                        <td><%=controller.text("Court Order")%>:</td>
                        <td id="doc_ref"></td>
                        <td id="doc_desc"></td>
                        <td id="doc_File"></td>
                        <td><a  class="btn btn-nrlais"><i class="fa fa-plus"></i></a> </td>
                    </tr>
                    <%}%>
                    <tr doc_type="<%=DocumentTypeInfo.DOC_TYPE_LAND_HOLDING_CERTIFICATE%>">
                        <td><%=controller.text("Land holding certificate")%>:</td>
                        <td id="doc_ref"></td>
                        <td id="doc_desc"></td>
                        <td id="doc_File"></td>
                        <td><a  class="btn btn-nrlais"><i class="fa fa-plus"></i></a> </td>
                    </tr>
                </tbody>
            </table>

            <div class="align-right pull-right">
                <div class="btn-group">
                    <button class="btn btn-nrlais" type="button" data-toggle='modal' data-target='#add_document' data-backdrop='static'><i class="fa fa-plus"></i><%=controller.text("Add")%> </button>
                    <button class="btn btn-nrlais nextBtn" type="button" id="doc_nextbtn" onclick="wizardPages.gotoNext()"><%=controller.text("Next")%> <i class="fa fa-angle-double-right"></i></button>
                </div>
            </div>
            <div class="pull-left">
                <div class="btn-group">
                    <div class="error-req-doc-panel">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row setup-content" id="step-7">
        <div class="col-md-12">
            <h4> <small><%=controller.text("Review and Submit Application")%></small></h4>
            <hr>
            <div class="print_slip" id="print_slip">
            </div>
            <div class="align-right">
                <div class="btn-group">
                    <button class="btn btn-nrlais" type="button" id="print"><i class="fa fa-print"></i><%=controller.text("Print")%> </button>
                    <button class="btn btn-nrlais nextBtn" type="button" id="restrictive_interest_save_btn" ><%=controller.text("Register Transaction")%> <i class="fa fa-exclamation"></i></button>
                </div>
            </div>
        </div>
    </div>
</form>
