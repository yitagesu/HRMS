<%@ include file="/remark_popover.jsp" %>   
<tr>
    <td class="table_separetor"><label><%=controller.tranTypeText()%> <%=controller.text("Transaction")%></label> (<%=controller.tran.nrlais_kebeleid%>/<%=controller.tran.seqNo%>)<br>
        <small>(<%=controller.statusMessage()%>)</small>
    </td>
    <td class="align-right table_separetor">
        <%if(controller.editExOfficeTransaction()){%>
        <a target='_parent' href="/lr_manipulator/lr_manipulator.jsp?tran_uid=<%=controller.tran.transactionUID%>&tran_type=<%=controller.tran.transactionType%>" class="btn btn-warning btn-sm" id='edit_tran_btn'><%=controller.text("Manipulate Holding")%></a>
        <%}%>
        <%if(controller.showEdit()){%>
        <a target='_parent' href="/register_transaction.jsp?tran_uid=<%=controller.tran.transactionUID%>&tran_type=<%=controller.tran.transactionType%>" class="btn btn-warning btn-sm" id='edit_tran_btn'><%=controller.text("Edit Transaction")%></a>
        <%}%>
        <%if(controller.showApprove()){%>
        <button  class="btn btn-warning btn-sm" id='edit_tran_btn_approve' data-toggle="popover-x" data-target="#remarkPopover_accept" data-placement="bottom"><%=controller.text(controller.approveText())%></button>
        <%}%>
        <%if(controller.showReject()){%>
        <button  class="btn btn-warning btn-sm" id='edit_tran_btn_reject' data-toggle="popover-x" data-target="#remarkPopover_reject    " data-placement="bottom"><%=controller.text(controller.rejectText())%></button>
        <%}%>
    </td>
</tr>
<tr><td><label><%=controller.text("Application Type")%>:</label> <%=controller.text(controller.tranType.name.textEn)%></td><td><label><%=controller.text("Application Date")%>:</label> <span id="app_time"><%=EthiopianCalendar.ToEth(controller.tran.time).toString()+" "+controller.text("EC")%></span></td></tr>
<tr><td><label><%=controller.text("Woreda")%>:</label> <%=controller.text(controller.woreda==null?"":controller.woreda.name.textEn)%></td> <td><label><%=controller.text("Kebele")%>:</label> <%=controller.text(controller.kebele==null?"":controller.kebele.name.textEn)%></td></tr>
