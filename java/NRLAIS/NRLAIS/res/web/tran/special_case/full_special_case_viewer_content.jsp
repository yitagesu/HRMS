<table class="table table-striped">
    <tbody id="">
        <%@ include file="/tran/tran_viewer_header.jsp" %>
        <tr><td class="table_separetor" colspan="2"><label><%=controller.text("Holder Information")%></label></td> </tr>
        <%for(PartyItem r:controller.applicants()){
            String holderROLE=controller.roleText(r.party.mreg_familyrole);
            String holderSEX=controller.sexText(r.party.sex);
            
        %>
        <tr id="final_" class="holder">
            <td class="many">
                <label><%=holderROLE%> Info</label>
                <ul>
                    <li><label><%=controller.text("Name")%>: </label> <%=r.party.getFullName()%></li>
                    <li><label><%=controller.text("Sex")%>Sex: </label> <%=holderSEX%></li>
                    <li><label><%=controller.text("Age")%>Age: </label> <%=EthiopianCalendar.ToEth(r.party.dateOfBirth).toString()+" EC"%></li>
                    <li><label><%=controller.text("Share")%>Share: </label> <%=r.share.num%>/<%=r.share.denum%></li>
                   </ul>
            </td>
            <td class="many">
        </tr>
        <%}//for%>
        
         <tr><td colspan="2" class="table_separetor"><label><%=controller.text("Parcel Information")%></label> </td></tr>
        <%for(ParcelItem p:controller.parcels()){
           
        %>
        <tr>
            <td class="many" colspan="2">
                <ul>
                    <li><label><%=controller.text("Parcel No")%>: </label> <%=controller.parcelNo(p.parcel)%></li>
                    <li><label><%=controller.text("Area")%>: </label> <%=p.parcel.areaGeom%> M<sup>2</sup></li>
                    <li><label><%=controller.text("Team ID")%>: </label> <%=p.parcel.mreg_teamid%></li>
                    <li><label><%=controller.text("Map Sheet No")%>Map Sheet No: </label> <%=p.parcel.mreg_mapsheetNo%></li>
                    <li><label><%=controller.text("Acquisition Type")%>: </label> <%=p.parcel.mreg_actype%></li>
                    <li><label><%=controller.text("Acquisition Year")%>: </label> <%=p.parcel.mreg_acyear%></li>
                    <li><label><%=controller.text("Current Land Use")%>: </label> <%=p.parcel.landUse%></li>
                     <li><label><%=controller.text("Survey Date")%>: </label> <%=EthiopianCalendar.ToEth(p.parcel.mreg_surveyDate).toString()+" EC"%></li>
                </ul>
            </td>
        </tr>
        <%}//for%>
        <tr><td colspan="2" class="table_separetor"><label><%=controller.text("Document Provides")%>:</label></td></tr>
        <tr>
            <td colspan="2">
                <ol>
                    <li><%=controller.adminSourceText(DocumentTypeInfo.DOC_TYPE_DIVORCE_CLAIM_RESOLUTION)%> (<a href="<%=controller.claimResolutionLink()%>"><%=controller.claimResolutionText()%></a>)</li>
                    <li><%=controller.adminSourceText(DocumentTypeInfo.DOC_TYPE_FIRST_LEVEL_CERTIFICATE)%> first holding(<a href="<%=controller.landHoldingLink()%>"><%=controller.landHoldingText()%></a>)</li>
                    <%for(SourceDocument doc:controller.data.otherDocument){%>
                        <li><%=controller.adminSourceText(DocumentTypeInfo.DOC_TYPE_OTHER)%>: <%=doc.description%> (<%=doc.refText%>)</li>
                    <%}%>
                </ol>
            </td>
        </tr>
    </tbody>
</table>
