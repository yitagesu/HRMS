/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var special_case_combined;
var special_case_parcel_picker;
var special_case_beneficiaries_picker;
function special_case_buildTransactionObject()
{
    var data = {
        holding: {
            nrlais_kebeleid: $("#special_case_kebele").val(),
            holdingType: $("#special_case_holdingType").val(),
        },
        applicants: special_case_beneficiaries_picker.parties,
        parcels: special_case_parcel_picker.parcels,
        claimResolutionDoc: documentPicker.getRequiredDocument(0),
        firtLevelLandHoldingBook: documentPicker.getRequiredDocument(1),
        otherDocument: documentPicker.getAllAddtionalDocument()
    };
    var tranData = transaction_getTransactionInfo(TRAN_TYPE_SPECIAL_CASE);
    tranData.nrlais_kebeleid = $("#special_case_kebele").val();
    tranData.data = data;
    special_case_combined = tranData;

}
function special_case_SaveToServer() {
    transaction_saveTransaction(special_case_combined, false, function (ternID) {
        bootbox.alert({
            message: lang("You Have Successfully Registered a Transaction") + "!",
            callback: function () {
                window.location.href = "/dashboard.jsp";
            }
        });
    }, function (err) {
        bootbox.alert(lang("Transaction Registration Failed") + ".\n" + err);
    });
}
function special_case_showPreview() {
    special_case_buildTransactionObject();
    $.ajax({type: 'POST',
        url: "/tran/special_case/full_special_case_viewer_embeded.jsp",
        contentType: "application/json",
        data: JSON.stringify(special_case_combined),
        async: true,
        error: function (errorThrown) {
            bootbox.alert(lang("error"));
        },
        success: function (data) {
            $("#print_slip").html(data);
        }

    });
}

function special_case_validateApplicationPage()
{
   validApplication = true;
    if ($('#app_date').val() == "") {
        $('#emptyDate').remove();
        $('.error-panel').append("<span id='emptyDate'> <i class='fa fa-warning faa-flash animated'></i> Please Pick The Application Date<br/></span>");
        $('#app_date').removeClass('form-control');
        $('#app_date').addClass('form-error');
        validApplication = false;
    } else {
        $('#emptyDate').remove();
        $('#app_date').removeClass('form-error');
        $('#app_date').addClass('form-control');
        if (!isValidDate($("#app_date"))) {
            $('#invalidDate').remove();
            $('.error-panel').append("<span id='invalidDate'><i class='fa fa-warning faa-flash animated'></i> Invalid Date Please Fix the Date<br/></span>");
            $('#app_date').removeClass('form-control');
            $('#app_date').addClass('form-error');
            validApplication = false;
        } else {
            $('#invalidDate').remove();
            $('#app_date').removeClass('form-error');
            $('#app_date').addClass('form-control');
        }
    }
    if ($("#special_case_kebele").val() == "") {
        $("#special_case_kebele").removeClass('form-control');
        $('#special_case_kebele').addClass('form-error');
        validApplication = false;
    } else {
        $('#special_case_kebele').removeClass('form-error');
        $('#special_case_kebele').addClass('form-control');
    }
    if ($("#special_case_holdingType").val() == "") {
        $("#special_case_holdingType").removeClass('form-control');
        $('#special_case_holdingType').addClass('form-error');
        validApplication = false;
    } else {
        $('#special_case_holdingType').removeClass('form-error');
        $('#special_case_holdingType').addClass('form-control');
    }
    return validApplication;
}
function special_case_validateApplicantsPage() {
    var validApplication = true;
        var validator = validatorPanel(".error-tenant-party-panel");
        if (!$.trim($('#special_case_beneficiaries tbody').html()).length) {
            validator.removeError("partyPanel");
            validator.addError("partyPanel", lang("Please add Party Information"));
            validApplication = false;
        } else {
            validator.removeError("partyPanel");
        }
        return validApplication;
}
function special_case_validateParcelPage(){
    var valid = true;
        var validator = validatorPanel('.parcel-error-panel');
        if (!$.trim($('#special_case_parcels tbody').html()).length) {
            validator.removeError("selectParcel");
            validator.addError("selectParcel", lang("Please add at least one parcel"));
            valid = false;
        } else {
            validator.removeError("selectParcel");
        }
        return valid;
}
function special_case_validReqDocument() {
    var valid = true;
    $("#uploadCleamResolution").remove();
    $("#uploadLandCertificate").remove();
    if (!documentPicker.getRequiredDocument(0))
    {
        $(".error-req-doc-panel").append("<span id='uploadCleamResolution'><i class='fa fa-warning faa-flash animated'></i>" + lang("Please add claim resolution document") + "<br/></span>");
        valid = false;
    } else {
        $("#uploadCleamResolution").remove();
    }
    if (!documentPicker.getRequiredDocument(1)) {
        $(".error-req-doc-panel").append("<span id='uploadLandCertificate'><i class='fa fa-warning faa-flash animated'></i>" + lang("Please Add proof of Holding") + "<br/></span>");
        valid = false;
    } else {
        $("#uploadLandCertificate").remove();
    }

    return valid;
}


function special_case_configureWizard()
{
    wizardPages.buildNavHtml("#tab-buttons", [
        {tab: "#step-app-tab", cont: "#step-app", enable: true},
        {tab: "#step-holders-tab", cont: "#step-holders", enable: true},
        {tab: "#step-parcel-tab", cont: "#step-parcel", enable: true},
        {tab: "#step-doc-tab", cont: "#step-doc", enable: true},
        {tab: "#step-finish-tab", cont: "#step-finish", enable: true},
    ]);

    wizardPages.beforePageChange = function (show, hide)
    {
        var ret = true;
        if (hide == "#step-app-tab")
        {
            ret = special_case_validateApplicationPage();
        } else if (hide == "#step-holders-tab")
        {
            ret = special_case_validateApplicantsPage();

        } else if (hide == "#step-parcel-tab")
        {
            ret = special_case_validateParcelPage();
        } else if (hide == "#step-doc-tab")
        {
            ret = special_case_validReqDocument();
        }
        if (show == "#step-finish-tab")
        {
            special_case_showPreview();
        }
        return ret;
    }
}
function special_case_init() {

    //configure wizard pages
    special_case_configureWizard();

    $("#app_type").val(lang("Special Case Transaction"));

    //configure holding form
    if (special_case_combined)
    {
        $("#special_case_kebele").val(special_case_combined.data.holding.nrlais_kebeleid);
        $("#special_case_holdingType").val(special_case_combined.data.holding.holdingType);
    }

    //setup required document picker
    documentPicker.load("#documentTableBody");
//override save initial transaction
    transaction_saveInitial = function ()
    {
        special_case_buildTransactionObject();
        transaction_saveTransaction(special_case_combined, true, function (tranUID) {
            bootbox.alert({
                message: lang("You Have Saved a Transaction Data Temporarily") + "!",
                callback: function () {
                    window.location.href = "/dashboard.jsp";
                }
            });
        }, function (err) {
            bootbox.alert(lang("Saving Transaction Data Failed") + ".\n" + err);
        });
    }
    //initialize parcel list picker
    special_case_parcel_picker = getParcelListPicker({
        containerEl: "special_case_parcels",
        addNewButtonEl: "special_case_parcel_add",
        allowExisting: false,
        allowNew: true,
        edit: true,
        delete: true,
    });

    if (special_case_combined)
    {
        special_case_parcel_picker.setParcelItems(special_case_combined.data.parcels);
    }

    //initialize beneficiaries party list picker
    special_case_beneficiaries_picker = getPartyListPicker({
        containerEl: "special_case_beneficiaries",
        addButtonEl: "special_case_beneficiary_add",
        share: true,
        idDoc: true,
        edit: true,
        delete: true,
        member: true,
        guardian: true,
        partyType: -1,
    });
    special_case_beneficiaries_picker.onChanged = function ()
    {

    };
    if (special_case_combined)
    {
        special_case_beneficiaries_picker.setParyItems(special_case_combined.data.applicants);
    }

    //load required documents
    if (special_case_combined)
    {
        documentPicker.setRequiredDocument(0, special_case_combined.data.claimResolutionDoc);
        documentPicker.setRequiredDocument(1, special_case_combined.data.firtLevelLandHoldingBook);
        if (special_case_combined.data.otherDocument.length > 0) {
            documentPicker.setAddtionalDocument(special_case_combined.data.otherDocument);
        }
    }

    $("#special_case_kebele").change(function () {
        //console.log($("#special_case_kebele" ).find('option:selected').val());
        transaction_map.zoomKebele('nrlais_sys', $("#special_case_kebele").find('option:selected').val());
    });
}






