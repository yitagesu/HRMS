<%@page import="com.intaps.nrlais.model.*" %>
<%@page import="com.intaps.nrlais.repo.*" %>
<%@page import="com.intaps.nrlais.controller.*" %>
<%@page import="com.intaps.nrlais.controller.tran.*" %>
<script src="assets/JS/wizard_pages.js"></script>
<script src="/assets/JS/ejs.js"></script>
<script src="/party/party_list.js"></script>
<script src="/party/register_party.js"></script>
<script src="/parcel/parcel_list.js"></script>
<script src="/parcel/register_parcel.js"></script>

<%
    SpecialCaseViewController controller=new SpecialCaseViewController(request,response,true);
    /*if there is a holding already saved we will declare the holding variable here
    this will override the declartion in register_inheritance.jsp and the inheritance_init function will load the holding when 
    it sees that the hodling is not empty
    */
    if(controller.data.holding!=null){ 
%>
<script>
    var special_case_combined =<%=controller.json(controller.tran)%>;
</script>
<%}%>
<div class="stepwizard">
    <div class="stepwizard-row setup-panel" id="tab-buttons">

    </div>
</div>
<form role="form" action="" method="post">
    <div class="row setup-content" id="step-app">
        <div class="col-md-12">
            <h4><small><%=controller.text("Set Application")%></small></h4>
            <hr>
            <div class="form-group col-md-6">
                <label class="control-label"><%=controller.text("Application Type")%>:</label>
                <input  type="text"  class="form-control" disabled="disabled" id="app_type" value="<%=controller.tranTypeText()%>"/>
            </div>
            <div class="form-group col-md-6">
                <label class=""><%=controller.text("Application Date")%>:</label>
                <input max="5" type="text" class="form-control" placeholder="<%=controller.text("pick application Date")%>"  id="app_date" dateVal="<%=controller.grigDate(controller.tran.time)%>"/>
            </div>
            <div class="form-group col-md-6">
                <select class="form-control" id="special_case_kebele">
                    <option value=""><%=controller.text("Select Kebele")%></option>
                    <%for(CodeText k:controller.allKebeles()){%>
                    <option value="<%=k.code%>"><%=k.name%> (<%=k.code%>)</option>
                    <%}%>
                </select>
            </div>
            <div class="form-group col-md-6">
                <select  class="form-control" id="special_case_holdingType">
                    <option value=""><%=controller.text("Select Holding Type")%></option>
                    <%for(IDNameText h:controller.holdingTypeList()){%>
                    <option value="<%=h.id%>"><%=h.name%></option>
                    <%}%>
                </select>
            </div>
            <div class="form-group col-md-12">
                <label class=""><%=controller.text("Application Description")%>:</label>
                <textarea rows="4"  class="form-control" placeholder="<%=controller.text("Write application description")%>" id="app_description"><%=controller.escapeHtml(controller.tran.notes)%></textarea>
            </div>

            <div class="col-md-12">
                <div class="pull-left">
                    <div class="error-panel">

                    </div>
                </div>
                <div class="pull-right">
                    <div class="btn-group">
                        <button class="btn btn-nrlais" type="button" onclick="wizardPages.gotoNext()" id="nextToSecond"><%=controller.text("Next")%> <i class="fa fa-angle-double-right"></i></button>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="row setup-content" id="step-holders">
        <div class="col-md-12">
            <h4><small><%=controller.text("Add Holders")%></small></h4>
            <hr>
            <div id="special_case_beneficiaries">
            </div>
            <div class="align-right pull-right">
                <div class="btn-group">
                    <button id="special_case_beneficiary_add" class="btn btn-nrlais" type="button" ><i class="fa fa-plus"></i> <%=controller.text("Add")%></button>
                    <button class="btn btn-nrlais nextBtn" type="button" id="doc_nextbtn" onclick="wizardPages.gotoNext()"><%=controller.text("Next")%> <i class="fa fa-angle-double-right"></i></button>
                </div>
            </div>
            <div class="pull-left">
                <div class="btn-group">
                    <div class="error-tenant-party-panel error_panel">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row setup-content" id="step-parcel">
        <div class="col-md-12">
            <h4> <small><%=controller.text("Add Parcels")%></small></h4>
            <hr>
            <div id="special_case_parcels">
            </div>
            <div class="pull-right">
                <div class="btn-group">
                    <button id="special_case_parcel_add" class="btn btn-nrlais" type="button" ><i class="fa fa-plus"></i> <%=controller.text("Add")%></button>
                    <button class="btn btn-nrlais" type="button" onclick="wizardPages.gotoNext()"><%=controller.text("Next")%> <i class="fa fa-angle-double-right"></i></button>
                </div>
            </div>
                <div class="pull-left">
                    <div class="parcel-error-panel error_panel"></div>
                </div>
        </div>
    </div>
    <div class="row setup-content" id="step-doc">
        <div class="col-md-12">
            <h4><small><%=controller.text("Add Required Documents")%></small></h4>
            <table class="table table-striped jambo_table">
                <thead>
                    <tr>
                        <td><%=controller.text("Document Type")%></td>
                        <td><%=controller.text("Document Reference")%></td>
                        <td><%=controller.text("Document Description")%></td>
                        <td><%=controller.text("Document File")%></td>
                        <td></td>
                    </tr>
                </thead>
                <tbody id="documentTableBody">                    
                    <tr doc_type="<%=DocumentTypeInfo.DOC_TYPE_DIVORCE_CLAIM_RESOLUTION%>">
                        <td><%=controller.text("Claim Resolution Document")%>:</td>
                        <td id="doc_ref"></td>
                        <td id="doc_desc"></td>
                        <td id="doc_File"></td>
                        <td><a class="btn btn-nrlais"><i class="fa fa-plus"></i></a> </td>
                    </tr>
                    <tr doc_type="<%=DocumentTypeInfo.DOC_TYPE_FIRST_LEVEL_CERTIFICATE%>">
                        <td><%=controller.text("Proof of Holding")%>:</td>
                        <td id="doc_ref"></td>
                        <td id="doc_desc"></td>
                        <td id="doc_File"></td>
                        <td><a class="btn btn-nrlais"><i class="fa fa-plus"></i></a> </td>
                    </tr>
                </tbody>
            </table>
            <div class="align-right pull-right">
                <div class="btn-group">
                    <button class="btn btn-nrlais" type="button" data-toggle='modal' data-target='#add_document' data-backdrop='static'><i class="fa fa-plus"></i><%=controller.text("Add")%> </button>
                    <button class="btn btn-nrlais nextBtn" type="button" id="doc_nextbtn" onclick="wizardPages.gotoNext()"><%=controller.text("Next")%> <i class="fa fa-angle-double-right"></i></button>
                </div>
            </div>
            <div class="pull-left">
                <div class="btn-group">
                    <div class="error-req-doc-panel">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row setup-content" id="step-finish">
        <div class="col-md-12">
            <h4><small><%=controller.text("Review and Submit Application")%></small></h4>
            <hr>
            <div class="print_slip" id="print_slip">
            </div>
            <div class="align-right">
                <div class="btn-group">
                    <button class="btn btn-nrlais" type="button" id="print"><i class="fa fa-print"></i><%=controller.text("Print")%> </button>
                    <button class="btn btn-nrlais nextBtn" type="button" id="doc_nextbtn" onclick="special_case_SaveToServer()"><%=controller.text("Register Transaction")%> <i class="fa fa-exclamation"></i></button>
                </div>
            </div>
        </div>
    </div>
</form>


                        <script>
                            
                        </script>
