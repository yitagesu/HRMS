<table class="table table-striped">
    <tbody id="">

        <%@ include file="/tran/tran_viewer_header.jsp" %>        
        <tr><td class="table_separetor" colspan="2"><label><%=controller.text("Land Holder")%></label></td></tr>
        <%for(LADM.Right r:controller.holding.getHolders()){
            String holderROLE=controller.roleText(r.party.mreg_familyrole);
            String holderSEX=controller.sexText(r.party.sex);
            Applicant ap=controller.partyApplicant(r.party.partyUID);
        %>
        <tr id="final_<%=controller.holding.holdingUID%>" class="holder">
            <td class="many" colspan="2">
                <label><%=holderROLE%> <%=controller.text("Info")%></label>
                <ul>
                    <li><label><%=controller.text("Name")%>: </label> <%=r.party.getFullName()%></li>
                    <li><label><%=controller.text("Sex")%>: </label> <%=holderSEX%></li>
                    <li><label><%=controller.text("Age")%>: </label> <span id="ageOfApp"><%=controller.age(r.party.dateOfBirth)%> (<%=EthiopianCalendar.ToEth(r.party.dateOfBirth).toString()+" "+controller.text("EC")%>)</span></li>
                    <li><label><%=controller.text("Share")%>: </label> <%=r.share.num%>/<%=r.share.denum%></li>
                    
                </ul>
            </td>
        </tr>
        <%}//for%>
        <tr><td colspan="2" class="table_separetor"><label><%=controller.text("Applicant Information")%></label></td></tr>
        <%for(PartyItem w:controller.data.wordaAdministrtion){
               
        %>
        <tr>
            <td class="many" colspan="2">
                <ul>
                    <li><label><%=controller.text("Woreda Name")%>: </label> <%=w.party.name1%></li>
                    <li><label><%=controller.text("Address")%>: </label> 
                       <% for(LADM.PartyContact a:w.party.contacts){%>
                        <%=a.contactData%>
                                <%}%>
                    </li>
                </ul>
            </td>
        </tr>
        <%}%>
        <tr><td colspan="2" class="table_separetor"><label><%=controller.text("Parcel for Expropriation")%></label> </td></tr>
        <%for(ParcelTransfer p:controller.data.transfers){
               LADM.Parcel pr=controller.transferParcel(p);
        %>
        <tr>
            <td class="many" colspan="2">
                <ul>
                    <li><label><%=controller.text("Parcel No")%>: </label> <%=pr.seqNo%></li>
                    
                    <%if(p.splitParcel==true){%>
                    <li><label><%=controller.text("Area")%>: </label>  <%=pr.areaGeom%> <%=controller.text("M")%><sup>2</sup>  (<%=controller.text("Parcel Split")%>)</li>
                    <%}else{%>
                    <li><label><%=controller.text("Area")%>: </label>  <%=pr.areaGeom%> <%=controller.text("M")%><sup>2</sup></li>
                    <%}%>
                </ul>
            </td>
        </tr>
        <%}%>
        <tr><td colspan="2" class="table_separetor"><label><%=controller.text("Document Provides")%></label></td></tr>
        <tr>
            <td colspan="2">
                <ol>
                    <li><%=controller.adminSourceText(DocumentTypeInfo.DOC_TYPE_EXPROPRATION_DECISION)%> (<a href="<%=controller.decisionOfWoredaAdministrationLink()%>"><%=controller.decisionOfWoredaAdministrationText()%></a>)</li>
                    <li><%=controller.adminSourceText(DocumentTypeInfo.DOC_TYPE_PROOF_OF_COMPENSATION)%> (<a href="<%=controller.proofOfpaymentOfCompensationLink()%>"><%=controller.proofOfpaymentOfCompensationText()%></a>)</li>
                    <li><%=controller.adminSourceText(DocumentTypeInfo.DOC_TYPE_LAND_HOLDING_CERTIFICATE)%> (<a href="<%=controller.landHoldingCertificateLink()%>"><%=controller.landHoldingCertificateText()%></a>)</li>
                    <%for(SourceDocument doc:controller.data.otherDocument){%>
                        <li><%=controller.adminSourceText(DocumentTypeInfo.DOC_TYPE_OTHER)%>: <%=doc.description%> (<%=doc.refText%>)</li>
                    <%}%>
                </ol>
            </td>
        </tr>
    </tbody>

</table>