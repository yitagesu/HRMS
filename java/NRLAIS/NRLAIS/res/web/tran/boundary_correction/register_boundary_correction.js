/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var boundary_correction_holding_data;
var boundary_correction_holders;
var boundary_correction_holdingID;
var boundary_correction_combined;
var boundary_correction_parcel = null;
var boundary_correction_adjacent_parcels;
function boundary_correction_buildTransactionObject()
{
    var data = {};

    data.parcel = {parcelUID: boundary_correction_parcel, holdingUID: boundary_correction_holdingID};
    data.adjucentParcels = [];
    boundary_correction_adjacent_parcels.getSelectd().forEach(function (p) {
        data.adjucentParcels.push({parcelUID: p});
    });
    data.applicants = applicantPicker.getApplicants();
    data.landHoldingCertificateDoc = documentPicker.getRequiredDocument(0);
    data.otherDocument=documentPicker.getAllAddtionalDocument()

    var tranData = transaction_getTransactionInfo(TRAN_TYPE_BOUNDARY_CORRECTION);
    tranData.nrlais_kebeleid = boundary_correction_holding_data.nrlais_kebeleid;
    tranData.data = data;
    boundary_correction_combined = tranData;
}

function boundary_correction_addHolding(holdingUID)
{
    boundary_correction_holdingID = holdingUID;
    transaction_map.zoomHolding('nrlais_inventory', holdingUID);
    //show holding information
    $.ajax('/holding/min_holding_search_result.jsp?holding_uid=' + holdingUID, {
        'data': {'method': 'GET'},
        'success': function (data, textStatus, jqXHR) {
            $('#holder_content').html(data);
            $('#pick_person').modal('hide');
        }
    });


    //load holding data from database
    $.ajax('/api/get_holding?schema=nrlais_inventory&holding_uid=' + holdingUID,
            {
                method: 'GET',
                success: function (data)
                {
                    //alert(JSON.stringify(data));
                    boundary_correction_holding_data = data.res;
                    var party_done = [];
                    boundary_correction_holders = [];
                    boundary_correction_holding_data.parcels.forEach(function (parcel)
                    {
                        parcel.rights.forEach(function (right)
                        {
                            if (right.rightType != RIGHT_TYPE_PUR)
                                return;
                            if (party_done[right.partyUID])
                                return;
                            party_done[right.partyUID] = true;
                            boundary_correction_holders.push(right);
                        });
                    });
                },
                dataType: 'json'
            });

    //setup applicant table
    var setting = {
        pickPOM: true,
        pickShare: false,
        selectable: false
    };

    if (boundary_correction_combined)
        applicantPicker.load("#holder_with_representative", holdingUID, boundary_correction_combined.data.applicants, setting);
    else
        applicantPicker.load("#holder_with_representative", holdingUID, null, setting);

    //setup parcel tansfer table
    var parcelSetting = {
        split: false,
        transferArea: false,
        transferShare: false,
        singleParcel: true,
        transferLabel: lang('Select')
    };
    if (boundary_correction_combined == null)
        parcelTransferTable.load('#holder_parcel', holdingUID, null, parcelSetting);
    else
    {
        parcelTransferTable.load('#holder_parcel', holdingUID, [{parcelUID: boundary_correction_combined.data.parcel.parcelUID}], parcelSetting);
    }

    //setup neighbours selector
    if (boundary_correction_combined)
    {
        var n = [];
        boundary_correction_combined.data.adjucentParcels.forEach(function (nb) {
            n.push(nb.parcelUID);
        });
        boundary_correction_parcel = boundary_correction_combined.data.parcel.parcelUID;
        boundary_correction_adjacent_parcels.load(boundary_correction_parcel, n);
    } else
    {
        boundary_correction_parcel = null;
        boundary_correction_adjacent_parcels.load();
    }
    if (boundary_correction_combined)
    {
        documentPicker.setRequiredDocument(0, boundary_correction_combined.data.landHoldingCertificateDoc);
//        if (boundary_correction_combined.data.otherDoc.length > 0) {
//            documentPicker.setAddtionalDocument(boundary_correction_combined.data.otherDoc);
//        }
    }



    //load required documents

    boundary_correction_holdingID = holdingUID;
}
function boundary_correction_validateApplicantPage() {
    return applicantPicker.validate(boundary_correction_holders);
}
function boundary_correction_validateApplicationPage()
{
    return transaction_validateApplication() && validatePageOne();
}
function boundary_correction_SaveToServer()
{
    transaction_saveTransaction(boundary_correction_combined, false, function (ternID) {
        bootbox.alert({
            message: lang("You Have Successfully Registered a Transaction")+"!",
            callback: function () {
                window.location.href = "/dashboard.jsp";
            }
        });
    }, function (err) {
        bootbox.alert(lang("Transaction Registration Failed")+"\n" + err);
    });
}
function boundary_correction_showPreview() {
    boundary_correction_buildTransactionObject();
    $.ajax({type: 'POST',
        url: "/tran/boundary_correction/full_boundary_correction_embeded.jsp",
        contentType: "application/json",
        data: JSON.stringify(boundary_correction_combined),
        async: true,
        error: function (errorThrown) {
            alert("error");
        },
        success: function (data) {

            var iframe = $('iframe#printSlip').contents();
            iframe.find('body').html(data);
            $('#printSlip').attr('height', document.getElementById("printSlip").contentWindow.document.body.scrollHeight + "px");
        }

    });
}
function boundary_correction_validateParcelPage() {
    var valid = true;
    var validator = validatorPanel('.parcel-error-panel');
    var parcel = $('#holder_parcel  input:checked').length > 0;
    if (!parcel) {

        validator.addError("selectParcel", lang("Please select only one parcel for boundary correction"));
        valid = false;
    } else {
        validator.removeError("selectParcel");
    }
    return valid;
}

function boundary_correction_validateNeighboringParcel() {
    var valid = true;
    var validator = validatorPanel('.error_boundary_correction_neighbors');
    var parcel = $('#boundary_correction_neighbors  input:checked').length > 0;
    if (!parcel) {

        validator.addError("neighborSelectParcel", lang("Please Select one parcel"));
        valid = false;
    } else {
        validator.removeError("neighborSelectParcel");
    }
    return valid;
}
function boundary_correction_validateReqDocument() {
    var valid = true;
    if (!documentPicker.getRequiredDocument(0)) {
        $("#boundary_correction_doc_error_panel #uploadLandCertificate").remove();
        $("#boundary_correction_doc_error_panel").append("<span id='uploadLandCertificate'><i class='fa fa-warning faa-flash animated'></i> "+lang("Please Add Land Holding Certificate")+"<br/></span>");
        valid = false;
    } else {
        $("#boundary_correction_doc_error_panel #uploadLandCertificate").remove();
    }
    return valid;
}
function boundary_correction_init() {

    wizardPages.buildNavHtml("#tab-buttons",
            [{tab: "#step-app-tab", cont: "#step-app", enable: true},
                {tab: "#step-applicant-tab", cont: "#step-applicant", enable: true},
                {tab: "#step-parcel-tab", cont: "#step-parcel", enable: true},
                {tab: "#step-neighbors-tab", cont: "#step-neighbors", enable: true},
                {tab: "#step-docs-tab", cont: "#step-docs", enable: true},
                {tab: "#step-finish-tab", cont: "#step-finish", enable: true},
            ]);
    wizardPages.beforePageChange = function (show, hide)
    {
        var ret = true;
        if (hide == "#step-app-tab") {
            ret = boundary_correction_validateApplicationPage();
        } else if (hide == "#step-applicant-tab") {
            ret = boundary_correction_validateApplicantPage();
        } else if (hide == "#step-parcel-tab") {
            ret = boundary_correction_validateParcelPage();
        } else if (hide == "#step-neighbors-tab") {
            ret = true;
        } else if (hide == "#step-docs-tab") {
            ret = boundary_correction_validateReqDocument();
        }
        if (show == "#step-finish-tab") {
            boundary_correction_showPreview();
        }

        return ret;
    }
    $("#app_type").val(lang("Boundary Correction Transaction"));

    app_holdingSearch.onHoldingPicked = boundary_correction_addHolding;

    documentPicker.load("#documentTableBody");

    //initialize neighbourds list picker
    boundary_correction_adjacent_parcels = adjacentParcelsPicker("#boundary_correction_neighbors");

    //load existing data
    if (boundary_correction_holdingID != null)
    {
        boundary_correction_addHolding(boundary_correction_holdingID);
    }

    transaction_saveInitial = function ()
    {
        if (boundary_correction_parcel == null)
        {
            bootbox.alert(lang('You must at least select a parcel to save the boundary correction transaction'));
            return;
        }
        boundary_correction_buildTransactionObject();
        transaction_saveTransaction(boundary_correction_combined, true, function (tranUID) {
            bootbox.alert({
                message: lang("You Have Saved a Transaction Data Temporarily")+"!",
                callback: function () {
                    window.location.href = "/dashboard.jsp";
                }
            });
        }, function (err) {
            bootbox.alert(lang("Saving Transaction Data Failed")+".\n" + err);
        });
    };
    //initialize parcel selector
    parcelTransferTable.onChanged = function ()
    {
        var t = parcelTransferTable.getTransfers();
        if (t.length > 0)
        {
            boundary_correction_parcel = t[0].parcelUID;
            boundary_correction_adjacent_parcels.load(boundary_correction_parcel);
        } else
        {
            boundary_correction_parcel = null;
            boundary_correction_adjacent_parcels.load();
        }
    };



    //load required documents
    if (boundary_correction_combined)
    {
        documentPicker.setRequiredDocument(0, boundary_correction_combined.data.landHoldingCertificateDoc);
        if (boundary_correction_combined.data.otherDocument.length > 0) {
           documentPicker.setAddtionalDocument(boundary_correction_combined.data.otherDocument);
       }
    }

}
