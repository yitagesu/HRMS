<table class="table table-striped">
    <tbody id="">

        <%@ include file="/tran/tran_viewer_header.jsp" %>        
        <tr><td class="table_separetor" colspan="2"><label><%=controller.text("Information of Holders")%></label></td></tr>
        <%for(LADM.Right r:controller.holding.getHolders()){
            String holderROLE=controller.roleText(r.party.mreg_familyrole);
            String holderSEX=controller.sexText(r.party.sex);
            Applicant ap=null;
        %>
        <tr id="final_<%=controller.holding.holdingUID%>" class="holder">
            <td class="many">
                <label><%=holderROLE%> <%=controller.text("Info")%></label>
                <ul>
                    <li><label><%=controller.text("Name")%>:</label> <%=r.party.getFullName()%></li>
                    <li><label><%=controller.text("Sex")%>: </label> <%=holderSEX%></li>
                    <li><label><%=controller.text("Age")%>: </label> <span id="ageOfApp"><%=controller.age(r.party.dateOfBirth)%> (<%=EthiopianCalendar.ToEth(r.party.dateOfBirth).toString()+" "+controller.text("EC")%>)</span></li>
                    <li><label><%=controller.text("Share")%>: </label> <%=r.share.num%>/<%=r.share.denum%></li>
                    
                </ul>
            </td>
            <td class="many">
                <label><%=holderROLE%> <%=controller.text("Representative info")%></label>
                <%
                            String message="";
                            
                            if(ap!=null && ap.representative!=null)
                            {
                                String repSEX="";
                                repSEX=controller.sexText(ap.representative.sex);
                            
            
                %>
                <ul>
                    <li> <label><%=controller.text("Name")%>: </label> <%=ap.representative.getFullName()%></li>
                    <li> <label><%=controller.text("Relationship")%>: </label> <%=ap.relationWithParty%></li>
                    <li> <label><%=controller.text("Age")%>: </label> <span id="ageOfRep"><%=controller.age(ap.representative.dateOfBirth)%> (<%=EthiopianCalendar.ToEth(ap.representative.dateOfBirth).toString()+" "+controller.text("EC")%>)</span></li>
                    <li><label><%=controller.text("Sex")%>: </label> <%=repSEX%></li>
                    <%for(LADM.PartyContact a:ap.representative.contacts){
                        if(a.contactdataType==LADM.PartyContact.CONTACT_TYPE_OTHER){%>
                    <li><label><%=controller.text("Phone")%>: </label> <%=a.contactData%></li>
                        <%}if(a.contactdataType==LADM.PartyContact.CONTACT_TYPE_ADDRESS){%>
                    <li><label><%=controller.text("Address")%>: </label> <%=a.contactData%></li> 
                    <%}}%> 
                    <li><label><%=controller.text("Letter of Attorney")%>:</label>
                        <%if(ap.letterOfAttorneyDocument.fileImage ==null){%>
                            <%=controller.letterOfAttroneyText(ap)%>
                        <%}else{%>
                        <a href="<%=controller.letterOfAttroneyLink(ap)%>"><%=controller.letterOfAttroneyText(ap)%></a>
                        <%}%>
                    </li>                    
                </ul>

                <%}else{ message=controller.text("No Representative");%>

                <p><%=message%></p>

                <%}//if-else%>


            </td>
        </tr>
        <%}//for%>
        <tr><td colspan="2" class="table_separetor"><label><%=controller.text("Information of Heirs")%></label></td></tr>
        <%for(PartyItem heir:controller.beneficiaries()){
             String heirROLE=controller.roleText(heir.party.mreg_familyrole);
            String heirSEX=controller.sexText(heir.party.sex);%>
        <tr>
            <td class="many">
                <ul>
                    <li><label><%=controller.text("Name")%>: </label> <%=heir.party.getFullName()%></li>
                    <li><label><%=controller.text("Sex")%>: </label> <%=heirSEX%></li>
                    <li><label><%=controller.text("Age")%>: </label> <span id="ageOfApp"><%=controller.age(heir.party.dateOfBirth)%> (<%=EthiopianCalendar.ToEth(heir.party.dateOfBirth).toString()+" "+controller.text("EC")%>)</span></li>
                    <li><label><%=controller.text("Family Role")%>: </label> <%=heirROLE%></li>
                    <li><label><%=controller.text("ID")%>: </label>  <%if(heir.idDoc.fileImage ==null){%>
                        <%=controller.applicantIDTypeText(heir)%> (<%=controller.applicantIDText(heir)%>)
                        <%}else{%>
                        <a href="<%=controller.applicantIDLink(heir)%>"><%=controller.applicantIDTypeText(heir)%> (<%=controller.applicantIDText(heir)%>)</a>
                        <%}%>
                    </li>
                    
                </ul>
            </td>
            <td></td>
        </tr>   
        <%}%>
        <tr><td colspan="2" class="table_separetor"><label><%=controller.text("Parcels Transfer for Heirs")%></label> </td></tr>
        <%for(LADM.Parcel p:controller.holding.parcels){
            Partition partition=controller.partition(p.parcelUID);
            if(partition!=null)
            {
        %>
        <tr>
            <td class="many" colspan="2">
                <ul>
                    <li><label><%=controller.text("Parcel No")%>: </label> <%=p.seqNo%></li>
                    <li><label><%=controller.text("Area")%>: </label> <%=p.areaGeom%> M<sup>2</sup></li>
                    <li><label><%=controller.text("Share")%>:</label>
                        <ul>
                            <%
                                for(PartyItem heir:controller.beneficiaries())
                                {
                                    PartyParcelShare sh=controller.share(partition,heir.party.partyUID);
                                    String name=heir.party.getFullName();
                                    String split="";
                                    if(controller.isSplit(sh))
                                        split="("+controller.text("Parcel Split")+")";
                                    
                            %>

                            <li><%=name%>:  <%=controller.shareText(sh)%> <%=split%></li>

                            <%if(controller.showCerticateLink(p.parcelUID,heir.party.partyUID)){%>
                            <li><a href="<%=controller.showCertificateLink(p.parcelUID,heir.party.partyUID)%>"><%=controller.text("Show Certificate")%></a>
                                <%}%>
                            </li>
                            <%}//for%>
                        </ul>
                    </li>
                </ul>
            </td>
        </tr>
        <%} //if(partion!=null)
}//for%>
<tr><td colspan="2" class="table_separetor"><label><%=controller.text("Document Provides")%></label></td></tr>
        <tr>
            <td colspan="2">
                <ol>
                    <li><%=controller.adminSourceText(DocumentTypeInfo.DOC_TYPE_PROOF_OF_DIVORCE)%> (<a href="<%=controller.showDocumentLink(controller.data.proofOfDeath)%>"><%=controller.showDocumentText(controller.data.proofOfDeath)%></a>)</li>
                    <li><%=controller.adminSourceText(DocumentTypeInfo.DOC_TYPE_DIVORCE_CLAIM_RESOLUTION)%> (<a href="<%=controller.showDocumentLink(controller.data.claimResolutionDocument)%>"><%=controller.showDocumentText(controller.data.claimResolutionDocument)%></a>)</li>
                        <%if(controller.isWill()){%>
                    <li><%=controller.adminSourceText(DocumentTypeInfo.DOC_TYPE_WILL)%> (<a href="<%=controller.showDocumentLink(controller.data.willDocument)%>"><%=controller.showDocumentText(controller.data.willDocument)%></a>)</li>
                        <%}else{%>
                    <li><%=controller.adminSourceText(DocumentTypeInfo.DOC_TYPE_INHERITANCE_COURT_DECISION)%> (<a href="<%=controller.showDocumentLink(controller.data.courtDocument)%>"><%=controller.showDocumentText(controller.data.courtDocument)%></a>)</li>
                        <%}%>
                    <li><%=controller.adminSourceText(DocumentTypeInfo.DOC_TYPE_LAND_HOLDING_CERTIFICATE)%> (<a href="<%=controller.showDocumentLink(controller.data.landHoldingCertifcateDocument)%>"><%=controller.showDocumentText(controller.data.landHoldingCertifcateDocument)%></a>)</li>
                        <%for(SourceDocument doc:controller.data.otherDocument){%>
                    <li><%=controller.adminSourceText(DocumentTypeInfo.DOC_TYPE_OTHER)%>: <%=doc.description%> (<%=doc.refText%>)</li>
                        <%}%>
                </ol>
            </td>
        </tr>
    </tbody>

</table>
