/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var inheritance_holding_data;
var inheritance_holders;
var inheritance_holdingID;
var inheritance_combined;
var inheritance_beneficiaries;
var inheritance_withWill;
function inheritance_buildTransactionObject()
{
    var inheritanceData =
            {
                holdingUID: inheritance_holdingID,
                holderBeneficiaries: applicantPicker.getApplicants(),
                partitions: parcelSplitTable.getPartitions(),
                beneficiaries: inheritance_beneficiaries.parties,
                proofOfDeath: documentPicker.getRequiredDocument(0),
                claimResolutionDocument: documentPicker.getRequiredDocument(1),
                landHoldingCertifcateDocument: documentPicker.getRequiredDocument(3),
                withWill: inheritance_withWill == "true",
                manipulateManually:$("#man_holding").is(":checked"),
                otherDocument:documentPicker.getAllAddtionalDocument()
            };
    if (inheritance_withWill == "true") {
        inheritanceData.willDocument = documentPicker.getRequiredDocument(2);
    }
    if (inheritance_withWill == "false") {
        inheritanceData.courtDocument = documentPicker.getRequiredDocument(2)
    }
    var tranData;
    if (inheritanceData.withWill == false) {
        tranData = transaction_getTransactionInfo(TRAN_TYPE_INHERITANCE);
    }
    else{
        tranData = transaction_getTransactionInfo(TRAN_TYPE_INHERITANCE_WITHWILL);
    }
    tranData.nrlais_kebeleid = inheritance_holding_data.nrlais_kebeleid;
    tranData.data = inheritanceData;
    inheritance_combined = tranData;
}
function inheritance_SaveToServer() {
    transaction_saveTransaction(inheritance_combined, false, function (ternID) {
        bootbox.alert({
            message: lang("You Have Successfully Registered a Transaction")+"!",
            callback: function () {
                window.location.href = "/dashboard.jsp";
            }
        });
    }, function (err) {
        bootbox.alert(lang("Transaction Registration Failed.")+" \n" + err);
    });
}
function inheritance_showPreview() {
    inheritance_buildTransactionObject();
    $.ajax({type: 'POST',
        url: "/tran/inheritance/full_inheritance_viewer_embeded.jsp?tran_type="+inheritance_combined.transactionType,
        contentType: "application/json",
        data: JSON.stringify(inheritance_combined),
        async: true,
        error: function (errorThrown) {
            bootbox.alert(lang("error"));
        },
        success: function (data) {
            var iframe = $('iframe#printSlip').contents();
            iframe.find('body').html(data);
            $('#printSlip').attr('height', document.getElementById("printSlip").contentWindow.document.body.scrollHeight + "px");
        }

    });
}
function inheritance_configureSplitTable(holdingUID, data, beneficiaries, applicants)
{
    var s = {
        holderFilter: [],
        additionalParties: beneficiaries,
    };
    for (var key in applicants)
    {
        if (applicants[key].inheritancRole == INHERITANCE_ROLE_BENEFICIARY)
            s.holderFilter.push(applicants[key].selfPartyUID);
    }

    parcelSplitTable.load('#holder_parcel', holdingUID, data, s);
}
function inheritance_configureTabs(beneficiaries, applicants)
{
    var nDeceased = 0;
    for (var key in applicants)
    {
        if (applicants[key].inheritancRole == INHERITANCE_ROLE_DECEASED)
            nDeceased++;
    }
    var tabSetup;
    if (applicants.length - nDeceased + beneficiaries.length > 1)
    {
        tabSetup = [
            {tab: "#step-app-tab", cont: "#step-app", enable: true},
            {tab: "#step-this-holding-tab", cont: "#step-this-holding", enable: true},
            {tab: "#step-beneficiary-tab", cont: "#step-beneficiary", enable: true},
            {tab: "#step-parcel-tab", cont: "#step-parcel", enable: true},
            {tab: "#step-doc-tab", cont: "#step-doc", enable: true},
            {tab: "#step-finish-tab", cont: "#step-finish", enable: true},
        ];
    } else
    {
        tabSetup = [
            {tab: "#step-app-tab", cont: "#step-app", enable: true},
            {tab: "#step-this-holding-tab", cont: "#step-this-holding", enable: true},
            {tab: "#step-beneficiary-tab", cont: "#step-beneficiary", enable: true},
            {tab: "#step-doc-tab", cont: "#step-doc", enable: true},
            {tab: "#step-finish-tab", cont: "#step-finish", enable: true},
        ];
    }
    var all = ["#step-app", "#step-this-holding", "#step-beneficiary", "#step-parcel", "#step-doc", "#step-finish"];
    for (var i in all)
    {
        var hide = true;
        for (var j in tabSetup)
        {
            if (tabSetup[j].cont == all[i])
            {
                hide = false;
                break;
            }
        }
        if (hide)
            $(all[i]).hide();
    }
    wizardPages.buildNavHtml("#tab-buttons", tabSetup);
}
function inheritance_addInheritanceHolding(holdingUID)
{

    //zoom map
    transaction_map.zoomHolding('nrlais_inventory', holdingUID);
    //show holding information
    $.ajax('/holding/min_holding_search_result.jsp?holding_uid=' + holdingUID, {
        'data': {'method': 'GET'},
        'success': function (data, textStatus, jqXHR) {
            $('#holder_content').html(data);
            $('#pick_person').modal('hide');
        }
    });
    //load holding data from database
    $.ajax('/api/get_holding?schema=nrlais_inventory&holding_uid=' + holdingUID,
            {
                method: 'GET',
                success: function (data)
                {
                    //alert(JSON.stringify(data));
                    inheritance_holding_data = data.res;
                    var party_done = [];
                    inheritance_holders = [];
                    inheritance_holding_data.parcels.forEach(function (parcel)
                    {
                        parcel.rights.forEach(function (right)
                        {
                            if (right.rightType != RIGHT_TYPE_PUR)
                                return;
                            if (party_done[right.partyUID])
                                return;
                            party_done[right.partyUID] = true;
                            inheritance_holders.push(right);
                        });
                    });
                },
                dataType: 'json'
            });
    //setup applicant table
    var setting = {
        pickID: true,
        pickRepresentative: true,
        pickPOM: true,
        pickShare: false,
        selectable: false,
        inheritance: true,
        shareLabel:"New Share"
        
    };
    if (inheritance_combined == null)
    {
        applicantPicker.load("#holder_with_representative", holdingUID, null, setting);
    } else
    {
        applicantPicker.load("#holder_with_representative", holdingUID, inheritance_combined.data.holderBeneficiaries, setting);
    }
    applicantPicker.onChanged = function ()
    {
        inheritance_configureTabs(inheritance_beneficiaries.parties, applicantPicker.getApplicants());
    };
    //setup parcel sharing table
    if (inheritance_combined == null)
    {
        inheritance_configureSplitTable(holdingUID, null, [], []);
        inheritance_configureTabs([], []);
    } else
    {
        inheritance_configureSplitTable(holdingUID, inheritance_combined.data.partitions, inheritance_combined.data.beneficiaries, inheritance_combined.data.holderBeneficiaries);
        inheritance_configureTabs(inheritance_combined.data.beneficiaries, inheritance_combined.data.holderBeneficiaries);
    }


    //load required documents
    if (inheritance_combined)
    {
        documentPicker.setRequiredDocument(0, inheritance_combined.data.proofOfDeath);
        documentPicker.setRequiredDocument(1, inheritance_combined.data.claimResolutionDocument);
        documentPicker.setRequiredDocument(3, inheritance_combined.data.landHoldingCertifcateDocument);
        if (inheritance_withWill == "true") {
            documentPicker.setRequiredDocument(2, inheritance_combined.data.willDocument);
        }
        if (inheritance_withWill == "false") {
            documentPicker.setRequiredDocument(2, inheritance_combined.data.courtDocument);
        }
    }
    inheritance_holdingID = holdingUID;
}

function inheritance_validateApplicationPage()
{
    return transaction_validateApplication() && validatePageOne();
}
function inheritance_validateApplicantsPage() {
    return applicantPicker.validate(inheritance_holders);
}

function inheritance_validReqDocument() {
    var valid = true;
    var validator = validatorPanel('.error-req-doc-panel');

    if (!documentPicker.getRequiredDocument(0))
    {
        validator.removeError("proofOfDeath");
        validator.addError("proofOfDeath", lang("Please add Proof of Death"));
        valid = false;
    } else {
        validator.removeError("proofOfDeath");
    }
    if (!documentPicker.getRequiredDocument(1)) {
        validator.removeError("claimResolution");
        validator.addError("claimResolution", lang("Please add Cliam Resolution Document"));
        valid = false;
    } else {
        validator.removeError("claimResolution");
    }
    if (inheritance_withWill == "true") {
        if (!documentPicker.getRequiredDocument(2)) {
            validator.removeError("willDocument");
            validator.addError("willDocument", lang("Please add Willing Document"));
            valid = false;
        } else {
            validator.removeError("willDocument");
        }
    } else if (inheritance_withWill == "false") {
        if (!documentPicker.getRequiredDocument(2)) {
            validator.removeError("courtDecision");
            validator.addError("courtDecision", lang("Please add Court Decision Document"));
            valid = false;
        } else {
            validator.removeError("courtDecision");
        }
    }
    if (!documentPicker.getRequiredDocument(3)) {
        validator.removeError("landHoldingCertificate");
        validator.addError("landHoldingCertificate", lang("Please add Land Holding Certificate"));
        valid = false;
    } else {
        validator.removeError("landHoldingCertificate");
    }
    return valid;
}

function inheritance_validateBenificiaryPage() {
    var valid = true;
    var validator = validatorPanel(".error-benificiary-panel");
    if (!$.trim($('#inheritance_beneficiaries tbody').html()).length) {
        validator.removeError("benificiaryPanel");
        validator.addError("benificiaryPanel", lang("Please Add At least One Party Information"));
        valid = false;
    } else {
        validator.removeError("benificiaryPanel");
    }
    return valid;
}


function inheritance_init() {
    if (window.location.href.search("tran_type=1") > 0)
    {
        inheritance_withWill = "true";
    }
    if (window.location.href.search("tran_type=2") > 0) {
        inheritance_withWill = "false";
    }
    inheritance_configureTabs([], []);
    wizardPages.beforePageChange = function (show, hide)
    {
        var ret = true;
        if (hide == "#step-app-tab")
        {
            ret = inheritance_validateApplicationPage();
            if (ret)
            {
                inheritance_configureTabs(inheritance_beneficiaries.parties, applicantPicker.getApplicants());
            }
        } else if (hide == "#step-this-holding-tab")
        {
            ret = inheritance_validateApplicantsPage();
            if (ret)
            {
                var validDeceased = true;
                var validator = validatorPanel('.id-error-panel');
                var deceased = $('#holder_with_representative  #ap_inheritance_deceased:checked').length > 0;
                if (!deceased) {
                    validator.removeError("selectDeceased");
                    validator.addError("selectDeceased", lang("Please Select at least one deceased holder"));
                    validDeceased = false;
                } else {
                    validator.removeError("selectDeceased");
                }
                return validDeceased;
                inheritance_configureSplitTable(inheritance_holdingID,
                        parcelSplitTable.getPartitions(),
                        inheritance_beneficiaries.parties,
                        applicantPicker.getApplicants());
                inheritance_configureTabs(inheritance_beneficiaries.parties, applicantPicker.getApplicants());
            }
        } else if (hide == "#step-beneficiary-tab")
        {
            ret =inheritance_validateBenificiaryPage;// inheritance_validateBenificiaryPage();
            if (ret)
            {

                inheritance_configureSplitTable(inheritance_holdingID,
                        parcelSplitTable.getPartitions(),
                        inheritance_beneficiaries.parties,
                        applicantPicker.getApplicants());
                inheritance_configureTabs(inheritance_beneficiaries.parties, applicantPicker.getApplicants());
            }
        } else if (hide == "#step-parcel-tab")
        {
            ret = true;
        } else if (hide == "#step-doc-tab")
        {
            ret = inheritance_validReqDocument();
        }
        if (show == "#step-finish-tab")
        {
            inheritance_showPreview();
        }
        return ret;
    }
    if (inheritance_withWill == "true") {
        $("#app_type").val(lang("Inheritance with Will Transaction"));
    }
    if (inheritance_withWill == "false") {
        $("#app_type").val(lang("Inheritance without Will Transaction"));
    }

    app_holdingSearch.onHoldingPicked = inheritance_addInheritanceHolding;
    //setup required document picker
    documentPicker.load("#documentTableBody");
    //load existing data
    if (inheritance_holdingID != null)
    {
        inheritance_addInheritanceHolding(inheritance_holdingID);
    }

//override save initial transaction
    transaction_saveInitial = function ()
    {
        if (!inheritance_holding_data)
        {
            bootbox.alert(lang('You must at least select holding to save divorce transaction'));
            return;
        }
        inheritance_buildTransactionObject();
        transaction_saveTransaction(inheritance_combined, true, function (tranUID) {
            bootbox.alert({
                message: lang("You Have Saved a Transaction Data Temporarily")+"!",
                callback: function () {
                    window.location.href = "/dashboard.jsp";
                }
            });
        }, function (err) {
            bootbox.alert(lang("Saving Transaction Data Failed.")+"\n" + err);
        });
    }

//initialize beneficiaries party list picker
    inheritance_beneficiaries = getPartyListPicker({
        containerEl: "inheritance_beneficiaries",
        addButtonEl: "inheritance_beneficiary_add",
        share: true,
        idDoc: true,
        edit: true,
        delete: true,
        guardian: true,
        pom: true
    });
    inheritance_beneficiaries.onChanged = function ()
    {
        inheritance_configureTabs(inheritance_beneficiaries.parties, applicantPicker.getApplicants());
    };
    if (inheritance_combined)
    {
        inheritance_beneficiaries.setParyItems(inheritance_combined.data.beneficiaries);
    }
    if (inheritance_combined) {

        documentPicker.setRequiredDocument(0, inheritance_combined.data.proofOfDeath);
        documentPicker.setRequiredDocument(1, inheritance_combined.data.claimResolutionDocument);
        documentPicker.setRequiredDocument(3, inheritance_combined.data.landHoldingCertifcateDocument);
        if (inheritance_withWill == "true") {
            documentPicker.setRequiredDocument(2, inheritance_combined.data.willDocument);
        }
        if (inheritance_withWill == "false") {
            documentPicker.setRequiredDocument(2, inheritance_combined.data.courtDocument);
        }
    }

}






