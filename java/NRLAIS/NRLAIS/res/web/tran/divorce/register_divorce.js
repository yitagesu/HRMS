/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var divorce_holding_data;
var divorce_holders;
var divorce_holdingID;
var divorce_combined;


function divorce_buildTransactionObject()
{
    var divorceData =
            {
                holdingUID: divorce_holdingID,
                applicants: applicantPicker.getApplicants(),
                partitions: parcelSplitTable.getPartitions(divorce_holding_data),
                proofOfDivorceDocument: documentPicker.getRequiredDocument(0),
                claimResolutionDocument: documentPicker.getRequiredDocument(1),
                landHoldingCertifcateDocument: documentPicker.getRequiredDocument(2),
                otherDocument: documentPicker.getAllAddtionalDocument()
            };
    var tranData = transaction_getTransactionInfo(TRAN_TYPE_DIVORCE);
    tranData.nrlais_kebeleid = divorce_holding_data.nrlais_kebeleid;
    tranData.data = divorceData;
    divorce_combined = tranData;
    //alert(JSON.stringify(divorce_combined));

}
function divorce_SaveToServer() {
    transaction_saveTransaction(divorce_combined, false, function (ternID) {
        bootbox.alert({
            message: lang("You Have Successfully Registered a Transaction")+"!",
            callback: function () {
                window.location.href = "/dashboard.jsp";
            }
        });
    }, function (err) {
        bootbox.alert(lang("Transaction Registration Failed")+".\n" + err);
    });
}
function divorce_showPreview() {
    divorce_buildTransactionObject();
    $.ajax({type: 'POST',
        url: "/tran/divorce/full_divorce_viewer_embeded.jsp",
        contentType: "application/json",
        data: JSON.stringify(divorce_combined),
        async: true,
        error: function (errorThrown) {
            bootbox.alert(lang("error"));
        },
        success: function (data) {
            var iframe = $('iframe#printSlip').contents();
            iframe.find('body').html(data);
            $('#printSlip').attr('height', document.getElementById("printSlip").contentWindow.document.body.scrollHeight + "px");
        }

    });
}
function divorce_addDivorceHolding(holdingUID)
{

    //zoom map
    transaction_map.zoomHolding('nrlais_inventory', holdingUID);

    //show holding information
    $.ajax('/holding/min_holding_search_result.jsp?holding_uid=' + holdingUID, {
        'data': {'method': 'GET'},
        'success': function (data, textStatus, jqXHR) {
            $('#holder_content').html(data);
            $('#pick_person').modal('hide');
        }
    });


    //load holding data from database
    $.ajax('/api/get_holding?schema=nrlais_inventory&holding_uid=' + holdingUID,
            {
                method: 'GET',
                success: function (data)
                {
                    //alert(JSON.stringify(data));
                    divorce_holding_data = data.res;
                    var party_done = [];
                    divorce_holders = [];
                    divorce_holding_data.parcels.forEach(function (parcel)
                    {
                        parcel.rights.forEach(function (right)
                        {
                            if (right.rightType != RIGHT_TYPE_PUR)
                                return;
                            if (party_done[right.partyUID])
                                return;
                            party_done[right.partyUID] = true;
                            divorce_holders.push(right);
                        });
                    });
                },
                dataType: 'json'
            });

    //setup applicant table

    if (divorce_combined == null)
        applicantPicker.load("#holder_with_representative", holdingUID, null, {});
    else
    {
        applicantPicker.load("#holder_with_representative", holdingUID, divorce_combined.data.applicants, {});
    }

    //setup parcel sharing table
    if (divorce_combined == null)
        parcelSplitTable.load('#holder_parcel', holdingUID);
    else
    {
        parcelSplitTable.load('#holder_parcel', holdingUID, divorce_combined.data.partitions);
    }

    //load required documents
    if (divorce_combined)
    {
        documentPicker.setRequiredDocument(0, divorce_combined.data.proofOfDivorceDocument);
        documentPicker.setRequiredDocument(1, divorce_combined.data.claimResolutionDocument);
        documentPicker.setRequiredDocument(2, divorce_combined.data.landHoldingCertifcateDocument);
        if (divorce_combined.data.otherDocument.length > 0) {
            documentPicker.setAddtionalDocument(divorce_combined.data.otherDocument);
        }
    }
    divorce_holdingID = holdingUID;
}

function divorce_validateApplicationPage()
{
    return transaction_validateApplication() && validatePageOne();
}
function divorce_validateApplicantsPage() {
    return applicantPicker.validate(divorce_holders);
}

function divorce_validReqDocument() {
    var valid = true;
    $("#uploadDivorceProof").remove();
    $("#uploadCleamResolution").remove();
    $("#uploadLandCertificate").remove();
    if (!documentPicker.getRequiredDocument(0))
    {
        $(".error-req-doc-panel").append("<span id='uploadDivorceProof'><i class='fa fa-warning faa-flash animated'></i>"+lang("Please Add Proof of Divorce")+"<br/></span>");
        valid = false;
    } else {
        $("#uploadDivorceProof").remove();
    }
    if (!documentPicker.getRequiredDocument(1)) {
        $(".error-req-doc-panel").append("<span id='uploadCleamResolution'><i class='fa fa-warning faa-flash animated'></i>"+lang("Please Add Elders committee or Woreda court statement on claim resolution")+"<br/></span>");
        valid = false;
    } else {
        $("#uploadCleamResolution").remove();
    }
    if (!documentPicker.getRequiredDocument(2)) {
        $(".error-req-doc-panel").append("<span id='uploadLandCertificate'><i class='fa fa-warning faa-flash animated'></i>"+lang("Please Add Land Holding Certificate")+"<br/></span>");
        valid = false;
    } else {
        $("#uploadLandCertificate").remove();
    }
    return valid;
}



function divorce_initDivorce() {
    //setup wizard
    wizardPages.load([
        {tab: "#step-1-tab", cont: "#step-1", enable: true},
        {tab: "#step-2-tab", cont: "#step-2", enable: true},
        {tab: "#step-3-tab", cont: "#step-3", enable: true},
        {tab: "#step-4-tab", cont: "#step-4", enable: true},
        {tab: "#step-5-tab", cont: "#step-5", enable: true},
    ]);
    wizardPages.beforePageChange = function (show, hide)
    {
        var ret = true;
        if (hide == "#step-1-tab")
        {
            ret = divorce_validateApplicationPage();
        } else if (hide == "#step-2-tab")
        {
            ret = divorce_validateApplicantsPage();
        } else if (hide == "#step-3-tab")
        {
            ret = true;//NOTE: no validation for parcel page?
        } else if (hide == "#step-4-tab")
        {
            ret = divorce_validReqDocument();
        }
        if (show == "#step-5-tab")
        {
            divorce_showPreview();
        }
        return ret;
    }
    $("#app_type").val(lang("Divorce Transcation"));
    app_holdingSearch.onHoldingPicked = divorce_addDivorceHolding;

    //setup required document picker
    documentPicker.load("#documentTableBody");

    //load existing data
    if (divorce_holdingID != null)
    {
        divorce_addDivorceHolding(divorce_holdingID);
    }

    //override save initial transaction
    transaction_saveInitial = function ()
    {
        if (!divorce_holding_data)
        {
            bootbox.alert(lang('You must at least select holding to save divorce transaction'));
            return;
        }
        divorce_buildTransactionObject();
        transaction_saveTransaction(divorce_combined, true, function (tranUID) {

            bootbox.alert({
                message: lang("You Have Saved a Transaction Data Temporarily")+"!",
                callback: function () {
                    window.location.href = "/dashboard.jsp";
                }
            });
        }, function (err) {
            bootbox.alert(lang("Saving Transaction Data Failed")+".\n" + err);
        });
    }
}






