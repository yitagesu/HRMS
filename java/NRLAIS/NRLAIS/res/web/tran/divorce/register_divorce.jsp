<%@page import="com.intaps.nrlais.model.*" %>
<%@page import="com.intaps.nrlais.repo.*" %>
<%@page import="com.intaps.nrlais.controller.*" %>
<%@page import="com.intaps.nrlais.controller.tran.*" %>
<script src="assets/JS/wizard_pages.js"></script>
<%
    DivorceViewController controller=new DivorceViewController(request,response,true);
    /*if there is a holding already saved we will declare the holding variable here
    this will override the declartion in register_divorce.jsp and the divorce_initDivorce function will load the holding when 
    it sees that the hodling is not empty
    */
    if(controller.holding.holdingUID!=null){ 
%>
    <script>
        var divorce_holdingID='<%=controller.holding.holdingUID%>';
        var divorce_combined=<%=controller.json(controller.tran)%>;
    </script>
<%}%>
<div class="stepwizard">
    <div class="stepwizard-row setup-panel">
        <div class="stepwizard-step">,
            <a id='step-1-tab' href="javascript:wizardPages.openPage('#step-1-tab')" type="button" class="btn btn-default btn-circle">1</a>
            
        </div>
        <div class="stepwizard-step">
            <a id='step-2-tab' href="javascript:wizardPages.openPage('#step-2-tab')" type="button" class="btn btn-default btn-circle">2</a>
            
        </div>
        <div class="stepwizard-step">
            <a id='step-3-tab' href="javascript:wizardPages.openPage('#step-3-tab')" type="button" class="btn btn-default btn-circle">3</a>
            
        </div>
        <div class="stepwizard-step">
            <a id='step-4-tab' href="javascript:wizardPages.openPage('#step-4-tab')" type="button" class="btn btn-default btn-circle">4</a>
           
        </div>
        <div class="stepwizard-step">
            <a id='step-5-tab' href="javascript:wizardPages.openPage('#step-5-tab')" type="button" class="btn btn-default btn-circle">5</a>
            
        </div>
    </div>
</div>
<form role="form" action="" method="post">
    <div class="row setup-content" id="step-1">
        <div class="col-md-12">
            <h4><small><%=controller.text("Set Application")%></small></h4>
            <hr>
            <div class="form-group col-md-6">
                <label class="control-label"><%=controller.text("Application Type")%>:</label>
                <input  type="text"  class="form-control" disabled="disabled" id="app_type" value="<%=controller.tranTypeText()%>"/>
            </div>
            <div class="form-group col-md-6">
                <label class=""><%=controller.text("Application Date")%>:</label>
                <input max="5" type="text" class="form-control" placeholder=<%=controller.text("pick application Date")%>  id="app_date" dateVal="<%=controller.grigDate(controller.tran.time)%>"/>
            </div>
            <div class="form-group col-md-12">
                <label class=""><%=controller.text("Application Description")%>:</label>
                <textarea rows="4"  class="form-control" placeholder="<%=controller.text("Write application description")%>" id="app_description"><%=controller.escapeHtml(controller.tran.notes)%></textarea>
            </div>
            <div class="form-group col-md-12" id="holder_content">

            </div>
            <div class="col-md-12">
                <div class="pull-left">
                    <div class="error-panel">

                    </div>
                </div>
                <div class="pull-right">
                    <div class="btn-group">
                        <button class="btn btn-nrlais" type="button" data-toggle='modal' data-target='#pick_person' data-backdrop='static' ><i class="fa fa-hand-o-up"></i><%=controller.text("Load Parcel")%> </button>
                        <button class="btn btn-nrlais" type="button" onclick="wizardPages.gotoNext()" id="nextToSecond"><%=controller.text("Next")%> <i class="fa fa-angle-double-right"></i></button>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="row setup-content" id="step-2">
        <div class="col-md-12">
            <h4><small><%=controller.text("Set Applicant Information")%></small></h4>
            <hr>
            <div id="holder_with_representative">

            </div>
            <div class="pull-left">
                <div class="id-error-panel">

                </div>
            </div>
            <div class="pull-right">
                <div class="btn-group">
                    <button class="btn btn-nrlais" type="button" onclick="wizardPages.gotoNext()" id="nextToThird"><%=controller.text("Next")%> <i class="fa fa-angle-double-right"></i></button>
                </div>
            </div>
        </div>
    </div>
    <div class="row setup-content" id="step-3">
        <div class="col-md-12">
            <h4><small><%=controller.text("Assign Parcels")%></small></h4>
            <hr>
            <div id="holder_parcel">
            </div>
            <div class="align-right">
                <div class="btn-group">
                    <button class="btn btn-nrlais" type="button" onclick="wizardPages.gotoNext()"><%=controller.text("Next")%> <i class="fa fa-angle-double-right"></i></button>
                </div>
            </div>
        </div>
    </div>
    <div class="row setup-content" id="step-4">
        <div class="col-md-12">
            <h4><small><%=controller.text("Add Required Documents")%></small></h4>
            <table class="table table-striped jambo_table">
                <thead>
                    <tr>
                        <td><%=controller.text("Document Type")%></td>
                        <td><%=controller.text("Document Reference")%></td>
                        <td><%=controller.text("Document Description")%></td>
                        <td><%=controller.text("Document File")%></td>
                        <td></td>
                    </tr>
                </thead>
                <tbody id="documentTableBody">                    
                    <tr doc_type="<%=DocumentTypeInfo.DOC_TYPE_PROOF_OF_DIVORCE%>">
                        <td><%=controller.text("Proof of divorce")%>:</td>
                        <td id="doc_ref"></td>
                        <td id="doc_desc"></td>
                        <td id="doc_File"></td>
                        <td><a class="btn btn-nrlais"><i class="fa fa-plus"></i></a> </td>
                    </tr>
                    <tr doc_type="<%=DocumentTypeInfo.DOC_TYPE_DIVORCE_CLAIM_RESOLUTION%>">
                        <td><%=controller.text("Elders committee or Woreda court statement on claim resolution")%>:</td>
                        <td id="doc_ref"></td>
                        <td id="doc_desc"></td>
                        <td id="doc_File"></td>
                        <td><a class="btn btn-nrlais"><i class="fa fa-plus"></i></a> </td>
                    </tr>
                    <tr doc_type="<%=DocumentTypeInfo.DOC_TYPE_LAND_HOLDING_CERTIFICATE%>">
                        <td><%=controller.text("Land holding certificate")%>:</td>
                        <td id="doc_ref"></td>
                        <td id="doc_desc"></td>
                        <td id="doc_File"></td>
                        <td><a  class="btn btn-nrlais"><i class="fa fa-plus"></i></a> </td>
                    </tr>
                </tbody>
            </table>
            <div class="align-right pull-right">
                <div class="btn-group">
                    <button class="btn btn-nrlais" type="button" data-toggle='modal' data-target='#add_document' data-backdrop='static'><i class="fa fa-plus"></i> <%=controller.text("Add")%></button>
                    <button class="btn btn-nrlais nextBtn" type="button" id="doc_nextbtn" onclick="wizardPages.gotoNext()"><%=controller.text("Next")%> <i class="fa fa-angle-double-right"></i></button>
                </div>
            </div>
            <div class="pull-left">
                <div class="btn-group">
                    <div class="error-req-doc-panel">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row setup-content" id="step-5">
        <div class="col-md-12">
            <h4><small><%=controller.text("Review and Submit Application")%></small></h4>
            <hr>
            <div class="print_slip" id="print_slip">
                <iframe id="printSlip"  frameborder="0" width="100%" height="150px" scrolling="no"></iframe>
            </div>
            <div class="align-right">
                <div class="btn-group">
                    <button class="btn btn-nrlais" type="button" id="print" onclick="pritSlip();"><i class="fa fa-print"></i><%=controller.text("Print")%></button>
                    <button class="btn btn-nrlais nextBtn" type="button" id="doc_nextbtn" onclick="divorce_SaveToServer()"><%=controller.text("Register Transaction")%><i class="fa fa-exclamation"></i></button>
                </div>
            </div>
        </div>
    </div>
</form>

