/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var replacement_holding_data;
var replacement_holders;
var applicant_holdingID;
var certificate_replacement_combined;
function certificateReplacement_buildTransactionObject()
{
    var replacementData =
            {
                holdingUID: applicant_holdingID,
                applicant: applicantPicker.getApplicants(),
                parcelUID: parcelTransferTable.getTransfers(replacement_holding_data),
                claimResolution: documentPicker.getRequiredDocument(0),
                landHoldingCertificateDoc:documentPicker.getRequiredDocument(1),
                otherDocument: documentPicker.getAllAddtionalDocument()
            };
    var tranData = transaction_getTransactionInfo(TRAN_TYPE_REPLACE_CERTIFICATE);
    tranData.nrlais_kebeleid = replacement_holding_data.nrlais_kebeleid;
    tranData.data = replacementData;
    certificate_replacement_combined = tranData;
    //alert(JSON.stringify(divorce_combined));

}

function certificate_replacement_SaveToServer() {
    transaction_saveTransaction(certificate_replacement_combined, false, function (ternID) {
        bootbox.alert({
            message: lang("You Have Successfully Registered a Transaction!"),
            callback: function () {
                window.location.href = "/dashboard.jsp";
            }
        });
    }, function (err) {
        bootbox.alert(lang("Transaction Registration Failed")+". \n" + err);
    });
}


function addHolders(holdingUID) {

    //zoom map
    transaction_map.zoomHolding('nrlais_inventory', holdingUID);

    //show holding information
    $.ajax('/holding/min_holding_search_result.jsp?holding_uid=' + holdingUID, {
        'data': {'method': 'GET'},
        'success': function (data, textStatus, jqXHR) {
            $('#holder_content').html(data);
            $('#pick_person').modal('hide');
        }
    });

    $.ajax('/api/get_holding?schema=nrlais_inventory&holding_uid=' + holdingUID,
            {
                method: 'GET',
                success: function (data)
                {
                    //alert(JSON.stringify(data));
                    replacement_holding_data = data.res;
                    var party_done = [];
                    replacement_holders = [];
                    replacement_holding_data.parcels.forEach(function (parcel)
                    {
                        parcel.rights.forEach(function (right)
                        {
                            if (right.rightType != RIGHT_TYPE_PUR)
                                return;
                            if (party_done[right.partyUID])
                                return;
                            party_done[right.partyUID] = true;
                            replacement_holders.push(right);
                        });
                    });
                },
                dataType: 'json'
            });
    if (certificate_replacement_combined == null)
        applicantPicker.load("#holder_with_representative", holdingUID, null, {pickPOM: true});
    else
    {
        applicantPicker.load("#holder_with_representative", holdingUID, certificate_replacement_combined.data.applicant, {pickPOM: true});
    }


    if (certificate_replacement_combined)
    {
        documentPicker.setRequiredDocument(0, certificate_replacement_combined.data.claimResolution);
        if (certificate_replacement_combined.data.otherDocument.length > 0) {
            documentPicker.setAddtionalDocument(certificate_replacement_combined.data.otherDocument);
        }
    }
    applicant_holdingID = holdingUID;

    var parcelSetting = {
        split: false,
        transferArea: false,
        transferShare: false,
        singleParcel: true,
        transferLabel: lang('Select')
    };
    if (certificate_replacement_combined == null)
        parcelTransferTable.load('#holder_parcel', holdingUID, null, parcelSetting);

    else
        parcelTransferTable.load('#holder_parcel', holdingUID, certificate_replacement_combined.data.parcelUID, parcelSetting);
}
function certificate_replacement_showPreview() {
    console.log("called");
    certificateReplacement_buildTransactionObject();
    $.ajax({type: 'POST',
        url: "/tran/certificate_replacement/full_certificate_replacement_viewer_embeded.jsp",
        contentType: "application/json",
        data: JSON.stringify(certificate_replacement_combined),
        async: true,
        error: function (errorThrown) {
            bootbox.alert(lang("error"));
        },
        success: function (data) {
            console.log("hooray");
            $("#print_slip").html(data);
        }

    });
}

function certificate_validReqDocument() {
    var valid = true;
    $("#uploadCleamResolution").remove();
    $("#uploadLandCertificate").remove();
    if (!documentPicker.getRequiredDocument(0)) {
        $(".error-req-doc-panel").append("<span id='uploadCleamResolution'><i class='fa fa-warning faa-flash animated'></i>"+lang("Please add Claim Resolution Document")+"<br/></span>");
        valid = false;
    } else {
        $("#uploadCleamResolution").remove();
    }
    return valid;
}

function certificate_rep_validateApplicationPage()
{
    return transaction_validateApplication() && validatePageOne();
}

function certificate_replacement_validateApplicantsPage() {
    return applicantPicker.validate(replacement_holders);
}

function certificate_replacement_validateParcelPage(){
    var valid = true;
    var validator = validatorPanel('.parcel-error-panel');
    var parcel = $('#holder_parcel input[field=select]:checked').length > 0;
    if (!parcel) {
        validator.removeError("selectParcel");
        validator.addError("selectParcel", lang("Please select at least one parcel"));
        valid = false;
    } else {
        validator.removeError("selectParcel");
    }
    return valid;
}
function certificateReplacement_init() {

    wizardPages.load([
        {tab: "#step-1-tab", cont: "#step-1", enable: true},
        {tab: "#step-2-tab", cont: "#step-2", enable: true},
        {tab: "#step-3-tab", cont: "#step-3", enable: true},
        {tab: "#step-4-tab", cont: "#step-4", enable: true},
        {tab: "#step-5-tab", cont: "#step-5", enable: true}
    ]);

    wizardPages.beforePageChange = function (show, hide)
    {
        var ret = true;
        if (hide === "#step-1-tab")
        {
           ret = certificate_rep_validateApplicationPage();
        } else if (hide === "#step-2-tab")
        {
            ret = certificate_replacement_validateApplicantsPage();
        } else if (hide === "#step-3-tab")
        {
            ret = certificate_replacement_validateParcelPage();
        } else if (hide === "#step-4-tab")
        {
            //alert("step 3");
            ret = certificate_validReqDocument();

        }

        if (show === "#step-5-tab")
        {
            certificate_replacement_showPreview();
            console.log("final step");

        }
        return ret;
    }


    $("#app_type").val(lang("Certificate Replacement Transcation"));
    app_holdingSearch.onHoldingPicked = addHolders;

    //setup required document picker
    documentPicker.load("#documentTableBody");

    //load existing data
    if (applicant_holdingID != null)
    {
        addHolders(applicant_holdingID);
    }
    transaction_saveInitial = function () {
        if (!replacement_holding_data)
        {
            bootbox.alert(lang('You must at least select holding to save certificate Replacement transaction'));
            return;
        }
        certificateReplacement_buildTransactionObject();
        transaction_saveTransaction(certificate_replacement_combined, true, function (tranUID) {
            bootbox.alert({
                message: "You Have Saved a Transaction Data Temporarily!",
                callback: function () {
                    window.location.href = "/dashboard.jsp";
                }
            });
        }, function (err) {
            bootbox.alert("Saving Transaction Data Failed.\n" + err);
        });
    }

if(certificate_replacement_combined){
    documentPicker.setRequiredDocument(0, certificate_replacement_combined.data.claimResolution);
    documentPicker.setRequiredDocument(1, certificate_replacement_combined.data.landHoldingCertificateDoc);
    if (certificate_replacement_combined.data.otherDocument.length > 0) {
           documentPicker.setAddtionalDocument(certificate_replacement_combined.data.otherDocument);
       }
}

}


