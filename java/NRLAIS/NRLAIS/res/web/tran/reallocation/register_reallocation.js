/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var reallocation_holding_data;
var reallocation_holders;
var reallocation_holdingID;
var reallocation_combined;
var reallocation_beneficiaries;
var reallocation_type = null;
var reallocation_existing_holdingID;
var reallocation_allocated_party=null;
function reallocation_buildTransactionObject()
{
    var rentData = {};

    rentData.tranferType = reallocation_type;
    rentData.holdingUID = reallocation_holdingID;
    rentData.otherHoldingUID = reallocation_existing_holdingID;
    rentData.applicants = applicantPicker.getApplicants();
    rentData.transfers = parcelTransferTable.getTransfers(reallocation_holding_data);
    if (rentData.tranferType == TRANSFER_TYPE_OTHER_HOLDING)
        rentData.beneficiaries = reallocation_allocated_party;
    else
        rentData.beneficiaries = reallocation_beneficiaries.parties;
    rentData.adminDecisionDoc = documentPicker.getRequiredDocument(0);
    rentData.claimResolution = documentPicker.getRequiredDocument(1);
    rentData.landHoldingCertificateDoc = documentPicker.getRequiredDocument(2);
    rentData.otherDocument = documentPicker.getAllAddtionalDocument();
    var tranData = transaction_getTransactionInfo(TRAN_TYPE_REALLOCATION);
    tranData.nrlais_kebeleid = reallocation_holding_data.nrlais_kebeleid;
    tranData.data = rentData;
    reallocation_combined = tranData;
}
function reallocation_configureApplicantPicker()
{
    if (!reallocation_type || reallocation_type == -1)
        return;
    var setting = {
        pickID: false,
        pickRepresentative: false,
        pickPOM: false,
        pickShare: reallocation_type == TRANSFER_TYPE_THIS_HOLDING,
        selectable: reallocation_type == TRANSFER_TYPE_THIS_HOLDING,
        shareLabel: lang("Transfer Proportion")

    };
    if (reallocation_combined == null)
        applicantPicker.load("#reallocation_this_holding", reallocation_holdingID, null, setting);
    else
    {
        applicantPicker.load("#reallocation_this_holding", reallocation_holdingID, reallocation_combined.data.applicants, setting);
    }

}
function reallocation_configureBenificiaryApplicantPicker(){
    if (reallocation_type != TRANSFER_TYPE_OTHER_HOLDING || reallocation_existing_holdingID == null)
    {
        $("#reallocation_exiting_holding_applicant").html("");
        return;
    }
    var setting = {
        pickPOM: true,
        pickShare: false,
        selectable: false,
    };

    applicantPicker.load("#reallocation_exiting_holding_applicant", reallocation_existing_holdingID, applicantPicker.partyItemsToApplicants(reallocation_allocated_party), setting);
}
function reallocation_setExistingHolding(holdingUID)
{
    reallocation_existing_holdingID = holdingUID;
    reallocation_configureBenificiaryApplicantPicker();
    transaction_map.zoomHolding('nrlais_inventory', holdingUID);
    //show holding information
    $.ajax('/holding/min_holding_search_result.jsp?holding_uid=' + holdingUID, {
        'data': {'method': 'GET'},
        'success': function (data, textStatus, jqXHR) {
            $('#reallocation_exiting_holding').html(data);
            $('#pick_person').modal('hide');
        }
    });
}
function reallocation_addReallocationHolding(holdingUID)
{
    reallocation_holdingID = holdingUID;
    transaction_map.zoomHolding('nrlais_inventory', holdingUID);
    //show holding information
    $.ajax('/holding/min_holding_search_result.jsp?holding_uid=' + holdingUID, {
        'data': {'method': 'GET'},
        'success': function (data, textStatus, jqXHR) {
            $('#holder_content').html(data);
            $('#pick_person').modal('hide');
        }
    });


    //load holding data from database
    $.ajax('/api/get_holding?schema=nrlais_inventory&holding_uid=' + holdingUID,
            {
                method: 'GET',
                success: function (data)
                {
                    //alert(JSON.stringify(data));
                    reallocation_holding_data = data.res;
                    var party_done = [];
                    reallocation_holders = [];
                    reallocation_holding_data.parcels.forEach(function (parcel)
                    {
                        parcel.rights.forEach(function (right)
                        {
                            if (right.rightType != RIGHT_TYPE_PUR)
                                return;
                            if (party_done[right.partyUID])
                                return;
                            party_done[right.partyUID] = true;
                            reallocation_holders.push(right);
                        });
                    });
                },
                dataType: 'json'
            });

    //setup applicant table
    reallocation_configureApplicantPicker();
    if (reallocation_combined)
    {
       
        if (reallocation_combined.data.tranferType == TRANSFER_TYPE_OTHER_HOLDING)
            reallocation_allocated_party = reallocation_combined.data.beneficiaries;
        else
            reallocation_allocated_party = null;
    } 
    //setup parcel tansfer table
    var parcelSetting = {
        split: true,
        transferArea: false,
        transferShare: reallocation_type == TRANSFER_TYPE_THIS_HOLDING,
        transferLabel: lang("Reallocate"),
        areaLabel: lang("Reallocated Area"),
        shareLabel: lang("Transfered Share")
    }
    if (reallocation_combined == null)
        parcelTransferTable.load('#holder_parcel', holdingUID, null, parcelSetting);
    else
    {
        parcelTransferTable.load('#holder_parcel', holdingUID, reallocation_combined.data.transfers, parcelSetting);
    }
 



    //load required documents

    reallocation_holdingID = holdingUID;
}
function reallocation_validateParcelPage() {
    var valid = true;
    var validator = validatorPanel('.parcel-error-panel');
    var parcel = $('#holder_parcel  input:checked').length > 0;
    if (!parcel) {

        validator.addError("selectParcel", lang("Please select at least one parcel"));
        valid = false;
    } else {
        validator.removeError("selectParcel");
    }
    return valid;
}
function reallocation_validatePartyPage() {
    var validApplication = true;
    var validator = validatorPanel(".error_reallocation_recipient_panel");
    if (!$.trim($('#reallocation_beneficiaries tbody').html()).length) {
        validator.removeError("partyPanel");
        validator.addError("partyPanel", lang("Please add new party for reallocation"));
        validApplication = false;
    } else {
        validator.removeError("partyPanel");
    }
    return validApplication;
}
function reallocation_validateApplicantPage() {
    return applicantPicker.validate(reallocation_holders);
}
function reallocation_validateApplicationPage()
{
    return transaction_validateApplication() && validatePageOne();
}
function reallocation_validateReqDocumentPage() {
    var valid = true;
    var validator = validatorPanel('.error-req-doc-panel');

    if (!documentPicker.getRequiredDocument(0))
    {
        validator.removeError("adminDecisionDoc");
        validator.addError("adminDecisionDoc", lang("Please add reallocation decision document"));
        valid = false;
    } else {
        validator.removeError("adminDecisionDoc");
    }
    if (!documentPicker.getRequiredDocument(1)) {

        validator.removeError("claimResolution");
        validator.addError("claimResolution", lang("Please add claim resolution document"));
        valid = false;
    } else {
        validator.removeError("claimResolution");
    }
    if (!documentPicker.getRequiredDocument(2))
    {
        validator.removeError("landHoldingCertificate");
        validator.addError("landHoldingCertificate", lang("Please add land holding certificate"));
        valid = false;
    } else {
        validator.removeError("landHoldingCertificate");
    }
    return valid;
}
function reallocation_SaveToServer()
{
    transaction_saveTransaction(reallocation_combined, false, function (ternID) {
        bootbox.alert({
            message: lang("You Have Successfully Registered a Transaction")+"!",
            callback: function () {
                window.location.href = "/dashboard.jsp";
            }
        });
    }, function (err) {
        bootbox.alert(lang("Transaction Registration Failed")+". \n" + err);
    });
}
function reallocation_showPreview() {
    reallocation_buildTransactionObject();
    $.ajax({type: 'POST',
        url: "/tran/reallocation/full_reallocation_viewer_embeded.jsp",
        contentType: "application/json",
        data: JSON.stringify(reallocation_combined),
        async: true,
        error: function (errorThrown) {
            alert(lang("error"));
        },
        success: function (data) {
            $("#print_slip").html(data);
        }

    });
}

function reallocation_setReallocationType(type, advance)
{
    var all = ["#step-app", "#step-app", "#step-parcel"
                , "#step-beneficiary", "#step-existing-holding", "#step-docs", "#step-finish"
                , "#step-this-holding"];
    if (reallocation_type == type)
        return;
    reallocation_type = type;
    var newTabSetup;
    $("#reallocation_type_marker_nnew").html("");
    $("#reallocation_type_marker_other").html("");
    $("#reallocation_type_marker_this").html("");
    switch (type)
    {
        case TRANSFER_TYPE_NEW_HOLDING:
            newTabSetup = [{tab: "#step-app-tab", cont: "#step-app", enable: true},
                {tab: "#step-type-tab", cont: "#step-type", enable: true},
                {tab: "#step-this-holding-tab", cont: "#step-this-holding", enable: true},
                {tab: "#step-parcel-tab", cont: "#step-parcel", enable: true},
                {tab: "#step-beneficiary-tab", cont: "#step-beneficiary", enable: true},
                {tab: "#step-docs-tab", cont: "#step-docs", enable: true},
                {tab: "#step-finish-tab", cont: "#step-finish", enable: true},
            ];
            $("#reallocation_type_marker_nnew").html("*");
            break;
        case TRANSFER_TYPE_OTHER_HOLDING:
            newTabSetup = [{tab: "#step-app-tab", cont: "#step-app", enable: true},
                {tab: "#step-type-tab", cont: "#step-type", enable: true},
                {tab: "#step-this-holding-tab", cont: "#step-this-holding", enable: true},
                {tab: "#step-parcel-tab", cont: "#step-parcel", enable: true},
                {tab: "#step-existing-holding-tab", cont: "#step-existing-holding", enable: true},
                {tab: "#step-docs-tab", cont: "#step-docs", enable: true},
                {tab: "#step-finish-tab", cont: "#step-finish", enable: true},
            ];
            $("#reallocation_type_marker_other").html("*");
            break;
        case TRANSFER_TYPE_THIS_HOLDING:
            newTabSetup = [{tab: "#step-app-tab", cont: "#step-app", enable: true},
                {tab: "#step-type-tab", cont: "#step-type", enable: true},
                {tab: "#step-this-holding-tab", cont: "#step-this-holding", enable: true},
                {tab: "#step-beneficiary-tab", cont: "#step-beneficiary", enable: true},
                {tab: "#step-docs-tab", cont: "#step-docs", enable: true},
                {tab: "#step-finish-tab", cont: "#step-finish", enable: true},
            ];
            $("#reallocation_type_marker_this").html("*");
            break;
        default:
            newTabSetup = [{tab: "#step-app-tab", cont: "#step-app", enable: true},
                {tab: "#step-type-tab", cont: "#step-type", enable: true},
            ];
            break;
    }
    for (var i in all)
    {
        var hide = true;
        for (var j in newTabSetup)
        {
            if (newTabSetup[j].cont == all[i])
            {
                hide = false;
                break;
            }
        }
        if (hide)
            $(all[i]).hide();
    }

    wizardPages.buildNavHtml("#tab-buttons", newTabSetup);

    reallocation_configureApplicantPicker();
    if (advance)
        wizardPages.gotoNext();
}
function reallocation_init() {
    //setup wizard
    if (reallocation_combined)
        reallocation_setReallocationType(reallocation_combined.data.tranferType);
    else
        reallocation_setReallocationType(-1);
    wizardPages.beforePageChange = function (show, hide)
    {
        var ret = true;
        if (hide == "#step-app-tab") {
            ret = reallocation_validateApplicationPage();
        } else if (hide == "#step-type-tab") {
            ret = true;
        } else if (hide == "#step-parcel-tab") {
            ret = reallocation_validateParcelPage();
        } else if (hide == "#step-beneficiary-tab") {
            ret = reallocation_validatePartyPage();
        } else if (hide == "step-existing-holding-tab") {
            reallocation_allocated_party = applicantPicker.getPartyItems();
            ret = true;
        } else if (hide == "step-this-holding-tab") {
            ret = true;
        } else if (hide == "#step-docs-tab") {
            ret = reallocation_validateReqDocumentPage();
        } if (show == "#step-finish-tab") {
            reallocation_showPreview();
        }
        if (show == "#setp-app-tab")
            app_holdingSearch.onHoldingPicked = reallocation_addReallocationHolding;
        else if (show == "#step-existing-holding-tab")
        {
            reallocation_configureBenificiaryApplicantPicker();
            app_holdingSearch.onHoldingPicked = reallocation_setExistingHolding;
        }
        return ret;
    }
    $("#app_type").val(lang("Reallocation Transacation"));

    app_holdingSearch.onHoldingPicked = reallocation_addReallocationHolding;

    documentPicker.load("#documentTableBody");

    //load existing data
    if (reallocation_holdingID != null)
    {
        reallocation_addReallocationHolding(reallocation_holdingID);
    }
    if (reallocation_combined != null)
    {
        reallocation_setExistingHolding(reallocation_combined.data.otherHoldingUID);
    }
    transaction_saveInitial = function ()
    {
        if (!reallocation_holding_data)
        {
            bootbox.alert(lang('You must at least select holding to save the rent transaction'));
            return;
        }
        reallocation_buildTransactionObject();
        transaction_saveTransaction(reallocation_combined, true, function (tranUID) {
            bootbox.alert({
                message: lang("You Have Saved a Transaction Data Temporarily")+"!",
                callback: function () {
                    window.location.href = "/dashboard.jsp";
                }
            });
        }, function (err) {
            bootbox.alert(lang("Saving Transaction Data Failed")+".\n" + err);
        });
    }

    //initialize beneficiaries party list picker
    reallocation_beneficiaries = getPartyListPicker({
        containerEl: "reallocation_beneficiaries",
        addButtonEl: "reallocation_beneficiary_add",
        share: true,
        idDoc: true,
        edit: true,
        delete: true,
    });
    if (reallocation_combined)
    {
        reallocation_beneficiaries.setParyItems(reallocation_combined.data.beneficiaries);
    }


    //load required documents
    if (reallocation_combined)
    {
        documentPicker.setRequiredDocument(0, reallocation_combined.data.adminDecisionDoc);
        documentPicker.setRequiredDocument(1, reallocation_combined.data.claimResolution);
        documentPicker.setRequiredDocument(2, reallocation_combined.data.landHoldingCertificateDoc);
    }

}
