/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var ex_officio_holding_data;
var ex_officio_holdingID;
var ex_officio_combined;
var ex_officio_holders;
var ex_officio_applicant;
var ex_officio_parcels;

function ex_officio_buildTransactionObject() {
    var ex_officioData = {
        holdingUID: ex_officio_holdingID,
        applicant: ex_officio_applicant.parties,
        woredaAdministrationDecission: documentPicker.getRequiredDocument(0),
        otherDocument:documentPicker.getAllAddtionalDocument()
    }
    var tranData = transaction_getTransactionInfo(TRAN_TYPE_EX_OFFICIO);
    tranData.nrlais_kebeleid = ex_officio_holding_data.nrlais_kebeleid;
    tranData.data = ex_officioData;
    ex_officio_combined = tranData;
}
function ex_officio_SaveToServer() {
    transaction_saveTransaction(ex_officio_combined, false, function (ternID) {
         bootbox.alert({
            message: lang("You Have Successfully Registered a Transaction")+"!",
            callback: function () {
                window.location.href = "/dashboard.jsp";
            }
        });
    }, function (err) {
        bootbox.alert(lang("Transaction Registration Failed")+".\n" + err);
    });
}
function ex_officio_addExOfficioHolding(holdingUID) {
    transaction_map.zoomHolding('nrlais_inventory', holdingUID);
    $.ajax('/holding/min_holding_search_result.jsp?holding_uid=' + holdingUID, {
        'data': {'method': 'GET'},
        'success': function (data, textStatus, jqXHR) {
            $('#holder_content').html(data);
            $('#pick_person').modal('hide');
        }
    });
    $.ajax('/api/get_holding?schema=nrlais_inventory&holding_uid=' + holdingUID,
            {
                method: 'GET',
                success: function (data)
                {
                    //alert(JSON.stringify(data));
                    ex_officio_holding_data = data.res;
                    var party_done = [];
                    ex_officio_holders = [];
                    ex_officio_holding_data.parcels.forEach(function (parcel)
                    {
                        parcel.rights.forEach(function (right)
                        {
                            if (right.rightType != RIGHT_TYPE_PUR)
                                return;
                            if (party_done[right.partyUID])
                                return;
                            party_done[right.partyUID] = true;
                            ex_officio_holders.push(right);
                        });
                    });
                },
                dataType: 'json'
            });
    ex_officio_holdingID = holdingUID;
}
function ex_officio_validateApplicationPage()
{
    return transaction_validateApplication() && validatePageOne();
}
function ex_officio_validateApplicantPage(){
    var validApplication = true;
    var validator = validatorPanel(".ex_officio_applicant_error");
    if (!$.trim($('#ex_officio_applicant tbody').html()).length) {
        validator.removeError("woredaPanel");
        validator.addError("woredaPanel", lang("Please Add Applicant Information"));
        validApplication = false;
    } else {
        validator.removeError("woredaPanel");
    }
    return validApplication;
}
function ex_officio_showPreview() {

    ex_officio_buildTransactionObject();
    $.ajax({type: 'POST',
        url: "/tran/ex_officio/full_ex_officio_viewer_embeded.jsp",
        contentType: "application/json",
        data: JSON.stringify(ex_officio_combined),
        async: true,
        error: function (errorThrown) {
            bootbox.alert(lang("error"));
        },
        success: function (data) {
            var iframe = $('iframe#printSlip').contents();
            iframe.find('body').html(data);
            $('#printSlip').attr('height', document.getElementById("printSlip").contentWindow.document.body.scrollHeight + "px");
        }

    });
}
function ex_officio_validateDocument(){
     var valid = true;
    var validator = validatorPanel('.error-req-doc-panel');

    if (!documentPicker.getRequiredDocument(0))
    {
        validator.removeError("decisionOfWoreda");
        validator.addError("decisionOfWoreda", lang("Please add Ex-Officio State Decision"));
        valid = false;
    } else {
        validator.removeError("decisionOfWoreda");
    }
    return valid;
}
function ex_officio_init() {
    wizardPages.load([
        {tab: "#step-1-tab", cont: "#step-1", enable: true},
        {tab: "#step-2-tab", cont: "#step-2", enable: true},
        {tab: "#step-3-tab", cont: "#step-3", enable: true},
        {tab: "#step-4-tab", cont: "#step-4", enable: true},
    ]);
    wizardPages.beforePageChange = function (show, hide)
    {
        var ret = true;
        if (hide == "#step-1-tab")
        {
            //ret=rent_validateApplicationPage();   
            ret = ex_officio_validateApplicationPage();
        } else if (hide == "#step-2-tab")
        {
            //ret=rent_validateApplicantPage();
            ret = ex_officio_validateApplicantPage();
        } else if (hide == "#step-3-tab") {
            ret = ex_officio_validateDocument();
        }
        if (show == "#step-4-tab")
        {
            ex_officio_showPreview();
        }
        return ret;
    }
    $("#app_type").val(lang("Ex-Officio"));


    app_holdingSearch.onHoldingPicked = ex_officio_addExOfficioHolding;
    documentPicker.load("#documentTableBody");
    if (ex_officio_holdingID != null)
    {
        ex_officio_addExOfficioHolding(ex_officio_holdingID);
    }
    ex_officio_applicant = getPartyListPicker({
        containerEl: "ex_officio_applicant",
        addButtonEl: "ex_officio_applicant_add",
        edit: true,
        delete: true,
        partyType: 3
    });
    if (ex_officio_combined) {
        ex_officio_applicant.setParyItems(ex_officio_combined.data.applicant);
    }
    transaction_saveInitial = function ()
    {
        if (!ex_officio_holding_data)
        {
            bootbox.alert(lang('You must at least select holding to save divorce transaction'));
            return;
        }
        ex_officio_buildTransactionObject();
        transaction_saveTransaction(ex_officio_combined, true, function (tranUID) {
             bootbox.alert({
                message: lang("You Have Saved a Transaction Data Temporarily")+"!",
                callback: function () {
                    window.location.href = "/dashboard.jsp";
                }
            });
        }, function (err) {
            bootbox.alert(lang("Saving Transaction Data Failed")+".\n" + err);
        });
    }

    if (ex_officio_combined)
    {
        documentPicker.setRequiredDocument(0, ex_officio_combined.data.woredaAdministrationDecission);
        if(ex_officio_combined.data.otherDocument.length>0){
            documentPicker.setAddtionalDocument(divorce_combined.data.otherDocument);
        }
    }
}

