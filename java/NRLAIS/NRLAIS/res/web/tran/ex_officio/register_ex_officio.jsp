<%-- 
    Document   : register_ex_officio
    Created on : Feb 14, 2018, 10:22:15 AM
    Author     : yitagesu
--%>
<%@page import="com.intaps.nrlais.model.*" %>
<%@page import="com.intaps.nrlais.repo.*" %>
<%@page import="com.intaps.nrlais.controller.*" %>
<%@page import="com.intaps.nrlais.controller.tran.*" %>
<script src="/assets/JS/wizard_pages.js"></script>
<script src="/assets/JS/ejs.js"></script>
<script src="/party/party_list.js"></script>
<script src="/party/register_party.js"></script>
<%
    ExOfficioViewController controller=new ExOfficioViewController(request,response,true);
    /*if there is a holding already saved we will declare the holding variable here
    this will override the declartion in register_divorce.jsp and the divorce_initDivorce function will load the holding when 
    it sees that the hodling is not empty
    */
    if(controller.holding.holdingUID!=null){ 
%>
    <script>
        var ex_officio_holdingID='<%=controller.holding.holdingUID%>';
        var ex_officio_combined=<%=controller.json(controller.tran)%>;
    </script>
<%}%>

<div class="stepwizard">
    <div class="stepwizard-row setup-panel">
        <div class="stepwizard-step">
            <a id='step-1-tab' href="javascript:wizardPages.openPage('#step-1-tab')" type="button" class="btn btn-default btn-circle">1</a>
        </div>
        <div class="stepwizard-step">
            <a id='step-2-tab' href="javascript:wizardPages.openPage('#step-2-tab')" type="button" class="btn btn-default btn-circle">2</a>

        </div>
        <div class="stepwizard-step">
            <a id='step-3-tab' href="javascript:wizardPages.openPage('#step-3-tab')" type="button" class="btn btn-default btn-circle">3</a>

        </div>
        <div class="stepwizard-step">
            <a id='step-4-tab' href="javascript:wizardPages.openPage('#step-4-tab')" type="button" class="btn btn-default btn-circle">4</a>

        </div>
    </div>
</div>
<form role="form" action="" method="post">
    <div class="row setup-content" id="step-1">
        <div class="col-md-12">
            <h4><small><%=controller.text("Set Application")%></small></h4>
            <hr>
            <div class="form-group col-md-6">
                <label class="control-label"><%=controller.text("Application Type")%>:</label>
                <input  type="text"  class="form-control" disabled="disabled" id="app_type" value="<%=controller.tranTypeText()%>"/>
            </div>
            <div class="form-group col-md-6">
                <label class=""><%=controller.text("Application Date")%>:</label>
                <input max="5" type="text" class="form-control" placeholder="pick application Date"  id="app_date" dateVal="<%=controller.grigDate(controller.tran.time)%>"/>
            </div>
            <div class="form-group col-md-12">
                <label class=""><%=controller.text("Application Description")%>:</label>
                <textarea rows="4"  class="form-control" placeholder="<%=controller.text("Write application description")%>" id="app_description"><%=controller.escapeHtml(controller.tran.notes)%></textarea>
            </div>
            <div class="form-group col-md-12" id="holder_content">

            </div>
            <div class="col-md-12">
                <div class="pull-left">
                    <div class="error-panel">

                    </div>
                </div>
                <div class="pull-right">
                    <div class="btn-group">
                        <button class="btn btn-nrlais" type="button" data-toggle='modal' data-target='#pick_person' data-backdrop='static' ><i class="fa fa-hand-o-up"></i><%=controller.text("Load Parcel")%></button>
                        <button class="btn btn-nrlais" type="button" onclick="wizardPages.gotoNext()" id="nextToSecond"><%=controller.text("Next")%> <i class="fa fa-angle-double-right"></i></button>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="row setup-content" id="step-2">
        <div class="col-md-12">
            <h4><small><%=controller.text("Applicant Information")%></small></h4>
            <hr/>
            <div id="ex_officio_applicant">
            </div>
            <div class="align-right pull-right">
                <div class="btn-group">
                    <button id="ex_officio_applicant_add" class="btn btn-nrlais" type="button"><i class="fa fa-plus"></i><%=controller.text("Add Party")%> </button>
                    <button class="btn btn-nrlais nextBtn" type="button" id="doc_nextbtn" onclick="wizardPages.gotoNext()"><%=controller.text("Next")%> <i class="fa fa-angle-double-right"></i></button>
                </div>
            </div>
            <div class="pull-left">
                <div class="btn-group">
                    <div class="ex_officio_applicant_error error_panel">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row setup-content" id="step-3">
        <div class="col-md-12">
            <h4><small><%=controller.text("Add Required Documents")%></small></h4>
            <hr/>
            <table class="table table-striped jambo_table">
                <thead>
                    <tr>
                        <td><%=controller.text("Document Type")%></td>
                        <td><%=controller.text("Document Reference")%></td>
                        <td><%=controller.text("Document Description")%></td>
                        <td><%=controller.text("Document File")%></td>
                        <td></td>
                    </tr>
                </thead>
                <tbody id="documentTableBody">                    
                    <tr doc_type="<%=DocumentTypeInfo.DOC_TYPE_EX_OFFICIO_STATE_DECISION%>">
                        <td><%=controller.text("Ex-Officio State Decision")%>:</td>
                        <td id="doc_ref"></td>
                        <td id="doc_desc"></td>
                        <td id="doc_File"></td>
                        <td><a  class="btn btn-nrlais"><i class="fa fa-plus"></i></a> </td>
                    </tr>
                </tbody>
            </table>

            <div class="align-right pull-right">
                <div class="btn-group">
                    <button class="btn btn-nrlais" type="button" data-toggle='modal' data-target='#add_document' data-backdrop='static'><i class="fa fa-plus"></i><%=controller.text("Add")%> </button>
                    <button class="btn btn-nrlais nextBtn" type="button" id="doc_nextbtn" onclick="wizardPages.gotoNext()"><%=controller.text("Next")%> <i class="fa fa-angle-double-right"></i></button>
                </div>
            </div>
            <div class="pull-left">
                <div class="btn-group">
                    <div class="error-req-doc-panel">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row setup-content" id="step-4">
        <div class="col-md-12">
            <h4> <small><%=controller.text("Review and Submit Application")%></small></h4>
            <hr>
            <div class="print_slip" id="print_slip">
                <iframe id="printSlip"  frameborder="0" width="100%" height="150px" scrolling="no"></iframe>
            </div>
            <div class="align-right">
                <div class="btn-group">
                    <button class="btn btn-nrlais" type="button" id="print" onclick="pritSlip()"><i class="fa fa-print"></i> <%=controller.text("Print")%></button>
                    <button class="btn btn-nrlais nextBtn" type="button" id="doc_nextbtn" onclick="ex_officio_SaveToServer()"><%=controller.text("Register Transaction")%> <i class="fa fa-exclamation"></i></button>
                </div>
            </div>
        </div>
    </div>
</form>
