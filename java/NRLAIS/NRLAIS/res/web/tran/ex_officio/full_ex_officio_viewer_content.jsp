<table class="table table-striped">
    <tbody id="">

        <%@ include file="/tran/tran_viewer_header.jsp" %>        
        <tr><td class="table_separetor" colspan="2"><label><%=controller.text("Land Holder")%></label></td></tr>

        <%for(LADM.Right r:controller.holding.getHolders()){
            String holderROLE=controller.roleText(r.party.mreg_familyrole);
            String holderSEX=controller.sexText(r.party.sex);
        %>
        <tr id="final_<%=controller.holding.holdingUID%>" class="holder">
            <td class="many">
                <label><%=holderROLE%> <%=controller.text("Info")%></label>
                <ul>
                    <li><label><%=controller.text("Name")%>: </label> <%=r.party.getFullName()%></li>
                    <li><label><%=controller.text("Sex")%>: </label> <%=holderSEX%></li>
                    <li><label><%=controller.text("Age")%>: </label> <span id="ageOfApp"><%=controller.age(r.party.dateOfBirth)%> (<%=EthiopianCalendar.ToEth(r.party.dateOfBirth).toString()+" "+controller.text("EC")%>)</span></li>
                    <li><label><%=controller.text("Share")%>: </label> <%=r.share.num%>/<%=r.share.denum%></li>
                </ul>
            </td>
            <td class="many">



            </td>
        </tr>
        <%}//for%>

        <tr><td class="table_separetor" colspan="2"><label><%=controller.text("Applicant Information")%></label></td></tr>
        <%for(PartyItem p:controller.data.applicant){%>
        <tr><td colspan="2">
                <ul>
                    <li><label><%=controller.text("Name")%>: </label> <%=p.party.name1%></li>
                    <li><label><%=controller.text("Address")%>: </label> <%for(LADM.PartyContact ad:p.party.contacts){%>
                        <%=ad.contactData%>
                        <%}%></li>
                </ul>
            </td></tr>
            <%}%>
        <tr><td colspan="2" class="table_separetor"><label><%=controller.text("Document Provides")%></label></td></tr>
        <tr>
            <td colspan="2">
                <ol>
                    <li><%=controller.adminSourceText(DocumentTypeInfo.DOC_TYPE_EX_OFFICIO_STATE_DECISION)%> (<a href="<%=controller.woredaAdministrationDecisionLink()%>"><%=controller.woredaAdministrationDecisionText()%></a>)</li>
                        <%for(SourceDocument doc:controller.data.otherDocument){%>
                    <li><%=controller.adminSourceText(DocumentTypeInfo.DOC_TYPE_OTHER)%>:
                        <%if(doc.fileImage==null){%>
                        <%=doc.description%> (<%=doc.refText%>)
                        <%}else{%>
                        <a href="<%=controller.showDocumentLink(doc)%>"><%=doc.description%> (<%=doc.refText%>)</a>
                        <%}%>
                    </li>
                    <%}%>
                </ol>
            </td>
        </tr>
    </tbody>

</table>