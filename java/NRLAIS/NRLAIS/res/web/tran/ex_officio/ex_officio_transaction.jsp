<%-- 
    Document   : ex_officio_transaction
    Created on : Feb 15, 2018, 9:55:15 AM
    Author     : yitagesu
--%>
<%@page import="com.intaps.nrlais.repo.*"%>
<%@page import="com.intaps.nrlais.*"%>
<%@page import="com.intaps.nrlais.controller.*"%>
<%@page import="com.intaps.nrlais.controller.tran.*"%>
<%@page import="com.intaps.nrlais.model.*"%>
<%@page import="java.util.*"%>
<%@page pageEncoding="UTF-8" %>
<%

   HeaderViewController headerController=new HeaderViewController(request,response,false);
   ExOfficioViewController controller=new ExOfficioViewController(request,response,true);
   
%>
<html>
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>NRLAIS</title>
        <link rel="stylesheet" href="/assets/CSS/bootstrap.min.css"/>
        <link rel="stylesheet" href="/assets/CSS/bootstrap-popover-x.min.css"/>
        <link rel="stylesheet" href="/assets/CSS/custom.css"/>
        <link rel="stylesheet" href="/assets/ol/ol.css"/>
        <!--        third party-->
        <script src="/assets/JS/jquery.min.js"></script>
        <script src="/assets/JS/jquery-ui.js"></script>
        <script src="/assets/JS/bootstrap.min.js"></script>
        <script src="/assets/JS/bootstrap-popover-x.min.js"></script>
        <script src="/assets/JS/bootbox.js"></script>
        <script src="/assets/JS/string.js"></script>
        <script src="/assets/JS/uuid-gen.js"></script>

        <!--        INTAPS common-->
        <script src="/assets/JS/et_dual_cal.js"></script>
        <script src="/assets/JS/general.js"></script>

        <!--        NRLAIS general-->
        <script src="/assets/JS/js_constants.jsp"></script>
        <script src="/assets/JS/document_picker.js"></script>        
        <script src="/holding/searching_holding.js"></script>
        <script src="/assets/JS/transaction.js"></script>
        <script src="/assets/JS/attachment.js"></script>
        <script src="/holding/applicants_picker.js"></script>
        <script src="/holding/parcel_split_table.js"></script>
        <script src="/holding/parcel_transfer_table.js"></script>
        <script src="/party/register_party.js"></script>
        <script src="/assets/JS/nrlais_map.js"></script>
        <script src="/assets/ol/ol-debug.js"></script>
        <script src="/assets/ol/proj4.js"></script>

        <!--        NRLAIS transaction specific-->
        <script src="/tran/divorce/register_divorce.js"></script>
        <script src="/tran/rent/register_rent.js"></script>
        <script src="/tran/gift/register_gift.js"></script>
        <script src="/tran/inheritance/register_inheritance.js"></script>
        <script src="/tran/reallocation/register_reallocation.js"></script>
        <script src="/tran/boundary_correction/register_boundary_correction.js"></script>
        <script src="/tran/expropriation/register_expropriation.js"></script>
        <script src="/tran/boundary_correction/register_boundary_correction.js"></script>
        <script src="/tran/special_case/register_special_case.js"></script>
        <script src="/tran/consolidation/register_consolidation.js"></script>
        <script src="/tran/exchange/register_exchange.js"></script>

        <script src="/tran/ex_officio/register_ex_officio.js"></script>
        <script src="/assets/JS/lookups.js.jsp"></script>

        <script src="/assets/JS/modal_controler.js"></script>
        <script src="/assets/JS/ejs.js"></script>
        <script src="/party/party_list.js"></script>
        <script src="/party/register_party.js"></script>
        <script src="/parcel/parcel_list.js"></script>
        <script src="/parcel/register_parcel.js"></script>
        <%if(controller.holding.holdingUID!=null){ %>
        <script>
            var ex_officio_holdingID = '<%=controller.holding.holdingUID%>';
            var ex_officio_combined =<%=controller.json(controller.tran)%>;
        </script>
        <%}%>
        <script>
            $(document).ready(function () {
                ex_officio_transaction();
            });
        </script>
    </head>

    <body id="body">
        <%@ include file="../../header.jsp" %>
        <div class="dashboard-body">
            <div class="list-of-transc col-md-8" style='height:95%'>
                <div id="reg_application" style='height:100%'>
                    <div class="x_panel " id="" style='height:100%'>
                        <div class="x_title">
                            <ul class="nav nav-tabs tabs-left sideways">
                                <li class="active " id="holding_tap"><a href="#holding" data-toggle="tab"><%=controller.text("Holding")%> (01)</a></li>
                                <li class="" id="add_holding_tap"><a href="#add_holding" data-toggle="tab"><%=controller.text("Add Holding")%></a></li> 
                            </ul>
                        </div>
                        <div class="x_content has-scroll" style='height:95%'>
                            <div id="appContent" class="col-md-12">
                                <div class="" id="holding">
                                    <div class="col-md-12 bhoechie-tab-container">
                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 bhoechie-tab-menu">
                                            <div class="list-group">
                                                <a href="#" class="list-group-item active"><h4 class="fa fa-folder-open"></h4><%=controller.text("Holding")%></a>                   
                                                <a href="#" class="list-group-item"><h4 class="fa fa-map-marker"></h4><%=controller.text("Parcels")%> </a>                
                                                <a href="#" class="list-group-item"><h4 class="fa fa-user"></h4> <%=controller.text("Holder")%></a>
                                                <a href="#" class="list-group-item"><h4 class="fa fa-registered"></h4><%=controller.text("Rent")%> </a>
                                                <a href="#" class="list-group-item"><h4 class="fa fa-minus-hexagon"></h4><%=controller.text("Restriction")%></a>
                                            </div>

                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 bhoechie-tab has-scroll">
                                            <div class="bhoechie-tab-content active" id="tranContent">
                                                <table class="table table-striped jambo_table">
                                                    <thead>
                                                        <tr>
                                                            <th><%=controller.text("Woreda")%></th>
                                                            <th><%=controller.text("Kebele")%></th>
                                                            <th><%=controller.text("Holding ID")%></th>
                                                            <th><%=controller.text("Holding Type")%></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td><%=controller.woreda.name.textEn%></td>
                                                            <td><%=controller.kebele.name.textEn%></td>
                                                            <td><%=controller.holding.holdingSeqNo%></td>
                                                            <td><%=controller.holdingTypeText(controller.holding.holdingType)%></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>                
                                            <div class="bhoechie-tab-content">
                                                <div id="ex_officio_parcel">

                                                </div>
                                                <div class="align-right">
                                                    <input type="button" class="btn btn-nrlais" id="add_ex_officio_parcel_btn" value="Add Parcel"/>
                                                </div>
                                            </div>                  
                                            <div class="bhoechie-tab-content"></div>
                                            <div class="bhoechie-tab-content"></div>
                                            <div class="bhoechie-tab-content"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="map col-md-4" style='height:95%'>
                <div class="x_panel" style='height:100%'>
                    <div class="x_content has-scroll" id="map" style='height:100%'>
                    </div>
                </div>
            </div>
        </div>
    </body>

</html>



