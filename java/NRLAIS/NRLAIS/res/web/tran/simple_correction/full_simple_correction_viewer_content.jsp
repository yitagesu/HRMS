<%-- 
    Document   : full_simple_correction_viewer_content
    Created on : Feb 27, 2018, 9:35:15 AM
    Author     : yitagesu
--%>

<table class="table table-striped">
    <tbody id="">
        <%@ include file="/tran/tran_viewer_header.jsp" %>
        <tr><td class="table_separetor" colspan="2"><label><%=controller.text("Holder Information")%></label></td> </tr>
        <%for(LADM.Right r:controller.holding.getHolders()){
            String holderROLE=controller.roleText(r.party.mreg_familyrole);
            String holderSEX=controller.sexText(r.party.sex);
            Applicant ap=controller.applicant(r.party.partyUID);
        %>
        <tr id="final_<%=controller.holding.holdingUID%>" class="holder">
            <td class="many">
                <label><%=holderROLE + " "%><%=controller.text("Info")%></label>
                <ul>
                    <li><label><%=controller.text("Name")%>: </label> <%=r.party.getFullName()%></li>
                    <li><label><%=controller.text("Sex")%>: </label> <%=holderSEX%></li>
                    <li><label><%=controller.text("Age")%>: </label> <span id="ageOfApp"> <%=controller.age(r.party.dateOfBirth)%> (<%=EthiopianCalendar.ToEth(r.party.dateOfBirth).toString()+" "+controller.text("EC")%>)</span></li>
                    <li><label><%=controller.text("Share")%>: </label> <%=r.share.num%>/<%=r.share.denum%></li>
                    <li><label><%=controller.text("ID")%>: </label><%if(ap.idDocument.fileImage ==null){%>
                        <%=controller.applicantIDTypeText(ap)%> (<%=controller.applicantIDText(ap)%>)
                        <%}else{%>
                        <a href="<%=controller.applicantIDLink(ap)%>"><%=controller.applicantIDTypeText(ap)%> (<%=controller.applicantIDText(ap)%>)</a>
                        <%}%>
                    </li>
                    
                    <li><label><%=controller.text("Proof of Marital Status")%>: </label> <%if(ap.pomDocument.fileImage ==null){%>
                        <%=controller.applicantPOMText(ap)%>
                        <%}else{%>
                        <a href="<%=controller.applicantPOMLink(ap)%>"><%=controller.applicantPOMText(ap)%></a>
                        <%}%>
                    </li>
                </ul>
            </td>
            <td class="many">
                <label><%=holderROLE + " "%><%=controller.text("Representative info")%> </label>
                <%
                            String message="";
                            
                            if(ap!=null && ap.representative!=null)
                            {
                                String repSEX="";
                                repSEX=controller.sexText(ap.representative.sex);
                            
            
                %>
                <ul>
                    <li> <label><%=controller.text("Name")%>: </label> <%=ap.representative.getFullName()%></li>
                    <li> <label><%=controller.text("Relationship")%>: </label> <%=ap.relationWithParty%></li>
                    <li> <label><%=controller.text("Age")%>: </label> <span id="ageOfRep"><%=controller.age(ap.representative.dateOfBirth)%> (<%=EthiopianCalendar.ToEth(ap.representative.dateOfBirth).toString()+" "+controller.text("EC")%>)</span></li>
                    <li><label><%=controller.text("Sex")%>: </label> <%=repSEX%></li>
                    <%for(LADM.PartyContact a:ap.representative.contacts){
                        if(a.contactdataType==LADM.PartyContact.CONTACT_TYPE_OTHER){%>
                            <li><label><%=controller.text("Phone")%>: </label> <%=a.contactData%></li>
                        <%}if(a.contactdataType==LADM.PartyContact.CONTACT_TYPE_ADDRESS){%>
                            <li><label><%=controller.text("Address")%>: </label> <%=a.contactData%></li> 
                    <%}}%>
                    <li><label>Letter of Attorney:</label> <%if(ap.letterOfAttorneyDocument.fileImage ==null){%>
                            <%=controller.letterOfAttroneyText(ap)%>
                        <%}else{%>
                        <a href="<%=controller.letterOfAttroneyLink(ap)%>"><%=controller.letterOfAttroneyText(ap)%></a>
                        <%}%>
                    </li>                    
                </ul>

                <%}else{ message= controller.text("No Representative");%>

                <p><%=message%></p>

                <%}//if-else%>


            </td>
        </tr>
        <%}//for%>
        <tr><td colspan="2" class="table_separetor"><label><%=controller.text("Document Provides")%></label></td></tr>
        <tr>
            <td colspan="2">
                <ol>
                    <li><%=controller.adminSourceText(DocumentTypeInfo.DOC_TYPE_LAND_HOLDING_CERTIFICATE)%> (<a href="<%=controller.landHoldingCertificateLink()%>"><%=controller.landHoldingCertificateText()%></a>)</li>
                    <%for(SourceDocument doc:controller.data.otherDocument){%>
                        <li><%=controller.adminSourceText(DocumentTypeInfo.DOC_TYPE_OTHER)%>: <%=doc.description%> (<%=doc.refText%>)</li>
                    <%}%>
                </ol>
            </td>
        </tr>
    </tbody>
</table>
