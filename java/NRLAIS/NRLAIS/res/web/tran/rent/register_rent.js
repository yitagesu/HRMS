/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var rent_holding_data;
var rent_holders;
var rent_holdingID;
var rent_combined;
var rent_tenants;
var rent_contract_document_attachment = null;
var rent_contract_document_attachment_mime = null;
var rent_start_date_picker;
var rent_end_date_picker
function rent_buildTransactionObject()
{
    var rentData = rent_validateAndGateRentContract();

    rentData.holdingUID = rent_holdingID;
    rentData.applicants = applicantPicker.getApplicants();
    rentData.transfers = parcelTransferTable.getTransfers(rent_holding_data);
    rentData.tenants = rent_tenants.parties;
    rentData.landHoldingCertificateDoc=documentPicker.getRequiredDocument(0);
    rentData.otherDocument=documentPicker.getAllAddtionalDocument();
    var tranData = transaction_getTransactionInfo(TRAN_TYPE_RENT);
    tranData.nrlais_kebeleid = rent_holding_data.nrlais_kebeleid;
    tranData.data = rentData;
    rent_combined = tranData;
}

function rent_addRentHolding(holdingUID)
{
    rent_holdingID = holdingUID;
    transaction_map.zoomHolding('nrlais_inventory',holdingUID);
    //show holding information
    $.ajax('/holding/min_holding_search_result.jsp?holding_uid=' + holdingUID, {
        'data': {'method': 'GET'},
        'success': function (data, textStatus, jqXHR) {
            $('#holder_content').html(data);
            $('#pick_person').modal('hide');
        }
    });


    //load holding data from database
    $.ajax('/api/get_holding?schema=nrlais_inventory&holding_uid=' + holdingUID,
            {
                method: 'GET',
                success: function (data)
                {
                    //alert(JSON.stringify(data));
                    rent_holding_data = data.res;
                    var party_done = [];
                    rent_holders = [];
                    rent_holding_data.parcels.forEach(function (parcel)
                    {
                        parcel.rights.forEach(function (right)
                        {
                            if (right.rightType != RIGHT_TYPE_PUR)
                                return;
                            if (party_done[right.partyUID])
                                return;
                            party_done[right.partyUID] = true;
                            rent_holders.push(right);
                        });
                    });
                },
                dataType: 'json'
            });

    //setup applicant table
    if (rent_combined == null)
        applicantPicker.load("#holder_with_representative", holdingUID, null, {pickPOM:true});
    else
    {
        applicantPicker.load("#holder_with_representative", holdingUID, rent_combined.data.applicants, {pickPOM:true});
    }

    //setup parcel tansfer table
   
    var parcelSetting = {
        split: false,
        transferArea: true,
        transferShare: false,
        transferLabel: lang("Rent or Lease"),
        areaLabel: lang("Rented or Leased Area"),
        shareLabel: "Transfered Share"
    }
    if (rent_combined == null)
        parcelTransferTable.load('#holder_parcel', holdingUID, null, parcelSetting);
    else
    {
        parcelTransferTable.load('#holder_parcel', holdingUID, rent_combined.data.transfers, parcelSetting);
    }




    //load required documents

    rent_holdingID = holdingUID;
}
function rent_validateApplicantPage() {
    return applicantPicker.validate(rent_holders);
}
function rent_validateApplicationPage()
{
    return transaction_validateApplication() && validatePageOne();
}
function rent_validateParcelPage(){
    var valid = true;
    var validator = validatorPanel('.parcel-error-panel');
    var parcel = $('#holder_parcel input[field=select]:checked').length > 0;
    if (!parcel) {
        validator.removeError("selectParcel");
        validator.addError("selectParcel", lang("Please select at least one parcel"));
        valid = false;
    } else {
        validator.removeError("selectParcel");
    }
    return valid;
}
function rent_validatePartyPage(){
    var validApplication = true;
    var validator = validatorPanel(".error-tenant-party-panel");
    if (!$.trim($('#rent_tenants tbody').html()).length) {
        validator.removeError("tenantPanel");
        validator.addError("tenantPanel", lang("Please Add Tenant or Lessee Information"));
        validApplication = false;
    } else {
        validator.removeError("tenantPanel");
    }
    return validApplication;
}
function rent_validateAgreementPage(){
    var valid=true;
    var oneValid = true;
    var requiredField=['rent_value','rent_date_start','rent_date_end','rent_agreement_doc_ref'];
    for (var i = 0; i < requiredField.length; i++) {
                oneValid = ($('#' + requiredField[i]).val() != "");
                setValidationStyle($('#' + requiredField[i]), oneValid);
                valid = valid && oneValid;
            }
    return valid;
}
function rent_validateRegDocumentPage(){
     var valid = true;
    var validator = validatorPanel('.error-req-doc-panel');

    if (!documentPicker.getRequiredDocument(0)) {
        validator.removeError("landHolding");
        validator.addError("landHolding", lang("Please add Land Holding Certificate"));
        valid = false;
    } else {
        validator.removeError("landHolding");
    }
    return valid;
}
function rent_SaveToServer()
{
    transaction_saveTransaction(rent_combined, false, function (ternID) {
        bootbox.alert({
            message: lang("You Have Successfully Registered a Transaction!"),
            callback: function () {
                window.location.href = "/dashboard.jsp";
            }
        });
    }, function (err) {
        bootbox.alert(lang("Transaction Registration Failed")+". \n" + err);
    });
}
function rent_showPreview() {
    rent_buildTransactionObject();
    $.ajax({type: 'POST',
        url: "/tran/rent/full_rent_viewer_embeded.jsp",
        contentType: "application/json",
        data: JSON.stringify(rent_combined),
        async: true,
        error: function (errorThrown) {
            bootbox.alert(lang("error"));
        },
        success: function (data) {
            $("#print_slip").html(data);
        }

    });
}
function rent_validateAndGateRentContract()
{
    var ret = {};
    ret.isRent = $("#rent_rent_type").is(":checked");
    ret.leaseFrom = getLongDateFromCal($("#rent_date_start"));
    ret.leaseTo = getLongDateFromCal($("#rent_date_end"));

    ret.amount = parseFloat($("#rent_value").val());
    ret.rentContractDoc =
            {
                docfile: $("#rent_agreement_doc").val(),
                description: $("#rent_agreement_disc").val(),
                refText: $("#rent_agreement_doc_ref").val(),
                fileImage: rent_contract_document_attachment,
                mimeType: rent_contract_document_attachment_mime,
                archiveType: rent_contract_document_attachment == null ? 2 : 1
            };
    return ret;
}
function rent_initRentContract()
{
    if (rent_combined)
    {
        rent_start_date_picker.setDate(new Date(rent_combined.data.leaseFrom));
        rent_end_date_picker.setDate(new Date(rent_combined.data.leaseTo));
        if (rent_combined.data.isRent)
            $("#rent_rent_type").attr("checked", "checked");
        else
            $("#rent_lease_type").attr("checked", "checked");
        $("#rent_value").val(rent_combined.data.amount.toString());
        if (rent_combined.data.rentContractDoc)
        {
            rent_contract_document_attachment = rent_combined.data.rentContractDoc.fileImage;
            rent_contract_document_attachment_mime = rent_combined.data.rentContractDoc.mimeType;
            $("#rent_agreement_disc").val(rent_combined.data.rentContractDoc.description);
            $("#rent_agreement_doc_ref").val(rent_combined.data.rentContractDoc.refText);
        } else
            rent_contract_document = null;
    }
}
function rent_contractDocAttached()
{

    if (attachment.hasAttachment()) {
        rent_contract_document_attachment_mime = attachment.mimeType;
        rent_contract_document_attachment = attachment.imageBytes;
    } else {
        rent_contract_document_attachment = null;
        rent_contract_document_attachment_mime = null
    }
}
function rent_setContractDoc(doc)
{

}
function rent_initRent() {
    //setup wizard
    wizardPages.load([
        {tab: "#step-1-tab", cont: "#step-1", enable: true},
        {tab: "#step-2-tab", cont: "#step-2", enable: true},
        {tab: "#step-3-tab", cont: "#step-3", enable: true},
        {tab: "#step-4-tab", cont: "#step-4", enable: true},
        {tab: "#step-5-tab", cont: "#step-5", enable: true},
        {tab: "#step-6-tab", cont: "#step-6", enable: true},
        {tab: "#step-7-tab", cont: "#step-7", enable: true},
    ]);
    wizardPages.beforePageChange = function (show, hide)
    {
        var ret = true;
        if (hide == "#step-1-tab")
        {
            //ret=rent_validateApplicationPage();   
            ret = rent_validateApplicationPage();
        } else if (hide == "#step-2-tab")
        {
            //ret=rent_validateApplicantPage();
            ret = rent_validateApplicantPage();
        } else if (hide == "#step-3-tab")
        {
            ret = rent_validateParcelPage();//NOTE: no validation for parcel page?
        } else if (hide == "#step-4-tab")
        {
            ret = rent_validatePartyPage();
        } else if (hide == "#step-5-tab") {
            ret = rent_validateAgreementPage();
        } else if (hide == "#step-6-tab") {
            ret = rent_validateRegDocumentPage();
        }
        if (show == "#step-7-tab")
        {
            rent_showPreview();
        }
        return ret;
    }
    $("#app_type").val(lang("Rent or Lease Transcation"));
    app_holdingSearch.onHoldingPicked = rent_addRentHolding;
    
    documentPicker.load("#documentTableBody");

    //load existing data
    if (rent_holdingID != null)
    {
        rent_addRentHolding(rent_holdingID);
    }
    transaction_saveInitial = function ()
    {
        if (!rent_holding_data)
        {
            bootbox.alert(lang("You must at least select holding to save the rent transaction"));
            return;
        }
        rent_buildTransactionObject();
        transaction_saveTransaction(rent_combined, true, function (tranUID) {
            bootbox.alert({
                message: lang("You Have Saved a Transaction Data Temporarily!"),
                callback: function () {
                    window.location.href = "/dashboard.jsp";
                }
            });
        }, function (err) {
            bootbox.alert(lang("Saving Transaction Data Failed")+".\n" + err);
        });
    }

    //initialize rent tenants party list picker
    rent_tenants = getPartyListPicker({
        containerEl: "rent_tenants",
        addButtonEl: "rent_tenant_add",
        share: true,
        idDoc: true,
        edit: true,
        delete: true,
    });
    if (rent_combined)
    {
        rent_tenants.setParyItems(rent_combined.data.tenants);
    }
    //initailize ethiopian dates
    rent_start_date_picker = new EthiopianDualCalendar("#rent_date_start");
    rent_end_date_picker = new EthiopianDualCalendar("#rent_date_end");

    //intialize rent contract section
    rent_initRentContract();

    //load required documents
    if(rent_combined)
    {
        documentPicker.setRequiredDocument(0,rent_combined.data.landHoldingCertificateDoc);
    }

}
