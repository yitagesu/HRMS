<table class="table table-striped">
    <tbody id="">
        <%@ include file="/tran/tran_viewer_header.jsp" %>
        <tr><td colspan="2" class="table_separetor"><label><%=controller.text("Information of Renter/Lessor")%></label></td></tr>
        <%for(LADM.Right r:controller.holding.getHolders()){
            String holderROLE=controller.roleText(r.party.mreg_familyrole);
            String holderSEX=controller.sexText(r.party.sex);
            Applicant ap=controller.partyApplicant(r.party.partyUID);
        %>
        <tr id="final_<%=controller.holding.holdingUID%>" class="holder">
            <td class="many">
                <label><%=holderROLE%> Info</label>
                <ul>
                    <li><label><%=controller.text("Name")%>: </label> <%=r.party.getFullName()%></li>
                    <li><label><%=controller.text("Sex")%>: </label> <%=holderSEX%></li>
                    <li><label><%=controller.text("Age")%>: </label> <span id="ageOfApp"> <%=controller.age(r.party.dateOfBirth)%>  (<%=EthiopianCalendar.ToEth(r.party.dateOfBirth).toString()+" EC"%>)</span></li>
                    <li><label><%=controller.text("Share")%>: </label> <%=r.share.num%>/<%=r.share.denum%></li>
                    <li><label><%=controller.text("ID")%>: </label> <%if(ap.idDocument.fileImage ==null){%>
                        <%=controller.applicantIDTypeText(ap)%> (<%=controller.applicantIDText(ap)%>)
                        <%}else{%>
                        <a href="<%=controller.applicantIDLink(ap)%>"><%=controller.applicantIDTypeText(ap)%> (<%=controller.applicantIDText(ap)%>)</a>
                        <%}%>
                    </li>
                    <li><label><%=controller.text("Proof of Marital Status")%>: </label>  <%if(ap.pomDocument.fileImage ==null){%>
                        <%=controller.applicantPOMText(ap)%> 
                        <%}else{%>
                        <a href="<%=controller.applicantPOMLink(ap)%>"><%=controller.applicantPOMText(ap)%></a>
                        <%}%>
                    </li>
                </ul>
            </td>
            <td class="many">
                <label><%=holderROLE%><%=controller.text("Representative info")%> </label>
                <%
                            String message="";
                            
                            if(ap!=null && ap.representative!=null)
                            {
                                String repSEX="";
                                repSEX=controller.sexText(ap.representative.sex);
                            
            
                %>
                <ul>
                    <li> <label><%=controller.text("Name")%>: </label> <%=ap.representative.getFullName()%></li>
                    <li> <label><%=controller.text("Relationship")%>: </label> <%=ap.relationWithParty%></li>
                    <li> <label><%=controller.text("Age")%>: </label> <span id="ageOfRep"><%=controller.age(ap.representative.dateOfBirth)%>  (<%=EthiopianCalendar.ToEth(ap.representative.dateOfBirth).toString()+" EC"%>)</span></li>
                    <li><label><%=controller.text("Sex")%>: </label> <%=repSEX%></li>
                    <%for(LADM.PartyContact a:ap.representative.contacts){
                        if(a.contactdataType==LADM.PartyContact.CONTACT_TYPE_OTHER){%>
                            <li><label><%=controller.text("Phone")%>: </label> <%=a.contactData%></li>
                        <%}if(a.contactdataType==LADM.PartyContact.CONTACT_TYPE_ADDRESS){%>
                            <li><label><%=controller.text("Address")%>: </label> <%=a.contactData%></li> 
                    <%}}%>
                    <li><label><%=controller.text("Letter of Attorney")%>:</label> <%if(ap.letterOfAttorneyDocument.fileImage ==null){%>
                            <%=controller.letterOfAttroneyText(ap)%>
                        <%}else{%>
                        <a href="<%=controller.letterOfAttroneyLink(ap)%>"><%=controller.letterOfAttroneyText(ap)%></a>
                        <%}%>
                    </li>                       
                </ul>

                <%}else{ message=controller.text("No Representative");%>

                <p><%=message%></p>

                <%}//if-else%>


            </td>
        </tr>
        <%}//for%>
        <tr><td class="table_separetor" colspan="2"><%=controller.text("Information of Tenant/Lessee")%></td></tr>
        <%for(PartyItem t:controller.data.tenants){
            String tenatROLE=controller.roleText(t.party.mreg_familyrole);
            String tenatSEX=controller.sexText(t.party.sex);
        %>
        <tr id="tenat_">
            <td class="many">
                <ul>
                    <li><label><%=controller.text("Name")%>: </label> <%=t.party.getFullName()%></li>
                    <li><label><%=controller.text("Sex")%>: </label> <%=tenatSEX%></li>
                    <li><label><%=controller.text("Age")%>: </label> <%=controller.age(t.party.dateOfBirth)%> (<%=EthiopianCalendar.ToEth(t.party.dateOfBirth).toString()+" EC"%>)</li>
                    <li><label><%=controller.text("Family Role")%>: </label> <%=tenatROLE%></li>
                    <li><label><%=controller.text("Share")%>: </label><%=t.share.num%>/<%=t.share.denum%></li>
                    <li><label><%=controller.text("ID")%>: </label> <%if(t.idDoc.fileImage ==null){%>
                        <%=controller.applicantIDTypeText(t)%> (<%=controller.applicantIDText(t)%>)
                        <%}else{%>
                        <a href="<%=controller.applicantIDLink(t)%>"><%=controller.applicantIDTypeText(t)%> (<%=controller.applicantIDText(t)%>)</a>
                        <%}%>
                    </li>
                    <li><label><%=controller.text("Proof of Marital Status")%>: </label>  <%if(t.proofOfMaritalStatus.fileImage ==null){%>
                        <%=controller.applicantPOMText(t)%> 
                        <%}else{%>
                        <a href="<%=controller.applicantPOMLink(t)%>"><%=controller.applicantPOMText(t)%></a>
                        <%}%>
                    </li>
                </ul>
            </td>
            <td></td>
        </tr>
        <%}%>
        <tr><td colspan="2" class="table_separetor"><label><%=controller.text("Rent/Lease Agreement Details")%></label> </td></tr>

        <tr>
            <td class="many" colspan="2">
                <%for(ParcelTransfer p:controller.data.transfers){
                LADM.Parcel pr=controller.transferParcel(p);
                %>
                <h6><%=controller.text("Parcel Details")%></h6>
                <ul>
                    <li><label><%=controller.text("Parcel No")%>: </label> <%=pr.seqNo%></li>
                    <li><label><%=controller.text("Area")%>: </label> <%=pr.areaGeom%> M<sup>2</sup></li>
                        <% String rentArea="";
                            if(pr.areaGeom == p.area){
                                rentArea=controller.text("Full");
                            }
                            else{
                                rentArea=""+p.area+" "+controller.text("M")+"<sup>2</sup>";
                            }
                        %>
                    <li><label><%=controller.text("Rented Area")%>: </label> <%=rentArea%> <hr></li>
                </ul>
                <%}//for parcel%>
                <h6><%=controller.text("Contract Details")%></h6>
                <ul>
                    <% String rentType="";
                        if(controller.data.isRent==true){
                            rentType= controller.text("Rent");
                        }
                        else{
                            rentType= controller.text("Lease");                        }
                    %>
                    <li><label><%=controller.text("Rent Type")%>: </label> <%=rentType%></li>
                    <li><label><%=controller.text("Total Rent value")%>: </label> <%=controller.data.amount%></li>
                    <li><label><%=controller.text("Rent/Lease Start Date")%>: </label> <%=EthiopianCalendar.ToEth(controller.data.leaseFrom).toString()+" EC"%></li>
                    <li><label><%=controller.text("Rent/Lease End Date")%>: </label> <%=EthiopianCalendar.ToEth(controller.data.leaseTo).toString()+" EC"%></li>
                    <li><label><%=controller.text("Document Description")%>: </label>  <%=controller.documentDescription(controller.data.rentContractDoc)%></li>
                </ul>
            </td>
        </tr>
        
        <tr><td colspan="2" class="table_separetor"><label><%=controller.text("Document Provides")%></label></td></tr>
        <tr>
            <td colspan="2">
                <ol>
                    <li><%=controller.text("Rent/Lease Contract")%>: (<a href="<%=controller.showDocumentLink(controller.data.rentContractDoc)%>"><%=controller.showDocumentText(controller.data.rentContractDoc)%></a>)</li>
                    <li><%=controller.text("Land Holding Certificate")%>: (<a href="<%=controller.showDocumentLink(controller.data.landHoldingCertificateDoc)%>"><%=controller.showDocumentText(controller.data.landHoldingCertificateDoc)%></a>)</li>
                    <%for(SourceDocument doc:controller.data.otherDocument){%>
                        <li><%=controller.text(controller.adminSourceText(DocumentTypeInfo.DOC_TYPE_OTHER))%>: <%=doc.description%> (<%=doc.refText%>)</li>
                    <%}%>
                </ol>
            </td>
        </tr>
    </tbody>

</table>

