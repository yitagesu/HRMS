/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function exchange_init(data)
{
    var kebeleID;
    var holdingUID1;
    var holdingUID2;
    var applicants1;
    var applicants2;

    var parcels1;
    var parcels2;

    function buildTransactionObject()
    {
        var exchangeData = {};


        exchangeData.holdingUID1 = holdingUID1;
        exchangeData.holdingUID2 = holdingUID2;

        exchangeData.parcelUID1 = [];
        exchangeData.parcelUID2 = [];

        for (var i in parcels1)
        {
            exchangeData.parcelUID1.push(parcels1[i].parcelUID);
        }
        for (var i in parcels2)
        {
            exchangeData.parcelUID2.push(parcels2[i].parcelUID);
        }
        exchangeData.applicants1 = applicants1;
        exchangeData.applicants2 = applicants2;

        exchangeData.claimResolutionDocument = documentPicker.getRequiredDocument(0);
        exchangeData.landHoldingCertifcateDocument1 = documentPicker.getRequiredDocument(1);
        exchangeData.landHoldingCertifcateDocument2 = documentPicker.getRequiredDocument(2);
        exchangeData.otherDocument = documentPicker.getAllAddtionalDocument();

        var tranData = transaction_getTransactionInfo(TRAN_TYPE_EXCHANGE);
        tranData.nrlais_kebeleid = kebeleID;
        tranData.data = exchangeData;
        return tranData;
    }
    function configureApplicantPicker(sel, holdingUID, applicants)
    {

        var setting = {
            pickPOM: true,
            pickShare: false,
            selectable: false,
        };
        applicantPicker.load(sel, holdingUID, applicants, setting);
    }
    function configureParcelTransferTable(sel, holdingUID, parcels)
    {
        //setup parcel tansfer table
        var setting = {
            split: false,
            transferArea: false,
            transferShare: false,
            transferLabel: lang("Select"),
        }

        parcelTransferTable.load(sel, holdingUID, parcels, setting);

    }

    function setHoldingUID2(holdingUID)
    {
        holdingUID2 = holdingUID;
        transaction_map.zoomHolding('nrlais_inventory', holdingUID);

        //show holding information
        $.ajax('/holding/min_holding_search_result.jsp?holding_uid=' + holdingUID, {
            'data': {'method': 'GET'},
            'success': function (data, textStatus, jqXHR) {
                $('#exchange_exiting_holding').html(data);
                $('#pick_person').modal('hide');
            }
        });
        configureApplicantPicker("#exchange_applicant2", holdingUID2, applicants2);
    }
    function setHoldingUID1(holdingUID)
    {
        holdingUID1 = holdingUID;
        transaction_map.zoomHolding('nrlais_inventory', holdingUID);
        //show holding information
        $.ajax('/holding/min_holding_search_result.jsp?holding_uid=' + holdingUID, {
            'data': {'method': 'GET'},
            'success': function (data, textStatus, jqXHR) {
                $('#holder_content').html(data);
                $('#pick_person').modal('hide');
            }
        });


        //load holding data from database
        $.ajax('/api/get_holding?schema=nrlais_inventory&holding_uid=' + holdingUID,
                {
                    method: 'GET',
                    success: function (data)
                    {
                        kebeleID = data.res.nrlais_kebeleid;
                    },
                    dataType: 'json'
                });


        //setup applicant table
        if (data)
        {
            applicants1 = data.data.applicants1;
            applicants2 = data.data.applicants2;
        } else
        {
            applicants1 = null;
            applicants2 = null;
        }
    }

    function validateApplicationPage()
    {
        return transaction_validateApplication() && validatePageOne();
    }
    function exchange_validateHolder1Page() {
        return applicantPicker.validate(applicants1);
    }
    function exchange_validateHolder2Page() {
    var validApplication = true;
    var validator = validatorPanel(".Holder2_error");
    if (!$.trim($('#exchange_applicant2 tbody').html()).length) {
        validator.removeError("holder2");
        validator.addError("holder2", lang("Please Load 2nd Holding Parcel"));
        validApplication = false;
    } else {
        validator.removeError("holder2");
        return applicantPicker.validate(applicants2);
    }
    return validApplication;
        
    }
    function exchange_validateParcel1Page() {
        var valid = true;
        var validator = validatorPanel('.parcel1-error-panel');
        var parcel = $('#exchange_parcels1  input[field=select]:checked').length > 0;
        if (!parcel) {

            validator.addError("selectParcel1", lang("Please select at least one parcel"));
            valid = false;
        } else {
            validator.removeError("selectParcel1");
        }
        return valid;
    }
    function exchange_validateParcel2Page() {
        var valid = true;
        var validator = validatorPanel('.parcel2-error-panel');
        var parcel = $('#exchange_parcels2  input[field=select]:checked').length > 0;
        if (!parcel) {

            validator.addError("selectParcel2", lang("Please select at least one parcel"));
            valid = false;
        } else {
            validator.removeError("selectParcel2");
        }
        return valid;
    }
    function exchange_validateReqDocumentPage(){
        var valid = true;
    if (!documentPicker.getRequiredDocument(0)) {
        $(".error-req-doc-panel #cliamResolution").remove();
        $(".error-req-doc-panel").append("<span id='cliamResolution'><i class='fa fa-warning faa-flash animated'></i>"+lang("Please Add Elders committee or Woreda court statement on claim resolution")+"<br/></span>");
        valid = false;
    } else {
        $(".error-req-doc-panel #cliamResolution").remove();
    }
    if (!documentPicker.getRequiredDocument(1)) {
        $(".error-req-doc-panel #landHoldingCertificate1").remove();
        $(".error-req-doc-panel").append("<span id='landHoldingCertificate1'><i class='fa fa-warning faa-flash animated'></i>"+lang("Please Add First Holding Land Holding Certificate")+"<br/></span>");
        valid = false;
    } else {
        $(".error-req-doc-panel #landHoldingCertificate1").remove();
    }
    if (!documentPicker.getRequiredDocument(2)) {
        $(".error-req-doc-panel #landHoldingCertificate2").remove();
        $(".error-req-doc-panel").append("<span id='landHoldingCertificate2'><i class='fa fa-warning faa-flash animated'></i>"+lang("Please Add Second Holding Land Holding Certificate")+"<br/></span>");
        valid = false;
    } else {
        $(".error-req-doc-panel #landHoldingCertificate2").remove();
    }
    return valid;
    }
    function saveToServer()
    {
        data = buildTransactionObject();
        transaction_saveTransaction(data, false, function (ternID) {
            bootbox.alert({
                message: lang("You Have Successfully Registered a Transaction")+"!",
                callback: function () {
                    window.location.href = "/dashboard.jsp";
                }
            });
        }, function (err) {
            bootbox.alert(lang("Transaction Registration Failed")+".\n" + err);
        });
    }
    function showPreview() {
        var tran = buildTransactionObject();
        $.ajax({type: 'POST',
            url: "/tran/exchange/full_exchange_viewer_embeded.jsp",
            contentType: "application/json",
            data: JSON.stringify(tran),
            async: true,
            error: function (errorThrown) {
                alert("error");
            },
            success: function (data) {
                var iframe = $('iframe#printSlip').contents();
                iframe.find('body').html(data);
                //iframe.find('head').append('<script src="assets/JS/document_picker.js"></script> ');
                $('#printSlip').attr('height', document.getElementById("printSlip").contentWindow.document.body.scrollHeight +"px");
            }

        });
    }

    function configureTabPages()
    {

        wizardPages.buildNavHtml("#tab-buttons", [
            {tab: "#step-app-tab", cont: "#step-app", enable: true},
            {tab: "#step-holding1-applicants-tab", cont: "#step-holding1-applicants", enable: true},
            {tab: "#step-parcel1-tab", cont: "#step-parcel1", enable: true},
            {tab: "#step-holding2-tab", cont: "#step-holding2", enable: true},
            {tab: "#step-parcel2-tab", cont: "#step-parcel2", enable: true},
            {tab: "#step-docs-tab", cont: "#step-docs", enable: true},
            {tab: "#step-finish-tab", cont: "#step-finish", enable: true},
        ]);
        wizardPages.beforePageChange = function (show, hide)
        {
            var ret = true;

            if (hide == "step-app-tab") {
                ret = validateApplicationPage();
            } else if (hide == "#step-holding1-applicants-tab")
            {

                applicants1 = applicantPicker.getApplicants();
                ret = exchange_validateHolder1Page();
            } else if (hide == "#step-holding2-tab")
            {
                
                applicants2 = applicantPicker.getApplicants();
                ret = exchange_validateHolder2Page();
               
            } else if (hide == "#step-parcel1-tab")
            {
                parcels1 = parcelTransferTable.getTransfers();
                ret=exchange_validateParcel1Page();

            } else if (hide == "#step-parcel2-tab")
            {
                parcels2 = parcelTransferTable.getTransfers();
                ret=exchange_validateParcel2Page();
            }
            else if(hide=="#step-docs-tab"){
                ret=exchange_validateReqDocumentPage();
            }


            if (show == "#step-holding1-applicants-tab")
            {
                configureApplicantPicker("#exchange_applicant1", holdingUID1, applicants1);
                ret = validateApplicationPage();
            } else if (show == "#step-holding2-tab")
            {
                app_holdingSearch.onHoldingPicked = setHoldingUID2;
                configureApplicantPicker("#exchange_applicant2", holdingUID2, applicants2);
                ret=exchange_validateParcel1Page();
            } else if (show == "#step-parcel1-tab")
            {
                configureParcelTransferTable("#exchange_parcels1", holdingUID1, parcels1);
                ret = exchange_validateHolder1Page();

            } else if (show == "#step-parcel2-tab")
            {
                configureParcelTransferTable("#exchange_parcels2", holdingUID2, parcels2);
                ret = exchange_validateHolder2Page();
            } else if (show == "#step-app-tab")
            {
                if (validatePageOne()) {
                    app_holdingSearch.onHoldingPicked = setHoldingUID1;
                } else {
                    ret = validatePageOne();
                }

            } else if (show == "#step-finish-tab") {
                showPreview();
            }


            return ret;
        }
    }

    //initialization code

    configureTabPages();
    $("#app_type").val(lang("Exchange Transaction"));
    $("#exchange_save_btn").click(function () {
        saveToServer();
    });
    documentPicker.load("#documentTableBody");
    app_holdingSearch.onHoldingPicked = setHoldingUID1;
    if (data)
    {
        holdingUID1 = data.data.holdingUID1;
        holdingUID2 = data.data.holdingUID2;
        parcels1 = [];
        for (var i in data.data.parcelUID1)
            parcels1.push({parcelUID: data.data.parcelUID1[i]});

        parcels2 = [];
        for (var i in data.data.parcelUID2)
            parcels2.push({parcelUID: data.data.parcelUID2[i]});

        applicants1 = data.data.applicants1;
        applicants2 = data.data.applicants2;
        documentPicker.setRequiredDocument(0, data.data.claimResolutionDocument);
        documentPicker.setRequiredDocument(1, data.data.landHoldingCertifcateDocument1);
        documentPicker.setRequiredDocument(2, data.data.landHoldingCertifcateDocument2);
        if (data.data.otherDocument.length > 0) {
            documentPicker.setAddtionalDocument(data.data.otherDocument);
        }
        setHoldingUID1(holdingUID1);
    }


    transaction_saveInitial = function ()
    {
        if (!holdingUID1)
        {
            bootbox.alert(lang('You must at least select holding to save the rent transaction'));
            return;
        }
        transaction_saveTransaction(buildTransactionObject(), true, function (tranUID) {
            bootbox.alert({
                message: lang("You Have Saved a Transaction Data Temporarily")+"!",
                callback: function () {
                    window.location.href = "/dashboard.jsp";
                }
            });
        }, function (err) {
            bootbox.alert(lang("Saving Transaction Data Failed")+".\n" + err);
        });
    }

}
