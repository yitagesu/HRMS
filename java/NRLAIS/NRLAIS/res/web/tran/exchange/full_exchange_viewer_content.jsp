<table class="table table-striped">
    <tbody id="">
        <%@ include file="/tran/tran_viewer_header.jsp" %>
        <tr><td class="table_separetor" colspan="2"><label><%=controller.text("First Holding Holder Information")%></label></td></tr>
        <%for(LADM.Right r:controller.holding1.getHolders()){
            String holderROLE=controller.roleText(r.party.mreg_familyrole);
            String holderSEX=controller.sexText(r.party.sex);
            Applicant ap=controller.partyApplicant(r.party.partyUID);
        %>
        <tr id="final_<%=controller.holding1.holdingUID%>" class="holder">
            <td class="many">
                <label><%=holderROLE%> <%=controller.text("Info")%> </label>
                <ul>
                    <li><label><%=controller.text("Name")%>: </label> <%=r.party.getFullName()%></li>
                    <li><label><%=controller.text("Sex")%>: </label> <%=holderSEX%></li>
                    <li><label><%=controller.text("Age")%>: </label> <span id="ageOfApp"><%=controller.age(r.party.dateOfBirth)%> (<%=EthiopianCalendar.ToEth(r.party.dateOfBirth).toString()+" "+controller.text("EC")%>)</span></li>
                    <li><label><%=controller.text("Share")%>: </label> <%=r.share.num%>/<%=r.share.denum%></li>
                    <li><label><%=controller.text("ID")%>: </label> <%if(ap.idDocument.fileImage ==null){%>
                        <%=controller.applicantIDTypeText(ap)%> (<%=controller.applicantIDText(ap)%>)
                        <%}else{%>
                        <a href="<%=controller.applicantIDLink(ap)%>"><%=controller.applicantIDTypeText(ap)%> (<%=controller.applicantIDText(ap)%>)</a>
                        <%}%>
                    </li>
                    <li><label><%=controller.text("Proof of Martial Status")%>: </label> <%if(ap.pomDocument.fileImage ==null){%>
                        <%=controller.applicantPOMText(ap)%>
                        <%}else{%>
                        <a href="<%=controller.applicantPOMLink(ap)%>"><%=controller.applicantPOMText(ap)%></a>
                        <%}%>
                    </li>
                </ul>
            </td>
            <td class="many">
                <label><%=holderROLE%> <%=controller.text("Representative info")%> </label>
                <%
                            String message="";
                            
                            if(ap!=null && ap.representative!=null)
                            {
                                String repSEX="";
                                repSEX=controller.sexText(ap.representative.sex);
                            
            
                %>
                <ul>
                    <li> <label><%=controller.text("Name")%>: </label> <%=ap.representative.getFullName()%></li>
                    <li> <label><%=controller.text("Relationship")%>: </label> <%=ap.relationWithParty%></li>
                    <li> <label><%=controller.text("Age")%>: </label> <span id="ageOfRep"><%=controller.age(ap.representative.dateOfBirth)%> (<%=EthiopianCalendar.ToEth(ap.representative.dateOfBirth).toString()+" "+controller.text("EC")%>)</span></li>
                    <li><label><%=controller.text("Sex")%>: </label> <%=repSEX%></li>
                    <%for(LADM.PartyContact a:ap.representative.contacts){
                        if(a.contactdataType==LADM.PartyContact.CONTACT_TYPE_OTHER){%>
                            <li><label><%=controller.text("Phone")%>: </label> <%=a.contactData%></li>
                        <%}if(a.contactdataType==LADM.PartyContact.CONTACT_TYPE_ADDRESS){%>
                            <li><label><%=controller.text("Address")%>: </label> <%=a.contactData%></li> 
                    <%}}%>
                    <li><label><%=controller.text("Letter of Attorney")%>:</label> <%if(ap.letterOfAttorneyDocument.fileImage ==null){%>
                            <%=controller.letterOfAttroneyText(ap)%>
                        <%}else{%>
                        <a href="<%=controller.letterOfAttroneyLink(ap)%>"><%=controller.letterOfAttroneyText(ap)%></a>
                        <%}%>
                    </li>                  
                </ul>

                <%}else{ message=controller.text("No Representative");%>

                <p><%=message%></p>

                <%}//if-else%>


            </td>
        </tr>
        <%}//for%>
        <tr><td colspan="2" class="table_separetor"><label><%=controller.text("First Holding Parcel Ready for Exchange")%></label> </td></tr>
        <%for(String p:controller.data.parcelUID1){
            LADM.Parcel par=controller.exchangeParcel(p);
        %>
        <tr>
            <td class="many" colspan="2">
                <ul>
                    <li><label><%=controller.text("Parcel No")%>: </label> <%=par.seqNo%></li>
                    <li><label><%=controller.text("Area")%>: </label> <%=par.areaGeom%> <%=controller.text("M")%><sup>2</sup></li>
                    
                </ul>
            </td>
        </tr>
        <%}//for%>
                <tr><td class="table_separetor" colspan="2"><label><%=controller.text("Second Holding Holder Information")%></label></td></tr>
        <%for(LADM.Right r:controller.holding2.getHolders()){
            String holderROLE=controller.roleText(r.party.mreg_familyrole);
            String holderSEX=controller.sexText(r.party.sex);
            Applicant ap=controller.partyApplicant(r.party.partyUID);
        %>
        <tr id="final_<%=controller.holding2.holdingUID%>" class="holder">
            <td class="many">
                <label><%=holderROLE%> <%=controller.text("Info")%></label>
                <ul>
                    <li><label><%=controller.text("Name")%>: </label> <%=r.party.getFullName()%></li>
                    <li><label><%=controller.text("Sex")%>: </label> <%=holderSEX%></li>
                    <li><label><%=controller.text("Age")%>: </label> <span id="ageOfApp"><%=controller.age(r.party.dateOfBirth)%> (<%=EthiopianCalendar.ToEth(r.party.dateOfBirth).toString()+" "+controller.text("EC")%>)</span></li>
                    <li><label><%=controller.text("Share")%>: </label> <%=r.share.num%>/<%=r.share.denum%></li>
                    <li><label><%=controller.text("ID")%>: </label>  <%if(ap.idDocument.fileImage ==null){%>
                        <%=controller.applicantIDTypeText(ap)%> (<%=controller.applicantIDText(ap)%>)
                        <%}else{%>
                        <a href="<%=controller.applicantIDLink(ap)%>"><%=controller.applicantIDTypeText(ap)%> (<%=controller.applicantIDText(ap)%>)</a>
                        <%}%>
                    </li>
                    <li><label><%=controller.text("Proof of Martial Status")%>: </label> <%if(ap.pomDocument.fileImage ==null){%>
                        <%=controller.applicantPOMText(ap)%>
                        <%}else{%>
                        <a href="<%=controller.applicantPOMLink(ap)%>"><%=controller.applicantPOMText(ap)%></a>
                        <%}%></li>
                </ul>
            </td>
            <td class="many">
                <label><%=holderROLE%> <%=controller.text("Representative info")%></label>
                <%
                            String message="";
                            
                            if(ap!=null && ap.representative!=null)
                            {
                                String repSEX="";
                                repSEX=controller.sexText(ap.representative.sex);
                            
            
                %>
                <ul>
                    <li> <label><%=controller.text("Name")%>: </label> <%=ap.representative.getFullName()%></li>
                    <li> <label><%=controller.text("Relationship")%>: </label> <%=ap.relationWithParty%></li>
                    <li> <label><%=controller.text("Age")%>: </label> <span id="ageOfRep"><%=controller.age(ap.representative.dateOfBirth)%> (<%=EthiopianCalendar.ToEth(ap.representative.dateOfBirth).toString()+" "+controller.text("EC")%>)</span></li>
                    <li><label><%=controller.text("Sex")%>: </label> <%=repSEX%></li>
                    <%for(LADM.PartyContact a:ap.representative.contacts){
                        if(a.contactdataType==LADM.PartyContact.CONTACT_TYPE_OTHER){%>
                            <li><label><%=controller.text("Phone")%>: </label> <%=a.contactData%></li>
                        <%}if(a.contactdataType==LADM.PartyContact.CONTACT_TYPE_ADDRESS){%>
                            <li><label><%=controller.text("Address")%>: </label> <%=a.contactData%></li> 
                    <%}}%>
                    <li><label><%=controller.text("Letter of Attorney")%>:</label> <%if(ap.letterOfAttorneyDocument.fileImage ==null){%>
                            <%=controller.letterOfAttroneyText(ap)%>
                        <%}else{%>
                        <a href="<%=controller.letterOfAttroneyLink(ap)%>"><%=controller.letterOfAttroneyText(ap)%></a>
                        <%}%>
                    </li>                      
                </ul>

                <%}else{message=controller.text("No Representative");%>

                <p><%=message%></p>

                <%}//if-else%>


            </td>
        </tr>
        <%}//for%>
         <tr><td colspan="2" class="table_separetor"><label><%=controller.text("Second Holding Parcel Ready for Exchange")%></label> </td></tr>
        <%for(String p:controller.data.parcelUID2){
            LADM.Parcel par=controller.exchangeParcel(p);
        %>
        <tr>
            <td class="many" colspan="2">
                <ul>
                    <li><label><%=controller.text("Parcel No")%>: </label> <%=par.seqNo%></li>
                    <li><label><%=controller.text("Area")%>: </label> <%=par.areaGeom%> <%=controller.text("M")%><sup>2</sup></li>
                    
                </ul>
            </td>
        </tr>
        <%}//for%>
        <tr><td colspan="2" class="table_separetor"><label><%=controller.text("Document Provides")%></label></td></tr>
        <tr>
            <td colspan="2">
                <ol>
                    <li><%=controller.adminSourceText(DocumentTypeInfo.DOC_TYPE_DIVORCE_CLAIM_RESOLUTION)%> (<a href="<%=controller.claimResolutionLink()%>"><%=controller.claimResolutionText()%></a>)</li>
                    <li><%=controller.adminSourceText(DocumentTypeInfo.DOC_TYPE_LAND_HOLDING_CERTIFICATE)%> <%=controller.text("first holding")%>(<a href="<%=controller.landHoldingCertifcateLink1()%>"><%=controller.landHoldingCertifcateText1()%></a>)</li>
                    <li><%=controller.adminSourceText(DocumentTypeInfo.DOC_TYPE_LAND_HOLDING_CERTIFICATE)%> <%=controller.text("second holding")%>(<a href="<%=controller.landHoldingCertifcateLink2()%>"><%=controller.landHoldingCertifcateText2()%></a>)</li>
                    <%for(SourceDocument doc:controller.data.otherDocument){%>
                        <li><%=controller.adminSourceText(DocumentTypeInfo.DOC_TYPE_OTHER)%>:
                                <%if(doc.fileImage==null){%>
                         <%=doc.description%> (<%=doc.refText%>)
                         <%}else{%>
                         <a href="<%=controller.showDocumentLink(doc)%>"><%=doc.description%> (<%=doc.refText%>)</a>
                         <%}%>
                    </li>
                    <%}%>
                </ol>
            </td>
        </tr>
    </tbody>
</table>
