/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function adjacentParcelsPicker(containerEl)
{

    var state = {
        parcels: null,
        getParcels: function ()
        {
            return this.parcels;
        },
        getSelectd: function ()
        {
            var ret = [];
            $(containerEl + " tbody tr").each(function ()
            {
                if ($(this).find("input").is(":checked"))
                    ret.push($(this).attr("parcel_uid"));
            });
            return ret;
        },
        parcelObject: function (parcelUID)
        {
            for (var i in this.parcels)
            {
                if (this.parcels[i].parcelUID == parcelUID)
                    return this.parcels[i];
            }
            return null;
        },
        tr: function (parcelUID)
        {
            return $(containerEl + " tr[parcel_uid=" + parcelUID + "] ");
        },
        seqNo: function (parcelUID)
        {
            return this.tr(parcelUID).find("td").eq(0).text();
        },
        setSelected: function (selected)
        {
            for (var i in selected)
            {
                this.tr(selected[i]).find("input").attr("checked", "checked");
            }
        },
        isSelected: function (parcelUID)
        {
            var selected = $(containerEl + " tbody tr[parcel_uid='" + parcelUID + "'] input").is(':checked');
            return selected;
        },
        load: function (parcelUID, selected)
        {
            var that = this;
            if (parcelUID == null)
            {
                $(containerEl).html("Please select a parcel");
            } else
            {
                $.ajax({url: '/api/get_adj_parcel?schema=nrlais_inventory&parcel_uid=' + parcelUID,
                    dataType: 'json',
                    success: function (data)
                    {
                        if (data.error == null)
                        {
                            that.parcels = data.res;
                            fetchEJSTemplat('/parcel/adjacent_parcels_template.ejs',
                                    {controller: that},
                                    function (html) {
                                        $(containerEl).html(html);
                                        that.setSelected(selected)
                                    }
                            );
                        } else
                            console.log('Could not load parcel data:', data.error)
                    }
                });
            }
        }
    }
    return state;
}