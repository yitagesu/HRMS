/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var parcelPicker = {
    instances: [],
    validateAndSaveParcel: function (containerEl) {
        this.instances[containerEl].validateAndSave();
    }
};
function getParcelPicker(setting)
{
    if (parcelPicker.instances[setting.conatinerEl])
    {
        ready(parcelPicker.instances[setting.conatinerEl]);
        return;
    }
    var state = {
        parcelEditors: [],
        onParcelSave: null,
        setting: setting,
        surveyDate: null,
        areaGeom: 0,
        seqNo: 0,
        parcelUID: null,
        beforeSave: null,
        showModal: function (data)
        {
            var that = this;
            if (data == null)
            {
                $.ajax({
                    url: '/parcel/register_parcel_modal.jsp?setting=' + encodeURIComponent(JSON.stringify(setting)),
                    success: function (html)
                    {
                        $("#" + setting.containerEl).html(html);
                        that.surveyDate = new EthiopianDualCalendar("#" + setting.containerEl + " #parcel_surveyDate");
                        $("#" + setting.containerEl + '_modal').modal("show");
                    }
                });
            } else
            {
                this.areaGeom = data.parcel.areaGeom;
                this.seqNo = data.parcel.seqNo;
                this.parcelUID = data.parcel.parcelUID;
                $.ajax({
                    url: '/parcel/register_parcel_modal.jsp?setting=' + encodeURIComponent(JSON.stringify(setting)),
                    method: "POST",
                    data: JSON.stringify(data),
                    contentType: "application/json",
                    success: function (html)
                    {
                        $("#" + setting.containerEl).html(html);
                        that.surveyDate = new EthiopianDualCalendar("#" + setting.containerEl + " #parcel_surveyDate");
                        $("#" + setting.containerEl + '_modal').modal("show");
                    }
                });
            }
        },
        load: function ()
        {

        },
        saveParcel: function ()
        {
            var p = this.getParcelData();
            if (this.beforeSave)
            {
                var that=this;
                this.beforeSave(p, function (success)
                {
                    if (success)
                    {
                        if (that.onParcelSave)
                            that.onParcelSave(p);
                        $("#" + setting.containerEl + '_modal').modal("hide");
                    }
                });
            } else
            {
                if (this.onParcelSave)
                    this.onParcelSave(p);
                $("#" + setting.containerEl + '_modal').modal("hide");
            }

        },
        validateAndSave: function ()
        {
            var container = $("#" + setting.containerEl);
            var valid = true;
            var oneValid = true;
            var requiredField = ['parcel_orthograph', 'parcel_meansAcquisition', 'parcel_cLandUse', 'parcel_surveyDate', 'parcel_acquisitionYear'];
            for (var i = 0; i < requiredField.length; i++) {
                oneValid = (container.find('#' + requiredField[i]).val() != "");
                setValidationStyle(container.find('#' + requiredField[i]), oneValid);
                valid = valid && oneValid;
            }
            if (valid)
            {
                this.saveParcel();
            }
        }, //function validateAndSave        
        getParcelData: function () {

            var container = $("#" + setting.containerEl);
            var date = new Date(container.find('#parcel_surveyDate').attr("dateval"));
            var parcel =
                    {
                        parcelUID: this.parcelUID,
                        areaGeom: this.areaGeom,
                        seqNo: this.seqNo,
                        landUse: container.find('#parcel_cLandUse').val(),
                        mreg_actype: container.find('#parcel_meansAcquisition').val(),
                        mreg_acyear: container.find("#parcel_acquisitionYear").val(),
                        soilfertilityType: container.find('#parcel_soilFertility').val(),
                        mreg_surveyDate: date.getTime(),
                        mreg_teamid: container.find('#parcel_team_info').val(),
                        mreg_mapsheetNo: container.find('#parcel_orthograph').val()
                    };
            return {parcel: parcel};
        }//function getPartyData
    };
    state.load();
    parcelPicker.instances[setting.containerEl] = state;
    return state;
}

