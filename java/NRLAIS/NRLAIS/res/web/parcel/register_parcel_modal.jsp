<%@page import="com.intaps.nrlais.repo.*"%>
<%@page import="com.intaps.nrlais.*"%>
<%@page import="com.intaps.nrlais.model.*"%>
<%@page import="com.intaps.nrlais.controller.*"%>
<%@page import="com.intaps.nrlais.controller.tran.*"%>
<%@page import="java.util.*"%>
<%@page pageEncoding="UTF-8" %>
<%
    ParcelViewController controller=new ParcelViewController(request,response);
%>
<div id="<%=controller.setting.containerEl%>_modal"  class="modal fade " tabindex="0" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4><%=controller.text("Parcel Information")%></h4>

            </div>            <!-- /modal-header -->
            <div class="modal-body">
                <%@include  file="register_parcel_content.jsp"%>
            </div>
            <div class="modal-footer">
                <div class="pull-left parcel-error-panel">

                </div>
                <div class="form-group pull-right">
                    <button class="btn btn-nrlais" type="button" onclick="parcelPicker.validateAndSaveParcel('<%=controller.setting.containerEl%>')"><%=controller.saveButtonLabel()%></button>
                </div>
            </div>
        </div>
    </div>      
</div>


