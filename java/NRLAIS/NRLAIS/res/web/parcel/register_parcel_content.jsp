<form id="parcel_form">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group col-md-6">
                <label><%=controller.text("Team ID")%></label>
                <input type="text" id="parcel_team_info" class="form-control" value="<%=controller.parcel.parcel.mreg_teamid%>">
            </div>
            <div class="form-group col-md-6">
                <label><%=controller.text("OrthophotoMap Sheet No")%></label>
                <input type="text" class="form-control" id="parcel_orthograph" value="<%=controller.parcel.parcel.mreg_mapsheetNo%>"/>
            </div>
        </div> 
        <div class="col-md-12">
            <div class="form-group col-md-6">
                <label><%=controller.text("Means of Acquisition")%></label>
                <select class="form-control" id="parcel_meansAcquisition">
                    <option value=""><%=controller.text("Please Select Value")%></option>
                    <%for(IDNameText a:controller.meansOfAcquisition()){%>
                    <option value="<%=a.id%>" <%=controller.parcel.parcel.mreg_actype==a.id?"selected":""%>><%=a.name%></option>
                    <%}%>
                </select>
            </div>
            <div class="form-group col-md-6">
                <label><%=controller.text("Acquisition Year")%> </label>
                <select class="form-control" id="parcel_acquisitionYear">
                    <option value=""><%=controller.text("Please Select Value")%></option>
                    <%
                        for (int i = 1900; i <= 2016; i++) {%>
                        <option value = '<%=i%>' <%=controller.parcel.parcel.mreg_acyear==i?"selected":""%>><%=i%></option>
                        <%}
                    %>
                </select>           
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group col-md-6">
                <label><%=controller.text("Current Land Use")%></label>
                <select class="form-control" id="parcel_cLandUse" >
                    <option value=""><%=controller.text("Please Select Value")%></option>
                    <%for(IDNameText l:controller.landUse()){%>
                    <option value="<%=l.id%>" <%=controller.parcel.parcel.landUse==l.id?"selected":""%>><%=l.name%></option>
                    <%}%>
                </select>
            </div>
            <div class="form-group col-md-6">
                <label><%=controller.text("Soil Fertility")%></label>
                <select class="form-control" id="parcel_soilFertility">
                    <option value=""><%=controller.text("Please Select Value")%></option>  
                    <%for(IDNameText s:controller.soilFertiltiy()){%>
                    <option value="<%=s.id%>" <%=controller.parcel.parcel.soilfertilityType==s.id?"selected":""%>><%=s.name%></option>
                    <%}%>
                </select>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group col-md-6">
                <label><%=controller.text("Survey Date")%></label>
                <input type="text" class="form-control" id="parcel_surveyDate" dateval="<%=controller.surveyDate()%>"/>
            </div>
        </div>
    </div>    
</form>