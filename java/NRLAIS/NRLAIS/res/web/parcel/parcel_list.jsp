<%@page import="com.intaps.nrlais.repo.*"%>
<%@page import="com.intaps.nrlais.*"%>
<%@page import="com.intaps.nrlais.model.*"%>
<%@page import="com.intaps.nrlais.model.tran.*"%>
<%@page import="com.intaps.nrlais.controller.*"%>
<%@page import="com.intaps.nrlais.controller.tran.*"%>
<%@page import="java.util.*"%>
<%@page pageEncoding="UTF-8" %>
<%
    ParcelListViewController listController=new ParcelListViewController(request,response);
%>
<table  class="table table-striped jambo_table">
    <thead>
        <tr>
            <% if(listController.setting.select){%>
            <td><%=listController.text("Select")%></td>
            <%}%>
            <td ><%=listController.text("Seq No")%></td>
            <td ><%=listController.text("Area")%></td>
            <td ><%=listController.text("Land Use")%></td>
            <td><%=listController.text("Acquisition Type")%></td>
            <td><%=listController.text("Acquisition Year")%></td>
            <%if(listController.setting.edit||listController.setting.delete){%>
            <td>    
            </td>
            <%}%>
            <% if(listController.setting.mapLink){%>
            <td></td>
            <%}%>
        </tr>
    </thead>
    <tbody id="<%=listController.setting.containerEl%>_parcel_list">                    
        <%for(ParcelItem item:listController.items()){%>
        <tr>
            <td><%=listController.itemSeqNo(item)%></td>
            <td><%=listController.itemArea(item)%></td>
            <td><%=listController.itemLandUse(item)%></td>   
            <td><%=listController.itemAcqusitionType(item)%></td>
            <td><%=listController.itemAcqusitionYear(item)%></td>
            <%if(listController.setting.edit||listController.setting.delete){%>
            <td>    
            </td>
            <%}%>
        </tr>
        <%}//for%>
    </tbody>
</table>
<div id="<%=listController.setting.containerEl%>_parcel_editor">
   
</div>
<div class="modal fade " id="<%=listController.setting.containerEl%>_parcel_picker" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div id="app_search_holding_container" class="modal-content">
            <%@ include file="/holding/holding_search.jsp" %>
        </div>
    </div>
</div>
<div id="<%=listController.setting.containerEl%>_new_parcel_picker">
   
</div>
<div id="<%=listController.setting.containerEl%>_splitPopover" class="popover popover-x popover-default">
    <div class="arrow"></div>
    <h3 class="popover-header popover-title"><span class="close pull-right" data-dismiss="popover-x">&times;</span>Enter Your Remark</h3>
    <form class="form-vertical">
        <div class="popover-body popover-content">


            <div class="form-group">
                <input type="text"  class="form-control" placeholder="Write Your Remark Here!" id="<%=listController.setting.containerEl%>_split_txt"/>
            </div>

        </div>
        <div class="popover-footer">
            <a target='_parent' href="javascript:transaction_change_state" class="btn btn-sm btn-primary">Submit Approval</a>
            
            <button type="reset" class="btn btn-sm btn-default">Reset</button>
        </div>
    </form>
</div>