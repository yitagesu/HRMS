<%@page import="com.intaps.nrlais.repo.*"%>
<%@page import="com.intaps.nrlais.*"%>
<%@page import="com.intaps.nrlais.controller.*"%>
<%@page import="com.intaps.nrlais.controller.tran.*"%>
<%@page import="com.intaps.nrlais.model.*"%>
<%@page import="com.intaps.nrlais.model.tran.*"%>
<%@page import="com.intaps.nrlais.util.*"%>
<%@page import="java.util.*"%>
<%@page pageEncoding="UTF-8" %>
<link rel="stylesheet" href="/assets/CSS/bootstrap.min.css"/>
<link rel="stylesheet" href="/assets/CSS/bootstrap-popover-x.min.css"/>
<link rel="stylesheet" href="/assets/CSS/custom.css"/>

<script src="/assets/ol/proj4.js"></script>
<link href="/assets/ol/ol.css" type="text/css" rel="stylesheet">        
<script src="/assets/ol/ol-debug.js"></script>
<script src="/assets/JS/nrlais_map.js"></script>
<script src="assets/JS/lang.js.jsp"></script>
<script src="/assets/JS/jquery.min.js"></script>
<script src="/assets/JS/jquery-ui.js"></script>
<script src="/assets/JS/bootstrap.min.js"></script>
<script src="/assets/JS/bootstrap-popover-x.min.js"></script>
<script src="/assets/JS/bootbox.js"></script>        
<script src="assets/JS/string.js"></script>

<script src="assets/JS/et_dual_cal.js"></script>
<script src="assets/JS/application_page_load.js"></script>
<script src="assets/JS/transaction.js"></script>
<script src="assets/JS/ejs.js"></script>
<script src="assets/JS/general.js"></script>
<script src="assets/JS/document_picker.js"></script>


<%
    TransactionContainerViewController containerController=new TransactionContainerViewController(request,response);
    

%>
<script>
    <%if(containerController.typedController.summarySection()){%>
    var summaryData =<%=GSONUtil.toJson(containerController.typedController.summary())%>;
    <%}else{%>
    var summaryData = null;
    <%}%>
    $(document).ready(function () {
        transcation_initMap('<%=containerController.mapServerUrl()%>');
        transaction_map.onMapReady = (function(){
            transaction_map.showTransactionParcels('<%=containerController.txuid%>');
        });
        if (summaryData)
        {
            fetchEJSTemplat('/transaction_summary.ejs', summaryData, function(html){
                $("#summary_content").html(html);
                for (var i = 0; i < summaryData.deletedHoldings.length; i++)
                {
                    $("#sum_del_" + summaryData.deletedHoldings[i].holdingUID).html("Loading...");
                    (function(holding){
                        $.ajax({url: holding.dataUrl, success: function(dhtml){
                                $("#sum_del_" + holding.holdingUID).html(dhtml);
                            }});
                    })(summaryData.deletedHoldings[i]);
                }

                for (var i = 0; i < summaryData.updatedHoldings.length; i++)
                {
                    $("#sum_up_old_" + summaryData.updatedHoldings[i].holdingUID).html("Loading...");
                    (function(holding){
                        $.ajax({url: holding.oldDataUrl,
                            success: function(dhtml){
                                $("#sum_up_old_" + holding.holdingUID).html(dhtml);
                            }});
                    })(summaryData.updatedHoldings[i]);
                }

                for (var i = 0; i < summaryData.updatedHoldings.length; i++)
                {
                    $("#sum_up_new_" + summaryData.updatedHoldings[i].holdingUID).html("Loading...");
                    (function(holding) {
                        $.ajax({url: holding.newDataUrl, success: function(dhtml){
                                $("#sum_up_new_" + holding.holdingUID).html(dhtml);
                            }});
                    })(summaryData.updatedHoldings[i]);
                }


                for (var i = 0; i < summaryData.newHoldings.length; i++)
                {
                    $("#sum_new_" + summaryData.newHoldings[i].holdingUID).html("Loading...");
                    (function(holding){
                        $.ajax({url: holding.dataUrl,
                            success: function(dhtml) {
                                $("#sum_new_" + holding.holdingUID).html(dhtml);
                            }});
                    })(summaryData.newHoldings[i]);
                }
            });
        }
    });

</script>
<div class="row" id="view-content">
    <div class="col-md-12 bhoechie-tab-container">
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 bhoechie-tab-menu">
            <div class="list-group">
                <a href="#" class="list-group-item active"><h4 class="fa fa-map"></h4> <%=containerController.text("Transaction")%></a>
                <%if(containerController.typedController.certficateSection()){%>                    
                <a href="#" class="list-group-item"><h4 class="fa fa-map"></h4> <%=containerController.text("Certificate")%> </a>
                <%}%>
                <%if(containerController.typedController.summarySection())
                    if(containerController.tranType != LATransaction.TRAN_TYPE_REPLACE_CERTIFICATE) 
                {%>                    
                <a href="#" class="list-group-item"><h4 class="fa fa-map"></h4> <%=containerController.text("Change Summary")%></a>
                <%}%>
                <a href="#" class="list-group-item"><h4 class="fa fa-map"></h4> <%=containerController.text("Work Flow")%></a>
            </div>

        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 bhoechie-tab has-scroll">
            <div class="bhoechie-tab-content active" id="tranContent">
                <%if(containerController.tranType==LATransaction.TRAN_TYPE_DIVORCE){
                    DivorceViewController controller=(DivorceViewController)containerController.typedController; 
                %>
                <jsp:include page="/tran/divorce/full_divorce_viewer_embeded.jsp" />
                <%} else if(containerController.tranType==LATransaction.TRAN_TYPE_RENT){
                    RentViewController controller=(RentViewController)containerController.typedController; 
                %>
                <jsp:include page="/tran/rent/full_rent_viewer_embeded.jsp" />
                <%} else if(containerController.tranType==LATransaction.TRAN_TYPE_GIFT){
                    GiftViewController controller=(GiftViewController)containerController.typedController; 
                %>
                <jsp:include page="/tran/gift/full_gift_viewer_embeded.jsp" />
                <%} else if(containerController.tranType==LATransaction.TRAN_TYPE_INHERITANCE
                    ||containerController.tranType==LATransaction.TRAN_TYPE_INHERITANCE_WITHWILL){
                    InheritanceViewController controller=(InheritanceViewController)containerController.typedController; 
                %>
                <jsp:include page="/tran/inheritance/full_inheritance_viewer_embeded.jsp" />
                <%} else if(containerController.tranType==LATransaction.TRAN_TYPE_REALLOCATION){
                    ReallocationViewController controller=(ReallocationViewController)containerController.typedController; 
                %>
                <jsp:include page="/tran/reallocation/full_reallocation_viewer_embeded.jsp" />
                <%} else if(containerController.tranType==LATransaction.TRAN_TYPE_BOUNDARY_CORRECTION){
                    BoundaryCorrectionViewController controller=(BoundaryCorrectionViewController)containerController.typedController; 
                %>
                <jsp:include page="/tran/boundary_correction/full_boundary_correction_embeded.jsp" />
                <%} else if(containerController.tranType==LATransaction.TRAN_TYPE_SPECIAL_CASE){
                    SpecialCaseViewController controller=(SpecialCaseViewController)containerController.typedController; 
                %>
                <jsp:include page="/tran/special_case/full_special_case_viewer_embeded.jsp" />
                <%} else if(containerController.tranType==LATransaction.TRAN_TYPE_EXPROPRIATION){
                    ExpropriationViewController controller=(ExpropriationViewController)containerController.typedController; 
                %>
                <jsp:include page="/tran/expropriation/full_expropriation_viewer_embeded.jsp" />
                <%} else if(containerController.tranType==LATransaction.TRAN_TYPE_CONSOLIDATION){
                    ConsolidationViewController controller=(ConsolidationViewController)containerController.typedController; 
                %>
                <jsp:include page="/tran/consolidation/full_conslidation_viewer_embeded.jsp" />
                <%} else if(containerController.tranType==LATransaction.TRAN_TYPE_EXCHANGE){
                    ExchangeViewController controller=(ExchangeViewController)containerController.typedController; 
                %>
                <jsp:include page="/tran/exchange/full_exchange_viewer_embeded.jsp" />
                <%} else if(containerController.tranType==LATransaction.TRAN_TYPE_RESTRICTIVE_INTEREST){
                    RestrictionViewController controller=(RestrictionViewController)containerController.typedController; 
                %>
                <jsp:include page="/tran/restrictive_interest/full_restrictive_interest_viewer_embeded.jsp" />
                <%} else if(containerController.tranType==LATransaction.TRAN_TYPE_SERVITUDE){
                    RestrictionViewController controller=(RestrictionViewController)containerController.typedController; 
                %>
                <jsp:include page="/tran/restrictive_interest/full_restrictive_interest_viewer_embeded.jsp" />
                <%} else if(containerController.tranType==LATransaction.TRAN_TYPE_EX_OFFICIO){
                    ExOfficioViewController controller=(ExOfficioViewController)containerController.typedController; 
                %>
                <jsp:include page="/tran/ex_officio/full_ex_officio_viewer_embeded.jsp" />
                <%} else if(containerController.tranType==LATransaction.TRAN_TYPE_REPLACE_CERTIFICATE){
                    CertificateReplacementViewController controller=(CertificateReplacementViewController)containerController.typedController; 
                %>
                <jsp:include page="/tran/certificate_replacement/full_certificate_replacement_viewer_embeded.jsp" />
                  <%} else if(containerController.tranType==LATransaction.TRAN_TYPE_SIMPLE_CORRECTION){
                    SimpleCorrectionViewController controller=(SimpleCorrectionViewController)containerController.typedController; 
                %>
                <jsp:include page="/tran/simple_correction/full_simple_correction_viewer_embeded.jsp" />
                <%}else{ 
                    TransactionViewController controller=TransactionViewController.createController(containerController.tranType,request, response, true);  
                %>
                <%@ include file="/tran/tran_viewer_header.jsp" %>
                <%}%>
            </div>

            <%if(containerController.typedController.certficateSection()){
                List<TransactionViewController.CertificateLink> links=containerController.typedController.certficateLinks();
            %>                    
            <div class="bhoechie-tab-content">
                <%
                    for(TransactionViewController.CertificateLink link:links){%>
                <a href="<%=link.url%>"><%=link.upid%> (<%=link.owner%>)</a><br/>
                <%}%>
            </div>
            <%}%>

            <%if(containerController.typedController.summarySection()){
                TransactionViewController.ChangeSummary sum=containerController.typedController.summary();
                if(containerController.tranType != LATransaction.TRAN_TYPE_REPLACE_CERTIFICATE){
            %>                    
            <div class="bhoechie-tab-content" id="summary_content">

            </div>
            <%}}%>
            <div class="bhoechie-tab-content" id="work_flow">
                
                <table class="table">
                    <thead>
                        <tr>
                            <td><%=containerController.text("Time")%></td>
                            <td><%=containerController.text("User")%></td>
                            <td><%=containerController.text("Status")%></td>
                            <td><%=containerController.text("Note")%></td>
                        </tr>
                    </thead>
                    <tbody>
                        <%
                            for(Object _flow:containerController.typedController.workFlowItems()){
                                
                                LATransaction.TranactionFlow flow=(LATransaction.TranactionFlow)_flow;
                            %>
                        <tr>
                            <td><%=flow.timeStr%></td>
                            <td><%=flow.syscreateby%></td>
                            <td><%=containerController.typedController.newStatus(flow)%></td>
                            <td><%=flow.notes%></td>
                        </tr>
                         <%}%>
                    </tbody>
                </table>
               
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 map">
            <div class="" id="map" style='height:100%'>

            </div>

        </div>
    </div>
</div>
<div class="modal fade " id="document_preview_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <button type="button" class="btn btn-nrlais" onclick="printCertificate()"><i class="fa fa-print"></i></button>
            </div>            <!-- /modal-header -->

            <div class="modal-body">
                <iframe style="width: 100%" id="attachment_preview" src=''>
                    <img src='assets/IMAGES/image.jpg'/>
                </iframe>
            </div>
            <div class="modal-footer">
                <div class="input-form">
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function ()
    {
        $("div.bhoechie-tab-menu>div.list-group>a").click(function (e) {
            e.preventDefault();
            $(this).siblings('a.active').removeClass("active");
            $(this).addClass("active");
            var index = $(this).index();
            $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
            $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
        });
    });
</script>
