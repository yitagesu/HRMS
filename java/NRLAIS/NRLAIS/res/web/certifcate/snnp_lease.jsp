<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<%@page import="com.intaps.nrlais.repo.*"%>
<%@page import="com.intaps.nrlais.*"%>
<%@page import="com.intaps.nrlais.controller.*"%>
<%@page import="com.intaps.nrlais.controller.tran.*"%>
<%@page import="com.intaps.nrlais.model.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% 
    CertficateViewController controller=new CertficateViewController(request,response);
%>
        
<html>
    <head>
        <title>SNNP Land Certificate</title>
        <meta charset="utf-8">
        <meta charset="windows-1252">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/external.css" rel="stylesheet"/>
        <link media="print" rel="Alternate" href="print.pdf">
    </head>
    <body id="snnp">
         <div class="certificate-body">
            <table style="width:100%">
                
                <tr>
                    <td class="cert-desc" >
                                            <div style = "width:65%">
                            <table class="holder-info">
                                <tr>
                                    <th>
                                        ደ/ብ/ብ/ህ/ክ የመንግሥት ኢንቨስትመንት ኮሚሽን
                                    </th>
                                </tr>
                                <tr>
                                    <th>
                                        S/N/N/P/R Investment Commision
                                    </th>
                                </tr>
                                <tr>
                                    <th>
                                    የፕሮጀክቱ ባለቤት:<% for(String name:controller.tenantNameList()){%>
                                            <%=name%><br/>
                                        <%}%>
                                    </th>    
                                </tr>                              
                                  <tr>
                                    <th>
                                    የቦታው ካርታ/Site Map 
                                    </th>  
                                </tr>

                            </table>
                        </div>
                    </td>
                                       <td style="float:left"><img src="image/snnpdirction.png"/></td> 
                 <td class="cert-desc" style ="width:25%">
                        <div class="holding-description" >
                            
                            <table  class="holder-parcel">
                            
                                      <tr>
                                    <td>
                                        <b>የካርታ ቁትር：</b> 
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>ተሻሽሎ የቀረበበት ቀን：</b> 
                                    </td>
                                </tr>
                                <tr>
                            
                            </table>
                            
                            <table class="holder-parcel">
                                    <td>
                                        <b>ወራዳ :</b> <%=controller.woreda%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>                                        
                                        <b>ቀበሌ :</b> <%=controller.kebele%>
                                    </td>
                                </tr>
                                
                                
                                <tr>
                                    <td colspan="2">
                                        <b>የማሳው ስፋት (ሄ/ር) :</b> <%=((double)Math.round(controller.area))/10000%>
                                    </td>
                                </tr>
                            </table>
                            <table class="parcel-confirm">
                                <tr>
                                    <td colspan="2">
                                        <b>ቀያሽ ባለሞያ ስም:</b><%=controller.checker%>
                                    </td>
                                </tr>
                                
                              <tr>
                                     <td><b>ቀን：</b> <span id="checkDate"><%=controller.checkeDate%></span>
                                    </td>
                                </tr>
                                <tr>
                                   <td>                                        
                                        <b>ፊርማ：</b>________________________
                                    </td>
                                   
                                </tr>
                            </table>
                                    <table class="parcel-confirm">
                                <tr>
                                    <td colspan="2">
                                        <b>ያጸደቀው ባለሙያ ስም:</b><%=controller.approval%>
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>ቀን :</b> <span id="approveDate"><%=controller.approvedDate%></span>
                                    </td>
                                </tr>
                                <tr>
                                      <td>
                                        <b>ፊርማ :</b>________________________
                                    </td>  
                                </tr>
                                   
                             </table>
                    <table class="parcel-confirm" style="margin-bottom:-20px">
                                         <tr>
                                         
                                     <td>
                                         <div>
                                         Projection:Adindan UTM zone 37N 
                                          </div>
                                         </td>
                                        
                                </tr> 
                                    </table>
                    </td>
                                    
                                    
                        </div>
                    </td> 
                </tr>
                <tr>
                    <td>
                        <div class="map" style = "width:65%;margin-left: -4px;margin-top: -290px">
                            <img src="/api/map/cert?width=1000&height=1000&tran_uid=<%=controller.tran.transactionUID%>&parcel_uid=<%=controller.parcel.parcelUID%>"/>
                        </div>
                    </td>
                    
                </tr>

            </table>
        </div>
    </body>
    <script>
    </script>
</html>
