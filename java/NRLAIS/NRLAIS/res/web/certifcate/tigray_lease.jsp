<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<%@page import="com.intaps.nrlais.repo.*"%>
<%@page import="com.intaps.nrlais.*"%>
<%@page import="com.intaps.nrlais.controller.*"%>
<%@page import="com.intaps.nrlais.controller.tran.*"%>
<%@page import="com.intaps.nrlais.model.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% 
    CertficateViewController controller=new CertficateViewController(request,response);
%>
        
<html>
    <head>
        <title>SNNP Land Certificate</title>
        <meta charset="utf-8">
        <meta charset="windows-1252">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/external.css" rel="stylesheet"/>
        <link media="print" rel="Alternate" href="print.pdf">
    </head>
    <body id="tigray">
         <div class="certificate-body">
            <table style="width:100%">
                
                <tr>
                
                    <td>
                                      
                        <div style = "width:55%">   
                            <table>
                                   
                <tr>
                    <td> 
                      <h3 style="margin-right: 0px;"><ins>ጠቁሚ ካርታ መረጋገጺ ትሕዝቶ መሬት ኢንቨስትመንት</ins></h3>
                    </td>
                </tr>       
                <tr>
                    <td>
                        <b>ሽም በዓል ትሕዝቶ መሬት :</b> <label><ins>
                            
                                <% for(String name:controller.nameList()){%>
                                            <%=name%><br/>
                                        <%}%>
                                
                                
                            </ins></label>
                    </td>
                    <td style="margin-left: 300px;">
                        <b>ስፍሓት: </b><label><ins><%=((double)Math.round(controller.area))/10000%></ins></label><b>ሄክታር</b>
                    </td>
                    
                </tr> 
                <tr>
                    <td>
                        <b>በዓልቲ ዓዳር:</b> <label><ins></ins></label>
                    </td>
                    <td>
                        <b>ሽም ኢንቨስትመንት:</b> <label><ins></ins></label>
                    </td>
                </tr>
                <tr>
                    
                    <td>
                        <b>ዓይነት ኢንቨስትመንት:</b><label><ins></ins></label>
                    </td>
                </tr>
                
                
                </table>
                </div>
            </td>
</tr>
                                   
                         
                                        
                          

                <tr>
                    <td>
                        <div class="map" style = "width:45%">
                            <img src="/api/map/cert?width=1000&height=1000&tran_uid=<%=controller.tran.transactionUID%>&parcel_uid=<%=controller.parcel.parcelUID%>"/>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table style = "width:50%">
                            <tr>
                            <td>
                                <b>ዘዳለው ሽም:</b><label><ins></ins></label>
                    </td>
                     <td>
                         <b>ዘረጋገጺ ሽም:</b><label><ins></ins></label>
                    </td>
                    <td>
                        <b>ዘጸደቐ ሽም:</b><label><ins></ins></label>
                    </td>
                            </tr>
                            <tr>
                               <td>
                                   <b>ሓላፍነት:</b><label><ins></ins></label>
                    </td> 
                      <td>
                          <b>ሓላፍነት:</b><label><ins></ins></label>
                    </td> 
                      <td>
                          <b>ሓላፍነት:</b><label><ins></ins></label>
                    </td> 
                     </tr>
                            <tr>
                                <td>
                                    <b>ፊርማ: <label><ins></ins></label> </b><b>ዕለት:<label></label> </b>
                                    
                                </td>
                                <td>
                                    <b>ፊርማ: <label><ins></ins></label></b><b>ዕለት:<label></label> </b>
                                    
                                </td>
                                <td>
                                    <b>ፊርማ: <label><ins></ins></label></b><b>ዕለት:<label></label> </b>
                                    
                                </td>
                            </tr>
                                
                                
                           
                    </table>
                    </td>
                                       
                </tr>
                       
                
                 <td style ="position:absolute; width:35%;top:30px;right:80px">
                        <div class="holding-description" >
                            
                            <table  >
                            
                                      <tr>
                                    <td>
                                        <b>ሕታም：<label><ins></ins></label></b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>መለለይ ቁጸሪ ትሕዝቶ：<label><ins></ins></label></b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>ቐፅሪ መጠቓለሊ ካርታ ትሕዝቶ መሬት：<label><ins></ins></label>
                                    </td>
                                    
                                </tr>
                                <tr>
                                    <td>ኣድራሻ<td>
                                </tr>
                                <tr><td>ከልል:ትግራይ</td>
                                    <td>
                                        <b>ጣብያ：<label><ins></ins></label></b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>ዞባ：<label><ins></ins></label></b>
                                    </td>
                                    <td>
                                        <b>ቁሽት：<label><ins></ins></label></b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>ወረዳ：<label> <ins><%=controller.woreda%></ins></label></b>
                                    </td>
                                    <td>
                                        <b>ፉሉይ ቦታ：<label><ins></ins></label></b>
                                    </td>
                                </tr>
                                <tr>
                                   <td> 
                      <h3 ><ins>መፍትሕ</ins></h3>
                    </td> 
                                </tr>
                                <tr>
                                    <td>
                                        መፃፅሪ ነትብ
                                    </td>
                                </tr>
                                <tr><td>ግራት በዓል ትሕዝቶ</td></tr>
                            </table>
    
                        </div>
                    </td> 
              
            </table>
        </div>
    </body>
    <script>
    </script>
</html>
