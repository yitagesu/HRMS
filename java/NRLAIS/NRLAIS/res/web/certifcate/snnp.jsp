<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<%@page import="com.intaps.nrlais.repo.*"%>
<%@page import="com.intaps.nrlais.*"%>
<%@page import="com.intaps.nrlais.controller.*"%>
<%@page import="com.intaps.nrlais.controller.tran.*"%>
<%@page import="com.intaps.nrlais.model.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% 
    CertficateViewController controller=new CertficateViewController(request,response);
%>
<html>
    <head>
        <title>snnp Land Certificate</title>
        <meta charset="utf-8">
        <meta charset="windows-1252">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/external.css" rel="stylesheet"/>
        <link media="print" rel="Alternate" href="print.pdf">
        <script src="<%=request.getContextPath()%>/assets/js/jquery.js"></script>
        <script src="<%=request.getContextPath()%>/assets/js/string.js"></script>
        <script src="<%=request.getContextPath()%>/assets/js/et_dual_cal.js" charset="utf-8"></script>
    </head>
    <body id="snnp">
        <div class="certificate-body">
            <table style="width:100%">
                <tr>
                    <td style="width:15%" class="align-center"><img src="image/snnp-logo.png"/></td>
                    <td style="width:52%" class="align-center"><h1><u>የማሳ ካርታ</u></h1></td>
                    <td style="width:33%" class="align-right"><img src="image/snnpdirction.png"/></td>
                </tr>
            </table>
            <table style="width:100%">
                <tr>
                    <td style="width:60%">
                        <div class="map">
                            <img src="/api/map/cert?width=1000&height=1000&tran_uid=<%=controller.tran.transactionUID%>&parcel_uid=<%=controller.parcel.parcelUID%>"/>
                        </div>
                    </td>
                    <td class="cert-desc">
                        <div class="holding-description">
                            <table class="holder-parcel">

                                <tr>
                                    <td>
                                        <%if(controller.region!=null){%>
                                        <b>ክልል:</b> <%=controller.region%>
                                        <%} else {%>
                                        <b>ክልል:</b>________________________ 
                                        <%}%>

                                    </td>
                                    <td>
                                        <%if(controller.zone!=null){%>
                                        <b>ዞን :</b> <%=controller.zone%>
                                        <%} else {%>
                                        <b>ዞን :</b>________________________ 
                                        <%}%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <%if(controller.woreda!=null){%>
                                        <b>ወራዳ :</b> <%=controller.woreda%>
                                        <%} else {%>
                                        <b>ወራዳ :</b>________________________ 
                                        <%}%>
                                    </td>
                                    <td>
                                        <%if(controller.kebele!=null){%>
                                        <b>ቀበሌ :</b> <%=controller.kebele%>
                                        <%} else {%>
                                        <b>ቀበሌ :</b>________________________ 
                                        <%}%>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <%if(controller.parcel.upid!=null){%>
                                        <b>አስ/ልዩ የማሣ መለያ ቁጥር :</b> <%=controller.parcel.upid%>
                                        <%} else {%>
                                        <b>አስ/ልዩ የማሣ መለያ ቁጥር :</b>________________________ 
                                        <%}%>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <b>የማሳው ስፋት (ሄ/ር) :</b> <%=((double)Math.round(controller.area))/10000%>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <b>የመሬት አጠቃቀም :</b> <%=controller.currentLandUse%>
                                    </td>
                                </tr>
                            </table>
                            <table class="holder-info">
                                <tr>
                                    <th>
                                        የባለይዞታው/ዋ/ዎች ስም :
                                    </th>
                                </tr>


                                <tr>
                                   <td>
                                        <% for(String name:controller.nameList()){%>
                                            <%=name%><br/>
                                        <%}%>
                                    </td>
                                </tr>

                            </table>
                            <table class="table-title">
                                <tr>
                                    <td class="vertical-align">
                                        <table class="legend-table">
                                            <tr>
                                                <td colspan="2"><p><u>መግለጫ</u><p></td>
                                            </tr>
                                            <tr>
                                                <td><img src="image/parcel.png"/></td>
                                                <td>የባለይዞታው ማሳ</td>
                                            </tr>
                                            <tr>
                                                <td><img src="image/border-parcel.png"/></td>
                                                <td>አዋሳኝ ማሳ</td>
                                            </tr>
                                        </table></td>
                                </tr>
                            </table>
                            <table class="parcel-confirm">
                                <tr>
                                    <td>
                                        <b>የተሰጠበት ቀን :</b> <span id="issueDate"><%=controller.issuedDate%></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <%if(controller.checker!=null){%>
                                        <b>ያረጋገጠው ባለሙያ ስም :</b><%=controller.checker%><br/>
                                        <%} else {%>
                                        <b>ያረጋገጠው ባለሙያ ስም :</b>________________________ <br/>
                                        <%}%>

                                        <b>ፊርማ :</b>________________________<br/>   
                                        <b>ቀን :</b> <span id="checkDate"><%=controller.checkeDate%></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <%if(controller.approval!=null){%>
                                        <b>ያፀደቀው ኃላፊ ስም :</b><%=controller.approval%><br/>
                                        <%} else {%>
                                        <b>ያፀደቀው ኃላፊ ስም :</b>________________________ <br/>
                                        <%}%>
                                        <b>ፊርማ :</b>________________________<br/>
                                        <b>ቀን :</b> <span id="approveDate"><%=controller.approvedDate%></span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <script>
        </script>

    </body>
</html>
