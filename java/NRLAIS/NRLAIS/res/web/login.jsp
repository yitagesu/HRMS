<%@page import="com.intaps.nrlais.worlais.*"%>
<%@page import="com.intaps.nrlais.*"%>
<%@page import="com.intaps.nrlais.controller.*"%>
<%@page pageEncoding="UTF-8" %>

<%
    LoginViewController controller=new LoginViewController(request,response);
    if(controller.abortView)
        return;
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=0.8, maximum-scale=0.8">
        <title>NRLAIS Login</title>
        <link rel="stylesheet" href="/assets/CSS/custom.css"/>
        <link rel="stylesheet" href="/assets/CSS/bootstrap.min.css"/>
        <script src="/assets/JS/jquery.min.js"></script>
        <script src="/assets/JS/jquery-ui.js"></script>
        <script src="/assets/JS/bootstrap.min.js"></script>
        <script src="/assets/JS/login.js"></script>
    </head>

    <body id="login">
        <div class="container">
            <div class="login-container">
                <div class="login-advert">

                </div>
                <div class="login-warper">
                    <div class="login-header align-center">

                        <h1>NRLAIS</h1>
                        <p>WORLAIS</p>
                    </div>
                    <div class="login-body">
                        <div class="login-body-h">
                            <h2>Login</h2>
                        </div>
                        <hr>
                        <div class="login-body-b">
                            <form id="loginForm" method="POST" action="login.jsp">
                                <div class="input-group">
                                    <span class="input-group-addon" id="addon">
                                        <i class="fa fa-user"></i>
                                    </span>
                                    <input type="text" placeholder="Username!" class="form-control username" id="username" name="username"  value="<%=controller.userName%>"/>
                                    <span class="input-group-addon username_error" id="username_error" style="display: none">
                                        <i class="fa fa-warning faa-flash animated"></i>
                                    </span>
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon" id="addon">
                                        <i class="fa fa-lock"></i>
                                    </span>
                                    <input type="password" placeholder="Password!" class="form-control password" id="password" name="password" value="<%=controller.password%>"/>
                                    <span class="input-group-addon password_error" id="password_error" style="display: none">
                                        <i class="fa fa-warning faa-flash animated"></i>

                                    </span>
                                </div>
                                <div class="input-group">
                                    <span class="user_pass_error" id="user_pass_error" style="display: <%=controller.loginError==null?"none":"block"%>">
                                        <i class="fa fa-warning faa-flash animated"></i>
                                        <%=controller.loginError==null?"":controller.loginError%>
                                    </span>
                                </div>
                                <div class="align-right">
                                    <div class="btn-group">

                                        <select name='lang' class="btn btn-default form-select">
                                            <option value="am-et">Amharic</option>
                                            <option value="om-et">Oromifa</option>
                                            <option value="ti-et">Tigrigna</option>
                                            <option value="en-us" selected>English</option>
                                        </select>
                                        <button type="button" class="btn btn-primary" onclick="valLogin()">Login</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="login-footer">

                        <hr/>
                        <div class="login-body-f align-center">
                            <p></p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </body>
</html>
