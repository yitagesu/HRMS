/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var transaction_map=null;
function transcation_initMap(server)
{
    transaction_map=NrlaisMap(server,"map");
    transaction_map.load();
}
function mapview_zoomBox(x,y,w,h)
{
    transaction_map.zoomBox(x,y,w,h);
}
function transcation_showCertiricate(txuid,huid,puid)
{
   var url ="";
        $.ajax({
            method: 'GET',
            url: "/api/transaction",
            contentType :"application/json",
            data: {txuid:txuid},
            success:function(result){
                var regID = result.res.csaregionid; 
               if(result.res.transactionType == 9)
               {
                switch(regID){
                    case "01":
                        url = "/certifcate/tigray_lease.jsp?tran_uid="+txuid+"&holding_uid="+huid+"&parcel_uid="+puid;
                        break;
                    case "03":
                        url = "/certifcate/amhara_lease.jsp?tran_uid="+txuid+"&holding_uid="+huid+"&parcel_uid="+puid;
                        break;
                    case "04":
                        url = "/certifcate/oromia_lease.jsp?tran_uid="+txuid+"&holding_uid="+huid+"&parcel_uid="+puid;
                        break;
                    case "07":
                        url = "/certifcate/snnp_lease.jsp?tran_uid="+txuid+"&holding_uid="+huid+"&parcel_uid="+puid;
                        break;
                    default:
                        url = "/certifcate/amhara_lease.jsp?tran_uid="+txuid+"&holding_uid="+huid+"&parcel_uid="+puid;
                        break;
                }
            
               }
               else
               {
                    switch(regID){
                    case "01":
                        url = "/certifcate/tigray.jsp?tran_uid="+txuid+"&holding_uid="+huid+"&parcel_uid="+puid;
                        break;
                    case "03":
                        url = "/certifcate/amhara.jsp?tran_uid="+txuid+"&holding_uid="+huid+"&parcel_uid="+puid;
                        break;
                    case "04":
                        url = "/certifcate/oromia.jsp?tran_uid="+txuid+"&holding_uid="+huid+"&parcel_uid="+puid;
                        break;
                    case "07":
                        url = "/certifcate/snnp.jsp?tran_uid="+txuid+"&holding_uid="+huid+"&parcel_uid="+puid;
                        break;
                    default:
                        url = "/certifcate/amhara.jsp?tran_uid="+txuid+"&holding_uid="+huid+"&parcel_uid="+puid;
                        break;
                }
                   
               }
               $('#document_preview_modal').modal('show');
               $('#attachment_preview').attr("src", url);
           
            }
            ,
            error:function(err){
                error(err);
            }
    });
    
    
  
    
}
function transaction_change_state(txuid,cmd,note)
{
    $.ajax({
            method: 'POST',
            url: "/api/transaction_state",
            contentType :"application/json",
            data: JSON.stringify({txuid:txuid,cmd:cmd,note:note}),
            success:function(result){
                if(result.error)
                    bootbox.alert(result.error);
                else
                {
                    parent.location.reload();
                }
            },
            error:function(err){
                error(err);
            }
    });
}
function transaction_saveTransaction(data,initial,done,error)
{
    $.ajax({
            method: 'POST',
            url: "/api/transaction?initial="+initial,
            contentType :"application/json",
            data: JSON.stringify(data),
            success:function(result){
                if(result.error)
                    error(result.error);
                else
                    done(result.res);
            },
            error:function(err){
                error(err);
            }
    });
}


function transaction_showTranDetail(tranUID, tranType,setting) {
    var url = "/view_transaction_detail.jsp?tran_uid=" + tranUID + "&tran_type=" + tranType;
    if(setting)
        url+="&setting="+JSON.stringify(setting)
    ;
    $("#application_detail").modal('show');
    $("#transactionContent-iframe").attr('src', url);

}
function transaction_validateApplication() {

    validApplication = true;
    if ($('#app_date').val() == "") {
        $('#emptyDate').remove();
        $('.error-panel').append("<span id='emptyDate'> <i class='fa fa-warning faa-flash animated'></i> Please Pick The Application Date<br/></span>");
        $('#app_date').removeClass('form-control');
        $('#app_date').addClass('form-error');
        validApplication = false;
    } else {
        $('#emptyDate').remove();
        $('#app_date').removeClass('form-error');
        $('#app_date').addClass('form-control');
        if (!isValidDate($("#app_date"))) {
            $('#invalidDate').remove();
            $('.error-panel').append("<span id='invalidDate'><i class='fa fa-warning faa-flash animated'></i> Invalid Date Please Fix the Date<br/></span>");
            $('#app_date').removeClass('form-control');
            $('#app_date').addClass('form-error');
            validApplication = false;
        } else {
            $('#invalidDate').remove();
            $('#app_date').removeClass('form-error');
            $('#app_date').addClass('form-control');
        }
    }    
    return validApplication;
}
function transaction_getTransactionInfo(type) {
    var applicationInfo = {};
    var d = new Date($("#app_date").attr("dateval"));
    var time = d.getTime();
    applicationInfo.transactionType = type;
    applicationInfo.time = time;
    applicationInfo.notes = $("#app_description").val();
    applicationInfo.transactionUID=transaction_tran_uid;
    return applicationInfo;
}
function validatePageOne()
{
    var validApplication = true;
    if (!$.trim($('#holder_content').html()).length) {
        $('#emptyHolding').remove();
        $('.error-panel').append("<span id='emptyHolding'><i class='fa fa-warning faa-flash animated'></i> "+lang("Please Pick Holding Information")+"<br/></span>");
        validApplication = false;
    } else {
        $('#emptyHolding').remove();
    }
    return validApplication;
}

function navigateMap(){
    
   if($(".list-of-transc").attr("view")=="half"){
       $(".list-of-transc").attr("view","full");
       $(".list-of-transc").removeClass("col-md-7");
       $(".list-of-transc").addClass("col-md-11");
       $(".map").removeClass("col-md-5");
       $(".map").addClass("col-md-1");
       $("#map").hide();
       $("#map_text").show();
       $("#arrow").removeClass("fa-arrow-right");
       $("#arrow").addClass("fa-arrow-left");
   }
   else
   {
       $(".list-of-transc").attr("view","half");
       $(".list-of-transc").removeClass("col-md-11");
       $(".list-of-transc").addClass("col-md-7");
       $(".map").removeClass("col-md-1");
       $(".map").addClass("col-md-5");
       $("#map_text").hide();
       $("#map").show();
       $("#arrow").removeClass("fa-arrow-left");
       $("#arrow").addClass("fa-arrow-right");
   }
}