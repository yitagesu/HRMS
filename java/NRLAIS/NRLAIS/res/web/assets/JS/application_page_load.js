/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function show(image, mime) {

    $('#document_preview_modal').modal('show');
    $('#attachment_preview').attr("src", "data:" + mime + ";base64," + image);
}
function showByUID(uid) {

    $('#document_preview_modal').modal('show');
    $('#attachment_preview').attr("src", "/api/get_doc?doc_uid=" + uid);
}

function clickBackW() {
    document.getElementsByName('iframe').parent.history.back();
    return false;
}
function clickForW() {
    iframe.contentWindow.history.go(1);
}
