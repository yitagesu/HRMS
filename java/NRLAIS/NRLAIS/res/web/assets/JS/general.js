/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function common_parseFraction(f)
{
    var parts = f.split('/');
    if (parts.length == 0)
        return {num: 0, denum: 1};
    else if (parts.length == 1)
        return {num: parseInt(parts[0]), denum: 1};
    else
        return {num: parseInt(parts[0]), denum: parseInt(parts[1])};
}

function common_fractionToString(f)
{
    if (!f)
        return "";
    if (!f.denum || !f.num)
        return "";
    if (f.denum == 0)
        return f.num.toString();
    return f.num + "/" + f.denum;
}
function formatParcelSeqNo(n)
{
    if (n)
        return S(n).padLeft(5, '0');
    return "";
}
function formatHoldingSeqNo(n)
{
    if (n)
        return S(n).padLeft(5, '0');
    return "";
}
function formatParcelArea(area)
{
    if (area)
        return Math.round(area);
    return "";
}

//date validation
function isValidDate(jqel)
{
    var d = jqel.attr("dateval");
    if (d == null || d == "")
        return false;
    var dd = Date.parse(d);
    if (isNaN(dd))
        return false;
    if (dd > (+new Date())) //future date
        return false;
    return true;
}
function getLongDateFromCal(sel)
{
    var d = new Date(sel.attr("dateval"));
    return d.getTime();
}
function getDateString(val)
{
    var d = new Date(val);
    return d.getDate() + "/" + d.getMonth() + "/" + d.getFullYear();
}
function getAgeFromCal(sel)
{
    var today = new Date();
    var nowyear = today.getFullYear();
    var nowmonth = today.getMonth();
    var nowday = today.getDate();
    //"#parcel_form_dateOfBirth"
    var dob = sel;
    var birth = new Date(dob);

    var birthyear = birth.getFullYear();
    var birthmonth = birth.getMonth();
    var birthday = birth.getDate();

    var age = nowyear - birthyear;
    var age_month = nowmonth - birthmonth;
    var age_day = nowday - birthday;
    if (age_month < 0 || (age_month == 0 && age_day < 0) || age_month < 0) {
        age = parseInt(age) - 1;
    }
    return age;
}

$(document).ready(function () {
    $("div.bhoechie-tab-menu>div.list-group>a").click(function (e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
    });
});

function fetchEJSTemplat(url, data, done, error)
{
    $.ajax({
        url: url,
        success: function (template)
        {
            var html = ejs.render(template, {locals: data});
            done(html);
        },
        error: error
    });
}
function toEthDateStr(time)
{
    if (!time)
        return "";
    return EthiopianDate.prototype.ethiopianDateString(EthiopianDate.prototype.dateToEthiopian(new Date(time)));
}
function validTextInputNoEmpty(el)
{
    var x = el.val().trim();
    var valid = x != "" && x != -1;

    setValidationStyle(el, valid);
    return valid;
}
function setValidationStyle(el, valid) {
    if (valid) {
        (el).removeClass("form-error");
        (el).addClass("form-control");
    } else {
        (el).removeClass("form-control");
        (el).addClass("form-error");
    }
}
function validatorPanel(panelSel)
{
    var state = {};
    state.addError = function (id, message)
    {
        $(panelSel + " [errorid=" + id + "]").remove();
        $(panelSel).append("<p errorid='" + id + "'><i class='fa fa-warning faa-flash animated'></i>" + message + "</p>");
    }
    state.removeError = function (id)
    {
        $(panelSel + " [errorid=" + id + "]").remove();
    }
    state.clear = function ()
    {
        $(panelSel).html("");
    }
    return state;
}

function pritSlip() {
    var frm = document.getElementById("printSlip").contentWindow;
    frm.focus();
    frm.print();
}
var busyClue = {
    show: function (message)
    {
        $("#busy_clue_message").html(message);
        $("#busy_clue_modal").modal('show');
    },
    hide: function ()
    {
        $("#busy_clue_modal").modal('hide');
    }
};
function printCertificate() {
    var frm = document.getElementById("attachment_preview").contentWindow;
    frm.focus();
    frm.print();
}
$(document).ready(function ()
{
    $("#app_phone").on('keyup', function () {
        this.value = this.value.replace(/[^0-9\.\+\(\)\-]/g, '');
    });
});