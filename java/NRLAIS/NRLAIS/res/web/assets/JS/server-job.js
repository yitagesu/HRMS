/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function monitorJob(startUrl, startData, monitorUrl, pgbar, statusText,startButton
        ,onStartSuccess
        ,onDone
        ,stopButton
                )
{
    var monitor = false;
    var timer = null;
    var _monitorUrl=monitorUrl;
    var attach_monitor=false;
    if(stopButton)
        $(stopButton).prop('disabled', true);
    function stopMonitoring()
    {
        monitor = false;
        if(startButton)
            $(startButton).prop('disabled', false);
        if(stopButton)
            $(stopButton).prop('disabled', true);
        if(pgbar)
            $(pgbar).attr("value","0");
        if(attach_monitor)
            if(statusText)
                $(statusText).text("");                        
        attach_monitor=false;
    }
    function startProgressMonitoring()
    {
        monitor = true;
        if(startButton)
            $(startButton).prop('disabled', true);
        if(stopButton)
            $(stopButton).prop('disabled', false);
        timer = window.setInterval(function ()
        {
            if (!monitor)
            {
                window.clearInterval(timer);
                return;
            }
            $.get(_monitorUrl, function (res)
            {
                if (res.error != null)
                {
                    if(statusText)
                        $(statusText).text(res.error);                        
                    else
                        alert(res.error);
                    stopMonitoring();
                } else
                {
                    if(statusText)
                        $(statusText).text(res.res.status_message);
                    if(pgbar)
                        $(pgbar).attr("value",(100*res.res.progress).toString());
                    if (res.res.status != 1 && res.res.status != 2)
                    {
                        stopMonitoring();
                        if(onDone)
                            onDone(res.res);
                        if(res.res.errorList!=null && res.res.errorList.length>0)
                        {
                            var msg=res.res.errorList[0];
                            for(var i=1;i<res.res.errorList.length;i++)
                                msg+="\n"+res.res.errorList[i];
                        }
                    }
                }
            }).fail(function()
            {
                if(statusText)
                        $(statusText).text("Communication error");
            });
        }, 1000);
    }
    function startTransfer()
    {
        if(startButton)
            $(startButton).prop('disabled', true);
        $.post(startUrl, JSON.stringify(startData), function (res)
        {
            if (res.error != null)
            {
                if(statusText)
                    $(statusText).text(res.error);
            } else
            {
            if(onStartSuccess)
                onStartSuccess(res.res);

            startProgressMonitoring();
            }
        }).fail(function()
            {
                if(statusText)
                        $(statusText).text("Communication error");
            });
    }
    
    return {
        start:startTransfer,
        stop:function()
        {
            $.ajax(monitorUrl, 
            {
                method:"PUT"
            });   
        },
        attach:function()
        {
            attach_monitor=true;
            startProgressMonitoring();  
        },
        setMonitorUrl:function(url)
        {
            _monitorUrl=url;
        }
        
    };
}