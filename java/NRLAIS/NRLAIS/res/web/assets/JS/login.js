/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function valLogin() {
    var valid = true;
    var requiredField = ['username', 'password'];
    for (var i = 0; i < requiredField.length; i++) {
        var arrlist = $('#' + requiredField[i]).val();
        if (arrlist == "" || arrlist == -1)
        {
            $('#' + requiredField[i] + '_error').show();
            valid = false;
        } else
        {
            $('#' + requiredField[i] + '_error').hide();
        }
    }

    if (valid)
    {
        document.getElementById("loginForm").submit();
    }
}
function application_logout()
{
    $.ajax({
        url: "/login.jsp?logout=true",
        method: "POST",
        success: function ()
        {
            window.location = "/login.jsp";
        },
    });
}