/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var documentPicker =
        {
            max_row_id: 0,
            pickedDocs: [],
            doc_type_id: null,
            row_id: null,
            callBack: null,
            tableBodyElement: null,
            n_required: 0,
            getAllDocs: function ()
            {
                var ret = [];
                var l = Object.keys(this.pickedDocs).length;
                for (var i = 0; i < l; i++)
                {
                    ret.push(this.getRequiredDocument(i));
                }
                return ret;
            },
            countDocumentByType: function (typeID)
            {
                var ret = 0;
                for (var row_id in this.pickedDocs)
                {
                    if (this.pickedDocs[row_id] && this.pickedDocs[row_id].docType == typeID)
                    {
                        ret++;
                    }
                }
                return ret;
            },
            setRequiredDocument: function (index, doc)
            {
                var row_id = 'row_' + (index + 1);
                var tr = $(this.tableBodyElement + " #" + row_id);
                if (doc)
                {
                    tr.find("#doc_ref").text(doc.refText);
                    tr.find("#doc_desc").text(doc.description);
                } else
                {
                    tr.find("#doc_ref").text("");
                    tr.find("#doc_desc").text("");
                }
                this.pickedDocs[row_id] = doc;
            },
            setAddtionalDocument: function (doc)
            {


                if (doc.length > 0) {
                    for (var i = 0; i < doc.length; i++) {
                        this.max_row_id++;
                        var row_id = 'row_' + (this.max_row_id);
                        var rowHTML = `<td>`+lang("Other")+` </td>
                        <td>` + doc[i].refText + `</td>
                        <td>` + doc[i].description + `</td>
                        <td><a href="#">` + doc[i].docfile + `</a></td>
                        <td><a href="javascript:documentPicker.remove('` + row_id + `')" class="btn btn-warning"><i class="fa fa-trash"></i></a></td>`;
                        $(this.tableBodyElement).append(`<tr id='` + row_id + `'>` + rowHTML + `</tr>`);
                    }
                }
            },
            getRequiredDocument: function (index)
            {
                var row_id = 'row_' + (index + 1);
                return this.pickedDocs[row_id];
            },
            getAllAddtionalDocument: function () {
                var ret = [];
                var l = Object.keys(this.pickedDocs).length;
                for (var i = this.n_required; i < l; i++)
                {
                    ret.push(this.getRequiredDocument(i));
                }
                return ret;
            },
            validateAndSaveDocument: function () {
                var validDoc = true;
                var ref = $('#doc_reference_req').val();
                if (ref == "") {
                    $('#doc_reference_req').removeClass('form-control');
                    $('#doc_reference_req').addClass('form-error');
                    validDoc = false;
                } else {
                    $('#doc_reference_req').removeClass('form-error');
                    $('#doc_reference_req').addClass('form-control');
                }
                if (validDoc) {
                    this.callBack(this.getDocumentData());
                    $('#req_doc_form').trigger('reset');
                }
            },
            getDocumentData: function () {
                var reqDocument = {};
                reqDocument.docfile = $("#upload_doc_req").val();
                reqDocument.description = $("#doc_description_req").val();
                reqDocument.refText = $("#doc_reference_req").val();

                if (attachment.hasAttachment()) {
                    reqDocument.archiveType = 1;
                    reqDocument.mimeType = attachment.mimeType;
                    reqDocument.fileImage = attachment.imageBytes;
                } else {
                    reqDocument.archiveType = 2;
                    reqDocument.mimeType = null;
                }
                return reqDocument;
            },
            getAdditionalDocumentData: function () {
                var reqDocument = {};
                reqDocument.docfile = $("#upload_doc_add").val();
                reqDocument.docType = DOC_TYPE_OTHER;
                reqDocument.description = $("#doc_description_add").val();
                reqDocument.refText = $("#doc_ref_add").val();

                if (attachment.hasAttachment()) {
                    reqDocument.archiveType = 1;
                    reqDocument.mimeType = attachment.mimeType;
                    reqDocument.fileImage = attachment.imageBytes;
                } else {
                    reqDocument.archiveType = 2;
                    reqDocument.mimeType = null;
                }
                return reqDocument;
            },
            initPicker: function ()
            {
                $(documentPicker.tableBodyElement + " tr").each(function ()
                {
                    var doc_type = $(this).attr("doc_type");
                    documentPicker.max_row_id++;
                    var row_id = "row_" + documentPicker.max_row_id;
                    $(this).attr("id", row_id);
                    $(this).find("a").attr("href", "javascript:documentPicker.show('" + row_id + "'," + doc_type + ")");
                    documentPicker.pickedDocs[row_id] = null;
                    documentPicker.n_required++;
                });
            },
            load: function (table_body, types, doneLoading)
            {
                this.tableBodyElement = table_body;
                this.max_row_id = 0;
                this.pickedDocs = [];
                var that = this;
                if (types == null)
                {
                    this.initPicker();
                    if (doneLoading)
                        doneLoading();
                } else
                {
                    fetchEJSTemplat('/assets/JS/document_picker_row.ejs', {types: types},
                            function (html) {
                                $(documentPicker.tableBodyElement).html(html);
                                that.initPicker();
                                if (doneLoading)
                                    doneLoading();
                            });
                }
            },
            show: function (row_id, doc_type_id)
            {
                this.doc_type_id = doc_type_id;
                this.row_id = row_id;
                $("#add_req_document").trigger("reset");
                $('#add_req_document').modal('show');
                attachment.clear();
                this.callBack = function (docdata)
                {
                    docdata.docType = this.doc_type_id;
                    var tr = $(this.tableBodyElement + " #" + this.row_id);
                    tr.find("#doc_ref").html(docdata.refText);
                    tr.find("#doc_File").html("<a href=\"javascript:documentPicker.showAttachment('<%=jadova.util.Base64.getEncoder().encodeToString(" + docdata.fileImage + ")%>','" + docdata.mimeType + "')\">" + docdata.docfile + " </a>");
                    tr.find("#doc_desc").html(docdata.description);
                    this.pickedDocs[this.row_id] = docdata;
                    $('#add_req_document').modal('hide');

                };
            },
            remove: function (row_id)
            {
                $(this.tableBodyElement + " #" + row_id).remove();

            },
            addAdditional: function ()
            {
                var validDoc = true;
                var ref = $('#doc_ref_add').val();
                if (ref == "") {
                    $('#doc_ref_add').removeClass('form-control');
                    $('#doc_ref_add').addClass('form-error');
                    validDoc = false;
                } else {
                    $('#doc_ref_add').removeClass('form-error');
                    $('#doc_ref_add').addClass('form-control');
                }
                if (validDoc) {

                    this.max_row_id++;
                    var row_id = 'row_' + this.max_row_id;
                    var doc = this.getAdditionalDocumentData();
                    var rowHTML = `<td>`+lang("Other")+`  </td>
                        <td>` + doc.refText + `</td>
                        <td>` + doc.description + `</td>
                        <td><a href="#">` + doc.docfile + `</a></td>
                        <td><a href="javascript:documentPicker.remove('row_` + this.max_row_id + `')" class="btn btn-warning"><i class="fa fa-trash"></i></a></td>`;
                    $(this.tableBodyElement).append(`<tr id='` + row_id + `'>` + rowHTML + `</tr>`);


                    $("#add_document").modal("hide");
                    $("#doc_form").trigger("reset");
                    attachment.clear();
                    this.pickedDocs[row_id] = doc;
                }
            },
            showAttachment: function (image, mime) {
                $('#document_preview_modal').modal('show');
                $('#attachment_preview').attr("src", "data:" + mime + ";base64," + image);
            }

        };
