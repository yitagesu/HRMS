<%@page import="com.intaps.nrlais.repo.*"%>
<%@page import="com.intaps.nrlais.*"%>
<%@page import="com.intaps.nrlais.model.*"%>
<%@page import="com.intaps.nrlais.model.tran.*"%>
<%@page import="com.intaps.nrlais.util.*"%>
<%@page import="com.intaps.nrlais.controller.*"%>
<%@page import="com.intaps.nrlais.controller.tran.*"%>
<%@page import="com.intaps.nrlais.lang.*"%>
<%@page import="java.util.*"%>
<%@page import="java.util.HashMap"%>
<%@page pageEncoding="UTF-8" %>
<%
    String lang=Startup.getSessionByRequest(request).lang();
    %>
var lang=function()
{
    var str_table=[];
    <%
        HashMap<String,String> table=Lang.getTable(lang);
        if(table!=null)
        {
            for(Map.Entry<String,String> e :table.entrySet())
            {
                %>
                str_table['<%=e.getKey()%>']='<%=e.getValue()%>';
            <%
            }
        }
    %>
    return function(text,rep)
    {
        var val=str_table[text];
        if(val)
           text=val;
        for(var i in rep)
        {
            text=text.replace("{"+i+"}",rep[i]);
        }
        return text;
    };
}();