    /* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function NrlaisMap(server, el)
{
    var zoomMargin = 1.3;
    var map;
    var view;
    proj4.defs('EPSG:20137', '+proj=utm +zone=37 +ellps=clrk80 +units=m +no_defs');
    function scaleView(view, k)
    {
        var w = view.width * k;
        var h = view.height * k;
        return {x: view.x, y: view.y, width: w, height: h};
    }
    
    //var $div1 = $("<div>",{class:"modal fade ", id:"holding_view_modal", tabindex:"-1", role:"dialog"}
        //    );
   // var $div2 = $("<div>" ,{class:"modal-content"});
                      
                  
                  
              var tiledL = layerPrepare('nrlais:nrlais_inventory.t_parcels');
              var tiledE = layerPrepare('nrlais:ne_10m_admin_0_countries');
              var tiledR = layerPrepare('nrlais:t_regions');
              var tiledZ = layerPrepare('nrlais:t_zones');
              var tiledW = layerPrepare('nrlais:t_woredas');
              var tiledK = layerPrepare('nrlais:t_kebeles');

              function layerPrepare(layer){
                  return new ol.layer.Image({
                    source: new ol.source.ImageWMS({
                      ratio: 1,
                      url: server + '/nrlais/wms',
                      params: { tiled: true, 
                            STYLES: '',
                            LAYERS: layer
                      },
                      serverType: 'geoserver',
                      crossOrigin: null
                    })
                  });
                  
              }
    function getOverlayLayers()
    {
        var layers = 'nrlais:ne_10m_admin_0_countries,nrlais:t_regions,nrlais:t_zones,nrlais:t_woredas,nrlais:t_kebeles,nrlais:nrlais_inventory.t_parcels';
        var source = new ol.source.TileWMS({
            url: server + '/nrlais/wms',
            params: {layers: layers, 'TILED': true}
        });
        var layer = new ol.layer.Tile({source: source});
        return [layer];
    }
    //styles start
    var selectionStyle = new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: 'magenta',
            width: 2
        }),
        fill: new ol.style.Fill({
            color: 'transparent'
        }),
    });
    var beforeEditStyle = new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: 'red',
            width: 5
        }),
        fill: new ol.style.Fill({
            color: 'transparent'
        }),
    });

    var afterEditStyle = new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: 'green',
            width: 2
        }),
        fill: new ol.style.Fill({
            color: 'transparent'
        }),
    });
    //styles end
    var state = {
        initialized: false,
        map: null,
        view: null,
        selectionLayer: null,
        selectionSource: null,
        selectionData: null,
        onMapReady: null,
        zoomHolding: function (schema, huid)
        {
            if(!schema || !huid)
                return;

            var p = $.proxy(function (data)
            {
                if (data.error == null && data.res)
                {
                    data = data.res;
                    var dw = $("#" + el).width();
                    var dh = $("#" + el).height();
                    var hres = data.width * zoomMargin / dw;
                    var vres = data.height * zoomMargin / dh;
                    var res = vres > hres ? vres : hres;
                    this.view.animate({
                        center: [data.x, data.y],
                        resolution: res,
                        projection: 'EPSG:20137',
                    });
                    this.selectionData = data.gj;
                    this.selectionSource.clear();
                } else
                    console.log(data.error);
            }, state);
            $.ajax({
                url: '/api/map/get_parcels?type=holding&schema=' + schema + "&huid=" + huid,
                method: "GET",
                dataType: 'json',
                success: p}
            );

        },
        zoomParcel: function (schema, puid)
        {
            if(!schema || !puid)
                return;

            var p = $.proxy(function (data)
            {
                if (data.error == null && data.res)
                {
                    data = data.res;
                    var dw = $("#" + el).width();
                    var dh = $("#" + el).height();
                    var hres = data.width * zoomMargin / dw;
                    var vres = data.height * zoomMargin / dh;
                    var res = vres > hres ? vres : hres;
                    this.view.animate({
                        center: [data.x, data.y],
                        resolution: res,
                        projection: 'EPSG:20137',
                    });
                    this.selectionData = data.gj;
                    this.selectionSource.clear();
                } else
                    console.log(data.error);
            }, state);
            $.ajax({
                url: '/api/map/get_parcels?type=parcel&schema=' + schema + "&puid=" + puid,
                method: "GET",
                dataType: 'json',
                success: p}
            );

        },
        
        zoomKebele: function (schema, kcode)
        {
            if(!schema || !kcode)
                return;

            var p = $.proxy(function (data)
            {
                console.log(data);
                if (data.error == null && data.res)
                {
                    
                    data = data.res;
                    var dw = $("#" + el).width();
                    var dh = $("#" + el).height();
                    var hres = data.width * zoomMargin / dw;
                    var vres = data.height * zoomMargin / dh;
                    var res = vres > hres ? vres : hres;
                    this.view.animate({
                        center: [data.x, data.y],
                        resolution: res,
                        projection: 'EPSG:20137',
                    });
                    this.selectionData = data.gj;
                    this.selectionSource.clear();
                } else
                    console.log(data.error);
            }, state);
            $.ajax({
                url: '/api/map/get_parcels?type=kebele&schema=' + schema + "&kcode=" + kcode,
                method: "GET",
                dataType: 'json',
                success: p}
            );

        },
        
        
        
        
        showTransactionParcels: function (tranUID)
        {
            var p = $.proxy(function (data)
            {
                if (data.error == null)
                {
                    data = data.res;
                    var dw = $("#" + el).width();
                    var dh = $("#" + el).height();
                    var hres = data.width * zoomMargin / dw;
                    var vres = data.height * zoomMargin / dh;
                    var res = vres > hres ? vres : hres;
                    this.view.animate({
                        center: [data.x, data.y],
                        resolution: res,
                        projection: 'EPSG:20137',
                    });
                    this.selectionData = data.gj;
                    this.selectionSource.clear();
                } else
                    console.log(data.error);
            }, state);
            $.ajax({
                url: '/api/map/get_parcels?type=tran&tran_uid=' + tranUID,
                success: p,
                dataType: 'json',
            });
        },
        setupSelectionLayer: function ()
        {
            var selectionLoader = function (extent, resolution, projection)
            {
                if (this.selectionData)
                {
                    this.selectionSource.addFeatures((new ol.format.GeoJSON(
                            {
                                defaultDataProjection: "EPSG:20137",
                                featureProjection: "EPSG:20137"
                            })).readFeatures(this.selectionData));
                }
            }.bind(this);
            this.selectionSource = new ol.source.Vector({
                loader: selectionLoader
            });
            this.selectionLayer = new ol.layer.Vector({
                source: this.selectionSource,
                style: function (feature)
                {
                    if (feature.get('editStatus') == 'd')
                        return beforeEditStyle;
                    else if (feature.get('editStatus') == 'n')
                        return afterEditStyle;
                    else
                        return selectionStyle;
                }
            });
        },
        mapReady: false,
        zoomBox: function (x, y, w, h)
        {
            if (!this.mapReady)
                return;
            var dw = $("#" + el).width();
            var dh = $("#" + el).height();
            var hres = w * zoomMargin / dw;
            var vres = h * zoomMargin / dh;
            var res = vres > hres ? vres : hres;
            this.view.animate({
                center: [x, y],
                resolution: res,
                projection: 'EPSG:20137',
            });
        },
        load: function ()
        {

            var p = $.proxy(function (data)
            {
                data = data.res;
                var dw = $("#" + el).width();
                var dh = $("#" + el).height();
                var hres = data.width * zoomMargin / dw;
                var vres = data.height * zoomMargin / dh;
                var res = vres > hres ? vres : hres;


                this.view = new ol.View({
                    center: [data.x, data.y],
                    resolution: res,
                    projection: 'EPSG:20137',
                    maxResolution: 10000,
                    minResolution: 0.1,
                });
                //var layers = getOverlayLayers();
               
                this.setupSelectionLayer();
                var ls = [tiledE,tiledR,tiledZ,tiledW,tiledK,tiledL];
                ls.push(this.selectionLayer);
                this.map = new ol.Map({
                    layers: ls,
                    view: this.view,
                    target: el
                });
               
                var mapl = this.map;
                      mapl.on('singleclick', function(evt) {
                        var view = mapl.getView();
                        var viewResolution = view.getResolution();
                        var source = tiledL.getSource();
                        var url = source.getGetFeatureInfoUrl(
                          evt.coordinate, viewResolution, view.getProjection(),
                          {'INFO_FORMAT': 'application/json','propertyName':'uid'});
                        if (url) {
                         
                                 var parser = new ol.format.GeoJSON();
                                $.ajax({
                                  type: 'GET',
                                  url: url,
                                  contentType: 'text/plain',
                                    xhrFields: {
                                     withCredentials: false
                                       },
                                     headers: {
                                     },
                                  dataType: 'json',
                                  jsonpCallback: 'parseResponse',
                                }).then(function(response) {
                                 
                                  var result = parser.readFeatures(response);
                                  if (result.length) {
                                      
                                    //var info = [];
                                    
                                      $.ajax({
                                                method: 'GET',
                                                url: "/api/holding_from_map",
                                                contentType :"application/json",
                                                data: {uid:result[0].get('uid')},
                                                success:function(result){
                                               $.ajax('/holding/holding_information_viewer.jsp?holding_uid=' + result.res, {
                                                'method': 'GET',
                                                'success': function (data) {
                                                    
                                                    $("#holding_view_modal").modal('show');
                                                    $('#holdingContent').html(data);
                                                    $('#holdingContent [parcel_uid]').each(function()
                                                    {
                                                       var mapid=$(this).attr("id");
                                                       var m=NrlaisMap(map_server,mapid);
                                                    m.load();
                                                    var parcel_uid=$(this).attr("parcel_uid");
                                                    m.onMapReady=function()
                                                    {
                                                        m.zoomParcel("nrlais_inventory",parcel_uid);
                                                    };
                                                    });
                                                },
                                                'error': function (error) {
                                                    console.log(error);
                                                    bootbox.alert("error occord:" + error);
                                                }
                                            });
                                                   
                                                   
                                                   
                                                },
                                                error:function(err){
                                                    error(err);
                                                }
                                        });
                                  }
                                });
                      }
                      
                 
                  
                      });

                this.mapReady = true;
                if (this.onMapReady)
                    this.onMapReady();
            }, state);
            $.ajax({
                url: '/api/map/get_setting',
                method: "GET",
                dataType: 'json',
                success: p
            });

        }
    };

    return state;
}
