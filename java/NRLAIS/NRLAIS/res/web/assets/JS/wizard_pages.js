var wizardPages =
        {
            setup: {},
            current: null,
            tab_template: null,
            gotoNext: function ()
            {
                var found = this.current == null;
                for (var key in this.setup)
                {
                    if (found)
                    {
                        this.openPage(key);
                        return;
                    }
                    found = key == this.current;
                }
            },
            load: function (config)//[{tab:,cont:,enable:}..]
            {
                if(!this.current && config.length>0)
                    this.current=config[0].tab;
                
                this.setup=[];
                for (var i = 0; i < config.length; i++)
                {
                    if (this.current==config[i].tab)
                    {
                        $(config[i].tab).attr("class", "btn btn-primary btn-circle");
                        this.current = config[i].tab;
                        $(config[i].cont).show();
                    } else
                    {
                        $(config[i].cont).hide();
                        $(config[i].tab).attr("class", "btn btn-default btn-circle");
                    }
                    this.setup[config[i].tab] = config[i];
                    $(config[i].tab).prop("disabled", !config[i].enable);
                }
            },
            
            beforePageChange: null,
            afterPageChange: null,
            setTabEnaled: function (id, enable)
            {
                setup[id].enable = false;
                $(setup[id].tab).prop("disabled", !enable);
            },

            buildNavHtml: function (container,config)
            {
                var tabs = [];
                for (var i in config)
                {
                    var s=config[i];
                    tabs.push({id: s.tab.substring(1), page_id: s.cont.substring(1), active: this.current == s});
                }
                if (this.tab_template)
                {
                        var html=ejs.render(this.tab_template, {tabs:tabs});
                        $(container).html(html);
                        this.load(config);
                        return;
                } 
                var that = this;
                $.ajax({url:'/assets/JS/wizard_pages_buttons.ejs',success:function
                    (template)
                    {
                        that.tab_template = template;
                        var html=ejs.render(that.tab_template, {tabs:tabs});
                        $(container).html(html);
                        that.load(config);
                    }
                });
            },
            openPage: function (id)
            {
                var show = null, hide = null;
                for (var key in this.setup)
                {
                    if (this.setup[key].tab === id)
                    {
                        show = key;
                        break;
                    }
                }

                if (!show)
                    return;

                if (this.current == show)
                    return;


                if (this.beforePageChange)
                    if (!this.beforePageChange(show, this.current))
                        return;
                $(this.setup[show].cont).show();
                $(this.setup[show].tab).attr("class", "btn btn-primary btn-circle");
                if (this.current)
                {
                    $(this.setup[this.current].cont).hide();
                    $(this.setup[this.current].tab).attr("class", "btn btn-default btn-circle");
                }
                this.current = show;

                if (this.afterPageChange)
                    this.afterPageChange(show, hide);
            }
        };