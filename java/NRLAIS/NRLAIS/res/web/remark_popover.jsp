<div id="remarkPopover_accept" class="popover popover-x popover-default">
    <div class="arrow"></div>
    <h3 class="popover-header popover-title"><span class="close pull-right" data-dismiss="popover-x">&times;</span>Enter Your Remark</h3>
    <form class="form-vertical">
        <div class="popover-body popover-content">


            <div class="form-group">
                <textarea id="text_accept" rows="5" class="form-control" placeholder="Write Your Remark Here!"></textarea>
            </div>

        </div>
        <div class="popover-footer align-center">
            <a target='_parent' href="javascript:transaction_change_state('<%=controller.tran.transactionUID%>','<%=controller.approveCommand()%>',$('#text_accept').val())" class="btn btn-sm btn-primary">Submit</a>
        </div>
    </form>
</div>
<div id="remarkPopover_reject" class="popover popover-x popover-default">
    <div class="arrow"></div>
    <h3 class="popover-header popover-title"><span class="close pull-right" data-dismiss="popover-x">&times;</span>Enter Your Remark</h3>
    <form class="form-vertical">
        <div class="popover-body popover-content">


            <div class="form-group">
                <textarea rows="5" class="form-control" placeholder="Write Your Remark Here!" id="text_reject"></textarea>
            </div>

        </div>
        <div class="popover-footer">

            <a target='_parent' href="javascript:transaction_change_state('<%=controller.tran.transactionUID%>','<%=controller.rejectCommand()%>',$('#text_reject').val())" class="btn btn-sm btn-primary">Submit</a>

        </div>
    </form>
</div>