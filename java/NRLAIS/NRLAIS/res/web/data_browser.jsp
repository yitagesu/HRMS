<%-- 
    Document   : register_transaction
    Created on : Dec 25, 2017, 11:23:08 AM
    Author     : yitagesu
--%>
<%@page import="com.intaps.nrlais.repo.*"%>
<%@page import="com.intaps.nrlais.*"%>
<%@page import="com.intaps.nrlais.controller.*"%>
<%@page import="com.intaps.nrlais.controller.tran.*"%>
<%@page import="com.intaps.nrlais.model.*"%>
<%@page import="java.util.*"%>
<%@page pageEncoding="UTF-8" %>
<%
    DataBrowserViewController controller=new DataBrowserViewController(request,response);
    HeaderViewController headerController=new HeaderViewController(request,response,false);
%>
<html>
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>NRLAIS</title>
        <link rel="stylesheet" href="/assets/CSS/bootstrap.min.css"/>
        <link rel="stylesheet" href="/assets/CSS/custom.css"/>
        <link rel="stylesheet" href="/assets/ol/ol.css"/>
        <!--        third party-->
        <script src="/assets/JS/jquery.min.js"></script>
        <script src="/assets/JS/jquery-ui.js"></script>
        <script src="/assets/JS/bootstrap.min.js"></script>
        <script src="/assets/JS/bootbox.js"></script>
        <script src="/assets/JS/string.js"></script>

        <!--        INTAPS common-->
        <script src="assets/JS/et_dual_cal.js"></script>
        <script src="/assets/JS/general.js"></script>

        <!--        NRLAIS general-->
        <script src="/assets/JS/js_constants.jsp"></script>
        <script src="assets/JS/document_picker.js"></script>        
        <script src="/holding/searching_holding.js"></script>
        <script src="/assets/JS/transaction.js"></script>
        <script src="/assets/JS/attachment.js"></script>
        <script src="/holding/applicants_picker.js"></script>
        <script src="/holding/parcel_split_table.js"></script>
        <script src="/holding/parcel_transfer_table.js"></script>
        <script src="/party/register_party.js"></script>
        <script src="/assets/JS/nrlais_map.js"></script>
        <script src="/assets/ol/ol.js"></script>
        <script src="/assets/ol/proj4.js"></script>

        <!--        NRLAIS transaction specific-->
        <script src="/holding/searching_holding.js"></script>
        <script src="/assets/JS/data_browser.js"></script>


        <script>
            var dataBrowser_search;
            var map_server = '<%=controller.mapServerUrl()%>';

            $(document).ready(function ()
            {
                transcation_initMap(map_server);

                $(document).on('click', "div.bhoechie-tab-menu>div.list-group>a", function (e) {
                    e.preventDefault();
                    $(this).siblings('a.active').removeClass("active");
                    $(this).addClass("active");
                    var index = $(this).index();
                    $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
                    $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
                });

                dataBrowser_search = holdingBrowser("#search_container", false);
            });
        </script>
    </head>

    <body id="body">
        <%@ include file="header.jsp" %>
        <div class="dashboard-body">
            <div class="list-of-transc col-md-7" style='height:95%'>
                <div id="reg_application" style='height:100%'>
                    <div class="x_panel " id="search_container" style='height:100%'>
                        <%{%>
                        <%@ include file="holding/holding_search.jsp" %>
                        <%}%>
                    </div>
                </div>
            </div>

            <div class="map col-md-5" style='height:95%'>
                <div class="x_panel" style='height:100%'>
                    <div class="x_content has-scroll" id="map" style='height:100%'>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade " id="holding_view_modal" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                    </div>            <!-- /modal-header -->

                    <div class="modal-body">
                        <div id="holdingContent">

                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="input-form">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade " id="application_detail" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>            <!-- /modal-header -->

                    <div class="modal-body" id="transactionContent">
                        <iframe id="transactionContent-iframe" name="iframe"></iframe>

                    </div>
                    <div class="modal-footer"> </div>
                </div>
            </div>
        </div>         
    </body>

</html>

