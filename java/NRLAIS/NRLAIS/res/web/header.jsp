<%@page import="com.intaps.nrlais.*"%>
<%@page import="com.intaps.nrlais.api.*"%>
<%@page import="com.intaps.nrlais.model.*"%>
<%@page import="com.intaps.nrlais.viewmodel.*"%>
<%@page import="com.intaps.nrlais.model.tran.*"%>
<%@page import="com.intaps.nrlais.repo.*"%>
<%@page import="com.intaps.nrlais.tran.*"%>
<%@page import="com.intaps.nrlais.util.*"%>
<%@page import="com.intaps.nrlais.controller.*"%>
<%@page import="java.util.*"%>
<%@page pageEncoding="UTF-8" %>
<script src="/assets/JS/login.js"></script>
<script>
    var wipss_session_id='<%=headerController.wipssSessionID()%>'
    function gotoWipss()
    {
        document.cookies="connect.sid="+wipss_session_id;    
        document.location="<%=headerController.wipssUrl()%>";
    }
</script>
<div class="header">
    <div class="col-md-6">
        <h1><a href="/dashboard.jsp"><i class="fa fa-home"></i></a><%=headerController.text("NRLAIS")%>/<small><%=headerController.text("WORLAIS")%></small></h1>
    </div>
    <div class="col-md-6 align-right application_head">
        <div class="dropdown" style="visibility:<%=headerController.showNewTranButton?"visible":"hidden"%>">
            <button type="button" class="btn btn-transaction dropdown-toggle " data-toggle="dropdown">
                <strong><%=headerController.text("New Application")%></strong>
                <i class="fa fa-caret-down"></i>
            </button>

            <div class="dropdown-menu dropdown-menu-right transaction-dropdown-menu has-scroll">

                <div class="col-md-2 transaction">
                    <article class="media event">
                        <a class="pull-left date" href="register_transaction.jsp?tran_type=<%=LATransaction.TRAN_TYPE_BOUNDARY_CORRECTION%>">
                            <p class="day">01</p>
                            <p class="month"><%=headerController.text("Boundary Correction")%></p>
                        </a>
                    </article>
                </div>
                <div class="col-md-2 transaction">
                    <article class="media event">
                        <a class="pull-left date" href="register_transaction.jsp?tran_type=<%=LATransaction.TRAN_TYPE_DIVORCE%>">
                            <p class="day">02</p>
                            <p class="month"><%=headerController.text("Divorce")%></p>
                        </a>
                    </article>
                </div>
                <div class="col-md-2 transaction" >
                    <article class="media event">
                        <a class="pull-left date" href="register_transaction.jsp?tran_type=<%=LATransaction.TRAN_TYPE_EX_OFFICIO%>">
                            <p class="day">03</p>
                            <p class="month"><%=headerController.text("Ex-Officio")%></p>
                        </a>
                    </article>
                </div>
                <div class="col-md-2 transaction">
                    <article class="media event">
                        <a class="pull-left date" href="register_transaction.jsp?tran_type=<%=LATransaction.TRAN_TYPE_EXCHANGE%>">
                            <p class="day">04</p>
                            <p class="month"><%=headerController.text("Exchange")%></p>
                        </a>
                    </article>
                </div>
                <div class="col-md-2 transaction">
                    <article class="media event">
                        <a class="pull-left date" href="register_transaction.jsp?tran_type=<%=LATransaction.TRAN_TYPE_EXPROPRIATION%>">
                            <p class="day">05</p>
                            <p class="month"><%=headerController.text("Expropriation")%></p>
                        </a>
                    </article>
                </div>
                <div class="col-md-2 transaction">
                    <article class="media event">
                        <a class="pull-left date" href="register_transaction.jsp?tran_type=<%=LATransaction.TRAN_TYPE_INHERITANCE_WITHWILL%>&withwill=true">
                            <p class="day">06</p>
                            <p class="month"><%=headerController.text("Inheritance with Will")%></p>
                        </a>
                    </article>
                </div>
                <div class="col-md-2 transaction">
                    <article class="media event">
                        <a class="pull-left date" href="register_transaction.jsp?tran_type=<%=LATransaction.TRAN_TYPE_INHERITANCE%>&withwill=false">
                            <p class="day">07</p>
                            <p class="month"><%=headerController.text("Inheritance without Will")%></p>
                        </a>
                    </article>
                </div>
                <div class="col-md-2 transaction">
                    <article class="media event">
                        <a class="pull-left date" href="register_transaction.jsp?tran_type=<%=LATransaction.TRAN_TYPE_GIFT%>">
                            <p class="day">08</p>
                            <p class="month"><%=headerController.text("Gift")%></p>
                        </a>
                    </article>
                </div>                    
                <div class="col-md-2 transaction">
                    <article class="media event">
                        <a class="pull-left date" href="register_transaction.jsp?tran_type=<%=LATransaction.TRAN_TYPE_CONSOLIDATION%>">
                            <p class="day">09</p>
                            <p class="month"><%=headerController.text("Consolidation")%></p>
                        </a>
                    </article>
                </div>
                <div class="col-md-2 transaction">
                    <article class="media event">
                        <a class="pull-left date" href="register_transaction.jsp?tran_type=<%=LATransaction.TRAN_TYPE_SPLIT%>">
                            <p class="day">10</p>
                            <p class="month"><%=headerController.text("Split")%></p>
                        </a>
                    </article>
                </div>
                <div class="col-md-2 transaction">
                    <article class="media event">
                        <a class="pull-left date" href="register_transaction.jsp?tran_type=<%=LATransaction.TRAN_TYPE_REALLOCATION%>">
                            <p class="day">11</p>
                            <p class="month"><%=headerController.text("Reallocation")%></p>
                        </a>
                    </article>
                </div>
                <div class="col-md-2 transaction">
                    <article class="media event">
                        <a class="pull-left date" href="register_transaction.jsp?tran_type=<%=LATransaction.TRAN_TYPE_RENT%>">
                            <p class="day">12</p>
                            <p class="month"><%=headerController.text("Rent")%>/<%=headerController.text("Lease")%></p>
                        </a>
                    </article>
                </div>
                <div class="col-md-2 transaction">
                    <article class="media event">
                        <a class="pull-left date" href="register_transaction.jsp?tran_type=<%=LATransaction.TRAN_TYPE_REPLACE_CERTIFICATE%>">
                            <p class="day">13</p>
                            <p class="month"><%=headerController.text("Replacement of Certificate")%></p>
                        </a>
                    </article>
                </div>
                <div class="col-md-2 transaction">
                    <article class="media event">
                        <a class="pull-left date" href="register_transaction.jsp?tran_type=<%=LATransaction.TRAN_TYPE_RESTRICTIVE_INTEREST%>">
                            <p class="day">14</p>
                            <p class="month"><%=headerController.text("Restrictive Interest")%></p>
                        </a>
                    </article>
                </div>
                <div class="col-md-2 transaction">
                    <article class="media event">
                        <a class="pull-left date" href="register_transaction.jsp?tran_type=<%=LATransaction.TRAN_TYPE_SERVITUDE%>">
                            <p class="day">15</p>
                            <p class="month"><%=headerController.text("Servitude")%>/<%=headerController.text("Easement")%></p>
                        </a>
                    </article>
                </div>
                <div class="col-md-2 transaction">
                    <article class="media event">
                        <a class="pull-left date" href="register_transaction.jsp?tran_type=<%=LATransaction.TRAN_TYPE_SIMPLE_CORRECTION%>">
                            <p class="day">16</p>
                            <p class="month"><%=headerController.text("Simple Correction")%></p>
                        </a>
                    </article>
                </div>
                <div class="col-md-2 transaction">
                    <article class="media event">
                        <a href="register_transaction.jsp?tran_type=<%=LATransaction.TRAN_TYPE_SPECIAL_CASE%>" class="pull-left date">
                            <p class="day">17</p>
                            <p class="month"><%=headerController.text("Special Case")%></p>
                        </a>
                    </article>
                </div>
            </div>
        </div> 
        <div class="dropdown">
            <button type="button" class="btn-welcome dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-user"></i>
                <%=headerController.text("WELCOME")%>: <strong><%=headerController.userFullName()%></strong>
                <i class="fa fa-caret-down"></i>
            </button>
            <ul class="dropdown-menu dropdown-menu-right">
               
                <li>
                    <a href="/data_browser.jsp" id="" data-toggle='modal' data-target='' data-backdrop='static'><i class="fa fa-unlock-alt"></i>   <span><%=headerController.text("Data Browser")%></span></a>
                </li>
                <li>
                    <a href="javascript:application_logout()" id="logoutButton"><i class="fa fa-sign-out"></i> <%=headerController.text("Logout")%></a>
                </li>
<!--                <li>
                    <a href="javascript:gotoWipss()" id="logoutButton"></i> Goto WIPSS</a>
                </li>-->
            </ul>

        </div> 
    </div>
</div>
<%%>
