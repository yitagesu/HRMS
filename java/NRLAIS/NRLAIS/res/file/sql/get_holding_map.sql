/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Tewelde
 * Created: Jan 26, 2018
 */
--for use with StringUtils.replaceEach
Select 
	(SELECT jsonb_build_object(
    'type',     'FeatureCollection',
    'features', jsonb_agg(feature)
)
FROM (
  SELECT jsonb_build_object(
    'type',       'Feature',
    'id',         row.uid,
    'geometry',   ST_AsGeoJSON(geometry)::jsonb,
    'properties', to_jsonb(row) - 'uid' - 'geometry'
  ) AS feature
  FROM (

Select t_parcels.*,t_holdings.uid as holdinguid from 
@schema.t_holdings 
inner join @schema.t_sys_fc_holding on t_sys_fc_holding.holdinguid=t_holdings.uid
inner join @schema.fdconnector on fdconnector.uid=t_sys_fc_holding.fdc_uid
inner join @schema.t_parcels on t_parcels.uid=fdconnector.wfsid
where t_holdings.uid='@holdinguid'
) row) features)
,
(Select ST_Extent(t_parcels.geometry) from 
@schema.t_holdings 
inner join @schema.t_sys_fc_holding on t_sys_fc_holding.holdinguid=t_holdings.uid
inner join @schema.fdconnector on fdconnector.uid=t_sys_fc_holding.fdc_uid
inner join @schema.t_parcels on t_parcels.uid=fdconnector.wfsid
where t_holdings.uid='@holdinguid')
