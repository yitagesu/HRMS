/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Tewelde
 * Created: Jan 11, 2018
 */

INSERT INTO nrlais_transaction.t_partycontactdata(
	uid, csaregionid, nrlais_zoneid, nrlais_woredaid, nrlais_kebeleid, syscreateby, syscreatedate, syslastmodby, syslastmoddate, notes, partyuid, contactdata, contactdatatype,  contactdatatypeother, editstatus, currenttxuid)
	VALUES (
@uid::uuid, 
@csaregionid, 
@nrlais_zoneid, 
@nrlais_woredaid, 
@nrlais_kebeleid, 
@syscreateby, 
@syscreatedate, 
@syslastmodby, 
@syslastmoddate, 
@notes, 
@partyuid::uuid, 
@contactdata, 
@contactdatatype,  
@contactdatatypeother,
'n',
@currenttxuid::uuid);

INSERT INTO nrlais_inventory.t_transactioncontent(
	uid, csaregionid, nrlais_zoneid, nrlais_woredaid, nrlais_kebeleid, syscreateby, syscreatedate, syslastmodby, syslastmoddate, notes, txuid, inputobjecttype, inputobjectuid, outputobjecttype, outputobjectuid, task, isprimary, status)
	VALUES (
uuid_generate_v4(), 
@csaregionid, 
@nrlais_zoneid, 
@nrlais_woredaid, 
@nrlais_kebeleid, 
@syscreateby, 
@syscreatedate, 
@syslastmodby, 
@syslastmoddate, 
@notes, 
@currenttxuid::uuid, 
18, 
@uid::uuid, 
null, 
null, 
'AWFSS Task', 
true, 
2
);