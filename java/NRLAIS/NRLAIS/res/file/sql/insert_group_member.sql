/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Tewelde
 * Created: Jan 5, 2018
 */

INSERT INTO nrlais_transaction.t_groupparty(
	uid, csaregionid, nrlais_zoneid, nrlais_woredaid, nrlais_kebeleid, syscreateby, syscreatedate, syslastmodby, syslastmoddate, notes, grouppartyuid, memberpartyuid, sharenumerator, sharedenominator, editstatus, currenttxuid)
	VALUES (
@uid::uuid, 
@csaregionid, 
@nrlais_zoneid, 
@nrlais_woredaid, 
@nrlais_kebeleid, 
@syscreateby, 
@syscreatedate, 
@syslastmodby, 
@syslastmoddate, 
@notes, 
@grouppartyuid::uuid, 
@memberpartyuid::uuid, 
@sharenumerator, 
@sharedenominator, 
'n', 
@currenttxuid::uuid);

INSERT INTO nrlais_inventory.t_transactioncontent(
	uid, csaregionid, nrlais_zoneid, nrlais_woredaid, nrlais_kebeleid, syscreateby, syscreatedate, syslastmodby, syslastmoddate, notes, txuid, inputobjecttype, inputobjectuid, outputobjecttype, outputobjectuid, task, isprimary, status)
	VALUES (
uuid_generate_v4(), 
@csaregionid, 
@nrlais_zoneid, 
@nrlais_woredaid, 
@nrlais_kebeleid, 
@syscreateby, 
@syscreatedate, 
@syslastmodby, 
@syslastmoddate, 
@notes, 
@currenttxuid::uuid, 
19, 
@uid::uuid, 
null, 
null, 
'AWFSS Task', 
true, 
2
);