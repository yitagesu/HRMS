/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Tewelde
 * Created: Jan 8, 2018
 */

INSERT INTO nrlais_inventory.t_adminsource(
	uid, csaregionid, nrlais_zoneid, nrlais_woredaid, nrlais_kebeleid, syscreateby, syscreatedate, syslastmodby, syslastmoddate, notes, adminsourcetype, adminsourceref, adminsourcearchivetype, adminsourcedescription, txuid, mimetype, voidstatus)
	VALUES (
@uid::uuid, 
@csaregionid, 
@nrlais_zoneid, 
@nrlais_woredaid, 
@nrlais_kebeleid, 
@syscreateby, 
@syscreatedate, 
@syslastmodby, 
@syslastmoddate, 
@notes, 
@adminsourcetype, 
@adminsourceref, 
@adminsourcearchivetype, 
@adminsourcedescription, 
@txuid::uuid, 
@mimetype, 
@voidstatus
);