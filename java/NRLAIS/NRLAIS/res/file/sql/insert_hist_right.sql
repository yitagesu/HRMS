/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Tewelde
 * Created: Jan 25, 2018
 */

INSERT INTO nrlais_historic.t_rights(
	id, csaregionid, nrlais_zoneid, nrlais_woredaid, nrlais_kebeleid, inventoryid, sourcetx, archivetx, archivedate, type, holdingid, partyid, right_attr)
	VALUES (
@id::uuid, 
@csaregionid, 
@nrlais_zoneid, 
@nrlais_woredaid, 
@nrlais_kebeleid, 
@inventoryid::uuid, 
@sourcetx::uuid, 
@archivetx::uuid, 
@archivedate, 
@type, 
@holdingid::uuid, 
@partyid::uuid, 
@data::json
);