/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Tewelde
 * Created: Jan 11, 2018
 */

Update nrlais_transaction.t_partycontactdata
set
csaregionid=@csaregionid,
nrlais_zoneid=@nrlais_zoneid,
nrlais_woredaid=@nrlais_woredaid,
nrlais_kebeleid=@nrlais_kebeleid,
syscreateby=@syscreateby,
syscreatedate=@syscreatedate,
syslastmodby=@syslastmodby,
syslastmoddate=@syslastmoddate,
notes=@notes,
partyuid=@partyuid::uuid,
contactdata=@contactdata,
contactdatatype=@contactdatatype,
editstatus=(Select case when editstatus='n' then 'n' else 'u' end from nrlais_transaction.t_partycontactdata where uid=@uid::uuid),
currenttxuid=@currenttxuid::uuid,
contactdatatypeother=@contactdatatypeother
where uid=@uid::uuid