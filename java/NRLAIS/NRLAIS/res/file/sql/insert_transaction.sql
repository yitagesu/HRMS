INSERT INTO nrlais_inventory.t_transaction(
	uid, csaregionid, nrlais_zoneid, nrlais_woredaid, nrlais_kebeleid, syscreateby, syscreatedate, syslastmodby, syslastmoddate, notes, transactiontype, txstatus, txyear, txseqnr, txdate, syssessionid, spatialtask)
	VALUES (
@uid::uuid, 
@csaregionid, 
@nrlais_zoneid, 
@nrlais_woredaid, 
@nrlais_kebeleid, 
@syscreateby, 
@syscreatedate, 
@syslastmodby, 
@syslastmoddate, 
@notes, 
@transactiontype, 
@txstatus, 
@txyear, 
@txseqnr, 
@txdate, 
@syssessionid::uuid, 
@spatialtask);

INSERT INTO nrlais_inventory.t_transaction_flow(
	uid, csaregionid, nrlais_zoneid, nrlais_woredaid, nrlais_kebeleid, syscreateby, syscreatedate, syslastmodby, syslastmoddate, notes, txuid, oldstatus, newstatus, flowseq)
	VALUES (
@flow_uid::uuid, 
@csaregionid, 
@nrlais_zoneid, 
@nrlais_woredaid, 
@nrlais_kebeleid, 
@syscreateby, 
@syscreatedate, 
@syslastmodby, 
@syslastmoddate, 
@notes, 
@uid::uuid, 
0, 
1, 
1);

INSERT INTO nrlais_inventory.t_transaction_data(tx_uid) values (@uid::uuid);
