INSERT INTO nrlais_transaction.t_parcels(
	uid, csaregionid, nrlais_zoneid, nrlais_woredaid, nrlais_kebeleid, syscreateby, syscreatedate, syslastmodby, syslastmoddate, seqnr, upid, areageom, arealegal, areasurveyed, adjudicationid, adjudicatedby, geometry, referencepoint, landuse, level, notes, isprimary, editstatus, currenttxuid, mreg_teamid, mreg_mapsheet, mreg_stage, mreg_actype, mreg_acyear, soilfertilitytype, mreg_surveydate)
	VALUES (
@uid::uuid, 
@csaregionid, 
@nrlais_zoneid, 
@nrlais_woredaid, 
@nrlais_kebeleid, 
@syscreateby, 
@syscreatedate, 
@syslastmodby, 
@syslastmoddate, 
@seqnr, 
@upid, 
@areageom, 
@arealegal, 
@areasurveyed, 
@adjudicationid, 
@adjudicatedby, 
ST_GeomFromText(@geometry), 
ST_GeomFromText(@referencepoint), 
@landuse, 
@level, 
@notes, 
@isprimary, 
'n', 
@currenttxuid::uuid, 
@mreg_teamid, 
@mreg_mapsheet, 
@mreg_stage, 
@mreg_actype, 
@mreg_acyear, 
@soilfertilitytype, 
@mreg_surveydate
);

Update nrlais_transaction.t_parcels set areageom=ST_Area(geometry) where uid=@uid::uuid;

INSERT INTO nrlais_inventory.t_transactioncontent(
	uid, csaregionid, nrlais_zoneid, nrlais_woredaid, nrlais_kebeleid, syscreateby, syscreatedate, syslastmodby, syslastmoddate, notes, txuid, inputobjecttype, inputobjectuid, outputobjecttype, outputobjectuid, task, isprimary, status)
	VALUES (
uuid_generate_v4(), 
@csaregionid, 
@nrlais_zoneid, 
@nrlais_woredaid, 
@nrlais_kebeleid, 
@syscreateby, 
@syscreatedate, 
@syslastmodby, 
@syslastmoddate, 
@notes, 
@currenttxuid::uuid, 
8, 
@uid::uuid, 
null, 
null, 
'AWFSS Task', 
true, 
2
);

INSERT INTO nrlais_transaction.fdconnector
(
	uid, csaregionid, nrlais_zoneid, nrlais_woredaid, nrlais_kebeleid, syscreateby, syscreatedate, syslastmodby, syslastmoddate, wfsid, modelid, editstatus, currenttxuid)
	VALUES (
@fdc_uid::uuid, 
@csaregionid, 
@nrlais_zoneid, 
@nrlais_woredaid, 
@nrlais_kebeleid, 
@syscreateby, 
@syscreatedate, 
@syslastmodby, 
@syslastmoddate, 
@uid::uuid, 
1, 
'n', 
@currenttxuid::uuid);

INSERT INTO nrlais_inventory.t_transactioncontent(
	uid, csaregionid, nrlais_zoneid, nrlais_woredaid, nrlais_kebeleid, syscreateby, syscreatedate, syslastmodby, syslastmoddate, notes, txuid, inputobjecttype, inputobjectuid, outputobjecttype, outputobjectuid, task, isprimary, status)
	VALUES (
uuid_generate_v4(), 
@csaregionid, 
@nrlais_zoneid, 
@nrlais_woredaid, 
@nrlais_kebeleid, 
@syscreateby, 
@syscreatedate, 
@syslastmodby, 
@syslastmoddate, 
@notes, 
@currenttxuid::uuid, 
3, 
@fdc_uid::uuid, 
null, 
null, 
'AWFSS Task', 
true, 
2
);

INSERT INTO nrlais_transaction.t_sys_fc_holding(
	uid, 
csaregionid, 
nrlais_zoneid, 
nrlais_woredaid, 
nrlais_kebeleid, 
syscreateby, 
syscreatedate, 
syslastmodby, 
syslastmoddate, 
fdc_uid, 
holdinguid, 
editstatus, 
currenttxuid)
	VALUES 
(@sys_fs_h_uid::uuid, 
@csaregionid, 
@nrlais_zoneid, 
@nrlais_woredaid, 
@nrlais_kebeleid, 
@syscreateby, 
@syscreatedate, 
@syslastmodby, 
@syslastmoddate, 
@fdc_uid::uuid, 
@holdinguid::uuid, 
'n', 
@currenttxuid::uuid);

INSERT INTO nrlais_inventory.t_transactioncontent(
	uid, csaregionid, nrlais_zoneid, nrlais_woredaid, nrlais_kebeleid, syscreateby, syscreatedate, syslastmodby, syslastmoddate, notes, txuid, inputobjecttype, inputobjectuid, outputobjecttype, outputobjectuid, task, isprimary, status)
	VALUES (
uuid_generate_v4(), 
@csaregionid, 
@nrlais_zoneid, 
@nrlais_woredaid, 
@nrlais_kebeleid, 
@syscreateby, 
@syscreatedate, 
@syslastmodby, 
@syslastmoddate, 
@notes, 
@currenttxuid::uuid, 
12, 
@sys_fs_h_uid::uuid, 
null, 
null, 
'AWFSS Task', 
true, 
2
);