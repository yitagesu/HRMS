--for use with StringUtils.replaceEach
Select @select from 
@schema.t_holdings 
inner join @schema.t_rights on t_holdings.uid=t_rights.holdinguid
inner join @schema.t_party on t_rights.partyuid=t_party.uid
left outer join @schema.t_groupparty on t_rights.partyuid=t_groupparty.grouppartyuid
left outer join @schema.t_party gparty on gparty.uid=t_groupparty.memberpartyuid
inner join @schema.t_sys_fc_holding on t_sys_fc_holding.holdinguid=t_holdings.uid
inner join @schema.fdconnector on fdconnector.uid=t_sys_fc_holding.fdc_uid
inner join @schema.t_parcels on t_parcels.uid=fdconnector.wfsid
where @cr