delete from nrlais_transaction.t_partycontactdata where currenttxuid=@txuid::uuid;
delete from nrlais_transaction.t_partyrole where currenttxuid=@txuid::uuid;
delete from nrlais_transaction.t_partyrelationship where currenttxuid=@txuid::uuid;
delete from nrlais_transaction.t_restrictions where currenttxuid=@txuid::uuid;
delete from nrlais_transaction.t_rights where currenttxuid=@txuid::uuid;
delete from nrlais_transaction.t_groupparty where currenttxuid=@txuid::uuid;
delete from nrlais_transaction.t_party where currenttxuid=@txuid::uuid;
delete from nrlais_transaction.t_sys_fc_holding where currenttxuid=@txuid::uuid;
delete from nrlais_transaction.t_holdings where currenttxuid=@txuid::uuid;
delete from nrlais_transaction.fdconnector where currenttxuid=@txuid::uuid;
delete from nrlais_transaction.t_parcels where currenttxuid=@txuid::uuid;

delete from nrlais_inventory.t_transactioncontent where txuid=@txuid::uuid;