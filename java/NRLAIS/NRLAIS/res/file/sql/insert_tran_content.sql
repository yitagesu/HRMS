/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Tewelde
 * Created: Jan 22, 2018
 */

INSERT INTO nrlais_inventory.t_transactioncontent(
	uid, 
csaregionid, 
nrlais_zoneid, 
nrlais_woredaid, 
nrlais_kebeleid, 
syscreateby, 
syscreatedate, 
syslastmodby, 
syslastmoddate, 
notes, 
txuid, 
inputobjecttype, 
inputobjectuid, 
outputobjecttype, 
outputobjectuid, 
task, 
isprimary, 
status)
	VALUES (
@uid::uuid, 
@csaregionid, 
@nrlais_zoneid, 
@nrlais_woredaid, 
@nrlais_kebeleid, 
@syscreateby, 
@syscreatedate, 
@syslastmodby, 
@syslastmoddate, 
@notes, 
@txuid::uuid, 
@inputobjecttype, 
@inputobjectuid::uuid, 
@outputobjecttype, 
@outputobjectuid::uuid, 
@task, 
@isprimary, 
@status);