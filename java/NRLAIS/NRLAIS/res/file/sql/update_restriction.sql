update nrlais_transaction.t_restrictions
set
csaregionid=@csaregionid,
nrlais_zoneid=@nrlais_zoneid,
nrlais_woredaid=@nrlais_woredaid,
nrlais_kebeleid=@nrlais_kebeleid,
syscreateby=@syscreateby,
syscreatedate=@syscreatedate,
syslastmodby=@syslastmodby,
syslastmoddate=@syslastmoddate,
notes=@notes,
restrictiontype=@restrictiontype,
restrictiontypeother=@restrictiontypeother,
holdinguid=@holdinguid::uuid,
partyuid=@partyuid::uuid,
editstatus=(Select case when editstatus='n' then 'n' else 'u' end from nrlais_transaction.t_restrictions where uid=@uid::uuid),
currenttxuid=@currenttxuid::uuid,
startdate=@startdate,
enddate=@enddate
where uid=@uid::uuid;
