Select distinct uid,syslastmoddate from (select t_transaction.uid, t_transaction.syslastmoddate from 
nrlais_inventory.t_transaction
left outer join nrlais_inventory.t_holdings on t_transaction.uid=t_holdings.sourcetxuid
left outer join nrlais_inventory.t_rights on t_holdings.uid=t_rights.holdinguid
left outer join nrlais_inventory.t_party on t_rights.partyuid=t_party.uid
left outer join  nrlais_inventory.t_groupparty on t_rights.partyuid=t_groupparty.grouppartyuid
left outer join nrlais_inventory.t_party gparty on gparty.uid=t_groupparty.memberpartyuid
left outer join nrlais_inventory.t_sys_fc_holding on t_sys_fc_holding.holdinguid=t_holdings.uid
left outer join nrlais_inventory.fdconnector on fdconnector.uid=t_sys_fc_holding.fdc_uid
left outer join nrlais_inventory.t_parcels on t_parcels.uid=fdconnector.wfsid
where @cr
union 
Select t_transaction.uid, t_transaction.syslastmoddate  from 
nrlais_inventory.t_transaction
left outer join nrlais_transaction.t_holdings on t_transaction.uid=t_holdings.currenttxuid
left outer join nrlais_transaction.t_rights on t_holdings.uid=t_rights.holdinguid
left outer join nrlais_transaction.t_party on t_rights.partyuid=t_party.uid
left outer join nrlais_transaction.t_groupparty on t_rights.partyuid=t_groupparty.grouppartyuid
left outer join nrlais_transaction.t_party gparty on gparty.uid=t_groupparty.memberpartyuid
left outer join nrlais_transaction.t_sys_fc_holding on t_sys_fc_holding.holdinguid=t_holdings.uid
left outer join nrlais_transaction.fdconnector on fdconnector.uid=t_sys_fc_holding.fdc_uid
left outer join nrlais_transaction.t_parcels on t_parcels.uid=fdconnector.wfsid
where @cr) res 
order by syslastmoddate desc