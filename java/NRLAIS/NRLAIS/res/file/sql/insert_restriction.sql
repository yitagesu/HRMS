/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Tewelde
 * Created: Jan 5, 2018
 */

INSERT INTO nrlais_transaction.t_restrictions(
	uid, csaregionid, nrlais_zoneid, nrlais_woredaid, nrlais_kebeleid, syscreateby, syscreatedate, syslastmodby, syslastmoddate, 
notes, restrictiontype, restrictiontypeother, holdinguid, partyuid, editstatus, currenttxuid, startdate, enddate,parceluid)
	VALUES (
@uid::uuid, 
@csaregionid, 
@nrlais_zoneid, 
@nrlais_woredaid, 
@nrlais_kebeleid, 
@syscreateby, 
@syscreatedate, 
@syslastmodby, 
@syslastmoddate, 
@notes, 
@restrictiontype, 
@restrictiontypeother, 
@holdinguid::uuid, 
@partyuid::uuid, 
'n', 
@currenttxuid::uuid, 
@startdate, 
@enddate,
@parceluid::uuid);

INSERT INTO nrlais_inventory.t_transactioncontent(
	uid, csaregionid, nrlais_zoneid, nrlais_woredaid, nrlais_kebeleid, syscreateby, syscreatedate, syslastmodby, syslastmoddate, notes, txuid, inputobjecttype, inputobjectuid, outputobjecttype, outputobjectuid, task, isprimary, status)
	VALUES (
uuid_generate_v4(), 
@csaregionid, 
@nrlais_zoneid, 
@nrlais_woredaid, 
@nrlais_kebeleid, 
@syscreateby, 
@syscreatedate, 
@syslastmodby, 
@syslastmoddate, 
@notes, 
@currenttxuid::uuid, 
9, 
@uid::uuid, 
null, 
null, 
'AWFSS Task', 
true, 
2
);