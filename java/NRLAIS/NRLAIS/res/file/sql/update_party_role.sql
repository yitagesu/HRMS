Update nrlais_transaction.t_partyrole
set     
csaregionid=@csaregionid,
nrlais_zoneid=@nrlais_zoneid,
nrlais_woredaid=@nrlais_woredaid,
nrlais_kebeleid=@nrlais_kebeleid,
syscreateby=@syscreateby,
syscreatedate=@syscreatedate,
syslastmodby=@syslastmodby,
syslastmoddate=@syslastmoddate,
notes=@notes,
partyuid=@partyuid::uuid,
roletype=@roletype,
roletypeother=@roletypeother,
editstatus=(Select case when editstatus='n' then 'n' else 'u' end from nrlais_transaction.t_partyrole where uid=@uid::uuid),
currenttxuid=@currenttxuid::uuid
where uid=@uid::uuid;



