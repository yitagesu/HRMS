Update nrlais_transaction.t_holdings
set csaregionid=@csaregionid,
nrlais_zoneid=@nrlais_zoneid,
nrlais_woredaid=@nrlais_woredaid,
nrlais_kebeleid=@nrlais_kebeleid,
syscreateby=@syscreateby,
syscreatedate=@syscreatedate,
syslastmodby=@syslastmodby,
syslastmoddate=@syslastmoddate,
seqnr=@seqnr,
uhid=@uhid,
holdingtype=@holdingtype,
notes=@notes,
editstatus=(Select case when editstatus='n' then 'n' else 'u' end from nrlais_transaction.t_holdings where uid=@uid::uuid),
currenttxuid=@currenttxuid::uuid
where uid=@uid::uuid