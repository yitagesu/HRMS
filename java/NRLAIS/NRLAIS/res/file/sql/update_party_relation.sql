update nrlais_transaction.t_partyrelationship
set
csaregionid=@csaregionid,
nrlais_zoneid=@nrlais_zoneid,
nrlais_woredaid=@nrlais_woredaid,
nrlais_kebeleid=@nrlais_kebeleid,
syscreateby=@syscreateby,
syscreatedate=@syscreatedate,
syslastmodby=@syslastmodby,
syslastmoddate=@syslastmoddate,
notes=@notes,
frompartyuid=@frompartyuid::uuid,
topartyuid=@topartyuid::uuid,
relationshiptype=@relationshiptype,
editstatus=(Select case when editstatus='n' then 'n' else 'u' end from nrlais_transaction.t_partyrelationship where uid=@uid::uuid),
currenttxuid=@currenttxuid::uuid
where uid=@uid::uuid;
