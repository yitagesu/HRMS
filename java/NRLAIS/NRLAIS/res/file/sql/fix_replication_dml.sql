Update nrlais_inventory.t_replicationlog as rr
set dml=( SELECT nrlais_tx_pkg.gen_dml(rr.operation, tt.schema, tt.tablename, rr.objuid))
from nrlais_inventory.fdtables as tt
where rr.objecttype = tt.id and rr.dml is null
