/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Tewelde
 * Created: Mar 19, 2018
 */

CREATE OR REPLACE VIEW nrlais_inventory.v_replication_log AS
 SELECT rr.woredaid,
    rr.repno,
    rr.objuid,
    rr.objecttype,
    rr.operation,
    ( SELECT nrlais_tx_pkg.gen_dml(rr.operation, tt.schema, tt.tablename, rr.objuid) AS gen_dml) AS d
   FROM nrlais_inventory.t_replicationlog rr
     JOIN nrlais_inventory.fdtables tt ON rr.objecttype = tt.id;

ALTER TABLE nrlais_inventory.v_replication_log
    OWNER TO postgres;

