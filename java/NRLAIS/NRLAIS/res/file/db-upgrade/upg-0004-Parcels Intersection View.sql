-- View: nrlais_transaction.v_cmss_parcels_intran

-- DROP VIEW nrlais_transaction.v_cmss_parcels_intran;

CREATE OR REPLACE VIEW nrlais_transaction.v_cmss_parcels_intran AS
 SELECT p.uid,
    p.csaregionid,
    p.nrlais_zoneid,
    p.nrlais_woredaid,
    p.nrlais_kebeleid,
    p.syscreateby,
    p.syscreatedate,
    p.syslastmodby,
    p.syslastmoddate,
    p.seqnr,
    p.upid,
    p.areageom,
    p.arealegal,
    p.areasurveyed,
    p.adjudicationid,
    p.adjudicatedby,
    p.geometry,
    p.referencepoint,
    p.landuse,
    p.level,
    p.notes,
    p.isprimary,
    p.editstatus,
    p.currenttxuid,
    p.mreg_teamid,
    p.mreg_mapsheet,
    p.mreg_stage,
    p.mreg_actype,
    p.mreg_acyear,
    p.soilfertilitytype,
    p.mreg_surveydate
   FROM nrlais_transaction.t_parcels p
     JOIN nrlais_inventory.t_transaction t ON p.currenttxuid = t.uid
  WHERE t.txstatus = 2;

ALTER TABLE nrlais_transaction.v_cmss_parcels_intran
    OWNER TO docker;

GRANT SELECT ON TABLE nrlais_transaction.v_cmss_parcels_intran TO user_cmss;
GRANT ALL ON TABLE nrlais_transaction.v_cmss_parcels_intran TO docker;