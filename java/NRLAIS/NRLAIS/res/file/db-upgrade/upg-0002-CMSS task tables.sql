/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Tewelde
 * Created: Jan 7, 2018
 */

-- Table: nrlais_transaction.cmss_task

-- DROP TABLE nrlais_transaction.cmss_task;

CREATE TABLE nrlais_transaction.cmss_task
(
    uid uuid,
    transaction_uid uuid,
    status integer,
    task_type integer
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE nrlais_transaction.cmss_task
    OWNER to docker;

GRANT ALL ON TABLE nrlais_transaction.cmss_task TO docker;

GRANT SELECT ON TABLE nrlais_transaction.cmss_task TO user_cmss;


-- Table: nrlais_transaction.cmss_task_geom

-- DROP TABLE nrlais_transaction.cmss_task_geom;

CREATE TABLE nrlais_transaction.cmss_task_geom
(
    id integer,
    task_uid uuid,
    geom geometry(MultiPolygon,20137) NOT NULL,
    label character varying(300) COLLATE pg_catalog."default",
    parcel_uid uuid
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE nrlais_transaction.cmss_task_geom
    OWNER to docker;

GRANT ALL ON TABLE nrlais_transaction.cmss_task_geom TO docker;

GRANT SELECT ON TABLE nrlais_transaction.cmss_task_geom TO user_cmss;


-- Table: nrlais_transaction.cmss_split_task

-- DROP TABLE nrlais_transaction.cmss_split_task;

CREATE TABLE nrlais_transaction.cmss_split_task
(
    task_uid uuid,
    parcel_uid uuid
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE nrlais_transaction.cmss_split_task
    OWNER to docker;

GRANT ALL ON TABLE nrlais_transaction.cmss_split_task TO docker;

GRANT SELECT ON TABLE nrlais_transaction.cmss_split_task TO user_cmss;


-- Table: nrlais_transaction.cmss_split_task_parcels

-- DROP TABLE nrlais_transaction.cmss_split_task_parcels;

CREATE TABLE nrlais_transaction.cmss_split_task_parcels
(
    task_uid uuid,
    target_parcel_uid uuid
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE nrlais_transaction.cmss_split_task_parcels
    OWNER to docker;

GRANT ALL ON TABLE nrlais_transaction.cmss_split_task_parcels TO docker;

GRANT SELECT ON TABLE nrlais_transaction.cmss_split_task_parcels TO user_cmss;

-- Table: nrlais_transaction.cmss_edit_task

-- DROP TABLE nrlais_transaction.cmss_edit_task_parcels;

CREATE TABLE nrlais_transaction.cmss_edit_task_parcels
(
    task_uid uuid,
    parcel_uid uuid
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE nrlais_transaction.cmss_edit_task_parcels
    OWNER to docker;

GRANT ALL ON TABLE nrlais_transaction.cmss_edit_task_parcels TO docker;

GRANT SELECT ON TABLE nrlais_transaction.cmss_edit_task_parcels TO user_cmss;

-- Table: nrlais_transaction.cmss_create_task_parcels

-- DROP TABLE nrlais_transaction.cmss_create_task_parcels;

CREATE TABLE nrlais_transaction.cmss_create_task_parcels
(
    task_uid uuid,
    target_parcel_uid uuid
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE nrlais_transaction.cmss_create_task_parcels
    OWNER to docker;

GRANT ALL ON TABLE nrlais_transaction.cmss_create_task_parcels TO docker;

GRANT SELECT ON TABLE nrlais_transaction.cmss_create_task_parcels TO user_cmss;