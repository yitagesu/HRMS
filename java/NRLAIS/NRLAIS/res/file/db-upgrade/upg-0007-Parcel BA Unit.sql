ALTER TABLE nrlais_inventory.t_rights
ADD COLUMN parceluid uuid;

ALTER TABLE nrlais_inventory.t_restrictions
ADD COLUMN parceluid uuid;


ALTER TABLE nrlais_transaction.t_rights
ADD COLUMN parceluid uuid;

ALTER TABLE nrlais_transaction.t_restrictions
ADD COLUMN parceluid uuid;

--Select * from nrlais_inventory.fdcolumns where tableid=10
INSERT INTO nrlais_inventory.fdcolumns(
	id, tableid, columnname, dbtype, jstype, displayname, issystem, displayorder, visibility, editable, defaultvalue, validator, validatordesc, codelisttable)
	VALUES ((Select max(id) from nrlais_inventory.fdcolumns)+1,10,'parceluid', 'uuid', 'String', 
			'Parcel UID',false, null, true, false, null,null, null, null);
			
INSERT INTO nrlais_transaction.fdcolumns(
	id, tableid, columnname, dbtype, jstype, displayname, issystem, displayorder, visibility, editable, defaultvalue, validator, validatordesc, codelisttable)
	VALUES ((Select max(id) from nrlais_transaction.fdcolumns)+1,10,'parceluid', 'uuid', 'String', 
			'Parcel UID',false, null, true, false, null,null, null, null);


INSERT INTO nrlais_inventory.fdcolumns(
	id, tableid, columnname, dbtype, jstype, displayname, issystem, displayorder, visibility, editable, defaultvalue, validator, validatordesc, codelisttable)
	VALUES ((Select max(id) from nrlais_inventory.fdcolumns)+1,9,'parceluid', 'uuid', 'String', 
			'Parcel UID',false, null, true, false, null,null, null, null);
			
INSERT INTO nrlais_transaction.fdcolumns(
	id, tableid, columnname, dbtype, jstype, displayname, issystem, displayorder, visibility, editable, defaultvalue, validator, validatordesc, codelisttable)
	VALUES ((Select max(id) from nrlais_transaction.fdcolumns)+1,9,'parceluid', 'uuid', 'String', 
			'Parcel UID',false, null, true, false, null,null, null, null);