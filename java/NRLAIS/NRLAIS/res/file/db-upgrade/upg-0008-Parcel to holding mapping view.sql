-- View: nrlais_inventory.v_holding_parcel

-- DROP VIEW nrlais_inventory.v_holding_parcel;

CREATE OR REPLACE VIEW nrlais_inventory.v_holding_parcel AS
 SELECT h.uid AS holdinguid,
    p.uid AS parceluid
   FROM nrlais_inventory.t_parcels p
     JOIN nrlais_inventory.fdconnector f ON p.uid = f.wfsid
     JOIN nrlais_inventory.t_sys_fc_holding s ON s.fdc_uid = f.uid
     JOIN nrlais_inventory.t_holdings h ON h.uid = s.holdinguid
  ORDER BY h.uid;

ALTER TABLE nrlais_inventory.v_holding_parcel
    OWNER TO docker;

GRANT SELECT ON TABLE nrlais_inventory.v_holding_parcel TO user_cmss;
GRANT ALL ON TABLE nrlais_inventory.v_holding_parcel TO docker;

