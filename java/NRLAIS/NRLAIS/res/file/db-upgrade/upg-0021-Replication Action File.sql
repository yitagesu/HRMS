--DROP TABLE nrlais_inventory.t_replication_actions;

CREATE SEQUENCE nrlais_inventory.seq_replication_action
    INCREMENT 1
    START 164
    MINVALUE 1
    MAXVALUE 1000000000000000000
    CACHE 1;

ALTER SEQUENCE nrlais_inventory.seq_replication_action
    OWNER TO postgres;

CREATE TABLE nrlais_inventory.t_replication_actions
(
    id integer NOT NULL DEFAULT nextval('nrlais_inventory.seq_replication_action'::regclass),
    operation character varying COLLATE pg_catalog."default" NOT NULL,
    op_time bigint NOT NULL,
    op_user character varying COLLATE pg_catalog."default",
    op_data character varying COLLATE pg_catalog."default",
    CONSTRAINT t_replication_actions_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;