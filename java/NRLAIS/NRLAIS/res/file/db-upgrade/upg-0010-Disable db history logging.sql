-- FUNCTION: nrlais_tx_pkg.update_inv(uuid)

-- DROP FUNCTION nrlais_tx_pkg.update_inv(uuid);

CREATE OR REPLACE FUNCTION nrlais_tx_pkg.update_inv(
	txuid uuid)
    RETURNS boolean
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

DECLARE
	invSchema varchar := 'nrlais_inventory';
	txSchema varchar := 'nrlais_transaction';
	invAtt varchar;
	txAtt varchar;
	table_id integer;
	t varchar;
	obj varchar[];
	insertAndUpdateObjects varchar[][];
	deleteObjects varchar[][];
	checkResult varchar;
	insertQuery varchar;
	updateQuery varchar;
	deleteQuery varchar;
	notDeleted boolean;
	replicationLogQuery varchar;
	replicationLogUID uuid;
	historic_written boolean;
	obj_status integer;
BEGIN
	-- check if the transaction exists
	IF (EXISTS (SELECT 1 FROM nrlais_inventory.t_transaction WHERE uid = $1)) THEN
	
		-- transaction needs to be in status 3 (Approval requested)
		IF (SELECT(SELECT txstatus FROM nrlais_inventory.t_transaction WHERE uid = $1) IN (3) ) THEN
			
			-- run mainCheck for current transaction
			SELECT nrlais_tx_pkg.mainCheck($1) INTO checkResult; 
			
			IF (SELECT checkResult IN ('passed', 'warning')) THEN
			
				-- archive data by performing copy_inv2his function
				--INTAPS SELECT nrlais_tx_pkg.copy_inv2his($1) INTO historic_written;
				
				--INTAPS IF (historic_written IS FALSE) THEN
				--INTAPS 	RAISE EXCEPTION USING ERRCODE = 'UINV6';
				--INTAPS END IF;
						
				--RAISE NOTICE 'Begin writing back data of transaction %', quote_ident($1);
				
				SELECT nrlais_tx_pkg.get_tx_objects($1, true) INTO insertAndUpdateObjects;
				SELECT nrlais_tx_pkg.get_tx_objects($1, false) INTO deleteObjects;
				
				FOREACH obj SLICE 1 IN ARRAY insertAndUpdateObjects 
				LOOP
					table_id := obj[2]::integer;
					SELECT tablename INTO t FROM nrlais_transaction.fdtables WHERE id = table_id;
					
					EXECUTE 'SELECT array_to_string(array_remove(array(SELECT col.columnname FROM '
							|| quote_ident(invSchema) 
							||'.fdcolumns col INNER JOIN '
							|| quote_ident(invSchema) 
							||'.fdtables tab ON col.tableid = tab.id WHERE tab.schema = '
							|| quote_literal(invSchema) 
							||' and tab.tablename = '
							|| quote_literal(t) 
							||' ORDER BY col.id),''sourcetxuid''), '','')' 
						INTO invAtt;
						
					EXECUTE 'SELECT array_to_string(array_remove(array_remove(array(select col.columnname from '
							|| quote_ident(txSchema) 
							||'.fdcolumns col inner join '
							|| quote_ident(txSchema) 
							||'.fdtables tab ON col.tableid = tab.id where tab.schema = '
							|| quote_literal(txSchema) 
							||' and tab.tablename = '
							|| quote_literal(t) 
							||' order by col.id),''editstatus''), ''isprimary''), '','')' 
						INTO txAtt;
						
					EXECUTE 'SELECT status FROM '|| quote_ident(invSchema) ||'.t_transactioncontent WHERE txuid = '|| quote_literal($1) ||' AND inputobjectuid = '|| quote_literal(obj[1]) ||'' INTO obj_status;

					-- object was not edited, do nothing
					IF (obj[3] = 'i' AND obj_status = 2) THEN		

						-- Add source transaction
						--EXECUTE 'UPDATE '|| quote_ident(invSchema) ||'.'|| quote_ident(t) ||' SET sourcetxuid = '|| quote_literal($1) ||' WHERE uid = '|| quote_literal(obj[1]) ||'';
						
						NULL;
						
					-- object is new, insert into inventory
					ELSEIF (obj[3] = 'n' AND obj_status = 2) THEN
						--RAISE NOTICE 'Object % is new', quote_ident(obj[1]);

						insertQuery := format('INSERT INTO ' 
											  || quote_ident(invSchema) || '.' || quote_ident(t) 
											  || '(' || invAtt || ') 
												 SELECT '|| txAtt || ' FROM '
											  || quote_ident(txSchema) ||'.'|| quote_ident(t) 
											  || ' WHERE uid = '|| quote_literal(obj[1]) 
											  ||'');
						
						EXECUTE insertQuery;				
						--RAISE NOTICE 'Performed %', insertQuery;
						
						-- set sourcetxuid to currenttxuid
						EXECUTE 'UPDATE '|| quote_ident(invSchema) ||'.'|| quote_ident(t) ||' SET sourcetxuid = '|| quote_literal($1) ||' WHERE uid = '|| quote_literal(obj[1]) ||'';
						--RAISE NOTICE 'Added source transaction';
						
						-- change object status to 3 (inv updated)
						EXECUTE 'UPDATE '|| quote_ident(invSchema) ||'.t_transactioncontent SET status = 3 WHERE txuid = '|| quote_literal($1::uuid) ||' AND inputobjectuid = '|| quote_literal(obj[1]) ||'';
						
						
						-- write to nrlais_inventory.t_replicationlog
						replicationLogUID := uuid_generate_v1();
						
						replicationLogQuery := format('INSERT INTO nrlais_inventory.t_replicationlog(
								uid, txuid, objuid, objtype, operation, status)
								VALUES (%L, %L, %L, %s, %L, %s);',
								replicationLogUID, $1, obj[1]::uuid, obj[2]::integer, obj[3]::char, 1
						);
						
						EXECUTE replicationLogQuery;
							
						
					-- object was edited, update in inventory
					ELSEIF (obj[3] = 'u' AND obj_status = 2) THEN
						
						updateQuery := format('UPDATE ' 
											  || quote_ident(invSchema) ||'.'|| quote_ident(t) 
											  || ' SET ('|| invAtt ||') = (SELECT '
											  || invAtt 
											  || ' FROM '|| quote_ident(txSchema) ||'.'|| quote_ident(t) 
											  ||' WHERE uid = '|| quote_literal(obj[1]) ||') WHERE uid = '|| quote_literal(obj[1]) ||'');
											  
						EXECUTE updateQuery;				
						
						-- set sourcetxuid to currenttxuid
						EXECUTE 'UPDATE '|| quote_ident(invSchema) ||'.'|| quote_ident(t) ||' SET sourcetxuid = '|| quote_literal($1) ||' WHERE uid = '|| quote_literal(obj[1]) ||'';
					
						-- change object status to 3 (inv updated)
						EXECUTE 'UPDATE '|| quote_ident(invSchema) ||'.t_transactioncontent SET status = 3 WHERE txuid = '|| quote_literal($1::uuid) ||' AND inputobjectuid = '|| quote_literal(obj[1]) ||'';
						
						
						-- write to nrlais_inventory.t_replicationlog
						replicationLogUID := uuid_generate_v1();
						
						replicationLogQuery := format('INSERT INTO nrlais_inventory.t_replicationlog(
								uid, txuid, objuid, objtype, operation, status)
								VALUES (%L, %L, %L, %s, %L, %s);',
								replicationLogUID, $1, obj[1]::uuid, obj[2]::integer, obj[3]::char, 1
						);
						
						EXECUTE replicationLogQuery;						
							
						
					ELSEIF (obj[3] = 'd' AND obj_status = 2) THEN NULL;
					ELSEIF (obj_status = 3) THEN NULL;
					
					ELSE 
						RAISE EXCEPTION USING ERRCODE = 'UINV2', MESSAGE = format('object: %s, table: %s editstatus: %s, object_status: %s', quote_ident(obj[1]::varchar), quote_ident(t::varchar), quote_ident(obj[3]::varchar), obj_status::integer);			
					END IF;

				END LOOP;
				
				FOREACH obj SLICE 1 IN ARRAY deleteObjects 
				LOOP
					table_id := obj[2]::integer;
					SELECT tablename INTO t FROM nrlais_transaction.fdtables WHERE id = table_id;
					
					EXECUTE 'SELECT array_to_string(array_remove(array(SELECT col.columnname FROM '
							|| quote_ident(invSchema) 
							||'.fdcolumns col INNER JOIN '
							|| quote_ident(invSchema) 
							||'.fdtables tab ON col.tableid = tab.id WHERE tab.schema = '
							|| quote_literal(invSchema) 
							||' and tab.tablename = '
							|| quote_literal(t) 
							||' ORDER BY col.id),''sourcetxuid''), '','')' 
						INTO invAtt;
					EXECUTE 'SELECT array_to_string(array_remove(array_remove(array(select col.columnname from '
							|| quote_ident(txSchema) 
							||'.fdcolumns col inner join '
							|| quote_ident(txSchema) 
							||'.fdtables tab ON col.tableid = tab.id where tab.schema = '
							|| quote_literal(txSchema) 
							||' and tab.tablename = '
							|| quote_literal(t) 
							||' order by col.id),''editstatus''), ''isprimary''), '','')' 
						INTO txAtt;
					
					-- get copy status of object
					EXECUTE 'SELECT status FROM '|| quote_ident(invSchema) ||'.t_transactioncontent WHERE txuid = '|| quote_literal($1) ||' AND inputobjectuid = '|| quote_literal(obj[1]) ||'' INTO obj_status;
					
					-- object was deleted, remove from inventory
					IF (obj[3] = 'd' AND obj_status = 2) THEN	
						
						--RAISE NOTICE 'Object % is deleted', quote_ident(obj[1]);
						EXECUTE 'SELECT EXISTS(SELECT uid FROM ' || quote_ident(invSchema) ||'.'|| quote_ident(t) || ' WHERE uid = '|| quote_literal(obj[1]) ||')' INTO notDeleted;
						
						IF (notDeleted = true) THEN
							deleteQuery := 'DELETE FROM '|| quote_ident(invSchema) ||'.'|| quote_ident(t) ||' WHERE uid = '|| quote_literal(obj[1]) ||'';				
							EXECUTE deleteQuery;
							
							--RAISE NOTICE 'Performed %', deleteQuery;	
							
							-- change object status to 3 (inv updated)
							EXECUTE 'UPDATE '|| quote_ident(invSchema) ||'.t_transactioncontent SET status = 3 WHERE txuid = '|| quote_literal($1) ||' AND inputobjectuid = '|| quote_literal(obj[1]) ||'';

						ELSE
							RAISE EXCEPTION USING ERRCODE = 'UINV3', MESSAGE = obj[1]::varchar;
						END IF;
						
						-- write to nrlais_inventory.t_replicationlog
						replicationLogUID := uuid_generate_v1();
						
						replicationLogQuery := format('INSERT INTO nrlais_inventory.t_replicationlog(
								uid, txuid, objuid, objtype, operation, status)
								VALUES (%L, %L, %L, %s, %L, %s);',
								replicationLogUID, $1, obj[1]::uuid, obj[2]::integer, obj[3]::char, 1
						);
						
						EXECUTE replicationLogQuery;						

						
					ELSEIF (obj[3] = 'i' AND obj_status = 2) THEN NULL;
					ELSEIF (obj[3] = 'n' AND obj_status = 2) THEN NULL;
					ELSEIF (obj[3] = 'u' AND obj_status = 2) THEN NULL;
					ELSEIF (obj_status = 3) THEN NULL;
					
					ELSE 
						RAISE EXCEPTION USING ERRCODE = 'UINV2', MESSAGE = format('object: %s, table: %s editstatus: %s, object_status: %s', quote_ident(obj[1]::varchar), quote_ident(t::varchar), quote_ident(obj[3]::varchar), obj_status::integer);					
					END IF;

				END LOOP;
			ELSE
				RAISE EXCEPTION USING ERRCODE = 'UINV1';
			END IF;
			
			PERFORM nrlais_tx_pkg.logger('LOG','update_inv', 'Data of txUID '||quote_literal($1)||' updated in inventory schema');

			RETURN true;
		ELSE
			RAISE EXCEPTION USING ERRCODE = 'UINV4', MESSAGE = (SELECT s.en FROM nrlais_sys.t_cl_txstatus s JOIN nrlais_inventory.t_transaction t ON (s.codeid = t.txstatus) WHERE t.uid = $1);     
		END IF;
	ELSE
		RAISE EXCEPTION USING ERRCODE = 'UINV5';     
	END IF;
	
EXCEPTION
	WHEN SQLSTATE 'UINV1' THEN
		PERFORM nrlais_tx_pkg.logger('ERROR','update_inv', 'No valid data in txUID '|| quote_literal($1) ||'');
		RAISE EXCEPTION 'Data of transaction % is not valid', quote_literal($1);
	
	WHEN SQLSTATE 'UINV2' THEN
		PERFORM nrlais_tx_pkg.logger('ERROR','update_inv', 'Unknown edit or copy status in txUID '||quote_literal($1)||'');
		RAISE EXCEPTION 'Unknown edit or copy status: %', quote_literal(SQLERRM);
	
	WHEN SQLSTATE 'UINV3' THEN
		RAISE EXCEPTION 'Object % already deleted or not existent', quote_literal(SQLERRM);
	
	WHEN SQLSTATE 'UINV4' THEN
		PERFORM nrlais_tx_pkg.logger('ERROR','update_inv', 'Transaction status is '||quote_ident(SQLERRM)||'');
		RAISE EXCEPTION 'Transaction status is % ', quote_ident(SQLERRM);   	
		
	WHEN SQLSTATE 'UINV5' THEN
		PERFORM nrlais_tx_pkg.logger('ERROR','update_inv', 'Cannot find transaction UID '||quote_literal($1)||'');
		RAISE EXCEPTION 'Cannot find transaction UID % ', quote_literal($1);  
		
	WHEN SQLSTATE 'UINV6' THEN
		PERFORM nrlais_tx_pkg.logger('ERROR','update_inv', 'Failed to archive the inventory data of '||quote_literal($1)||'');
		RAISE EXCEPTION 'Failed to archive the inventory data of txUID %', quote_literal($1);  	
   
    WHEN no_data_found THEN
		RAISE EXCEPTION 'No data found (%) ', quote_literal(SQLERRM);
	
	WHEN unique_violation THEN
		RAISE EXCEPTION 'Unique Constraint Violation (%) ', quote_literal(SQLERRM);   				
	
	WHEN OTHERS THEN
		RAISE EXCEPTION 'Error update_inv: %', SQLERRM;
END;

$BODY$;

ALTER FUNCTION nrlais_tx_pkg.update_inv(uuid)
    OWNER TO docker;

COMMENT ON FUNCTION nrlais_tx_pkg.update_inv(uuid)
    IS 'The function update_inv updates the inventory schema with transaction objects.';

