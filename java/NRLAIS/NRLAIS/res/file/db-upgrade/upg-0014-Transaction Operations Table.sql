--DROP TABLE nrlais_transaction.t_transaction_op;
--DROP  SEQUENCE nrlais_transaction.seq_tran_op;

CREATE SEQUENCE nrlais_transaction.seq_tran_op
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE nrlais_transaction.seq_tran_op
    OWNER TO postgres;
CREATE TABLE nrlais_transaction.t_transaction_op
(
    txuid uuid NOT NULL,
    "seqNo" integer NOT NULL DEFAULT nextval('nrlais_transaction.seq_tran_op'::regclass),
    operation json NOT NULL,
    "userName" character varying(100) COLLATE pg_catalog."default" NOT NULL,
    "time" bigint NOT NULL,
    "opType" character varying(100) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT t_transaction_op_pkey PRIMARY KEY ("seqNo")
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE nrlais_transaction.t_transaction_op
    OWNER to postgres;

