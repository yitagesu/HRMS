/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Tewelde
 * Created: Jan 6, 2018
 */

INSERT INTO nrlais_sys.t_cl_restrictiontype(
	codeid, en, am, ti, om, codeinregion, description)
	VALUES ('6','Irrigation','','','','XX','');
INSERT INTO nrlais_sys.t_cl_restrictiontype(
	codeid, en, am, ti, om, codeinregion, description)
	VALUES ('9','Other Servitude/Easement','','','','XX','');

INSERT INTO nrlais_sys.t_cl_restrictiontype(
	codeid, en, am, ti, om, codeinregion, description)
	VALUES ('11','Court Restraining Order','','','','XX','');
INSERT INTO nrlais_sys.t_cl_restrictiontype(
	codeid, en, am, ti, om, codeinregion, description)
	VALUES ('12','Revenue Authority Restraining Order','','','','XX','');
INSERT INTO nrlais_sys.t_cl_restrictiontype(
	codeid, en, am, ti, om, codeinregion, description)
	VALUES ('19','Other Restrictive Interest','','','','XX','');