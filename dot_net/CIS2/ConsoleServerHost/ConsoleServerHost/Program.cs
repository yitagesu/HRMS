﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleServerHost
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                CISServer.CISService.Run();
            }
            catch(Exception ex)
            {
                while (ex != null)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine(ex.StackTrace);
                    ex = ex.InnerException;
                }
            }
        }
    }
}
